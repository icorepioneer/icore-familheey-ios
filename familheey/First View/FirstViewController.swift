//
//  FirstViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 01/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON

class FirstViewController: UIViewController {
    private var networkProvider = MoyaProvider<FamilyheeyApi>()

    @IBOutlet weak var tempView: UIView!
    @IBOutlet weak var getStartButt: UIButton!
    @IBOutlet weak var lblVersion: UILabel!
    @IBOutlet weak var lblAppVersion: UILabel!
    
    var version = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
        
        if version.isEmpty{
            lblVersion.isHidden = true
            lblAppVersion.isHidden = true
        }
        else{
            lblVersion.text = "Version: \(version)"
            lblAppVersion.text = "Version: \(version)"
        }
        
        getStartButt.cornerRadius = 13
        getStartButt.clipsToBounds = true
        let isLogin = UserDefaults.standard.value(forKey: "isLogin") as? String
        if let id =  UserDefaults.standard.value(forKey: "userId") as? String{
            print(id)
            if isLogin == "true"{
               tempView.isHidden = false
                if id.isEmpty{
                    
                }
                else{
//                    getPreloadFamilyList2()
                    
                }
            }
            else{
                if id.isEmpty{
                    
                }
                else{
//                    getPreloadFamilyList2()
                }
              tempView.isHidden = true
            }
        }
        else{
            tempView.isHidden = true
        }
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func getStartClicked(_ sender: Any) {

        
        let registrationView = self.storyboard?.instantiateViewController(withIdentifier: "registrationView") as! ViewController
        self.navigationController?.pushViewController(registrationView, animated: true)
    }
    
    
     //MARK:- Web API
    func getPreloadFamilyList2(){
        let param = [
            "userid" : UserDefaults.standard.value(forKey: "userId") as! String,
            "searchtxt" : "",
            "offset": 0,
            "limit": 10,
            "phonenumbers":[],
            "type":"family"
            ] as [String : Any]
        
        
//        ActivityIndicatorView.show("Loading.....")
        DispatchQueue.global(qos: .userInitiated).async {
            self.networkProvider.request(.globalSearch(parameter: param)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                        
                        if response.statusCode == 200{
                            print("@@@####!!!!---\(response.data)--\(response)")
                            
                            let someDictionaryFromJSON = try JSONSerialization.jsonObject(with: response.data, options: .allowFragments) as! [String: Any]
                            print(someDictionaryFromJSON)
                            let tempArr = someDictionaryFromJSON
                            print(tempArr)
                            do{
                                let searchResult = try SearchList(tempArr)
                                print(searchResult)
                                if let results = searchResult as SearchList?{
                                    print(results)
                                    
                                    
                                    if results.searchResult2 != nil{
                                        appDel.PreloadFamilyList.append(contentsOf: results.searchResult2!)
                                        DispatchQueue.main.async {
                                            NotificationCenter.default.post(name: Notification.Name("NotificationForDiscoverTab"), object: nil)
                                        }
                                    }
                                }

                            }catch{
                                //                                       print("catched")
                                return
                            }
                        }
                        else if response.statusCode == 401{
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                self.getPreloadFamilyList2()
                            }
                        }
                        else{
                            self.displayAlert(alertStr: "Something went wrong! Please try again later", title: "Error")
                            ActivityIndicatorView.hiding()
                        }
   
                    }catch let err {
                        ActivityIndicatorView.hiding()
                        Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                    }
                case .failure( let error):
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                    break
                }
            }
            
        }
    }
    
    //MARK:- WebAPI
    func getPreloadFamilyList(){
        let param = [
            "userid" : UserDefaults.standard.value(forKey: "userId") as! String,
            "searchtxt" : "",
            "offset": 0,
            "limit": 30,
            "phonenumbers":[],
            "type":"family"
            ] as [String : Any]
        APIServiceManager.callServer.searchRequest(url: EndPoint.search, param: param, fromSearchTabViewController: true, success: { (response) in
            print(response)
            if let results = response as! SearchList?{

                if results.status_code == 200{

                    if results.searchResult2 != nil{
                        appDel.PreloadFamilyList.append(contentsOf: results.searchResult2!)
                    }
                    
                }
            }
            
            
        }) { (error) in
            
            
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
