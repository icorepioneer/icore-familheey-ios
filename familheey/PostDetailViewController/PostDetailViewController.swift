//
//  PostDetailViewController.swift
//  familheey
//
//  Created by Giri on 27/12/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON
import Kingfisher
import SafariServices



class PostDetailViewController: UIViewController,postUpdateDelegate  {
    //    var isFromSticky = false
    @IBOutlet weak var tblListView: UITableView!
    let maxNumberOfLines = 2
    var ArrayPosts = [JSON]()
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    var postoptionsTittle = ["Mute conversation","Edit post","Delete post","Report"]
    var postId = ""
    var ShareOptionArray = [String]()
    var documentInteractionController: UIDocumentInteractionController!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getPostDetails(PostId: postId)

    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        
    }
    //MARK:- Get Post Details
    func postupdateSuccess(index:Int , SuccessData:JSON){
        
    }
    
    func getPostDetails(PostId:String){
        let param = ["type":"post","post_id":postId,"user_id":UserDefaults.standard.value(forKey: "userId") as! String]
        
        ActivityIndicatorView.show("Please wait....")
        networkProvider.request(.post_by_user(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    ActivityIndicatorView.hiding()
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        if let response = jsonData["data"].array{
                            self.ArrayPosts.append(contentsOf: response)
                            if self.ArrayPosts.count > 0 {
                                self.tblListView.isHidden = false
                                self.tblListView.delegate = self
                                self.tblListView.dataSource = self
                                self.tblListView.reloadData()
                                return
                            }
                            else{
                                self.displayAlertChoice(alertStr: "Oops! This content is no longer available.", title: "", completion: { (result) in
                                    self.navigationController?.popViewController(animated: true)
                                })
                            }
                        }
                        else{
                            self.displayAlertChoice(alertStr: "Oops! This content is no longer available.", title: "", completion: { (result) in
                                self.navigationController?.popViewController(animated: true)
                            })
                        }
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getPostDetails(PostId: PostId)
                        }
                    }
                    else{
                        
                        self.displayAlertChoice(alertStr: "Oops! This content is no longer available.", title: "", completion: { (result) in
                            self.navigationController?.popViewController(animated: true)
                        })
                        
                        
                    }
                    //                        self.navigationController?.popViewController(animated: false)
                }catch let err {
                    ActivityIndicatorView.hiding()
                    //  Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    // Open App Link
    func openAppLink(url:String){
        
        let param = ["url" : url]
        
        networkProvider.request(.openAppGetParams(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("@@##!!! REsult\(jsonData)")
                    if response.statusCode == 200{
                        let dataArr = jsonData["data"]
                        let subType = dataArr["sub_type"].stringValue
                        let type =  dataArr["type"].stringValue
                        
                        if subType == notificationSubTypes.publicfeed || subType == notificationSubTypes.familyfeed && type == notificationTypes.home{
                            let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
                            vc.postId = "\(dataArr["type_id"].intValue)"
                            self.navigationController?.pushViewController(vc, animated: false)
                            
                        }
                        else if type == notificationTypes.family{
                            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                            let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
                            intro.groupId = "\(dataArr["type_id"].intValue)"
                            self.navigationController?.pushViewController(intro, animated: false)
                        }
                        else{
                            let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
                            vc.enableEventEdit = false
                            vc.isFromNotification = "true"
                            vc.eventId = "\(dataArr["type_id"].intValue)"
                            self.navigationController?.pushViewController(vc, animated: false)
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.openAppLink(url: url)
                        }
                    }
                    else{
                        
                    }
                    
                }catch _ {
                    
                    
                }
            case .failure( _):
                break
            }
        }
    }
    
    //MARK:- Button Actions
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
        
    }
    
    @IBAction func onClickReadmoreAction(_ sender: UIButton) {
        
        let cell = tblListView.cellForRow(at: IndexPath.init(row: sender.tag, section: 0)) as! PostImageTableViewCell
        if cell.labelPost.calculateMaxLines() > maxNumberOfLines &&  cell.labelPost.numberOfLines == maxNumberOfLines{
            
            tblListView.beginUpdates()
            cell.labelPost.numberOfLines = 0
            cell.readmoreButton.setTitle("view less", for: .normal)
            tblListView.endUpdates()
            
            
        }
        else if cell.labelPost.calculateMaxLines() > maxNumberOfLines &&  cell.labelPost.numberOfLines == 0 {
            UIView.performWithoutAnimation {
                tblListView.beginUpdates()
                cell.labelPost.numberOfLines = maxNumberOfLines
                cell.readmoreButton.setTitle("view more", for: .normal)
                tblListView.endUpdates()
            }
        }
        
    }
    
    @IBAction func onClickReadmoreShareAction(_ sender: UIButton) {
        
        let cell = tblListView.cellForRow(at: IndexPath.init(row: sender.tag, section: 0)) as! SharedPostTableViewCell
        if cell.labelPost.calculateMaxLines() > maxNumberOfLines &&  cell.labelPost.numberOfLines == maxNumberOfLines{
            
            tblListView.beginUpdates()
            cell.labelPost.numberOfLines = 0
            cell.readmoreButton.setTitle("view less", for: .normal)
            tblListView.endUpdates()
            
            
        }
        else if cell.labelPost.calculateMaxLines() > maxNumberOfLines &&  cell.labelPost.numberOfLines == 0 {
            UIView.performWithoutAnimation {
                tblListView.beginUpdates()
                cell.labelPost.numberOfLines = maxNumberOfLines
                cell.readmoreButton.setTitle("view more", for: .normal)
                tblListView.endUpdates()
            }
        }
        
    }
    
    @IBAction func onClickSharedUsersShareAction(_ sender: UIButton) {
        
        /* let post = ArrayPosts[sender.tag]
         
         if let sharedusers = post["shared_user_names"].string , !sharedusers.isEmpty{
         
         let array = sharedusers.components(separatedBy: ",")
         
         showActionSheet(titleArr: array as NSArray, title: "Select option", completion: { (index) in
         
         })
         
         }*/
        let post = ArrayPosts[sender.tag]
        let mainStoryBoard = UIStoryboard(name: "fourth", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "UserFamilyListViewController") as! UserFamilyListViewController
        if post["orgin_id"].stringValue.count > 0{
            vc.postId = post["orgin_id"].stringValue
        }
        else{
            vc.postId = post["post_id"].stringValue
        }
        vc.isFrompost = true
        vc.isFrom = "groupshare"
        vc.familyID = post["to_group_id"].stringValue
        self.navigationController?.pushViewController(vc, animated:true)
    }
    
    @IBAction func onClickShareSharedPostAction(_ sender: UIButton) {
        
        /* let post = ArrayPosts[sender.tag]
         
         let mainStoryBoard = UIStoryboard(name: "third", bundle: nil)
         let vc = mainStoryBoard.instantiateViewController(withIdentifier: "PostShareViewController") as! PostShareViewController
         vc.postId = post["post_id"].stringValue
         self.navigationController?.pushViewController(vc, animated:true)*/
        self.ShareOptionArray = ["Share in Familheey","Share in Social Media"]
        showActionSheet(titleArr: ShareOptionArray as NSArray, title: "Select options") { (index) in
            if index == 0{
                let post = self.ArrayPosts[sender.tag]
                print(post)
                //                 DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                let mainStoryBoard = UIStoryboard(name: "third", bundle: nil)
                let vc = mainStoryBoard.instantiateViewController(withIdentifier: "PostShareViewController") as! PostShareViewController
                vc.postId = post["post_id"].stringValue
                self.navigationController?.pushViewController(vc, animated: false)
                //                }
                //                 self.navigationController?.pushViewController(vc, animated: false)
            }
            else if index == 1{
                let post = self.ArrayPosts[sender.tag]
                _ =
                    Helpers.showActivityViewcontroller(url: "\(BaseUrl.PostUrlPrd)\(post["post_id"].stringValue)", VC: self)
            }
            else{
                
            }
        }
    }
    
    @IBAction func onClickViewsAction(_ sender: UIButton) {
        let post = ArrayPosts[sender.tag]
        let mainStoryBoard = UIStoryboard(name: "fourth", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "UserFamilyListViewController") as! UserFamilyListViewController
        vc.postId = post["post_id"].stringValue
        vc.isFrompost = true
        vc.isFrom = "postview"
        self.navigationController?.pushViewController(vc, animated:true)
        
    }
    
    @IBAction func onClickSharedUsersAction(_ sender: UIButton) {
        let post = ArrayPosts[sender.tag]
        let mainStoryBoard = UIStoryboard(name: "fourth", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "UserFamilyListViewController") as! UserFamilyListViewController
        if post["orgin_id"].stringValue.count > 0{
            vc.postId = post["orgin_id"].stringValue
        }
        else{
            vc.postId = post["post_id"].stringValue
        }
        vc.isFrompost = true
        vc.isFrom = "usershare"
        self.navigationController?.pushViewController(vc, animated:true)
    }
    
    @IBAction func onClickCommentAction(_ sender: UIButton) {
        let post = ArrayPosts[sender.tag]
        let mainStoryBoard = UIStoryboard(name: "Conversations", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "ConversationsViewController") as! ConversationsViewController
        vc.post = post
        vc.conversationT = .post
        self.navigationController?.pushViewController(vc, animated:true)
    }
    
    @IBAction func onClickPostMenu(_ sender: UIButton) {
        
        let post = ArrayPosts[sender.tag]
        
        postoptionsTittle.removeAll()
        
        
        if post["created_by"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")" {
            postoptionsTittle =  ["Edit","Delete"]
        }
        else{
            postoptionsTittle =  ["Hide","Report"]
        }
        
        if post["muted"].stringValue == "true"{
            postoptionsTittle.insert("Unmute", at: 0)
        }
        else{
            postoptionsTittle.insert("Mute", at: 0)
        }
        //          if isFromSticky  {
        ////            if post["stick"].boolValue == true{
        ////                        postoptionsTittle.insert("Unstick", at: 3)
        ////                    }
        ////                    else{
        ////                postoptionsTittle.insert("Stick", at: 3)
        ////                       }
        //            postoptionsTittle.insert("Unstick", at: 3)
        //        }
        
        
        showActionSheet(titleArr: postoptionsTittle as NSArray, title: "Select options") { (index) in
            
            if index == 2{
                if post["created_by"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")"{
                    self.deletePostForCreator(tag: sender.tag)
                }
                else{
                    let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "ReportPostViewController") as! ReportPostViewController
                    
                    vc.postDetails =  self.ArrayPosts[sender.tag]
                    vc.isFrom = "post"
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
                //             if index == 3{
                //                self.applyForStickyPost(tag: sender.tag)
                //             }
            else if index == 1{
                if post["created_by"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")"{
                    
                    let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "CreatePostViewController") as! CreatePostViewController
                    
                    vc.fromEdit = true
                    vc.editPostDetails = post
                    vc.callBackDelegate = self
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    self.removePostFromTimeline(tag: sender.tag)
                }
            }
            else if index == 100{
            }
            else{
                self.muteConversation(tag: sender.tag)
            }
        }
    }
    
    @IBAction func onClickOpenRequest(_ sender: UIButton) {
        let need = ArrayPosts[sender.tag]
        if let postType = need["publish_type"].string, postType.lowercased() == "request"{
            let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "RequestDetailsViewController") as! RequestDetailsViewController
            vc.requestId = need["publish_id"].stringValue
            //        vc.requestDic = need
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            let storyboard = UIStoryboard.init(name: "third", bundle: nil)
            /* let addMember = storyboard.instantiateViewController(withIdentifier: "AlbumListingViewController") as! AlbumListingViewController
             addMember.groupID = need["to_group_id"].stringValue
             addMember.isFromPost = true
             addMember.isFromdocument = false
             addMember.albumId = need["publish_id"].stringValue
             
             
             self.navigationController?.pushViewController(addMember, animated: false)*/
            let AlbumDetailsViewController = storyboard.instantiateViewController(withIdentifier: "AlbumDetailsViewController") as! AlbumDetailsViewController
            //            AlbumDetailsViewController.albumDetailsDict = items
            AlbumDetailsViewController.isFrom = "groups"
            AlbumDetailsViewController.isFromPost = true
            AlbumDetailsViewController.groupId = need["to_group_id"].stringValue
            if let postType = need["publish_type"].string, postType.lowercased() == "albums"{
                AlbumDetailsViewController.folder_type = "albums"
            }
            else{
                AlbumDetailsViewController.folder_type = "documents"
                AlbumDetailsViewController.isFromDocs = true
            }
            
            AlbumDetailsViewController.createdBy = need["created_by"].stringValue
            AlbumDetailsViewController.albumId = need["publish_id"].stringValue
            self.navigationController?.pushViewController(AlbumDetailsViewController, animated: true)
        }
        
    }
    @IBAction func onClickThanksReadMore(_ sender: UIButton) {
        let cell = tblListView.cellForRow(at: IndexPath.init(row: sender.tag, section: 0)) as! PostThanksTableViewCell
        
        if cell.labelPost.calculateMaxLines() > maxNumberOfLines &&  cell.labelPost.numberOfLines == maxNumberOfLines{
            
            tblListView.beginUpdates()
            cell.labelPost.numberOfLines = 0
            cell.readmoreButton.setTitle("view less", for: .normal)
            tblListView.endUpdates()
        }
        else if cell.labelPost.calculateMaxLines() > maxNumberOfLines &&  cell.labelPost.numberOfLines == 0 {
            UIView.performWithoutAnimation {
                tblListView.beginUpdates()
                cell.labelPost.numberOfLines = maxNumberOfLines
                cell.readmoreButton.setTitle("view more", for: .normal)
                tblListView.endUpdates()
            }
        }
    }
    
    @IBAction func onClickShareAction(_ sender: UIButton) {
        
        /*  let post = ArrayPosts[sender.tag]
         print(post)
         
         let mainStoryBoard = UIStoryboard(name: "third", bundle: nil)
         let vc = mainStoryBoard.instantiateViewController(withIdentifier: "PostShareViewController") as! PostShareViewController
         vc.postId = post["post_id"].stringValue
         self.navigationController?.pushViewController(vc, animated:true)*/
        self.ShareOptionArray = ["Share in Familheey","Share in Social Media"]
        showActionSheet(titleArr: ShareOptionArray as NSArray, title: "Select options") { (index) in
            if index == 0{
                let post = self.ArrayPosts[sender.tag]
                print(post)
                //                 DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                let mainStoryBoard = UIStoryboard(name: "third", bundle: nil)
                let vc = mainStoryBoard.instantiateViewController(withIdentifier: "PostShareViewController") as! PostShareViewController
                vc.postId = post["post_id"].stringValue
                self.navigationController?.pushViewController(vc, animated: false)
                //                }
                //                 self.navigationController?.pushViewController(vc, animated: false)
            }
            else if index == 1{
                let post = self.ArrayPosts[sender.tag]
                _ =
                    Helpers.showActivityViewcontroller(url: "\(BaseUrl.PostUrlPrd)\(post["post_id"].stringValue)", VC: self)
            }
            else{
                
            }
        }
    }
    //MARK:- Unstick Post
    func applyForStickyPost(tag:Int){
        
        let post = self.ArrayPosts[tag]
        print(post)
        let param = ["post_id" : post["post_id"].stringValue,"group_id":post["to_group_id"].stringValue,"type":"unstick"]
        print(param)
        ActivityIndicatorView.show("Please wait....")
        self.networkProvider.request(.applyStickyPost(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        print("view contents : \(jsonData)")
                        self.navigationController?.popViewController(animated: true)
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.applyForStickyPost(tag: tag)
                        }
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
        
        
    }
    
    //MARK:- Delete post
    func deletePostForCreator(tag:Int){
        let alert = UIAlertController(title: "Familheey", message: "Do you really want to delete this post? Deleting this post will delete it from everywhere you posted it to and you will lose all associated conversations", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "DELETE", style: .destructive, handler: { _ in
            let post = self.ArrayPosts[tag]
            
            
            let param = ["id" : post["post_id"].stringValue,"user_id":UserDefaults.standard.value(forKey: "userId") as! String]
            
            ActivityIndicatorView.show("Please wait....")
            self.networkProvider.request(.deletePost(parameter: param)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                        print("view contents : \(jsonData)")
                        if response.statusCode == 200{
                            print("view contents : \(jsonData)")
                            
                            self.navigationController?.popViewController(animated: true)
                            
                        }
                        else if response.statusCode == 401 {
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                self.deletePostForCreator(tag: tag)
                            }
                        }
                        
                        ActivityIndicatorView.hiding()
                        
                    }catch let err {
                        ActivityIndicatorView.hiding()
                        // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                    }
                case .failure( let error):
                    ActivityIndicatorView.hiding()
                    //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                    break
                }
            }
            
        }))
        alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK:- Remove post from timeline
    
    func removePostFromTimeline(tag:Int){
        let post = ArrayPosts[tag]
        
        let param = ["post_id" : post["post_id"].stringValue, "user_id": UserDefaults.standard.value(forKey: "userId") as! String]
        
        ActivityIndicatorView.show("Please wait....")
        networkProvider.request(.removePost(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        print("view contents : \(jsonData)")
                        
                        self.navigationController?.popViewController(animated: true)
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.removePostFromTimeline(tag: tag)
                        }
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    //MARK:- Mute conversation
    
    func muteConversation(tag:Int){
        let post = ArrayPosts[tag]
        let param = ["post_id" : post["post_id"].stringValue, "user_id": UserDefaults.standard.value(forKey: "userId") as! String]
        
        ActivityIndicatorView.show("Please wait....")
        networkProvider.request(.mute_conversation(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        print("view contents : \(jsonData)")
                        //                        self.getPosts()
                        let temp = jsonData["data"].dictionaryValue
                        if temp["is_active"]!.boolValue{
                            var temp = post
                            temp["muted"] = true
                            self.ArrayPosts[tag] = temp
                            print("#######NEW ARR - \(self.ArrayPosts[tag])")
                        }
                        else{
                            var temp = post
                            temp["muted"] = false
                            self.ArrayPosts[tag] = temp
                            print("#######NEW ARR - \(self.ArrayPosts[tag])")
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.muteConversation(tag: tag)
                        }
                    }
                    
                    ActivityIndicatorView.hiding()
                }catch let err {
                    ActivityIndicatorView.hiding()
                    //  Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    //MARK:- View Post
    
    func viewCurrentPost(index:Int){
        
        if  ArrayPosts.count > index  {
            
            let post = ArrayPosts[index]
            
            let param = ["post_id" : post["post_id"].stringValue, "user_id": UserDefaults.standard.value(forKey: "userId") as! String]
            
            networkProvider.request(.add_view_count(parameter: param)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                        print("view contents : \(jsonData)")
                        if response.statusCode == 200{
                            print("view contents : \(jsonData)")
                        }
                        else if response.statusCode == 401 {
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                self.viewCurrentPost(index: index)
                            }
                        }
                        
                    }catch let err {
                    }
                case .failure( let error):
                    break
                }
            }
        }
    }
    
    
    
    
}
extension PostDetailViewController : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ArrayPosts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard ArrayPosts.count != 0 else {
            return UITableViewCell.init()
        }
        
        let post = ArrayPosts[indexPath.row]
        
        if indexPath.row == 0{
            self.viewCurrentPost(index: indexPath.row)
        }
        
        if let postType = post["publish_type"].string, postType.lowercased() == "request" || postType.lowercased() == "albums" || postType.lowercased() == "documents" {
            
            let cellid = "PostThanksTableViewCell"
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! PostThanksTableViewCell
            
            
            cell.readmoreButton.tag = indexPath.row
            cell.buttonShare.tag = indexPath.row
            cell.buttonRightMenu.tag = indexPath.row
            cell.commentsButton.tag = indexPath.row
            cell.viewsButton.tag = indexPath.row
            cell.sharedUsersButton.tag = indexPath.row
            cell.buttonComments.tag = indexPath.row
            cell.buttonComments.tag = indexPath.row
            cell.btnFamilyClick.tag = indexPath.row
            cell.btnOpenRequest.tag = indexPath.row
            
            //User Details
            cell.labelUserName.text = post["created_user_name"].string ?? "Unknown"
            cell.labelPostedIn.text = post["group_name"].string ?? "Public"
            if cell.labelPostedIn.text == "Public"
            {
                cell.btnFamilyClick.isUserInteractionEnabled = false
            }
            else
            {
                cell.btnFamilyClick.isUserInteractionEnabled = true
                
            }
            //            cell.labelDate.text = post["createdAt"].string ?? ""
            cell.labelDate.text = HomeTabViewController.dateFormatterCommon(dateStr: post["createdAt"].stringValue)
            
            let proPic = post["propic"].string ?? ""
            
            DispatchQueue.main.async {
                
                if proPic.count != 0{
                    let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic
                    let imgUrl = URL(string: temp)
                    cell.imageViewProfilePic.kf.indicatorType = .activity
                    
                    cell.imageViewProfilePic.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                }
                else{
                    cell.imageViewProfilePic.image = #imageLiteral(resourceName: "Male Colored")
                }
            }
            cell.imageViewProfilePic.isTappable(id: post["created_by"].intValue)
            //Post
            cell.labelPost.text = post["snap_description"].string ?? ""
            
            //Details
            cell.lableNumberOfConversations.text = post["conversation_count"].stringValue
            cell.lblNumberOfShares.text = post["shared_user_count"].stringValue
            cell.labelNumberOfViews.text = post["views_count"].stringValue
            
            if let Attachment = post["post_attachment"].array, Attachment.count != 0{
                print("updating slide........")
                print(indexPath.row)
                cell.updateSliderThankYou(data: Attachment, Ptype: postType)
            }
            else{
                print("No Slide........")
                print(indexPath.row)
                
                cell.viewPostBg.isHidden = false
                cell.viewPageControllBG.isHidden = false
                if let urlArray = post["valid_urls"].array, urlArray.count != 0{
                    if urlArray.count == 1{
                        if let urlMetadata = post["url_metadata"].dictionary, urlMetadata.count != 0{
                            if let result = urlMetadata["urlMetadataResult"], result.count != 0{
                                cell.viewPostBg.isHidden = false
                                cell.viewPageControllBG.isHidden = true
                                
                                cell.updateSliderLink(data: [result])
                            }
                            else{
                                print(indexPath.row)
                                cell.viewPostBg.isHidden = true
                                cell.viewPageControllBG.isHidden = true
                            }
                        }
                        
                        
                    }
                    else{
                        print(indexPath.row)
                        cell.viewPostBg.isHidden = true
                        cell.viewPageControllBG.isHidden = true
                    }
                }
                else{
                    print(indexPath.row)
                    cell.viewPostBg.isHidden = true
                    cell.viewPageControllBG.isHidden = true
                }
            }
            
            if cell.labelPost.calculateMaxLines() > maxNumberOfLines{
                cell.labelPost.numberOfLines = maxNumberOfLines
                cell.readMoreView.isHidden = false
                
                //            tableView.layoutIfNeeded()
            }
            else{
                cell.labelPost.numberOfLines = 0
                cell.readMoreView.isHidden = true
                //            tableView.layoutIfNeeded()
            }
            //                cell.delegate = self
            
            
            // String(describing: UserDefaults.standard.value(forKey: "userId"))
            let user = UserDefaults.standard.value(forKey: "userId") as! String
            if  user  == post["created_by"].stringValue{
                
                cell.viewViews.isHidden = false
                cell.viewShared.isHidden = false
                if post["conversation_enabled"].bool ?? false{
                    cell.viewComments.isHidden = false
                }
                else{
                    cell.viewComments.isHidden = true
                }
            }
            else{
                
                cell.viewViews.isHidden = true
                cell.viewShared.isHidden = true
                if post["conversation_enabled"].bool ?? false{
                    cell.viewComments.isHidden = false
                }
                else{
                    cell.viewComments.isHidden = true
                }
            }
            
            if post["conversation_count_new"].intValue > 0{
                cell.lblNewConvrstn.isHidden = false
            }
            else{
                cell.lblNewConvrstn.isHidden = true
            }
            
            
            if post["is_shareable"].bool ?? false {
                
                cell.shareView.isHidden = false
                
            }
            else{
                
                cell.shareView.isHidden = true
                
            }
//            var stringArr = [String]()
//            if let mentionedUsers = post["publish_mention_users"].array, mentionedUsers.count != 0{
//                for index in mentionedUsers{
//                    let name = index["user_name"].stringValue
//                    let temArr = name.components(separatedBy: " ")
//                    if temArr.count > 0{
//                        let formattedName = "@\(temArr[0])"
//                        stringArr.append(formattedName)
//                    }
//                }
//                cell.lblMentionedNames.text = stringArr.joined(separator: ", ")
//            }
//            else{
//                cell.lblMentionedNames.text = ""
//            }
            cell.btnMentionedNames.tag = indexPath.row

             if let mentionedUsers = post["publish_mention_users"].array, mentionedUsers.count != 0{

                               if mentionedUsers.count == 1
                               {
            cell.lblMentionedNames.text = "@ \(mentionedUsers[0]["user_name"].stringValue.components(separatedBy: " ")[0])"
                                                   cell.lblMentionedNames.isHidden = false
                                cell.btnMentionedNames.isHidden = false
                                
                                                   cell.collVwOfMentionedNames.isHidden = true

                                                   cell.heightOfMentionedView.constant = 30
                              
                                
                                }
                                else
                               {
                                cell.lblMentionedNames.isHidden = true
                                cell.btnMentionedNames.isHidden = true

                                                                      cell.collVwOfMentionedNames.isHidden = false
                                cell.loadMentionedNames(data:mentionedUsers)

                                }
                                

                            }
                            else{
                                cell.lblMentionedNames.text = ""
                                cell.lblMentionedNames.isHidden = true
                                cell.collVwOfMentionedNames.isHidden = true
                                cell.btnMentionedNames.isHidden = true

                                cell.heightOfMentionedView.constant = 0

                            }
            cell.viewShared.isHidden = true
            if let type = post["publish_type"].string, type.lowercased() == "request"{
                cell.lblItemNames.isHidden = false
                if let mentionedItems = post["publish_mention_items"].array, mentionedItems.count != 0{
                    //                    for index in mentionedUsers{
                    //                        let name = index["user_name"].stringValue
                    //                        let temArr = name.components(separatedBy: " ")
                    //                        if temArr.count > 0{
                    //                            let formattedName = "@\(temArr[0])"
                    //                            stringArr.append(formattedName)
                    //                        }
                    //                    }
                    cell.lblItemNames.text = mentionedItems[0]["item_name"].stringValue
                }
                else{
                    cell.lblItemNames.text = ""
                }
                cell.btnOpenRequest.setTitle("Open Request", for: .normal)
                //                cell.viewViews.isHidden = true
            }
            else{
                cell.lblItemNames.text = ""
                cell.lblItemNames.isHidden = true
                //                cell.viewViews.isHidden = false
                
                cell.delegate = self
                if let type = post["publish_type"].string, type.lowercased() == "albums"{
                    cell.btnOpenRequest.setTitle("Open Album", for: .normal)
                }
                else{
                    cell.btnOpenRequest.setTitle("Open Folder", for: .normal)
                }
            }
            
            /* cell.labelPost.handleHashtagTap { hashtag in
             print("Success. You just tapped the \(hashtag) hashtag")
             self.txtSearch.text  = "#"+hashtag
             self.openSearch()
             //                self.searchview.isHidden = false
             //                self.searchConstraintheight.constant = 36
             self.txtSearch.becomeFirstResponder()
             
             }*/
            cell.labelPost.handleMentionTap { (mension) in
                print("Success. You just tapped the \(mension) mension")
                
                
            }
            cell.labelPost.handleURLTap { (url) in
                print(url)
                if url.absoluteString.contains("familheey"){
                    self.openAppLink(url: url.absoluteString)
                }
                else{
                    if !(["http", "https"].contains(url.scheme?.lowercased())) {
                        var strUrl = url.absoluteString
                        strUrl = "http://"+strUrl
                        let Turl = URL(string: strUrl)
                        
                        let vc = SFSafariViewController(url: Turl!)
                        self.present(vc, animated: true, completion: nil)
                    }
                    else{
                        let vc = SFSafariViewController(url: url)
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            }
            
            return cell
            
        }
        else{
            if let sharedusers = post["shared_user_name"].string , !sharedusers.isEmpty{
                
                
                let cellid = "SharedPostTableViewCell"
                
                let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! SharedPostTableViewCell
                
                let post = ArrayPosts[indexPath.row]
                
                cell.readmoreButton.tag = indexPath.row
                cell.buttonShare.tag = indexPath.row
                cell.buttonSharedUsers.tag = indexPath.row
                cell.btnRightMrnu.tag = indexPath.row
                //            cell.commentsButton.tag = indexPath.row
                cell.buttonComments.tag = indexPath.row
                cell.viewsButtons.tag = indexPath.row
                cell.sharedUserButton.tag = indexPath.row
                
                
                //User Details
                
                if post["common_share_count"].intValue != 0{
                    
                    if post["common_share_count"].intValue == 1{
                        cell.labelUserName.text = "\(sharedusers) shared a post"
                    }
                    else if post["common_share_count"].intValue == 2{
                        cell.labelUserName.text = "\(sharedusers) and 1 other shared a post"
                    }
                    else {
                        cell.labelUserName.text = "\(sharedusers) and \(post["common_share_count"].intValue-1) others shared a post"
                    }
                }
                else{
                    cell.labelUserName.text = "\(sharedusers)"
                }
                if post["to_user_id"].stringValue == UserDefaults.standard.value(forKey: "userId") as! String{
                    cell.lblShared.text = "Shared with"
                    cell.lblShare_width.constant = 78.0
                    cell.labelPostedIn.text = " you"
                }
                else{
                    cell.lblShared.text = "Shared in"
                    cell.lblShare_width.constant = 65.0
                    cell.labelPostedIn.text = post["group_name"].string ?? ""
                }
                
                cell.labelUserNameInner.text = post["parent_post_created_user_name"].string ?? ""
                if post["parent_post_grp_name"].stringValue.count > 0 {
                    cell.labelPostedInInner.text = post["parent_post_grp_name"].string ?? ""
                }
                else{
                    cell.labelPostedInInner.text = post["privacy_type"].string ?? ""
                }
                
                
                //            cell.labelDate.text = post["createdAt"].string ?? ""
                cell.labelDate.text = HomeTabViewController.dateFormatterCommon(dateStr: post["createdAt"].stringValue)
                
                let proPic = post["parent_post_created_propic"].stringValue
                
                
                if proPic.count != 0{
                    let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic
                    let imgUrl = URL(string: temp)
                    cell.imageviewProfilePicInner.kf.indicatorType = .activity
                    cell.imageviewProfilePicInner.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                }
                else{
                    cell.imageviewProfilePicInner.image = #imageLiteral(resourceName: "Male Colored")
                }
                
                //Post
                cell.labelPost.text = post["snap_description"].string ?? ""
                
                //Details
                cell.lableNumberOfConversations.text = post["conversation_count"].stringValue
                cell.lblNumberOfShares.text = post["shared_user_count"].stringValue
                cell.lblNumberOfViews.text = post["views_count"].stringValue
                
                if let Attachment = post["post_attachment"].array, Attachment.count != 0{
                    
                    cell.updateSlider(data: Attachment)
                }
                else{
                    
                    print("No Slide........")
                    print(indexPath.row)
                    
                    if let urlArray = post["valid_urls"].array, urlArray.count != 0{
                        if urlArray.count == 1{
                            if let urlMetadata = post["url_metadata"].dictionary, urlMetadata.count != 0{
                                if let result = urlMetadata["urlMetadataResult"], result.count != 0{
                                    cell.viewPostBg.isHidden = false
                                    cell.viewPageControllBG.isHidden = true
                                    
                                    cell.updateSliderLink(data: [result])
                                }
                                else{
                                    print(indexPath.row)
                                    cell.viewPostBg.isHidden = true
                                    cell.viewPageControllBG.isHidden = true
                                }
                            }
                        }
                        else{
                            print(indexPath.row)
                            cell.viewPostBg.isHidden = true
                            cell.viewPageControllBG.isHidden = true
                        }
                    }
                    else{
                        print(indexPath.row)
                        cell.viewPostBg.isHidden = true
                        cell.viewPageControllBG.isHidden = true
                    }
                }
                /*       if cell.labelPost.calculateMaxLines() > maxNumberOfLines{
                 cell.labelPost.numberOfLines = maxNumberOfLines
                 cell.readMoreView.isHidden = false
                 
                 //            tableView.layoutIfNeeded()
                 }
                 else{
                 cell.labelPost.numberOfLines = 0
                 //                cell.readMoreView.isHidden = true
                 cell.readmoreButton.isHidden = true
                 cell.readMore_height.constant = 5
                 //            tableView.layoutIfNeeded()
                 } */
                
                cell.readmoreButton.isHidden = true
                cell.labelPost.numberOfLines = 0
                cell.readMore_height.constant = 5
                
                cell.delegate = self
                
                
                let user = UserDefaults.standard.value(forKey: "userId") as! String
                if  user  == post["created_by"].stringValue{
                    //                    cell.viewViews.isHidden = false
                    //                    cell.viewShared.isHidden = false
                    if post["conversation_enabled"].bool ?? false{
                        cell.ViewComments.isHidden = false
                    }
                    else{
                        cell.ViewComments.isHidden = true
                    }
                }
                else{
                    //                    cell.viewViews.isHidden = true
                    //                    cell.viewShared.isHidden = true
                    if post["conversation_enabled"].bool ?? false{
                        cell.ViewComments.isHidden = false
                    }
                    else{
                        cell.ViewComments.isHidden = true
                    }
                }
                if  user  == post["created_by"].stringValue{
                    cell.rightMenuVew.isHidden = false
                }
                else{
                    cell.rightMenuVew.isHidden = true
                }
                if post["conversation_count_new"].intValue > 0{
                    cell.lblNewConverstn.isHidden = false
                }
                else{
                    cell.lblNewConverstn.isHidden = true
                }
                
                if post["is_shareable"].bool ?? false {
                    
                    cell.shareView.isHidden = false
                    
                }
                else{
                    
                    cell.shareView.isHidden = true
                    
                }
                /* cell.labelPost.handleHashtagTap { hashtag in
                 print("Success. You just tapped the \(hashtag) hashtag")
                 self.txtSearch.text  = "#"+hashtag
                 self.openSearch()
                 //                self.searchview.isHidden = false
                 //                self.searchConstraintheight.constant = 36
                 self.txtSearch.becomeFirstResponder()
                 
                 }
                 cell.labelPost.handleMentionTap { (mension) in
                 print("Success. You just tapped the \(mension) mension")
                 self.txtSearch.text  = mension
                 //                self.searchview.isHidden = false
                 //                self.searchConstraintheight.constant = 36
                 self.openSearch()
                 self.txtSearch.becomeFirstResponder()
                 
                 }*/
                cell.labelPost.handleURLTap { (url) in
                    print(url)
                    if url.absoluteString.contains("familheey"){
                        self.openAppLink(url: url.absoluteString)
                    }
                    else{
                        if !(["http", "https"].contains(url.scheme?.lowercased())) {
                            var strUrl = url.absoluteString
                            strUrl = "http://"+strUrl
                            let Turl = URL(string: strUrl)
                            
                            let vc = SFSafariViewController(url: Turl!)
                            self.present(vc, animated: true, completion: nil)
                        }
                        else{
                            let vc = SFSafariViewController(url: url)
                            self.present(vc, animated: true, completion: nil)
                        }
                    }
                }
                
                return cell
            }
            else{
                let cellid = "PostImageTableViewCell"
                
                let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! PostImageTableViewCell
                
                
                cell.readmoreButton.tag = indexPath.row
                cell.buttonShare.tag = indexPath.row
                cell.buttonRightMenu.tag = indexPath.row
                cell.commentsButton.tag = indexPath.row
                cell.viewsButton.tag = indexPath.row
                cell.sharedUsersButton.tag = indexPath.row
                cell.buttonComments.tag = indexPath.row
                
                //User Details
                cell.labelUserName.text = post["created_user_name"].string ?? ""
                cell.labelPostedIn.text = post["group_name"].string ?? "Public"
                //            cell.labelDate.text = post["createdAt"].string ?? ""
                cell.labelDate.text = HomeTabViewController.dateFormatterCommon(dateStr: post["createdAt"].stringValue)
                
                let proPic = post["propic"].string ?? ""
                
                
                if proPic.count != 0{
                    let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic
                    let imgUrl = URL(string: temp)
                    cell.imageViewProfilePic.kf.indicatorType = .activity
                    cell.imageViewProfilePic.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                    
                    

                }
                else{
                    cell.imageViewProfilePic.image = #imageLiteral(resourceName: "Male Colored")
                }
                cell.imageViewProfilePic.isTappable(id: post["created_by"].intValue )
                cell.btnProfileImage.tag = post["created_by"].intValue
                //Post
                cell.labelPost.text = post["snap_description"].string ?? ""
                
                //Details
                cell.lableNumberOfConversations.text = post["conversation_count"].stringValue
                cell.lblNumberOfShares.text = post["shared_user_count"].stringValue
                cell.labelNumberOfViews.text = post["views_count"].stringValue
                
                if let Attachment = post["post_attachment"].array, Attachment.count != 0{
                    print("updating slide........")
                    print(indexPath.row)
                    cell.updateSlider(data: Attachment)
                }
                else{
                    print("No Slide........")
                    print(indexPath.row)
                    
                    if let urlArray = post["valid_urls"].array, urlArray.count != 0{
                        if urlArray.count == 1{
                            if let urlMetadata = post["url_metadata"].dictionary, urlMetadata.count != 0{
                                if let result = urlMetadata["urlMetadataResult"], result.count != 0{
                                    cell.viewPostBg.isHidden = false
                                    cell.viewPageControllBG.isHidden = true
                                    
                                    cell.updateSliderLink(data: [result])
                                }
                                else{
                                    print(indexPath.row)
                                    cell.viewPostBg.isHidden = true
                                    cell.viewPageControllBG.isHidden = true
                                }
                            }
                        }
                        else{
                            print(indexPath.row)
                            cell.viewPostBg.isHidden = true
                            cell.viewPageControllBG.isHidden = true
                        }
                    }
                    else{
                        print(indexPath.row)
                        cell.viewPostBg.isHidden = true
                        cell.viewPageControllBG.isHidden = true
                    }
                }
                /*   if cell.labelPost.calculateMaxLines() > maxNumberOfLines{
                 cell.labelPost.numberOfLines = maxNumberOfLines
                 cell.readMoreView.isHidden = false
                 
                 //            tableView.layoutIfNeeded()
                 }
                 else{
                 cell.labelPost.numberOfLines = 0
                 cell.readMoreView.isHidden = true
                 //            tableView.layoutIfNeeded()
                 } */
                cell.labelPost.numberOfLines = 0
                cell.readMoreView.isHidden = true
                cell.delegate = self
                
                
                // String(describing: UserDefaults.standard.value(forKey: "userId"))
                let user = UserDefaults.standard.value(forKey: "userId") as! String
                if  user  == post["created_by"].stringValue{
                    
                    cell.viewViews.isHidden = false
                    cell.viewShared.isHidden = false
                    if post["conversation_enabled"].bool ?? false{
                        cell.viewComments.isHidden = false
                    }
                    else{
                        cell.viewComments.isHidden = true
                    }
                }
                else{
                    
                    cell.viewViews.isHidden = true
                    cell.viewShared.isHidden = true
                    if post["conversation_enabled"].bool ?? false{
                        cell.viewComments.isHidden = false
                    }
                    else{
                        cell.viewComments.isHidden = true
                    }
                }
                
                if post["conversation_count_new"].intValue > 0{
                    cell.lblNewConvrstn.isHidden = false
                }
                else{
                    cell.lblNewConvrstn.isHidden = true
                }
                
                
                if post["is_shareable"].bool ?? false {
                    
                    cell.shareView.isHidden = false
                    
                }
                else{
                    
                    cell.shareView.isHidden = true
                    
                }
                /* cell.labelPost.handleHashtagTap { hashtag in
                 print("Success. You just tapped the \(hashtag) hashtag")
                 self.txtSearch.text  = "#"+hashtag
                 self.openSearch()
                 //                self.searchview.isHidden = false
                 //                self.searchConstraintheight.constant = 36
                 self.txtSearch.becomeFirstResponder()
                 
                 }
                 cell.labelPost.handleMentionTap { (mension) in
                 print("Success. You just tapped the \(mension) mension")
                 self.txtSearch.text  = mension
                 //                self.searchview.isHidden = false
                 //                self.searchConstraintheight.constant = 36
                 self.openSearch()
                 self.txtSearch.becomeFirstResponder()
                 
                 }*/
                cell.labelPost.handleURLTap { (url) in
                    print(url)
                    if url.absoluteString.contains("familheey"){
                        self.openAppLink(url: url.absoluteString)
                    }
                    else{
                        if !(["http", "https"].contains(url.scheme?.lowercased())) {
                            var strUrl = url.absoluteString
                            strUrl = "http://"+strUrl
                            let Turl = URL(string: strUrl)
                            
                            let vc = SFSafariViewController(url: Turl!)
                            self.present(vc, animated: true, completion: nil)
                        }
                        else{
                            let vc = SFSafariViewController(url: url)
                            self.present(vc, animated: true, completion: nil)
                        }
                    }
                }
                return cell
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func getnumberOfLines(label:UILabel) -> Int{
        var   lineCount:NSInteger = 0
        let  textSize:CGSize = CGSize.init(width: label.frame.size.width, height: CGFloat(MAXFLOAT))
        let   rHeight:Int = lroundf(Float(label.sizeThatFits(textSize).height))
        let   charSize:Int = lroundf(Float(label.font!.lineHeight))
        lineCount = rHeight/charSize
        NSLog("No of lines: %i",lineCount)
        return lineCount
    }
    
    @IBAction func btnProfile_OnClick(_ sender: UIButton) {
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "UserProfileTableViewController") as! UserProfileTableViewController
        vc.userID = "\(sender.tag)"
        
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
}
extension PostDetailViewController : PostImageTableViewCellDelegate{
    func gotoPreview(data: [JSON],index:Int) {
        
        if let type = data[index]["type"].string ,( type.contains("text") || type.contains("pdf") || type.contains("doc") || type.contains("docx") || type.contains("xls") || type.contains("xlsx") || type.contains("msword") || type.contains("txt")) {
            var temp:String!
            temp = "\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+data[index]["filename"].stringValue
            ActivityIndicatorView.show("Loading....")
            let url = URL(string: temp)!
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else { return }
                let tmpURL = FileManager.default.temporaryDirectory
                    .appendingPathComponent(response?.suggestedFilename ?? "fileName.png")
                do {
                    try data.write(to: tmpURL)
                } catch {
                    print(error)
                    ActivityIndicatorView.hiding()
                }
                DispatchQueue.main.async {
                    /// STOP YOUR ACTIVITY INDICATOR HERE
                    ActivityIndicatorView.hiding()
                    print(tmpURL)
                    self.share(url: tmpURL)
                }
            }.resume()
            
            /*let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
             
             let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementDocPreviewViewController") as! AnnouncementDocPreviewViewController
             vc.UrlToLoad = temp
             self.present(vc, animated: true, completion: nil)*/
        }
        else if let type = data[index]["url"].string {
            if type.isEmpty{
                
            }
            else{
                print(type.contains("familheey"))
                if type.contains("familheey"){
                    self.openAppLink(url: type)
                    //                    Helpers.openAppLinkCommon(url: type, currentVC: self)
                }
                else{
                    //                    UIApplication.shared.open(url)
                    let url = URL(string: type)!
                    let vc = SFSafariViewController(url: url)
                    self.present(vc, animated: true, completion: nil)
                }
            }
            
        }
        else{
            let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsPreviewViewController") as! AnnouncementsPreviewViewController
            vc.attachments = data
            vc.isFromPost = true
            vc.selectedIndex = index
            self.navigationController?.addFadeAnimation()
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    func gotoSinglePreview(data: String) {
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: "ChangePicturesViewController") as! ChangePicturesViewController
        vc.isCoverOrProfilePic = "CoverPic"
        vc.ids           = UserDefaults.standard.value(forKey: "userId") as! String
        vc.isFrom = "post"
        vc.inFor = "cover"
        vc.isAdmin = ""
        vc.imageUrl = data
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}
extension PostDetailViewController : SharedPostTableViewCellDelegate{
    
    func gotoSharedPreview(data: [JSON], index: Int)
    {
        
        if let type = data[index]["type"].string ,( type.contains("text") || type.contains("pdf") || type.contains("doc") || type.contains("docx") || type.contains("xls") || type.contains("xlsx") || type.contains("msword") || type.contains("txt")) {
            var temp:String!
            temp = "\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+data[index]["filename"].stringValue
            let url = URL(string: temp)!
            ActivityIndicatorView.show("Loading....")
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else { return }
                let tmpURL = FileManager.default.temporaryDirectory
                    .appendingPathComponent(response?.suggestedFilename ?? "fileName.png")
                do {
                    try data.write(to: tmpURL)
                } catch {
                    print(error)
                    ActivityIndicatorView.hiding()
                }
                DispatchQueue.main.async {
                    /// STOP YOUR ACTIVITY INDICATOR HERE
                    ActivityIndicatorView.hiding()
                    print(tmpURL)
                    self.share(url: tmpURL)
                }
            }.resume()
            
            /*let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
             
             let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementDocPreviewViewController") as! AnnouncementDocPreviewViewController
             vc.UrlToLoad = temp
             self.present(vc, animated: true, completion: nil)*/
        }
        else if let type = data[index]["url"].string {
            if type.isEmpty{
                
            }
            else{
                print(type.contains("familheey"))
                if type.contains("familheey"){
                    self.openAppLink(url: type)
                    //                    Helpers.openAppLinkCommon(url: type, currentVC: self)
                }
                else{
                    //                    UIApplication.shared.open(url)
                    let url = URL(string: type)!
                    let vc = SFSafariViewController(url: url)
                    self.present(vc, animated: true, completion: nil)
                }
            }
            
        }
        else{
            let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsPreviewViewController") as! AnnouncementsPreviewViewController
            vc.attachments = data
            vc.isFromPost = true
            vc.selectedIndex = index
            self.navigationController?.addFadeAnimation()
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    func gotoSharedSinglePreview(data: String)
    {
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: "ChangePicturesViewController") as! ChangePicturesViewController
        vc.isCoverOrProfilePic = "CoverPic"
        vc.ids           = UserDefaults.standard.value(forKey: "userId") as! String
        vc.isFrom = "post"
        vc.inFor = "cover"
        vc.isAdmin = ""
        vc.imageUrl = data
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
}
extension PostDetailViewController : PostThanksableViewCellDelegate,UIDocumentInteractionControllerDelegate{
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    func share(url: URL) {
        print(url)
        documentInteractionController = UIDocumentInteractionController(url: url)
        //        documentInteractionController.uti = url.typeIdentifier ?? "public.data, public.content"
        //        documentInteractionController.name = url.localizedName ?? url.lastPathComponent
        documentInteractionController.delegate = self
        documentInteractionController.presentPreview(animated: true)
    }
    
    func gotoThanksPreview(data: [JSON], index: Int) {
        
        if let type = data[index]["type"].string ,( type.contains("text") || type.contains("pdf") || type.contains("doc") || type.contains("docx") || type.contains("xls") || type.contains("xlsx") || type.contains("msword") || type.contains("txt")) {
            var temp:String!
            temp = "\(Helpers.imageURl)"+"\(BaseUrl.document_image)"+data[index]["filename"].stringValue
            let url = URL(string: temp)!
            ActivityIndicatorView.show("Loading....")
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else { return }
                let tmpURL = FileManager.default.temporaryDirectory
                    .appendingPathComponent(response?.suggestedFilename ?? "fileName.png")
                do {
                    try data.write(to: tmpURL)
                } catch {
                    print(error)
                    ActivityIndicatorView.hiding()
                }
                DispatchQueue.main.async {
                    /// STOP YOUR ACTIVITY INDICATOR HERE
                    ActivityIndicatorView.hiding()
                    print(tmpURL)
                    self.share(url: tmpURL)
                }
            }.resume()
        }
        else if let type = data[index]["url"].string {
            if type.isEmpty{
                
            }
            else{
                print(type.contains("familheey"))
                if type.contains("familheey"){
                    self.openAppLink(url: type)
                    //                    Helpers.openAppLinkCommon(url: type, currentVC: self)
                }
                else{
                    //                    UIApplication.shared.open(url)
                    let url = URL(string: type)!
                    let vc = SFSafariViewController(url: url)
                    self.present(vc, animated: true, completion: nil)
                }
            }
            
        }
        else{
            let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsPreviewViewController") as! AnnouncementsPreviewViewController
            vc.attachments = data
            vc.isFromPost = true
            vc.typeAlbum = true
            vc.selectedIndex = index
            self.navigationController?.addFadeAnimation()
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
    }
    
    func gotoThanksSinglePreview(data: String) {
        /*let storyboard = UIStoryboard.init(name: "second", bundle: nil)
         
         let vc = storyboard.instantiateViewController(withIdentifier: "ChangePicturesViewController") as! ChangePicturesViewController
         vc.isCoverOrProfilePic = "CoverPic"
         vc.ids           = UserDefaults.standard.value(forKey: "userId") as! String
         vc.isFrom = "post"
         vc.inFor = "cover"
         vc.isAdmin = ""
         vc.imageUrl = data
         self.imgClicked = true
         self.navigationController?.pushViewController(vc, animated: true)*/
        
    }
     func selectMentionedName(data: [JSON], index: Int) {
          
          let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                 let vc = storyboard.instantiateViewController(withIdentifier: "UserProfileTableViewController") as! UserProfileTableViewController
          let userId = data[index]["user_id"].stringValue
                 vc.userID = userId
                 self.navigationController?.pushViewController(vc, animated: true)
          
      }
      @IBAction func btnMentinedNameClicked(_ sender: UIButton)
          {
              let post = ArrayPosts[sender.tag]
          
              
              let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                          let vc = storyboard.instantiateViewController(withIdentifier: "UserProfileTableViewController") as! UserProfileTableViewController
                   let userId = post["publish_mention_users"][0]["user_id"].stringValue
                          vc.userID = userId
                          self.navigationController?.pushViewController(vc, animated: true)
          }
    
}
