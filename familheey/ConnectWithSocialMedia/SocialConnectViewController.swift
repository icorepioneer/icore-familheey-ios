//
//  SocialConnectViewController.swift
//  familheey
//
//  Created by familheey on 04/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import FacebookLogin
import FBSDKLoginKit
import GoogleSignIn
import LinkedinSwift

class SocialConnectViewController: BaseClassViewController {
    @IBOutlet weak var btnGoogle: UIButton!
    @IBOutlet weak var btnFacebook: UIButton!
    @IBOutlet weak var btnLinkedin: UIButton!
    
    
   let linkedinHelper = LinkedinSwiftHelper(configuration: LinkedinSwiftConfiguration(clientId: "81hujmio4xeb6y", clientSecret: "ELTAbHgW5TNa3V4P", state: "DLKDJF45DIWOERCM", permissions: ["r_liteprofile", "r_basicprofile", "r_emailaddress"], redirectUrl: "https://www.familheey.com/signin"))
    
    override func onScreenLoad() {
        btnGoogle.layer.cornerRadius = 22
        btnFacebook.layer.cornerRadius = 22
        btnLinkedin.layer.cornerRadius = 22
        
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self
    }
    
    //MARK:- Button Actions
    
    @IBAction func onClickGoogle(_ sender: Any) {
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    @IBAction func onClickFacebook(_ sender: Any) {
        let loginManager = LoginManager()
        loginManager.logIn(permissions: [.email,.publicProfile], viewController: self) { (LoginResult) in
            switch LoginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                print("Logged in!")
                print(grantedPermissions)
                if grantedPermissions.contains("email"){
                    self.getUserDetails()
                }
            }
        }
    }
    
    @IBAction func onClickLinkedin(_ sender: Any) {
        linkedinHelper.authorizeSuccess({ (lsToken) in
            print(lsToken)
            if lsToken.accessToken.count == 0{
                print("empty")
            }
            else{
                self.getUserDetailsFromLinkedin(token: lsToken)
            }
        }, error: { (error) in
            print(error.localizedDescription)
        }) {
            print("user cancelled")
        }
    }
    
    @IBAction func onClickSkip(_ sender: Any) {
        navigateToNextView(type: "normal", name: "", email: "")
    }
    
    //MARK:- Get Data From FB
    func getUserDetails(){
        ActivityIndicatorView.show("Loading.....")
        if((AccessToken.current) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    ActivityIndicatorView.hiding()
                    var resultDic = NSDictionary()
                    resultDic = result as! NSDictionary
                    print(resultDic)
                    self.navigateToNextView(type: "facebook", name: resultDic.value(forKey: "name") as! String, email: resultDic.value(forKey: "email") as! String)
                }
            })
        }
        else{
            ActivityIndicatorView.hiding()
        }
    }
    
    //MARK:- Get Data From LinkedIn
    func getUserDetailsFromLinkedin(token:LSLinkedinToken){
        ActivityIndicatorView.show("Loading.....")
        var fullname:String = String()
        let url = "https://api.linkedin.com/v2/me?oauth2_access_token=\(String(describing: token.accessToken!))"
        
        linkedinHelper.requestURL(url, requestType: LinkedinSwiftRequestGet, success: { (response) -> Void in
            print(response.jsonObject!)
            
            let fName = response.jsonObject["localizedFirstName"]! as! String
            let lName = response.jsonObject["localizedLastName"]! as! String
            
             fullname = "\(fName) \(lName)"
            print(fullname)
            self.getEmailFromLinkedin(name: fullname)
            
        }) {(error) -> Void in
            print(error.localizedDescription)
            ActivityIndicatorView.hiding()
            //handle the error
        }
    }
    
    func getEmailFromLinkedin(name:String){
        let emailUrl = "https:/api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))"
        linkedinHelper.requestURL(emailUrl, requestType: LinkedinSwiftRequestGet, success: { (response) in
            print(response.jsonObject!)
            let tempArr = response.jsonObject["elements"] as! NSArray
            let temDic = tempArr[0] as! NSDictionary
            print(temDic.value(forKeyPath: "handle~.emailAddress")!)
            let email = temDic.value(forKeyPath: "handle~.emailAddress")!
            ActivityIndicatorView.hiding()
            self.navigateToNextView(type: "linkedin", name: name, email: email as! String)
        }) { (error) in
            print(error.localizedDescription)
            ActivityIndicatorView.hiding()
        }
    }
    
    //MARK:- Navigation Custom Method
    
    func navigateToNextView(type:String, name:String, email:String){
        let loginView = self.storyboard?.instantiateViewController(withIdentifier: "profileEditOne") as! ProfileEditOneViewController
        loginView.name  = name
        loginView.email = email
        loginView.type  = type
        self.navigationController?.pushViewController(loginView, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SocialConnectViewController: GIDSignInDelegate{
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        ActivityIndicatorView.show("Loading.....")
        if let error = error {
            ActivityIndicatorView.hiding()
            print("\(error.localizedDescription)")
        } else {
            // Perform any operations on signed in user here.
            
            //            let userId = user.userID                  // For client-side use only!
            //            let idToken = user.authentication.idToken // Safe to send to the server
            //let givenName = user.profile.givenName
            // let familyName = user.profile.familyName
            ActivityIndicatorView.hiding()
            let fullName = user.profile.name
            let email = user.profile.email
            print("name\(String(describing: fullName)) -- email\(String(describing: email))")
            
            navigateToNextView(type: "google", name: fullName!, email: email!)
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
        ActivityIndicatorView.hiding()
    }
}
