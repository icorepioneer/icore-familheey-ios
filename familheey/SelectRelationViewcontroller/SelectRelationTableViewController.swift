//
//  SelectRelationTableViewController.swift
//  familheey
//
//  Created by Giri on 25/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

protocol SelectRelationDelagate:class {
    func relationSelected(selectRelation:Relations)
    func fromNewRelation()
}

class SelectRelationTableViewController: UITableViewController {
    
    var selectedIndexs = [Int]()
    weak var delegate : SelectRelationDelagate?
    var allRelations : [Relations]?
    var newReltn = [String]()
    var SelectedUserId = ""
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var currentRelationShip = ""
    var isUpdate = false
    var tableId = ""
    var selectedIndex = 0
    var cate = ""
    var groupId = ""
    var typeOfRelation = ""
    var search:String=""
    var isFromNew = false
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearchReset: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.navigationController?.navigationBar.isHidden = true
        txtSearch.delegate = self
        addRightNavigationButton()
        Helpers.setleftView(textfield: txtSearch, customWidth: 30)

        tableView.tableFooterView = UIView.init()
        getAllRelations() 
//        if appDelegate.AllRelations.isEmpty{
//            getAllRelations()
//        }
//        else{
//            allRelations = appDelegate.AllRelations
//            tableView.reloadData()
//        }
        
        if cate.lowercased() == "regular"{
            self.typeOfRelation = "relation"
            self.navigationController?.navigationBar.topItem?.title = "Select Relation"
        }
        else{
            self.typeOfRelation = "role"
            self.navigationController?.navigationBar.topItem?.title = "Select Role"
        }
    }
    
    //MARK:- Right navigation button
    
    func addRightNavigationButton(){
        
        let rightButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(onClickrightButtonAction))
        self.navigationItem.rightBarButtonItem = rightButtonItem
        
    }
    
    //MARK:- Button Actions
    @objc func onClickrightButtonAction(sender: UIBarButtonItem){
        if isFromNew{
            self.delegate?.fromNewRelation()
            self.dismiss(animated: true, completion: nil)
        }
        else{
            guard let relation = allRelations?[selectedIndex] else { return }
            self.delegate?.relationSelected(selectRelation: relation)
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    @IBAction func onClickBack(_ sender: Any) {
         self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickSearchReset(_ sender: Any) {
        txtSearch.text = ""
        getAllRelations()
        btnSearchReset.isHidden = true
        self.txtSearch.endEditing(true)
    }
    
    @IBAction func onClickAdd(_ sender: Any) {
        guard let relation = allRelations?[selectedIndex] else { return }
        self.delegate?.relationSelected(selectRelation: relation)
        self.dismiss(animated: true, completion: nil)
    }
    //MARK:- Custom Actions
    
    func getAllRelations(){
        APIServiceManager.callServer.getAllRelations(url: EndPoint.getallRelations, type: cate, query: txtSearch.text!, success: { (response) in
            ActivityIndicatorView.hiding()
            if let results = response as! RelationsModel?{
                self.allRelations = results.result!
                self.appDelegate.AllRelations = self.allRelations!
            }
            if self.allRelations!.count > 0{
                self.isFromNew = false
                self.tableView.reloadData()
            }
            else{
                if  self.cate.lowercased() == "regular"{
                    self.isFromNew = true
                    self.tableView.reloadData()
                }
                else{
                    if self.txtSearch.text!.count > 0 {
                        self.newReltn.append(self.txtSearch.text!)
                        self.isFromNew = true
                         self.tableView.reloadData()
                    }
                }
            }
            

        }) { (error) in
            ActivityIndicatorView.hiding()
            self.tableView.reloadData()

        }
    }
    func addRelation(relationId:String){
        var params = [String:String]()
        if isFromNew{
            params = [
                "primary_user":UserDefaults.standard.value(forKey: "userId") as! String,
                "secondary_user":SelectedUserId,
                "relation_id":"others",
                "type":self.typeOfRelation,
                "group_id":self.groupId,
                "value":self.txtSearch.text!
            ]
        }
        else{
            params = [
                "primary_user":UserDefaults.standard.value(forKey: "userId") as! String,
                "secondary_user":SelectedUserId,
                "relation_id":relationId,
                "type":self.typeOfRelation,
                "group_id":self.groupId
            ]
        }
         
        
//        APIServiceManager.callServer.addRelationShip(url: EndPoint.addRelation, primaryUser: UserDefaults.standard.value(forKey: "userId") as! String, secondaryUser: SelectedUserId, relationId: relationId, groupId: self.groupId, type: self.typeOfRelation, success:
        APIServiceManager.callServer.addRelationShip(url: EndPoint.addRelation, params: params,  success:{ (response) in
            ActivityIndicatorView.hiding()
            if let results = response as! RelationUpdateResponseModel?{
                if let tId = results.result?.tableId{
                    self.tableId = "\(tId)"
                    self.isUpdate = true
                }
                if results.statusCode == 200{
                    if self.cate.lowercased() == "regular"{
                        self.displayAlert(alertStr: "Relation added successfully", title: "")
                    }
                    else{
                        self.displayAlert(alertStr: "Role added successfully", title: "")
                    }
                }
            }
        }) { (error) in
            ActivityIndicatorView.hiding()
            
        }
        
    }
    func updateRelation(relationId:String,tableId:String){
        
        var params = [String:String]()
               if isFromNew{
                   params = [
                       "primary_user":UserDefaults.standard.value(forKey: "userId") as! String,
                       "secondary_user":SelectedUserId,
                       "relation_id":"others",
                       "type":self.typeOfRelation,
                       "group_id":self.groupId,
                       "value":self.txtSearch.text!,
                       "id": tableId
                   ]
               }
               else{
                   params = [
                       "primary_user":UserDefaults.standard.value(forKey: "userId") as! String,
                       "secondary_user":SelectedUserId,
                       "relation_id":relationId,
                       "type":self.typeOfRelation,
                       "group_id":self.groupId,
                       "id": tableId
                   ]
               }
        
//        APIServiceManager.callServer.updateRelationShip(url: EndPoint.updateRelation, primaryUser: UserDefaults.standard.value(forKey: "userId") as! String, secondaryUser: SelectedUserId, relationId: relationId, tableId: tableId, groupId: self.groupId, type: self.typeOfRelation, success:
        
        APIServiceManager.callServer.updateRelationShip(url: EndPoint.updateRelation, params: params, success: { (response) in
            ActivityIndicatorView.hiding()
            if let results = response as! RelationUpdateResponseModel?{
                if let tId = results.result?.tableId{
                    self.tableId = "\(tId)"
                    self.isUpdate = true
                }
                if results.statusCode == 200{
                    if self.cate.lowercased() == "regular"{
                        self.displayAlert(alertStr: "Relation updated successfully", title: "")
                    }
                    else{
                        self.displayAlert(alertStr: "Role updated successfully", title: "")
                    }
                    
                }
            }
          
        }) { (error) in
            ActivityIndicatorView.hiding()
        }
    }
    
    //MARK:- Tableview Delegates
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       if isFromNew{
        return self.newReltn.count
        }
       else{
            return allRelations?.count ?? 0
        }
        
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: "Cell")
        if isFromNew{
            cell.textLabel?.text = self.newReltn[indexPath.row]
        }
        else{
            let relations = allRelations?[indexPath.row]
            cell.textLabel?.text = relations?.relationship
        }
       
        if currentRelationShip.isEmpty{
            if selectedIndexs.contains(indexPath.row){
                cell.accessoryType = UITableViewCell.AccessoryType.checkmark
            }
            else{
                cell.accessoryType = UITableViewCell.AccessoryType.none
            }
        }
        else{
            if currentRelationShip.lowercased() == cell.textLabel?.text?.lowercased(){
                cell.accessoryType = UITableViewCell.AccessoryType.checkmark
            }
            else{
                cell.accessoryType = UITableViewCell.AccessoryType.none
            }
        }
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //        if let index = selectedIndexs.firstIndex(of:indexPath.row) {
        //            selectedIndexs.remove(at: index)
        //        }
        //        else{
        //            selectedIndexs.append(indexPath.row)
        //        }
        
        self.selectedIndex = indexPath.row
        if isFromNew{
            currentRelationShip = ""
            selectedIndexs.removeAll()
            selectedIndexs.append(indexPath.row)
            tableView.reloadData()
            
            if isUpdate && !tableId.isEmpty{
                updateRelation(relationId: "others", tableId: tableId)

            }
            else{
                addRelation(relationId: "others")
            }
        }
        else{
            if let relations = allRelations?[indexPath.row]{
                currentRelationShip = ""
                selectedIndexs.removeAll()
                selectedIndexs.append(indexPath.row)
                tableView.reloadData()
                print("Selected Indexes:",selectedIndexs)
                if isUpdate && !tableId.isEmpty{
                    print(relations.id)
                    updateRelation(relationId: "\(relations.id)", tableId: tableId)
                }
                else{
                    addRelation(relationId: "\(relations.id)")
//                     updateRelation(relationId: "others", tableId: tableId)
                }
            }
        }
    }
}
extension SelectRelationTableViewController : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
           textField.resignFirstResponder()
           if textField == txtSearch
           {
               // selectWebAPI()
               if textField.text!.count > 0{
                   btnSearchReset.isHidden = false
               }
               else{
                   btnSearchReset.isHidden = true
               }
               textField.endEditing(true)
           }
           
           return true
       }
       func textFieldDidEndEditing(_ textField: UITextField){
           self.view.endEditing(true)
           if textField == txtSearch
           {
               if textField.text!.count > 0{
                   btnSearchReset.isHidden = false
               }
               else{
                   btnSearchReset.isHidden = true
               }
               getAllRelations()
           }
       }
       func textFieldDidBeginEditing(_ textField: UITextField) {
           if textField.text!.count > 0 {
               btnSearchReset.isHidden = false
           }
           else{
               btnSearchReset.isHidden = true
               // selectWebAPI()
           }
       }
       func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
           if textField.text!.count > 0 {
               btnSearchReset.isHidden = false
           }
           else{
               btnSearchReset.isHidden = true
           }
        if string.isEmpty
        {
            search = String(search.dropLast())
        }
        else
        {
            search=textField.text!+string
        }
        print("#### \(search)")
//        let namePredicate = NSPredicate(format: "relationship contains[c] %@", search)
//        let cArr = allRelations! as NSArray
//        let filteredArray = cArr.filter { namePredicate.evaluate(with: $0) }
//        print(filteredArray)
   
           return true
       }
}
