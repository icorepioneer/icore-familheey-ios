//
//  AddTabViewController.swift
//  familheey
//
//  Created by Giri on 20/11/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class AddTabViewController: UIViewController {
   

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
          
          self.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK:- Button Actions
    
    @IBAction func AddAction(_ sender: Any) {
        
        let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CreatePostViewController") as! CreatePostViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func createEvent(_ sender: Any) {
       let storyboard = UIStoryboard.init(name: "third", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CreateEventStep1ViewController") as! CreateEventStep1ViewController
               self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func createAnnouncement(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
              let vc = storyboard.instantiateViewController(withIdentifier: "CreateAnnouncmentViewController") as! CreateAnnouncmentViewController
              self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
