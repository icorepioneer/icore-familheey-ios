//
//  EditRequestViewController.swift
//  familheey
//
//  Created by familheey on 31/03/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON
import GooglePlaces

class EditRequestViewController: UIViewController,EditYourNeedDelegate,SelctedFamiliesForNeedDelegate {
    
    @IBOutlet weak var txtPostTo: UITextField!
    @IBOutlet weak var lblHead: UILabel!
    @IBOutlet weak var txtStartDate: UITextField!
    @IBOutlet weak var txtEndDate: UITextField!
    @IBOutlet weak var lblLocation: UITextField!
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var postToView: UIView!
    @IBOutlet weak var imgDown: UIImageView!
    @IBOutlet weak var lblFamilyCount: UILabel!
    @IBOutlet weak var btnAddMore: UIButton!
    @IBOutlet weak var viewAddMore: UIView!
    @IBOutlet weak var imgGeneral: UIImageView!
    @IBOutlet weak var imgFund: UIImageView!
    @IBOutlet weak var typeVew1: UIView!
    @IBOutlet weak var typeVew2: UIView!
    @IBOutlet weak var btnPostToSel: UIButton!
    
    @IBOutlet weak var genrlTypeView: UIView!
    @IBOutlet weak var fundTypeView: UIView!
    
    
    var theme: SambagTheme = .light
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    
    var arrayOfRequestListItems = [JSON] ()
    var arrayOfItems = NSMutableArray()
    var selectedIds = NSMutableArray()
    var requestDic = JSON()
    var isItemEdit = false
    var selectedItem = 0
    var requestType = ""
    var dateTag = Int()
    var timetag = Int()
    var selectedStartDate:String = ""
    var selectedEndDate:String = ""
    var selectedStartDateFormatted:String = ""
    var selectedEndDateFormatted:String = ""
    var startDateTimestamp:Int! = 0
    var endDateTimestamp:Int! = 0
    var post_requestId = ""
    var selectedLat : Double!
    var selectedLong :Double!
    var fundType = "general"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtPostTo.isEnabled = false
        self.lblHead.attributedText = Helpers.setAttributedPlaceHolder(placeHolderName: "Post this request to")
        if requestDic.count != 0{
            self.getRequestDetails(id: requestDic["post_request_id"].stringValue)
           
//            if let items = requestDic["items"].array{
//                self.arrayOfRequestListItems = items
//
//            }
            
            print(requestDic)
            
            if requestDic["request_type"].stringValue.lowercased() == "general"{
                fundType = "general"
                typeVew1.isHidden = false
                btnPostToSel.isEnabled = true
                fundTypeView.isHidden = true
                genrlTypeView.isHidden = false
                txtPostTo.isEnabled = true
                imgDown.isHidden = false
                
                imgGeneral.image =  #imageLiteral(resourceName: "Green_tick.png")
                imgFund.image = #imageLiteral(resourceName: "gray_circle.png")
            }
            else{
                fundType = "fund"
                typeVew1.isHidden = true
                btnPostToSel.isEnabled = false
                genrlTypeView.isHidden = true
                fundTypeView.isHidden = false
                txtPostTo.isEnabled = false
                imgDown.isHidden = true
                
                imgFund.image =  #imageLiteral(resourceName: "Green_tick.png")
                imgGeneral.image = #imageLiteral(resourceName: "gray_circle.png")
                
            }
            
            if requestDic["group_type"].stringValue.lowercased() == "selected_family"{
                txtPostTo.text = "Selected families"
                requestType = requestDic["group_type"].stringValue
            }
            else{
                txtPostTo.text = "All families"
                requestType = requestDic["group_type"].stringValue
            }
            
            lblLocation.text = requestDic["request_location"].stringValue
            post_requestId = requestDic["post_request_id"].stringValue
            
            txtStartDate.text = "\(EventsTabViewController.convertTimeStampToDate(timestamp: requestDic["start_date"].stringValue))"
            selectedStartDate = "\(convertTimeStamp(timestamp: requestDic["start_date"].stringValue))"
            print(selectedStartDate)
            
            if requestDic["end_date"].stringValue != "0"
            {
                txtEndDate.text = "\(EventsTabViewController.convertTimeStampToDate(timestamp: requestDic["end_date"].stringValue ))"
                selectedEndDate = "\(convertTimeStamp(timestamp: requestDic["end_date"].stringValue ))"
                print(selectedEndDate)
            }
            else
            {
                txtEndDate.text = ""
                selectedEndDate = ""
            }
            
            
            let arr = requestDic["to_groups"].arrayValue
            for index in arr{
                print(index)
                self.selectedIds.add(index["id"].stringValue)
            }
            print(selectedIds)
            if selectedIds.count == 0{
                lblFamilyCount.isHidden = true
            }
            else{
                lblFamilyCount.isHidden = false
                if selectedIds.count == 1{
                    lblFamilyCount.text = "\(selectedIds.count) family is selected"
                }
                else{
                    lblFamilyCount.text = "\(selectedIds.count) families are seleted"
                }
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
//        if !UIDevice.current.systemVersion.contains("13"){
//            
//            lblLocation.text = appDel.selectedEventVenue
//        }
        
        guard #available(iOS 13.0, *) else {
            lblLocation.text = appDel.selectedEventVenue
            return
        }
    }
    
    //MARK: Button Actions
    @IBAction func onClickBack(_ sender: Any) {
        if arrayOfRequestListItems.count == 0{
            self.displayAlert(alertStr: "Please add atleast one item in your request", title: "")
            return
        }
        else{
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    @IBAction func onClickPublish(_ sender: Any) {
        if requestType.isEmpty{
            self.displayAlert(alertStr: "Please select a family", title: "")
            return
        }
        else if arrayOfRequestListItems.count == 0{
            self.displayAlert(alertStr: "Please add atleast one item in your request", title: "")
            return
        }
        else if lblLocation.text!.isEmpty{
            self.displayAlert(alertStr: "Please select your location", title: "")
            return
        }
        else{
            publishYourRequestAPI()
        }
    }
    @IBAction func onClickPostTo(_ sender: Any) {
        if self.postToView.isHidden{
            postToView.isHidden = false
            self.imgDown.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        }
        else{
            postToView.isHidden = true
            self.imgDown.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi * 2))
        }
    }
    @IBAction func onClickStartDate(_ sender: Any) {
        self.dateTag = 0
        let vc = SambagDatePickerViewController()
        vc.theme = theme
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    @IBAction func onClickEndDate(_ sender: Any) {
        self.dateTag = 1
        let vc = SambagDatePickerViewController()
        vc.theme = theme
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    @IBAction func onClickLocation(_ sender: Any) {
        guard #available(iOS 13.0, *) else {
            // Code for earlier iOS versions
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let SearchPlacesViewController = storyboard.instantiateViewController(withIdentifier: "SearchPlacesViewController") as! SearchPlacesViewController
            SearchPlacesViewController.selectedField = "eventCreation"
            self.present(SearchPlacesViewController, animated: true, completion: nil)
            
            return
        }
        
        let acController = GMSAutocompleteViewController()
        
        acController.delegate = self
        present(acController, animated: true, completion: nil)
        /*if UIDevice.current.systemVersion.contains("13"){
         
         let acController = GMSAutocompleteViewController()
         
         acController.delegate = self
         present(acController, animated: true, completion: nil)
         }else{
         
         let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
         let SearchPlacesViewController = storyboard.instantiateViewController(withIdentifier: "SearchPlacesViewController") as! SearchPlacesViewController
         SearchPlacesViewController.selectedField = "eventCreation"
         self.present(SearchPlacesViewController, animated: true, completion: nil)
         }*/
    }
    @IBAction func onClickSelectPostTo(_ sender: UIButton) {
        if sender.tag == 1{
            self.txtPostTo.text = "All families"
            self.requestType = "all_family"
            selectedIds.removeAllObjects()
            lblFamilyCount.isHidden = true
            self.imgDown.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi * 2))
            self.postToView.isHidden = true
        }
        else{
            self.txtPostTo.text = "Selected families"
            self.requestType = "selected_family"
            
            let stryboard = UIStoryboard.init(name: "PostRequest", bundle: nil)
            let vc = stryboard.instantiateViewController(withIdentifier: "SelectFamiliesforNeedsViewController") as! SelectFamiliesforNeedsViewController
            vc.selectionDelegate = self
            vc.selectedUserIDArr = selectedIds
            self.navigationController?.pushViewController(vc, animated: true)
            self.imgDown.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi * 2))
            self.postToView.isHidden = true
        }
    }
    
    @IBAction func onClickAddItem(_ sender: Any) {
        let stryboard = UIStoryboard.init(name: "PostRequest", bundle: nil)
        let vc = stryboard.instantiateViewController(withIdentifier: "EditAddItemsViewController") as! EditAddItemsViewController
        vc.modalPresentationStyle = .fullScreen
        vc.delegate = self
        vc.post_requestId = post_requestId
        let navigationController = UINavigationController(rootViewController: vc)
        present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func onClickEdie(_ sender: UIButton) { //Edit item
        isItemEdit = true
        selectedItem = sender.tag
        let stryboard = UIStoryboard.init(name: "PostRequest", bundle: nil)
        let vc = stryboard.instantiateViewController(withIdentifier: "EditAddItemsViewController") as! EditAddItemsViewController
        vc.modalPresentationStyle = .fullScreen
        vc.delegate = self
        vc.isFromEdit = true
        vc.post_requestId = post_requestId
        let temp = self.arrayOfRequestListItems[sender.tag].dictionaryValue
        let dic = temp as NSDictionary
        vc.dicItem = dic.mutableCopy() as! NSMutableDictionary
        let navigationController = UINavigationController(rootViewController: vc)
        present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func onClickDelete(_ sender: UIButton) {
        let alert = UIAlertController(title: "Familheey", message: "Do you really want to delete this item?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "DELETE", style: .destructive, handler: { _ in
            self.deleteItem(tag: sender.tag)
        }))
        alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func onClickFundTypeSelection(_ sender: UIButton) {
        /* if sender.tag == 1{
         self.fundType = "general"
         
         typeVew1.isHidden = false
         self.txtPostTo.text = "All families"
         self.requestType = "all_family"
         
         self.selectedIds.removeAllObjects()
         self.requestType = ""
         
         imgGeneral.image =  #imageLiteral(resourceName: "Green_tick.png")
         imgFund.image = #imageLiteral(resourceName: "gray_circle.png")
         }
         else{
         self.fundType = "fund"
         
         self.txtPostTo.text = "Selected families"
         self.requestType = "selected_family"
         
         typeVew1.isHidden = true
         self.selectedIds.removeAllObjects()
         self.requestType = ""
         
         imgFund.image =  #imageLiteral(resourceName: "Green_tick.png")
         imgGeneral.image = #imageLiteral(resourceName: "gray_circle.png")
         }*/
    }
    
    //MARK:- Custom Delegate
    func itemUpdated(itemDic: NSMutableDictionary) {
        print(itemDic)
        if isItemEdit{
            var item = self.arrayOfRequestListItems[selectedItem]
            
            item["request_item_description"] = (itemDic["request_item_description"] as? JSON)!
            item["request_item_title"] = (itemDic["request_item_title"] as? JSON)!
            item["item_quantity"] = (itemDic["item_quantity"] as? JSON)!
            
            self.arrayOfRequestListItems.remove(at: selectedItem)
            self.arrayOfRequestListItems.insert(item, at: selectedItem)
            print(arrayOfRequestListItems)
            
        }
        else{
            
            //            self.arrayOfRequestListItems.append(testJson)
        }
        tblList.reloadData()
    }
    func newItemAdded(item: JSON) {
        self.arrayOfRequestListItems.append(item)
        tblList.reloadData()
    }
    
    func selectedCallBack(SelectedUserId: NSMutableArray,tempStr:String) {
        print(SelectedUserId)
        if SelectedUserId.count == 0{
            self.txtPostTo.text = ""
            self.requestType = ""
            lblFamilyCount.isHidden = true
        }
        else{
            lblFamilyCount.isHidden = false
            if selectedIds.count == 1{
                lblFamilyCount.text = "\(selectedIds.count) family is selected"
            }
            else{
                lblFamilyCount.text = "\(selectedIds.count) families are seleted"
            }
            
        }
        self.selectedIds = SelectedUserId
    }
    
    func fundSelectedCallBack(SelectedUserId: NSMutableArray, tempStr: String, fName: String) {
    }
    
    //MARK:- Web API
    
    
    func getRequestDetails(id:String){
        self.view.endEditing(true)
        
        ActivityIndicatorView.show("Please wait....")
        let param = ["post_request_id":id,"user_id":UserDefaults.standard.value(forKey: "userId") as! String]
        print(param)
        networkProvider.request(.post_need_detail(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        if let need = jsonData["data"].dictionary{
                            if (need["post_request_id"] != nil){
                                if let items = need["items"]?.array{
                                    self.arrayOfRequestListItems = items
                                    self.tblList.delegate = self
                                    self.tblList.dataSource = self
                                    self.tblList.reloadData()
                                }
                            }
                        }

                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getRequestDetails(id: id)
                        }
                    }
                    else if response.statusCode == 500{
                        
                        self.displayAlert(alertStr: "Sorry, details of this request are marked as private and cannot be displayed", title: "")
                        
                    }
                    else{
                        self.displayAlert(alertStr: "Oops! This content is no longer available.", title: "")
                        self.navigationController?.popViewController(animated: true)
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                    
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
        
    }

    
    func deleteItem(tag:Int){
        let post = self.arrayOfRequestListItems[tag]
        
        var postItemId = post["item_id"].stringValue
        if postItemId.isEmpty{
            postItemId = post["id"].stringValue
        }
        
        let param = ["id" : postItemId,"user_id":UserDefaults.standard.value(forKey: "userId")as! String, "post_request_id":post_requestId]
        print(param)
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.item_delete(parameter: param), completion: {(result) in
            
            ActivityIndicatorView.hiding()
            switch result {
                
            case .success(let response):
                do {
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    print(response.statusCode)
                    if response.statusCode == 200{
                        appDel.isRequestUpdated = true
                        self.displayAlertChoice(alertStr: "The item is deleted from your request", title: "") { (result) in
                            self.arrayOfRequestListItems.remove(at: tag)
                            self.tblList.reloadData()
                            appDel.isConversationUpdated = true
                            appDel.iscreation = isFromCreation.request
                            
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.deleteItem(tag: tag)
                        }
                    }
                    else{
                        print(jsonData["code"])
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                } catch let err {
                    
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
        })
    }
    
    func publishYourRequestAPI(){
        print(self.selectedStartDate)
        self.startDateTimestamp = self.convertTimetoTimestamp(date: self.selectedStartDate)
        
        if selectedEndDate != ""{
            self.endDateTimestamp = self.convertTimetoTimestamp(date: self.selectedEndDate)
        }
        else{
            self.endDateTimestamp = 0
        }
        
        var param = [String:Any]()
        if self.selectedLong != nil && selectedLat != nil{
            param = [
                "user_id":UserDefaults.standard.value(forKey: "userId") as! String,
                "group_type":self.requestType,
                "to_group_id":self.selectedIds,
                "start_date":self.startDateTimestamp!,
                "end_date":self.endDateTimestamp!,
                "request_location":self.lblLocation.text!,
                "items":self.arrayOfItems,
                "lat":self.selectedLat!,
                "long":self.selectedLong!,"post_request_id":post_requestId,
                "request_type":self.fundType
                ] as [String : Any]
        }
        else{
            param = [
                "user_id":UserDefaults.standard.value(forKey: "userId") as! String,
                "group_type":self.requestType,
                "to_group_id":self.selectedIds,
                "start_date":self.startDateTimestamp!,
                "end_date":self.endDateTimestamp!,
                "request_location":self.lblLocation.text!,
                "items":self.arrayOfItems,"post_request_id":post_requestId,
                "request_type":self.fundType
                ] as [String : Any]
        }
        
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.update_request(parameter: param), completion: {(result) in
            ActivityIndicatorView.hiding()
            switch result {
                
            case .success(let response):
                do {
                    let jsonData = JSON(response.data as Any)
                    if response.statusCode == 200{
                        appDel.isRequestUpdated = true
                        self.displayAlertChoice(alertStr: "Your request updated successfully", title: "") { (result) in
                            appDel.iscreation = isFromCreation.request
                            appDel.isConversationUpdated = true
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.publishYourRequestAPI()
                        }
                    }
                    else{
                        print(jsonData["code"])
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                } catch let err {
                    
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
        })
    }
    
    //MARK: Custom Date Functions
    func convertDateFormat(date:String)->String{
        do{
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy hh:mm a"
            //        let convertedDate = formatter.date(from: "16-09-2017 2:00 PM")
            if  let convertedDate = try formatter.date(from: date){
                //                formatter.timeZone = TimeZone(identifier: "UTC")
                formatter.timeZone = NSTimeZone.local
                let date = formatter.string(from: convertedDate)
                print(date)
                let formatter2 = DateFormatter()
                formatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
                let convertedDate2 = formatter.date(from: date)
                formatter2.timeZone = NSTimeZone.local
                let date2 = formatter2.string(from: convertedDate2!)
                print(date2)
                
                return date2
            }
            else{
                return ""
            }
            
        }catch{
            return ""
        }
    }
    
    func checkDateHigherThanCurrentDate (selectedDatestring:String)->Bool{
        print(selectedDatestring)
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"
        
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        let convertedDateNow = formatter.string(from: Date())
        let convertedDateNow2 = formatter.date(from: convertedDateNow)
        formatter2.timeZone = NSTimeZone.local
        let dateNow = formatter2.string(from: convertedDateNow2!)
        print(dateNow)
        print(selectedDatestring < dateNow)
        return selectedDatestring < dateNow
    }
    func checkstartDateandEndDate (StartDate:String , endDate:String)->Bool{
        return StartDate > endDate
    }
    
    func convertTimetoTimestamp(date:String)->Int{
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"
        //        let convertedDate = formatter.date(from: "16-09-2017 2:00 PM")
        let convertedDate = formatter.date(from: date)
        
        //        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.timeZone = NSTimeZone.local
        let date = formatter.string(from: convertedDate!)
        print(date)
        let convertedDate2 = formatter.date(from: date)
        
        let timestamp = convertedDate2!.timeIntervalSince1970
        print(timestamp)
        
        return Int(timestamp)
        
    }
    func convertTimeStamp(timestamp:String) -> String {
        let date = Date(timeIntervalSince1970: Double(timestamp) as! TimeInterval)
        //    dateFormatter.dateFormat       = "EEE MMM dd yyy"
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy   hh:mm a"
        //    formatter.timeZone = TimeZone(identifier: "UTC")
        //        formatter.timeZone = TimeZone.current
        formatter.timeZone = NSTimeZone.local
        
        
        let localDatef = formatter.string(from: date)
        //        print(localDatef)
        return localDatef
    }
    
    
    //MARK:- Tost Message
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: 20, y: self.view.frame.size.height/2-30, width: self.view.frame.width-40, height: 60))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.numberOfLines = 2
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 10.0)
        toastLabel.adjustsFontSizeToFitWidth = true
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 6.0, delay: 0.3, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension EditRequestViewController:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.arrayOfRequestListItems.count == 0{
            self.btnAddMore.isHidden = true
            self.viewAddMore.isHidden = true
            
            return 1
        }
        else{
            self.btnAddMore.isHidden = false
            self.viewAddMore.isHidden = false
            
            return arrayOfRequestListItems.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.arrayOfRequestListItems.count == 0{
            let cellid = "newRequestAddTableViewCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! newRequestAddTableViewCell
            return cell
        }
        else{
            let cellid = "RequestItemTableViewCell"
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! RequestItemTableViewCell
            
            let item = self.arrayOfRequestListItems[indexPath.row]
            cell.lblDesc.text = item["request_item_description"].string ?? ""
            cell.lblTitle.text = item["request_item_title"].string ?? ""
            
            if requestDic["request_type"].stringValue.lowercased() == "general"{
                if selectedItem == indexPath.row{
                    if isItemEdit{
                        isItemEdit = false
                        //                    cell.lblQuantity.text = item["item_quantity"].string ?? ""
                        cell.lblQuantity.text = "\(item["item_quantity"].int ?? 0)"
                    }
                    else{
                        cell.lblQuantity.text = "\(item["item_quantity"].int ?? 0)"
                    }
                }
                else{
                    cell.lblQuantity.text = "\(item["item_quantity"].int ?? 0)"
                }
                cell.lblFooter.text = "Quantity"
            }
            else{
                if selectedItem == indexPath.row{
                    if isItemEdit{
                        isItemEdit = false
                        //                    cell.lblQuantity.text = item["item_quantity"].string ?? ""
                        cell.lblQuantity.text = "$\(item["item_quantity"].int ?? 0)"
                    }
                    else{
                        cell.lblQuantity.text = "$\(item["item_quantity"].int ?? 0)"
                    }
                }
                else{
                    cell.lblQuantity.text = "$\(item["item_quantity"].int ?? 0)"
                }
                cell.lblFooter.text = "Amount"
            }
            
            
            
            cell.btnEdit.tag = indexPath.row
            cell.btnDelete.tag = indexPath.row
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if arrayOfRequestListItems.count == 0{
            return 250
        }
        else{
            return UITableView.automaticDimension
        }
    }
}

extension EditRequestViewController:SambagDatePickerViewControllerDelegate{
    func didSelectDate(result:String,dateTag:Int){
        if dateTag == 0{
            //            txtStartDate.text = "\(result)"
            selectedStartDate = "\(result)"
            print(selectedStartDate)
            self.timetag = 0
        }
        else{
            //            txtEndDate.text = "\(result)"
            selectedEndDate = "\(result)"
            print(selectedEndDate)
            
            self.timetag = 1
            
        }
        let vc = SambagTimePickerViewController()
        vc.theme = theme
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    func sambagDatePickerDidSet(_ viewController: SambagDatePickerViewController, result: SambagDatePickerResult) {
        if self.dateTag == 0{
            //            txtStartDate.text = "\(result)"
            selectedStartDate = "\(result)"
            //            print(selectedStartDate)
            self.timetag = 0
        }
        else{
            //            txtEndDate.text = "\(result)"
            selectedEndDate = "\(result)"
            //            print(selectedEndDate)
            self.timetag = 1
        }
        viewController.dismiss(animated: true, completion: nil)
        let vc = SambagTimePickerViewController()
        vc.theme = theme
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    func sambagDatePickerDidCancel(_ viewController: SambagDatePickerViewController) {
        if self.dateTag == 0{
            self.AutofillStartDatewithToday()
            
        }
        else{
            txtEndDate.text = ""
            selectedEndDate = ""
        }
        viewController.dismiss(animated: true, completion: nil)
    }
}
extension EditRequestViewController:SambagTimePickerViewControllerDelegate{
    func sambagTimePickerDidSet(_ viewController: SambagTimePickerViewController, result: SambagTimePickerResult) {
        if self.timetag == 0{
            self.txtEndDate.text = ""
            selectedEndDate = ""
            
            selectedStartDate += "   \(result)"
            self.selectedStartDateFormatted = self.convertDateFormat(date: selectedStartDate)
            print(selectedStartDateFormatted)
            if selectedStartDateFormatted != ""{
                txtStartDate.text = Helpers.sambagTimeDisplay(dateAsString:"\(selectedStartDate)")
                
            }
        }
        else{
            selectedEndDate += "   \(result)"
            self.selectedEndDateFormatted = self.convertDateFormat(date: selectedEndDate)
            if selectedEndDateFormatted != ""{
                if self.checkDateHigherThanCurrentDate(selectedDatestring: self.selectedEndDateFormatted){
                    self.showToast(message: "Please Select End date Higher than current date and time")
                    txtEndDate.text = ""
                    selectedEndDate = ""
                    selectedEndDateFormatted = ""
                }
                else if checkstartDateandEndDate(StartDate: selectedStartDateFormatted
                    , endDate: selectedEndDateFormatted){
                    self.showToast(message: "Please Select end date higher than startDate")
                    txtEndDate.text = ""
                    selectedEndDate = ""
                    selectedEndDateFormatted = ""
                }
                else if selectedStartDateFormatted == selectedEndDateFormatted{
                    self.showToast(message: "Please Select different endDate and startDate ")
                    txtEndDate.text = ""
                    selectedEndDate = ""
                    selectedEndDateFormatted = ""
                }
                else{
                    txtEndDate.text = Helpers.sambagTimeDisplay(dateAsString:"\(selectedEndDate)")
                }
            }
        }
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func sambagTimePickerDidCancel(_ viewController: SambagTimePickerViewController) {
        if self.timetag == 0{
            self.AutofillStartDatewithToday()
            
        }
        else{
            txtEndDate.text = ""
            selectedEndDate += ""
            self.selectedEndDateFormatted = ""
        }
        
    }
    
    func AutofillStartDatewithToday()
    {
        let currentDateTime = Date()
        print(currentDateTime)
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd yyyy hh:mm a"
        //        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.timeZone = TimeZone.current
        
        
        let localDatef = formatter.string(from: currentDateTime)
        txtStartDate.text = "\(localDatef)"
        
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "dd/MM/yyyy   hh:mm a"
        formatter2.timeZone = NSTimeZone.local
        
        
        let localDatef2 = formatter2.string(from: currentDateTime)
        selectedStartDate = "\(localDatef2)"
        print(selectedStartDate)
    }
}
extension EditRequestViewController: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        // Get the place name from 'GMSAutocompleteViewController'
        // Then display the name in textField
        
        print(place.addressComponents!)
        self.lblLocation.text = "\(place.formattedAddress!)"
        self.selectedLong = place.coordinate.longitude
        self.selectedLat = place.coordinate.latitude
        
        dismiss(animated: true, completion: nil)
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // Handle the error
        print("Error: ", error.localizedDescription)
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        // Dismiss when the user canceled the action
        dismiss(animated: true, completion: nil)
    }
}
