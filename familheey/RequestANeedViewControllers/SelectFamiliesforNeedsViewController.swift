//
//  SelectFamiliesforNeedsViewController.swift
//  familheey
//
//  Created by familheey on 31/03/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON

protocol SelctedFamiliesForNeedDelegate : class{
    func selectedCallBack(SelectedUserId:NSMutableArray,tempStr:String)
    func fundSelectedCallBack(SelectedUserId:NSMutableArray,tempStr:String,fName:String)

}

class SelectFamiliesforNeedsViewController: UIViewController {
    @IBOutlet weak var noDataView: UIView!

    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnReset: UIButton!
    
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    
    var selectedUserIDArr = NSMutableArray()
    var arrFamily = JSON()
    weak var selectionDelegate:SelctedFamiliesForNeedDelegate?
    var FamilyArr : familyListResponse!
    var fundType = false
    var groupAccount = ""
    var groupName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtSearch.delegate = self

        let headerNib = UINib.init(nibName: "FamilyListingView", bundle: Bundle.main)
        tblList.register(headerNib, forHeaderFooterViewReuseIdentifier: "FamilyListingView")
        
        if fundType{
            getAdminFamilyList()
        }
        else{
            getFamilies()
        }


        // Do any additional setup after loading the view.
    }
    
    //MARK:- UIButton actions
    @IBAction func onClickSearchReset(_ sender: Any) {
        txtSearch.text = ""
        btnReset.isHidden = true
        if fundType{
            getAdminFamilyList()
        }
        else{
            getFamilies()
        }

    }
    
    
    //MARK:- Web API
    func getFamilies(){
        let params = [
            "user_id":UserDefaults.standard.value(forKey: "userId") as! String,
            "query":txtSearch.text!
        ]
        APIServiceManager.callServer.getFamilyList(url: EndPoint.viewFamily, params: params, success: { (responseMdl) in
            
            guard let familyMdl = responseMdl as? familyListResponse else{
                return
            }
            ActivityIndicatorView.hiding()
            
            if familyMdl.statusCode == 200{
                //                    print(familyMdl.familyList!)
                
        
                
                if familyMdl.familyList!.count > 0{
                    self.FamilyArr = familyMdl
                    self.tblList.isHidden = false
                    self.noDataView.isHidden = true
                    self.tblList.delegate = self
                    self.tblList.dataSource = self
                    //                        @IBOutlet weak var lblSearchResultInList: UILabel!
                    //
                    //                          @IBOutlet weak var heightForSearchTextInList: NSLayoutConstraint!
                    
                    
                    /*if self.txtSearch.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
                        
                        self.heightForSearchTextInList.constant = 0
                        self.lblSearchResultInList.isHidden = true
                        self.lblSearchResult.isHidden = true
                        
                        
                        
                    } else {
                        
                        self.heightForSearchTextInList.constant = 25
                        self.lblSearchResultInList.isHidden = false
                        self.lblSearchResult.isHidden = true
                        
//                        self.lblSearchResultInList.text = "Search result for \"\(self.txtSearch.text!)\""
                        
                    }*/
                    
                    self.tblList.reloadData()
                }
                else{
                    
                    self.tblList.isHidden = true
                    self.noDataView.isHidden = false

//                    self.heightForSearchTextInList.constant = 0
//                    self.lblSearchResultInList.isHidden = true
//                    self.lblSearchResult.isHidden = false
                    
                    /*if self.txtSearch.text != "" {
                        
                        self.lblSearchResult.text = "Search result for \"\(self.txtSearch.text!)\""
                        self.bg_noFamilyView.isHidden = true
                        self.noSearchResultView.isHidden = false
                    } else {
                        
                        self.bg_noFamilyView.isHidden = false
                        self.noSearchResultView.isHidden = true
                        self.btnAdd.isHidden = true
                    }*/
                }
                
            }
            else{
                self.displayAlert(alertStr: "Something went wrong! Please try again later", title: "Error")
                self.tblList.isHidden = true
                           self.noDataView.isHidden = false
                ActivityIndicatorView.hiding()
            }
            
        }) { (error) in
            ActivityIndicatorView.hiding()
            self.tblList.isHidden = true
                       self.noDataView.isHidden = false
            self.displayAlert(alertStr: error!.description, title: "Error")
        }
    }
    
    func getAdminFamilyList(){
        
        let params = [
            "user_id":UserDefaults.standard.value(forKey: "userId") as! String,
            "query":txtSearch.text!
            ] as [String: Any]
        
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.getUserAdminGroups(parameter: params), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    let jsonData = JSON(response.data)
                    print(jsonData)
                    if response.statusCode == 200{
                        self.arrFamily = jsonData["data"]
                        if self.arrFamily.count > 0{
                            self.tblList.isHidden = false
                            self.noDataView.isHidden = true
                            self.tblList.delegate = self
                            self.tblList.dataSource = self
                            self.tblList.reloadData()
                        }
                        else{
                            self.tblList.isHidden = true
                            self.noDataView.isHidden = false
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            let btn = UIButton()
                            self.getAdminFamilyList()
                        }
                    }

                    else{
                        self.displayAlert(alertStr: "Something went wrong! Please try again later", title: "Error")
                        self.tblList.isHidden = true
                        self.noDataView.isHidden = false
                        ActivityIndicatorView.hiding()
                    }
                } catch let err {
                    
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
    //MARK:- Button actions
    @IBAction func onClickBack(_ sender: Any) {
//        self.selectionDelegate!.selectedCallBack(SelectedUserId: selectedUserIDArr, tempStr: groupAccount)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickDone(_ sender: Any) {
        if selectedUserIDArr.count > 0{
            if fundType{
                self.selectionDelegate!.fundSelectedCallBack(SelectedUserId: selectedUserIDArr, tempStr: groupAccount, fName: groupName)
            }
            else{
                self.selectionDelegate!.selectedCallBack(SelectedUserId: selectedUserIDArr, tempStr: groupAccount)
            }
            self.navigationController?.popViewController(animated: true)
        }
        else{
            self.displayAlert(alertStr: "Please select atleast one family", title: "")
        }
    }
    
    @IBAction func inviteFamilySelection(sender: UIButton){
        
        if fundType{
            selectedUserIDArr.removeAllObjects()
            let grpId = arrFamily.arrayValue[sender.tag]["id"].stringValue
            groupAccount = arrFamily.arrayValue[sender.tag]["stripe_account_id"].stringValue
            groupName = arrFamily.arrayValue[sender.tag]["group_name"].stringValue
            selectedUserIDArr.add(grpId)
            tblList.reloadData()
        }
        else{
            let grpId = "\(FamilyArr.familyList![sender.tag].faId)"
            if selectedUserIDArr.contains(grpId){
                selectedUserIDArr.remove(grpId)
            }
            else{
                selectedUserIDArr.add(grpId)
            }
            tblList.reloadSections(IndexSet(integer : sender.tag), with: .none)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension SelectFamiliesforNeedsViewController : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
            if textField.text!.count > 0{
                btnReset.isHidden = false
            }
            else{
                btnReset.isHidden = true
            }
            textField.endEditing(true)
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        self.view.endEditing(true)
            if textField.text!.count > 0{
                btnReset.isHidden = false
            }
            else{
                btnReset.isHidden = true
            }
        if fundType{
           getAdminFamilyList()
        }
        else{
          getFamilies()
        }
            
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text!.count > 0 {
            btnReset.isHidden = false
        }
        else{
            btnReset.isHidden = true
        }
        return true
    }
    
}

extension SelectFamiliesforNeedsViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        if fundType{
            return arrFamily.count
        }
        else{
           return FamilyArr.familyList!.count
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // return FamilyArr.familyList!.count
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
       
        let header = self.tblList.dequeueReusableHeaderFooterView(withIdentifier: "FamilyListingView") as! FamilyListingView
        header.btnAction.tag = section
        header.btnView.tag = section
        header.btnView.addTarget(self, action: #selector(inviteFamilySelection(sender:)), for: .touchUpInside)
        header.btnAction.addTarget(self, action: #selector(inviteFamilySelection(sender:)), for: .touchUpInside)
        
        if fundType{
            header.lblType.isHidden = true
            header.lblKnownHead.isHidden = true
            header.lblKnown.isHidden = true
            header.lblMemberHead.isHidden = true
            header.lblMembersCount.isHidden = true
            
            if header.lblTitle.text!.isEmpty ||  header.lblTitle.text == nil{
                header.lblTitle.text = "Unknown"
            }
            else{
                header.lblTitle.text = self.arrFamily.arrayValue[section]["group_name"].stringValue.firstUppercased
            }
            header.byTitle.text = "By"
            header.lblCreatedBy.text = self.arrFamily.arrayValue[section]["full_name"].stringValue
            header.lblRegion.text = self.arrFamily.arrayValue[section]["base_region"].stringValue
            
            if arrFamily.arrayValue[section]["logo"].stringValue.count != 0{
                let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=150&height=150&url="+"\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+arrFamily.arrayValue[section]["logo"].stringValue
                let imgurl = URL(string: temp)
                print(imgurl)
                header.imgLogo.kf.indicatorType = .activity
                
                header.imgLogo.kf.setImage(with: imgurl, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
            }
            else{
                header.imgLogo.image = UIImage(named: "Family Logo")
            }
            
            let grpId = arrFamily.arrayValue[section]["id"].stringValue
            if selectedUserIDArr.contains(grpId){
                header.btnAction.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
                header.btnAction.setTitle("Selected", for: .normal)
                
            }else{
                header.btnAction.borderWidth = 1
                header.btnAction.borderColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                header.btnAction.setTitleColor(#colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1), for: .normal)
                header.btnAction.backgroundColor = UIColor.clear
                header.btnAction.titleLabel?.font = UIFont.systemFont(ofSize: 15)
                header.btnAction.setTitle("Select", for: .normal)
            }
        }
        else{
            header.byTitle.text = "By"
            header.lblTitle.text = FamilyArr.familyList![section].faName
            if header.lblTitle.text!.isEmpty ||  header.lblTitle.text == nil{
                header.lblTitle.text = "Unknown"
            }
            header.lblType.text = FamilyArr.familyList![section].faCategory
            header.lblRegion.text = FamilyArr.familyList![section].faRegion
            header.lblCreatedBy.text = "\(FamilyArr.familyList![section].faAdmin)"
            // header.lblMembersCount.text = "\(FamilyArr.familyList![section].memberCount)"
            
            
            if FamilyArr.familyList![section].faLogo.count != 0{
                let temp = "\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+FamilyArr.familyList![section].faLogo
                let imgurl = URL(string: temp)
                let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+imgurl!.relativeString
                let url = URL(string: newUrlStr)
                header.imgLogo.kf.indicatorType = .activity
                
                header.imgLogo.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
            }
            else{
                header.imgLogo.image = #imageLiteral(resourceName: "Family Logo")
            }
            let grpId = "\(FamilyArr.familyList![section].faId)"
            if selectedUserIDArr.contains(grpId){
                header.btnAction.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
                header.btnAction.setTitle("Selected", for: .normal)
                
            }else{
                header.btnAction.borderWidth = 1
                header.btnAction.borderColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                header.btnAction.setTitleColor(#colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1), for: .normal)
                header.btnAction.backgroundColor = UIColor.clear
                header.btnAction.titleLabel?.font = UIFont.systemFont(ofSize: 15)
                header.btnAction.setTitle("Select", for: .normal)
            }
            
            
            header.lblKnownHead.text = "posts"
            
            let knownCount = FamilyArr.familyList![section].memberCount!
            
            if Int(knownCount)! > 0{
                header.lblMembersCount.text = FamilyArr.familyList![section].memberCount
                header.lblMemberHead.isHidden = false
                header.lblMembersCount.isHidden = false
            }
            else{
                header.lblMemberHead.isHidden = true
                header.lblMembersCount.isHidden = true
            }
            
            let eventCount = FamilyArr.familyList![section].knownCount!
            
            if Int(eventCount)! > 0{
                header.lblKnown.text = FamilyArr.familyList![section].knownCount
                header.lblKnownHead.isHidden = false
                header.lblKnown.isHidden = false
            }
            else{
                header.lblKnownHead.isHidden = true
                header.lblKnown.isHidden = true
            }
        }
        
        return header
    }
    
}
