//
//  AddNewItemViewController.swift
//  familheey
//
//  Created by familheey on 30/03/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit

protocol AddYourNeedDelegate: class {
    func itemAdded(itemDic:[String:String])
}

class AddNewItemViewController: UIViewController {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var txtDesc: UITextView!
    @IBOutlet weak var txtQuantity: UITextField!
    @IBOutlet weak var btnAddItem: UIButton!
    
    weak var delegate: AddYourNeedDelegate?
    var dicItem = [String:String]()
    var isFromEdit = false
    var isFund = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblTitle.attributedText = Helpers.setAttributedPlaceHolder(placeHolderName: "Request Title")
        if isFund{
            self.lblQuantity.attributedText = Helpers.setAttributedPlaceHolder(placeHolderName: "Amount")
        }
        else{
          self.lblQuantity.attributedText = Helpers.setAttributedPlaceHolder(placeHolderName: "Quantity")
        }
        
        self.navigationController?.navigationBar.isHidden = true
        if isFromEdit{
            txtTitle.text = dicItem["request_item_title"]
            txtDesc.text = dicItem["request_item_description"]
            txtQuantity.text = dicItem["item_quantity"]
            btnAddItem.setTitle("Update", for: .normal)
        }
        else{
//            dicItem.removeAllObjects()
            btnAddItem.setTitle("Add", for: .normal)
        }
        // Do any additional setup after loading the view.
    }
    
    
    //MARK:- Button Actions
    @IBAction func onClickBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickAdd(_ sender: Any) {
        if txtTitle.text!.isEmpty{
            self.displayAlert(alertStr: "Please enter a title", title: "")
            return
        }
        
        if txtQuantity.text!.isEmpty{
            self.displayAlert(alertStr: "Please enter the item quantity", title: "")
            return
        }
        
        dicItem.removeAll()
        dicItem = ["request_item_title":txtTitle.text!,"item_quantity":txtQuantity.text!, "request_item_description":txtDesc.text!]
        
        self.delegate?.itemAdded(itemDic: dicItem)
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
