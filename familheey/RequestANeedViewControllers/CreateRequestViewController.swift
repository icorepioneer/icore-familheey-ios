//
//  CreateRequestViewController.swift
//  familheey
//
//  Created by familheey on 27/03/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit
import iOSDropDown
import GooglePlaces
import Moya
import SwiftyJSON

class CreateRequestViewController: UIViewController, AddYourNeedDelegate,SelctedFamiliesForNeedDelegate {
    var isAdmin = ""
    
    @IBOutlet weak var viewOfFunding: UIView!
    @IBOutlet weak var viewOfGeneral: UIView!
    @IBOutlet weak var lblHead1: UILabel!
    @IBOutlet weak var txtPostTo: UITextField!
    @IBOutlet weak var postToView: UIView!
    @IBOutlet weak var imgDown: UIImageView!
    @IBOutlet weak var txtStartDate: UITextField!
    @IBOutlet weak var txtEndDate: UITextField!
    @IBOutlet weak var txtLocation: UITextField!
    @IBOutlet weak var tblItemList: UITableView!
    @IBOutlet weak var btnAddMore: UIButton!
    @IBOutlet weak var lblFamilyCount: UILabel!
    @IBOutlet weak var viewAddMore: UIView!
    @IBOutlet weak var imgGeneral: UIImageView!
    @IBOutlet weak var imgFunding: UIImageView!
    @IBOutlet weak var typeVew1: UIView!
    @IBOutlet weak var typeVew2: UIView!
    @IBOutlet weak var btnGrpAcunt: UIButton!
    @IBOutlet weak var accountVew: UIView!
    @IBOutlet weak var lblAcntDetails: UILabel!
    @IBOutlet weak var noAccontVew: UIView!
    @IBOutlet weak var acntNumbrVew: UIView!
    @IBOutlet weak var lblAcntNumbr: UILabel!
    @IBOutlet weak var lblacntStatus: UILabel!
    @IBOutlet weak var statusVew: UIView!
    @IBOutlet weak var lblacntStatusHead: UILabel!
    @IBOutlet weak var imgAlert: UIImageView!
    @IBOutlet weak var postVw_height: NSLayoutConstraint!
    @IBOutlet weak var btnAcntStatusClick: UIButton!
    @IBOutlet weak var btnCreateRqst: UIButton!
    
    
    var fromGroup = false
    
    var theme: SambagTheme = .light
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    let locationManager = CLLocationManager()
    
    var arrayOfItems = NSMutableArray()
    var selectedIds = NSMutableArray()
    var accountArr = JSON()
    var dateTag = Int()
    var timetag = Int()
    var selectedStartDate:String = ""
    var selectedEndDate:String = ""
    var selectedStartDateFormatted:String = ""
    var selectedEndDateFormatted:String = ""
    var startDateTimestamp:Int! = 0
    var endDateTimestamp:Int! = 0
    var requestType = ""
    var isItemEdit = false
    var selectedItem = 0
    var selectedLat : Double!
    var selectedLong :Double!
    var fundType = "general"
    var grupAccount = ""
    var isAcntPending = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(accountCreateCheck(notification:)), name: Notification.Name("AccountCreate"), object: nil)
        self.lblHead1.attributedText = Helpers.setAttributedPlaceHolder(placeHolderName: "Post this request to")
        tblItemList.delegate = self
        tblItemList.dataSource = self
        tblItemList.reloadData()
        
        if fromGroup {
            if isAdmin.lowercased() == "admin"{
                viewOfFunding.isHidden = false
                viewOfGeneral.isHidden = false
            }
            else
            {
                viewOfFunding.isHidden = true
                viewOfGeneral.isHidden = false
            }
            self.btnCreateRqst.isEnabled = false
            self.txtPostTo.isEnabled = false
            self.imgDown.isHidden = true
            
            self.selectedCallBack(SelectedUserId: self.selectedIds, tempStr: grupAccount)
            self.txtPostTo.text = "Selected families"
            self.requestType = "selected_family"
        }
        else{
            self.btnCreateRqst.isEnabled = true
            self.txtPostTo.isEnabled = true
            self.imgDown.isHidden = false
            
            self.txtPostTo.text = "All families"
            self.requestType = "all_family"
        }
        
        self.AutofillStartDatewithToday()
        if CLLocationManager.locationServicesEnabled() {
            
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access location")
                //self.callCurrentLocation()
                let currentLocale = NSLocale.current as NSLocale
                let countryCode = currentLocale.object(forKey: .countryCode) as! String//get the set country name, code of your iphone
                let countryName = currentLocale.displayName(forKey: NSLocale.Key.countryCode, value: countryCode)
                print("from current location")
                print(countryName!)
                
                if countryName!.isEmpty{
                    
                }
                else{
                  self.txtLocation.text = "\(countryName!)"
                }
                
                
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access location")
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
                
            @unknown default:
                print("No access location")
            }
        }
        else{
            let currentLocale = NSLocale.current as NSLocale
            let countryCode = currentLocale.object(forKey: .countryCode) as! String//get the set country name, code of your iphone
            let countryName = currentLocale.displayName(forKey: NSLocale.Key.countryCode, value: countryCode)
            print("from current location")
            print(countryName!)
            
            if countryName!.isEmpty{
                
            }
            else{
                self.txtLocation.text = "\(countryName!)"
            }
        }
        
        
        //            print(localDatef)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
//        if !UIDevice.current.systemVersion.contains("13"){
//            txtLocation.text = appDel.selectedEventVenue
//        }
        guard #available(iOS 13.0, *) else {
            txtLocation.text = appDel.selectedEventVenue
            return
        }
        
        if selectedIds.count == 1 && !grupAccount.isEmpty{
            accountVew.isHidden = false
            noAccontVew.isHidden = true
            getStripeAccountByID()
            
        }
//        else if appDel.createAcntFromGrp{
//            getStripeAccountByID()
//        }
    }
    
    func AutofillStartDatewithToday()
    {
        let currentDateTime = Date()
        print(currentDateTime)
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd yyyy hh:mm a"
        //        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.timeZone = TimeZone.current
        
        
        let localDatef = formatter.string(from: currentDateTime)
        txtStartDate.text = "\(localDatef)"
        
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "dd/MM/yyyy   hh:mm a"
        //    formatter.timeZone = TimeZone(identifier: "UTC")
        //        formatter.timeZone = TimeZone.current
        formatter2.timeZone = NSTimeZone.local
        
        
        let localDatef2 = formatter2.string(from: currentDateTime)
        selectedStartDate = "\(localDatef2)"
        print(selectedStartDate)
    }
    
    //MARK:- UIButton Actions
    @IBAction func onClickBack(_ sender: Any) {
        if arrayOfItems.count > 0{
            let alert = UIAlertController(title: "Familheey", message: "There are unsaved items in this. Do you really want to cancel the request?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { _ in
                self.navigationController?.popViewController(animated: true)
            }))
            alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
        else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func onClickPostTo(_ sender: Any) {
        if self.postToView.isHidden{
            postToView.isHidden = false
            self.imgDown.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        }
        else{
            postToView.isHidden = true
            self.imgDown.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi * 2))
        }
    }
    
    @IBAction func onClickStartDate(_ sender: Any) {
        self.dateTag = 0
        let vc = SambagDatePickerViewController()
        vc.theme = theme
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    @IBAction func onClickEndDate(_ sender: Any) {
        self.dateTag = 1
        let vc = SambagDatePickerViewController()
        vc.theme = theme
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func onClickSelectPostTp(_ sender: UIButton) {
        
        if sender.tag == 1{
            self.txtPostTo.text = "All families"
            self.requestType = "all_family"
            selectedIds.removeAllObjects()
            lblFamilyCount.isHidden = true
            self.imgDown.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi * 2))
            self.postToView.isHidden = true
        }
        else{
            self.txtPostTo.text = "Selected families"
            self.requestType = "selected_family"
            self.grupAccount = ""
            self.accountVew.isHidden = true
            
            
            let stryboard = UIStoryboard.init(name: "PostRequest", bundle: nil)
            let vc = stryboard.instantiateViewController(withIdentifier: "SelectFamiliesforNeedsViewController") as! SelectFamiliesforNeedsViewController
            vc.selectionDelegate = self
            vc.selectedUserIDArr = selectedIds
            if fundType.lowercased() == "general"{
                vc.fundType = false
            }
            else{
                vc.fundType = true
            }
            self.navigationController?.pushViewController(vc, animated: true)
            self.imgDown.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi * 2))
            self.postToView.isHidden = true
        }
    }
    
    @IBAction func onClickAddItem(_ sender: Any) {
        let stryboard = UIStoryboard.init(name: "PostRequest", bundle: nil)
        let vc = stryboard.instantiateViewController(withIdentifier: "AddNewItemViewController") as! AddNewItemViewController
        vc.modalPresentationStyle = .fullScreen
        if fundType.lowercased() == "general"{
            vc.isFund = false
        }
        else{
            vc.isFund = true
        }
        vc.delegate = self
        let navigationController = UINavigationController(rootViewController: vc)
        present(navigationController, animated: true, completion: nil)
    }
    @IBAction func onClickPublishNeed(_ sender: Any) {
        if requestType.isEmpty{
            self.displayAlert(alertStr: "Please select a family", title: "")
            return
        }
        else if arrayOfItems.count == 0{
            self.displayAlert(alertStr: "Please add atleast one item in your request", title: "")
            return
        }
        else if txtLocation.text!.isEmpty{
            self.displayAlert(alertStr: "Please select your location", title: "")
            return
        }
        else if fundType.lowercased() == "fund"{
            if grupAccount.isEmpty{
                self.displayAlert(alertStr: "For requesting funds, you need to add banking details", title: "")
                return
            }
            else if isAcntPending{
                self.displayAlert(alertStr: "Your account status is pending, you can start requesting fund after account is active", title: "")
                return
            }
            else{
                publishYourRequestAPI()
            }
        }
            
        else{
            publishYourRequestAPI()
        }
        
    }
    @IBAction func onClickLocation(_ sender: Any) {
        
        guard #available(iOS 13.0, *) else {
            // Code for earlier iOS versions
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let SearchPlacesViewController = storyboard.instantiateViewController(withIdentifier: "SearchPlacesViewController") as! SearchPlacesViewController
            SearchPlacesViewController.selectedField = "eventCreation"
            self.present(SearchPlacesViewController, animated: true, completion: nil)
            
            return
        }
        
        let acController = GMSAutocompleteViewController()
        
        acController.delegate = self
        present(acController, animated: true, completion: nil)

    }
    @IBAction func onClickDelete(_ sender: UIButton) {
        let alert = UIAlertController(title: "Familheey", message: "Do you really want to delete this item?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "DELETE", style: .destructive, handler: { _ in
            self.arrayOfItems.removeObject(at: sender.tag)
            self.tblItemList.reloadData()
        }))
        alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func onClickEdit(_ sender: UIButton) {
        selectedItem = sender.tag
        isItemEdit = true
        
        let stryboard = UIStoryboard.init(name: "PostRequest", bundle: nil)
        let vc = stryboard.instantiateViewController(withIdentifier: "AddNewItemViewController") as! AddNewItemViewController
        vc.modalPresentationStyle = .fullScreen
        vc.delegate = self
        vc.isFromEdit = true
        let dic = self.arrayOfItems[sender.tag] as! NSDictionary
        vc.dicItem = self.arrayOfItems[sender.tag] as! [String : String]
        let navigationController = UINavigationController(rootViewController: vc)
        present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func onClickFundType(_ sender: UIButton) {
        if sender.tag == 1{
            self.fundType = "general"
            
            if fromGroup{
                typeVew1.isHidden = false
                postVw_height.constant = 100
                self.accountVew.isHidden =  true
                
                self.selectedCallBack(SelectedUserId: self.selectedIds, tempStr: grupAccount)
                self.txtPostTo.text = "Selected families"
                self.requestType = "selected_family"
            }
            else{
                typeVew1.isHidden = false
                postVw_height.constant = 100
                self.txtPostTo.text = "All families"
                self.requestType = "all_family"
                
                self.selectedIds.removeAllObjects()
                self.btnGrpAcunt.isHidden = true
                self.lblFamilyCount.isHidden = true
                self.requestType = ""
            }
            
            imgGeneral.image =  #imageLiteral(resourceName: "Green_tick.png")
            imgFunding.image = #imageLiteral(resourceName: "gray_circle.png")
        }
        else{
            self.fundType = "fund"
            
            self.txtPostTo.text = "Selected families"
            self.requestType = "selected_family"
            if fromGroup {
                if selectedIds.count == 1{
                    getStripeAccountByID()
                    self.accountVew.isHidden =  false
                }
                else{
                    self.selectedIds.removeAllObjects()
                    self.btnGrpAcunt.isHidden = true
                    self.lblFamilyCount.isHidden = true
                    self.requestType = ""
                }
                
            }
            else{
                self.selectedIds.removeAllObjects()
                self.btnGrpAcunt.isHidden = true
                self.lblFamilyCount.isHidden = true
                self.requestType = ""
            }
            typeVew1.isHidden = true
            postVw_height.constant = 75
            
            imgFunding.image =  #imageLiteral(resourceName: "Green_tick.png")
            imgGeneral.image = #imageLiteral(resourceName: "gray_circle.png")
        }
    }
    @IBAction func onClickCreateAccount(_ sender: Any) {
        let gId = self.selectedIds[0] as! String
        
        let parameter = ["group_id":gId] as [String : Any]
        print(parameter)
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.stripe_oauth_link_generation(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                do {
                    let jsonData = JSON(response.data as Any)
                    print("!!!!@ --- \(jsonData)")
                    if response.statusCode == 200{
                        if self.fromGroup{
                            appDel.createAcntFromGrp = true
                        }
                        if let urlStr = jsonData["link"].string, !urlStr.isEmpty{
                            let url = URL(string: urlStr)
                            UIApplication.shared.open(url!)
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            let btn = UIButton()
                            self.onClickCreateAccount(btn)
                        }
                    }
                } catch let err {
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
    @IBAction func onClickPendingStatus(_ sender: Any) {
        let attributedString = NSMutableAttributedString(string: "Your account is not active, few details might be missing, open the link below and sign into stripe to see details https://dashboard.stripe.com")
        let linkRange = (attributedString.string as NSString).range(of: "https://dashboard.stripe.com")
        attributedString.addAttribute(NSAttributedString.Key.link, value: "https://dashboard.stripe.com", range: linkRange)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value:#colorLiteral(red: 0.2274509804, green: 0.1333333333, blue: 0.3843137255, alpha: 1) , range: linkRange)
        
        let linkAttributes: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.foregroundColor: UIColor.green,
            NSAttributedString.Key.underlineColor: UIColor.lightGray,
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        
        
        let alert = UIAlertController(title: attributedString.string, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Open", style: .default, handler: { _ in
            let url = URL(string: "https://dashboard.stripe.com")
            UIApplication.shared.open(url!)
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        
        self.present(alert, animated: true, completion: nil)
    }
    

    
    //MARK:- Web API
    func publishYourRequestAPI(){
        
        self.startDateTimestamp = self.convertTimetoTimestamp(date: self.selectedStartDate)
        if selectedEndDate != ""
        {
            self.endDateTimestamp = self.convertTimetoTimestamp(date: self.selectedEndDate)
        }
        else
        {
            self.endDateTimestamp = 0 
        }
        
        var param = [String:Any]()
        if self.selectedLong != nil && selectedLat != nil{
            param = [
                "user_id":UserDefaults.standard.value(forKey: "userId") as! String,
                "group_type":self.requestType,
                "to_group_id":self.selectedIds,
                "start_date":self.startDateTimestamp!,
                "end_date":self.endDateTimestamp!,
                "request_location":self.txtLocation.text!,
                "items":self.arrayOfItems,
                "lat":self.selectedLat!,
                "long":self.selectedLong!,
                "request_type":self.fundType
                ] as [String : Any]
        }
        else{
            param = [
                "user_id":UserDefaults.standard.value(forKey: "userId") as! String,
                "group_type":self.requestType,
                "to_group_id":self.selectedIds,
                "start_date":self.startDateTimestamp!,
                "end_date":self.endDateTimestamp!,
                "request_location":self.txtLocation.text!,
                "items":self.arrayOfItems,
                "request_type":self.fundType
                ] as [String : Any]
        }
        
        
        print(param)
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.createNeeds(parameter: param), completion: {(result) in
            
            ActivityIndicatorView.hiding()
            switch result {
                
            case .success(let response):
                do {
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    print(jsonData["status"])
                    print(response.statusCode)
                    if response.statusCode == 200{
                        self.displayAlertChoice(alertStr: "Your request is successfully posted", title: "") { (result) in
                            if self.fromGroup {
                                self.navigationController?.popToViewController(ofClass: RequestInFamilyViewController.self)
                            }
                            else
                            {
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let tabBar = mainStoryBoard.instantiateViewController(withIdentifier: "mainTabView") as! HomeViewTabController
                                appDel.iscreation = isFromCreation.request
                                //  loginView.regType = regType
                                appDel.isConversationUpdated = true
                                appDel.noFamily = false
                                appDel.requestCreated = true
                                self.navigationController?.pushViewController(tabBar, animated: true)
                            }
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.publishYourRequestAPI()
                        }
                    }
                    else{
                        print(jsonData["code"])
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                } catch let err {
                    
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
        })
    }
    
    func getStripeAccountByID(){
        let gId = self.selectedIds[0] as! String
        
        let parameter = ["group_id":gId] as [String : Any]
        print(parameter)
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.stripeGetaccountById(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                do {
                    let jsonData = JSON(response.data as Any)
                    print("!!!!@ --- \(jsonData)")
                    if response.statusCode == 200{
                        self.accountArr = jsonData
                        if jsonData.count > 0{
                            if self.fromGroup{
                                self.accountVew.isHidden = false
                                self.acntNumbrVew.isHidden = false
                                self.noAccontVew.isHidden = true
                                self.lblAcntNumbr.text = jsonData["id"].stringValue
                                self.grupAccount = jsonData["id"].stringValue
                            }
                            appDel.createAcntFromGrp = false
                            if let payouts = jsonData["payouts_enabled"].bool, let charges = jsonData["charges_enabled"].bool, payouts && charges{
                                
                                self.lblacntStatus.text = "Active"
                                self.statusVew.isHidden = false
                                self.lblacntStatusHead.isHidden = false
                                self.isAcntPending = false
                                self.imgAlert.image = #imageLiteral(resourceName: "Green_tick.png")
                                self.btnAcntStatusClick.isEnabled = false
                            }
                            else{
                                self.lblacntStatusHead.text = "You can start fund requesting once your accounts gets active"
                                self.lblacntStatus.text = "Pending"
                                self.statusVew.isHidden = false
                                self.btnAcntStatusClick.isEnabled = true
                                self.isAcntPending = true
                                
                                let attributedString = NSMutableAttributedString(string: "Your account is not active, few details might be missing, open the link below and sign into stripe to see details https://dashboard.stripe.com")
                                let linkRange = (attributedString.string as NSString).range(of: "https://dashboard.stripe.com")
                                attributedString.addAttribute(NSAttributedString.Key.link, value: "https://dashboard.stripe.com", range: linkRange)
                                attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value:#colorLiteral(red: 0.2274509804, green: 0.1333333333, blue: 0.3843137255, alpha: 1) , range: linkRange)
                                
                                let linkAttributes: [NSAttributedString.Key : Any] = [
                                    NSAttributedString.Key.foregroundColor: UIColor.green,
                                    NSAttributedString.Key.underlineColor: UIColor.lightGray,
                                    NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue
                                ]
                                
                                
                                let alert = UIAlertController(title: attributedString.string, message: nil, preferredStyle: .actionSheet)
                                alert.addAction(UIAlertAction(title: "Open", style: .default, handler: { _ in
                                    let url = URL(string: "https://dashboard.stripe.com")
                                    UIApplication.shared.open(url!)
                                }))
                                
                                alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
                                /*If you want work actionsheet on ipad
                                 then you have to use popoverPresentationController to present the actionsheet,
                                 otherwise app will crash on iPad */
                                
                                self.present(alert, animated: true, completion: nil)
                            }
                            //                            self.tblItemList.reloadData()
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getStripeAccountByID()
                        }
                    }
                    else{
                        if appDel.createAcntFromGrp{
                            self.displayAlertChoice(alertStr: "If you have completed the stripe account creation, kindly wait for some time. We will notify you once account creation is successful. Otherwise complete the stripe account creation for raising fund request.", title: "") { (result) in
                                appDel.createAcntFromGrp = false
                            }
                        }
                        ActivityIndicatorView.hiding()
                        self.accountVew.isHidden = false
                        self.acntNumbrVew.isHidden = true
                        self.noAccontVew.isHidden = false
                        
                        self.lblAcntDetails.text = "For requesting funds, you need to add banking details. "
                        self.btnGrpAcunt.setTitle("Create an Account", for: .normal)
                        self.btnGrpAcunt.isEnabled = true
                        self.btnGrpAcunt.isHidden = false
                      /*  self.displayAlertChoice(alertStr: "There is no stripe account for this family. You can create a stripe account from settings.", title: "") { (result) in
                            self.typeVew1.isHidden = true
                            self.postVw_height.constant = 75
                            self.btnGrpAcunt.isHidden = true
                            
                            self.lblFamilyCount.isHidden = false
                            self.requestType = ""
                        }*/
                    }
                } catch let err {
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
    //MARK:- Custom Delegate and notifications

    @objc func accountCreateCheck(notification: Notification) {
        if appDel.createAcntFromGrp{
            //            appDel.createAcntFromGrp = false
            getStripeAccountByID()
        }
    }
    
    func itemAdded(itemDic: [String:String]) {
        
        if isItemEdit{
            self.arrayOfItems.replaceObject(at: selectedItem, with: itemDic)
            isItemEdit = false
        }
        else{
            self.arrayOfItems.add(itemDic)
        }
        
        tblItemList.reloadData()
    }
    
    func selectedCallBack(SelectedUserId: NSMutableArray,tempStr:String) {
        print(SelectedUserId)
        if SelectedUserId.count == 0{
            self.txtPostTo.text = ""
            self.requestType = ""
            lblFamilyCount.isHidden = true
        }
        else{
            lblFamilyCount.isHidden = false
            if selectedIds.count == 1{
                if fromGroup{
                    lblFamilyCount.text = "\(appDel.groupNamePublic) is selected"
                }
                else{
                   lblFamilyCount.text = "\(selectedIds.count) family is selected"
                }
            }
            else{
                lblFamilyCount.text = "\(selectedIds.count) families are seleted"
            }
            
        }
        self.selectedIds = SelectedUserId
        
    }
    
    func fundSelectedCallBack(SelectedUserId: NSMutableArray, tempStr: String, fName: String) {
        print(SelectedUserId)
        if SelectedUserId.count == 0{
            self.txtPostTo.text = ""
            self.requestType = ""
            lblFamilyCount.isHidden = true
        }
        else{
            lblFamilyCount.isHidden = false
            if selectedIds.count == 1{
                lblFamilyCount.text = "\(fName) is selected"
            }
            else{
                lblFamilyCount.text = "\(selectedIds.count) families are seleted"
            }
            
        }
        self.selectedIds = SelectedUserId
        if tempStr.isEmpty{
            self.accountVew.isHidden = false
            self.acntNumbrVew.isHidden = true
            self.noAccontVew.isHidden = false
            
            self.lblAcntDetails.text = "For requesting funds, you need to add banking details"
            self.btnGrpAcunt.setTitle("Create an Account", for: .normal)
            self.btnGrpAcunt.isEnabled = true
        }
        else{
            self.accountVew.isHidden = true
            self.acntNumbrVew.isHidden = false
            self.noAccontVew.isHidden = true
            grupAccount = tempStr
            self.lblAcntNumbr.text = self.grupAccount
            //            self.btnGrpAcunt.setTitle(tempStr, for: .normal)
            self.btnGrpAcunt.isEnabled = false
        }
        self.btnGrpAcunt.isHidden = false
    }
    
    //MARK: Custom Date Functions
    func convertDateFormat(date:String)->String{
        do{
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy hh:mm a"
            //        let convertedDate = formatter.date(from: "16-09-2017 2:00 PM")
            if  let convertedDate = try formatter.date(from: date){
                //                formatter.timeZone = TimeZone(identifier: "UTC")
                formatter.timeZone = NSTimeZone.local
                let date = formatter.string(from: convertedDate)
                print(date)
                let formatter2 = DateFormatter()
                formatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
                let convertedDate2 = formatter.date(from: date)
                formatter2.timeZone = NSTimeZone.local
                let date2 = formatter2.string(from: convertedDate2!)
                print(date2)
                
                return date2
            }
            else{
                return ""
            }
            
        }catch{
            return ""
        }
    }
    
    func checkDateHigherThanCurrentDate (selectedDatestring:String)->Bool{
        print(selectedDatestring)
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"
        
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        let convertedDateNow = formatter.string(from: Date())
        let convertedDateNow2 = formatter.date(from: convertedDateNow)
        formatter2.timeZone = NSTimeZone.local
        let dateNow = formatter2.string(from: convertedDateNow2!)
        print(dateNow)
        print(selectedDatestring < dateNow)
        return selectedDatestring < dateNow
    }
    func checkstartDateandEndDate (StartDate:String , endDate:String)->Bool{
        return StartDate > endDate
    }
    
    func convertTimetoTimestamp(date:String)->Int{
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"
        //        let convertedDate = formatter.date(from: "16-09-2017 2:00 PM")
        let convertedDate = formatter.date(from: date)
        
        //        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.timeZone = NSTimeZone.local
        let date = formatter.string(from: convertedDate!)
        print(date)
        let convertedDate2 = formatter.date(from: date)
        
        let timestamp = convertedDate2!.timeIntervalSince1970
        print(timestamp)
        
        return Int(timestamp)
        
    }
    
    //MARK:- Tost Message
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: 20, y: self.view.frame.size.height/2-30, width: self.view.frame.width-40, height: 60))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.numberOfLines = 2
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 10.0)
        toastLabel.adjustsFontSizeToFitWidth = true
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 6.0, delay: 0.3, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension CreateRequestViewController: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        
        guard let location: CLLocation = manager.location else { return }
        
        fetchCityAndCountry(from: location) { city, country, error in
            guard let city = city, let country = country, error == nil else { return }
            
            self.txtLocation.text = "\(city), \(country)"
            self.locationManager.stopUpdatingLocation()
            self.locationManager.delegate = nil
            
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }
    
    func fetchCityAndCountry(from location: CLLocation, completion: @escaping (_ city: String?, _ country:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            completion(placemarks?.first?.locality,
                       placemarks?.first?.country,
                       error)
        }
    }
}

extension CreateRequestViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.arrayOfItems.count == 0{
            self.btnAddMore.isHidden = true
            self.viewAddMore.isHidden = true
            return 1
        }
        else{
            self.btnAddMore.isHidden = false
            self.viewAddMore.isHidden = false
            return arrayOfItems.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.arrayOfItems.count == 0{
            let cellid = "newRequestAddTableViewCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! newRequestAddTableViewCell
            
            return cell
        }
        else{
            let cellid = "RequestItemTableViewCell"
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! RequestItemTableViewCell
            
            let item = self.arrayOfItems[indexPath.row] as! [String:String]
            cell.lblTitle.text = item["request_item_title"]
            cell.lblDesc.text = item["request_item_description"]
            if self.fundType.lowercased() == "general"{
                cell.lblQuantity.text = item["item_quantity"]
                cell.lblFooter.text = "Quantity"
            }
            else{
                cell.lblQuantity.text = "$\(item["item_quantity"] ?? "0")"
                cell.lblFooter.text = "Amount"
            }
            
            
            cell.btnEdit.tag = indexPath.row
            cell.btnDelete.tag = indexPath.row
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if arrayOfItems.count == 0{
            return 250
        }
        else{
            return UITableView.automaticDimension
        }
        
    }
}

extension CreateRequestViewController:SambagDatePickerViewControllerDelegate{
    func didSelectDate(result:String,dateTag:Int){
        if dateTag == 0{
            //            txtStartDate.text = "\(result)"
            selectedStartDate = "\(result)"
            print(selectedStartDate)
            self.timetag = 0
        }
        else{
            //            txtEndDate.text = "\(result)"
            selectedEndDate = "\(result)"
            print(selectedEndDate)
            
            self.timetag = 1
            
        }
        let vc = SambagTimePickerViewController()
        vc.theme = theme
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    func sambagDatePickerDidSet(_ viewController: SambagDatePickerViewController, result: SambagDatePickerResult) {
        if self.dateTag == 0{
            //            txtStartDate.text = "\(result)"
            selectedStartDate = "\(result)"
            //            print(selectedStartDate)
            self.timetag = 0
        }
        else{
            //            txtEndDate.text = "\(result)"
            selectedEndDate = "\(result)"
            //            print(selectedEndDate)
            self.timetag = 1
        }
        viewController.dismiss(animated: true, completion: nil)
        let vc = SambagTimePickerViewController()
        vc.theme = theme
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    func sambagDatePickerDidCancel(_ viewController: SambagDatePickerViewController) {
        if self.dateTag == 0{
            self.AutofillStartDatewithToday()
            
        }
        else{
            txtEndDate.text = ""
            selectedEndDate = ""
        }
        viewController.dismiss(animated: true, completion: nil)
    }
}
extension CreateRequestViewController:SambagTimePickerViewControllerDelegate{
    func sambagTimePickerDidSet(_ viewController: SambagTimePickerViewController, result: SambagTimePickerResult) {
        if self.timetag == 0{
            self.txtEndDate.text = ""
            selectedEndDate = ""
            
            selectedStartDate += "   \(result)"
            self.selectedStartDateFormatted = self.convertDateFormat(date: selectedStartDate)
            print(selectedStartDateFormatted)
            if selectedStartDateFormatted != ""{
                
                txtStartDate.text = Helpers.sambagTimeDisplay(dateAsString:"\(selectedStartDate)")
            }
        }
        else{
            selectedEndDate += "   \(result)"
            self.selectedEndDateFormatted = self.convertDateFormat(date: selectedEndDate)
            if selectedEndDateFormatted != ""{
                if checkstartDateandEndDate(StartDate: selectedStartDateFormatted
                    , endDate: selectedEndDateFormatted){
                    self.showToast(message: "Please Select end date higher than startDate")
                    txtEndDate.text = ""
                    selectedEndDate = ""
                    selectedEndDateFormatted = ""
                }
                else if selectedStartDateFormatted == selectedEndDateFormatted{
                    self.showToast(message: "Please Select different endDate and startDate ")
                    txtEndDate.text = ""
                    selectedEndDate = ""
                    selectedEndDateFormatted = ""
                }
                else{
                    txtEndDate.text = Helpers.sambagTimeDisplay(dateAsString:"\(selectedEndDate)")
                }
            }
        }
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func sambagTimePickerDidCancel(_ viewController: SambagTimePickerViewController) {
        if self.timetag == 0{
            //            txtStartDate.text = ""
            //            selectedStartDate += ""
            self.selectedStartDateFormatted = ""
            self.AutofillStartDatewithToday()
            
        }
        else{
            txtEndDate.text = ""
            selectedEndDate += ""
            self.selectedEndDateFormatted = ""
        }
        viewController.dismiss(animated: true, completion: nil)
        
    }
    
}
extension CreateRequestViewController: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        // Get the place name from 'GMSAutocompleteViewController'
        // Then display the name in textField
        
        print(place.addressComponents!)
  
        
        self.txtLocation.text = "\(place.formattedAddress!)"
        self.selectedLong = place.coordinate.longitude
        self.selectedLat = place.coordinate.latitude

        
        dismiss(animated: true, completion: nil)
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // Handle the error
        print("Error: ", error.localizedDescription)
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        // Dismiss when the user canceled the action
        dismiss(animated: true, completion: nil)
    }
}
