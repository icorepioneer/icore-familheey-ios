//
//  EditAddItemsViewController.swift
//  familheey
//
//  Created by familheey on 01/04/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON

protocol EditYourNeedDelegate: class {
    func itemUpdated(itemDic:NSMutableDictionary)
    func newItemAdded(item:JSON)
}


class EditAddItemsViewController: UIViewController {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var txtDec: UITextView!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var txtQuantity: UITextField!
    @IBOutlet weak var btnAddItem: UIButton!
    
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    
    weak var delegate: EditYourNeedDelegate?
    var dicItem = NSMutableDictionary()
    var isFromEdit = false
    var post_requestId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        appDel.isConversationUpdated = true
        appDel.iscreation = isFromCreation.request
        
        self.lblTitle.attributedText = Helpers.setAttributedPlaceHolder(placeHolderName: "Request Title")
        self.lblQuantity.attributedText = Helpers.setAttributedPlaceHolder(placeHolderName: "Quantity")
        self.navigationController?.navigationBar.isHidden = true
        
        if isFromEdit{
            if let title = dicItem["request_item_title"] {
                txtTitle.text = String(describing: title)
            }
            if let quantity = dicItem["item_quantity"] {
                txtQuantity.text = String(describing: quantity)
            }
            if let desc = dicItem["request_item_description"] {
                txtDec.text = String(describing: desc)
            }
            btnAddItem.setTitle("Update", for: .normal)
        }
        else{
            btnAddItem.setTitle("Add", for: .normal)
        }
    }
    
    //MARK:- Button Actions
    @IBAction func onClickBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickAddorUpdate(_ sender: Any) {
        if txtTitle.text!.isEmpty{
            self.displayAlert(alertStr: "Please enter a title", title: "")
            return
        }
        
        if txtQuantity.text!.isEmpty{
            self.displayAlert(alertStr: "Please enter the item quantity", title: "")
            return
        }
        if isFromEdit{
            editItem()
        }
        else{
            addNewItem()
        }
    }
    
    //MARK:- WEB API
    
    func addNewItem(){
        let param = ["user_id":UserDefaults.standard.value(forKey: "userId")as! String, "post_request_id":post_requestId,"request_item_title":txtTitle.text!,"request_item_description":txtDec.text!,"item_quantity":txtQuantity.text!] as [String:Any]
        print(param)
        ActivityIndicatorView.show("Loading....")
        
        networkProvider.request(.add_newItem(parameter: param), completion: {(result) in
            
            ActivityIndicatorView.hiding()
            switch result {
                
            case .success(let response):
                do {
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    print(response.statusCode)
                    if response.statusCode == 200{
                        appDel.isRequestUpdated = true
                        self.displayAlertChoice(alertStr: "The item is added to your request", title: "") { (result) in
                            self.dicItem.removeAllObjects()
                            let temp = jsonData["data"]
                            print(temp)
                            self.delegate?.newItemAdded(item:temp)
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.addNewItem()
                        }
                    }
                    else{
                        print(jsonData["code"])
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                } catch let err {
                    
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
        })
    }
    
    func editItem(){
        var item_id = ""
        if let id = dicItem["item_id"] {
            item_id = String(describing: id)
        }
        let param = ["user_id":UserDefaults.standard.value(forKey: "userId")as! String, "post_request_id":post_requestId,"request_item_title":txtTitle.text!,"request_item_description":txtDec.text!,"item_quantity":txtQuantity.text!,"id":item_id] as [String:Any]
        print(param)
        ActivityIndicatorView.show("Loading....")
        
        networkProvider.request(.item_edit(parameter: param), completion: {(result) in
            
            ActivityIndicatorView.hiding()
            switch result {
                
            case .success(let response):
                do {
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    print(response.statusCode)
                    if response.statusCode == 200{
                        appDel.isRequestUpdated = true
                        self.displayAlertChoice(alertStr: "The item is updated", title: "") { (result) in
                            self.dicItem.removeAllObjects()
                            let temp = jsonData["data"].dictionaryValue
                            print(temp)
                            let dic = temp as NSDictionary
                            self.dicItem = dic.mutableCopy() as! NSMutableDictionary
                            
                            self.delegate?.itemUpdated(itemDic: self.dicItem)
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.editItem()
                        }
                    }
                    else{
                        print(jsonData["code"])
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                } catch let err {
                    
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
        })
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
