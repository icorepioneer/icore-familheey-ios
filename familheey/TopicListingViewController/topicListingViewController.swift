//
//  topicListingViewController.swift
//  familheey
//
//  Created by familheey on 20/04/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON
import Kingfisher
import SafariServices
import SideMenu

class topicListingViewController: UIViewController, UIGestureRecognizerDelegate {
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearchReset: UIButton!
    @IBOutlet weak var tblListView: UITableView!
    @IBOutlet weak var lblNoTopic: UILabel!
    @IBOutlet weak var noTopicView: UIView!
    @IBOutlet weak var noTopicImgView: UIView!
    @IBOutlet weak var chatStartView: UIView!
    
    
    @IBOutlet weak var lblTopicHead: UILabel!
    @IBOutlet weak var lblFeedHead: UILabel!
    @IBOutlet weak var imgTopicHead: UIImageView!
    @IBOutlet weak var imgFeedHead: UIImageView!

    
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    
    var postoptionsTittle = ["Edit Topics"]
    var ArrayTopics = [JSON]()
    let maxNumberOfLines = 2
    var refreshControl = UIRefreshControl()
    var initiallimit = 1000
    var offset = 0
    var limit = 0
    var selectedIndex = 0
    var stopPagination = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtSearch.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
       
        var btn  = UIButton()

        if selectedIndex == 0{
            btn.tag = 100
            onClickTabChange(btn)
        }
        else{
            btn.tag = 101
            onClickTabChange(btn)
        }
        
        tblListView.tableFooterView =  UIView.init()

//        getTopicsList()
    }
    //MARK:- Custom Methods
    func addGuesture(){
        let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        lpgr.minimumPressDuration = 0.5
        lpgr.delaysTouchesBegan = true
        lpgr.delegate = self
        self.tblListView.addGestureRecognizer(lpgr)
    }
    @objc func handleLongPress(gestureReconizer: UILongPressGestureRecognizer) {
        if selectedIndex == 1{
            return
        }
        
        let touchPoint = gestureReconizer.location(in: self.tblListView)
        if let index = tblListView.indexPathForRow(at: touchPoint){
            print(index.row)
            
            showActionSheet(titleArr: postoptionsTittle as NSArray, title: "") { (result) in
                if result == 100{
                }
                else if result == 0{
                    let post = self.ArrayTopics[index.row]
                    
                    if post["is_accept"].boolValue{
                        let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "EditTopicsViewController") as! EditTopicsViewController
                        //                vc.isFromCreate = true
                        vc.topicId = post["topic_id"].stringValue
                        vc.isFromEdit = true
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    else{
                        self.displayAlert(alertStr: "", title: "You are not part of this conversation yet, please accept first")
                    }
                    
                }
                else{
                    
                }
            }

        }
    }
    //MARK:- Web API
    func getTopicsList(){
        
        ActivityIndicatorView.show("Please wait....")
        self.noTopicView.isHidden = true
        
        if offset == 0{
            ArrayTopics.removeAll()
            ActivityIndicatorView.show("Please wait....")
        }else{
            tblListView.showLoading()
        }
        self.stopPagination = false
        
        let param = ["user_id" : UserDefaults.standard.value(forKey: "userId") as! String,"txt":self.txtSearch.text!] as [String : Any]
        //        let param = ["user_id" : "14"]
        
        networkProvider.request(.topic_list(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    if self.offset == 0{
                        ActivityIndicatorView.hiding()
                    }else{
                        self.tblListView.hideLoading()
                    }
                    
                    let jsonData =  JSON(response.data)
                    
                    if response.statusCode == 200{
                        
                        if let response = jsonData["data"].array{
                            
                            print("Json data reponse contents : \(response.count) Offset = \(self.offset) Limit = \(response)")
                            if response.count == 0{
                                self.stopPagination = true
                            }
                            self.ArrayTopics.append(contentsOf: response)
                            if self.ArrayTopics.count > 0 {
                                self.tblListView.isHidden = false
                                
                                if self.offset == 0{
                                    self.tblListView.delegate = self
                                    self.tblListView.dataSource = self
                                    self.tblListView.reloadData()
                                }
                                if self.offset <= self.limit{
                                    if self.txtSearch.text!.isEmpty{
                                    }
                                    else{
                                    }
                                    
                                }
                            }
                            else{
                                self.tblListView.isHidden = true
                                if self.txtSearch.text!.count > 0{
                                    self.noTopicView.isHidden = false
                                    self.noTopicImgView.isHidden = true
                                    self.lblNoTopic.text = "No result to show"
                                }
                                else{
                                    self.noTopicView.isHidden = false
                                    self.noTopicImgView.isHidden = false
                                    self.lblNoTopic.text = "Let's start a conversation on a topic"
                                }
                            }
                        }
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getTopicsList()
                        }
                    }
                }catch _ {
                    if self.offset == 0{
                        ActivityIndicatorView.hiding()
                    }
                    else{
                        self.tblListView.hideLoading()
                        
                    }
                }
            case .failure( _):
                if self.offset == 0{
                    ActivityIndicatorView.hiding()
                }
                else{
                    self.tblListView.hideLoading()
                }                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    func getFeedTopics(){
        
        ActivityIndicatorView.show("Please wait....")
        self.noTopicView.isHidden = true
        
        if offset == 0{
            ArrayTopics.removeAll()
            ActivityIndicatorView.show("Please wait....")
        }else{
            tblListView.showLoading()
        }
        self.stopPagination = false
        
        let param = ["user_id" : UserDefaults.standard.value(forKey: "userId") as! String,"query":self.txtSearch.text!] as [String : Any]
        //        let param = ["user_id" : "14"]
        print(param)
        
        networkProvider.request(.get_user_commented_post(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    
                    if self.offset == 0{
                        ActivityIndicatorView.hiding()
                    }else{
                        self.tblListView.hideLoading()
                    }
                    
                    let jsonData =  JSON(response.data)
                    print(jsonData)
                    if response.statusCode == 200{
                        
                        if let response = jsonData.array{
                            
                            print("Json data reponse contents : \(response.count) Offset = \(self.offset) Limit = \(response)")
                            
                            if response.count == 0{
                                self.stopPagination = true
                            }
                            self.ArrayTopics.append(contentsOf: response)
                            if self.ArrayTopics.count > 0 {
                                self.tblListView.isHidden = false
                                
                                if self.offset == 0{
                                    self.tblListView.delegate = self
                                    self.tblListView.dataSource = self
                                    self.tblListView.reloadData()
                                }
                                if self.offset <= self.limit{
                                    if self.txtSearch.text!.isEmpty{
                                    }
                                    else{
                                    }
                                    
                                }
                            }
                            else{
                                self.tblListView.isHidden = true
                                if self.txtSearch.text!.count > 0{
                                    self.noTopicView.isHidden = false
                                    self.noTopicImgView.isHidden = true
                                    self.lblNoTopic.text = "No result to show"
                                }
                                else{
                                    self.noTopicView.isHidden = false
                                    self.noTopicImgView.isHidden = false
                                    self.lblNoTopic.text = "Let's start a conversation on a topic"
                                }
                            }
                        }
                    }
                    
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getFeedTopics()
                        }
                    }
                    
                }catch _ {
                    if self.offset == 0{
                        ActivityIndicatorView.hiding()
                    }
                    else{
                        self.tblListView.hideLoading()
                        
                    }
                }
            case .failure( _):
                if self.offset == 0{
                    ActivityIndicatorView.hiding()
                }
                else{
                    self.tblListView.hideLoading()
                }                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    //MARK:- Button Actions
    @IBAction func onClickBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        //        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickSearchAction(_ sender: Any) {
        searchView.isHidden = false
        btnSearchReset.isHidden = false
    }
    @IBAction func onClickSearchReset(_ sender: Any) {
        txtSearch.text = ""
        searchView.isHidden = true
        if selectedIndex == 0{
             getTopicsList()
        }
        else{
            getFeedTopics()
        }
       
        
    }
    
    @IBAction func onClickPostMenu(_ sender: UIButton) {
        let post = ArrayTopics[sender.tag]
        
        showActionSheet(titleArr: self.postoptionsTittle as NSArray, title: "Select Option") { (index) in
            if index == 0{
                if post["is_accept"].boolValue{
                    let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "EditTopicsViewController") as! EditTopicsViewController
                    //                vc.isFromCreate = true
                    vc.topicId = post["topic_id"].stringValue
                    vc.isFromEdit = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else{
                    self.displayAlert(alertStr: "", title: "You are not part of this conversation yet, please accept first")
                }
            }
            else if index == 100{
                
            }
            else{
                
            }
        }
    }
    
    @IBAction func onClickReadmoreAction(_ sender: UIButton) {
            
            let cell = tblListView.cellForRow(at: IndexPath.init(row: sender.tag, section: 0)) as! PostImageTableViewCell
            if cell.labelPost.calculateMaxLines() > maxNumberOfLines &&  cell.labelPost.numberOfLines == maxNumberOfLines{
                
                tblListView.beginUpdates()
                cell.labelPost.numberOfLines = 0
                cell.readmoreButton.setTitle("view less", for: .normal)
                tblListView.endUpdates()
                
                
            }
            else if cell.labelPost.calculateMaxLines() > maxNumberOfLines &&  cell.labelPost.numberOfLines == 0 {
                UIView.performWithoutAnimation {
                    tblListView.beginUpdates()
                    cell.labelPost.numberOfLines = maxNumberOfLines
                    cell.readmoreButton.setTitle("view more", for: .normal)
                    tblListView.endUpdates()
                }
            }
            
        }
        
    
    
    @IBAction func onClickCommentAction(_ sender: UIButton) {
        let data = ArrayTopics[sender.tag]
        
        let storyboard = UIStoryboard.init(name: "Topic", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "TopicsConversationViewController") as! TopicsConversationViewController
        vc.topicID = data["topic_id"].stringValue
        //        vc.Topic = data
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickNewTopic(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "Topic", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CreateTopicViewController") as! CreateTopicViewController
        vc.isFromCreate = true
        //        let navigationController = UINavigationController.init()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func onClickTabChange(_ sender: UIButton) {
        if sender.tag == 100{
            selectedIndex = 0
            offset = 0
            
            lblTopicHead.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            imgTopicHead.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            
            lblFeedHead.textColor = #colorLiteral(red: 0.5176470588, green: 0.5568627451, blue: 0.6, alpha: 1)
            imgFeedHead.backgroundColor = #colorLiteral(red: 0.5176470588, green: 0.5568627451, blue: 0.6, alpha: 1)
            
            chatStartView.isHidden = false
            addGuesture()
            getTopicsList()
        }
        else{
            selectedIndex = 1
            offset = 0
            
            lblFeedHead.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            imgFeedHead.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            
            lblTopicHead.textColor = #colorLiteral(red: 0.5176470588, green: 0.5568627451, blue: 0.6, alpha: 1)
            imgTopicHead.backgroundColor = #colorLiteral(red: 0.5176470588, green: 0.5568627451, blue: 0.6, alpha: 1)
            
            chatStartView.isHidden = true
//            for recognizer in tblListView.gestureRecognizers ?? [] {
//                tblListView.removeGestureRecognizer(recognizer)
//            }
            getFeedTopics()
        }
    }
    
    
}
extension topicListingViewController : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ArrayTopics.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard ArrayTopics.count != 0 else {
            return UITableViewCell.init()
        }
        
        let post = ArrayTopics[indexPath.row]
        
        if selectedIndex == 0{
            let cellid = "topicListTableViewCell"
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! topicListTableViewCell
            
            if let users = post["to_users"].array, users.count > 0{
                if users.count == 1{
                    cell.lblTopicUsers.text = users[0].string ?? ""
                }
                else{
                    cell.lblTopicUsers.text = "\(users[0].string ?? "") and \(users.count - 1) others"
                }
            }
            else{
                cell.lblTopicUsers.text = ""
            }
            
            cell.lblDate.text = ConversationsViewController.formatDateString(dateStr: post["created_at"].stringValue)
            //timeAgoSinceDate(dateStr: post["created_at"].stringValue)
            
            //Post
            cell.lblTopicTitle.text = post["title"].string ?? ""
            
            //Details
            //        cell.lableNumberOfConversations.text = post["conversation_count"].stringValue
            if post["conversation_count_new"].intValue > 0{
                cell.lblNewChatCount.isHidden = false
                cell.lblNewChatCount.text = "\(post["conversation_count_new"].intValue)"
            }
            else{
                cell.lblNewChatCount.isHidden = true
            }
            
            if !post["conversation_count"].stringValue.isEmpty{
                cell.lblTotalMessages.text = "\(post["conversation_count"].intValue)"
            }
            else{
               cell.lblTotalMessages.text = "0"
            }
            
            return cell
        }
        else{
            let cellid = "topicFeedListTableViewCell"
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! topicFeedListTableViewCell
            cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: UIScreen.main.bounds.width)
            
            cell.lblTopicDescription.text = post["snap_description"].string ?? ""
            cell.lblFamilyName.text =  post["group_name"].string ?? "Public"
            cell.lblLastMsgFrm.text = post["last_commented_user"].string ?? ""
            
            cell.lblDate.text = ConversationsViewController.formatDateString(dateStr: post["last_commented_date"].stringValue)
            
            if let Attachment = post["last_comment_attachment"].array, Attachment.count != 0{
                let attachment = Attachment[0]
                cell.lblLastMsg.isHidden = true
                cell.attachmentView.isHidden = false
                if let type = attachment["type"].string ,type.contains("audio"){
                    cell.imgAttachment.image = #imageLiteral(resourceName: "mic_gray")
                    cell.lblAttachmentType.text = "Audio"
                }
     
                else{
                    cell.imgAttachment.image = #imageLiteral(resourceName: "attachmentIcon")
                    cell.lblAttachmentType.text = "Attachment"
                }
            }
            else{
                cell.attachmentView.isHidden = true
                cell.lblLastMsg.isHidden = false
                cell.lblLastMsg.text = ": \(post["last_comment"].string ?? "")"
            }
            
            if let attachmentArr = post["post_attachment"].array, attachmentArr.count != 0{
                let data = attachmentArr[0]
                if let type = data["type"].string, type.contains("audio"){
                }
                else if let type = data["type"].string ,type.contains("image"){
                    var  temp  = ""
                    if let publishType = post["publish_type"].string, publishType.lowercased() == "albums" || publishType.lowercased() == "album"{
                        temp = "\(Helpers.imageURl)"+"\(BaseUrl.document_image)"+data["filename"].stringValue.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                    }
                    else{
                        temp = "\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+data["filename"].stringValue.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                    }
                    
                    
                    var imgurl = URL(string:Helpers.imaginaryImageBaseUrl+"width=200&height=200&url=" + temp )
                    if type.lowercased().contains("gif"){
                        imgurl = URL(string: temp )
                    }
                    DispatchQueue.main.async {
                        cell.imgFamilyLoogo.kf.indicatorType = .activity
                        cell.imgFamilyLoogo.kf.setImage(with: imgurl, placeholder:nil, options: nil)
                    }
                }
                else if let type = data["type"].string ,type.contains("video"){
                    var temp = ""
                    //"\(Helpers.imageURl)"+"default_video.jpg"
                    
                    if let thumb = data["video_thumb"].string, !thumb.isEmpty {
                        temp = "\(Helpers.imageURl)"+thumb
                        let imgurl = URL(string: Helpers.imaginaryImageBaseUrl+"width=250&height=250&url=" + temp )
                        DispatchQueue.main.async {
                            cell.imgFamilyLoogo.kf.indicatorType = .activity
                            cell.imgFamilyLoogo.kf.setImage(with: imgurl, placeholder:nil, options: nil)
                        }
                    }
                }
                else{
                    cell.imgFamilyLoogo.image = #imageLiteral(resourceName: "Family Logo")
                }
            }
            else{
                cell.imgFamilyLoogo.image = #imageLiteral(resourceName: "Family Logo")
            }
            
            if post["unread_conversation_count"].intValue > 0{
                cell.newChatView.isHidden = false
                cell.lblNewChatCount.text = "\(post["unread_conversation_count"].intValue)"
            }
            else{
                cell.newChatView.isHidden = true
            }
            
            if !post["total_comments"].stringValue.isEmpty{
                cell.lblTotalConversations.text = post["total_comments"].stringValue
            }
            else{
               cell.lblTotalConversations.text = "0"
            }
            
            if let arr = post["commented_user_count"].string, !arr.isEmpty{
                cell.lblTotalPeole.text = arr
            }
            else{
                cell.lblTotalPeole.text = "0"
            }
            
           let fLogo = post["logo"].stringValue
            
            if fLogo != ""{
                let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+fLogo.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                let imgUrl = URL(string: temp)
                cell.imgFamilyLoogo.kf.indicatorType = .activity
                
                cell.imgFamilyLoogo.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Family Logo"), options: nil, progressBlock: nil, completionHandler: nil)
                
            }
            else{

            }
            
            return cell
        }

        
       /* cell.readmoreButton.tag = indexPath.row
        cell.buttonShare.tag = indexPath.row
        cell.buttonRightMenu.tag = indexPath.row
        cell.commentsButton.tag = indexPath.row
        cell.viewsButton.tag = indexPath.row
        cell.sharedUsersButton.tag = indexPath.row
        cell.buttonComments.tag = indexPath.row
        //        cell.btnFamilyClick.tag = indexPath.row
        
        //Active label color
        cell.labelPost.mentionColor = .black
        cell.labelPost.hashtagColor = .black
        cell.labelPost.URLColor = .black
        
        //User Details
        if let users = post["to_users"].array, users.count > 0{
            if users.count == 1{
                cell.labelUserName.text = users[0].string ?? ""
            }
            else if users.count == 2{
                cell.labelUserName.text = "\(users[0].string ?? ""), \(users[1].string ?? "")"
            }
            else{
                cell.labelUserName.text = "\(users[0].string ?? ""), \(users[1].string ?? "") and \(users.count - 2) others"
            }
        }
        else{
            cell.labelUserName.text = ""
        }
        cell.labelDate.text = ConversationsViewController.formatDateString(dateStr: post["created_at"].stringValue)
        //timeAgoSinceDate(dateStr: post["created_at"].stringValue)
        //Post
        cell.labelPost.text = post["title"].string ?? ""
        
        //Details
        cell.lableNumberOfConversations.text = post["conversation_count"].stringValue
        if post["conversation_count_new"].intValue > 0{
            cell.lblNewConvrstn.isHidden = false
        }
        else{
            cell.lblNewConvrstn.isHidden = true
        }
        if let Attachment = post["topic_attachment"].array, Attachment.count != 0{
            print("updating slide........")
            print(indexPath.row)
            cell.updateSlider(data: Attachment)
        }
        else{
            print("No Slide........")
            print(indexPath.row)
            cell.viewPostBg.isHidden = true
            cell.viewPageControllBG.isHidden = true
        }
        if cell.labelPost.calculateMaxLines() > maxNumberOfLines{
            cell.labelPost.numberOfLines = maxNumberOfLines
            cell.readMoreView.isHidden = false
            
            //            tableView.layoutIfNeeded()
        }
        else{
            cell.labelPost.numberOfLines = 0
            cell.readMoreView.isHidden = true
            //            tableView.layoutIfNeeded()
        }
        //        cell.delegate = self
        
       /* cell.labelPost.handleHashtagTap { hashtag in
            print("Success. You just tapped the \(hashtag) hashtag")
            self.txtSearch.text  = "#"+hashtag
            //            self.openSearch()
            
            self.txtSearch.becomeFirstResponder()
            
        }
        cell.labelPost.handleMentionTap { (mension) in
            print("Success. You just tapped the \(mension) mension")
            self.txtSearch.text  = mension
            
            //            self.openSearch()
            self.txtSearch.becomeFirstResponder()
            
        }
        cell.labelPost.handleURLTap { (url) in
            print(url)
            //                    UIApplication.shared.open(url)
            let vc = SFSafariViewController(url: url)
            self.present(vc, animated: true, completion: nil)
        }*/
        */
      
        /* let proPic = post["propic"].string ?? ""
         
         DispatchQueue.main.async {
         
         if proPic.count != 0{
         let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic
         let imgUrl = URL(string: temp)
         cell.imageViewProfilePic.kf.indicatorType = .activity
         
         cell.imageViewProfilePic.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
         }
         else{
         cell.imageViewProfilePic.image = #imageLiteral(resourceName: "Male Colored")
         }
         }
         cell.imageViewProfilePic.isTappable(id: post["created_by"].intValue)*/
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = ArrayTopics[indexPath.row]
        
        if selectedIndex == 0{
            let storyboard = UIStoryboard.init(name: "Topic", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "TopicsConversationViewController") as! TopicsConversationViewController
            vc.topicID = data["topic_id"].stringValue
            //        vc.Topic = data
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
//            let post = ArrayPosts[sender.tag]
            let mainStoryBoard = UIStoryboard(name: "Conversations", bundle: nil)
            let vc = mainStoryBoard.instantiateViewController(withIdentifier: "ConversationsViewController") as! ConversationsViewController
            vc.post = data
            vc.conversationT = .post
            self.navigationController?.pushViewController(vc, animated:true)
        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
extension topicListingViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        //        if textField == txtSearch
        //        {
        //            // selectWebAPI()
        //            if textField.text!.count > 0{
        //                btnResetSearch.isHidden = false
        //            }
        //            else{
        //                btnResetSearch.isHidden = true
        //            }
        textField.endEditing(true)
        //        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        self.view.endEditing(true)
        if textField == txtSearch
        {
            //            if textField.text!.count > 0{
            //                btnResetSearch.isHidden = false
            //                if textField.text!.trimmingCharacters(in: .whitespaces).isEmpty {
            //                                  txtSearch.text = ""
            //                    btnResetSearch.isHidden = true
            //
            //                                    }
            //            }
            //            else{
            //                btnResetSearch.isHidden = true
            //            }
            self.offset = 0
            if selectedIndex == 0{
               getTopicsList()
            }
            else{
               getFeedTopics()
            }
            
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //        if textField.text!.count > 0 {
        //            btnResetSearch.isHidden = false
        //        }
        //        else{
        //            btnResetSearch.isHidden = true
        //            // selectWebAPI()
        //        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //        if textField.text!.count > 0 {
        //            btnResetSearch.isHidden = false
        //        }
        //        else{
        //            btnResetSearch.isHidden = true
        //            // selectWebAPI()
        //        }
        return true
    }
}
