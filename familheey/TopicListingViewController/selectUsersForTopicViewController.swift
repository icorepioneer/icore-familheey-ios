//
//  selectUsersForTopicViewController.swift
//  familheey
//
//  Created by familheey on 20/04/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit
import Moya
import Alamofire
import SwiftyJSON

protocol SelectedUsersForTopicsDelegate : class{
    func selectedUserCallBack(SelectedUserId:NSMutableArray)
}

class selectUsersForTopicViewController: UIViewController {
    @IBOutlet weak var tblListView: UITableView!
    @IBOutlet weak var noDateView: UIView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearchReset: UIButton!
    
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    var peopleListArr = [JSON]()
    var topicUser = [JSON]()
    var myConnection = [JSON]()
    var nonConnection = [JSON]()
    var selectedUserIDArr  = [String]()
    var topic_id = ""
    var isFromConverSation = false
    var isUserActive = true
    
    weak var delegate: SelectedUsersForTopicsDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = true
        
        let headerNib = UINib.init(nibName: "FamilyListingView", bundle: Bundle.main)
        tblListView.register(headerNib, forHeaderFooterViewReuseIdentifier: "FamilyListingView")
        txtSearch.delegate = self
        
       tblListView.tableFooterView =  UIView.init()
        
        callGroupMembers()
    }
    
    
    //MARK:- Web API
    func callGroupMembers(){
        
        ActivityIndicatorView.show("Loading....")
        
        //    let userId = UserDefaults.standard.value(forKey: "userId") as! Int
        
        let parameter = ["user_id" : "\(UserDefaults.standard.value(forKey: "userId") as! String)","topic_id":topic_id,"txt":txtSearch.text!]
        
        print("param : \(parameter)")
        ActivityIndicatorView.show("Loading...")
        
        networkProvider.request(.topic_users_list(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    let jsonData = JSON(response.data)
                    
                    if response.statusCode == 200
                    {
                        self.peopleListArr.removeAll()
                        
                        self.topicUser = jsonData["topicUsers"].arrayValue
                        self.myConnection = jsonData["connectionUsers"].arrayValue
                        self.nonConnection   = jsonData["nonConnectionUsers"].arrayValue
                        
                        self.peopleListArr.append(contentsOf: self.topicUser)
                        self.peopleListArr.append(contentsOf: self.myConnection)
                        self.peopleListArr.append(contentsOf: self.nonConnection)
                        
                        if self.topicUser.count > 0{
                            for index in self.topicUser{
                                let userId = index["user_id"].stringValue
                                self.selectedUserIDArr.append(userId)
                            }
                        }
                        
                        print(self.peopleListArr)
                        
                        if self.peopleListArr.count > 0{
                            self.tblListView.delegate = self
                            self.tblListView.dataSource = self
                            
                            self.noDateView.isHidden = true
                            
                            self.tblListView.isHidden = false
                            
                            self.tblListView.reloadData()
                        }
                        else{
                            self.tblListView.isHidden = true
                            
                            self.noDateView.isHidden = false
                            
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.callGroupMembers()
                        }
                    }
                    else{
                        self.tblListView.isHidden = true
                        self.noDateView.isHidden = false
                    }
                } catch let err {
                    
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
    func updateUsersinTopic(){
        
        ActivityIndicatorView.show("Loading....")
        
        //    let userId = UserDefaults.standard.value(forKey: "userId") as! Int
//        var users = selectedUserIDArr
        let users = selectedUserIDArr.compactMap { Int($0) }
//        users.append("\(UserDefaults.standard.value(forKey: "userId") as! String)")
        let parameter = ["updated_by" : "\(UserDefaults.standard.value(forKey: "userId") as! String)","topic_id":topic_id,"to_user_id":users] as [String : Any]
        
        print("param : \(parameter)")
        ActivityIndicatorView.show("Loading...")
        
        networkProvider.request(.update_topic(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    _ = JSON(response.data)
                    
                    if response.statusCode == 200{
                        let alert = UIAlertController(title: "", message: "Users added to the conversation successfully", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ -> Void in
                            self.navigationController?.popViewController(animated: true)
                            
                        }))
                        
                        self.present(alert, animated: true, completion: nil)
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.updateUsersinTopic()
                        }
                    }
                    else
                    {
                        
                    }
                } catch let err {
                    
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
    //MARK:- Custom Methods
    @IBAction func inviteFamilySelection(sender: UIButton){
        if selectedUserIDArr.contains(self.peopleListArr[sender.tag]["user_id"].stringValue){
            if self.topicUser.count > sender.tag{
                return
            }
            selectedUserIDArr.remove(at: selectedUserIDArr.firstIndex(of: (self.peopleListArr[sender.tag]["user_id"].stringValue))!)
        }else{
            selectedUserIDArr.append(self.peopleListArr[sender.tag]["user_id"].stringValue)
        }
        tblListView.reloadSections(IndexSet(integer : sender.tag), with: .none)
    }
    
    //MARK:- Button actions
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickDone(_ sender: Any) {
        if selectedUserIDArr.count > 0{
            if isFromConverSation{
                updateUsersinTopic()
            }
            else{
                let tempArr = selectedUserIDArr as NSArray
                let tempMutable = tempArr.mutableCopy() as! NSMutableArray
                
                self.delegate?.selectedUserCallBack(SelectedUserId: tempMutable )
                self.navigationController?.popViewController(animated: true)
            }
        }
        else{
            self.displayAlert(alertStr: "Please select atleast one user", title: "")
        }
    }
    
    @IBAction func onClickSearch(_ sender: Any) {
        searchView.isHidden = false
        btnSearchReset.isHidden = false
    }
    
    @IBAction func onClickSearchReset(_ sender: Any) {
        searchView.isHidden = true
        btnSearchReset.isHidden = false
        txtSearch.text = ""
        callGroupMembers()
    }
    
}

extension selectUsersForTopicViewController : UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return peopleListArr.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header = self.tblListView.dequeueReusableHeaderFooterView(withIdentifier: "FamilyListingView") as! FamilyListingView
        
        header.btnAction.tag       = section
        header.btnView.tag         = section
        header.btnView.addTarget(self, action: #selector(inviteFamilySelection(sender:)), for: .touchUpInside)
        header.btnAction.addTarget(self, action: #selector(inviteFamilySelection(sender:)), for: .touchUpInside)
        
        header.byTitle.text = ""
        header.lblType.text = ""//FamilyArr.familyList![section].faType
        header.lblType.isHidden = true
        header.lblCreatedBy.isHidden = true
        header.lblKnownHead.isHidden = true
        header.lblMemberHead.isHidden = true
        header.lblCreatedBy.text = ""//FamilyArr.familyList![section].createdByName
        header.lblMembersCount.text = ""//FamilyArr.familyList![section].memberCount
        header.lblKnown.text = ""//FamilyArr.familyList![section].knownCount
      
        
        
        header.lblTitle.text = self.peopleListArr[section]["full_name"].stringValue.firstUppercased
         
         //            header.lblCreated_height.constant = 0
         header.lblRegion.text = self.peopleListArr[section]["location"].stringValue.firstUppercased
         
         
//         let test = self.peopleListArr.arrayValue[section]["invitation_status"].intValue

        if selectedUserIDArr.contains(self.peopleListArr[section]["user_id"].stringValue){
            header.btnAction.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
            header.btnAction.setTitle("Selected", for: .normal)
            
        }else{
            
            header.btnAction.isEnabled = true
            header.btnView.isEnabled = true
            header.btnAction.titleLabel?.font = UIFont.systemFont(ofSize: 15)
            header.btnAction.setTitle("Select", for: .normal)
            
        }
         
         let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+self.peopleListArr[section]["propic"].stringValue
         let imgurl = URL(string: temp)
         header.imgLogo.kf.indicatorType = .activity
         
         header.imgLogo.kf.setImage(with: imgurl, placeholder: #imageLiteral(resourceName: "Male Colored") , options: nil, progressBlock: nil, completionHandler: nil)
        
        if !isUserActive{
            header.btnAction.isEnabled = false
        }
        
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 110
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
}
extension selectUsersForTopicViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        //        if textField == txtSearch
        //        {
        //            // selectWebAPI()
        //            if textField.text!.count > 0{
        //                btnResetSearch.isHidden = false
        //            }
        //            else{
        //                btnResetSearch.isHidden = true
        //            }
        textField.endEditing(true)
        //        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        self.view.endEditing(true)
        if textField == txtSearch
        {
            //            if textField.text!.count > 0{
            //                btnResetSearch.isHidden = false
            //                if textField.text!.trimmingCharacters(in: .whitespaces).isEmpty {
            //                                  txtSearch.text = ""
            //                    btnResetSearch.isHidden = true
            //
            //                                    }
            //            }
            //            else{
            //                btnResetSearch.isHidden = true
            //            }
            self.callGroupMembers()
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //        if textField.text!.count > 0 {
        //            btnResetSearch.isHidden = false
        //        }
        //        else{
        //            btnResetSearch.isHidden = true
        //            // selectWebAPI()
        //        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //        if textField.text!.count > 0 {
        //            btnResetSearch.isHidden = false
        //        }
        //        else{
        //            btnResetSearch.isHidden = true
        //            // selectWebAPI()
        //        }
        return true
    }
}
