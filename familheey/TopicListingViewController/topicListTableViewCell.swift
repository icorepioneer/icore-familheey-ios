//
//  topicListTableViewCell.swift
//  familheey
//
//  Created by familheey on 27/08/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit

class topicListTableViewCell: UITableViewCell {
    @IBOutlet weak var lblTopicTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTopicUsers: UILabel!
    @IBOutlet weak var lblNewChatCount: UILabel!
    @IBOutlet weak var lblTotalMessages: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class topicFeedListTableViewCell: UITableViewCell{
    
    @IBOutlet weak var imgFamilyLoogo: UIImageView!
    @IBOutlet weak var lblTopicDescription: UILabel!
    @IBOutlet weak var lblFamilyName: UILabel!
    @IBOutlet weak var lblLastMsgFrm: UILabel!
    @IBOutlet weak var lblLastMsg: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblNewChatCount: UILabel!
    @IBOutlet weak var newChatView: UIView!
    
    @IBOutlet weak var attachmentView: UIView!
    @IBOutlet weak var imgAttachment: UIImageView!
    @IBOutlet weak var lblAttachmentType: UILabel!
    @IBOutlet weak var lblTotalConversations: UILabel!
    @IBOutlet weak var lblTotalPeole: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
