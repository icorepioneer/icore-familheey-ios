//
//  CalendarEventsViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 30/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import KDCalendar
import Alamofire
import Moya
import SwiftyJSON
import DropDown
protocol calenderselectionDelegate {
    func didSelectDate(result:String,dateTag:Int)
}

class CalendarEventsViewController: UIViewController, CalendarViewDataSource, CalendarViewDelegate, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, selectFamilyDelegate,DatePickerPopupDelegate {
    
    
    var dateTag:Int!
    
    @IBOutlet weak var filterView: UIView!
    var isCreateEvent = false
    var didselct = false
    var dateCallBackDelegate:calenderselectionDelegate!
    private var networkProvider                     = MoyaProvider<FamilyheeyApi>()
    @IBOutlet weak var calendarVw                   : CalendarView!
    var isFromGroup                                 = false
    var groupID                                     = ""
    var isTypeFilter = "-1"
    var isPublic = false
    var isPrivate = false
    var isFromDateSelection = false
    var selectedDate = Date()
    var selectedMnt = ""
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearchReset: UIButton!
    
    @IBOutlet weak var imgPublic: UIImageView!
    @IBOutlet weak var imgPrivate: UIImageView!
    @IBOutlet weak var imgSelectFamily: UIImageView!
    @IBOutlet weak var lblFamilyName: UILabel!
    
    
    
    let apiUrl                                      = "https://api.dev.fabstyle.fashion/common-listings/dev/getCalendarData"
    @IBOutlet var tbl                               : UITableView!
    
    @IBOutlet weak var yearTbl: UITableView!
    
    var eventArr                                    = NSMutableArray()
    var eventDateArr                                = NSMutableArray()
    var isFromExplore                               = ""
    var exploreFD                                   = Date()
    var exploreTD                                   = Date()
    let dropDown                                    = DropDown()
    let yearArr                                     = [2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030]
    
    //    @IBOutlet weak var backWhiteVwHeightConstrain   : NSLayoutConstraint!
    
    @IBOutlet weak var topVwTopConstain             : NSLayoutConstraint!
    
    var dataArr                                        = [JSON]()
    var sharedArr                                      = [JSON]()
    var eventIds = [String]()
    var eventLocation = [String]()
    var eventPic = [String]()
    var evetCreatedBy = [String]()
    var eventType = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Helpers.setleftView(textfield: txtSearch, customWidth: 30)
        txtSearch.delegate = self
        
        if DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_XP{
            topVwTopConstain.constant    = 20
        }else{
            
            topVwTopConstain.constant    = 20
        }
        yearTbl.isHidden                 = true
        
        //        backWhiteVwHeightConstrain.constant                  = self.view.frame.height
        tbl.isHidden                                         = false
        //KDCalendar
        CalendarView.Style.cellShape                         = .bevel(9.0)
        CalendarView.Style.cellColorDefault                  = UIColor.clear
        if isCreateEvent{
        }
        else{
            
        }
        CalendarView.Style.cellColorToday                    = UIColor(red:1.00, green:0.84, blue:0.64, alpha:1.00)
        CalendarView.Style.cellSelectedBorderColor           = #colorLiteral(red: 0.4941176471, green: 0.3411764706, blue: 0.7607843137, alpha: 1) //UIColor(red:1.00, green:0.63, blue:0.24, alpha:1.00)
        CalendarView.Style.cellEventColor                    = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1) //UIColor(red:1.00, green:0.63, blue:0.24, alpha:1.00)
        CalendarView.Style.headerTextColor                   = UIColor.darkGray
        CalendarView.Style.cellTextColorDefault              = UIColor.lightGray
        CalendarView.Style.firstWeekday                      = .sunday
        calendarVw.dataSource                                = self
        calendarVw.delegate                                  = self
        calendarVw.direction                                 = .horizontal
        CalendarView.Style.cellSelectedColor                 = #colorLiteral(red: 0.4941176471, green: 0.3411764706, blue: 0.7607843137, alpha: 1)
        CalendarView.Style.cellColorOutOfRange               = UIColor.lightGray.withAlphaComponent(0.4)
        
        if isFromExplore == "explore"{
            
            calendarVw.multipleSelectionEnable              = false
        }else{
            
            calendarVw.multipleSelectionEnable              = false
        }
        //        calendarVw.multipleSelectionEnable              = false
        
        calendarVw.marksWeekends                             = false
        calendarVw.backgroundColor                           = .white
        
        CalendarView.Style.cellTextColorWeekend              = .black
        //        CalendarView.Style.cellTextColorToday                = .black
        //        CalendarView.Style.cellColorToday                    = .green
        
        CalendarView.Style.cellSelectedTextColor             = .white//KDCalendar
        CalendarView.Style.cellShape                         = .bevel(9.0)
        CalendarView.Style.cellColorDefault                  = UIColor.clear
        if isCreateEvent{
        }
        else{
            
        }
        CalendarView.Style.cellColorToday                    = UIColor(red:1.00, green:0.84, blue:0.64, alpha:1.00)
        CalendarView.Style.cellSelectedBorderColor           = #colorLiteral(red: 0.4941176471, green: 0.3411764706, blue: 0.7607843137, alpha: 1) //UIColor(red:1.00, green:0.63, blue:0.24, alpha:1.00)
        CalendarView.Style.cellEventColor                    = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1) //UIColor(red:1.00, green:0.63, blue:0.24, alpha:1.00)
        CalendarView.Style.headerTextColor                   = UIColor.darkGray
        CalendarView.Style.cellTextColorDefault              = UIColor.lightGray
        CalendarView.Style.firstWeekday                      = .sunday
        calendarVw.dataSource                                = self
        calendarVw.delegate                                  = self
        calendarVw.direction                                 = .horizontal
        
        CalendarView.Style.cellSelectedColor                 = #colorLiteral(red: 0.4941176471, green: 0.3411764706, blue: 0.7607843137, alpha: 1)
        
        if isFromExplore == "explore"{
            
            calendarVw.multipleSelectionEnable              = false
        }else{
            
            calendarVw.multipleSelectionEnable              = false
        }
        //        calendarVw.multipleSelectionEnable              = false
        
        calendarVw.marksWeekends                             = false
        calendarVw.backgroundColor                           = .white
        
        CalendarView.Style.cellTextColorWeekend              = .black
        CalendarView.Style.cellTextColorToday                = .lightGray
        //        CalendarView.Style.cellColorToday                    = .green
        
        CalendarView.Style.cellSelectedTextColor             = .white
        
        
        isPublic = true
        imgPublic.image = #imageLiteral(resourceName: "Green_tick.png")
        isPrivate = true
        imgPrivate.image = #imageLiteral(resourceName: "Green_tick.png")
        isTypeFilter = "-1"
        if isFromGroup{
            fetchGroupCalendar()
        }
        else{
            fetchCalendar()
        }
        
        
        
        if isCreateEvent{
            self.filterView.isHidden = true
            didselct = true
        }
        else{
            self.filterView.isHidden = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        tbl.delegate = self
        tbl.dataSource = self
        let today                     = Date()
        var tomorrowComponents        = DateComponents()
        tomorrowComponents.day        = 1
        
        
        let calendar                  = Calendar.current
        let componetsThatWeWant       = calendar.dateComponents([.year, .day, .month], from: today)
        
        let day                       = componetsThatWeWant.day
        let month                     = componetsThatWeWant.month
        let year                      = componetsThatWeWant.year
        
        var tempStr = ""
        if year! > 0 && month! > 0 && day! > 0{
            tempStr = "\(year!)-\(month!)-\(day!)"
        }
        
        let formatter1                = DateFormatter()
        formatter1.dateFormat         = "yyyy-MM-dd"
        formatter1.timeZone           = NSTimeZone(name: "UTC") as TimeZone?
        formatter1.locale             = .current
        let tDates                    = formatter1.date(from: tempStr)
        
        if isCreateEvent{
            self.calendarVw.setDisplayDate(tDates ?? today)
            self.calendarVw.selectDate(tDates ?? today)
        }
        else{
            
            if appDel.isBackToCalendar == false{
                
                
                
                self.calendarVw.selectDate(tDates ?? today)
                self.calendarVw.setDisplayDate(tDates ?? today)
                
                eventArr.removeAllObjects()
                eventDateArr.removeAllObjects()
                eventIds.removeAll()
                eventLocation.removeAll()
                eventPic.removeAll()
                evetCreatedBy.removeAll()
                eventType.removeAll()
                
                tbl.delegate = self
                tbl.dataSource = self
                tbl.reloadData()
                
            }
            
            appDel.isBackToCalendar = false
            
        }
        
        
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        formatter.locale = .current
        formatter.dateFormat = "MMMM yyyy"
        let result = formatter.string(from: tDates ?? today)
        print("result : \(result)")
        self.selectedMnt = "\(result)"
        tbl.reloadData()
        yearTbl.reloadData()
        //loadCalendarEvents()
        
        // getCalendarData(userID: "15")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
    }
    
    //MARK: - Fetch Calendar
    
    func fetchCalendar(){
        eventArr.removeAllObjects()
        eventDateArr.removeAllObjects()
        eventIds.removeAll()
        eventLocation.removeAll()
        eventPic.removeAll()
        evetCreatedBy.removeAll()
        eventType.removeAll()
        tbl.reloadData()
        
        //        if selectedDate != nil{
        //            self.calendarVw.deselectDate(selectedDate)
        //        }
        
        var previousMonth = Date()
        var nextYear = Date()
        
        var dateComponents = DateComponents()
        dateComponents.year = 2
        var dateComponents1 = DateComponents()
        dateComponents1.month = -1
        
        if isFromDateSelection{
            print(selectedDate)
            isFromDateSelection = false
            
            nextYear = Calendar.current.date(byAdding: dateComponents, to: selectedDate)!
            previousMonth = Calendar.current.date(byAdding: dateComponents1, to: selectedDate)!
        }
        else{
            //            var dateComponents = DateComponents()
            //            dateComponents.year = 2
            let today = Date()
            
            nextYear = Calendar.current.date(byAdding: dateComponents, to: today) ?? today
            previousMonth = Calendar.current.date(byAdding: dateComponents1, to: today) ?? today
        }
        
        
        print("###### \(isTypeFilter)")
        
        let parameter = ["from_date" : "\(convertTimetoTimestamp(date: previousMonth))", "to_date" : "\("\(convertTimetoTimestamp(date: nextYear))")", "user_id" : UserDefaults.standard.value(forKey: "userId") as! String, "query":txtSearch.text!,"type":isTypeFilter] as [String : Any]
        print("Param : \(parameter)")
        
        ActivityIndicatorView.show("Loading....")
        //        tbl.isHidden = true
        networkProvider.request(.fetch_calender(parameter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    if response.statusCode == 200{
                        print("Calendar : \(jsonData)")
                        
                        if jsonData.count > 0{
                            
                            let values = jsonData["data"]
                            
                            self.calendarVw.events.removeAll()
                            for i in 0 ..< values.count{
                                
                                let datVal = self.convertTimeStamptoString(val: values[i]["from_date"].doubleValue).components(separatedBy: " ").first!
                                
                                let toDatVal = self.convertTimeStamptoString(val: values[i]["to_date"].doubleValue).components(separatedBy: " ").first!
                                
                                let fromDate = self.convertStringToDate(dateStr: datVal)
                                let toDate = self.convertStringToDate(dateStr: toDatVal)
                                
                                print("######\(fromDate) #### \(toDate)")
                                
                                var datesBetweenArray = Date.datesArr(from: fromDate, to: toDate)
                                print("###\(datesBetweenArray)")
                                
                                let eventDate = "\(self.convertTimeStamp(timestamp: values[i]["from_date"].stringValue))-\(self.convertTimeStamp(timestamp: values[i]["to_date"].stringValue))"
                                var publicEve = ""
                                if values[i]["is_public"].boolValue{
                                    publicEve = "Public event"
                                }
                                else{
                                    publicEve = "Private event"
                                }
                                
                                let eventName = values[i]["event_name"].stringValue+"#"+eventDate+"#"+values[i]["event_id"].stringValue+"#"+values[i]["event_image"].stringValue+"#"+values[i]["location"].stringValue+"#"+values[i]["created_by_name"].stringValue+"#"+publicEve
                                
                                let today                     = Date()
                                
                                let calendar                  = Calendar.current
                                let componetsThatWeWant       = calendar.dateComponents([.year, .day, .month], from: today)
                                
                                let day                       = componetsThatWeWant.day
                                let month                     = componetsThatWeWant.month
                                let year                      = componetsThatWeWant.year
                                
                                var tempStr = ""
                                if year! > 0 && month! > 0 && day! > 0{
                                    tempStr = "\(year!)-\(month!)-\(day!)"
                                }
                                
                                let formatter1                = DateFormatter()
                                formatter1.dateFormat         = "yyyy-MM-dd"
                                formatter1.timeZone           = NSTimeZone(name: "UTC") as TimeZone?
                                formatter1.locale             = .current
                                let tDates                    = formatter1.date(from: tempStr)
                                
                                let todayDateStr              = "\(tDates!)".components(separatedBy: " ").first!
                                
                                for j in 0 ..< datesBetweenArray.count{
                                    
                                    let str = self.convertDateToString(currentDate: datesBetweenArray[j]).components(separatedBy: " ").first!
                                    
                                    self.calendarVw.addEventOnlyCollection(eventName, date: str.toDate()!)
                                    
                                    if str == todayDateStr{
                                        if !self.isCreateEvent{
                                            
                                            self.eventDateArr.add(str.toDate()!)
                                            self.eventArr.add(eventName)
                                        }
                                        
                                        
                                    }
                                }
                            }
                        }
                        else{
                            self.eventArr.removeAllObjects()
                            self.eventDateArr.removeAllObjects()
                            self.eventIds.removeAll()
                            self.eventLocation.removeAll()
                            self.eventPic.removeAll()
                            self.evetCreatedBy.removeAll()
                            self.eventType.removeAll()
                            
                            self.tbl.reloadData()
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.fetchCalendar()
                        }
                    }
                    
                    self.calendarVw.reloadData()
                    self.tbl.reloadData()
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    func fetchGroupCalendar(){
        /* var previousMonth = Date()
         var nextYear = Date()
         
         var dateComponents = DateComponents()
         dateComponents.year = 2
         var dateComponents1 = DateComponents()
         dateComponents1.month = -1
         
         if isFromDateSelection{
         print(selectedDate)
         isFromDateSelection = false
         
         nextYear = Calendar.current.date(byAdding: dateComponents, to: selectedDate)!
         previousMonth = Calendar.current.date(byAdding: dateComponents1, to: selectedDate)!
         }
         else{
         //            var dateComponents = DateComponents()
         //            dateComponents.year = 2
         let today = Date()
         
         nextYear = Calendar.current.date(byAdding: dateComponents, to: today) ?? today
         
         //            var dateComponents1 = DateComponents()
         //                    dateComponents1.month = -1
         previousMonth = Calendar.current.date(byAdding: dateComponents1, to: today) ?? today
         }
         */
        
        //        let parameter = ["from_date" : "\(convertTimetoTimestamp(date: today))", "to_date" : "\("\(convertTimetoTimestamp(date: nextYear!))")", "user_id" : UserDefaults.standard.value(forKey: "userId") as! String]
        //        print("Param : \(parameter)")
        eventArr.removeAllObjects()
        eventDateArr.removeAllObjects()
        eventIds.removeAll()
        eventLocation.removeAll()
        eventPic.removeAll()
        evetCreatedBy.removeAll()
        eventType.removeAll()
        
        tbl.reloadData()
        
        let parameter = ["group_id":groupID,"query":txtSearch.text!,"type":isTypeFilter] as [String : Any]
        
        ActivityIndicatorView.show("Loading....")
        
        networkProvider.request(.group_events(parameter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("json : \(jsonData)")
                    if response.statusCode == 200{
                        print("Calendar : \(jsonData)")
                        
                        if jsonData.count > 0{
                            
                            let values = jsonData["data"]
                            self.dataArr = jsonData["data"]["event_invitation"].arrayValue
                            self.sharedArr = jsonData["data"]["event_share"].arrayValue
                            
                            self.calendarVw.events.removeAll()
                            
                            let today                     = Date()
                            
                            let calendar                  = Calendar.current
                            let componetsThatWeWant       = calendar.dateComponents([.year, .day, .month], from: today)
                            
                            let day                       = componetsThatWeWant.day
                            let month                     = componetsThatWeWant.month
                            let year                      = componetsThatWeWant.year
                            
                            var tempStr = ""
                            if year! > 0 && month! > 0 && day! > 0{
                                tempStr = "\(year!)-\(month!)-\(day!)"
                            }
                            
                            let formatter1                = DateFormatter()
                            formatter1.dateFormat         = "yyyy-MM-dd"
                            formatter1.timeZone           = NSTimeZone(name: "UTC") as TimeZone?
                            formatter1.locale             = .current
                            let tDates                    = formatter1.date(from: tempStr)
                            
                            let todayDateStr              = "\(tDates!)".components(separatedBy: " ").first!
                            
                            for i in 0 ..< self.dataArr.count{
                                
                                var publicEve = ""
                                if self.dataArr[i]["public_event"].boolValue{
                                    publicEve = "Public event"
                                }
                                else{
                                    publicEve = "Private event"
                                }
                                
                                let eventDate = "\(self.convertTimeStamp(timestamp: self.dataArr[i]["from_date"].stringValue))-\(self.convertTimeStamp(timestamp: self.dataArr[i]["to_date"].stringValue))"
                                //                                  let datVal = self.convertTimeStamptoString(val: self.dataArr[i]["from_date"].doubleValue).components(separatedBy: " ").first!
                                
                                let datVal = self.convertTimeStamptoString(val: self.dataArr[i]["from_date"].doubleValue).components(separatedBy: " ").first!
                                
                                let toDatVal = self.convertTimeStamptoString(val: self.dataArr[i]["to_date"].doubleValue).components(separatedBy: " ").first!
                                
                                let fromDate = self.convertStringToDate(dateStr: datVal)
                                let toDate = self.convertStringToDate(dateStr: toDatVal)
                                
                                print("######\(fromDate) #### \(toDate)")
                                
                                var datesBetweenArray = Date.datesArr(from: fromDate, to: toDate)
                                print("###\(datesBetweenArray)")
                                
                                let eventName = self.dataArr[i]["event_name"].stringValue+"#"+eventDate+"#"+self.dataArr[i]["event_id"].stringValue+"#"+self.dataArr[i]["event_image"].stringValue+"#"+self.dataArr[i]["location"].stringValue+"#"+self.dataArr[i]["created_by_name"].stringValue+"#"+publicEve
                                
                                //                                self.calendarVw.addEventOnlyCollection(eventName, date: datVal.toDate()!)
                                //
                                //                                if datVal == todayDateStr{
                                //                                    if !self.isCreateEvent{
                                //
                                //                                        self.eventDateArr.add(datVal.toDate()!)
                                //                                        self.eventArr.add(eventName)
                                //                                    }
                                //
                                //                                }
                                for j in 0 ..< datesBetweenArray.count{
                                    
                                    let str = self.convertDateToString(currentDate: datesBetweenArray[j]).components(separatedBy: " ").first!
                                    
                                    self.calendarVw.addEventOnlyCollection(eventName, date: str.toDate()!)
                                    
                                    if str == todayDateStr{
                                        if !self.isCreateEvent{
                                            
                                            self.eventDateArr.add(str.toDate()!)
                                            self.eventArr.add(eventName)
                                        }
                                        
                                        
                                    }
                                }
                                
                            }
                            
                            for i in 0 ..< self.sharedArr.count{
                                var publicEve = ""
                                if self.sharedArr[i]["public_event"].boolValue{
                                    publicEve = "Public event"
                                }
                                let eventDate = "\(self.convertTimeStamp(timestamp: self.sharedArr[i]["from_date"].stringValue))-\(self.convertTimeStamp(timestamp: self.sharedArr[i]["to_date"].stringValue))"
                                
                                //                                let datVal = self.convertTimeStamptoString(val: self.sharedArr[i]["from_date"].doubleValue).components(separatedBy: " ").first!
                                let datVal = self.convertTimeStamptoString(val: self.sharedArr[i]["from_date"].doubleValue).components(separatedBy: " ").first!
                                
                                let toDatVal = self.convertTimeStamptoString(val: self.sharedArr[i]["to_date"].doubleValue).components(separatedBy: " ").first!
                                
                                let fromDate = self.convertStringToDate(dateStr: datVal)
                                let toDate = self.convertStringToDate(dateStr: toDatVal)
                                
                                print("######\(fromDate) #### \(toDate)")
                                
                                var datesBetweenArray = Date.datesArr(from: fromDate, to: toDate)
                                print("###\(datesBetweenArray)")
                                
                                let eventName = self.sharedArr[i]["event_name"].stringValue+"#"+eventDate+"#"+self.sharedArr[i]["event_id"].stringValue+"#"+self.sharedArr[i]["event_image"].stringValue+"#"+self.sharedArr[i]["location"].stringValue+"#"+self.sharedArr[i]["created_by_name"].stringValue+"#"+publicEve
                                
                                /* self.calendarVw.addEventOnlyCollection(eventName, date: datVal.toDate()!)
                                 
                                 if datVal == todayDateStr{
                                 
                                 if !self.isCreateEvent{
                                 
                                 self.eventDateArr.add(datVal.toDate()!)
                                 self.eventArr.add(eventName)
                                 }
                                 
                                 }*/
                                for j in 0 ..< datesBetweenArray.count{
                                    
                                    let str = self.convertDateToString(currentDate: datesBetweenArray[j]).components(separatedBy: " ").first!
                                    
                                    self.calendarVw.addEventOnlyCollection(eventName, date: str.toDate()!)
                                    
                                    if str == todayDateStr{
                                        if !self.isCreateEvent{
                                            
                                            self.eventDateArr.add(str.toDate()!)
                                            self.eventArr.add(eventName)
                                        }
                                        
                                        
                                    }
                                }
                            }
                        }
                        else{
                            self.eventArr.removeAllObjects()
                            self.eventDateArr.removeAllObjects()
                            self.eventIds.removeAll()
                            self.eventLocation.removeAll()
                            self.eventPic.removeAll()
                            
                            self.tbl.reloadData()
                        }
                        
                        self.tbl.reloadData()
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.fetchGroupCalendar()
                        }
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
        
    }
    
    
    @IBAction func onClickSearchReset(_ sender: Any) {
        txtSearch.text = ""
        eventArr.removeAllObjects()
        eventDateArr.removeAllObjects()
        eventIds.removeAll()
        eventLocation.removeAll()
        eventPic.removeAll()
        evetCreatedBy.removeAll()
        eventType.removeAll()
        
        self.calendarVw.events.removeAll()
        
        if isFromGroup{
            fetchGroupCalendar()
        }
        else{
            fetchCalendar()
        }
        self.btnSearchReset.isHidden = true
        self.txtSearch.endEditing(true)
        
    }
    
    //MARK:- Button Actions
    @IBAction func onClickTypeFilter(_ sender: UIButton) {
        if sender.tag == 1{
            if isPublic{
                isPublic = false
                imgPublic.image = nil
                isPrivate = true
                imgPrivate.image = #imageLiteral(resourceName: "Green_tick.png")
            }
            else{
                isPublic = true
                imgPublic.image = #imageLiteral(resourceName: "Green_tick.png")
            }
        }
        else{
            if isPrivate{
                isPrivate = false
                imgPrivate.image = nil
                isPublic = true
                imgPublic.image = #imageLiteral(resourceName: "Green_tick.png")
            }
            else{
                isPrivate = true
                imgPrivate.image = #imageLiteral(resourceName: "Green_tick.png")
            }
        }
        if isPrivate == true && isPublic == true{
            isTypeFilter = "-1"
        }
        else if isPrivate == false && isPublic == false{
            isTypeFilter = "-1"
        }
        else{
            if isPublic{
                isTypeFilter = "true"
            }
            else if isPrivate{
                isTypeFilter = "false"
            }
        }
        if isFromGroup{
            fetchGroupCalendar()
        }
        else{
            fetchCalendar()
        }
    }
    
    @IBAction func onClickFamilySelect(_ sender: Any) {
        if self.lblFamilyName.text?.lowercased() == "select family"{
            let storyboard = UIStoryboard.init(name: "third", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SelectFamilyCalndrViewController") as! SelectFamilyCalndrViewController
            //vc.mutialFamilies =  "\(usersList[indexPath.row].familycount ?? "0")"
            //  vc.mutualConnections =  "\(usersList[indexPath.row].mutualfamilycount ?? "0")"
            //        var userId = UserDefaults.standard.value(forKey: "userId") as! String
            //        vc.userID = userId
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            self.lblFamilyName.text = "Select Family"
            isFromGroup = false
            if #available(iOS 13.0, *) {
                self.imgSelectFamily.image = UIImage(systemName: "chevron.right")
            } else {
                // Fallback on earlier versions
                self.imgSelectFamily.image = UIImage(named: "back_right")
            }
            fetchCalendar()
        }
        
    }
    
    func selectFamilyId(groupId: String,grpName:String) {
        self.groupID = groupId
        self.lblFamilyName.text = grpName
        self.imgSelectFamily.image = #imageLiteral(resourceName: "reject")
        self.isFromGroup = true
        self.fetchGroupCalendar()
        //        self.viewDidLoad()
    }
    
    //MARK: - Convert Date
    
    func convertTimetoTimestamp(date:Date)->Int
    {
        //        let formatter = DateFormatter()
        //        formatter.dateFormat = "dd-MM-yy hh:mm a +0000"
        //        //        let convertedDate = formatter.date(from: "16-09-2017 2:00 PM")
        //     //   let convertedDate = formatter.date(from: date)
        //
        //        formatter.timeZone = TimeZone(identifier: "UTC")
        //        let date = formatter.date(from: date)
        //        print(date)
        //       // let convertedDate2 = formatter.date(from: date)
        
        let timestamp = date.timeIntervalSince1970
        
        return Int(timestamp)
        
    }
    
    func convertDate(datValue : Date) -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let myString = formatter.string(from: datValue) // string purpose I add here
        let yourDate = formatter.date(from: myString)
        //  formatter.dateFormat = "dd-MMM-yyyy hh:mm:ss a" //AM PM
        formatter.dateFormat = "dd-MMM-yyyy hh:mm:ss"
        formatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate!)
        
        print(myStringafd)
        return myStringafd
    }
    func convertDateToDisplayDate(dateFromResponse:String)->String{
        if dateFromResponse != ""
        {
            let dateAsString               = dateFromResponse
            let dateFormatter              = DateFormatter()
            dateFormatter.dateFormat       = "dd/MMM/yyyy hh:mma"
            
            let date = dateFormatter.date(from: dateAsString)
            print(date as Any)
            dateFormatter.dateFormat       = "MMM dd yyyy hh:mma"
            let DateFormatted = dateFormatter.string(from: date!)
            print(DateFormatted)
            return DateFormatted
        }else
        {
            return ""
        }
    }
    
    func convertDateToString(currentDate:Date) -> String{
        let dateFormatter = DateFormatter()
        //         dateFormatter.timeZone = TimeZone(identifier: "UTC")
        dateFormatter.timeStyle = DateFormatter.Style.short //Set time style
        dateFormatter.dateStyle = DateFormatter.Style.short //Set date style
        dateFormatter.timeZone = .current
        
        let formatter2 = DateFormatter()
        //        formatter2.timeZone = TimeZone(identifier: "UTC")
        formatter2.dateFormat = "yyyy-MM-dd HH:mm:ss +0000"
        formatter2.timeZone = TimeZone.current
        let datVal = formatter2.string(from: currentDate)
        
        return datVal
    }
    
    //MARK: - Time Stamp
    
    func convertTimeStamptoDate (val : Double) -> Date {
        let date = Date(timeIntervalSince1970: val)
        let dateFormatter = DateFormatter()
        //        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        dateFormatter.timeStyle = DateFormatter.Style.none //Set time style
        dateFormatter.dateStyle = DateFormatter.Style.short //Set date style
        dateFormatter.timeZone = .current
        
        return date
    }
    
    func convertTimeStamp(timestamp:String) -> String {
        let date = Date(timeIntervalSince1970: Double(timestamp) as! TimeInterval)
        
        //    dateFormatter.dateFormat       = "EEE MMM dd yyy"
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MMM/yyyy hh:mma"
        //              formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.timeZone = TimeZone.current
        
        
        let localDatef = formatter.string(from: date)
        print("###\(localDatef)")
        return localDatef
    }
    
    //MARK: - Time Stamp
    
    func convertTimeStamptoString (val : Double) -> String {
        let date = Date(timeIntervalSince1970: val)
        let dateFormatter = DateFormatter()
        //         dateFormatter.timeZone = TimeZone(identifier: "UTC")
        dateFormatter.timeStyle = DateFormatter.Style.short //Set time style
        dateFormatter.dateStyle = DateFormatter.Style.short //Set date style
        dateFormatter.timeZone = .current
        
        let formatter2 = DateFormatter()
        //        formatter2.timeZone = TimeZone(identifier: "UTC")
        formatter2.timeZone = TimeZone.current
        formatter2.dateFormat = "yyyy-MM-dd HH:mm:ss +0000"
        let datVal = formatter2.string(from: date)
        
        return datVal
    }
    
    //MARK:- Convert String to Date
    func convertStringToDate(dateStr:String) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from:dateStr)!
        
        return date
    }
    
    //MARK:- DatePicker View Delegate
    func datePickerPopupDidSelectDate(_ dateString: String!) {
        print(dateString!)
        let dateFormatter              = DateFormatter()
        dateFormatter.dateFormat       = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let result = dateFormatter.date(from: dateString)
        selectedDate = result!
        
        if isFromGroup{
        }
        else{
            //            tbl.isHidden = true
            fetchCalendar()
        }
        if isCreateEvent{
            calendarVw.setDisplayDate(result!, animated: true)
            calendarVw.selectDate(result!)
        }
        else{
            calendarVw.setDisplayDate(result!, animated: true)
            calendarVw.selectDate(result!)
        }
        
    }
    
    func datePickerPopupDidCancel() {
        isFromDateSelection = false
    }
    
    
    // MARK: - KDCalendarDataSource
    
    func startDate() -> Date {
        
        if isCreateEvent{
            
            let today                     = Date()
            let calendar                  = Calendar.current
            let componetsThatWeWant       = calendar.dateComponents([.year, .day, .month], from: today)
            
            let day                       = componetsThatWeWant.day
            let month                     = componetsThatWeWant.month
            let year                      = componetsThatWeWant.year
            
            var tempStr = ""
            if year! > 0 && month! > 0 && day! > 0{
                tempStr = "\(year!)-\(month!)-\(day!)"
            }
            
            let formatter1                = DateFormatter()
            formatter1.dateFormat         = "yyyy-MM-dd"
            formatter1.timeZone           = NSTimeZone(name: "UTC") as TimeZone?
            formatter1.locale             = .current
            let tDates                    = formatter1.date(from: tempStr)
            
            return tDates!
        }
        
        var dateComponents = DateComponents()
        dateComponents.month = -1
        
        let today = Date()
        
        let threeMonthsAgo = self.calendarVw.calendar.date(byAdding: dateComponents, to: today)!
        
        return threeMonthsAgo
    }
    
    func endDate() -> Date {
        
        var dateComponents = DateComponents()
        
        dateComponents.year = 10
        let today = Date()
        
        let twoYearsFromNow = self.calendarVw.calendar.date(byAdding: dateComponents, to: today)!
        
        return twoYearsFromNow
        
    }
    
    // MARK: - KDCalendarDelegate
    
    var checkOnceWorked = 0
    
    func calendar(_ calendar: CalendarView, didSelectDate date : Date, withEvents events: [CalendarEvent]) {
        print(date)
        
        if isCreateEvent{
            
            let today                     = Date()
            let calendar                  = Calendar.current
            let componetsThatWeWant       = calendar.dateComponents([.year, .day, .month], from: today)
            
            let day                       = componetsThatWeWant.day
            let month                     = componetsThatWeWant.month
            let year                      = componetsThatWeWant.year
            
            var tempStr = ""
            var tempStrLong = ""
            if year! > 0 && month! > 0 && day! > 0{
                tempStr = "\(year!)-\(month!)-\(day!)"
                tempStrLong = "\(year!)\(month!)\(day!)"
            }
            
            let formatter1                = DateFormatter()
            formatter1.dateFormat         = "yyyy-MM-dd"
            formatter1.timeZone           =  TimeZone.current
            formatter1.locale             = .current
            let tDates                    = formatter1.date(from: tempStr)
            
            let selectedDate               = date
            let calendar2                  = Calendar.current
            
            let componetsThatWeWant2       = calendar2.dateComponents([.year, .day, .month], from: selectedDate)
            
            let day2                       = componetsThatWeWant2.day
            let month2                     = componetsThatWeWant2.month
            let year2                      = componetsThatWeWant2.year
            
            var tempStr2 = ""
            var tempStrLong2 = ""
            if year! > 0 && month! > 0 && day! > 0{
                tempStr2 = "\(year2!)-\(month2!)-\(day2!)"
            }
            
            let formatter2                = DateFormatter()
            formatter2.dateStyle = .long
            formatter2.dateFormat         = "yyyy-MM-dd"
            formatter2.timeZone           =  NSTimeZone(name: "UTC") as TimeZone?
            formatter2.locale             = .current
            let temp                      = formatter2.string(from: date)
            let tDates2                   = formatter2.date(from: temp)
            let dateArr = temp.components(separatedBy: "-")
            
            if dataArr.count == 0{
                tempStrLong2  = dateArr[0] + dateArr[1] + dateArr[2]
            }
            
            let t1 = Int(tempStrLong)
            let t2 = Int(tempStrLong2)
            
            if t2! < t1! {
                return
            }
            
            if didselct{
                
                print(checkDateHigherThanCurrentDate(selectedDatestring:date))
                
                if checkOnceWorked == 1{
                    self.navigationController?.popViewController(animated: true)
                    dateCallBackDelegate.didSelectDate(result:checkDateHigherThanCurrentDate(selectedDatestring:date.adding(hour: 0, minutes: 0)), dateTag: self.dateTag)
                }else{
                    
                    checkOnceWorked = 1
                }
                
            }
            else{
                didselct = true
            }
        }
        else
        {
            
            eventArr.removeAllObjects()
            eventDateArr.removeAllObjects()
            eventIds.removeAll()
            eventLocation.removeAll()
            eventPic.removeAll()
            evetCreatedBy.removeAll()
            eventType.removeAll()
            //            tbl.isHidden = true
            
            print("Did Select: \(date) with \(events.count) events")
            
            for event in events {
                print("\t\"\(event.title)\" - Starting at:\(event.startDate)")
                eventDateArr.add(event.startDate)
                eventArr.add(event.title)
            }
            
            print("event arr : \(eventArr)")
            
            print("even date arr : \(eventDateArr)")
            
            tbl.reloadData()
            
            if eventArr.count > 0{
                //                tbl.isHidden = false
            }else{
                //                tbl.isHidden = true
            }
            
            print(90 * eventArr.count)
            
            if checkFlag == 1{
                appDel.calendarFromDate = convertTimetoTimestamp(date: date)
                checkFlag = 0
            }else{
                appDel.calendarToDate = convertTimetoTimestamp(date: date)
                checkFlag = 1
            }
            
        }
    }
    
    var checkFlag = 1
    
    //MARK: - Convet date
    
    
    func checkDateHigherThanCurrentDate (selectedDatestring:Date)->String
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let myString = formatter.string(from: selectedDatestring) // string purpose I add here
        let yourDate = formatter.date(from: myString)
        
        formatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        formatter.locale = .current
        formatter.dateFormat = "dd/MM/yyyy"
        let myStringafd = formatter.string(from: yourDate!)
        
        print(myStringafd)
        return myStringafd
    }
    func calendar(_ calendar: CalendarView, didScrollToMonth date: Date) {
        
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        formatter.locale = .current
        formatter.dateFormat = "MMMM yyyy"
        let result = formatter.string(from: date)
        print("result : \(result)")
        self.selectedMnt = "\(result)"
        yearTbl.reloadData()
    }
    
    func calendar(_ calendar: CalendarView, didDeselectDate date: Date) {
        
        //        if isCreateEvent{
        //            calendar.delegate?.calendar(calendarVw, didSelectDate: date, withEvents: [CalendarEvent]())
        //        }
        //
        print("de selected date : \(date)")
        self.eventIds = []
        self.eventPic = []
        self.eventLocation = []
        self.evetCreatedBy = []
        self.eventType = []
        self.tbl.reloadData()
        
        if isCreateEvent{
            
            let today                     = Date()
            let calendar                  = Calendar.current
            let componetsThatWeWant       = calendar.dateComponents([.year, .day, .month], from: today)
            
            let day                       = componetsThatWeWant.day
            let month                     = componetsThatWeWant.month
            let year                      = componetsThatWeWant.year
            
            var tempStr = ""
            if year! > 0 && month! > 0 && day! > 0{
                tempStr = "\(year!)-\(month!)-\(day!)"
            }
            
            let formatter1                = DateFormatter()
            formatter1.dateFormat         = "yyyy-MM-dd"
            formatter1.timeZone           = NSTimeZone(name: "UTC") as TimeZone?
            formatter1.locale             = .current
            let tDates                    = formatter1.date(from: tempStr)
            
            let selectedDate               = date
            let calendar2                  = Calendar.current
            let componetsThatWeWant2       = calendar2.dateComponents([.year, .day, .month], from: selectedDate)
            
            let day2                       = componetsThatWeWant2.day
            let month2                     = componetsThatWeWant2.month
            let year2                      = componetsThatWeWant2.year
            
            var tempStr2 = ""
            if year! > 0 && month! > 0 && day! > 0{
                tempStr2 = "\(year2!)-\(month2!)-\(day2!)"
            }
            
            let formatter2                = DateFormatter()
            formatter2.dateFormat         = "yyyy-MM-dd"
            formatter2.timeZone           = NSTimeZone(name: "UTC") as TimeZone?
            formatter2.locale             = .current
            let tDates2                    = formatter2.date(from: tempStr2)
            
            if tDates2! == tDates!{
                
                self.navigationController?.popViewController(animated: true)
                dateCallBackDelegate.didSelectDate(result:checkDateHigherThanCurrentDate(selectedDatestring:date.adding(hour: 0, minutes: 0)), dateTag: self.dateTag)
                
            }
        }
    }
    
    func headerString(_ date: Date) -> String? {
        
        print("date is : \(date)")
        
        let dd = date.adding(hour: 0, minutes: 0)
        
        return dd.nameOfMonth()! + " " + dd.yearOfDate()!
    }
    
    func calendar(_ calendar: CalendarView, didLongPressDate date: Date, withEvents events: [CalendarEvent]?) {
        
        let alert = UIAlertController(title: "Create New Event", message: "Message", preferredStyle: .alert)
        
        alert.addTextField { (textField: UITextField) in
            textField.placeholder = "Event Title"
        }
        
        let addEventAction = UIAlertAction(title: "Create", style: .default, handler: { (action) -> Void in
            let title = alert.textFields?.first?.text
            self.calendarVw.addEvent(title!, date: date)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        
        alert.addAction(addEventAction)
        alert.addAction(cancelAction)
        
    }
    
    func calendar(_ calendar: CalendarView, didLongPressDate date: Date) {
        
        let alert = UIAlertController(title: "Create New Event", message: "Message", preferredStyle: .alert)
        
        alert.addTextField { (textField: UITextField) in
            textField.placeholder = "Event Title"
        }
        
        let addEventAction = UIAlertAction(title: "Create", style: .default, handler: { (action) -> Void in
            let title = alert.textFields?.first?.text
            self.calendarVw.addEvent(title!, date: date)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        
        alert.addAction(addEventAction)
        alert.addAction(cancelAction)
        
    }
    
    func calendar(_ calendar: CalendarView, canSelectDate date: Date) -> Bool {
        
        return true
    }
    //MARK: - Load Calendar Events
    
    func loadCalendarEvents(){
        self.calendarVw.loadEvents() { error in
            if error != nil {
                let message = "The calender could not load system events. It is possibly a problem with permissions"
                let alert = UIAlertController(title: "Events Loading Error", message: message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    //MARK: - Scroll to Month
    
    @IBAction func yearDropClicked(_ sender: Any) {
        
        print("Scroll clicked...\(selectedMnt)")
        
        if yearTbl.isHidden == true{
            yearTbl.isHidden = false
            self.view.bringSubviewToFront(yearTbl)
        }else{
            yearTbl.isHidden = true
        }
        
        isFromDateSelection = true
    }
    
    //MARK:- Textfield delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == txtSearch
        {
            // selectWebAPI()
            if textField.text!.count > 0{
                btnSearchReset.isHidden = false
            }
            else{
                btnSearchReset.isHidden = true
            }
            textField.endEditing(true)
        }
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        self.view.endEditing(true)
        if textField == txtSearch
        {
            if textField.text!.count > 0{
                btnSearchReset.isHidden = false
            }
            else{
                btnSearchReset.isHidden = true
            }
            eventArr.removeAllObjects()
            eventDateArr.removeAllObjects()
            eventIds.removeAll()
            eventLocation.removeAll()
            eventPic.removeAll()
            evetCreatedBy.removeAll()
            eventType.removeAll()
            
            self.calendarVw.events.removeAll()
            if isFromGroup{
                //                tbl.isHidden = true
                fetchGroupCalendar()
            }
            else{
                //                tbl.isHidden = true
                fetchCalendar()
            }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
        }
        return true
    }
    
    
    
    //MARK: - Table View
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == yearTbl{
            return yearArr.count
        }
        
        return eventArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        
        if tableView == yearTbl{
            
            cell?.textLabel?.textAlignment    = .center
            cell?.textLabel?.font             = UIFont.boldSystemFont(ofSize: 22)
            cell?.textLabel?.text             = "\(yearArr[indexPath.row])"
            let temp = selectedMnt.components(separatedBy: " ").last
            if temp!.isEmpty{
            }
            else{
                if temp == "\(yearArr[indexPath.row])"{
                    cell?.textLabel?.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                }
                else{
                    cell?.textLabel?.textColor = .black
                }
            }
            return cell!
        }
        
        
        var  temp = ""
        if let lbl1 = cell?.viewWithTag(1) as? UILabel{
            
            let val = "\(eventArr[indexPath.row])".components(separatedBy: "#").first!
            let tempArr = (eventArr[indexPath.row] as AnyObject).components(separatedBy: "#") as! NSArray
            let temp = "\(tempArr[2])"
            let location = tempArr[4] as! String
            let pic = tempArr[3] as! String
            let createdBy = tempArr[5] as! String
            let publicEve = tempArr.lastObject as! String
            
            lbl1.text  = val
            
            if !self.eventIds.contains(temp){
                
                self.eventIds.append(temp)
            }
            
            self.eventLocation.append(location)
            
            self.eventPic.append(pic)
            self.evetCreatedBy.append(createdBy)
            self.eventType.append(publicEve)
            
        }
        
        if let lbl3 = cell?.viewWithTag(3) as? UILabel{
            lbl3.text = eventLocation[indexPath.row]
        }
        if let lbl5 = cell?.viewWithTag(5) as? UILabel{
            lbl5.text = "By \(evetCreatedBy[indexPath.row])"
        }
        if let lbl6 = cell?.viewWithTag(6) as? UILabel{
            lbl6.text = eventType[indexPath.row]
        }
        
        
        if let img = cell?.viewWithTag(4) as? UIImageView{
            let propic = eventPic[indexPath.row]
            if propic.isEmpty{
                img.image = #imageLiteral(resourceName: "Family Logo")
            }
            else{
                let temp = "\(Helpers.imageURl)"+"\(BaseUrl.event_image)"+propic
                let imgUrl = URL(string: temp)
                let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+(imgUrl?.relativeString ?? "")
                let url = URL(string: newUrlStr)
                img.kf.indicatorType = .activity
                
                img.kf.setImage(with: url, placeholder:#imageLiteral(resourceName: "imgNoImage") , options: nil, progressBlock: nil, completionHandler: nil)
            }
        }
        
        if let lbl2 = cell?.viewWithTag(2) as? UILabel{
            
            let temp = "\(eventArr[indexPath.row])".components(separatedBy: "#")
            let val = temp[1]
            let tempTime = val.components(separatedBy: "-")
            
            lbl2.text = "\(convertDateToDisplayDate(dateFromResponse:tempTime.first!)) - \(convertDateToDisplayDate(dateFromResponse:tempTime.last!))"
        }
        
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == yearTbl{
            
            if indexPath.row == 0{
                
                if isCreateEvent{
                    didselct = false
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    
                    if self.isFromGroup{
                        //                        self.tbl.isHidden = true
                        self.fetchGroupCalendar()
                    }
                    else{
                        //                        self.tbl.isHidden = true
                        self.fetchCalendar()
                    }
                })
                
                calendarVw.setDisplayDate(Date(), animated: true)
                calendarVw.selectDate(Date())
                selectedDate = Date()
                
                
            }else{
                
                let dateString = "\(yearArr[indexPath.row])-02-01" // change to your date format
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                dateFormatter.timeZone = NSTimeZone.local
                var date = dateFormatter.date(from: dateString)
                date = date?.adding(hour: 24, minutes: 0)
                selectedDate = date!
                
                if isCreateEvent{
                    calendarVw.setDisplayDate(selectedDate, animated: true)
                }
                else{
                    calendarVw.setDisplayDate(selectedDate, animated: true)
                    calendarVw.selectDate(selectedDate)
                }
                if isFromGroup{
                    //                    tbl.isHidden = true
                    fetchGroupCalendar()
                }
                else{
                    //                    tbl.isHidden = true
                    fetchCalendar()
                }
                
            }
            
            yearTbl.isHidden = true
        }
        else{
            guard self.eventIds.count >= indexPath.row else {
                return
            }
            
            let stryboard = UIStoryboard.init(name: "third", bundle: nil)
            
            let vc = stryboard.instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
            vc.fromCalendar = true
            vc.calnderId = self.eventIds[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == yearTbl{
            
            return 50
        }
        
        return UITableView.automaticDimension
    }
    
    @IBAction func backClicked(_ sender: Any) {
        if isCreateEvent{
            self.navigationController?.popViewController(animated: true)
        }
        else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension Date {
    static func datesArr(from fromDate: Date, to toDate: Date) -> [Date] {
        var dates: [Date] = []
        var date = fromDate
        
        while date <= toDate {
            dates.append(date)
            guard let newDate = Calendar.current.date(byAdding: .day, value: 1, to: date) else { break }
            date = newDate
        }
        return dates
    }
    
    func localString(dateStyle: DateFormatter.Style = .medium, timeStyle: DateFormatter.Style = .medium) -> String {
        return DateFormatter.localizedString(from: self, dateStyle: dateStyle, timeStyle: timeStyle)
    }
}
