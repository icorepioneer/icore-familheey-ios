//
//  SelectFamilyCalndrViewController.swift
//  familheey
//
//  Created by familheey on 11/12/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Alamofire
import Moya
import Kingfisher
import SwiftyJSON
import Firebase

protocol selectFamilyDelegate : class {
    func selectFamilyId(groupId:String,grpName:String)
}

class SelectFamilyCalndrViewController: UIViewController {
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearchReset: UIButton!
    @IBOutlet weak var tblListView: UITableView!
    @IBOutlet weak var noFamilyView: UIView!
    
    var FamilyArr : familyListResponse!
    var searchTxt = ""
    weak var delegate: selectFamilyDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let headerNib = UINib.init(nibName: "FamilyListingView", bundle: Bundle.main)
        tblListView.register(headerNib, forHeaderFooterViewReuseIdentifier: "FamilyListingView")
        txtSearch.delegate = self
        Helpers.setleftView(textfield: txtSearch, customWidth: 30)
     
        getFamilyList()
    }
    
    override func viewWillAppear(_ animated: Bool) {                self.navigationController?.navigationBar.isHidden  = true
    }
    

    //MARK:- Web API
    func getFamilyList(){
        let params = [
            "user_id":UserDefaults.standard.value(forKey: "userId") as! String,
            "query":self.txtSearch.text!
        ]
        APIServiceManager.callServer.getFamilyList(url: EndPoint.viewFamily, params: params,  success: { (responseMdl) in
                guard let familyMdl = responseMdl as? familyListResponse else{
                    return
                }
                ActivityIndicatorView.hiding()
                if familyMdl.statusCode == 200{
                    print(familyMdl.familyList!)
    //                    if self.txtSearch.text!.count > 0{
    //                        if familyMdl.familyList!.count > 0{
    //                            self.FamilyArr = familyMdl
    ////                            self.searchBar.isHidden = false
    //                            self.btnAdd.isHidden = false
    //                            self.tblListView.isHidden = false
    //                            self.bg_noFamilyView.isHidden = true
    //                            self.tblListView.delegate = self
    //                            self.tblListView.dataSource = self
    //
    //                            self.tblListView.reloadData()
    //                        }
    //                        else{
    //                        }
    //                    }
    //                    else{
                    if familyMdl.familyList!.count > 0{
                        self.FamilyArr = familyMdl
                        
                        self.tblListView.isHidden = false
                        self.tblListView.delegate = self
                        self.tblListView.dataSource = self
                        self.tblListView.reloadData()
                    }
                    else{
                        self.noFamilyView.isHidden = false
                        self.tblListView.isHidden = true
                    }
    //                    }
                       
                }
                else{
                    self.displayAlert(alertStr: "Something went wrong! Please try again later", title: "Error")
                    ActivityIndicatorView.hiding()
                }
                
            }) { (error) in
                ActivityIndicatorView.hiding()
                self.displayAlert(alertStr: error!.description, title: "Error")
            }
        }

    
    
    //MARK:- UIButton Actions
    @IBAction func onClickSearchReset(_ sender: Any) {
        txtSearch.text = ""
        searchTxt = ""
        getFamilyList()
        btnSearchReset.isHidden = true
        self.txtSearch.endEditing(true)
    }
    
    @IBAction func onClickViewFamily(_ sender: UIButton){
        var grpId = ""
        
        grpId = "\(FamilyArr.familyList![sender.tag].faId)"
        let grpName = FamilyArr.familyList![sender.tag].faName
        
        delegate?.selectFamilyId(groupId: grpId, grpName: grpName)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClikBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SelectFamilyCalndrViewController:UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return FamilyArr.familyList!.count
    }
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       // return FamilyArr.familyList!.count
        return 0
    }
  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       /* let cellid = "cell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! FamilyListingTableViewCell
        
        cell.selectionStyle = .none
        
        cell.lblFamilyName?.text = FamilyArr.familyList![indexPath.row].faName
        cell.lblFamilyType.text = FamilyArr.familyList![indexPath.row].faCategory
        cell.lblFamilyRegion.text = FamilyArr.familyList![indexPath.row].faRegion
        
        if FamilyArr.familyList![indexPath.row].faLogo.count != 0{
            let url = "\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+FamilyArr.familyList![indexPath.row].faLogo
            print(url)
            let imgUrl = URL(string: url)
            
            cell.imgLogo.kf.setImage(with:imgUrl, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
            
        }
        
        return cell*/
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 135
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
       
        let header = self.tblListView.dequeueReusableHeaderFooterView(withIdentifier: "FamilyListingView") as! FamilyListingView
        
        header.btnAction.tag = section
        header.btnView.tag = section
        
        header.byTitle.text = "By"
        header.lblTitle.text = FamilyArr.familyList![section].faName
        header.lblType.text = FamilyArr.familyList![section].faCategory
        header.lblRegion.text = FamilyArr.familyList![section].faRegion
        header.lblCreatedBy.text = "\(FamilyArr.familyList![section].faAdmin)"
       // header.lblMembersCount.text = "\(FamilyArr.familyList![section].memberCount)"
        
        
        if FamilyArr.familyList![section].faLogo.count != 0{
            let temp = "\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+FamilyArr.familyList![section].faLogo
            let imgurl = URL(string: temp)
            let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+imgurl!.relativeString
            let url = URL(string: newUrlStr)
            header.imgLogo.kf.indicatorType = .activity

            header.imgLogo.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        else{
            header.imgLogo.image = #imageLiteral(resourceName: "Family Logo")
        }
        
         header.btnAction.isHidden = true
         header.btnView.addTarget(self, action: #selector(onClickViewFamily(_:)), for: .touchUpInside)
        
         header.lblKnownHead.text = "posts"
        
        let knownCount = FamilyArr.familyList![section].memberCount!
        
        if Int(knownCount)! > 0{
            header.lblMembersCount.text = FamilyArr.familyList![section].memberCount
            header.lblMemberHead.isHidden = false
            header.lblMembersCount.isHidden = false
        }
        else{
            header.lblMemberHead.isHidden = true
            header.lblMembersCount.isHidden = true
        }
        
        let eventCount = FamilyArr.familyList![section].knownMemberCount
        
        if Int(eventCount)! > 0{
            header.lblKnown.text = FamilyArr.familyList![section].knownMemberCount
            header.lblKnownHead.isHidden = false
            header.lblKnown.isHidden = false
        }
        else{
            header.lblKnownHead.isHidden = true
            header.lblKnown.isHidden = true
        }
    
        return header
      
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    
   /* func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if FamilyArr.familyList![indexPath.row].isBlocked == 1{
            self.displayAlert(alertStr: "You are corrently blocked from this group", title: "")
        }
        else{
            let stryboard = UIStoryboard.init(name: "second", bundle: nil)
            
            let family = stryboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
            let temp = FamilyArr.familyList![indexPath.row].faId
            family.groupId = String(temp)
            self.navigationController?.pushViewController(family, animated: true)
        }
    }*/
}

extension SelectFamilyCalndrViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
           textField.resignFirstResponder()
           if textField == txtSearch
           {
               // selectWebAPI()
               if textField.text!.count > 0{
                   btnSearchReset.isHidden = false
               }
               else{
                   btnSearchReset.isHidden = true
               }
               textField.endEditing(true)
           }
           
           return true
       }
       func textFieldDidEndEditing(_ textField: UITextField){
           self.view.endEditing(true)
           if textField == txtSearch
           {
               if textField.text!.count > 0{
                   btnSearchReset.isHidden = false
               }
               else{
                   btnSearchReset.isHidden = true
               }
               getFamilyList()
           }
       }
       func textFieldDidBeginEditing(_ textField: UITextField) {
           if textField.text!.count > 0 {
               btnSearchReset.isHidden = false
           }
           else{
               btnSearchReset.isHidden = true
               // selectWebAPI()
           }
       }
       func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
           if textField.text!.count > 0 {
               btnSearchReset.isHidden = false
           }
           else{
               btnSearchReset.isHidden = true
           }
           return true
       }
}
