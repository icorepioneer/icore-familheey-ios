//
//  PaymentFundTableViewCell.swift
//  familheey
//
//  Created by Innovation Incubator on 05/08/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit

class PaymentFundTableViewCell: UITableViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var imgOfUser: UIImageView!
    @IBOutlet weak var btnImage: UIButton!
    @IBOutlet weak var lblNameOfUser: UILabel!

    @IBOutlet weak var lblType: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
           @IBOutlet weak var lineView: UIView!

    @IBOutlet weak var viewOfNotes: UIView!
    @IBOutlet weak var btnNotes: UIButton!
    
//    @IBOutlet weak var imgOfGreenTick: UIImageView!
//       @IBOutlet weak var viewOfCall: UIView!
//       @IBOutlet weak var btnCall: UIButton!
//       @IBOutlet weak var lblQty: UILabel!
//       @IBOutlet weak var lblDateOfContributor: UILabel!
//       @IBOutlet weak var lblPlaceOfContibutor: UILabel!
//       @IBOutlet weak var diagonalView: UIView!
//       
//       @IBOutlet weak var btnPay: UIButton!
//       @IBOutlet weak var payNowView: UIView!
//       @IBOutlet weak var btnTemSayThanks: UIButton!
//       @IBOutlet weak var imgOfMultipleCont: UIImageView!
//       @IBOutlet weak var btnSayThanks: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
