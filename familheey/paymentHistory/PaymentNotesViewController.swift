//
//  PaymentNotesViewController.swift
//  familheey
//
//  Created by familheey on 05/09/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON

class PaymentNotesViewController: UIViewController {
    @IBOutlet weak var txtNoteTo: UITextView!
    @IBOutlet weak var txtNoteFrom: UITextView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var mainHeightMultiplier: NSLayoutConstraint!
    
    @IBOutlet weak var txtFundView: UITextView!
    @IBOutlet weak var view1HeightMultiplier: NSLayoutConstraint!
    @IBOutlet weak var memberShipNoteView: UIView!
    @IBOutlet weak var fundNoteView: UIView!

    var isFromFund = false
    var ArrayOfFundRequestpData = [JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(ArrayOfFundRequestpData)
        let data = ArrayOfFundRequestpData[0]
        if isFromFund
        {
            self.memberShipNoteView.isHidden = true
                      self.fundNoteView.isHidden = false
            txtFundView.text = data["payment_note"].string ?? ""
            
         

        }
        else
        {
           self.memberShipNoteView.isHidden = false
           self.fundNoteView.isHidden = true
            txtNoteTo.text = data["membership_payment_notes"].string ?? ""
                   txtNoteFrom.text = data["membership_customer_notes"].string ?? ""
        }
        
       
    }
    
    func changeMultiplierValue(view:UIView , multiplier:CGFloat , heightconstarint:NSLayoutConstraint)
    {
        let newConstraint = heightconstarint.constraintWithMultiplier(multiplier)
                 view.removeConstraint(heightconstarint)
                 view.addConstraint(newConstraint)
                 view.layoutIfNeeded()
//                 heightconstarint = newConstraint
        
    }
    //MARK:- Button Actions
    @IBAction func onClickTouchOutSide(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

}


