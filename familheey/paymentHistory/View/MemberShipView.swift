//
//  MemberShipView.swift
//  familheey
//
//  Created by Innovation Incubator on 06/08/20.
//  Copyright © 2020 familheey. All rights reserved.
//



import UIKit
import Moya
import SwiftyJSON

class MemberShipView: UIView ,UITableViewDataSource,UITableViewDelegate, SambagDatePickerViewControllerDelegate{
    //    let appDel  = UIApplication.shared.delegate as! AppDelegate
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearchReset: UIButton!
    @IBOutlet weak var viewOfSearch: UIView!
    @IBOutlet weak var heightOfSearchView: NSLayoutConstraint!
    @IBOutlet weak var tblViewMembership: UITableView!
    @IBOutlet weak var viewOfNoData: UIView!
    @IBOutlet weak var lblFromDate: UILabel!
    @IBOutlet weak var lblToDate: UILabel!
    @IBOutlet weak var btnDone: UIButton!
    
    //    var isFromFamily = false
    //    var memberId = 0
    //    var groupId = ""
    //
    
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    
    var fromRefresh = false
    var lastFetchedIndex = 0;
    var stopPagination = false
    var isNewDataLoading = true
    
    var initiallimit = 10
    var offset = 0
    var limit = 0
    var refreshControl = UIRefreshControl()
    var searchTxt = ""
    var isFirstTime = false
    
    var superController = UIViewController()
    var ArrayOfMembershipData = [JSON]()
    
    //Using for to date
    var selectedStartDate:String = ""
    var selectedStartDateFormatted:String = ""
    var startDateTimestamp:Int! = 0
    var startDateTimeStampString:String = ""
    
    // Using for from date
    var selectedFromDate:String = ""
    var selectedFromDateFormatted:String = ""
    var fromDateTimestamp:Int! = 0
    var fromDateTimeStampString:String = ""
    
    var dateTag:Int!
    var theme: SambagTheme = .light
    
    
    
    
    //    override init (frame: CGRect) {
    //        super.init(frame: frame)
    //        initCommon()
    //    }
    
    
    //    required init?(coder: NSCoder) {
    //        fatalError("init(coder:) has not been implemented")
    //    }
    
    //    required init?(coder aDecoder: NSCoder) {
    //        super.init(coder: aDecoder)
    //        initCommon()
    //    }
    
    //    func initCommon() {
    //        // Your custom initialization code
    //        limit = initiallimit
    //        self.getPaymentMemberList()
    //
    //    }
    
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        //        tblViewFund.delegate = self
        //        tblViewFund.dataSource = self
        if fromDateTimeStampString.isEmpty && startDateTimeStampString.isEmpty{
            self.btnDone.isEnabled = false
        }
        else{
            self.btnDone.isEnabled = true
        }
        
        txtSearch.delegate = self
        Helpers.setleftView(textfield: txtSearch, customWidth: 30)
        tblViewMembership.rowHeight = UITableView.automaticDimension
        tblViewMembership.estimatedRowHeight = 120
        limit = initiallimit
        if appDel.paymentHistoryFromFamily && appDel.memberIdForPaymentHistory == 1000
        {
            self.viewOfSearch.isHidden =  false
            self.heightOfSearchView.constant = 45
            if !self.isFirstTime
            {
                self.isFirstTime = true
                
                self.getFamilyPaymentMemberList()
            }
        }
        else
        {
            self.viewOfSearch.isHidden =  true
            self.heightOfSearchView.constant = 0
            
            if !self.isFirstTime
            {
                self.isFirstTime = true
                
                self.getPaymentMemberList()
            }
        }
        lastFetchedIndex = initiallimit
        refreshControl.attributedTitle = NSAttributedString(string: "Please wait...")
        refreshControl.addTarget(self, action: #selector(refreshPost), for: .valueChanged)
        tblViewMembership.addSubview(refreshControl)
        
    }
    
    
    //MARK:- UITableView Delegates and Datasources
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ArrayOfMembershipData.count
    }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return UITableView.automaticDimension
        }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentMemberTableViewCell", for: indexPath) as! PaymentMemberTableViewCell
        let currentContributeUser = self.ArrayOfMembershipData[indexPath.row].dictionaryValue
        cell.btnNotes.tag = indexPath.row
        var ContributePic = ""
        
        if appDel.paymentHistoryFromFamily && appDel.memberIdForPaymentHistory == 1000{
            cell.lblNameOfUser.text = currentContributeUser["full_name"]?.string ?? "Unknown"
            ContributePic = (currentContributeUser["propic"]!.stringValue)
            
            if ContributePic != ""{
                let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+ContributePic.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                let imgUrl = URL(string: temp)
                cell.imgOfUser.kf.indicatorType = .activity
                
                cell.imgOfUser.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
                
            }
            else{
                cell.imgOfUser.image = #imageLiteral(resourceName: "Male Colored")
            }
        }
        else{
            cell.lblNameOfUser.text = currentContributeUser["group_name"]?.string ?? "Unknown"
            ContributePic = (currentContributeUser["logo"]!.stringValue)
            
            if ContributePic != ""{
                let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+ContributePic.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                let imgUrl = URL(string: temp)
                cell.imgOfUser.kf.indicatorType = .activity
                
                cell.imgOfUser.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                
            }
            else{
                cell.imgOfUser.image = #imageLiteral(resourceName: "Family Logo")
            }
        }
        
        
        // print(currentContributeUser)
        // print(UserDefaults.standard.value(forKey: "userId") as! String)
        
        
        cell.lblType.text = "\(currentContributeUser["membership_name"]!.stringValue)"
        
        
        cell.lblDate.attributedText = self.joinTwoAttributeString(string1: "Till : ", string2: "\(Helpers.convertTimeStampWithoutTime(dateAsString: currentContributeUser["membership_to"]!.stringValue))")
        
        
        if currentContributeUser["payment_status"]! == "Completed" || currentContributeUser["payment_status"]! == "success" {
            cell.lineView.backgroundColor =  #colorLiteral(red: 0.3843137255, green: 0.9803921569, blue: 0.1803921569, alpha: 1)
        }
        else{
            cell.lineView.backgroundColor = #colorLiteral(red: 0.9725490196, green: 0.9294117647, blue: 0.06666666667, alpha: 1)
        }
        
        
        //        cell.lblDate.text = "\(Helpers.convertTimeStampWithoutTime(dateAsString: currentContributeUser["membership_to"]!.stringValue))"
        
        
        let dueAmount:Int = currentContributeUser["membership_fees"]!.intValue - currentContributeUser["membership_total_payed_amount"]!.intValue
        // print(dueAmount)
        if dueAmount < 0
        {
            //            cell.lblPaymentDue.text = "Nil"
            cell.lblPaymentDue.attributedText = self.joinTwoAttributeString(string1: "Payment Due : ", string2: "Nil")
        }
        else
        {
            //            cell.lblPaymentDue.text = "$\(dueAmount )"
            cell.lblPaymentDue.attributedText = self.joinTwoAttributeString(string1: "Payment Due : ", string2: "$\(dueAmount )")
            
        }
        cell.lblAmount.text = "$\(currentContributeUser["membership_payed_amount"]?.intValue ?? 0)"
        cell.lblPaidDate.text = "\(Helpers.convertTimeStampWithoutYear(dateAsString: currentContributeUser["membership_paid_on"]!.stringValue))"
        
   // print(currentContributeUser)
        let membership_payment_notes =  currentContributeUser["membership_payment_notes"]?.string ?? ""
        let membership_customer_notes =  currentContributeUser["membership_customer_notes"]?.string ?? ""
        
        if membership_payment_notes == "" && membership_customer_notes == "" {
            cell.viewOfNote.isHidden = true

        }
        else
        {
            cell.viewOfNote.isHidden = false

        }

        
//        if currentContributeUser["membership_payment_notes"]!.stringValue != "" || currentContributeUser["membership_customer_notes"]!.stringValue != ""
//        {
//            cell.viewOfNote.isHidden = false
//
//        }
//        else
//        {
//            cell.viewOfNote.isHidden = true
//
//        }
//        if appDel.paymentHistoryFromFamily && appDel.memberIdForPaymentHistory == 1000{
//            cell.viewOfNote.isHidden = false
//        }
//        else{
//            cell.viewOfNote.isHidden = true
//        }
        
        //        let dict:[String:String] = arrayOfDatas[indexPath.row]
        //       cell.imgOfUser.layer.cornerRadius = 25
        //        cell.imgOfUser.clipsToBounds = true
        //        // set the text from the data model
        //        cell.lblNameOfUser?.text = dict["name"]
        //        cell.lblType?.text = dict["descri"]
        ////        if dict["status"] == "1"
        ////        {
        ////           cell.viewOfCount.isHidden = false
        ////            cell.viewOfCount.layer.cornerRadius = 3
        ////            cell.viewOfCount.clipsToBounds = true
        ////        }
        ////        else{
        ////            cell.viewOfCount.isHidden = true
        ////
        ////        }
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        // pagination
        
        if tableView.isLast(for: indexPath, arrayCount: ArrayOfMembershipData.count) {
            
            if !isNewDataLoading && !self.stopPagination{
                isNewDataLoading = true
                if ArrayOfMembershipData.count != 0{
                    offset = ArrayOfMembershipData.count + 1
                }
                // print("---------------------------------------------------\(offset)")
                
                if appDel.paymentHistoryFromFamily && appDel.memberIdForPaymentHistory == 1000
                {
                    
                    
                    self.getFamilyPaymentMemberList()
                }
                else
                {
                    self.getPaymentMemberList()
                }
            }
        }
        
        
    }
    
    //MARK:- Button Actions
    @IBAction func onClickSearchRest(_ sender: Any) {
        self.txtSearch.endEditing(true)
        txtSearch.text = ""
        searchTxt = ""
        offset = 0
        self.getFamilyPaymentMemberList()
        
        btnSearchReset.isHidden = true
        
    }
    
    @IBAction func onClickFromActions(_ sender: Any) {
        let viewController = UIApplication.topViewController()
        
        dateTag = 1
        let vc = SambagDatePickerViewController()
        vc.theme = theme
        vc.delegate = self
        
        viewController!.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func onClickToActions(_ sender: Any) {
        let viewController = UIApplication.topViewController()
        dateTag = 2
        let vc = SambagDatePickerViewController()
        vc.theme = theme
        vc.delegate = self
        
        viewController!.present(vc, animated: true, completion: nil)
    }
    @IBAction func onClickDoneAction(_ sender: Any) {
        if appDel.paymentHistoryFromFamily && appDel.memberIdForPaymentHistory == 1000{
            offset = 0
            self.getFamilyPaymentMemberList()
        }
        else{
            offset = 0
            self.getPaymentMemberList()
        }
    }
    
    @IBAction func onClickNoteView(_ sender: UIButton) {
        let viewController = UIApplication.topViewController()
           let arrayContent = ArrayOfMembershipData[sender.tag]
          
           let storyboard = UIStoryboard.init(name: "PaymentHistory", bundle: nil)
           let popOverVc = storyboard.instantiateViewController(withIdentifier: "PaymentNotesViewController") as! PaymentNotesViewController
           popOverVc.ArrayOfFundRequestpData = [arrayContent]
           popOverVc.modalTransitionStyle = .crossDissolve
        popOverVc.isFromFund = false
           popOverVc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        viewController!.present(popOverVc, animated: true)
    }
    //MARK:- Custom Methods
    
    func joinTwoAttributeString(string1:String,string2:String) -> NSMutableAttributedString
    {
        let firstAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.lightGray,.font:UIFont.systemFont(ofSize: 13)]
        let secondAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.black,.font:UIFont.systemFont(ofSize: 15)]
        
        //   let yourOtherAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.darkGray]
        
        let partOne = NSMutableAttributedString(string: string1, attributes: firstAttributes)
        let partTwo = NSMutableAttributedString(string: string2, attributes: secondAttributes)
        
        partOne.append(partTwo)
        // print(partOne)
        return partOne
    }
    func formatDateString(dateString:String) -> String{
        if dateString.isEmpty{
            return "Nill"
        }
        let dateFormatter = DateFormatter()
        
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        if let dateobj = dateFormatter.date(from: dateString) {
            dateFormatter.dateFormat = "MMM dd yyyy hh:mm a"
            dateFormatter.timeZone = .current
            return dateFormatter.string(from: dateobj)
        }
        else{
            return "Nill"
        }
    }
    @objc func refreshPost(refreshControl: UIRefreshControl) {
        refreshControl.endRefreshing()
        fromRefresh = true
        offset = 0
        if appDel.paymentHistoryFromFamily && appDel.memberIdForPaymentHistory == 1000
        {
            
            
            self.getFamilyPaymentMemberList()
        }
        else
        {
            getPaymentMemberList()
        }
    }
    
    func getFamilyPaymentMemberList(){
        
        
        if offset == 0{
            ArrayOfMembershipData.removeAll()
            ActivityIndicatorView.show("Please wait....")
        }else{
            tblViewMembership.showLoading()
        }
        //                ActivityIndicatorView.show("Please wait....")
        self.stopPagination = false
        var param = [String:Any]()
        param = ["type":"membership","group_id" :appDel.groupIdForPaymentHistory,"offset":"\(offset)","limit":"\(limit)", "query" : searchTxt,"filter_endDate":startDateTimeStampString,"filter_startDate": fromDateTimeStampString]
        
        
     //   // print(param)
        networkProvider.request(.paymentHistoryListInFamily(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    //                            ActivityIndicatorView.hiding()
                    self.isNewDataLoading = false
                    
                    let jsonData =  JSON(response.data)
                    // print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        if self.offset == 0{
                            ActivityIndicatorView.hiding()
                        }else{
                            self.tblViewMembership.hideLoading()
                        }
                        if let response = jsonData["data"].array{
                            if response.count == 0{
                                self.stopPagination = true
                            }
                            //                            if self.offset == 0
                            //                            {
                            //                               self.ArrayOfMembershipData = response
                            //                            }
                            //                            else
                            //                            {
                            self.ArrayOfMembershipData.append(contentsOf: response)
                            //                            }
                            if self.offset != 0{
                                if response.count > 0 {
                                    self.insertRowsAtTableView(responseCount: response.count)
                                }
                            }
                            self.tableviewreloadwithData()
                            
                            
                            
                            //                                    self.ArrayOfMemberShipData = response
                            //                                    print(self.ArrayOfMemberShipData)
                            //                                    print(self.ArrayOfMemberShipData.count)
                            //                                    self.viewOfMembershipList.tableviewreloadwithData(data: self.ArrayOfMemberShipData)
                            
                        }
                        else{
                            if self.offset == 0{
                                ActivityIndicatorView.hiding()
                            }
                            else{
                                self.tblViewMembership.hideLoading()
                                
                            }
                            self.viewOfNoData.isHidden = false
                            self.tblViewMembership.isHidden = true
                            Helpers.showAlertDialog(message: "Oops! This content is no longer available.", target: self.superController)
                            
                            //                            self.superController.displayAlertChoice(alertStr: "Oops! This content is no longer available.", title: "", completion: { (result) in
                            //                                self.superController.navigationController?.popViewController(animated: true)
                            //                            })
                        }
                        
                    }
                    else if response.statusCode == 401{
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getFamilyPaymentMemberList()
                        }
                    }
                    else{
                        
                        if self.offset == 0{
                            ActivityIndicatorView.hiding()
                        }
                        else{
                            self.tblViewMembership.hideLoading()
                            
                        }
                        
                        self.viewOfNoData.isHidden = false
                        self.tblViewMembership.isHidden = true
                        Helpers.showAlertDialog(message: "Oops! This content is no longer available.", target: self.superController)
                        //                        self.superController.displayAlertChoice(alertStr: "Oops! This content is no longer available.", title: "", completion: { (result) in
                        //                            self.superController.navigationController?.popViewController(animated: true)
                        //                        })
                        
                        
                    }
                    //                        self.navigationController?.popViewController(animated: false)
                }catch  _ {
                    if self.offset == 0{
                        ActivityIndicatorView.hiding()
                    }
                    else{
                        self.tblViewMembership.hideLoading()
                        
                    }
                }
            case .failure( _):
                if self.offset == 0{
                    ActivityIndicatorView.hiding()
                }
                else{
                    self.tblViewMembership.hideLoading()
                    
                }
                break
            }
        }
    }
    func getPaymentMemberList(){
        
        
        if offset == 0{
            ArrayOfMembershipData.removeAll()
            ActivityIndicatorView.show("Please wait....")
        }else{
            tblViewMembership.showLoading()
        }
        //                ActivityIndicatorView.show("Please wait....")
        self.stopPagination = false
        var param = [String:Any]()
        if appDel.paymentHistoryFromFamily
        {
            
            param = ["type":"membership","user_id":"\(appDel.memberIdForPaymentHistory)","group_id" :appDel.groupIdForPaymentHistory,"offset":"\(offset)","limit":"\(limit)","filter_endDate":startDateTimeStampString,"filter_startDate": fromDateTimeStampString]
        }
        else
        {
            param = ["type":"membership","user_id":UserDefaults.standard.value(forKey: "userId") as! String,"offset":"\(offset)","limit":"\(limit)","filter_endDate":startDateTimeStampString,"filter_startDate": fromDateTimeStampString]
        }
        
        // print(param)
        networkProvider.request(.paymentHistoryList(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    //                            ActivityIndicatorView.hiding()
                    self.isNewDataLoading = false
                    
                    let jsonData =  JSON(response.data)
                    // print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        if self.offset == 0{
                            ActivityIndicatorView.hiding()
                        }else{
                            self.tblViewMembership.hideLoading()
                        }
                        if let response = jsonData["data"].array{
                            if response.count == 0{
                                self.stopPagination = true
                            }
                            //                            if self.offset == 0
                            //                            {
                            //                               self.ArrayOfMembershipData = response
                            //                            }
                            //                            else
                            //                            {
                            self.ArrayOfMembershipData.append(contentsOf: response)
                            //                            }
                            if self.offset != 0{
                                if response.count > 0 {
                                    self.insertRowsAtTableView(responseCount: response.count)
                                }
                            }
                            self.tableviewreloadwithData()
                            
                            
                            
                            //                                    self.ArrayOfMemberShipData = response
                            //                                    print(self.ArrayOfMemberShipData)
                            //                                    print(self.ArrayOfMemberShipData.count)
                            //                                    self.viewOfMembershipList.tableviewreloadwithData(data: self.ArrayOfMemberShipData)
                            
                        }
                        else{
                            if self.offset == 0{
                                ActivityIndicatorView.hiding()
                            }
                            else{
                                self.tblViewMembership.hideLoading()
                                
                            }
                            //                            self.superController.displayAlertChoice(alertStr: "Oops! This content is no longer available.", title: "", completion: { (result) in
                            //                                self.superController.navigationController?.popViewController(animated: true)
                            //                            })
                            self.viewOfNoData.isHidden = false
                            self.tblViewMembership.isHidden = true
                            Helpers.showAlertDialog(message: "Oops! This content is no longer available.", target: self.superController)
                        }
                        
                    }
                    else if response.statusCode == 401{
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getPaymentMemberList()
                        }
                    }
                    else{
                        
                        if self.offset == 0{
                            ActivityIndicatorView.hiding()
                        }
                        else{
                            self.tblViewMembership.hideLoading()
                            
                        }
                        //                        self.superController.displayAlertChoice(alertStr: "Oops! This content is no longer available.", title: "", completion: { (result) in
                        //                            self.superController.navigationController?.popViewController(animated: true)
                        //                        })
                        self.viewOfNoData.isHidden = false
                        self.tblViewMembership.isHidden = true
                        Helpers.showAlertDialog(message: "Oops! This content is no longer available.", target: self.superController)
                        
                    }
                    //                        self.navigationController?.popViewController(animated: false)
                }catch  _ {
                    if self.offset == 0{
                        ActivityIndicatorView.hiding()
                    }
                    else{
                        self.tblViewMembership.hideLoading()
                        
                    }
                }
            case .failure( _):
                if self.offset == 0{
                    ActivityIndicatorView.hiding()
                }
                else{
                    self.tblViewMembership.hideLoading()
                    
                }
                break
            }
        }
    }
    
    func insertRowsAtTableView(responseCount: Int){
        
        var indexPaths = [IndexPath]()
        
        for i in 0 ..< responseCount{
            let indx = ArrayOfMembershipData.count - responseCount
            let indexPath = indx + i
            indexPaths.append(IndexPath(row: indexPath, section: 0))
        }
        
        print(indexPaths)
        self.tblViewMembership.beginUpdates()
        self.tblViewMembership.insertRows(at: indexPaths, with: .none)
        self.tblViewMembership.endUpdates()
    }
    //    func tableviewreloadwithData(data:[JSON])
    func tableviewreloadwithData()
        
    {
        //        self.ArrayOfMembershipData = data
        if self.ArrayOfMembershipData.count == 0
        {
            viewOfNoData.isHidden = false
            tblViewMembership.isHidden = true
        }
        else
        {
            viewOfNoData.isHidden = true
            tblViewMembership.isHidden = false
            tblViewMembership.delegate = self
            tblViewMembership.dataSource = self
            self.tblViewMembership.reloadData()
        }
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if fromRefresh{
            fromRefresh = false
        }
        else{
            var currentCellOffset = scrollView.contentOffset
            currentCellOffset.x += (self.tblViewMembership.bounds.width);
            
            let indepath = self.tblViewMembership.indexPathsForVisibleRows?.first
            //            if indepath != nil{
            //                self.viewCurrentPost(index: (indepath?.row)!)
            //            }
        }
    }
    /* */
    //    @IBAction func btnGroup_OnClick(_ sender: Any) {
    //       delegate.group_OnClick()
    //    }
    //
    //    @IBAction func btnContact_OnClick(_ sender: Any) {
    //        delegate.Contact_OnClick()
    //    }
}

extension MemberShipView:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == txtSearch
        {
            // selectWebAPI()
            if textField.text!.count > 0{
                btnSearchReset.isHidden = false
            }
            else{
                btnSearchReset.isHidden = true
            }
            searchTxt = txtSearch.text!
            
            textField.endEditing(true)
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        self.endEditing(true)
        if textField == txtSearch
        {
            if textField.text!.count > 0{
                btnSearchReset.isHidden = false
            }
            else{
                btnSearchReset.isHidden = true
            }
            searchTxt = txtSearch.text!
            offset = 0
            self.getFamilyPaymentMemberList()
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
            // selectWebAPI()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
            // selectWebAPI()
        }
        return true
    }
    
    
    //MARK:- SambagDatePickerViewController Delegates
    func didSelectDate(result:String,dateTag:Int){
        if dateTag == 1{
            selectedFromDate = "\(result)   00:00 a"
            if checkDateHigherThanCurrentDate(selectedDatestring: selectedFromDate){
                showToast(message: "Please select start date lower than today")
            }
            else{
                lblFromDate.text = Helpers.sambagTimeDisplaywithoutTime2(dateAsString: "\(result)")
                fromDateTimestamp = self.convertTimetoTimestamp(date: self.selectedFromDate)
                if fromDateTimestamp != 0{
                    fromDateTimeStampString = "\(fromDateTimestamp!)"
                }
                //            self.calculateToDateUsingDuration(dateString: "\(result)")
                self.selectedFromDateFormatted = self.convertDateFormat(date: selectedFromDate)
            }
        }
        else{
            
            selectedStartDate = "\(result)   11:59 p"
            // print(selectedStartDate)
            
            if checkDateHigherThanCurrentDate(selectedDatestring: "\(result)"){
                showToast(message: "Please select end date lower than today")
            }
            else{
                lblToDate.text = Helpers.sambagTimeDisplaywithoutTime2(dateAsString: "\(result)")
                startDateTimestamp = self.convertTimetoTimestamp(date: self.selectedStartDate)
                if startDateTimestamp != 0{
                    startDateTimeStampString = "\(startDateTimestamp!)"
                }
                self.selectedStartDateFormatted = self.convertDateFormat(date: selectedStartDate)
            }
        }
        
        if !fromDateTimeStampString.isEmpty && !startDateTimeStampString.isEmpty{
            btnDone.isEnabled = true
        }
        else{
            btnDone.isEnabled = false
        }
    }
    
    func sambagDatePickerDidSet(_ viewController: SambagDatePickerViewController, result: SambagDatePickerResult) {
        if dateTag == 1{
            selectedFromDate = "\(result)   00:00 a"
            if checkDateHigherThanCurrentDate(selectedDatestring: selectedFromDate){
                showToast(message: "Please select start date lower than today")
            }
            else{
                lblFromDate.text = Helpers.sambagTimeDisplaywithoutTime2(dateAsString: "\(result)")
                fromDateTimestamp = self.convertTimetoTimestamp(date: self.selectedFromDate)
                if fromDateTimestamp != 0{
                    fromDateTimeStampString = "\(fromDateTimestamp!)"
                }
                //            self.calculateToDateUsingDuration(dateString: "\(result)")
                self.selectedFromDateFormatted = self.convertDateFormat(date: selectedFromDate)
            }

        }
        else{
            selectedStartDate = "\(result)   11:59 p"
            if checkDateHigherThanCurrentDate(selectedDatestring: "\(result)"){
                showToast(message: "Please select end date lower than today")
            }
            else{
                lblToDate.text = Helpers.sambagTimeDisplaywithoutTime2(dateAsString: "\(result)")
                startDateTimestamp = self.convertTimetoTimestamp(date: self.selectedStartDate)
                if startDateTimestamp != 0{
                    startDateTimeStampString = "\(startDateTimestamp!)"
                }
                self.selectedStartDateFormatted = self.convertDateFormat(date: selectedStartDate)
            }

        }
        if !fromDateTimeStampString.isEmpty && !startDateTimeStampString.isEmpty{
            btnDone.isEnabled = true
        }
        else{
            btnDone.isEnabled = false
        }
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func sambagDatePickerDidCancel(_ viewController: SambagDatePickerViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    //MARK: Custom Date Functions
    func checkDateHigherThanCurrentDate (selectedDatestring:String)->Bool{
        print(selectedDatestring)
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"
        
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        let convertedDateNow = formatter.string(from: Date())
        let convertedDateNow2 = formatter.date(from: convertedDateNow)
        let convertSelectedD = formatter.date(from: selectedDatestring)
        
        formatter2.timeZone = NSTimeZone.local
        let dateNow = formatter2.string(from: convertedDateNow2!)
        print(dateNow)
        
        if convertSelectedD?.compare(convertedDateNow2!) == ComparisonResult.orderedDescending{
            return true
        }
        else{
            return false
        }
        
    }
    
    func convertDateFormat(date:String)->String{
        do{
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy hh:mm a"
            //        let convertedDate = formatter.date(from: "16-09-2017 2:00 PM")
            if  let convertedDate = try formatter.date(from: date){
                //                formatter.timeZone = TimeZone(identifier: "UTC")
                formatter.timeZone = NSTimeZone.local
                let date = formatter.string(from: convertedDate)
                // print(date)
                let formatter2 = DateFormatter()
                formatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
                let convertedDate2 = formatter.date(from: date)
                formatter2.timeZone = NSTimeZone.local
                let date2 = formatter2.string(from: convertedDate2!)
                // print(date2)
                
                return date2
            }
            else{
                return ""
            }
            
        }catch{
            return ""
        }
    }
    
    func convertThenCallCalculate(dateString:String){
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd yyyy"
        
        let convertedDate = formatter.date(from: dateString)
        
        formatter.dateFormat = "dd/MM/yyyy"
        let dateFormatted = formatter.string(from: convertedDate!)
        //           calculateToDateUsingDuration(dateString: dateFormatted)
    }
    
    /* func calculateToDateUsingDuration(dateString:String){
     let formatter = DateFormatter()
     formatter.dateFormat = "dd/MM/yyyy"
     formatter.timeZone = NSTimeZone.local
     formatter.locale = .current
     let convertedDate = formatter.date(from: dateString)
     
     let toDate = convertedDate?.addDay(n: numberofDays)
     print(toDate)
     print(convertedDate)
     
     formatter.dateFormat = "MMM dd yyyy"
     if membershipTypeId == 4{
     txtMembershipTo.text = "Life Time"
     }
     else{
     txtMembershipTo.text = formatter.string(from: toDate!)
     }
     
     }*/
    
    func convertTimetoTimestamp(date:String)->Int{
        
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"
        //        let convertedDate = formatter.date(from: "16-09-2017 2:00 PM")
        let convertedDate = formatter.date(from: date)
        
        //        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.timeZone = NSTimeZone.local
        let date = formatter.string(from: convertedDate!)
        // print(date)
        let convertedDate2 = formatter.date(from: date)
        
        let timestamp = convertedDate2!.timeIntervalSince1970
        // print(timestamp)
        
        return Int(timestamp)
        
    }
    
    //MARK:- Show toast
    func showToast(message : String) {
         
         let toastLabel = UILabel(frame: CGRect(x: 20, y: self.frame.size.height/2-30, width: self.frame.width-40, height: 60))
         toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
         toastLabel.textColor = UIColor.white
         toastLabel.textAlignment = .center;
         toastLabel.numberOfLines = 2
         toastLabel.font = UIFont(name: "Montserrat-Light", size: 10.0)
         toastLabel.adjustsFontSizeToFitWidth = true
         toastLabel.text = message
         toastLabel.alpha = 1.0
         toastLabel.layer.cornerRadius = 10;
         toastLabel.clipsToBounds  =  true
         self.addSubview(toastLabel)
         UIView.animate(withDuration: 6.0, delay: 0.3, options: .curveEaseOut, animations: {
             toastLabel.alpha = 0.0
         }, completion: {(isCompleted) in
             toastLabel.removeFromSuperview()
         })
     }
}
