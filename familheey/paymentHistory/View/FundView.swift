//
//  ChatView.swift
//  Gim
//
//  Created by NIC on 27/12/18.
//  Copyright © 2018 NIC. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON
protocol ChatViewDelegate1 {
    func group_OnClick()
    func Contact_OnClick()
    func chat_OnClick()
}

class FundView: UIView ,UITableViewDataSource,UITableViewDelegate, SambagDatePickerViewControllerDelegate{
    //    let appDel  = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var tblViewFund: UITableView!
    @IBOutlet weak var viewOfNoData: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearchReset: UIButton!
    @IBOutlet weak var viewOfSearch: UIView!
    @IBOutlet weak var lblFromDate: UILabel!
    @IBOutlet weak var lblToDate: UILabel!
    @IBOutlet weak var heightOfSearchView: NSLayoutConstraint!
    @IBOutlet weak var btnDone: UIButton!
    
    //    var isFromFamily = false
    //    var memberId = 0
    //    var groupId = ""
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    
    var fromRefresh = false
    var lastFetchedIndex = 0;
    var stopPagination = false
    var isNewDataLoading = true
    
    var initiallimit = 10
    var offset = 0
    var limit = 0
    var refreshControl = UIRefreshControl()
    var searchTxt = ""
    var isFirstTime = false
    
    var superController = UIViewController()
    var ArrayOfFundRequestpData = [JSON]()
    var delegate: ChatViewDelegate1!
    
    var selectedStartDate:String = ""
    var selectedStartDateFormatted:String = ""
    var startDateTimestamp:Int! = 0
    var startDateTimeStampString:String = ""
    
    var selectedFromDate:String = ""
    var selectedFromDateFormatted:String = ""
    var fromDateTimestamp:Int! = 0
    var fromDateTimeStampString:String = ""
    
    var dateTag:Int!
    var theme: SambagTheme = .light
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        //        tblViewFund.delegate = self
        //        tblViewFund.dataSource = self
        if fromDateTimeStampString.isEmpty && startDateTimeStampString.isEmpty{
            self.btnDone.isEnabled = false
        }
        else{
            self.btnDone.isEnabled = true
        }
        txtSearch.delegate = self
        Helpers.setleftView(textfield: txtSearch, customWidth: 30)
        
        tblViewFund.rowHeight = UITableView.automaticDimension
        tblViewFund.estimatedRowHeight = 120
        limit = initiallimit
        if appDel.paymentHistoryFromFamily && appDel.memberIdForPaymentHistory == 1000
        {
            self.viewOfSearch.isHidden =  false
            self.heightOfSearchView.constant = 45
            if !self.isFirstTime
            {
                self.isFirstTime = true
                
                self.getFamilyPaymentFundList()
            }
        }
        else
        {
            self.viewOfSearch.isHidden =  true
            self.heightOfSearchView.constant = 0
            
            if !self.isFirstTime
            {
                self.isFirstTime = true
                
                self.getPaymentFundList()
            }
        }
        
        
        lastFetchedIndex = initiallimit
        refreshControl.attributedTitle = NSAttributedString(string: "Please wait...")
        refreshControl.addTarget(self, action: #selector(refreshPost), for: .valueChanged)
        tblViewFund.addSubview(refreshControl)
        
    }
    
    //    override init (frame: CGRect) {
    //        super.init(frame: frame)
    //        initCommon()
    //    }
    //
    //    required init?(coder aDecoder: NSCoder) {
    //        super.init(coder: aDecoder)
    //        initCommon()
    //    }
    //
    //    func initCommon() {
    //          // Your custom initialization code
    //        limit = initiallimit
    //        self.getPaymentFundList()
    //
    //      }
    
    //MARK:- UITableVIew Delegate and Datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ArrayOfFundRequestpData.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentFundTableViewCell", for: indexPath) as! PaymentFundTableViewCell
        let currentContributeUser = self.ArrayOfFundRequestpData[indexPath.row].dictionaryValue
        
        cell.btnNotes.tag = indexPath.row
        
        
        let aPath = UIBezierPath()
        //
        //        aPath.move(to: CGPoint(x:cell.bgView.frame.size.width-50, y:cell.bgView.frame.size.height))
        //        aPath.addLine(to: CGPoint(x: cell.bgView.frame.size.width, y: cell.bgView.frame.size.height))
        //
        //        // Keep using the method addLine until you get to the one where about to close the path
        //        aPath.close()
        //
        //        // If you want to stroke it with a red color
        //        UIColor.red.set()
        //        aPath.lineWidth = 3
        //        aPath.stroke()
        
        //       let line =
        self.drawline(CGRect(x: 0, y: 0, width: cell.bgView.frame.width, height: cell.bgView.frame.height))
        //        cell.bgView.addSubview(line)
        
        
        var ContributePic = ""
        if appDel.paymentHistoryFromFamily && appDel.memberIdForPaymentHistory == 1000{
            cell.lblNameOfUser.text = currentContributeUser["full_name"]?.string ?? "Unknown"
            ContributePic = (currentContributeUser["propic"]!.stringValue)
            
            if ContributePic != ""{
                let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+ContributePic.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                let imgUrl = URL(string: temp)
                cell.imgOfUser.kf.indicatorType = .activity
                
                cell.imgOfUser.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
                
            }
            else{
                cell.imgOfUser.image = #imageLiteral(resourceName: "Male Colored")
            }
        }
        else{
            cell.lblNameOfUser.text = currentContributeUser["group_name"]?.string ?? "Unknown"
            ContributePic = (currentContributeUser["logo"]!.stringValue)
            
            if ContributePic != ""{
                let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+ContributePic.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                let imgUrl = URL(string: temp)
                cell.imgOfUser.kf.indicatorType = .activity
                
                cell.imgOfUser.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                
            }
            else{
                cell.imgOfUser.image = #imageLiteral(resourceName: "imgNoImage")
            }
        }
        
        
        // print(currentContributeUser)
        // print(UserDefaults.standard.value(forKey: "userId") as! String)
        
        if currentContributeUser["payment_status"]! == "pending" {
            cell.lineView.backgroundColor =  #colorLiteral(red: 0.9725490196, green: 0.9294117647, blue: 0.06666666667, alpha: 1)
        }
        else{
            cell.lineView.backgroundColor = #colorLiteral(red: 0.3843137255, green: 0.9803921569, blue: 0.1803921569, alpha: 1)
        }
        if let item = currentContributeUser["request_item_title"]?.string, item.isEmpty{
            cell.lblType.isHidden = true
        }
        else{
            cell.lblType.text = currentContributeUser["request_item_title"]?.stringValue
            cell.lblType.isHidden = false
        }
        
        cell.lblAmount.text = "$\(currentContributeUser["contribute_item_quantity"] ?? "")"
        // cell.lblDate.text = "\( self.formatDateString(dateString:currentContributeUser["paid_on"]!.stringValue))"
        cell.lblDate.text = Helpers.convertTimeStampWithoutTime(dateAsString: currentContributeUser["paid_on"]!.stringValue)
        
//        if appDel.paymentHistoryFromFamily && appDel.memberIdForPaymentHistory == 1000{
//            cell.viewOfNotes.isHidden = true
//        }
//        else{
//            cell.viewOfNotes.isHidden = true
//        }
        
        let payment_note =  currentContributeUser["payment_note"]?.string ?? ""
              
               if payment_note == "" {
                   cell.viewOfNotes.isHidden = true

               }
               else
               {
                   cell.viewOfNotes.isHidden = false

               }
    
        return cell
    }
    
  
    func drawline(_ rect: CGRect) {
        //        let context = UIGraphicsGetCurrentContext()
        //        context!.setLineWidth(2.0)
        //        context!.setStrokeColor(UIColor.red.cgColor)
        //        context?.move(to: CGPoint(x: 0, y: self.frame.size.height))
        //        context?.addLine(to: CGPoint(x: self.frame.size.width, y: 0))
        //        context!.strokePath()
        //
        let aPath = UIBezierPath()
        
        aPath.move(to: CGPoint(x:0, y:25))
        aPath.addLine(to: CGPoint(x: 25, y: 0))
        
        // Keep using the method addLine until you get to the one where about to close the path
        aPath.close()
        
        // If you want to stroke it with a red color
        UIColor.red.set()
        aPath.lineWidth = 5
        aPath.stroke()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        // pagination
        
        if tableView.isLast(for: indexPath, arrayCount: ArrayOfFundRequestpData.count) {
            
            if !isNewDataLoading && !self.stopPagination{
                isNewDataLoading = true
                if ArrayOfFundRequestpData.count != 0{
                    offset = ArrayOfFundRequestpData.count + 1
                }
                // print("---------------------------------------------------\(offset)")
                if appDel.paymentHistoryFromFamily && appDel.memberIdForPaymentHistory == 1000
                {
                    
                    
                    self.getFamilyPaymentFundList()
                }
                else
                {
                    self.getPaymentFundList()
                    
                }
            }
        }
        
        
    }
    func formatDateString(dateString:String) -> String{
        if dateString.isEmpty{
            return "Nill"
        }
        let dateFormatter = DateFormatter()
        
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        if let dateobj = dateFormatter.date(from: dateString) {
            dateFormatter.dateFormat = "MMM dd yyyy hh:mm a"
            dateFormatter.timeZone = .current
            return dateFormatter.string(from: dateobj)
        }
        else{
            return "Nill"
        }
    }
    
    //MARK:- Button actions
    @IBAction func onClickSearchRest(_ sender: Any) {
        self.txtSearch.endEditing(true)
        txtSearch.text = ""
        searchTxt = ""
        offset = 0
        
        self.getFamilyPaymentFundList()
        
        btnSearchReset.isHidden = true
        
    }
    
    @IBAction func onClickFromDateAction(_ sender: Any) {
        let viewController = UIApplication.topViewController()
        
        dateTag = 1
        let vc = SambagDatePickerViewController()
        vc.theme = theme
        vc.delegate = self
        
        viewController!.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func onClickToDateActions(_ sender: Any) {
        let viewController = UIApplication.topViewController()
        dateTag = 2
        let vc = SambagDatePickerViewController()
        vc.theme = theme
        vc.delegate = self
        
        viewController!.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func onClickDoneActions(_ sender: Any) {

        if appDel.paymentHistoryFromFamily && appDel.memberIdForPaymentHistory == 1000{
            offset = 0
            self.getFamilyPaymentFundList()
        }
        else{
            offset = 0
            self.getPaymentFundList()
        }
    }
  
    @IBAction func onClickNoteAction(_ sender: UIButton) {
        let viewController = UIApplication.topViewController()
        let arrayContent = ArrayOfFundRequestpData[sender.tag]
       
        let storyboard = UIStoryboard.init(name: "PaymentHistory", bundle: nil)
        let popOverVc = storyboard.instantiateViewController(withIdentifier: "PaymentNotesViewController") as! PaymentNotesViewController
        popOverVc.ArrayOfFundRequestpData = [arrayContent]
        popOverVc.isFromFund = true
        popOverVc.modalTransitionStyle = .crossDissolve
        popOverVc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        viewController!.present(popOverVc, animated: true)
    }
    
    
    //MARK:- Custom Methods
    
    @objc func refreshPost(refreshControl: UIRefreshControl) {
        refreshControl.endRefreshing()
        fromRefresh = true
        offset = 0
        if appDel.paymentHistoryFromFamily && appDel.memberIdForPaymentHistory == 1000
        {
            
            
            self.getFamilyPaymentFundList()
        }
        else
        {
            getPaymentFundList()
        }
    }
    
    
    func getFamilyPaymentFundList(){
        
        if offset == 0{
            ArrayOfFundRequestpData.removeAll()
            ActivityIndicatorView.show("Please wait....")
        }else{
            tblViewFund.showLoading()
        }
        //                ActivityIndicatorView.show("Please wait....")
        self.stopPagination = false
        
        var param = [String:Any]()
        
        param = ["type":"request","group_id" :appDel.groupIdForPaymentHistory,"offset":"\(offset)","limit":"\(limit)", "query" : searchTxt ,"filter_endDate":startDateTimeStampString,"filter_startDate": fromDateTimeStampString]
        
        
        //                let param = ["type":"request","user_id":UserDefaults.standard.value(forKey: "userId") as! String]
        print(param)
        ActivityIndicatorView.show("Please wait....")
        networkProvider.request(.paymentHistoryListInFamily(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    ActivityIndicatorView.hiding()
                    let jsonData =  JSON(response.data)
                    self.isNewDataLoading = false
                     print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        
                        if self.offset == 0{
                            ActivityIndicatorView.hiding()
                        }else{
                            self.tblViewFund.hideLoading()
                        }
                        if let response = jsonData["data"].array{
                            if response.count == 0{
                                self.stopPagination = true
                            }
                            self.ArrayOfFundRequestpData.append(contentsOf: response)
                            if self.offset != 0{
                                if response.count > 0 {
                                    self.insertRowsAtTableView(responseCount: response.count)
                                }
                            }
                            self.tableviewreloadwithData()
                            
                            //                                    self.tableviewreloadwithData(data: response)
                            //                                    self.ArrayOfFundRequestpData = response
                            //                                    print(self.ArrayOfFundRequestpData)
                            //                                    print(self.ArrayOfFundRequestpData.count)
                            //                                    self.viewOfFundList.tableviewreloadwithData(data: self.ArrayOfFundRequestpData)
                            //
                        }
                        else{
                            if self.offset == 0{
                                ActivityIndicatorView.hiding()
                            }
                            else{
                                self.tblViewFund.hideLoading()
                                
                            }
                            self.viewOfNoData.isHidden = false
                            self.tblViewFund.isHidden = true
                            Helpers.showAlertDialog(message: "Oops! This content is no longer available.", target: self.superController)
                            
                            //                            self.superController.displayAlertChoice(alertStr: "Oops! This content is no longer available.", title: "", completion: { (result) in
                            //                                self.superController.navigationController?.popViewController(animated: true)
                            //                            })
                        }
                        
                    }
                    else if response.statusCode == 401{
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getFamilyPaymentFundList()
                        }
                    }
                    else{
                        
                        if self.offset == 0{
                            ActivityIndicatorView.hiding()
                        }
                        else{
                            self.tblViewFund.hideLoading()
                            
                        }
                        self.viewOfNoData.isHidden = false
                        self.tblViewFund.isHidden = true
                        Helpers.showAlertDialog(message: "Oops! This content is no longer available.", target: self.superController)
                        
                        //                        self.superController.displayAlertChoice(alertStr: "Oops! This content is no longer available.", title: "", completion: { (result) in
                        //                            self.superController.navigationController?.popViewController(animated: true)
                        //                        })
                        
                    }
                    //                        self.navigationController?.popViewController(animated: false)
                }catch  _ {
                    if self.offset == 0{
                        ActivityIndicatorView.hiding()
                    }
                    else{
                        self.tblViewFund.hideLoading()
                        
                    }
                    ActivityIndicatorView.hiding()
                    //  Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure(  _):
                if self.offset == 0{
                    ActivityIndicatorView.hiding()
                }
                else{
                    self.tblViewFund.hideLoading()
                    
                }
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    func getPaymentFundList(){
        
        if offset == 0{
            ArrayOfFundRequestpData.removeAll()
            ActivityIndicatorView.show("Please wait....")
        }else{
            tblViewFund.showLoading()
        }
        //                ActivityIndicatorView.show("Please wait....")
        self.stopPagination = false
        
        var param = [String:Any]()
        if appDel.paymentHistoryFromFamily
            
        {
            param = ["type":"request","user_id":"\(appDel.memberIdForPaymentHistory)","group_id" :appDel.groupIdForPaymentHistory,"offset":"\(offset)","limit":"\(limit)", "filter_endDate":startDateTimeStampString,"filter_startDate": fromDateTimeStampString]
        }
        else
        {
            param = ["type":"request","user_id":UserDefaults.standard.value(forKey: "userId") as! String,"offset":"\(offset)","limit":"\(limit)", "filter_endDate":startDateTimeStampString,"filter_startDate": fromDateTimeStampString]
        }
        //                let param = ["type":"request","user_id":UserDefaults.standard.value(forKey: "userId") as! String]
         print(param)
        ActivityIndicatorView.show("Please wait....")
        networkProvider.request(.paymentHistoryList(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    ActivityIndicatorView.hiding()
                    let jsonData =  JSON(response.data)
                    self.isNewDataLoading = false
                     print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        
                        if self.offset == 0{
                            ActivityIndicatorView.hiding()
                        }else{
                            self.tblViewFund.hideLoading()
                        }
                        if let response = jsonData["data"].array{
                            if response.count == 0{
                                self.stopPagination = true
                            }
                            self.ArrayOfFundRequestpData.append(contentsOf: response)
                            if self.offset != 0{
                                if response.count > 0 {
                                    self.insertRowsAtTableView(responseCount: response.count)
                                }
                            }
                            self.tableviewreloadwithData()
                            
                            //                                    self.tableviewreloadwithData(data: response)
                            //                                    self.ArrayOfFundRequestpData = response
                            //                                    print(self.ArrayOfFundRequestpData)
                            //                                    print(self.ArrayOfFundRequestpData.count)
                            //                                    self.viewOfFundList.tableviewreloadwithData(data: self.ArrayOfFundRequestpData)
                            //
                        }
                        else{
                            if self.offset == 0{
                                ActivityIndicatorView.hiding()
                            }
                            else{
                                self.tblViewFund.hideLoading()
                                
                            }
                            self.viewOfNoData.isHidden = false
                            self.tblViewFund.isHidden = true
                            Helpers.showAlertDialog(message: "Oops! This content is no longer available.", target: self.superController)
                            
                            //                            self.superController.displayAlertChoice(alertStr: "Oops! This content is no longer available.", title: "", completion: { (result) in
                            //                                self.superController.navigationController?.popViewController(animated: true)
                            //                            })
                        }
                        
                    }
                    else if response.statusCode == 401{
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getPaymentFundList()
                        }
                    }
                    else{
                        
                        if self.offset == 0{
                            ActivityIndicatorView.hiding()
                        }
                        else{
                            self.tblViewFund.hideLoading()
                            
                        }
                        //                        self.superController.displayAlertChoice(alertStr: "Oops! This content is no longer available.", title: "", completion: { (result) in
                        //                            self.superController.navigationController?.popViewController(animated: true)
                        //                        })
                        
                        self.viewOfNoData.isHidden = false
                        self.tblViewFund.isHidden = true
                        Helpers.showAlertDialog(message: "Oops! This content is no longer available.", target: self.superController)
                        
                        
                        
                    }
                    //                        self.navigationController?.popViewController(animated: false)
                }catch  _ {
                    if self.offset == 0{
                        ActivityIndicatorView.hiding()
                    }
                    else{
                        self.tblViewFund.hideLoading()
                        
                    }
                    ActivityIndicatorView.hiding()
                    //  Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure(  _):
                if self.offset == 0{
                    ActivityIndicatorView.hiding()
                }
                else{
                    self.tblViewFund.hideLoading()
                    
                }
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    func insertRowsAtTableView(responseCount: Int){
        
        var indexPaths = [IndexPath]()
        
        for i in 0 ..< responseCount{
            let indx = ArrayOfFundRequestpData.count - responseCount
            let indexPath = indx + i
            indexPaths.append(IndexPath(row: indexPath, section: 0))
        }
        
        // print(indexPaths)
        self.tblViewFund.beginUpdates()
        self.tblViewFund.insertRows(at: indexPaths, with: .none)
        self.tblViewFund.endUpdates()
    }
    
    func tableviewreloadwithData()
    {
        //        self.ArrayOfFundRequestpData = data
        if self.ArrayOfFundRequestpData.count == 0
        {
            viewOfNoData.isHidden = false
            tblViewFund.isHidden = true
        }
        else
        {
            viewOfNoData.isHidden = true
            tblViewFund.isHidden = false
            tblViewFund.delegate = self
            tblViewFund.dataSource = self
            self.tblViewFund.reloadData()
        }
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if fromRefresh{
            fromRefresh = false
        }
        else{
            var currentCellOffset = scrollView.contentOffset
            currentCellOffset.x += (self.tblViewFund.bounds.width);
            
            let indepath = self.tblViewFund.indexPathsForVisibleRows?.first
            //            if indepath != nil{
            //                self.viewCurrentPost(index: (indepath?.row)!)
            //            }
        }
    }
    /* */
    //    @IBAction func btnGroup_OnClick(_ sender: Any) {
    //       delegate.group_OnClick()
    //    }
    //
    //    @IBAction func btnContact_OnClick(_ sender: Any) {
    //        delegate.Contact_OnClick()
    //    }
}


extension FundView:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == txtSearch
        {
            // selectWebAPI()
            if textField.text!.count > 0{
                btnSearchReset.isHidden = false
            }
            else{
                btnSearchReset.isHidden = true
            }
            textField.endEditing(true)
        }
        searchTxt = txtSearch.text!
        
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        self.endEditing(true)
        if textField == txtSearch
        {
            if textField.text!.count > 0{
                btnSearchReset.isHidden = false
            }
            else{
                btnSearchReset.isHidden = true
            }
            searchTxt = txtSearch.text!
            offset = 0
            
            
            self.getFamilyPaymentFundList()
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
            // selectWebAPI()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
            // selectWebAPI()
        }
        return true
    }
    
    //MARK:- SambagDatePickerViewController Delegates
    func didSelectDate(result:String,dateTag:Int){
        
        if dateTag == 0{
            selectedFromDate = "\(result)   00:00 a"
            if checkDateHigherThanCurrentDate(selectedDatestring: selectedFromDate){
                showToast(message: "Please select start date lower than today")
            }
            else{
                lblFromDate.text = Helpers.sambagTimeDisplaywithoutTime2(dateAsString: "\(result)")
                fromDateTimestamp = self.convertTimetoTimestamp(date: self.selectedFromDate)
                if fromDateTimestamp != 0{
                    fromDateTimeStampString = "\(fromDateTimestamp!)"
                }
                //            self.calculateToDateUsingDuration(dateString: "\(result)")
                self.selectedFromDateFormatted = self.convertDateFormat(date: selectedFromDate)
            }
        }
        else{
            selectedStartDate = "\(result)   11:59 p"
            // print(selectedStartDate)
            if checkDateHigherThanCurrentDate(selectedDatestring: "\(result)"){
                showToast(message: "Please select end date lower than today")
            }
            else{
                lblToDate.text = Helpers.sambagTimeDisplaywithoutTime2(dateAsString: "\(result)")
                startDateTimestamp = self.convertTimetoTimestamp(date: self.selectedStartDate)
                if startDateTimestamp != 0{
                    startDateTimeStampString = "\(startDateTimestamp!)"
                }
                self.selectedStartDateFormatted = self.convertDateFormat(date: selectedStartDate)
            }
        }
        
        if !fromDateTimeStampString.isEmpty && !startDateTimeStampString.isEmpty{
            btnDone.isEnabled = true
        }
        else{
            btnDone.isEnabled = false
        }
    }
    
    func sambagDatePickerDidSet(_ viewController: SambagDatePickerViewController, result: SambagDatePickerResult) {
        if dateTag == 1{
            selectedFromDate = "\(result)   00:00 a"
            if checkDateHigherThanCurrentDate(selectedDatestring: selectedFromDate){
                showToast(message: "Please select start date lower than today")
            }
            else{
                lblFromDate.text = Helpers.sambagTimeDisplaywithoutTime2(dateAsString: "\(result)")
                fromDateTimestamp = self.convertTimetoTimestamp(date: self.selectedFromDate)
                if fromDateTimestamp != 0{
                    fromDateTimeStampString = "\(fromDateTimestamp!)"
                    
                }
                //            self.calculateToDateUsingDuration(dateString: "\(result)")
                self.selectedFromDateFormatted = self.convertDateFormat(date: selectedFromDate)
            }
        }
        else{
            selectedStartDate = "\(result)   11:59 p"
            if checkDateHigherThanCurrentDate(selectedDatestring: "\(result)"){
                showToast(message: "Please select end date lower than today")
            }
            else{
                lblToDate.text = Helpers.sambagTimeDisplaywithoutTime2(dateAsString: "\(result)")
                startDateTimestamp = self.convertTimetoTimestamp(date: self.selectedStartDate)
                if startDateTimestamp != 0{
                    startDateTimeStampString = "\(startDateTimestamp!)"
                }
                self.selectedStartDateFormatted = self.convertDateFormat(date: selectedStartDate)
            }
        }
        
        if !fromDateTimeStampString.isEmpty && !startDateTimeStampString.isEmpty{
            btnDone.isEnabled = true
        }
        else{
            btnDone.isEnabled = false
        }
        
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func sambagDatePickerDidCancel(_ viewController: SambagDatePickerViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    //MARK: Custom Date Functions
    
    func checkDateHigherThanCurrentDate (selectedDatestring:String)->Bool{
        print(selectedDatestring)
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"
        
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        let convertedDateNow = formatter.string(from: Date())
        let convertedDateNow2 = formatter.date(from: convertedDateNow)
        let convertSelectedD = formatter.date(from: selectedDatestring)
        
        formatter2.timeZone = NSTimeZone.local
        let dateNow = formatter2.string(from: convertedDateNow2!)
        print(dateNow)
        
        if convertSelectedD?.compare(convertedDateNow2!) == ComparisonResult.orderedDescending{
            return true
        }
        else{
            return false
        }
        
    }
    
    func convertDateFormat(date:String)->String{
        do{
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy hh:mm a"
            //        let convertedDate = formatter.date(from: "16-09-2017 2:00 PM")
            if  let convertedDate = try formatter.date(from: date){
                //                formatter.timeZone = TimeZone(identifier: "UTC")
                formatter.timeZone = NSTimeZone.local
                let date = formatter.string(from: convertedDate)
                // print(date)
                let formatter2 = DateFormatter()
                formatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
                let convertedDate2 = formatter.date(from: date)
                formatter2.timeZone = NSTimeZone.local
                let date2 = formatter2.string(from: convertedDate2!)
                // print(date2)
                
                return date2
            }
            else{
                return ""
            }
            
        }catch{
            return ""
        }
    }
    
    func convertThenCallCalculate(dateString:String){
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd yyyy"
        
        let convertedDate = formatter.date(from: dateString)
        
        formatter.dateFormat = "dd/MM/yyyy"
        let dateFormatted = formatter.string(from: convertedDate!)
        //           calculateToDateUsingDuration(dateString: dateFormatted)
    }
    
    /* func calculateToDateUsingDuration(dateString:String){
     let formatter = DateFormatter()
     formatter.dateFormat = "dd/MM/yyyy"
     formatter.timeZone = NSTimeZone.local
     formatter.locale = .current
     let convertedDate = formatter.date(from: dateString)
     
     let toDate = convertedDate?.addDay(n: numberofDays)
     // print(toDate)
     print(convertedDate)
     
     formatter.dateFormat = "MMM dd yyyy"
     if membershipTypeId == 4{
     txtMembershipTo.text = "Life Time"
     }
     else{
     txtMembershipTo.text = formatter.string(from: toDate!)
     }
     
     }*/
    
    func convertTimetoTimestamp(date:String)->Int{
        let formatter = DateFormatter()
        
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"
        //        let convertedDate = formatter.date(from: "16-09-2017 2:00 PM")
        let convertedDate = formatter.date(from: date)
        
        //        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.timeZone = NSTimeZone.local
        let date = formatter.string(from: convertedDate!)
        // print(date)
        let convertedDate2 = formatter.date(from: date)
        
        let timestamp = convertedDate2!.timeIntervalSince1970
        // print(timestamp)
        
        return Int(timestamp)
        
    }
    
    //MARK:- show toast
    func showToast(message : String) {
         
         let toastLabel = UILabel(frame: CGRect(x: 20, y: self.frame.size.height/2-30, width: self.frame.width-40, height: 60))
         toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
         toastLabel.textColor = UIColor.white
         toastLabel.textAlignment = .center;
         toastLabel.numberOfLines = 2
         toastLabel.font = UIFont(name: "Montserrat-Light", size: 10.0)
         toastLabel.adjustsFontSizeToFitWidth = true
         toastLabel.text = message
         toastLabel.alpha = 1.0
         toastLabel.layer.cornerRadius = 10;
         toastLabel.clipsToBounds  =  true
         self.addSubview(toastLabel)
         UIView.animate(withDuration: 6.0, delay: 0.3, options: .curveEaseOut, animations: {
             toastLabel.alpha = 0.0
         }, completion: {(isCompleted) in
             toastLabel.removeFromSuperview()
         })
     }
}
