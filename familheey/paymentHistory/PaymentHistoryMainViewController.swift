//
//  PaymentHistoryMainViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 05/08/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit
import MXSegmentedPager
import Moya
import SwiftyJSON

class PaymentHistoryMainViewController: MXSegmentedPagerController  {
    
//    var isFromFamily = false
    var memberId = 0
    var groupId = ""
    var user_name = ""
    var family_name = ""

    var arrayOfTitle  = ["MEMBERSHIP","FUND REQUEST"]
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    var ArrayOfMemberShipData = [JSON]()
    var ArrayOfFundRequestpData = [JSON]()
    var arrayOfViews = [UIView]()
    @IBOutlet var viewOfFundList: FundView!
    @IBOutlet var viewOfMembershipList: MemberShipView!
    
    //    @IBOutlet weak var tblViewOfFundRequest: UITableView!
    @IBOutlet var headerView: UIView!
    //    @IBOutlet weak var viewOfList: MXSegmentedPager!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        viewOfMembershipList.setUpView(memberid: self.memberId, GroupId: self.groupId)
        arrayOfViews = [viewOfMembershipList,viewOfFundList]
        
        viewOfMembershipList.superController = self
        viewOfFundList.superController = self
        if appDel.paymentHistoryFromFamily{
            print("###\(memberId)")
//            viewOfMembershipList.isFromFamily = self.isFromFamily
//            viewOfMembershipList.memberId = self.memberId
//            viewOfMembershipList.groupId = self.groupId
            
//            viewOfFundList.isFromFamily = self.isFromFamily
//            viewOfFundList.memberId = self.memberId
//            viewOfFundList.groupId = self.groupId
            print(user_name)
            if user_name != ""
            {
                lblTitle.text = "\(user_name) Payments"

            }
            else
            {
                lblTitle.text = "\(family_name) Payments"

            }
            
        }
        
        //        viewOfList.dataSource = self
        //        viewOfList.delegate = self
        //        self.tblViewOfFundRequest.register(UITableViewCell.self, forCellReuseIdentifier: "PaymentFundTableViewCell")
        //        tblViewOfFundRequest.rowHeight = UITableView.automaticDimension
        //              tblViewOfFundRequest.estimatedRowHeight = 160
        ////        tblViewOfFundRequest.frame = viewOfList.frame
        //        self.getPaymentFundList()
        
        
        
        //        self.getPaymentMemberList()
        //
        segmentedPager.segmentedControl.selectionIndicatorLocation = .down
        //        segmentedPager.segmentedControl.backgroundColor = .yellow
        //        segmentedPager.segmentedControl.borderType = .top
        //        segmentedPager.segmentedControl.borderColor = UIColor.yellow
        // segmentedPager.segmentedControl.borderWidth = 0.5
        
        segmentedPager.backgroundColor = .white
        //                segmentedPager.parallaxHeader.delegate = self
        // Parallax Header
        segmentedPager.parallaxHeader.view = headerView
        segmentedPager.parallaxHeader.mode = .fill
        segmentedPager.parallaxHeader.height = 60
        segmentedPager.parallaxHeader.minimumHeight = 100
        segmentedPager.segmentedControl.selectionIndicatorHeight = 1
        segmentedPager.segmentedControl.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.darkGray, NSAttributedString.Key.font :UIFont.systemFont(ofSize: 16)]
        segmentedPager.segmentedControl.selectedTitleTextAttributes = [NSAttributedString.Key.foregroundColor :#colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1) , NSAttributedString.Key.font :UIFont.boldSystemFont(ofSize: 18)]
        segmentedPager.segmentedControl.selectionStyle = .textWidthStripe
        segmentedPager.segmentedControl.selectionIndicatorColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
        
        // Do any additional setup after loading the view.
    }
    


    @IBAction func onClickBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    // Asks the data source to return the number of pages in the segmented pager.
    override func numberOfPages(in segmentedPager: MXSegmentedPager) -> Int {
        return arrayOfTitle.count
    }
    
    // Asks the data source for a title realted to a particular page of the segmented pager.
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, titleForSectionAt index: Int) -> String {
        return arrayOfTitle[index]
    }
    
    // Asks the data source for a view to insert in a particular page of the pager.
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, viewForPageAt index: Int) -> UIView {
        return self.arrayOfViews[index]
        //        if index == 0
        //        {
        //            let label = UILabel()
        //                 label.text = "Page \(index)"
        //                 label.textAlignment = .center
        //                 return label;        }
        //        else
        //        {
        ////            self.tblViewOfFundRequest.dataSource = self
        ////                                         self.tblViewOfFundRequest.delegate = self
        ////                                         self.tblViewOfFundRequest.reloadData()
        //                   return self.viewOfList
        //
        //        }
    }
    
    
    //        func getPaymentFundList(){
    //            let param = ["type":"request","user_id":UserDefaults.standard.value(forKey: "userId") as! String]
    //
    //            ActivityIndicatorView.show("Please wait....")
    //            networkProvider.request(.paymentHistoryList(parameter: param)) { (result) in
    //                switch result{
    //                case .success( let response):
    //                    do{
    //                        ActivityIndicatorView.hiding()
    //                        let jsonData =  JSON(response.data)
    //                        print("view contents : \(jsonData)")
    //                        if response.statusCode == 200{
    //                            if let response = jsonData["data"].array{
    //                                self.ArrayOfFundRequestpData = response
    //                                print(self.ArrayOfFundRequestpData)
    //                                print(self.ArrayOfFundRequestpData.count)
    //                                self.viewOfFundList.tableviewreloadwithData(data: self.ArrayOfFundRequestpData)
    //
    //                            }
    //                            else{
    //                                self.displayAlertChoice(alertStr: "Oops! This content is no longer available.", title: "", completion: { (result) in
    //                                    self.navigationController?.popViewController(animated: true)
    //                                })
    //                            }
    //
    //                        }
    //                        else{
    //
    //                            self.displayAlertChoice(alertStr: "Oops! This content is no longer available.", title: "", completion: { (result) in
    //                                self.navigationController?.popViewController(animated: true)
    //                            })
    //
    //
    //                        }
    //                        //                        self.navigationController?.popViewController(animated: false)
    //                    }catch  _ {
    //                        ActivityIndicatorView.hiding()
    //                        //  Helpers.showAlertDialog(message: err.localizedDescription, target: self)
    //                    }
    //                case .failure(  _):
    //                    ActivityIndicatorView.hiding()
    //                    //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
    //                    break
    //                }
    //            }
    //        }
    
    //            func getPaymentMemberList(){
    //                let param = ["type":"membership","user_id":UserDefaults.standard.value(forKey: "userId") as! String,"offset":"0","limit":"10"]
    //
    //                ActivityIndicatorView.show("Please wait....")
    //                networkProvider.request(.paymentHistoryList(parameter: param)) { (result) in
    //                    switch result{
    //                    case .success( let response):
    //                        do{
    //                            ActivityIndicatorView.hiding()
    //                            let jsonData =  JSON(response.data)
    //                            print("view contents : \(jsonData)")
    //                            if response.statusCode == 200{
    //                                if let response = jsonData["data"].array{
    //                                    self.ArrayOfMemberShipData = response
    //                                    print(self.ArrayOfMemberShipData)
    //                                    print(self.ArrayOfMemberShipData.count)
    //                                    self.viewOfMembershipList.tableviewreloadwithData(data: self.ArrayOfMemberShipData)
    //
    //                                }
    //                                else{
    //                                    self.displayAlertChoice(alertStr: "Oops! This content is no longer available.", title: "", completion: { (result) in
    //                                        self.navigationController?.popViewController(animated: true)
    //                                    })
    //                                }
    //
    //                            }
    //                            else{
    //
    //                                self.displayAlertChoice(alertStr: "Oops! This content is no longer available.", title: "", completion: { (result) in
    //                                    self.navigationController?.popViewController(animated: true)
    //                                })
    //
    //
    //                            }
    //                            //                        self.navigationController?.popViewController(animated: false)
    //                        }catch  _ {
    //                            ActivityIndicatorView.hiding()
    //                            //  Helpers.showAlertDialog(message: err.localizedDescription, target: self)
    //                        }
    //                    case .failure( _):
    //                        ActivityIndicatorView.hiding()
    //                        //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
    //                        break
    //                    }
    //                }
    //            }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


