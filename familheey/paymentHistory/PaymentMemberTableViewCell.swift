//
//  PaymentMemberTableViewCell.swift
//  familheey
//
//  Created by Innovation Incubator on 05/08/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit

class PaymentMemberTableViewCell: UITableViewCell {

    @IBOutlet weak var lblPaidDate: UILabel!
    @IBOutlet weak var lblPaymentDue: UILabel!
    @IBOutlet weak var bgView: UIView!
       @IBOutlet weak var imgOfUser: UIImageView!
       @IBOutlet weak var btnImage: UIButton!
       @IBOutlet weak var lblNameOfUser: UILabel!

       @IBOutlet weak var lblType: UILabel!
       
       @IBOutlet weak var lblDate: UILabel!
       @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var viewOfNote: UIView!
    @IBOutlet weak var btnNotes: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
