//
//  ImagePreviewViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 15/11/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit
import SwiftyJSON
import AVKit
import Kingfisher
import SafariServices
import Moya

class imagePreviewCollectionViewCell: UICollectionViewCell{
    
    @IBOutlet weak var imgPreview: UIImageView!
    
}

class ImagePreviewViewController: UIViewController {
    @IBOutlet weak var rightMenu_view: UIView!
    @IBOutlet weak var clctnViewImage: UICollectionView!
    
    
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    var arrayOfOptions = [String]()
    
    var panGestureRecognizer: UIPanGestureRecognizer?
    var originalPosition: CGPoint?
    var currentPositionTouched: CGPoint?
    var collectionViewLayout = UICollectionViewFlowLayout()
    var indexOfCellBeforeDragging = 0
    var Cellheight = CGFloat.init(0.0)
    
    var pastevents:Bool = false
    var imgDetails                           = JSON()
    
    var isFrom                               = ""
    var isCoverOrProfilePic                  = ""
    var ids                                  = ""
    
    var imageUrl                             = ""
    var inFor                                = ""
    var isOwn                                = ""
    var isAdmin                              = ""
    var groupId                              = ""
    var eventId                              = ""
    var selectedImg                          = UIImage()
    var pinchGesture = UIPinchGestureRecognizer()
    var originalCover:UIImage!
    var orignalPicData = NSData()
    var originalData = NSData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(panGestureAction(_:)))
         view.addGestureRecognizer(panGestureRecognizer!)
        
        clctnViewImage.delegate = self
        clctnViewImage.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    
    
        @objc func panGestureAction(_ panGesture: UIPanGestureRecognizer) {
           let translation = panGesture.translation(in: view)

           if panGesture.state == .began {
             originalPosition = view.center
             currentPositionTouched = panGesture.location(in: view)
            self.view.backgroundColor = .lightGray
           } else if panGesture.state == .changed {
               view.frame.origin = CGPoint(
                 x: translation.x,
                 y: translation.y
               )
           } else if panGesture.state == .ended {
             let velocity = panGesture.velocity(in: view)

             if velocity.y >= 1000 {
               UIView.animate(withDuration: 0.2
                 , animations: {
                   self.view.frame.origin = CGPoint(
                     x: self.view.frame.origin.x,
                     y: self.view.frame.size.height
                   )
                 }, completion: { (isCompleted) in
                   if isCompleted {
                    self.navigationController?.addFadeAnimation()
                           self.navigationController?.popViewController(animated: false)
    //                 self.dismiss(animated: false, completion: nil)
                   }
               })
             } else {
               UIView.animate(withDuration: 0.2, animations: {
                 self.view.center = self.originalPosition!
                self.view.backgroundColor = .black

               })
             }
           }
         }
    
    //MARK:- Button Actions
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickRightMenu(_ sender: Any) {
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


extension ImagePreviewViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imagePreviewCollectionViewCell", for: indexPath as IndexPath) as! imagePreviewCollectionViewCell
        cell.imgPreview.image = #imageLiteral(resourceName: "bgFirst")
        
        return cell
    }
    
    
}
