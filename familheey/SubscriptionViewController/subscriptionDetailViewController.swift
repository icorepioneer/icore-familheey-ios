//
//  subscriptionDetailViewController.swift
//  familheey
//
//  Created by familheey on 25/02/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit

class subscriptionDetailViewController: UIViewController {
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblCurrntPlan: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblExpiryOn: UILabel!
    @IBOutlet weak var lblRenewOn: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblUsername: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        self.lblUsername.text = (UserDefaults.standard.value(forKey: "user_fullname") as! String)
        //        print(appDel.subscriptionArr[0]["created_at"])
        if appDel.subscriptionArr.count > 0{
            if (appDel.subscriptionArr[0]["subscribed_date"]?.stringValue.count)! > 0{
                let tempStr = appDel.subscriptionArr[0]["subscribed_date"]?.stringValue.components(separatedBy: "Z").first
                let tepStr2 = tempStr?.components(separatedBy: "T")
                print(tepStr2)
                let t1 = tepStr2?.first
                let t2 = tepStr2?.last
                lblStartDate.text = "\(t1!) \(t2!)"
            }
            else{
            }
            if (appDel.subscriptionArr[0]["subscribe_expiry"]?.stringValue.count)! > 0{
                let tempStr = appDel.subscriptionArr[0]["subscribe_expiry"]?.stringValue.components(separatedBy: "Z").first
                let tepStr2 = tempStr?.components(separatedBy: "T")
                print(tepStr2)
                let t1 = tepStr2?.first
                let t2 = tepStr2?.last
                lblExpiryOn.text = "\(t1!) \(t2!)"
            }
            else{
            }
            if (appDel.subscriptionArr[0]["subscribe_expiry"]?.stringValue.count)! > 0{
                let tempStr = appDel.subscriptionArr[0]["subscribe_expiry"]?.stringValue.components(separatedBy: "Z").first
                let tepStr2 = tempStr?.components(separatedBy: "T")
                print(tepStr2)
                let t1 = tepStr2?.first
                let t2 = tepStr2?.last
                lblRenewOn.text = "\(t1!) \(t2!)"
            }
            else{
            }
        }
        
    }
    

    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    static  func convertTimeStampToDate(timestamp:String)->String
    {
        if timestamp != ""
        {
//            let date = Date(timeIntervalSince1970: Double(timestamp)!)
            
            
            
            let formatter = DateFormatter()
            formatter.dateFormat = "MMM dd yyyy hh:mm a"
            formatter.timeZone = TimeZone(identifier: "UTC")
            formatter.locale = .current
            
            
            let temp = formatter.date(from: timestamp)
            //            print(localDatef)
            let localDatef = formatter.string(from: temp!)
            return localDatef
        }
        else
        {
            return ""
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
