//
//  viewRequestsTableViewController.swift
//  familheey
//
//  Created by Giri on 25/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class viewRequestsTableViewController: UITableViewController {
    var allInvitations = [invitations]()

    override func viewDidLoad() {
        
        super.viewDidLoad()
        getAllInvitations()
    }

    
    //MARK:- Custom functions
    func getAllInvitations(){
        ActivityIndicatorView.show("Loading....")
        APIServiceManager.callServer.getAllinvitations(url: EndPoint.listallInvitation, user_id: UserDefaults.standard.value(forKey: "userId") as! String, success: { (response) in
            ActivityIndicatorView.hiding()
            if let results = response as! InvitationsModel?{
                self.allInvitations = results.result!
                self.tableView.reloadData()
            }
            
        }, failure: { (error) in
            ActivityIndicatorView.hiding()
        })
    }
    
    
    //MARK:- Tableview Delegates
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return self.allInvitations.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddMembers_RequestsTableViewCell", for: indexPath) as! AddMembers_RequestsTableViewCell
        
        let invitation = allInvitations[indexPath.row]
        
        cell.lblName.text = invitation.group_name
        cell.lblLocation.text = invitation.location
        
        if invitation.logo.count != 0{
            let url = "\(Helpers.imaginaryImageBaseUrl)"+"width=150&height=150&url="+"\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+invitation.logo
            let imgUrl = URL(string: url)
            cell.imageViewAvatar.kf.indicatorType = .activity

            cell.imageViewAvatar.kf.setImage(with: imgUrl, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
        }
        else{
            
        }
        
        cell.btnAccept.tag = indexPath.row
        cell.btnReject.tag = indexPath.row
        
        return cell
    }
    
    @IBAction func onClickAcceptAction(_ sender: UIButton) {
        print(sender.tag)
        let invitation = allInvitations[sender.tag]

        APIServiceManager.callServer.invitationResponse(url: EndPoint.invitationResp, tableId: "\(invitation.req_id)", user_id: UserDefaults.standard.value(forKey: "userId") as! String, group_id: "\(invitation.group_id)", status: "accepted", fromId: "\(invitation.from_id)", success: { (response) in
//            ActivityIndicatorView.hiding()

            if let results = response as! requestSuccessModel?{
                if results.status_code == 200{
                    self.getAllInvitations()
                }
                else{
                    ActivityIndicatorView.hiding()
                    self.displayAlert(alertStr: "Something went wrong!", title: "")
                }
            }
            
        }) { (error) in
            ActivityIndicatorView.hiding()

        }
        
    }
    @IBAction func onClickRejectAction(_ sender: UIButton) {
        print(sender.tag)
        let invitation = allInvitations[sender.tag]
        
        APIServiceManager.callServer.invitationResponse(url: EndPoint.invitationResp, tableId: "\(invitation.req_id)", user_id: UserDefaults.standard.value(forKey: "userId") as! String, group_id: "\(invitation.group_id)", status: "rejected", fromId: "\(invitation.from_id)", success: { (response) in
//            ActivityIndicatorView.hiding()

            if let results = response as! requestSuccessModel?{
                if results.status_code == 200{
                    self.getAllInvitations()
                }
                else{
                    ActivityIndicatorView.hiding()
                    self.displayAlert(alertStr: "Something went wrong!", title: "")
                }
            }
            
        }) { (error) in
            ActivityIndicatorView.hiding()
   
        }
        
    }
    
}
