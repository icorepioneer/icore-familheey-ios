//
//  CreateAnnouncmentViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 25/11/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Alamofire
import Moya
import SwiftyJSON
import YPImagePicker
import AVKit
import iOSDropDown
import ImageIO
import MobileCoreServices
import PDFKit
import QuickLookThumbnailing
import DKImagePickerController



class CreateAnnouncmentViewController: UIViewController,UITextFieldDelegate,FamilyOrPeopleSelectionDelegate ,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIDocumentPickerDelegate {
    
    
    //      var arrayOfPostTypewitharrow = [["text":"All Families","hidebool":true],["text":"Selected families","hidebool":false],["text":"Selected Connections","hidebool":false],["text":"Anyone","hidebool":true],["text":"Only Me","hidebool":true]]
    //    let dropDown = DropDown()
    //    @IBOutlet weak var dropDownView: UIView!
    //    var selectedItems = [YPMediaItem]()
    //    var selectedAttachments = NSMutableArray()
    //    let selectedImageV = UIImageView()
    
    @IBOutlet weak var heightOfDropDown: NSLayoutConstraint!
    @IBOutlet weak var btnPostType: UIButton!
    
    @IBOutlet weak var btnPost: UIButton!
    
    @IBOutlet weak var lblNavHeading: UILabel!
    
    @IBOutlet weak var swithSharable: UISwitch!
    @IBOutlet weak var swithConversation: UISwitch!
    
    @IBOutlet weak var lblNoFiles: UILabel!
    @IBOutlet weak var viewOfShade: UIView!
    @IBOutlet weak var tblViewDropList: UITableView!
    @IBOutlet weak var collViewOfAttachments: UICollectionView!
    @IBOutlet weak var selectedImageV: UIImageView!
    @IBOutlet weak var lblPostTypeStatus: UILabel!
    @IBOutlet weak var lblHeading2: UILabel!
    @IBOutlet weak var lblHeading1: UILabel!
    @IBOutlet weak var txtPostType: DropDown!
    
    @IBOutlet weak var heightOfPostSelection: NSLayoutConstraint!
    
    @IBOutlet weak var txtDescription: UITextView!
    
    
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    var postoptions = ["Images","Videos","Documents"]
    var fromEdit = false
    var fromGroup = false
    var selectedUserIDArr    = NSMutableArray()
    
    var editPostDetails = JSON()
    var arrayOfPostTypewitharrow = [["text":"All Families","hidebool":true],["text":"Selected families","hidebool":false]]
    var imageUploadStatus = false
    var vedioUploadStatus = false
    var documentUploadStatus = false
    var deleteAttachment:Bool = false
    var previousSelectedItemsexisted = Array<[String:Any]>()
    
    var imgDataArr = [Data]()
    var videoDataArr = [Data]()
    var documentDataArr = [Data]()
    var pickerController: DKImagePickerController!
    var selectedAttachments = NSMutableArray()
    var post_images:[JSON] = []
    var selectedItemsexisted = Array<[String:Any]>()
    
    
    var arrayOfSelectedUser = NSMutableArray()
    
    var arrayOfPostType = ["All Families",
                           "Selected families",
                           "Selected Connections",
                           "Anyone",
                           "Only Me"]
    var selectedPostType:String = ""
    var privacyType:String = ""
    var isConversation:Bool!
    var isSharing:Bool!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var sizeArray : [[String:String]] = [[:]]
    var pathExt = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        self.txtDescription.autocapitalizationType      = .sentences
        self.vedioUploadStatus = true
        self.heightOfDropDown.constant = CGFloat(40 * self.arrayOfPostTypewitharrow.count)
        pickerController = DKImagePickerController()
        
        
        
        self.tblViewDropList.dataSource = self
        self.tblViewDropList.delegate = self
        tblViewDropList.register(UINib(nibName: "postTypeDropdownCell", bundle: nil), forCellReuseIdentifier: "cell")
        tblViewDropList.isHidden = true
        self.viewOfShade.isHidden = true
        tblViewDropList.tableFooterView = UIView.init()
        
        tblViewDropList.reloadData()
        
        //        selectedAttachments = NSMutableArray.init()
        self.collViewOfAttachments.delegate = self
        self.collViewOfAttachments.dataSource = self
        Helpers.setleftView(textfield: txtPostType, customWidth: 15)
        txtPostType.delegate = self
        
        txtPostType.optionArray = self.arrayOfPostType.sorted()
        txtPostType.didSelect {(selectedText , index ,id) in
            print(selectedText)
            
            
            if selectedText == "All Families"
            {
                self.heightOfPostSelection.constant = 75
                self.lblPostTypeStatus.isHidden = true
                self.arrayOfSelectedUser = []
                self.selectedPostType = "all_family"
                self.privacyType = "public"
                
            }
            else if selectedText == "Selected families"
            {
                self.selectedPostType = "only_groups"
                self.privacyType = "public"
                let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "SelectFamilyOrPeopleViewController") as! SelectFamilyOrPeopleViewController
                vc.selectedIndex = 0
                vc.selectionDelegate = self
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if selectedText == "Selected Connections"
            {
                self.selectedPostType = "only_users"
                self.privacyType = "public"
                let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "SelectFamilyOrPeopleViewController") as! SelectFamilyOrPeopleViewController
                vc.selectedIndex = 1
                vc.selectionDelegate = self
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else  if selectedText == "Anyone"
            {
                self.selectedPostType = "public"
                self.privacyType = "public"
                self.heightOfPostSelection.constant = 75
                self.lblPostTypeStatus.isHidden = true
                self.arrayOfSelectedUser = []
                
            }
            else
            {
                self.selectedPostType = "private"
                self.privacyType = "private"
                self.heightOfPostSelection.constant = 75
                self.lblPostTypeStatus.isHidden = true
                self.arrayOfSelectedUser = []
            }
            
            
            
            
        }
        
        
        
        if fromEdit{
            
            
            
            self.heightOfPostSelection.constant = 0
            self.txtDescription.text = editPostDetails["snap_description"].stringValue
            if editPostDetails["is_shareable"].boolValue
            {
                self.swithSharable.isOn = true
                self.isSharing = true
                
            }
            else
            {
                self.swithSharable.isOn = false
                self.isSharing = false
                
            }
            
            if editPostDetails["conversation_enabled"].boolValue
            {
                self.swithConversation.isOn = true
                self.isConversation = true
                
            }
            else
            {
                self.swithConversation.isOn = false
                self.isConversation = false
                
            }
            
            
            self.txtPostType.isHidden = true
            self.lblHeading1.isHidden = true
            self.btnPostType.isHidden = true
            tblViewDropList.isHidden = true
            self.viewOfShade.isHidden = true
            
            self.lblNavHeading.text = "Update Announcement"
            self.btnPost.setTitle("Update", for: .normal)
            
            self.post_images = editPostDetails["post_attachment"].arrayValue
            
            
            if post_images.count != 0
            {
                for itemsRespone in post_images
                {
                    var dict = [String:Any]()
                    dict["existing"] = true
                    var dict2 = [String:String]()
                    dict2["filename"] = itemsRespone["filename"].stringValue
                    dict2["type"] = itemsRespone["type"].stringValue
                    print(dict)
                    if  itemsRespone["width"].stringValue  != ""
                    {
                        dict2["width"] = itemsRespone["width"].stringValue
                    }
                    if  itemsRespone["height"].stringValue  != ""
                    {
                        dict2["height"] = itemsRespone["height"].stringValue
                    }
                    if  itemsRespone["video_thumb"].stringValue  != ""
                    {
                        dict2["video_thumb"] = itemsRespone["video_thumb"].stringValue
                    }
                    dict["value"] = dict2
                    self.selectedAttachments.add(dict2)
                    self.selectedItemsexisted.append(dict)
                    
                }
                self.collViewOfAttachments.isHidden = false
                self.lblNoFiles.isHidden = true
                self.collViewOfAttachments.reloadData()
            }
            else
            {
                self.selectedAttachments = []
                self.selectedItemsexisted = []
                self.collViewOfAttachments.isHidden = true
                self.lblNoFiles.isHidden = false
                
            }
            
            
        }
        else
        {
            self.lblNavHeading.text = "Make an Announcement"
            self.btnPost.setTitle("Announce", for: .normal)
            self.txtPostType.isHidden = false
            self.lblHeading1.isHidden = false
            self.btnPostType.isHidden = false
            
            
            self.tblViewDropList.dataSource = self
            self.tblViewDropList.delegate = self
            tblViewDropList.register(UINib(nibName: "postTypeDropdownCell", bundle: nil), forCellReuseIdentifier: "cell")
            tblViewDropList.isHidden = true
            self.viewOfShade.isHidden = true
            tblViewDropList.tableFooterView = UIView.init()
            
            tblViewDropList.reloadData()
            
            self.heightOfPostSelection.constant = 75
            self.lblPostTypeStatus.isHidden = true
            
            self.isConversation = false
            self.isSharing = true
            self.collViewOfAttachments.isHidden = true
            self.lblNoFiles.isHidden = false
            
            self.selectedAttachments = []
            self.selectedItemsexisted = []
            print(self.selectedUserIDArr)
            
            if fromGroup {
                self.selctionCallback(SelectedUserId: self.selectedUserIDArr,currentIndex:0)
                self.selectedPostType = "only_groups"
                self.privacyType = "public"
                self.txtPostType.text = "Selected families"
                self.txtPostType.isUserInteractionEnabled = false
                
                
                
            }
            
        }
        self.setTextFiledPlaceholder()
        // Do any additional setup after loading the view.
    }
    
    //MARK:-custo methods
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.tblViewDropList.isHidden = true
        self.viewOfShade.isHidden = true
        
    }
    
    @IBAction func hideButton(_ sender: Any) {
        self.tblViewDropList.isHidden = true
        self.viewOfShade.isHidden = true
    }
    
    // MARK: setUpDropDown
    //    func setUpDropDown() {
    //
    //        dropDown.anchorView = dropDownView
    //        dropDown.dataSource = ["All Families",
    //                               "Selected families",
    //                               "Selected Connections",
    //                               "Anyone",
    //                               "Only Me"]
    //        dropDown.cellNib = UINib(nibName: "MyDropDownCell", bundle: nil)
    //        DropDown.init(anchorView: dropDownView, selectionAction: { (index, item) in
    //                        print("Selected item: \(item) at index: \(index)")
    //
    //        }, dataSource: ["All Families",
    //        "Selected families",
    //        "Selected Connections",
    //        "Anyone",
    //        "Only Me"], topOffset: CGPoint(0,0), bottomOffset: 0, cellConfiguration: { (index, item) -> String in
    //            <#code#>
    //        }) {
    //            <#code#>
    //        }
    //
    ////        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
    ////            guard let cell = cell as? MyDropDownCell else { return }
    ////
    ////            // Setup your custom UI components
    ////            cell.optionImageView.image = self.dropDownImageArray[index] // image to dropdownimage array
    ////            cell.optionCountLbl.text = String(self.dropDownCountArray[index])// image to dropdowncount array
    ////        }
    //        dropDown.selectionAction = { (index: Int, item: String) in
    //
    //
    //            print("Selected item: \(item) at index: \(index)")
    ////            self.isSearchDone = true
    ////            self.dropDownIndex = index
    ////            var statusId = Int()
    ////            if index == 0 {
    ////
    ////                // all vehicle selected
    ////                self.isSearchDone = false
    ////                self.oldBucketArray = nil
    ////                NotificationCenter.default.post(name: Notification.Name("getDataForMapNotificationIdentifier"), object: nil, userInfo: ["bucketArray":self.totalBuketArray!,"fromSearch": true,"isPlotFirstSearch": true])
    ////                self.totalCount = self.totalBuketArray!.count
    ////            } else if index == 1 {
    ////
    ////                //  selected running vehicles
    ////                statusId = 3
    ////                self.oldBucketArray = nil
    ////                let newBucketsArray = self.totalBuketArray!.filter({$0._source!.active_status! == statusId})
    ////                self.totalCount = newBucketsArray.count
    ////                NotificationCenter.default.post(name: Notification.Name("getDataForMapNotificationIdentifier"), object: nil, userInfo: ["bucketArray":newBucketsArray,"fromSearch": self.isSearchDone,"isPlotFirstSearch": true])
    ////            } else if index == 2 {
    ////
    ////                //  selected Parking vehicles
    ////                statusId = 2
    ////                self.oldBucketArray = nil
    ////                let newBucketsArray = self.totalBuketArray!.filter({$0._source!.active_status! == statusId})
    ////                self.totalCount = newBucketsArray.count
    ////                NotificationCenter.default.post(name: Notification.Name("getDataForMapNotificationIdentifier"), object: nil, userInfo: ["bucketArray":newBucketsArray,"fromSearch": self.isSearchDone,"isPlotFirstSearch": true])
    ////            } else if index == 3 {
    ////
    ////                //  selected Offline vehicles
    ////                statusId = 0
    ////                self.oldBucketArray = nil
    ////                let newBucketsArray = self.totalBuketArray!.filter({$0._source!.active_status! == statusId})
    ////                self.totalCount = newBucketsArray.count
    ////                NotificationCenter.default.post(name: Notification.Name("getDataForMapNotificationIdentifier"), object: nil, userInfo: ["bucketArray":newBucketsArray,"fromSearch": self.isSearchDone,"isPlotFirstSearch": true])
    ////            } else if index == 4 {
    ////
    ////                //  selected Idle vehicles
    ////                statusId = 1
    ////                self.oldBucketArray = nil
    ////                let newBucketsArray = self.totalBuketArray!.filter({$0._source!.active_status! == statusId})
    ////                self.totalCount = newBucketsArray.count
    ////                NotificationCenter.default.post(name: Notification.Name("getDataForMapNotificationIdentifier"), object: nil, userInfo: ["bucketArray":newBucketsArray,"fromSearch": self.isSearchDone,"isPlotFirstSearch": true])
    ////            }
    ////
    ////            self.frontDropdownLbl.text = "\(item) (\(self.totalCount))"
    //
    //        }
    //    }
    
    @IBAction func postTypeBtnPressed(_ sender: Any) {
        //
        //        dropDown.show()
        self.viewOfShade.isHidden = false
        
        tblViewDropList.isHidden = false
        
    }
    
    func setTextFiledPlaceholder(){
        
        self.lblHeading1.attributedText = Helpers.setAttributedPlaceHolder(placeHolderName: "Announce this to")
        
        self.lblHeading2.attributedText = Helpers.setAttributedPlaceHolder(placeHolderName: "What do you want to announce?")
    }
    
    func selectImage(index:Int){
        pickerController = DKImagePickerController()
        DKImageExtensionController.unregisterExtension(for:.camera)
        
        if index == 0{
            pickerController.assetType = .allPhotos
            pickerController.maxSelectableCount = 10
        }
        else{
            pickerController.assetType = .allVideos
            pickerController.maxSelectableCount = 1
            DKImageExtensionController.registerExtension(extensionClass: CustomCameraExtension.self, for: .camera)
        }
        
        pickerController.exportsWhenCompleted = true
        
        pickerController.deselectAll()
        
        pickerController.didSelectAssets = { [unowned self] (assets: [DKAsset]) in
            
            self.imgDataArr = []
            self.videoDataArr = []
            self.sizeArray = []
            
            self.previousSelectedItemsexisted = self.selectedItemsexisted
            for item in assets{
                var dict = [String:Any]()
                dict["existing"] = false
                dict["value"] = item
                self.selectedItemsexisted.append(dict)
                
            }
            if assets.count != 0{
                
                ActivityIndicatorView.show("Loading....")
                DKImageAssetExporter.sharedInstance.exportAssetsAsynchronously(assets: assets) { (info) in
                    ActivityIndicatorView.hiding()
                    for asset in assets {
                        switch asset.type {
                        case .photo:
                            if let localTemporaryPath = asset.localTemporaryPath,
                                let imageData = try? Data(contentsOf: localTemporaryPath) {
                                
                                let img = UIImage.init(data: imageData)
                                
                                self.sizeArray.append(["W":"\(String(describing: img!.size.width))","H":"\(String(describing:img!.size.height))"])
                                if let fixedData = img?.fixedOrientation.jpegData(compressionQuality: 0.75){
                                    self.imgDataArr.append(fixedData)
                                }
                                
                            }
                            
                            if self.imgDataArr.count == assets.count{
                                self.imageUploadStatus = false
                                self.uploadImageMultipleImages(arr: self.imgDataArr, sizeArray: self.sizeArray)
                                
                                if self.selectedItemsexisted.count != 0{
                                    self.collViewOfAttachments.isHidden = false
                                    self.lblNoFiles.isHidden = true
                                    self.collViewOfAttachments.reloadData()
                                    
                                }
                                else{
                                    self.collViewOfAttachments.isHidden = true
                                    self.lblNoFiles.isHidden = false
                                    
                                }
                            }
                            
                        case .video:
                            
                            if let localTemporaryPath = asset.localTemporaryPath{
                                
                                self.vedioUploadStatus = false
                                
                                asset.fetchFullScreenImage { (image, info) in
                                    AWSS3Manager.shared.uploadImage(image: image!, file_name: "video_thumb/", progress: { (progress) in
                                        
                                    }) { [weak self] (uploadedThumbUrl, error) in
                                        
                                        if let finalPathThumb = uploadedThumbUrl as? URL {
                                            
                                            print("Uploaded file url: " , finalPathThumb)
                                            AWSS3Manager.shared.uploadVideo(videoUrl: localTemporaryPath, folder: "post/", progress: { (progress) in
                                                print(progress)
                                                
                                            }) { [weak self] (uploadedFileUrl, error) in
                                                
                                                if let finalPath = uploadedFileUrl as? URL {
                                                    print("Uploaded file url: " , finalPath)
                                                    self?.vedioUploadStatus = true
                                                    self?.collViewOfAttachments.reloadData()
                                                    
                                                    var dict = [String:String]()
                                                    dict["filename"] = finalPath.lastPathComponent
                                                    dict["type"] = "video"
                                                    dict["video_thumb"] = "video_thumb/\(finalPathThumb.lastPathComponent)"
                                                    print(dict)
                                                    
                                                    self?.selectedAttachments.add(dict)
                                                    
                                                } else {
                                                    print("\(String(describing: error?.localizedDescription))")
                                                    self?.displayAlert(alertStr: "Unable to upload file, please try again", title: "")
                                                    
                                                    self?.deleteAttachment = true
                                                    self?.vedioUploadStatus = true
                                                    self?.selectedItemsexisted =  self!.previousSelectedItemsexisted
                                                    self?.deleteAttachment = false
                                                    
                                                    if self?.selectedItemsexisted.count != 0{
                                                        self?.collViewOfAttachments.isHidden = false
                                                        self?.lblNoFiles.isHidden = true
                                                        self?.collViewOfAttachments.reloadData()
                                                        
                                                    }
                                                    else{
                                                        self?.collViewOfAttachments.isHidden = true
                                                        self?.lblNoFiles.isHidden = false
                                                        
                                                    }
                                                }
                                            }
                                            
                                        } else {
                                            print("\(String(describing: error?.localizedDescription))")
                                            self?.displayAlert(alertStr: "Unable to upload file, please try again", title: "")
                                            self?.deleteAttachment = true
                                            self?.vedioUploadStatus = true
                                            self?.selectedItemsexisted =  self!.previousSelectedItemsexisted
                                            self?.deleteAttachment = false
                                            
                                            if self?.selectedItemsexisted.count != 0{
                                                self?.collViewOfAttachments.isHidden = false
                                                self?.lblNoFiles.isHidden = true
                                                self?.collViewOfAttachments.reloadData()
                                                
                                            }
                                            else{
                                                self?.collViewOfAttachments.isHidden = true
                                                self?.lblNoFiles.isHidden = false
                                                
                                            }
                                            
                                        }
                                    }
                                }
                                
                                if self.selectedItemsexisted.count != 0{
                                    self.collViewOfAttachments.isHidden = false
                                    self.lblNoFiles.isHidden = true
                                    self.collViewOfAttachments.reloadData()
                                    
                                }
                                else{
                                    self.collViewOfAttachments.isHidden = true
                                    self.lblNoFiles.isHidden = false
                                    
                                }
                            }
                        }
                    }
                }
            }
            
        }
        
        if pickerController.UIDelegate == nil {
            pickerController.UIDelegate = AssetClickHandler()
        }
        
        self.present(pickerController, animated: true) {}
        
    }
    
    @IBAction func btnBack_Onclick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAttachFile_OnClick(_ sender: Any) {
        
        
        if self.imgDataArr.count != 0  && !imageUploadStatus
        {
            
            Helpers.showAlertDialog(message: "Uploading images Please Wait a moment.", target: self)
        }
        else if  !vedioUploadStatus{
//            self.videoDataArr.count != 0 &&
            Helpers.showAlertDialog(message: "Uploading video Please Wait a moment.", target: self)
            
            
        }
        else if self.documentDataArr.count != 0 && !documentUploadStatus{
            
            Helpers.showAlertDialog(message: "Uploading document Please Wait a moment.", target: self)
        }
        else
        {
            showActionSheet(titleArr: postoptions as NSArray, title: "Select options") { (index) in
                if index == 0{
                    self.selectImage(index: index)
                }
                else if index == 1{
                    self.selectImage(index: index)
                }
                else if index == 100{
                }
                else
                {
                    //                let types = [kUTTypePDF, kUTTypeText, kUTTypeRTF, kUTTypeSpreadsheet, kUTTypePNG, kUTTypeJPEG, kUTTypeGIF, "com.microsoft.word.doc" as CFString, "org.openxmlformats.wordprocessingml.document" as CFString]
                    
                    let types = [kUTTypePDF, kUTTypeText, kUTTypeRTF, kUTTypeSpreadsheet, "com.microsoft.word.doc" as CFString]
                    let documentPicker = UIDocumentPickerViewController(documentTypes: types as [String], in: .import)
                    
                    documentPicker.allowsMultipleSelection = true
                    //                    let documentPicker = UIDocumentPickerViewController(documentTypes: [String(kUTTypeText),String(kUTTypeContent),String(kUTTypeItem),String(kUTTypeData)], in: .import)
                    
                    //Call Delegate
                    
                    
                    documentPicker.delegate = self
                    
                    self.present(documentPicker, animated: true)
                    
                    
                }
            }
        }
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        
        print(urls)
        
        self.documentDataArr = []
        self.previousSelectedItemsexisted = self.selectedItemsexisted
        for currentUrl in urls
        {
            //        let videoUrl = video.url
            print(currentUrl.pathExtension)
            pathExt = currentUrl.pathExtension
            
            do {
                let documentData = try NSData(contentsOf: currentUrl, options: NSData.ReadingOptions())
                self.documentDataArr.append(documentData as Data)
                
                var dict = [String:Any]()
                dict["existing"] = false
                dict["document"] = currentUrl
                self.selectedItemsexisted.append(dict)
                
            } catch {
                print(error)
            }
        }
        print(self.selectedItemsexisted)
        if self.documentDataArr.count != 0
        {
            self.documentUploadStatus = false
            self.uploadDocumentMultiple(arr: self.documentDataArr)
            
        }
        
        if self.selectedItemsexisted.count != 0
        {
            self.collViewOfAttachments.isHidden = false
            self.lblNoFiles.isHidden = true
            self.collViewOfAttachments.reloadData()
            
        }
        else
        {
            self.collViewOfAttachments.isHidden = true
            self.lblNoFiles.isHidden = false
            
        }
        
    }
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        
        dismiss(animated: true, completion: nil)
        
    }
    //MARK: - Multiple Image Uploading
    
    //    func uploadImageMultipleImages(arr : [Data]){
    //
    //        let param = ["name" :"announcement"]
    //
    //
    ////        ActivityIndicatorView.show("Uploading...")
    //        Helpers.requestWithMultipleImages(fileParamName: "file", urlAppendString: "upload_file_tos3", imageData: arr, parameters: param, mimeType: "image/png") { (response) in
    //            print("Response : \(response)")
    //            self.imageUploadStatus = true
    //            self.collViewOfAttachments.reloadData()
    //            let arrayOfData = response["data"].arrayValue
    //
    //            for currentdata in arrayOfData{
    //                var dict = [String:String]()
    //                dict["filename"] = currentdata["filename"].stringValue
    //                dict["type"] = currentdata["type"].stringValue
    //                print(dict)
    //
    //                self.selectedAttachments.add(dict)
    //
    //            }
    //
    //
    //
    //            print(self.selectedAttachments)
    ////            ActivityIndicatorView.hiding()
    //
    //
    //
    //        }
    //    }
    
    
    func uploadImageMultipleImages(arr : [Data],sizeArray:[[String:String]]){
        let param = ["name" :"post"] as [String : Any]
        
        
        //        ActivityIndicatorView.show("Uploading...")
        Helpers.requestWithMultipleImages(fileParamName: "file", urlAppendString: "upload_file_tos3", imageData: arr, parameters: param, mimeType: "image/png") { (response) in
            print("Response : \(response)")
            if response["data"].arrayValue.count != 0{
                self.imageUploadStatus = true
                self.collViewOfAttachments.reloadData()
                let arrayOfData = response["data"].arrayValue
                
                for currentdata in arrayOfData{
                    var dict = [String:String]()
                    dict["filename"] = currentdata["filename"].stringValue
                    dict["type"] = currentdata["type"].stringValue
                    dict["width"] = sizeArray[0]["W"]
                    dict["height"] = sizeArray[0]["H"]
                    print(dict)
                    
                    self.selectedAttachments.add(dict)
                    
                }
                
                print(self.selectedAttachments)
                //            ActivityIndicatorView.hiding()
            }
            else{
                //failed Condition
                self.imageUploadStatus = true
                self.imgDataArr = []
                
                if self.deleteAttachment
                {
                    self.selectedItemsexisted =  self.previousSelectedItemsexisted
                    
                    //                self.imageUploadStatus = true
                    self.deleteAttachment = false
                    
                    print(self.selectedItemsexisted)
                    print(self.selectedItemsexisted.count)
                    
                    
                    
                    if self.selectedItemsexisted.count != 0{
                        self.collViewOfAttachments.isHidden = false
                        self.lblNoFiles.isHidden = true
                        self.collViewOfAttachments.reloadData()
                        
                    }
                    else{
                        self.collViewOfAttachments.isHidden = true
                        self.lblNoFiles.isHidden = false
                        
                    }
                    
                }
            }
        }
    }
    
    func uploadVideoMultiple(arr : [Data]){
        let param = ["name" :"post"]
        
        //        ActivityIndicatorView.show("Uploading...")
        Helpers.requestWithMultipleImages(fileParamName: "file", urlAppendString: "upload_file_tos3", imageData: arr, parameters: param, mimeType: "video/quicktime/m4v") { (response) in
            print("Response : \(response)")
            if response["data"].arrayValue.count != 0{
                self.vedioUploadStatus = true
                self.collViewOfAttachments.reloadData()
                
                let arrayOfData = response["data"].arrayValue
                
                for currentdata in arrayOfData{
                    var dict = [String:String]()
                    dict["filename"] = currentdata["filename"].stringValue
                    dict["type"] = currentdata["type"].stringValue
                    dict["video_thumb"] = currentdata["video_thumb"].stringValue
                    print(dict)
                    
                    self.selectedAttachments.add(dict)
                    
                }
                print(self.selectedAttachments)
                //            ActivityIndicatorView.hiding()
            }
            else{
                //failed Condition
                self.vedioUploadStatus = true
                self.videoDataArr = []
                
                if self.deleteAttachment
                {
                    self.selectedItemsexisted =  self.previousSelectedItemsexisted
                    
                    //                 self.vedioUploadStatus = true
                    self.deleteAttachment = false
                    
                    print(self.selectedItemsexisted)
                    print(self.selectedItemsexisted.count)
                    
                    
                    
                    if self.selectedItemsexisted.count != 0{
                        self.collViewOfAttachments.isHidden = false
                        self.lblNoFiles.isHidden = true
                        self.collViewOfAttachments.reloadData()
                        
                    }
                    else{
                        self.collViewOfAttachments.isHidden = true
                        self.lblNoFiles.isHidden = false
                        
                    }
                    
                }
            }
        }
    }
    
    func uploadDocumentMultiple(arr : [Data]){
        
        let param = ["name" :"post"]
        
        // ActivityIndicatorView.show("Uploading...")
        Helpers.requestWithMultipleImages(fileParamName: "file", urlAppendString: "upload_file_tos3", imageData: arr, parameters: param, mimeType: pathExt) { (response) in
            print("Response : \(response)")
            if response["data"].arrayValue.count != 0{
                self.documentUploadStatus = true
                self.collViewOfAttachments.reloadData()
                
                let arrayOfData = response["data"].arrayValue
                
                for currentdata in arrayOfData{
                    var dict = [String:String]()
                    dict["filename"] = currentdata["filename"].stringValue
                    dict["type"] = currentdata["type"].stringValue
                    print(dict)
                    
                    self.selectedAttachments.add(dict)
                    
                }
                
                print(self.selectedAttachments)
            }
            else{
                //failed Condition
                self.documentUploadStatus = true
                self.documentDataArr = []
                
                if self.deleteAttachment
                {
                    self.selectedItemsexisted =  self.previousSelectedItemsexisted
                    
                    //                self.imageUploadStatus = true
                    self.deleteAttachment = false
                    
                    print(self.selectedItemsexisted)
                    print(self.selectedItemsexisted.count)
                    
                    
                    
                    if self.selectedItemsexisted.count != 0{
                        self.collViewOfAttachments.isHidden = false
                        self.lblNoFiles.isHidden = true
                        self.collViewOfAttachments.reloadData()
                        
                    }
                    else{
                        self.collViewOfAttachments.isHidden = true
                        self.lblNoFiles.isHidden = false
                        
                    }
                    
                }
            }
            //  ActivityIndicatorView.hiding()
            
        }
    }
    //MARK: - Upload Image
    
    //    func uploadImage(img : UIImage){
    //
    //        var param = [String:Any]()
    //        var imgData = Data()
    //
    //        let mime    = "image/png"
    //
    //
    //                let img2 = img.resized(withPercentage: 0.5)
    //
    //                imgData = (img2?.pngData())!
    //
    //                param = ["name" :"post"]
    //
    //
    //
    //
    //
    //        ActivityIndicatorView.show("Uploading...")
    //        let tData = NSData()
    //        Helpers.requestWith(fileParamName: "file", originalCover: "", urlAppendString: "upload_file_tos3", imageData: imgData, originalImg: tData as Data, parameters: param, mimeType: mime) { (response) in
    //
    //            print("response : \(response)")
    //            var dict = [String:String]()
    //            dict["filename"] = response["filename"].stringValue
    //            dict["type"] = response["type"].stringValue
    //            print(dict)
    //
    //            self.selectedAttachments.add(dict)
    //
    //            print(self.selectedAttachments)
    //
    //            ActivityIndicatorView.hiding()
    ////            self.callViewContents()
    //        }
    //    }
    
    //    func uploadVideo(vedioUrl : URL){
    //
    //        var param = [String:Any]()
    //        param = ["name" :"post"]
    //
    //
    //
    //        let mime    = "video/quicktime/m4v"
    //
    //        do {
    //            let videoData = try NSData(contentsOf: vedioUrl, options: NSData.ReadingOptions())
    ////            print(videoData)
    //            ActivityIndicatorView.show("Uploading...")
    //            let tData = NSData()
    //            Helpers.requestWith(fileParamName: "file", originalCover: "", urlAppendString: "upload_file_tos3", imageData: videoData as Data, originalImg: tData as Data, parameters: param, mimeType: mime) { (response) in
    //
    //                print("response : \(response)")
    //
    //                ActivityIndicatorView.hiding()
    //                var dict = [String:String]()
    //                dict["filename"] = response["filename"].stringValue
    //                dict["type"] = response["type"].stringValue
    //                print(dict)
    //
    //                self.selectedAttachments.add(dict)
    //                print(self.selectedAttachments)
    //
    //
    //                //            self.callViewContents()
    //            }
    //        } catch {
    //            print(error)
    //        }
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //    }
    @IBAction func btnSharableOnClick(_ sender: UISwitch) {
        if (sender.isOn == true){
            isSharing = true
            
        }
        else{
            isSharing = false
            
        }
        
    }
    @IBAction func btnConversation_OnClick(_ sender: UISwitch) {
        if (sender.isOn == true){
            isConversation = true
            
        }
        else{
            isConversation = false
        }
        
    }
    
    @IBAction func btnPost_OnClick(_ sender: Any) {
        if fromEdit
        {
            
            if DatavalidationForUpdateAnnouncement()
            {
                self.apiForUpdateAnnouncement()
                
            }
        }
        else{
            if DatavalidationForSubmit()
            {
                self.apiForCreateAnnouncement()
                
            }
        }
        
    }
    
    
    func apiForCreateAnnouncement() {
        
        //        "ticket_type":dataFromPrevious["ticket_type"]!,
        
        var parameter = [String:Any]()
        //        :all_family/only_users/only_groups/public
        if self.selectedPostType == "only_groups"
        {
            parameter = ["category_id":"3","created_by":UserDefaults.standard.value(forKey: "userId") as! String, "post_info":[],
                         "snap_description":txtDescription.text!.trimmingCharacters(in: .whitespacesAndNewlines),
                         "title":"",
                         "post_attachment":self.selectedAttachments,
                         "type":"announcement",
                         "post_type":self.selectedPostType,
                         "selected_groups" : self.arrayOfSelectedUser,"selected_users":[],"is_shareable":self.isSharing!,"conversation_enabled":self.isConversation!,"privacy_type":self.privacyType] as [String : Any]
            
        }
        else  if self.selectedPostType == "only_users"
        {
            parameter = ["category_id":"3","created_by":UserDefaults.standard.value(forKey: "userId") as! String, "post_info":[],
                         "snap_description":txtDescription.text!.trimmingCharacters(in: .whitespacesAndNewlines),
                         "title":"",
                         "post_attachment":self.selectedAttachments,
                         "type":"announcement",
                         "post_type":self.selectedPostType,
                         "selected_groups":[],"selected_users":self.arrayOfSelectedUser,"is_shareable":self.isSharing!,"conversation_enabled":self.isConversation!,"privacy_type":self.privacyType] as [String : Any]
        }
        else
        {
            parameter = ["category_id":"3","created_by":UserDefaults.standard.value(forKey: "userId") as! String, "post_info":[],
                         "snap_description":txtDescription.text!.trimmingCharacters(in: .whitespacesAndNewlines),
                         "title":"",
                         "post_attachment":self.selectedAttachments,
                         "type":"announcement",
                         "post_type":self.selectedPostType,
                         "selected_groups" : [],"selected_users":[],"is_shareable":self.isSharing!,"conversation_enabled":self.isConversation!,"privacy_type":self.privacyType] as [String : Any]
        }
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.CreatePost(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    print(jsonData["status"])
                    print(response.statusCode)
                    if response.statusCode == 200
                    {
                        let AlertCus = CustomAlertVC.show(in: self, title: "Success", message: "Successfully created Announcement", image:UIImage.init(named: "Green_tick")!)
                        AlertCus.addAction(CPAlertAction.init(title: "OK", type: .normal, handler: {
                            
                            if self.fromGroup{
                                self.navigationController?.popViewController(animated: true)
                            }
                            else{
                                self.navigationController?.popToViewController(ofClass: HomeTabViewController.self)
                            }
                        }))
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.apiForCreateAnnouncement()
                        }
                    }
                    else
                    {
                        
                        print(jsonData["code"])
                        Helpers.showAlertDialog(message: "Error in Announcement Upload", target: self)
                    }
                    
                    
                } catch let err {
                    
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
    
    
    func apiForUpdateAnnouncement() {
        var parameter = [String:Any]()
        
        parameter = ["id" : editPostDetails["post_id"].stringValue,
                     "snap_description":txtDescription.text!.trimmingCharacters(in: .whitespacesAndNewlines),
                     "title":"",
                     "post_attachment":self.selectedAttachments,
                     "type":"announcement","is_shareable":self.isSharing!,"conversation_enabled":self.isConversation!,"post_ref_id":self.editPostDetails["post_ref_id"].stringValue, "user_id":UserDefaults.standard.value(forKey: "userId") as! String,"created_by":UserDefaults.standard.value(forKey: "userId") as! String,"update_type":"single","privacy_type":self.editPostDetails["privacy_type"].stringValue] as [String : Any]
        
        
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.UpdatePost(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    let jsonData = JSON(response.data as Any)
                    
                    if response.statusCode == 200
                    {
                        let AlertCus = CustomAlertVC.show(in: self, title: "Success", message: "Successfully Updated Announcement", image:UIImage.init(named: "Green_tick")!)
                        AlertCus.addAction(CPAlertAction.init(title: "OK", type: .normal, handler: {
                            self.navigationController?.popViewController(animated: true)
                        }))
                        self.appDelegate.isfromConversationAnnouncement = true
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.apiForUpdateAnnouncement()
                        }
                    }
                    else{
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                } catch let err {
                    
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    //MARK:- VALIDATION
    func DatavalidationForSubmit()->Bool{
        
        if txtPostType.text == "" {
            
            Helpers.showAlertDialog(message: "Announcement Type is empty", target: self)
            return false
        }
        if txtDescription.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            
            Helpers.showAlertDialog(message: "Announcement Description is empty", target: self)
            return false
        }
            
        else
            if self.imgDataArr.count != 0  && !imageUploadStatus
            {
                
                Helpers.showAlertDialog(message: "Uploading images Please Wait a moment.", target: self)
                return false
                
                
                
            }
            else if  !vedioUploadStatus{
//                self.videoDataArr.count != 0 &&
                Helpers.showAlertDialog(message: "Uploading video Please Wait a moment.", target: self)
                
                return false
                
            }
            else if self.documentDataArr.count != 0 && !documentUploadStatus{
                
                Helpers.showAlertDialog(message: "Uploading document Please Wait a moment.", target: self)
                
                return false
                
            }
            else {
                
                return true
                
        }
    }
    
    func DatavalidationForUpdateAnnouncement()->Bool{
        
        
        if self.imgDataArr.count != 0  && !imageUploadStatus
        {
            
            Helpers.showAlertDialog(message: "Uploading images Please Wait a moment.", target: self)
            return false
            
            
            
        }
        else if  !vedioUploadStatus{
//            self.videoDataArr.count != 0 &&
            Helpers.showAlertDialog(message: "Uploading video Please Wait a moment.", target: self)
            
            return false
            
        }
        else if self.documentDataArr.count != 0 && !documentUploadStatus{
            
            Helpers.showAlertDialog(message: "Uploading document Please Wait a moment.", target: self)
            
            return false
            
        }
        else {
            
            return true
            
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    func selctionCallback(SelectedUserId:NSMutableArray,currentIndex:Int)
    {
        print(SelectedUserId)
        if currentIndex == 0
        {
            self.heightOfPostSelection.constant = 90
            self.lblPostTypeStatus.isHidden = false
            self.arrayOfSelectedUser = SelectedUserId
            self.lblPostTypeStatus.text = "\(self.arrayOfSelectedUser.count) Families Selected"
        }
        else if currentIndex == 1
        {
            self.heightOfPostSelection.constant = 90
            self.lblPostTypeStatus.isHidden = false
            self.arrayOfSelectedUser = SelectedUserId
            self.lblPostTypeStatus.text = "\(self.arrayOfSelectedUser.count) Peoples Selected"
        }
        else
        {
            self.heightOfPostSelection.constant = 75
            self.lblPostTypeStatus.isHidden = true
            self.arrayOfSelectedUser = []
            self.txtPostType.text = ""
            self.selectedPostType = ""
            
        }
    }
    
    
    
    //MARK:- collectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.selectedItemsexisted.count
    }
    func generateThumbnail(for asset:AVAsset) -> UIImage? {
        let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        let time = CMTimeMake(value: 1, timescale: 2)
        let img = try? assetImgGenerate.copyCGImage(at: time, actualTime: nil)
        if img != nil {
            let frameImg  = UIImage(cgImage: img!)
            return frameImg
        }
        return nil
    }
    func generateThumbnail(asset: AVAsset) -> UIImage? {
        do {
            let imageGenerator = AVAssetImageGenerator.init(asset: asset)
            imageGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imageGenerator.copyCGImage(at: .zero,
                                                         actualTime: nil)
            return UIImage(cgImage: cgImage)
        } catch {
            return #imageLiteral(resourceName: "imgNoImage_landscape")
        }
    }
    func generatePdfThumbnail(of thumbnailSize: CGSize , for documentUrl: URL, atPage pageIndex: Int) -> UIImage? {
        let pdfDocument = PDFDocument(url: documentUrl)
        let pdfDocumentPage = pdfDocument?.page(at: pageIndex)
        return pdfDocumentPage?.thumbnail(of: thumbnailSize, for: PDFDisplayBox.trimBox)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PostImageCollectionViewCell
        let item = self.selectedItemsexisted[indexPath.row]
        cell.btnClose.tag = indexPath.row
        
        //         for item in self.selectedItems{
        
        if item["existing"]as! Bool == true
        {
            let dict = item["value"] as! [String:String]
            let history_pic = dict["filename"]!
            let history_type = dict["type"]!
            
            
            if history_pic != ""
            {
                if history_type.contains("image")
                {
                    let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+history_pic
                    let imgUrl = URL(string: temp)
                    cell.imgOfPostItems.kf.indicatorType  = .activity
                    cell.imgOfPlayVideo.isHidden = true
                    cell.viewOfVideoBg.isHidden = true
                    cell.imgOfPostItems.kf.setImage(with: imgUrl, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
                    cell.imgOfProgress.image = #imageLiteral(resourceName: "completed_progress.png")
                    cell.activityIndicator.isHidden = true
                }
                else  if history_type.contains("text") || history_type.contains("pdf") || history_type.contains("doc") || history_type.contains("docx") || history_type.contains("xls") || history_type.contains("xlsx") || history_type.contains("msword") || history_type.contains("txt")
                {
                    cell.imgOfPostItems.kf.indicatorType  = .activity
                    cell.imgOfPlayVideo.isHidden = true
                    cell.viewOfVideoBg.isHidden = true
                    let temp = "\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+history_pic
                    let docUrl = URL(string: temp)
                    print(temp)
                    
                    //                print(docUrl)
                    if let pdf = CGPDFDocument(docUrl as! CFURL)
                    {
                        print("it is pdf")
                        let thumbnailSize = CGSize(width: cell.imgOfPostItems.frame.width, height: cell.imgOfPostItems.frame.height)
                        
                        let thumbnail = generatePdfThumbnail(of: thumbnailSize, for: docUrl!, atPage: 0)
                        
                        cell.imgOfPostItems.image = thumbnail as! UIImage
                    }
                    else
                    {
                        print("not pdf")
                        let asset = AVAsset(url: docUrl!)
                        
                        if let img = self.generateThumbnail(for: asset)
                        {
                            cell.imgOfPostItems.image = img
                            //
                        }else {
                            print("Error: Thumbnail can be generated.")
                            cell.imgOfPostItems.image = #imageLiteral(resourceName: "imgNoImage")
                            
                            //                                                           return
                        }
                        
                    }
                    
                    //
                    
                    
                    //                                        let size: CGSize = CGSize(width: cell.imgOfPostItems.frame.width, height: cell.imgOfPostItems.frame.height)
                    //                                        let scale = UIScreen.main.scale
                    //
                    //                                        // Create the thumbnail request.
                    //                                        if #available(iOS 13.0, *) {
                    //                                            let request = QLThumbnailGenerator.Request(fileAt: docUrl!,
                    //                                                                                       size: size,
                    //                                                                                       scale: scale,
                    //                                                                                       representationTypes: .all)
                    //
                    //                                            // Retrieve the singleton instance of the thumbnail generator and generate the thumbnails.
                    //                                            let generator = QLThumbnailGenerator.shared
                    //                                            generator.generateRepresentations(for: request) { (thumbnail, type, error) in
                    //                                                DispatchQueue.main.async {
                    //                                                    if thumbnail == nil || error != nil {
                    //                                                        print("error in thumbnail generation")
                    //                                                        // Handle the error case gracefully.
                    //                                                    } else {
                    //                    //                                    print(thumbnail?.uiImage)
                    //                                                        cell.imgOfPostItems.image = thumbnail?.uiImage
                    //
                    //                                                        // Display the thumbnail that you created.
                    //                                                    }
                    //                                                }
                    //                                            }
                    //                                        } else {
                    //                                            // Fallback on earlier versions
                    //                                        }
                    
                    
                    
                    cell.imgOfProgress.image = #imageLiteral(resourceName: "completed_progress.png")
                    cell.activityIndicator.isHidden = true
                }
                else
                {
                    if let thumb = dict["video_thumb"]{
                        
                        let temp = "\(Helpers.imageURl)"+thumb
                        let imgUrl = URL(string:BaseUrl.imaginaryURLForDetail2X + temp )
                        cell.imgOfPostItems.kf.indicatorType  = .activity
                        cell.imgOfPostItems.kf.setImage(with: imgUrl)
                        
                        cell.imgOfPlayVideo.isHidden = false
                        cell.viewOfVideoBg.isHidden = false
                    }
                    else {
                        print("Error: Thumbnail can be generated.")
                        cell.imgOfPostItems.image = #imageLiteral(resourceName: "imgNoImage")
                        cell.imgOfPlayVideo.isHidden = false
                        
                        //                                                           return
                    }
                    
                    
                    //                    let asset = AVAsset(url: imgUrl!)
                    //                    if let img = self.generateThumbnail(for: asset)
                    //                    {
                    //                        cell.imgOfPostItems.image = img
                    //                        //
                    //                    }else {
                    //                        print("Error: Thumbnail can be generated.")
                    //                        cell.imgOfPostItems.image = #imageLiteral(resourceName: "imgNoImage")
                    //
                    //                        //                                                           return
                    //                    }
                    
                    //                                            cell.imgOfPostItems.kf.setImage(with: imgUrl, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
                    cell.imgOfProgress.image = #imageLiteral(resourceName: "completed_progress.png")
                    cell.activityIndicator.isHidden = true
                    
                }
                
            }
        }
        else
        {
            print(item["value"])
            cell.imgOfPlayVideo.isHidden = true
            cell.viewOfVideoBg.isHidden = true
            
            if let docUrl = item["document"] , docUrl != nil
            {
                //                print(docUrl)
                
                print(docUrl)
                
                let size: CGSize = CGSize(width: cell.imgOfPostItems.frame.width, height: cell.imgOfPostItems.frame.height)
                let scale = UIScreen.main.scale
                
                // Create the thumbnail request.
                if #available(iOS 13.0, *) {
                    let request = QLThumbnailGenerator.Request(fileAt: docUrl as! URL,
                                                               size: size,
                                                               scale: scale,
                                                               representationTypes: .all)
                    
                    // Retrieve the singleton instance of the thumbnail generator and generate the thumbnails.
                    let generator = QLThumbnailGenerator.shared
                    generator.generateRepresentations(for: request) { (thumbnail, type, error) in
                        DispatchQueue.main.async {
                            if thumbnail == nil || error != nil {
                                print("error in thumbnail generation")
                                cell.imgOfPostItems.image = #imageLiteral(resourceName: "imgNoImage")
                                
                                // Handle the error case gracefully.
                            } else {
                                //                                    print(thumbnail?.uiImage)
                                cell.imgOfPostItems.image = thumbnail?.uiImage
                                
                                // Display the thumbnail that you created.
                            }
                        }
                    }
                } else {
                    // Fallback on earlier versions
                    
                    
                    
                    if let pdf = CGPDFDocument(docUrl as! CFURL)
                    {
                        print("it is pdf")
                        let thumbnailSize = CGSize(width: cell.imgOfPostItems.frame.width, height: cell.imgOfPostItems.frame.height)
                        
                        let thumbnail = generatePdfThumbnail(of: thumbnailSize, for: docUrl as! URL, atPage: 0)
                        
                        cell.imgOfPostItems.image = (thumbnail as! UIImage)
                    }
                    else
                    {
                        print("not pdf")
                        let asset = AVAsset(url: docUrl as! URL)
                        
                        if let img = self.generateThumbnail(for: asset)
                        {
                            cell.imgOfPostItems.image = img
                            //
                        }else {
                            print("Error: Thumbnail can be generated.")
                            cell.imgOfPostItems.image = #imageLiteral(resourceName: "imgNoImage")
                            //                                                           return
                        }
                        
                    }
                    
                    
                }
                
                if !documentUploadStatus{
                    let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "imageProgressor", withExtension: "gif")!)
                    let advTimeGif = UIImage.gifImageWithData(imageData!)
                    cell.imgOfProgress.image = advTimeGif
                    
                    
                    //                                    cell.imgOfProgress.image = #imageLiteral(resourceName: "ongoing_progress.png")
                    cell.activityIndicator.isHidden = false
                    
                }
                else
                {
                    cell.imgOfProgress.image = #imageLiteral(resourceName: "completed_progress.png")
                    cell.activityIndicator.isHidden = true
                    
                }
                
            }
            else
            {
                if let url = item["value"] as? Data{
                    if let type = item["type"] as? String, !type.isEmpty, type == "image"{
                        //                        do{
                        //                            let imageData = try Data.init(contentsOf: url)
                        cell.imgOfPostItems.image = UIImage(data: url)
                        cell.imgOfPlayVideo.isHidden = true
                        if !imageUploadStatus
                        {
                            let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "imageProgressor", withExtension: "gif")!)
                            let advTimeGif = UIImage.gifImageWithData(imageData!)
                            cell.imgOfProgress.image = advTimeGif
                            cell.activityIndicator.isHidden = false
                            cell.activityIndicator.startAnimating()
                        }
                        else
                        {
                            cell.imgOfProgress.image = #imageLiteral(resourceName: "completed_progress.png")
                            cell.activityIndicator.isHidden = true
                        }
                        
                        //                    }
                        //                        catch{
                        //                            print("Error")
                        //                        }
                    }
                    if let type = item["type"] as? String, !type.isEmpty, type == "video"{
                        cell.imgOfPostItems.image = generateThumbnail(asset: url.getAVAsset())
                        self.selectedImageV.image = cell.imgOfPostItems.image
                        
                        
                        cell.imgOfPlayVideo.isHidden = false
                        if !vedioUploadStatus
                        {
                            let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "imageProgressor", withExtension: "gif")!)
                            let advTimeGif = UIImage.gifImageWithData(imageData!)
                            cell.imgOfProgress.image = advTimeGif
                            cell.activityIndicator.isHidden = false
                            cell.activityIndicator.startAnimating()
                            
                        }
                        else
                        {
                            cell.imgOfProgress.image = #imageLiteral(resourceName: "completed_progress.png")
                            cell.activityIndicator.isHidden = true
                        }
                        
                    }
                }
                else{
                    
                    let currentItem = item["value"] as! DKAsset
                    
                    switch currentItem.type {
                    case .photo:
                        
                        currentItem.fetchImage(with: CGSize.init(width: 200, height: 200), completeBlock: { image, info in
                            cell.imgOfPostItems.image = image
                        })
                        
                        cell.imgOfPlayVideo.isHidden = true
                        if !imageUploadStatus
                        {
                            if cell.imgOfProgress.image == #imageLiteral(resourceName: "completed_progress.png"){
                                
                            }
                            else{
                                let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "imageProgressor", withExtension: "gif")!)
                                let advTimeGif = UIImage.gifImageWithData(imageData!)
                                cell.imgOfProgress.image = advTimeGif
                                cell.activityIndicator.isHidden = false
                                cell.activityIndicator.startAnimating()
                            }
                        }
                        else
                        {
                            cell.imgOfProgress.image = #imageLiteral(resourceName: "completed_progress.png")
                            cell.activityIndicator.isHidden = true
                            
                        }
                        
                    case .video:
                        currentItem.fetchImage(with: CGSize.init(width: 200, height: 200), completeBlock: { image, info in
                            self.selectedImageV.image = image
                            cell.imgOfPostItems.image = image
                            
                        })
                        
                        cell.imgOfPlayVideo.isHidden = false
                        if !vedioUploadStatus
                        {
                            if cell.imgOfProgress.image == #imageLiteral(resourceName: "completed_progress.png"){
                                
                            }
                            else{
                                
                                let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "imageProgressor", withExtension: "gif")!)
                                let advTimeGif = UIImage.gifImageWithData(imageData!)
                                cell.imgOfProgress.image = advTimeGif
                                cell.activityIndicator.isHidden = false
                                cell.activityIndicator.startAnimating()
                                
                            }
                            
                        }
                        else
                        {
                            cell.imgOfProgress.image = #imageLiteral(resourceName: "completed_progress.png")
                            cell.activityIndicator.isHidden = true
                            
                        }
                        
                    }
                }
            }
        }
        
        return cell
    }
    
    
    @IBAction func btnCloseImage_Onclick(_ sender: UIButton) {
        //below code for stop current request
        //        Helpers.StopAPICALL()
        
        if self.imgDataArr.count != 0  && !imageUploadStatus{
            
            //            Helpers.showAlertDialog(message: "Wait for image uploading", target: self)
            
            let alert = UIAlertController(title: "Familheey", message: "Do you really want to cancel upload?", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "YES", style: .destructive, handler: { (acion) in
                
                if  self.imageUploadStatus{
                    self.deleteAlreadyUploadingFiles(index:sender.tag)
                }
                else
                {
                    
                    self.deleteAttachment = true
                    Helpers.StopAPICALL()
                    
                }
                
            }))
            
            alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
            
        }
        else if  !vedioUploadStatus{
//             && self.videoDataArr.count != 0
            //            Helpers.showAlertDialog(message: "Wait for video uploading", target: self)
            
            
            let alert = UIAlertController(title: "Familheey", message: "Do you really want to cancel upload?", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "YES", style: .destructive, handler: { (acion) in
                
                if  self.vedioUploadStatus
                {
                    self.deleteAlreadyUploadingFiles(index:sender.tag)
                }
                else
                {
                    self.deleteAttachment = true
                    AWSS3Manager.shared.cancelAllUploads()
                    self.vedioUploadStatus = true
                    self.videoDataArr = []
                    self.selectedItemsexisted =  self.previousSelectedItemsexisted
                    self.deleteAttachment = false
                    
                    if self.selectedItemsexisted.count != 0{
                        self.collViewOfAttachments.isHidden = false
                        self.lblNoFiles.isHidden = true
                        self.collViewOfAttachments.reloadData()
                        
                    }
                    else{
                        self.collViewOfAttachments.isHidden = true
                        self.lblNoFiles.isHidden = false
                        
                    }
                    
                }
                
            }))
            
            alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
            
        }
        else if self.documentDataArr.count != 0 && !documentUploadStatus{
            
            //            Helpers.showAlertDialog(message: "Wait for document uploading", target: self)
            
            
            let alert = UIAlertController(title: "Familheey", message: "Do you really want to delete currently uploading document otherwise wait for document uploading", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "YES", style: .destructive, handler: { (acion) in
                if  self.documentUploadStatus
                {
                    self.deleteAlreadyUploadingFiles(index:sender.tag)
                }
                else
                {
                    self.deleteAttachment = true
                    Helpers.StopAPICALL()
                }
                
            }))
            
            alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }
        else
        {
            //            self.selectedItemsexisted.remove(at: sender.tag)
            //            self.selectedAttachments.removeObject(at: sender.tag)
            //            if self.selectedItemsexisted.count != 0
            //            {
            //                self.collViewOfAttachments.isHidden = false
            //                self.lblNoFiles.isHidden = true
            //                self.collViewOfAttachments.reloadData()
            //
            //            }
            //            else
            //            {
            //                self.collViewOfAttachments.isHidden = true
            //                self.lblNoFiles.isHidden = false
            //
            //            }
            
            self.deleteAlreadyUploadingFiles(index: sender.tag)
        }
        //        self.collViewOfAttachments.reloadData()
        //         print("current Agenda: \(currentAgenda)")
        
    }
    
    
    func deleteAlreadyUploadingFiles(index:Int){
        self.selectedItemsexisted.remove(at: index)
        self.selectedAttachments.removeObject(at: index)
        if self.selectedItemsexisted.count != 0
        {
            self.collViewOfAttachments.isHidden = false
            self.lblNoFiles.isHidden = true
            self.collViewOfAttachments.reloadData()
            
        }
        else
        {
            self.collViewOfAttachments.isHidden = true
            self.lblNoFiles.isHidden = false
            
        }
        //        self.collViewOfAttachments.reloadData()
        
    }
    
    //    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    //        let item = self.selectedItems[indexPath.row]
    //
    //
    //
    //    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //        let numberOfCellInRow                   = 3
        //        let padding : Int                       = 2
        //        let collectionCellWidth : CGFloat       = (self.collViewOfAttachments.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
        //        return CGSize(width: collectionCellWidth, height: collectionCellWidth + 20)
        
        return CGSize.init(width: 90, height:  90)
    }
    
    
}

extension CreateAnnouncmentViewController:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.arrayOfPostTypewitharrow.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! postTypeDropdownCell
        let dict = arrayOfPostTypewitharrow[indexPath.row]
        if dict["hidebool"] as! Bool == true
        {
            cell.imgOfArrow.isHidden = true
        }
        else
        {
            cell.imgOfArrow.isHidden = false
            
        }
        cell.lblTitle.text = dict["text"] as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = self.arrayOfPostTypewitharrow[indexPath.row]
        let selectedText = dict["text"] as! String
        self.txtPostType.text = selectedText
        self.tblViewDropList.isHidden = true
        self.viewOfShade.isHidden = true
        
        if selectedText == "All Families"
        {
            self.heightOfPostSelection.constant = 75
            self.lblPostTypeStatus.isHidden = true
            self.arrayOfSelectedUser = []
            self.selectedPostType = "all_family"
            self.privacyType = "public"
            
        }
        else if selectedText == "Selected families"
        {
            self.selectedPostType = "only_groups"
            self.privacyType = "public"
            let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SelectFamilyOrPeopleViewController") as! SelectFamilyOrPeopleViewController
            vc.fromAnnouncement = true
            vc.selectedIndex = 0
            vc.selectionDelegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if selectedText == "Selected Connections"
        {
            self.selectedPostType = "only_users"
            self.privacyType = "public"
            let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SelectFamilyOrPeopleViewController") as! SelectFamilyOrPeopleViewController
            vc.selectedIndex = 1
            vc.selectionDelegate = self
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else  if selectedText == "Anyone"
        {
            self.selectedPostType = "public"
            self.privacyType = "public"
            self.heightOfPostSelection.constant = 75
            self.lblPostTypeStatus.isHidden = true
            self.arrayOfSelectedUser = []
            
        }
        else
        {
            self.selectedPostType = "private"
            self.privacyType = "private"
            self.heightOfPostSelection.constant = 75
            self.lblPostTypeStatus.isHidden = true
            self.arrayOfSelectedUser = []
        }
        
    }
}

// Support methods
extension CreateAnnouncmentViewController {
    /* Gives a resolution for the video by URL */
    func resolutionForLocalVideo(url: URL) -> CGSize? {
        guard let track = AVURLAsset(url: url).tracks(withMediaType: AVMediaType.video).first else { return nil }
        let size = track.naturalSize.applying(track.preferredTransform)
        return CGSize(width: abs(size.width), height: abs(size.height))
    }
}

