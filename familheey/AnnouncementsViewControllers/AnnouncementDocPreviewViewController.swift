//
//  AnnouncementDocPreviewViewController.swift
//  familheey
//
//  Created by Giri on 04/12/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import WebKit

class AnnouncementDocPreviewViewController: UIViewController {
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var Activity: UIActivityIndicatorView!
    var UrlToLoad = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        // add activity
        self.webView.addSubview(self.Activity)
        self.Activity.startAnimating()
        self.webView.navigationDelegate = self
        self.Activity.hidesWhenStopped = true

      
    }
    override func viewDidAppear(_ animated: Bool) {
        if !UrlToLoad.isEmpty{
            DispatchQueue.main.async{
                
                if let myURL = URL(string:self.UrlToLoad) {
                    if let data = try? Data(contentsOf: myURL) {
                        self.webView.load(data, mimeType: "application/pdf", characterEncodingName: "", baseURL: myURL)
                        
                    }
                }
            }
        }
        else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    @IBAction func closeAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
}
extension AnnouncementDocPreviewViewController : WKNavigationDelegate{
     
     func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
         Activity.stopAnimating()
     }

     func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
         Activity.stopAnimating()
     }
    
    
}
