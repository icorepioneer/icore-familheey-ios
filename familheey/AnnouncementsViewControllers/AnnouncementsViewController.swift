//
//  AnnouncementsViewController.swift
//  familheey
//
//  Created by Giri on 25/11/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON
import Kingfisher
import SafariServices



class AnnouncementsViewController: UIViewController {
    @IBOutlet weak var tableviewAnnouncements: UITableView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearchReset: UIButton!
    @IBOutlet weak var noAnnounceVew: UIView!

    @IBOutlet weak var lblUnreadHead: UILabel!
    @IBOutlet weak var lblReadHead: UILabel!
    @IBOutlet weak var imgUnreadHead: UIImageView!
    @IBOutlet weak var imgReadHead: UIImageView!
    

    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var postoptionsTittle = [String]()
    var refreshControl = UIRefreshControl()
    var ArrayAnnouncements = [JSON]()
    var readAnnouncements = [JSON]()
    var unreadAnnouncements = [JSON]()
    var offset = 0
    var limit = 1000
    var selectedIndex = 0
    var isFromHome = false
    var whiteBG = UIView()
    let loadingDefaultView = UIView()
    var selectedTag = 100
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtSearch.delegate = self
        refreshControl.attributedTitle = NSAttributedString(string: "Please wait...")
        refreshControl.addTarget(self, action: #selector(refreshPost), for: .valueChanged)
        tableviewAnnouncements.addSubview(refreshControl)
        //        getAnnouncements()
        tableviewAnnouncements.dataSource = self
        tableviewAnnouncements.delegate = self
        appDelegate.isConversationUpdated = true
        if isFromHome{
            whiteBG = UIView.init(frame: self.view.bounds)
            whiteBG.backgroundColor = .white
            self.view.addSubview(whiteBG)
            
            //            self.loadingDefaultView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height:  self.view.frame.size.height)
            //                   self.loadingDefaultView.backgroundColor = .white
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.updateAnnouncementSeen()
//        getAnnouncements()
        let btn = UIButton()
        btn.tag = selectedTag
//        selectedIndex = selectedIndex
        onClickTabChange(btn)
        self.tabBarController?.tabBar.isHidden = true
        
    }
    
    @objc func refreshPost(refreshControl: UIRefreshControl) {
        refreshControl.endRefreshing()
        ArrayAnnouncements = [JSON]()
        getAnnouncements()
    }
    
    //MARK:- Update Announcement seen
    func updateAnnouncementSeen(){
//        ActivityIndicatorView.show("Please wait....")
        
        
        let param = ["user_id" : UserDefaults.standard.value(forKey: "userId") as! String]
        
        networkProvider.request(.addUpdate_Announcement_Seen(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
//                    ActivityIndicatorView.hiding()
                    
                    let jsonData =  JSON(response.data)
                    
                    if response.statusCode == 200{
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.updateAnnouncementSeen()
                        }
                    }
                    
                }catch _ {
                    
                    
                }
            case .failure( _):
//                ActivityIndicatorView.hiding()
                break
            }
        }
        
        
        
    }
    //MARK:- Get all Announcements
    
    func getAnnouncements(){
        DispatchQueue.main.async {
            ActivityIndicatorView.show("Please wait....")
        }
        
        var param = [String:Any]()
        if selectedIndex == 0{
            param = ["user_id" : UserDefaults.standard.value(forKey: "userId") as! String,"offset":"\(offset)","limit":"\(limit)","type":"announcement","query":txtSearch.text!,"read_status":false]
        }
        else{
            param = ["user_id" : UserDefaults.standard.value(forKey: "userId") as! String,"offset":"\(offset)","limit":"\(limit)","type":"announcement","query":txtSearch.text!,"read_status":true]
        }
//        param = ["user_id" : UserDefaults.standard.value(forKey: "userId") as! String,"offset":"\(offset)","limit":"\(limit)","type":"announcement","query":txtSearch.text!]
        print(param)
        
        networkProvider.request(.announcementList(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        self.ArrayAnnouncements.removeAll()
                        
                        if self.selectedIndex == 0{
                            if let unreadResponse = jsonData["unread_announcement"].array{
                                self.unreadAnnouncements.removeAll()
                                self.unreadAnnouncements.append(contentsOf: unreadResponse)
                                self.ArrayAnnouncements.append(contentsOf: unreadResponse)
                            }
                        }
                        else{
                            if let readResponse = jsonData["read_announcement"].array{
                                self.readAnnouncements.removeAll()
                                self.readAnnouncements.append(contentsOf: readResponse)
                                self.ArrayAnnouncements.append(contentsOf: readResponse)
                            }
                        }
                        
                       /* if let unreadResponse = jsonData["unread_announcement"].array{
                            self.unreadAnnouncements.removeAll()
                            self.unreadAnnouncements.append(contentsOf: unreadResponse)
                            self.ArrayAnnouncements.append(contentsOf: unreadResponse)
                            
                        }
                        
                        if let readResponse = jsonData["read_announcement"].array{
                            self.readAnnouncements.removeAll()
                            self.readAnnouncements.append(contentsOf: readResponse)
                            self.ArrayAnnouncements.append(contentsOf: readResponse)
                            
                        }*/
                        
                        if self.ArrayAnnouncements.count > 0{
                            self.noAnnounceVew.isHidden = true
                            self.tableviewAnnouncements.isHidden = false
                            self.tableviewAnnouncements.dataSource = self
                            self.tableviewAnnouncements.delegate = self
                            self.tableviewAnnouncements.reloadData()
                            if self.isFromHome{
                                self.whiteBG.removeFromSuperview()
                                
                                let indexPath = IndexPath(row: 0, section: 0);
                                self.tableviewAnnouncements.selectRow(at: indexPath, animated: false, scrollPosition: UITableView.ScrollPosition.none)
                                self.tableView(self.tableviewAnnouncements, didSelectRowAt: indexPath)
                            }
                        }
                        else{
                            self.whiteBG.removeFromSuperview()
                            self.tableviewAnnouncements.isHidden = true
                            self.noAnnounceVew.isHidden = false
                        }
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getAnnouncements()
                        }
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    self.whiteBG.removeFromSuperview()
                    ActivityIndicatorView.hiding()
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                self.whiteBG.removeFromSuperview()
                break
            }
        }
    }
    
    func readUnread(status:String, index:Int){
        let post = ArrayAnnouncements[index]
        let parameter = ["post_id":post["post_id"].stringValue,"user_id":UserDefaults.standard.value(forKey: "userId") as! String, "read_status":status]
        print("Param : \(parameter)")
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.postReadUnread(parameter: parameter)) { (result) in
            switch result{
            case .success( let response):
                do{
                    
                    let jsonData =  JSON(response.data)
                    if response.statusCode == 200{
                        print("view contents : \(jsonData)")
                        self.getAnnouncements()
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.readUnread(status: status, index: index)
                        }
                    }
                    else{
                        self.displayAlert(alertStr: "Oops! Something went wrong..", title: "")
                        ActivityIndicatorView.hiding()
                    }
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                }
            case .failure(let error):
                ActivityIndicatorView.hiding()
                break
            }
        }
    }
    
    //MARK:- Custom Methods
    func deletePostForCreator(tag:Int){
        let alert = UIAlertController(title: "Familheey", message: "Do you really want to delete this announcement? Deleting this announcement will delete it from everywhere you posted it to and you will lose all associated conversations", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "DELETE", style: .destructive, handler: { _ in
            let post = self.ArrayAnnouncements[tag]
            
            
            let param = ["id" : post["post_id"].stringValue,"user_id":UserDefaults.standard.value(forKey: "userId")as! String]
            
            ActivityIndicatorView.show("Please wait....")
            self.networkProvider.request(.deletePost(parameter: param)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                        print("view contents : \(jsonData)")
                        if response.statusCode == 200{
                            print("view contents : \(jsonData)")
                            self.offset = 0
                            self.getAnnouncements()
                        }
                        else if response.statusCode == 401 {
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                self.deletePostForCreator(tag: tag)
                            }
                        }
                        
                        ActivityIndicatorView.hiding()
                        
                    }catch let err {
                        ActivityIndicatorView.hiding()
                        // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                    }
                case .failure( let error):
                    ActivityIndicatorView.hiding()
                    //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                    break
                }
            }
            
        }))
        alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func removePostFromTimeline(tag:Int){
        let post = ArrayAnnouncements[tag]
        
        let param = ["post_id" : post["post_id"].stringValue, "user_id": UserDefaults.standard.value(forKey: "userId") as! String]
        
        ActivityIndicatorView.show("Please wait....")
        networkProvider.request(.removePost(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        print("view contents : \(jsonData)")
                        self.offset = 0
                        self.getAnnouncements()
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.removePostFromTimeline(tag: tag)
                        }
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                break
            }
        }
    }
    
    
    //MARK:- Button Actions
    @IBAction func onClickTabChange(_ sender: UIButton) {
        if sender.tag == 100{
            selectedIndex = 0
            offset = 0
            selectedTag = sender.tag
            
            lblUnreadHead.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            imgUnreadHead.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            
            lblReadHead.textColor = #colorLiteral(red: 0.5176470588, green: 0.5568627451, blue: 0.6, alpha: 1)
            imgReadHead.backgroundColor = #colorLiteral(red: 0.5176470588, green: 0.5568627451, blue: 0.6, alpha: 1)
            
            getAnnouncements()
        }
        else{
            selectedIndex = 1
            offset = 0
            selectedTag = sender.tag
            
            lblReadHead.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            imgReadHead.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            
            lblUnreadHead.textColor = #colorLiteral(red: 0.5176470588, green: 0.5568627451, blue: 0.6, alpha: 1)
            imgUnreadHead.backgroundColor = #colorLiteral(red: 0.5176470588, green: 0.5568627451, blue: 0.6, alpha: 1)
            
            getAnnouncements()
        }
    }
    
    @IBAction func onClickSearchReset(_ sender: Any) {
        txtSearch.text = ""
        getAnnouncements()
        btnSearchReset.isHidden = true
        self.txtSearch.endEditing(true)
    }
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickRightMenu(_ sender: UIButton) {
        let post = ArrayAnnouncements[sender.tag]
        
        if post["created_by"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")" {
            
            if let sharedusers = post["shared_user_names"].string , !sharedusers.isEmpty{
                postoptionsTittle =  ["Delete"]
            }
            else{
                postoptionsTittle =  ["Edit","Delete"]
            }
        }
        else{
            postoptionsTittle =  ["Hide","Report"]
            //            if post["read_status"].boolValue{
            //                postoptionsTittle =  ["Mark as unread","Mute conversation","Remove announcements","Report"]
            //            }
            //            else{
            //                postoptionsTittle =  ["Mark as read","Mute conversation","Remove announcements","Report"]
            //            }
        }
        if post["read_status"].boolValue{
            postoptionsTittle.insert("Mark as unread", at: 0)
        }
        else{
            postoptionsTittle.insert("Mark as read", at: 0)
        }
        if post["muted"].stringValue == "true"{
            postoptionsTittle.insert("Unmute", at: 1)
        }
        else{
            postoptionsTittle.insert("Mute", at: 1)
        }
        
        
        showActionSheet(titleArr: postoptionsTittle as NSArray, title: "Select options") { (index) in
            
            if index == 3{
                if post["created_by"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")"{
                    self.deletePostForCreator(tag: sender.tag)
                }
                else{
                    let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "ReportPostViewController") as! ReportPostViewController
                    
                    vc.postDetails =  self.ArrayAnnouncements[sender.tag]
                    vc.isFrom = "announcement"
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else if index == 1{
                self.muteConversation(tag: sender.tag)
            }
            else if index == 2{
                if post["created_by"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")"{
                    if let sharedusers = post["shared_user_names"].string , !sharedusers.isEmpty{
                        self.deletePostForCreator(tag: sender.tag)
                    }
                    else{
                        //                self.deletePostForCreator(tag: sender.tag)
                        let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
                        
                        let vc = storyboard.instantiateViewController(withIdentifier: "CreateAnnouncmentViewController") as! CreateAnnouncmentViewController
                        
                        vc.fromEdit = true
                        vc.editPostDetails = post
                        self.navigationController?.pushViewController(vc, animated: true)
                    }

                }else{
                    self.removePostFromTimeline(tag: sender.tag)
                }
            }
            else if index == 100{
                
            }
                
            else {
                if post["read_status"].boolValue{
                    self.readUnread(status: "false", index: sender.tag)
                }
                else{
                    self.readUnread(status: "true", index: sender.tag)
                }
            }
        }
    }
    
    func muteConversation(tag:Int){
        let post = ArrayAnnouncements[tag]
        let param = ["post_id" : post["post_id"].stringValue, "user_id": UserDefaults.standard.value(forKey: "userId") as! String]
        
        ActivityIndicatorView.show("Please wait....")
        networkProvider.request(.mute_conversation(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        print("view contents : \(jsonData)")
                        let temp = jsonData["data"].dictionaryValue
                        if temp["is_active"]!.boolValue{
                            var temp = post
                            temp["muted"] = true
                            self.ArrayAnnouncements[tag] = temp
                        }
                        else{
                            var temp = post
                            temp["muted"] = false
                            self.ArrayAnnouncements[tag] = temp
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.muteConversation(tag: tag)
                        }
                    }
                    ActivityIndicatorView.hiding()
                }catch let err {
                    ActivityIndicatorView.hiding()
                    //  Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    @IBAction func onClickComment(_ sender: UIButton) {
        let post = ArrayAnnouncements[sender.tag]
        let mainStoryBoard = UIStoryboard(name: "Conversations", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "ConversationsViewController") as! ConversationsViewController
        vc.post = post
        vc.conversationT = .announcement
        self.navigationController?.pushViewController(vc, animated:true)
    }
    
    @IBAction func onClickAnnouncementViews(_ sender: UIButton) {
        let post = ArrayAnnouncements[sender.tag]
        let mainStoryBoard = UIStoryboard(name: "fourth", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "UserFamilyListViewController") as! UserFamilyListViewController
        vc.postId = post["post_id"].stringValue
        vc.isFrompost = true
        vc.isFrom = "postview"
        self.navigationController?.pushViewController(vc, animated:true)
        
    }
    @IBAction func onClickShare(_ sender: UIButton) {
        let post = ArrayAnnouncements[sender.tag]
        //        let post = ArrayPosts[sender.tag]
        print(post)
        
        let mainStoryBoard = UIStoryboard(name: "third", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "PostShareViewController") as! PostShareViewController
        vc.fromAnnouncement = true
        vc.postId = post["post_id"].stringValue
        self.navigationController?.pushViewController(vc, animated:true)
    }
    
    @IBAction func onClickSharedBy(_ sender: UIButton) {
        let post = ArrayAnnouncements[sender.tag]
        let mainStoryBoard = UIStoryboard(name: "fourth", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "UserFamilyListViewController") as! UserFamilyListViewController
        if post["orgin_id"].stringValue.count > 0{
            vc.postId = post["orgin_id"].stringValue
        }
        else{
            vc.postId = post["post_id"].stringValue
        }
        vc.isFrompost = true
        vc.isFrom = "groupshare"
        vc.familyID = post["to_group_id"].stringValue
        self.navigationController?.pushViewController(vc, animated:true)
    }
    
    @IBAction func onClickDocs(_ sender: Any) {
    }
    
    //MARK:- url tap
    func openAppLink(url:String){
        
        let param = ["url" : url]
        
        networkProvider.request(.openAppGetParams(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("@@##!!! REsult\(jsonData)")
                    if response.statusCode == 200{
                        let dataArr = jsonData["data"]
                        let subType = dataArr["sub_type"].stringValue
                        let type =  dataArr["type"].stringValue
                        
                        if subType == notificationSubTypes.publicfeed || subType == notificationSubTypes.familyfeed && type == notificationTypes.home{
                            let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
                            vc.postId = "\(dataArr["type_id"].intValue)"
                            self.navigationController?.pushViewController(vc, animated: false)
                            
                        }
                        else if type == notificationTypes.family{
                            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                            let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
                            intro.groupId = "\(dataArr["type_id"].intValue)"
                            self.navigationController?.pushViewController(intro, animated: false)
                        }
                        else{
                            let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
                            vc.enableEventEdit = false
                            vc.isFromNotification = "true"
                            vc.eventId = "\(dataArr["type_id"].intValue)"
                            self.navigationController?.pushViewController(vc, animated: false)
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.openAppLink(url: url)
                        }
                    }
                    else{
                        
                    }
                    
                }catch _ {
                    
                    
                }
            case .failure( _):
                break
            }
        }
    }
}
extension AnnouncementsViewController : UITableViewDataSource,UITableViewDelegate{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if section == 0{
//            return unreadAnnouncements.count
//        }
//        else{
//            return readAnnouncements.count
//        }
        return ArrayAnnouncements.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if ArrayAnnouncements.count == 0{
            return UITableViewCell.init()
        }
        
        let announcement = ArrayAnnouncements[indexPath.row]

//        var announcement = JSON()
//        if indexPath.section == 0{
//            announcement = unreadAnnouncements[indexPath.row]
//        }
//        else{
//            announcement = readAnnouncements[indexPath.row]
//        }
        
        if let sharedusers = announcement["shared_user_names"].string , !sharedusers.isEmpty{
            
            let cell  = tableView.dequeueReusableCell(withIdentifier: "AnnouncementsSharedTableViewCell", for: indexPath) as! AnnouncementsSharedTableViewCell
            
            cell.btnSharedBy.tag = indexPath.row
            
            let array = sharedusers.components(separatedBy: ",")
            if array.count != 0{
                if array.count == 1{
                    cell.lblsharedUserName.text = "\(array[0]) shared an announcement"
                }
                else if array.count == 2{
                    cell.lblsharedUserName.text = "\(array[0]) and 1 other shared an announcement"
                }
                else {
                    cell.lblsharedUserName.text = "\(array[0]) and \(array.count-1) others shared an announcement"
                }
            }
            
            cell.lblShared.text = "Shared in"
            cell.lblShare_width.constant = 65.0
            cell.lblPostedIn.text = announcement["parent_post_created_user_name"].string ?? ""
            cell.lblUserName.text = announcement["parent_post_grp_name"].string ?? ""
            
            cell.lblSharedPostedIn.text = announcement["group_name"].string ?? ""
            
            if indexPath.section == 0{
                cell.buttonDocs.tag = indexPath.row
                cell.buttonViews.tag = indexPath.row
                cell.buttonComments.tag = indexPath.row
                cell.buttonShareAnnouncement.tag = indexPath.row
                cell.buttonSideMenu.tag = indexPath.row
            }
            else{
                cell.buttonDocs.tag = indexPath.row+unreadAnnouncements.count
                cell.buttonViews.tag = indexPath.row+unreadAnnouncements.count
                cell.buttonComments.tag = indexPath.row+unreadAnnouncements.count
                cell.buttonShareAnnouncement.tag = indexPath.row+unreadAnnouncements.count
                cell.buttonSideMenu.tag = indexPath.row+unreadAnnouncements.count
            }
            
            cell.lblSharedDate.text = HomeTabViewController.dateFormatterCommon(dateStr: announcement["createdAt"].stringValue)
            
            cell.lblnumberofComments.text = announcement["conversation_count"].string ?? ""
            cell.lblnumberofView.text = announcement["views_count"].string ?? ""
            
            
            var count = 0
            
            if announcement["read_status"].boolValue{
                cell.lblAnnouncement.font = UIFont.systemFont(ofSize: 15.0)
                cell.lblAnnouncement.text = announcement["snap_description"].string ?? ""
            }
            else{
                cell.lblAnnouncement.font = UIFont.boldSystemFont(ofSize: 15.0)
                cell.lblAnnouncement.text = announcement["snap_description"].string ?? ""
            }
            if let attachments = announcement["post_attachment"].array{
                
                for items in attachments{
                    if items["type"].stringValue.contains("image") {
                        count = count + 1
                    }
                }
                let proPic = announcement["parent_family_logo"].stringValue
                
                
                if proPic.count != 0{
                    let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+proPic
                    let imgUrl = URL(string: temp)
                    cell.imageviewUserProfilepic.kf.indicatorType = .activity
                    cell.imageviewUserProfilepic.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Family Logo"), options: nil, progressBlock: nil, completionHandler: nil)
                }
                else{
                    cell.imageviewUserProfilepic.image = #imageLiteral(resourceName: "Family Logo")
                }
            }
            if announcement["is_shareable"].boolValue {
                cell.viewShare.isHidden = false
            }
            else{
                cell.viewShare.isHidden = true
                
            }
            if announcement["conversation_enabled"].boolValue{
                cell.viewComments.isHidden = false
            }
            else{
                cell.viewComments.isHidden = true
            }
            cell.numberofDocs.text = "\(count)"
            
            if "\(UserDefaults.standard.value(forKey: "userId") ?? "0")" == announcement["created_by"].stringValue {
                
                cell.viewViews.isHidden = false
                
            }
            else{
                cell.viewViews.isHidden = true
                
            }
            cell.lblAnnouncement.handleHashtagTap { hashtag in
                print("Success. You just tapped the \(hashtag) hashtag")
                self.txtSearch.text  = hashtag
                self.txtSearch.becomeFirstResponder()
            }
            cell.lblAnnouncement.handleMentionTap { (mension) in
                print("Success. You just tapped the \(mension) mension")
                self.txtSearch.text  = mension
                self.txtSearch.becomeFirstResponder()
            }
            cell.lblAnnouncement.handleHashtagTap { (url) in
                print(url)
                guard let Requrl = URL(string: url) else { return }
                //                UIApplication.shared.open(Requrl)
                let vc = SFSafariViewController(url: Requrl)
                self.present(vc, animated: true, completion: nil)
                
                
                
            }
            return cell
        }
        else{
            
            let cell  = tableView.dequeueReusableCell(withIdentifier: "AnnouncementsTableViewCell", for: indexPath) as! AnnouncementsTableViewCell
            
            if indexPath.section == 0{
                cell.buttonDocs.tag = indexPath.row
                cell.buttonViews.tag = indexPath.row
                cell.buttonComments.tag = indexPath.row
                cell.buttonShareAnnouncement.tag = indexPath.row
                cell.buttonSideMenu.tag = indexPath.row
            }
            else{
                cell.buttonDocs.tag = indexPath.row+unreadAnnouncements.count
                cell.buttonViews.tag = indexPath.row+unreadAnnouncements.count
                cell.buttonComments.tag = indexPath.row+unreadAnnouncements.count
                cell.buttonShareAnnouncement.tag = indexPath.row+unreadAnnouncements.count
                cell.buttonSideMenu.tag = indexPath.row+unreadAnnouncements.count
            }
            
            //        cell.lblDate.text = announcement["createdAt"].string ?? ""
            cell.lblDate.text = HomeTabViewController.dateFormatterCommon(dateStr: announcement["createdAt"].stringValue)
            
            cell.lblPostedIn.text = announcement["created_user_name"].string ?? ""
            cell.lblUserName.text = announcement["group_name"].string ?? ""
            //                cell.lblAnnouncement.text = announcement["snap_description"].string ?? ""
            
            cell.lblnumberofComments.text = announcement["conversation_count"].string ?? ""
            
            cell.lblnumberofView.text = announcement["views_count"].string ?? ""
            
            if announcement["read_status"].boolValue{
                cell.lblAnnouncement.font = UIFont.systemFont(ofSize: 15.0)
                cell.lblAnnouncement.text = announcement["snap_description"].string ?? ""
            }
            else{
                cell.lblAnnouncement.font = UIFont.boldSystemFont(ofSize: 15.0)
                cell.lblAnnouncement.text = announcement["snap_description"].string ?? ""
            }
            
            var count = 0
            
            if let attachments = announcement["post_attachment"].array{
                
                for items in attachments{
                    if items["type"].stringValue.contains("image") {
                        count = count + 1
                    }
                }
                
                let proPic = announcement["family_logo"].stringValue
                
                
                if proPic.count != 0{
                    let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+proPic
                    let imgUrl = URL(string: temp)
                    cell.imageviewUserProfilepic.kf.indicatorType = .activity
                    cell.imageviewUserProfilepic.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Family Logo"), options: nil, progressBlock: nil, completionHandler: nil)
                }
                else{
                    cell.imageviewUserProfilepic.image = #imageLiteral(resourceName: "Family Logo")
                }
            }
            if announcement["is_shareable"].boolValue {
                cell.viewShare.isHidden = false
            }
            else{
                cell.viewShare.isHidden = true
                
            }
            if announcement["conversation_enabled"].boolValue{
                cell.viewComments.isHidden = false
            }
            else{
                cell.viewComments.isHidden = true
            }
            cell.numberofDocs.text = "\(count)"
            
            if "\(UserDefaults.standard.value(forKey: "userId") ?? "0")" == announcement["created_by"].stringValue {
                
                cell.viewViews.isHidden = false
                
            }
            else{
                cell.viewViews.isHidden = true
                
            }
            cell.lblAnnouncement.handleHashtagTap { hashtag in
                print("Success. You just tapped the \(hashtag) hashtag")
                self.txtSearch.text  = hashtag
                self.txtSearch.becomeFirstResponder()
            }
            cell.lblAnnouncement.handleMentionTap { (mension) in
                print("Success. You just tapped the \(mension) mension")
                self.txtSearch.text  = mension
                self.txtSearch.becomeFirstResponder()
            }
            cell.lblAnnouncement.handleURLTap { (url) in
                print(url)
                //                    UIApplication.shared.open(url)
                if url.absoluteString.contains("familheey"){
                    self.openAppLink(url: url.absoluteString)
                    //                            Helpers.openAppLinkCommon(url: url.absoluteString, currentVC: self)
                }
                else{
                    if !(["http", "https"].contains(url.scheme?.lowercased())) {
                        var strUrl = url.absoluteString
                        strUrl = "http://"+strUrl
                        let Turl = URL(string: strUrl)
                        
                        let vc = SFSafariViewController(url: Turl!)
                        self.present(vc, animated: true, completion: nil)
                    }
                    else{
                        let vc = SFSafariViewController(url: url)
                        self.present(vc, animated: true, completion: nil)
                    }
                }
                
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            if unreadAnnouncements.count != 0{
//                return 40
                return 0
            }
        }
        else if section == 1{
            if readAnnouncements.count != 0{
//                return 40
                return 0
            }
        }
        return 0
    }
    
   /* func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 20, y: 10, width: tableView.frame.width-40, height: 40))
        headerView.backgroundColor = .white
        let label = UILabel()
        label.frame = CGRect.init(x: 5, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
        label.textColor = .darkGray
        var headerString = String()
        if section == 0{
            headerString = "Unread announcement"
        }
        else{
            headerString = "Past announcement"
        }
        label.text = headerString
        headerView.addSubview(label)
        return headerView
    }*/
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        tableView.deselectRow(at: indexPath, animated: false)
        
        let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementDetailViewController") as! AnnouncementDetailViewController
        //        if indexPath.section == 0{
        //            vc.ArrayAnnouncements = unreadAnnouncments
        //        }
        //        else{
        //            vc.ArrayAnnouncements = readAnnouncements
        //        }
        vc.ArrayAnnouncements = ArrayAnnouncements
        
//        if indexPath.section == 0{
//            vc.selectedindex = indexPath
//        }
//        else{
//            vc.selectedindex = IndexPath.init(row: indexPath.row+unreadAnnouncements.count, section: 0)
//        }
        vc.selectedindex = indexPath
        
        if isFromHome{
            self.isFromHome = false
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else{
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
extension AnnouncementsViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        textField.resignFirstResponder()
        if textField == txtSearch
        {
            // selectWebAPI()
            if textField.text!.count > 0{
                btnSearchReset.isHidden = false
            }
            else{
                btnSearchReset.isHidden = true
            }
            textField.endEditing(true)
        }
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        self.view.endEditing(true)
        if textField == txtSearch
        {
            if textField.text!.count > 0{
                btnSearchReset.isHidden = false
            }
            else{
                btnSearchReset.isHidden = true
            }
            offset = 0
            getAnnouncements()
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
            // selectWebAPI()
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
        }
        return true
    }
}
