//
//  AnnouncementDetailViewController.swift
//  familheey
//
//  Created by Giri on 26/11/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON
import AVKit
import SafariServices


class AnnouncementDetailViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var ArrayAnnouncements = [JSON]()
    var selectedindex = IndexPath()
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    var postoptionsTittle = [String]()
    var indexOfCellBeforeDragging = 0
    var collectionViewLayout = UICollectionViewFlowLayout()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var currentID = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        print(ArrayAnnouncements)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionViewLayout.scrollDirection = .horizontal
        collectionViewLayout.minimumLineSpacing = 0
        self.collectionView.setCollectionViewLayout(collectionViewLayout, animated: true)
        collectionView.isHidden = true
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        
    }
    override func viewDidAppear(_ animated: Bool) {
        if appDelegate.isfromConversationAnnouncement && ArrayAnnouncements.count >= selectedindex.row{
            getAnnouncementDetails(id: "\(String(describing: currentID))")
        }
        if ArrayAnnouncements.count >= selectedindex.row{
            self.viewCurrentPost(index: selectedindex.row)
            self.collectionView.scrollToItem(at: selectedindex, at: [.centeredVertically, .centeredHorizontally], animated: false)
            //                    self.collectionView.scrollToItem(at: selectedindex, at: .centeredHorizontally, animated: true)
        }
        collectionView.isHidden = false
    }
    
    //MARK:- get details by id
    
    func getAnnouncementDetails(id: String){
        appDelegate.isfromConversationAnnouncement = false
        ActivityIndicatorView.show("Please wait....")
        let param = ["type":"announcement","post_id":id,"user_id":UserDefaults.standard.value(forKey: "userId") as! String]
        
        networkProvider.request(.post_by_user(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        if let response = jsonData["data"].array{
                            
                            if response.count != 0{
                                let announcement = response[0]
                                
                                self.ArrayAnnouncements[self.selectedindex.row] = announcement
                                
                                self.collectionView.reloadItems(at: [self.selectedindex])
                                
                            }
                            else{
                            }
                        }
                        else{
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getAnnouncementDetails(id: id)
                        }
                    }
                    else{
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
        
    }
    
    //MARK:- Custom Methods
    func viewCurrentPost(index:Int){
        if  ArrayAnnouncements.count >= index  {
            let post = ArrayAnnouncements[index]
            let param = ["post_id" : post["post_id"].stringValue, "user_id": UserDefaults.standard.value(forKey: "userId") as! String]
            networkProvider.request(.add_view_count(parameter: param)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                        print("view contents : \(jsonData)")
                        if response.statusCode == 200{
                            print("view contents : \(jsonData)")
                        }
                        else if response.statusCode == 401 {
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                self.viewCurrentPost(index: index)
                            }
                        }
                    }catch let err {
                        //                           ActivityIndicatorView.hiding()
                        // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                    }
                case .failure( let error):
                    //                       ActivityIndicatorView.hiding()
                    //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                    break
                }
            }
        }
    }
    
    func deletePostForCreator(tag:Int){
        let alert = UIAlertController(title: "Familheey", message: "Do you really want to delete this post? Deleting this post will delete it from everywhere you posted it to and you will lose all associated conversations", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "DELETE", style: .destructive, handler: { _ in
            let post = self.ArrayAnnouncements[tag]
            
            
            let param = ["id" : post["post_id"].stringValue,"user_id":UserDefaults.standard.value(forKey: "userId")as! String]
            
            ActivityIndicatorView.show("Please wait....")
            self.networkProvider.request(.deletePost(parameter: param)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                        print("view contents : \(jsonData)")
                        if response.statusCode == 200{
                            print("view contents : \(jsonData)")
                            //                            self.getPosts()
                        }
                        else if response.statusCode == 401 {
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                self.deletePostForCreator(tag: tag)
                            }
                        }
                        
                        ActivityIndicatorView.hiding()
                        
                    }catch let err {
                        ActivityIndicatorView.hiding()
                        // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                    }
                case .failure( let error):
                    ActivityIndicatorView.hiding()
                    //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                    break
                }
            }
            
        }))
        alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func removePostFromTimeline(tag:Int){
        let post = ArrayAnnouncements[tag]
        
        let param = ["post_id" : post["post_id"].stringValue, "user_id": UserDefaults.standard.value(forKey: "userId") as! String]
        
        ActivityIndicatorView.show("Please wait....")
        networkProvider.request(.removePost(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        print("view contents : \(jsonData)")
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.removePostFromTimeline(tag: tag)
                        }
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    //MARK:- Button Action
    
    @IBAction func closeAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ReadMoreAction(_ sender: UIButton) {
        if sender.isSelected{
            sender.setTitle("Read more...", for: .normal)
            let cell  = collectionView.cellForItem(at: IndexPath.init(item: sender.tag, section: 0)) as? AnnouncementDetailCollectionViewCell
            cell?.labelAnnouncementOutter.numberOfLines = 3
            sender.isSelected = false
            
        }
        else{
            sender.setTitle("Read less...", for: .normal)
            let cell  = collectionView.cellForItem(at: IndexPath.init(item: sender.tag, section: 0)) as? AnnouncementDetailCollectionViewCell
            cell?.labelAnnouncementOutter.numberOfLines = 0
            sender.isSelected = true
            
        }
        let post = ArrayAnnouncements[sender.tag]
    }
    @IBAction func imageAction(_ sender: UIButton) {
        let announcement = ArrayAnnouncements[sender.tag]
        if let attachment = announcement["post_attachment"].array{
            selectedindex = IndexPath.init(item: sender.tag, section: 0)
            let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsPreviewViewController") as! AnnouncementsPreviewViewController
            vc.attachments = attachment
            self.navigationController?.addFadeAnimation()
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    @IBAction func onClickViewConversation(_ sender: UIButton) {
        appDelegate.isfromConversationAnnouncement = true
        let post = ArrayAnnouncements[sender.tag]
        self.currentID = "\(post["post_id"].stringValue)"
        selectedindex = IndexPath.init(item: sender.tag, section: 0)
        let mainStoryBoard = UIStoryboard(name: "Conversations", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "ConversationsViewController") as! ConversationsViewController
        vc.post = post
        vc.conversationT = .announcement
        self.navigationController?.pushViewController(vc, animated:true)
    }
    
    @IBAction func onClickRightMenu(_ sender: UIButton) {
        let post = ArrayAnnouncements[sender.tag]
        selectedindex = IndexPath.init(item: sender.tag, section: 0)
        if post["created_by"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")" {
            postoptionsTittle =  ["Mute","Edit","Delete",]
        }
        else{
            postoptionsTittle =  ["Mute","Hide","Report"]
        }
        showActionSheet(titleArr: postoptionsTittle as NSArray, title: "Select options") { (index) in
            if index == 2{
                if post["created_by"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")"{
                    self.deletePostForCreator(tag: sender.tag)
                }
                else{
                    let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "ReportPostViewController") as! ReportPostViewController
                    
                    vc.postDetails =  self.ArrayAnnouncements[sender.tag]
                    vc.isFrom = "post"
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else if index == 1{
                if post["created_by"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")"{
                    
                    let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "CreateAnnouncmentViewController") as! CreateAnnouncmentViewController
                    vc.fromEdit = true
                    vc.editPostDetails = post
                    self.navigationController?.pushViewController(vc, animated: true)
                    //                  self.deletePostForCreator(tag: sender.tag)
                }else{
                    self.removePostFromTimeline(tag: sender.tag)
                }
            }
            else if index == 100{
            }
            else{
            }
        }
    }
    
    @IBAction func onClickAnnouncementViews(_ sender: UIButton) {
        
        let post = ArrayAnnouncements[sender.tag]
        selectedindex = IndexPath.init(item: sender.tag, section: 0)
        let mainStoryBoard = UIStoryboard(name: "fourth", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "UserFamilyListViewController") as! UserFamilyListViewController
        vc.postId = post["post_id"].stringValue
        vc.isFrompost = true
        vc.isFrom = "postview"
        self.navigationController?.pushViewController(vc, animated:true)
    }
    
    @IBAction func onClickAnnouncementDocs(_ sender: UIButton) {
    }
    
    @IBAction func onClickAnnouncementShare(_ sender: UIButton) {
        let post = ArrayAnnouncements[sender.tag]
        selectedindex = IndexPath.init(item: sender.tag, section: 0)
        let mainStoryBoard = UIStoryboard(name: "fourth", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "UserFamilyListViewController") as! UserFamilyListViewController
        vc.postId = post["post_id"].stringValue
        vc.isFrompost = true
        vc.isFrom = "usershare"
        self.navigationController?.pushViewController(vc, animated:true)
    }
    
}
extension AnnouncementDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ArrayAnnouncements.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AnnouncementDetailCollectionViewCell", for: indexPath) as! AnnouncementDetailCollectionViewCell
        
        cell.playIcon.isHidden = true
        cell.buttonAttachment.tag = indexPath.row
        cell.buttonDocs.tag = indexPath.row
        cell.buttonViews.tag = indexPath.row
        cell.buttonShares.tag = indexPath.row
        cell.buttonConversation.tag = indexPath.row
        cell.buttonReadmore.tag = indexPath.row
        cell.btnRightMenu.tag = indexPath.row
        cell.labelAnnouncementOutter.numberOfLines = 3
        let announcement = ArrayAnnouncements[indexPath.row]
//        self.viewCurrentPost(index: indexPath.row)
        
        //        cell.lblDate.text = announcement["createdAt"].string ?? ""
        cell.lblDate.text = HomeTabViewController.dateFormatterCommon(dateStr: announcement["createdAt"].stringValue)
        cell.lblPostedIn.text = announcement["created_user_name"].string ?? ""
        cell.lblUserNAme.text = announcement["group_name"].string ?? ""
        cell.lblNumberofShares.text = announcement["shared_user_count"].stringValue
        
        var proPic = ""
        if let sharedusers = announcement["shared_user_names"].string , !sharedusers.isEmpty{
            proPic = announcement["parent_family_logo"].stringValue
            
        }
        else{
            proPic = announcement["family_logo"].stringValue
        }
        if proPic.count != 0{
            let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+proPic
            let imgUrl = URL(string: temp)
            cell.imageViewProfilePic.kf.indicatorType = .activity
            cell.imageViewProfilePic.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        else{
            cell.imageViewProfilePic.image = #imageLiteral(resourceName: "Family Logo")
        }
        
        
        if let desc = announcement["snap_description"].string{
            
            cell.scrollviewBG.isHidden = false
            cell.labelAnnouncement.text = desc
            cell.labelAnnouncementOutter.text = desc
        }
        else{
            
            cell.scrollviewBG.isHidden = true
            cell.labelAnnouncement.text = ""
            cell.labelAnnouncementOutter.text = ""
        }
        
        if cell.labelAnnouncementOutter.calculateMaxLines() > 3{
            cell.buttonReadmore.isHidden = false
            cell.labelBottomSpaceConstraint.constant = 28
        }
        else{
            cell.buttonReadmore.isHidden = true
            cell.labelBottomSpaceConstraint.constant = 4
        }
        
        if announcement["conversation_enabled"].boolValue{
            cell.viewComments.isHidden = false
        }
        else{
            cell.viewComments.isHidden = true
        }
        
        
        if let attachment = announcement["post_attachment"].array{
            if attachment.count > 0 {
                cell.viewImageAnnouncementBG.isHidden = false
                if let type = attachment[0]["type"].string ,type.contains("image"){
                    
                    let temp = "\(BaseUrl.imaginaryURLForDetail)\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+attachment[0]["filename"].stringValue
                    let imgurl = URL(string: temp)
                    print(temp)
                    cell.imageViewAnnouncement.kf.indicatorType = .activity
                    cell.imageViewAnnouncement.kf.setImage(with: imgurl, options: [.transition(.fade(0.2))])
                    
                    cell.imageViewAnnouncement.kf.setImage(with: imgurl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil) { (image, error, cacheType, url) in
                        
                        if error == nil{
                            //
                            //                            let imageFrame = self.calculateRectOfImageInImageView(imageView: cell.imageViewAnnouncement)
                            //
                            //                            cell.imageViewHeight.constant = imageFrame.size.height + 20
                        }
                        
                    }
                    
                }
                else if let type = attachment[0]["type"].string ,type.contains("video"){
                    //                    let temp = "\(Helpers.imageURl)"+"\(BaseUrl.announcement)"+attachment[0]["filename"].stringValue
                    cell.playIcon.isHidden = false
                    
                    let thumb = attachment[0]["video_thumb"].stringValue
                    if thumb.count != 0{
                        
                        let temp = "\(Helpers.imageURl)"+thumb
                        let imgUrl = URL(string:BaseUrl.imaginaryURLForDetail + temp )
                        //                print(temp)
                        DispatchQueue.main.async {
                            cell.imageViewAnnouncement.kf.indicatorType = .activity
                            cell.imageViewAnnouncement.kf.setImage(with: imgUrl)
                        }
                    }
                    else{
                        //                        cell.imageViewProfilePic.image = #imageLiteral(resourceName: "imgNoImage")
                    }
                    //                    cell.imageViewAnnouncement.image = #imageLiteral(resourceName: "videoThumbnail")
                    
                }
                else{
                    cell.imageViewAnnouncement.image = #imageLiteral(resourceName: "DocumentTumb")
                    cell.playIcon.isHidden = true
                    
                }
                
                
                if attachment.count > 1{
                    cell.moreAvailView.isHidden = false
                    cell.lblNumberofAttachments.text = "+\(attachment.count-1)"
                }
                else{
                    cell.moreAvailView.isHidden = true
                    cell.lblNumberofAttachments.text = ""
                    
                }
                
            }
            else{
                cell.viewImageAnnouncementBG.isHidden = true
                
            }
        }
        else{
            cell.viewImageAnnouncementBG.isHidden = true
            
        }
        
        cell.lblNumberofComments.text = announcement["conversation_count"].string ?? ""
        
        cell.lblNumberofViews.text = announcement["views_count"].string ?? ""
        
        cell.lblNumberofShares.text = announcement["shared_user_count"].string ?? ""
        
        cell.lblNumberofDocs.text = "0"
        
        
        if "\(UserDefaults.standard.value(forKey: "userId") ?? "0")" == announcement["created_by"].stringValue {
            
            cell.viewViews.isHidden = false
            if announcement["is_shareable"].boolValue{
                 cell.viewShares.isHidden = false
            }
            else{
                cell.viewShares.isHidden = true
            }
        }
        else{
            cell.viewViews.isHidden = true
            cell.viewShares.isHidden = true
        }
        if let desc = announcement["snap_description"].string,!desc.isEmpty , let attachment = announcement["post_attachment"].array , attachment.count > 0{
            cell.scrollviewBG.isHidden = true
            cell.outterScroll.isHidden = false
        }
        else{
            cell.scrollviewBG.isHidden = false
            cell.outterScroll.isHidden = true
        }
        
        
        cell.labelAnnouncement.handleURLTap { (url) in
            print(url)
            //                    UIApplication.shared.open(url)
            if !(["http", "https"].contains(url.scheme?.lowercased())) {
                var strUrl = url.absoluteString
                strUrl = "http://"+strUrl
                let Turl = URL(string: strUrl)
                
                let vc = SFSafariViewController(url: Turl!)
                self.present(vc, animated: true, completion: nil)
            }
            else{
                let vc = SFSafariViewController(url: url)
                self.present(vc, animated: true, completion: nil)
            }
        }
        cell.labelAnnouncementOutter.handleURLTap { (url) in
            print(url)
            //                    UIApplication.shared.open(url)
            if !(["http", "https"].contains(url.scheme?.lowercased())) {
                var strUrl = url.absoluteString
                strUrl = "http://"+strUrl
                let Turl = URL(string: strUrl)
                
                let vc = SFSafariViewController(url: Turl!)
                self.present(vc, animated: true, completion: nil)
            }
            else{
                let vc = SFSafariViewController(url: url)
                self.present(vc, animated: true, completion: nil)
            }
        }
        
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //        if UIDevice.current.hasNotch{
        //            return CGSize.init(width: self.view.bounds.width , height: collectionView.bounds.height)
        //        }
        //        else{
        //            return CGSize.init(width: self.view.bounds.width , height: self.view.bounds.height)
        //        }
        
        if DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_XP{
            
            return CGSize.init(width: self.view.bounds.width, height:self.view.bounds.height-85);
        }else{
            
            return CGSize.init(width: self.view.bounds.width, height:self.view.bounds.height-65);
        }
        
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        
    }
    
//        func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
            
    //
    //        var currentCellOffset = scrollView.contentOffset
    //        currentCellOffset.x += (self.collectionView.bounds.width);
    //        let indepath = self.collectionView.indexPathForItem(at: currentCellOffset) ?? IndexPath.init(row: 0, section: 0)
    //        self.collectionView.scrollToItem(at: indepath, at: .centeredHorizontally, animated: true)
    //
//        }
    //
    func calculateRectOfImageInImageView(imageView: UIImageView) -> CGRect {
        let imageViewSize = imageView.frame.size
        let imgSize = imageView.image?.size
        
        guard let imageSize = imgSize else {
            return CGRect.zero
        }
        
        let scaleWidth = imageViewSize.width / imageSize.width
        let scaleHeight = imageViewSize.height / imageSize.height
        let aspect = fmin(scaleWidth, scaleHeight)
        
        var imageRect = CGRect(x: 0, y: 0, width: imageSize.width * aspect, height: imageSize.height * aspect)
        // Center image
        imageRect.origin.x = (imageViewSize.width - imageRect.size.width) / 2
        imageRect.origin.y = (imageViewSize.height - imageRect.size.height) / 2
        
        // Add imageView offset
        imageRect.origin.x += imageView.frame.origin.x
        imageRect.origin.y += imageView.frame.origin.y
        
        return imageRect
    }
    
    //      func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
    //            targetContentOffset.pointee = scrollView.contentOffset
    //            let pageWidth:Float = Float(self.collectionView.bounds.width)
    //            let minSpace:Float = 0.0
    //            var cellToSwipe:Double = Double(Float((scrollView.contentOffset.x))/Float((pageWidth+minSpace))) + Double(0.5)
    //            if cellToSwipe < 0 {
    //                cellToSwipe = 0
    //            } else if cellToSwipe >= Double(self.ArrayAnnouncements.count) {
    //                cellToSwipe = Double(self.ArrayAnnouncements.count) - Double(1)
    //            }
    //            let indexPath:IndexPath = IndexPath(row: Int(cellToSwipe), section:0)
    ////            page.currentPage = indexPath.item
    //            self.collectionView.scrollToItem(at:indexPath, at: UICollectionView.ScrollPosition.left, animated: true)
    //
    //
    //        }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        let pageWidth = self.collectionView.bounds.width
        let proportionalOffset = collectionView.contentOffset.x / pageWidth
        indexOfCellBeforeDragging = Int(round(proportionalOffset))
        
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if(scrollView.panGestureRecognizer.translation(in: scrollView.superview).x < 0) && indexOfCellBeforeDragging >= self.ArrayAnnouncements.count - 1{ //right swipe
            self.collectionView.scrollToItem(at:IndexPath.init(row: 0, section: 0), at: UICollectionView.ScrollPosition.left, animated: true)
        }
        for cell in collectionView.visibleCells {
            let indexPath = collectionView.indexPath(for: cell)
            self.viewCurrentPost(index: indexPath!.row)
        }

    }
    
}
extension UIDevice {
    var hasNotch: Bool {
        let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        return bottom > 0
    }
}
extension UIImageView {
    
    /******************************************************************************/
    /** @fcn        sizeToImage()
     *  @brief      size view to image
     *  @@assum     (image!=nil)
     */
    /******************************************************************************/
    func sizeToImage() {
        
        //Grab loc
        let xC = self.center.x;
        let yC = self.center.y;
        
        //Size to fit
        self.frame  = CGRect (x: 0, y: 0, width: (self.image?.size.width)!/2, height: (self.image?.size.height)!/2);
        
        //Move to loc
        self.center = CGPoint(x:xC, y:yC);
        
        return
    }
}
