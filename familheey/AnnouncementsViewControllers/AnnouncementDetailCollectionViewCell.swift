//
//  AnnouncementDetailCollectionViewCell.swift
//  familheey
//
//  Created by Giri on 26/11/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import ActiveLabel


class AnnouncementDetailCollectionViewCell: UICollectionViewCell, UIScrollViewDelegate {
    
    @IBOutlet weak var imageViewProfilePic: UIImageView!
    @IBOutlet weak var lblPostedIn: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblUserNAme: UILabel!
    @IBOutlet weak var buttonConversation: UIButton!
    @IBOutlet weak var lblNumberofDocs: UILabel!
    @IBOutlet weak var lblNumberofComments: UILabel!
    @IBOutlet weak var buttonDocs: UIButton!
    
    @IBOutlet weak var scrolView: UIScrollView!
    @IBOutlet weak var viewImageAnnouncementBG: UIView!
    @IBOutlet weak var tetxviewAnnouncement: UITextView!
    @IBOutlet weak var imageViewAnnouncement: UIImageView!
        
    @IBOutlet weak var centerViewConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var centerTextviewConstraint: NSLayoutConstraint!
    @IBOutlet weak var stackViewAnnouncement: UIStackView!
    
    @IBOutlet weak var labelAnnouncement: ActiveLabel!
    @IBOutlet weak var scrollviewBG: UIScrollView!
    @IBOutlet weak var viewComments: UIView!
    
    @IBOutlet weak var viewViews: UIView!
    @IBOutlet weak var lblNumberofViews: UILabel!
    @IBOutlet weak var buttonViews: UIButton!
    
    @IBOutlet weak var viewShares: UIView!
    @IBOutlet weak var lblNumberofShares: UILabel!
    @IBOutlet weak var buttonShares: UIButton!
    
    @IBOutlet weak var viewDocs: UIView!
    @IBOutlet weak var buttonAttachment: UIButton!
    
    @IBOutlet weak var btnRightMenu: UIButton!
    
    @IBOutlet weak var moreAvailView: UIView!
    @IBOutlet weak var lblNumberofAttachments: UILabel!
    @IBOutlet weak var buttonReadmore: UIButton!
    
    @IBOutlet weak var outterScroll: UIScrollView!
    
    @IBOutlet weak var labelAnnouncementOutter: ActiveLabel!
    @IBOutlet weak var labelBottomSpaceConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var playIcon: UIImageView!
    @IBOutlet weak var imageViewHeight: NSLayoutConstraint!
    override func awakeFromNib() {
        /*   self.scrolView.minimumZoomScale = scrolView.frame.size.width / imageViewAnnouncement.frame.size.width;
           self.scrolView.maximumZoomScale = 3.5
           self.scrolView.delegate = self */
       }
       
   /*    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
           return self.imageViewAnnouncement
       } */
}
