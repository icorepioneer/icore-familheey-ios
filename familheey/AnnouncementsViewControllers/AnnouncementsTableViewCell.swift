//
//  AnnouncementsTableViewCell.swift
//  familheey
//
//  Created by Giri on 25/11/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import ActiveLabel

class AnnouncementsTableViewCell: UITableViewCell {
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblPostedIn: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblAnnouncement: ActiveLabel!
    
    @IBOutlet weak var viewViews: UIView!
    @IBOutlet weak var viewDocs: UIView!
    @IBOutlet weak var viewComments: UIView!
    @IBOutlet weak var viewSideMenu: UIView!
    @IBOutlet weak var viewShare: UIView!
    @IBOutlet weak var imageviewUserProfilepic: UIImageView!
    
    
    @IBOutlet weak var lblnumberofComments: UILabel!
    @IBOutlet weak var numberofDocs: UILabel!
    @IBOutlet weak var lblnumberofView: UILabel!
    
    
    @IBOutlet weak var buttonShareAnnouncement: UIButton!
    @IBOutlet weak var buttonSideMenu: UIButton!
    @IBOutlet weak var buttonViews: UIButton!
    @IBOutlet weak var buttonDocs: UIButton!
    @IBOutlet weak var buttonComments: UIButton!
    
    
    //MARK:- Review post extra items
    
    @IBOutlet weak var btnAcceptReview: UIButton!
    @IBOutlet weak var btnRejectReview: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//MARK:- Shared Announcements

class AnnouncementsSharedTableViewCell: UITableViewCell {
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblPostedIn: UILabel!
    @IBOutlet weak var lblAnnouncement: ActiveLabel!
    @IBOutlet weak var btnSharedBy: UIButton!
    
    @IBOutlet weak var viewViews: UIView!
    @IBOutlet weak var viewDocs: UIView!
    @IBOutlet weak var viewComments: UIView!
    @IBOutlet weak var viewSideMenu: UIView!
    @IBOutlet weak var viewShare: UIView!
    @IBOutlet weak var imageviewUserProfilepic: UIImageView!
    
    
    @IBOutlet weak var lblnumberofComments: UILabel!
    @IBOutlet weak var numberofDocs: UILabel!
    @IBOutlet weak var lblnumberofView: UILabel!
    
    
    @IBOutlet weak var buttonShareAnnouncement: UIButton!
    @IBOutlet weak var buttonSideMenu: UIButton!
    @IBOutlet weak var buttonViews: UIButton!
    @IBOutlet weak var buttonDocs: UIButton!
    @IBOutlet weak var buttonComments: UIButton!
    
    
    @IBOutlet weak var lblsharedUserName: UILabel!
    @IBOutlet weak var lblSharedPostedIn: UILabel!
    @IBOutlet weak var lblSharedDate: UILabel!
    @IBOutlet weak var imageViewSharedUser: UIImageView!
    @IBOutlet weak var lblShared: UILabel!
    @IBOutlet weak var lblShare_width: NSLayoutConstraint!

    
    
    //MARK:- Review post extra items
    
    @IBOutlet weak var btnAcceptReview: UIButton!
    @IBOutlet weak var btnRejectReview: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

