//
//  AnnouncementPreviewCollectionViewCell.swift
//  familheey
//
//  Created by Giri on 03/12/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import AVKit

protocol AnnouncementPreviewImageCollectionViewCellDelegate : class {
        func UpdateView(isFullScreen:Bool)
}

class AnnouncementPreviewImageCollectionViewCell: UICollectionViewCell,UIScrollViewDelegate {
    
    var scrollView: UIScrollView!
    var imageViewPost: UIImageView!
    weak var delegate: AnnouncementPreviewImageCollectionViewCellDelegate?

    override func prepareForReuse() {

          imageViewPost.image =  nil
        
      }
    override func layoutSubviews() {
    }
    @objc func handleDoubleTapScrollView(recognizer: UITapGestureRecognizer) {
           if scrollView.zoomScale == 1 {
               scrollView.zoom(to: zoomRectForScale(scale: scrollView.maximumZoomScale, center: recognizer.location(in: recognizer.view)), animated: true)
           } else {
               scrollView.setZoomScale(1, animated: true)

           }
       }

       func zoomRectForScale(scale: CGFloat, center: CGPoint) -> CGRect {
           var zoomRect = CGRect.zero
           zoomRect.size.height = imageViewPost.frame.size.height / scale
           zoomRect.size.width  = imageViewPost.frame.size.width  / scale
           let newCenter = imageViewPost.convert(center, from: scrollView)
           zoomRect.origin.x = newCenter.x - (zoomRect.size.width / 2.0)
           zoomRect.origin.y = newCenter.y - (zoomRect.size.height / 2.0)
           return zoomRect
       }

       func viewForZooming(in scrollView: UIScrollView) -> UIView? {
           return self.imageViewPost
       }
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        
        if scrollView.zoomScale == scrollView.minimumZoomScale
        {
            delegate?.UpdateView(isFullScreen: false)
            
        }
        else{
            delegate?.UpdateView(isFullScreen: true)
            
        }
        
    }

    func setupUI(){
        let vWidth = self.frame.width
        let vHeight = self.frame.height

        scrollView = UIScrollView()
        scrollView.delegate = self
        scrollView.frame = CGRect(x: 0, y: 0, width: vWidth, height: vHeight)
        scrollView.backgroundColor = .clear
        scrollView.alwaysBounceVertical = false
        scrollView.alwaysBounceHorizontal = false
        scrollView.showsVerticalScrollIndicator = true
        scrollView.flashScrollIndicators()

        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 10.0

        let doubleTapGest = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTapScrollView(recognizer:)))
        doubleTapGest.numberOfTapsRequired = 2
        scrollView.addGestureRecognizer(doubleTapGest)

        self.addSubview(scrollView)

        imageViewPost = UIImageView(frame: CGRect(x: 0, y: 0, width: vWidth, height: vHeight))
        imageViewPost.image = UIImage(named: "cat")
        imageViewPost!.layer.cornerRadius = 11.0
        imageViewPost!.clipsToBounds = false
        imageViewPost.contentMode = .scaleAspectFit
        scrollView.addSubview(imageViewPost!)
    }
}


class AnnouncementPreviewVideoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var videoLayer: UIView!
    var player: AVPlayer!
    var avpController = AVPlayerViewController()
    @IBOutlet weak var imageViewDoc: UIImageView!
     @IBOutlet weak var labaleDocName: UILabel!
     @IBOutlet weak var buttonView: UIButton!

    override class func awakeFromNib() {
         
        
     }
}
