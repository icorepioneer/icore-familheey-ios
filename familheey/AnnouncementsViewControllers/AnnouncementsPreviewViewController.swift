//
//  AnnouncementsPreviewViewController.swift
//  familheey
//
//  Created by Giri on 27/11/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import SwiftyJSON
import AVKit
import Kingfisher
import SafariServices

class AnnouncementsPreviewViewController: UIViewController {
    @IBOutlet weak var collectionviewPreview: UICollectionView!
    @IBOutlet weak var viewTopBar: UIView!
    @IBOutlet weak var viewCloseButton: UIView!
    @IBOutlet weak var topBarHeight: NSLayoutConstraint!
    @IBOutlet weak var pageCntrlHeight: NSLayoutConstraint!
    @IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var viewDownloadButton: UIView!
    
    var documentInteractionController: UIDocumentInteractionController!
    
    var attachments = [JSON]()
    var isFromFamilyHistory = false
    var isFromConversation = false
    var isFromPost = false
    var typeAlbum = false
    var selectedIndex = 0
    var attachmentIndex = 0

    var panGestureRecognizer: UIPanGestureRecognizer?
    var originalPosition: CGPoint?
    var currentPositionTouched: CGPoint?

    
    var collectionViewLayout = UICollectionViewFlowLayout()
    var indexOfCellBeforeDragging = 0
    var Cellheight = CGFloat.init(0.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(panGestureAction(_:)))
         view.addGestureRecognizer(panGestureRecognizer!)
        ActivityIndicatorView.show("Please wait....")
        
        collectionviewPreview.delegate = self
        collectionviewPreview.dataSource = self
        collectionViewLayout.scrollDirection = .horizontal
        collectionViewLayout.minimumLineSpacing = 0
        self.collectionviewPreview?.setCollectionViewLayout(collectionViewLayout, animated: true)
        
        Cellheight = self.collectionviewPreview.bounds.height
        pageController.currentPage = selectedIndex
        pageController.numberOfPages = attachments.count
        if attachments.count == 1{
            pageController.isHidden = true
        }
        
        
    }
    @objc func panGestureAction(_ panGesture: UIPanGestureRecognizer) {
       let translation = panGesture.translation(in: view)

       if panGesture.state == .began {
         originalPosition = view.center
         currentPositionTouched = panGesture.location(in: view)
        self.view.backgroundColor = .lightGray
       } else if panGesture.state == .changed {
           view.frame.origin = CGPoint(
             x: translation.x,
             y: translation.y
           )
       } else if panGesture.state == .ended {
         let velocity = panGesture.velocity(in: view)

         if velocity.y >= 1000 {
           UIView.animate(withDuration: 0.2
             , animations: {
               self.view.frame.origin = CGPoint(
                 x: self.view.frame.origin.x,
                 y: self.view.frame.size.height
               )
             }, completion: { (isCompleted) in
               if isCompleted {
                self.navigationController?.addFadeAnimation()
                       self.navigationController?.popViewController(animated: false)
//                 self.dismiss(animated: false, completion: nil)
               }
           })
         } else {
           UIView.animate(withDuration: 0.2, animations: {
             self.view.center = self.originalPosition!
            self.view.backgroundColor = .black

           })
         }
       }
     }
    
    override func viewDidAppear(_ animated: Bool) {
        collectionviewPreview.scrollToItem(at: IndexPath.init(item: selectedIndex, section: 0), at: .centeredHorizontally, animated: true)
        ActivityIndicatorView.hiding()

    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    
    //MARK:- Button Actions
    @IBAction func backAction(_ sender: Any) {
        UIView.animate(withDuration: 0.2
            , animations: {
                self.view.frame.origin = CGPoint(
                    x: self.view.frame.origin.x,
                    y: self.view.frame.size.height
                )
        }, completion: { (isCompleted) in
            if isCompleted {
                self.navigationController?.addFadeAnimation()
                self.navigationController?.popViewController(animated: false)
            }
        })
//        downloadDocumentFromUrl(index: 0)
    }
    @IBAction func onClickDownload(_ sender: Any) {
        self.downloadDocumentFromUrl(index: attachmentIndex)
    }
    
    
    func downloadDocumentFromUrl(index:Int){
        /* let data = self.dataArr[index]
         print(data)
         
         // Url in String format
         let urlStr = data["url"].stringValue
         // Converting string to URL Object
         let url = URL(string: data["url"].stringValue)
         let fileName = String((url!.lastPathComponent)) as NSString
         
         let urlSession = URLSession(configuration: .default, delegate: self, delegateQueue: OperationQueue())
         
         let downloadTask = urlSession.downloadTask(with: url!)
         downloadTask.resume()*/
        
        
        let attachment = attachments[index]
        var temp:String!
        ActivityIndicatorView.show("Please wait downloading.......")
        if self.isFromFamilyHistory
        {
            temp = "\(Helpers.imageURl)"+"\(BaseUrl.history_images)"+attachment["filename"].stringValue
        }
        else if self.isFromConversation{
            
            temp = "\(Helpers.imageURl)"+"\(BaseUrl.conversation)"+attachment["filename"].stringValue
            
        }
        else if self.isFromPost{
            if self.typeAlbum{
                temp = "\(Helpers.imageURl)"+"\(BaseUrl.document_image)"+attachment["filename"].stringValue
            }
            else{
                temp = "\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+attachment["filename"].stringValue
            }
        }
        else{
            if self.typeAlbum{
                temp = "\(Helpers.imageURl)"+"\(BaseUrl.document_image)"+attachment["filename"].stringValue
            }
            else{
                temp = "\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+attachment["filename"].stringValue
            }
        }
        
        let fileURL = URL(string: temp)!
        let dateStr = "\(Date().timeIntervalSince1970)"
        let fileName1 = String((fileURL.lastPathComponent)) as NSString
        //        let tempStr = dateStr.trimmingCharacters(in: .whitespacesAndNewlines)
        //        let strArr = tempStr.components(separatedBy: "+")
        let dateArr = dateStr.components(separatedBy: ".")
        var strDateTemp = ""
        if dateArr.count > 0{
            strDateTemp = dateArr[0]
        }
        else{
            strDateTemp = dateStr
        }
        let fileName =  strDateTemp + "_" + (fileName1 as String)
        
        // Create destination URL
        let documentsUrl:URL =  (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL?)!
        let destinationFileUrl = documentsUrl.appendingPathComponent("\(fileName)")
        //Create URL to the source file you want to download
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        let request = URLRequest(url:fileURL)
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            if let tempLocalUrl = tempLocalUrl, error == nil {
                // Success
                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                    print("Successfully downloaded. Status code: \(statusCode)")
                    
                }
                ActivityIndicatorView.hiding()
                //self.displayAlertChoice(alertStr: "Please choose where to save image", title: "") { (result) in
                  //  self.saveToDevice(documentsUrl: documentsUrl, tempLocalUrl: tempLocalUrl, destinationFileUrl: destinationFileUrl)
                //}
                do {
                    try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                    
                    print("tempLocalUrl : \(tempLocalUrl)")
                    print("destinationFileUrl : \(destinationFileUrl)")
                    
                    do {
                        //Show UIActivityViewController to save the downloaded file
                        let contents  = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
                        for indexx in 0..<contents.count {
                            if contents[indexx].lastPathComponent == destinationFileUrl.lastPathComponent {
                              
                                if FileManager.default.fileExists(atPath: destinationFileUrl.absoluteString) {
                                    // Delete file
                                    try FileManager.default.removeItem(atPath: destinationFileUrl.absoluteString)
                                } else {
                                    print("File does not exist")
                                }
                                
                              DispatchQueue.main.async {
                                  
                                  let activityViewController = UIActivityViewController(activityItems: [contents[indexx]], applicationActivities: nil)
                                  self.present(activityViewController, animated: true, completion: nil)
                              }
                              
                          }
                        }
                    }
                    catch (let err) {
                        print("error: \(err)")
                    }

                } catch (let writeError) {
                    print("Error creating a file \(destinationFileUrl) : \(writeError)")
                }

            } else {
                
                print("Error took place while downloading a file. Error description: \(error?.localizedDescription ?? "")")
            }
        }
        task.resume()
        
    }
    
    func saveToDevice(documentsUrl:URL, tempLocalUrl:URL, destinationFileUrl:URL){


    }
    
    func gotoPreview(url: String) {
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: "ChangePicturesViewController") as! ChangePicturesViewController
        vc.isCoverOrProfilePic = "CoverPic"
        vc.ids           = UserDefaults.standard.value(forKey: "userId") as! String
        vc.isFrom = "post"
        vc.inFor = "cover"
        vc.isAdmin = ""
        
        vc.imageUrl = url
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @objc func playerItemFailedToPlay(_ notification: Notification) {
        let error = notification.userInfo?[AVPlayerItemFailedToPlayToEndTimeErrorKey] as? Error
        print(error)

    }
    
    
}
extension AnnouncementsPreviewViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,AnnouncementPreviewImageCollectionViewCellDelegate, UIDocumentInteractionControllerDelegate {
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        attachments.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let attachment = attachments[indexPath.item]
        attachmentIndex = indexPath.item
        
        if let type = attachment["type"].string ,type.contains("image"){
            self.viewDownloadButton.isHidden = false
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AnnouncementPreviewImageCollectionViewCell", for: indexPath) as! AnnouncementPreviewImageCollectionViewCell
            var temp:String!
            if self.isFromFamilyHistory
            {
                temp = "\(BaseUrl.imaginaryURLForDetail)\(Helpers.imageURl)"+"\(BaseUrl.history_images)"+attachment["filename"].stringValue
            }
            else if self.isFromConversation{
                
                temp = "\(BaseUrl.imaginaryURLForDetail)\(Helpers.imageURl)"+"\(BaseUrl.conversation)"+attachment["filename"].stringValue
                
            }
            else if self.isFromPost{
                if self.typeAlbum{
                    temp = "\(BaseUrl.imaginaryURLForDetail)\(Helpers.imageURl)"+"\(BaseUrl.document_image)"+attachment["filename"].stringValue
                }
                else{
                    temp = "\(BaseUrl.imaginaryURLForDetail)\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+attachment["filename"].stringValue
                }
                
                
            }
            else{
                if self.typeAlbum{
                    temp = "\(BaseUrl.imaginaryURLForDetail)\(Helpers.imageURl)"+"\(BaseUrl.document_image)"+attachment["filename"].stringValue
                }
                else{
                    temp = "\(BaseUrl.imaginaryURLForDetail)\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+attachment["filename"].stringValue
                }
            }
            
            let imgurl = URL(string: temp)
            
            cell.setupUI()
            cell.scrollView.isUserInteractionEnabled = true
            cell.imageViewPost.kf.indicatorType = .activity
            cell.imageViewPost.kf.setImage(with: imgurl, placeholder:#imageLiteral(resourceName: "imgNoImage") , options: [.transition(.fade(0.2))], progressBlock: nil, completionHandler: nil)
            
            cell.delegate = self
            viewTopBar.isHidden = false
            viewCloseButton.isHidden = true
//            collectionView.collectionViewLayout.invalidateLayout()
            return cell
        }
        else if let type = attachment["type"].string ,type.contains("video"){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AnnouncementPreviewVideoCollectionViewCell", for: indexPath) as! AnnouncementPreviewVideoCollectionViewCell
            var temp:String!
            
            if self.typeAlbum{
                temp = "\(Helpers.imageURl)"+"\(BaseUrl.document_image)"+attachment["filename"].stringValue
            }
            else{
                temp = "\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+attachment["filename"].stringValue
            }
            
            
            if self.isFromConversation{
                
                temp = "\(Helpers.imageURl)"+"\(BaseUrl.conversation)"+attachment["filename"].stringValue
                
            }
            else if self.isFromFamilyHistory
            {
                temp = "\(Helpers.imageURl)"+"\(BaseUrl.history_images)"+attachment["filename"].stringValue
            }
            else if self.isFromPost
            {
                if self.typeAlbum{
                    temp = "\(Helpers.imageURl)"+"\(BaseUrl.document_image)"+attachment["filename"].stringValue
                }
                else{
                    temp = "\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+attachment["filename"].stringValue
                }
            }
            
            let url = URL(string:temp)
            
            cell.player = AVPlayer(url: url!)
            
            cell.avpController.player = cell.player
            
            cell.avpController.view.frame.size.height = cell.videoLayer.frame.size.height - 16
            
            cell.avpController.view.frame.size.width = cell.videoLayer.frame.size.width
            cell.avpController.showsPlaybackControls = true
            do {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
            }
            catch {
                // report for an error
            }
            cell.videoLayer.addSubview(cell.avpController.view)
            
            //            collectionView.collectionViewLayout.invalidateLayout()
            viewTopBar.isHidden = true
            viewCloseButton.isHidden = false
            self.viewDownloadButton.isHidden = false
            return cell
            
        }
        else{
            self.viewDownloadButton.isHidden = false
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AnnouncementPreviewVideoCollectionViewCell", for: indexPath) as! AnnouncementPreviewVideoCollectionViewCell
            
            var name = attachment["original_name"].string ?? attachment["file_name"].stringValue
            if name.isEmpty{
                name  = attachment["filename"].stringValue
            }
            
            cell.labaleDocName.text = name
            if attachment["original_name"].stringValue.lowercased().contains("pdf"){
                cell.imageViewDoc.image = #imageLiteral(resourceName: "PdfImage")
            }
            else{
                cell.imageViewDoc.image = #imageLiteral(resourceName: "docsIcon")
            }
            //            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AnnouncementPreviewImageCollectionViewCell", for: indexPath) as! AnnouncementPreviewImageCollectionViewCell
            //
            //            cell.setupUI()
            //            cell.scrollView.isUserInteractionEnabled = false
            //            cell.imageViewPost.image = #imageLiteral(resourceName: "DocumentTumb")
            ////            collectionView.collectionViewLayout.invalidateLayout()
            //            viewTopBar.isHidden = false
            //            viewCloseButton.isHidden = true
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let attachment = attachments[indexPath.item]

//        if let type = attachment["type"].string ,type.contains("image"){
//            return CGSize.init(width: collectionView.bounds.width, height:self.view.bounds.height);
//        }
//        else{
        if DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_XP{
              
            return CGSize.init(width: self.view.bounds.width, height:self.view.bounds.height-85);
             }else{
                 
            return CGSize.init(width: self.view.bounds.width, height:self.view.bounds.height-65);
             }
        

//        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        pageController.currentPage = indexPath.item
        if let cell = cell as?AnnouncementPreviewVideoCollectionViewCell{
                if (cell.player != nil){
                    NotificationCenter.default.addObserver(self, selector: #selector(playerItemFailedToPlay), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: cell.player)
                    cell.player.play()
                    
                }
            }
    }
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        
        if let cell = cell as?AnnouncementPreviewVideoCollectionViewCell{
            
            if (cell.player != nil){
                cell.player.pause()
                
            }
        }
        
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let attachment = attachments[indexPath.item]
        
        
        if let type = attachment["type"].string ,type.contains("image"){
            
            
            var temp:String!
            if self.isFromFamilyHistory
            {
                temp = "\(Helpers.imageURl)"+"\(BaseUrl.history_images)"+attachment["filename"].stringValue
            }
            else if self.isFromConversation{
                
                temp = "\(Helpers.imageURl)"+"\(BaseUrl.conversation)"+attachment["filename"].stringValue
                
            }
            else if self.isFromPost
            {
                temp = "\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+attachment["filename"].stringValue
            }
            else
            {
                temp = "\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+attachment["filename"].stringValue
            }
            
            gotoPreview(url: temp)
            
        }
        if let type = attachment["type"].string ,( type.contains("text") || type.contains("pdf") || type.contains("doc") || type.contains("docx") || type.contains("xls") || type.contains("xlsx") || type.contains("msword") || type.contains("txt"))  {
            var temp:String!
            var filename = attachment["filename"].string ?? attachment["file_name"].stringValue
            if filename.isEmpty{
                filename = attachment["original_name"].stringValue
            }
            if self.isFromFamilyHistory
            {
                temp = "\(Helpers.imageURl)"+"\(BaseUrl.history_images)"+filename
            }
            else if self.isFromConversation{
                
                temp = "\(Helpers.imageURl)"+"\(BaseUrl.conversation)"+filename
                
            }
            else if self.isFromPost
            {
                temp = "\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+filename
            }
            else
            {
                temp = "\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+filename
            }
            
            let url = URL(string: temp)!
             ActivityIndicatorView.show("Loading....")
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else { return }
                let tmpURL = FileManager.default.temporaryDirectory
                    .appendingPathComponent(response?.suggestedFilename ?? "fileName.pdf")
                do {
                    try data.write(to: tmpURL)
                } catch {
                    print(error)
                    ActivityIndicatorView.hiding()
                }
                DispatchQueue.main.async {
                    /// STOP YOUR ACTIVITY INDICATOR HERE
                    ActivityIndicatorView.hiding()
                    print(tmpURL)
                    self.share(url: tmpURL)
                }
            }.resume()
            
           /* let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
            
            let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementDocPreviewViewController") as! AnnouncementDocPreviewViewController
            vc.UrlToLoad = temp
            self.present(vc, animated: true, completion: nil)*/

        }
        
        
    }
    
    func share(url: URL) {
        print(url)
        documentInteractionController = UIDocumentInteractionController(url: url)
        //        documentInteractionController.uti = url.typeIdentifier ?? "public.data, public.content"
        //        documentInteractionController.name = url.localizedName ?? url.lastPathComponent
        documentInteractionController.delegate = self
        documentInteractionController.presentPreview(animated: true)
    }

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {

    let pageWidth = self.collectionviewPreview.bounds.width
        let proportionalOffset = collectionviewPreview.contentOffset.x / pageWidth
        indexOfCellBeforeDragging = Int(round(proportionalOffset))

    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
     
        
        if(scrollView.panGestureRecognizer.translation(in: scrollView.superview).x < 0) && indexOfCellBeforeDragging >= self.attachments.count - 1{ //right swipe
            self.collectionviewPreview.scrollToItem(at:IndexPath.init(row: 0, section: 0), at: UICollectionView.ScrollPosition.left, animated: true)
//            pageController.currentPage = 0
        }
        
        for cell in self.collectionviewPreview.visibleCells {
            let indexPath = collectionviewPreview.indexPath(for: cell)
            attachmentIndex = indexPath!.item
        }
        
    }


    func UpdateView(isFullScreen:Bool){
        
        if isFullScreen{
            viewTopBar.isHidden = true
            topBarHeight.constant = 0
            pageCntrlHeight.constant = 0
            pageController.isHidden = true
        }
        else{
            viewTopBar.isHidden = false
            topBarHeight.constant = 55
            pageCntrlHeight.constant = 10
            pageController.isHidden = false
        }
        
        
    }
    
}
