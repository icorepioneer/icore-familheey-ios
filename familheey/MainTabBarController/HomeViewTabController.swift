//
//  HomeViewTabController.swift
//  familheey
//
//  Created by familheey on 12/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON

class HomeViewTabController: UITabBarController {
    
    @IBOutlet weak var tabbar: CustomizedTabBar!
    
    var isFromInvite: Bool = false
    var isType:String = ""
    var itemId:String = ""
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var isFromShare = false
    var networkProvider = MoyaProvider<FamilyheeyApi>()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = false
        self.delegate = self
        if isFromInvite || appDel.fromInvite{
            appDel.fromInvite = false
            if appDel.invitetype.lowercased() == "event"{
                self.selectedIndex = 4
                navigationController?.navigationBar.isTranslucent = true
                DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                    let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
                    vc.isFromNotification = "true"
                    vc.enableEventEdit = false
                    vc.eventId = appDel.invitetypeId
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }
            else if appDel.invitetype.lowercased() == "family" || appDel.invitetype.lowercased() == "groups"{
                self.selectedIndex = 1
                navigationController?.navigationBar.isTranslucent = true
                DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
                    intro.groupId = appDel.invitetypeId
                self.navigationController?.pushViewController(intro, animated: false)
                }
            }
            else if appDel.invitetype.lowercased() == "posts" || appDel.invitetype.lowercased() == "post"{
                self.selectedIndex = 0
                navigationController?.navigationBar.isTranslucent = true
                DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
                let intro = storyboard.instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
                    intro.postId = appDel.invitetypeId
                self.navigationController?.pushViewController(intro, animated: false)
                }
            }
            else if appDel.invitetype.lowercased().contains("announcement"){
                self.selectedIndex = 0
                navigationController?.navigationBar.isTranslucent = true
                getAnnouncementDetails(id: appDel.invitetypeId)
                
            }
            else if isType.lowercased() == "nofamily"{
                self.selectedIndex = 0
            }
            else if appDel.invitetype.lowercased() == "request" || appDel.invitetype.lowercased() == "payment"{
                let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "RequestDetailsViewController") as! RequestDetailsViewController
                vc.requestId = appDel.invitetypeId
                //        vc.requestDic = need
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else{
                self.selectedIndex = 0
            }
        }
        else if appDel.isFromNotification{
            
        }
//        else if isFromShare{
//            let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
//            let vc = storyboard.instantiateViewController(withIdentifier: "CreatePostViewController") as! CreatePostViewController
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
//       else if((UserDefaults.standard.value(forKey: "PermissionsReloadClass")) != nil){
//            if (UserDefaults.standard.value(forKey: "PermissionsReloadClass")) as! String == "CreatePostViewController" {
//
//                UserDefaults.standard.set(nil, forKey: "PermissionsReloadClass")
//
//                print(UserDefaults.standard.value(forKey: "PermissionsReloadClass") as Any)
//                let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
//                let vc = storyboard.instantiateViewController(withIdentifier: "CreatePostViewController") as! CreatePostViewController
//                self.navigationController?.pushViewController(vc, animated: false)
//            }
//        }
        else{

//            else{
                guard let isFirst = UserDefaults.standard.value(forKey: "isFirst_Time") as? String else{
                    return
                }
                
                if isFirst == "1"{
                    appDel.noFamily = true
                    appDel.postcreatedInPublic = false

                    self.selectedIndex = 0
                    navigationController?.navigationBar.isTranslucent = true
                }
                if appDel.noFamily{
                    self.selectedIndex = 0
                    navigationController?.navigationBar.isTranslucent = true
                }
                else{
                    self.selectedIndex = 0
                    navigationController?.navigationBar.isTranslucent = true
                }
//            }
        }
        
//        
//        self.tabBar.backgroundImage = UIImage()
//        self.tabBar.shadowImage = UIImage()
//        let bg = UIView()
//        bg.backgroundColor = .clear
//        bg.frame = CGRect.init(x: 0, y: 0, width: self.tabBar.bounds.width, height: 60)
//        
//        let tabBarView = UIImageView()
//        tabBarView.backgroundColor = .white
//        tabBarView.layer.cornerRadius = 10.0
//        tabBarView.clipsToBounds = true
//        tabBarView.frame = CGRect(x:10, y: -2, width: self.tabBar.bounds.width-20, height: 45)
//        bg.addSubview(tabBarView)
//        self.tabBar.addSubview(bg)
//        self.tabBar.sendSubviewToBack(bg)
    }
    

    
    func getAnnouncementDetails(id: String){
        
        ActivityIndicatorView.show("Please wait....")
        let param = ["type":"announcement","post_id":id,"user_id":UserDefaults.standard.value(forKey: "userId") as! String]
        
        networkProvider.request(.post_by_user(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        if let response = jsonData["data"].array{
                            
                            if response.count != 0{
                                let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementDetailViewController") as! AnnouncementDetailViewController
                                vc.ArrayAnnouncements = response
                                vc.selectedindex = IndexPath.init(row: 0, section: 0)
                                self.navigationController?.pushViewController(vc, animated: false)
                            }
                            else{
                                self.displayAlert(alertStr: "Oops! This content is no longer available.", title: "")
                            }
                        }
                        else{
                            self.displayAlert(alertStr: "Oops! This content is no longer available.", title: "")
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getAnnouncementDetails(id: id)
                        }
                    }

                    else{
                        self.displayAlert(alertStr: "Oops! This content is no longer available.", title: "")
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
        
    }

}
extension HomeViewTabController: UITabBarControllerDelegate{
    // UITabBarDelegate
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        print("Selected item")
        print(item.tag)
        if item.tag == 3{
            var titileArr = [String]()

            if appDelegate.noFamily{
                titileArr = ["Create Post","Create Event","Create Family"]
            }
            else{
//                titileArr  = ["Create Post","Make an Announcement","Create Event","Create Family","Request for Help"]
                
                
                //remya added
               titileArr  = ["Create Post","Make an Announcement","Create Event","Create Family","Create Request"]
                
                
            }
          
            showActionSheet(titleArr: titileArr as NSArray, title: "Select Option") { (index) in
                if index == 0{
                    let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "CreatePostViewController") as! CreatePostViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else if index == 1{
                    if self.appDelegate.noFamily{
                        let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "CreateEventStep1ViewController") as! CreateEventStep1ViewController
                        self.navigationController?.pushViewController(vc, animated: true)
                        return
                    }
                    else{
                        let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "CreateAnnouncmentViewController") as! CreateAnnouncmentViewController
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                else if index == 100{
                }
                else if index == 3{
                    let storyBoard = UIStoryboard.init(name: "CreateFamily", bundle: nil)
                    let addFamily = storyBoard.instantiateViewController(withIdentifier: "addFamilyOne") as! AddFamilyScreenOneViewController
                           //  loginView.regType = regType
                    self.navigationController?.pushViewController(addFamily, animated: true)
                }
                else if index == 4{
                    let storyBoard = UIStoryboard.init(name: "PostRequest", bundle: nil)
                    let addFamily = storyBoard.instantiateViewController(withIdentifier: "CreateRequestViewController") as! CreateRequestViewController
                           //  loginView.regType = regType
                    self.navigationController?.pushViewController(addFamily, animated: true)
                }
                else  if index == 2{
                    if self.appDelegate.noFamily{
                        let storyBoard = UIStoryboard.init(name: "CreateFamily", bundle: nil)
                        let addFamily = storyBoard.instantiateViewController(withIdentifier: "addFamilyOne") as! AddFamilyScreenOneViewController
                        //  loginView.regType = regType
                        self.navigationController?.pushViewController(addFamily, animated: true)
                    }
                    else{
                        let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "CreateEventStep1ViewController") as! CreateEventStep1ViewController
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                else {
                    
//                    let storyboard = UIStoryboard.init(name: "Topic", bundle: nil)
//                    let vc = storyboard.instantiateViewController(withIdentifier: "CreateTopicViewController") as! CreateTopicViewController
//                    vc.isFromCreate = true
//                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
        else if item.tag == 4{
            appDelegate.isFromDiscoverTab = true
        }
        
    }
    
    // UITabBarControllerDelegate
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        print("Selected view controller")
        
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if viewController is  AddTabViewController {
            return false
        }
        else{
            return true
        }
//        return true
    }
}
