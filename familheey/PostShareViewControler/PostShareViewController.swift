//
//  PostShareViewController.swift
//  familheey
//
//  Created by familheey on 25/11/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Moya
import Alamofire
import SwiftyJSON

class PostShareViewController: UIViewController {
    @IBOutlet weak var lblHead: UILabel!
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearchReset: UIButton!
    
    @IBOutlet weak var imgFamily: UIImageView!
    @IBOutlet weak var btnFamily: UIButton!
    @IBOutlet weak var imgPeople: UIImageView!
    @IBOutlet weak var btnPeople: UIButton!
    
    @IBOutlet weak var tblListView: UITableView!
    @IBOutlet weak var peopleVew: UIView!
    @IBOutlet weak var familyVew: UIView!
    
    var searchTxt = ""
    var selectedUserIDArr                     = NSMutableArray()
    private var networkProvider               = MoyaProvider<FamilyheeyApi>()
    var typeOfInvitations                     = String()
    var selectedIndex = 0
    var fromAnnouncement = false
    var postId = "", eventId  = ""
    //    var FamilyArr : familyListResponse!
    var FamilyArr = JSON()
    var selectedGroups = [[String:AnyObject]]()
    var selectedIdGroup   = NSMutableDictionary()
    
    var peopleListArr                         = JSON()
    var isFromFolder                          = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Helpers.setleftView(textfield: txtSearch, customWidth: 40)
        self.tabBarController?.tabBar.isHidden = true
        txtSearch.delegate = self
        if self.fromAnnouncement
        {
            self.lblHead.text = "Share Announcement"
        }
        else
        {
            self.lblHead.text = "Share Post"
            
        }
        
        print("####### \(postId)")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let button = UIButton()
        self.onClickTabSelection(button)
        // getFamilyList()
        
        if selectedIndex == 0{
            let headerNib = UINib.init(nibName: "FamilyListingView", bundle: Bundle.main)
            tblListView.register(headerNib, forHeaderFooterViewReuseIdentifier: "FamilyListingView")
        }
        
        if fromAnnouncement{
            peopleVew.isHidden = true
        }
        else{
            peopleVew.isHidden = false
        }
    }
    
    
    
    //MARK:- WEBAPI
    
    func getFamilyList(){
        ActivityIndicatorView.show("Loading....")
        let parameter = ["user_id" : "\(UserDefaults.standard.value(forKey: "userId") as! String)" ,"query":self.txtSearch.text!]
        ActivityIndicatorView.show("Loading...")
        
        networkProvider.request(.getFamilyListForPost(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    let jsonData = JSON(response.data)
                    if response.statusCode == 200
                    {
                        self.FamilyArr = jsonData["data"]
                        if self.FamilyArr.count > 0{
                            self.tblListView.delegate = self
                            self.tblListView.dataSource = self
                            self.tblListView.isHidden = false
                            self.tblListView.reloadData()
                        }
                        else{
                            self.tblListView.isHidden = true
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getFamilyList()
                        }
                    }
                    else{
                        self.tblListView.isHidden = true
                    }
                } catch let err {
                    
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
    
    func callGroupMembers(){
        
        ActivityIndicatorView.show("Loading....")
        
        //    let userId = UserDefaults.standard.value(forKey: "userId") as! Int
        
        let parameter = ["user_id" : "\(UserDefaults.standard.value(forKey: "userId") as! String)", "event_id" : eventId ,"query":self.txtSearch.text!]
        
        print("param : \(parameter)")
        ActivityIndicatorView.show("Loading...")
        
        networkProvider.request(.user_group_members(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                do {
                    let jsonData = JSON(response.data)
                    print(jsonData)
                    if response.statusCode == 200{
                        self.peopleListArr = jsonData["data"]
                        if self.peopleListArr.count > 0{
                            self.tblListView.delegate = self
                            self.tblListView.dataSource = self
                            self.tblListView.isHidden = false
                            self.tblListView.reloadData()
                        }
                        else{
                            self.tblListView.isHidden = true
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.callGroupMembers()
                        }
                    }
                    else{
                        self.tblListView.isHidden = true
                    }
                } catch let err {
                    
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
    
    //MARK:- Custom
    @IBAction func inviteFamilySelection(sender: UIButton){
        
        print("tag : \(sender.tag)")
        
        if selectedIndex == 1{
            
            if selectedUserIDArr.contains(self.peopleListArr.arrayValue[sender.tag]["user_id"].intValue){
                
                selectedUserIDArr.remove(self.peopleListArr.arrayValue[sender.tag]["user_id"].intValue)
            }else{
                selectedUserIDArr.add(self.peopleListArr.arrayValue[sender.tag]["user_id"].intValue)
            }
            
        }else{
            if selectedGroups.count > 0{
                var i = 0
                for n in 0..<selectedGroups.count {
                    let temp = selectedGroups[n] as NSDictionary
                    let str = temp.value(forKey: "id") as! String
                    if str == "\(self.FamilyArr.arrayValue[sender.tag]["id"].intValue)"{
                        break
                    }
                    else{
                        i = i + 1
                    }
                }
                if i == selectedGroups.count{
                    selectedIdGroup.setValue("\(self.FamilyArr.arrayValue[sender.tag]["id"].intValue)", forKey: "id")
                    if fromAnnouncement{
                        selectedIdGroup.setValue("\(self.FamilyArr.arrayValue[sender.tag]["announcement_create"].intValue)", forKey: "announcement_create")
                    }
                    else{
                        selectedIdGroup.setValue("\(self.FamilyArr.arrayValue[sender.tag]["post_create"].intValue)", forKey: "post_create")
                    }
                    selectedGroups.append(selectedIdGroup as! [String:AnyObject])
                }
                else{
                    selectedGroups.remove(at: i)
                }
            }
            else{
                selectedIdGroup.setValue("\(self.FamilyArr.arrayValue[sender.tag]["id"].intValue)", forKey: "id")
                if fromAnnouncement{
                    selectedIdGroup.setValue("\(self.FamilyArr.arrayValue[sender.tag]["announcement_create"].intValue)", forKey: "announcement_create")
                }
                else{
                    selectedIdGroup.setValue("\(self.FamilyArr.arrayValue[sender.tag]["post_create"].intValue)", forKey: "post_create")
                }
                selectedGroups.append(selectedIdGroup as! [String:AnyObject])
            }
            
            /*   if selectedUserIDArr.contains(FamilyArr.familyList![sender.tag].faId){
             
             selectedUserIDArr.remove(FamilyArr.familyList![sender.tag].faId)
             }else{
             selectedUserIDArr.add(FamilyArr.familyList![sender.tag].faId)
             }
             
             if selectedUserIDArr.count > 0{
             
             }else{
             //                invitedPeopleNameLbl.text = ""
             }*/
        }
        
        
        
        tblListView.reloadSections(IndexSet(integer : sender.tag), with: .none)
        
        //print(FamilyArr.familyList![sender.tag].faId)
    }
    
    
    
    func callinvite(param : Parameters){
        
        print("params : \(param)")
        
        ActivityIndicatorView.show("Loading....")
        
        networkProvider.request(.post_share(parameter: param), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    let jsonData = JSON(response.data)
                    
                    print("Json data : \(jsonData)")
                    
                    if response.statusCode == 200
                    {
                        var successMessage:String = ""
                        if self.fromAnnouncement
                        {
                            successMessage = "Announcement shared successfully"
                        }
                        else
                        {
                            successMessage = "Post shared successfully"
                            
                        }
                        let alert = UIAlertController(title: "Success", message:successMessage , preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                            
                            self.navigationController?.popViewController(animated: true)
                        }))
                        
                        self.present(alert, animated: true, completion: nil)
                        /* if self.typeOfInvitations.lowercased() == "share"{
                         let alert = UIAlertController(title: "Success", message: "Event shared successfully", preferredStyle: .alert)
                         alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                         
                         self.onClickBackAction(self)
                         }))
                         
                         self.present(alert, animated: true, completion: nil)
                         }
                         else{
                         let alert = UIAlertController(title: "Success", message: "You have successfully sent invitations", preferredStyle: .alert)
                         alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                         
                         self.onClickBackAction(self)
                         }))
                         
                         self.present(alert, animated: true, completion: nil)
                         }*/
                        
                        
                    }else
                    {
                        ActivityIndicatorView.hiding()
                    }
                    
                    
                } catch let err {
                    
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
    //MARK:- Button Actions
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickSearchReset(_ sender: Any) {
        txtSearch.text = ""
        searchTxt = ""
        self.btnSearchReset.isHidden = true
        if self.selectedIndex == 1{
            callGroupMembers()
        }
        else if self.selectedIndex == 0{
            getFamilyList()
        }
        else{
        }
        self.txtSearch.endEditing(true)
    }
    
    @IBAction func onClickShare(_ sender: Any) {
        if selectedIndex == 0{
            self.selectedUserIDArr.addObjects(from: selectedGroups)
            if selectedUserIDArr.count > 0{
                
                let params: Parameters = ["post_id" : postId, "to_group_id" : selectedUserIDArr as NSArray,  "shared_user_id" : UserDefaults.standard.value(forKey: "userId") as! String]
                callinvite(param: params)
                
            }
            else
            {
                Helpers.showAlertDialog(message: "Please select anyone from the list", target: self)
                
            }
        }
        else if selectedIndex == 1{
            
            if selectedUserIDArr.count > 0{
                
                let params: Parameters = ["post_id" : postId, "to_user_id" : selectedUserIDArr as NSArray, "shared_user_id" : UserDefaults.standard.value(forKey: "userId") as! String]
                print(params)
                callinvite(param: params)
            }
            else
            {
                Helpers.showAlertDialog(message: "Please select anyone from the list", target: self)
                
            }
        }
    }
    
    @IBAction func onClickTabSelection(_ sender: UIButton) {
        if sender.tag == 101{
            self.selectedIndex = 1
            selectedUserIDArr.removeAllObjects()
            
            btnFamily.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            btnPeople.setTitleColor(UIColor(named: "greenBackgrounf"), for: .normal)
            
            imgFamily.backgroundColor  = UIColor(named: "lightGrayTextColor")
            imgPeople.backgroundColor  = UIColor(named: "greenBackgrounf")
            self.callGroupMembers()
            
        }
        else{
            self.selectedIndex = 0
            selectedUserIDArr.removeAllObjects()
            // getFamilyList()
            
            btnFamily.setTitleColor(UIColor(named: "greenBackgrounf"), for: .normal)
            btnPeople.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            
            imgFamily.backgroundColor  = UIColor(named: "greenBackgrounf")
            imgPeople.backgroundColor  = UIColor(named: "lightGrayTextColor")
            getFamilyList()
        }
        tblListView.reloadData()
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension PostShareViewController : UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if selectedIndex == 0{
            return FamilyArr.count
        }
        else  {
            return peopleListArr.count
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedIndex == 0{
            return 0
        }else if selectedIndex == 2{
            
            return 1
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        // if selectedIndex == 0{
        let header = self.tblListView.dequeueReusableHeaderFooterView(withIdentifier: "FamilyListingView") as! FamilyListingView
        
        header.btnAction.tag       = section
        header.btnView.tag         = section
        header.btnView.addTarget(self, action: #selector(inviteFamilySelection(sender:)), for: .touchUpInside)
        header.btnAction.addTarget(self, action: #selector(inviteFamilySelection(sender:)), for: .touchUpInside)
        
        
        if selectedIndex == 1{
            
            header.lblTitle.text = self.peopleListArr.arrayValue[section]["full_name"].stringValue.firstUppercased
            header.byTitle.text = ""
            header.lblType.text = ""//FamilyArr.familyList![section].faType
            header.lblType.isHidden = true
            header.lblCreatedBy.isHidden = true
            header.lblKnownHead.isHidden = true
            header.lblMemberHead.isHidden = true
            //                header.lblCreated_height.constant = 0
            header.lblRegion.text = self.peopleListArr.arrayValue[section]["location"].stringValue.firstUppercased
            header.lblCreatedBy.text = ""//FamilyArr.familyList![section].createdByName
            header.lblMembersCount.text = ""//FamilyArr.familyList![section].memberCount
            header.lblKnown.text = ""//FamilyArr.familyList![section].knownCount
            
            if selectedUserIDArr.contains(self.peopleListArr.arrayValue[section]["user_id"].intValue){
                header.btnAction.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
                header.btnAction.setTitle("Selected", for: .normal)
                
            }else{
                
                header.btnAction.titleLabel?.font = UIFont.systemFont(ofSize: 15)
                header.btnAction.setTitle("Select", for: .normal)
                
            }
            
            let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+self.peopleListArr.arrayValue[section]["propic"].stringValue
            let imgurl = URL(string: temp)
            header.imgLogo.kf.indicatorType = .activity
            
            header.imgLogo.kf.setImage(with: imgurl, placeholder: UIImage(named: "imgNoImage") , options: nil, progressBlock: nil, completionHandler: nil)
            
            
        }
        else if selectedIndex == 2{
        }
        else{
            header.lblType.isHidden = false
            header.lblCreatedBy.isHidden = false
            //                header.lblKnownHead.isHidden = false
            //                header.lblMemberHead.isHidden = false
            header.lblMembersCount.isHidden = true
            header.lblMemberHead.isHidden = true
            header.lblKnownHead.isHidden = true
            header.lblKnown.isHidden = true
            //                header.lblCreated_height.constant = 18
            
            header.lblTitle.text = self.FamilyArr.arrayValue[section]["group_name"].stringValue.firstUppercased
            header.lblType.text = self.FamilyArr.arrayValue[section]["group_type"].stringValue
            header.lblRegion.text = self.FamilyArr.arrayValue[section]["base_region"].stringValue
            header.lblCreatedBy.text = self.FamilyArr.arrayValue[section]["created_by"].stringValue ?? ""
            if selectedUserIDArr.contains(self.FamilyArr.arrayValue[section]["id"].intValue){
                header.btnAction.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
                header.btnAction.setTitle("Selected", for: .normal)
                
            }else{
                
                header.btnAction.titleLabel?.font = UIFont.systemFont(ofSize: 15)
                header.btnAction.setTitle("Select", for: .normal)
                
            }
            let foundItems = selectedGroups.filter { $0["id"]!.intValue == self.FamilyArr.arrayValue[section]["id"].intValue }
            if foundItems.count > 0{
                header.btnAction.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
                header.btnAction.setTitle("Selected", for: .normal)
            }else{
                
                header.btnAction.titleLabel?.font = UIFont.systemFont(ofSize: 15)
                header.btnAction.setTitle("Select", for: .normal)
            }
            
            
            if FamilyArr.arrayValue[section]["logo"].stringValue.count != 0{
                let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=150&height=150&url="+"\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+FamilyArr.arrayValue[section]["logo"].stringValue
                let imgurl = URL(string: temp)
                print(imgurl)
                header.imgLogo.kf.indicatorType = .activity
                
                header.imgLogo.kf.setImage(with: imgurl, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
            }
            else{
                header.imgLogo.image = UIImage(named: "Family Logo")
            }
            // header.byTitle.text = "By \(FamilyArr.familyList![section].faName)"
            
            
            /*header.lblTitle.text = FamilyArr.familyList![section].faName
             header.lblType.text = FamilyArr.familyList![section].faType
             header.lblRegion.text = FamilyArr.familyList![section].faRegion
             header.lblCreatedBy.text = FamilyArr.familyList![section].faAdmin
             
             let memberCount = FamilyArr.familyList![section].memberCount
             let knownCount = FamilyArr.familyList![section].knownCount
             
             if Int(memberCount!)! > 0{
             header.lblMembersCount.text = FamilyArr.familyList![section].memberCount
             header.lblMemberHead.isHidden = false
             header.lblMembersCount.isHidden = false
             }
             else{
             header.lblMemberHead.isHidden = true
             header.lblMembersCount.isHidden = true
             }
             
             if Int(knownCount!)! > 0{
             header.lblKnown.text = FamilyArr.familyList![section].memberCount
             header.lblKnownHead.isHidden = false
             header.lblKnown.isHidden = false
             }
             else{
             header.lblKnownHead.isHidden = true
             header.lblKnown.isHidden = true
             }
             
             header.lblKnown.text = FamilyArr.familyList![section].knownCount
             
             if selectedUserIDArr.contains(FamilyArr.familyList![section].faId){
             header.btnAction.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
             header.btnAction.setTitle("Selected", for: .normal)
             
             //                header.btnAction.backgroundColor          = .white
             //                header.btnAction.layer.borderWidth        = 1
             //                header.btnAction.borderColor              = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
             //                header.btnAction.titleLabel?.textColor    = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
             
             }else{
             
             header.btnAction.titleLabel?.font = UIFont.systemFont(ofSize: 15)
             header.btnAction.setTitle("Select", for: .normal)
             
             //                header.btnAction.backgroundColor          = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
             //                header.btnAction.titleLabel?.textColor    = .white
             }
             
             if FamilyArr.familyList![section].faLogo.count != 0{
             let temp = "\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+FamilyArr.familyList![section].faLogo
             let imgurl = URL(string: temp)
             print(imgurl)
             header.imgLogo.kf.setImage(with: imgurl, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
             }
             else{
             header.imgLogo.image = UIImage(named: "Family Logo")
             }*/
        }
        
        
        return header
        // }
        //        else{
        //
        //        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if  selectedIndex == 0{
            return 150
        }
        else if selectedIndex == 1{
            return 110
        }
        else{
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
}

extension PostShareViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        textField.resignFirstResponder()
        /* if self.selectedIndex == 1{
         if textField == txtSearch
         {
         callGroupMembers()
         }
         }
         else if self.selectedIndex == 0{
         if textField == txtSearch
         {
         getFamilyList()
         }
         
         }
         else{
         }*/
        if textField.text!.count > 0{
            self.btnSearchReset.isHidden = false
        }
        else{
            self.btnSearchReset.isHidden = true
        }
        textField.endEditing(true)
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        self.view.endEditing(true)
        if textField.text!.count > 0{
            self.btnSearchReset.isHidden = false
        }
        else{
            self.btnSearchReset.isHidden = true
        }
        
        
        if self.selectedIndex == 1{
            if textField == txtSearch
            {
                callGroupMembers()
            }
        }
        else if self.selectedIndex == 0{
            if textField == txtSearch
            {
                getFamilyList()
            }
            
        }
        else{
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text!.count > 0{
            self.btnSearchReset.isHidden = false
        }
        else{
            self.btnSearchReset.isHidden = true
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
        }
        return true
    }
}
