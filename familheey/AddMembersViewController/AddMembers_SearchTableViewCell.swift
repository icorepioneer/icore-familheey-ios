//
//  AddMembers_SearchTableViewCell.swift
//  familheey
//
//  Created by Giri on 23/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class AddMembers_SearchTableViewCell: UITableViewCell {
    @IBOutlet weak var imageViewAvatar: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var imageViewGender: UIImageView!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblNumberOfFamiles: UILabel!
    @IBOutlet weak var lblNumberOfMutualFriends: UILabel!
    @IBOutlet weak var btnAddMember: UIButton!
    @IBOutlet weak var lblAddMember: UILabel!
    @IBOutlet weak var viewRightButton: UIView!
    @IBOutlet weak var lblDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
