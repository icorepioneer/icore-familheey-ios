//
//  InviteFriendsViewController.swift
//  familheey
//
//  Created by familheey on 28/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class InviteFriendsViewController: UIViewController, popUpDelegate {
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtCode: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    
    var groupId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Helpers.setleftView(textfield: txtName, customWidth: 15)
        Helpers.setleftView(textfield: txtName, customWidth: 15)
        Helpers.setleftView(textfield: txtEmail, customWidth: 15)

        // Do any additional setup after loading the view.
    }
    

    
    //MARK:- Button Actions
    @IBAction func onClickCode(_ sender: Any) {
        self.view.endEditing(true)
        
        let popUp = popupTableViewController()
        self.addChild(popUp)
        // popUp.frmGndrFlag = fromGndr
        popUp.view.frame = UIScreen.main.bounds
        self.view.addSubview(popUp.view)
        popUp.delegate = self
        self.view.bringSubviewToFront(popUp.view)
    }
    
    @IBAction func onClickInvite(_ sender: Any) {
        var phone = ""
        if txtCode.text?.isEmpty != true && txtPhone.text?.isEmpty != true{
            phone = txtPhone.text!+txtCode.text!
        }
        else{
            self.displayAlert(alertStr: "Please enter phone number with country code", title: "")
        }
        
        if txtEmail.text?.isEmpty != true && phone.count > 0{
            APIServiceManager.callServer.inviteMailorPhone(url: EndPoint.inviteViaMsg, email: txtEmail.text!, phone: phone, userId: UserDefaults.standard.value(forKey: "userId") as! String, groupId:groupId, urlEncode: "", name: self.txtName.text!, fromName: UserDefaults.standard.value(forKey: "user_fullname") as! String, success: { (responseMdl) in
                
                guard let result = responseMdl as? requestSuccessModel else{
                    return
                }
                ActivityIndicatorView.hiding()
                if result.status_code == 200{
                    //self.dismiss(animated: true, completion: nil)
//                    self.displayAlert(alertStr: "Invitation send", title: "Success")
                    self.txtName.text = ""
                     self.txtCode.text = ""
                     self.txtPhone.text = ""
                     self.txtEmail.text = ""
                    self.displayAlertChoice(alertStr: "Invitation send", title: "Success", completion: { (result) in
                        self.onClickBack(self)
                    })
                }
            }) { (error) in
                ActivityIndicatorView.hiding()
            }

        }
    }
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    //MARK:- Protocol PopupDelegate
    func selectCountry(id: String, name: String) {
        
        txtPhone.text = id
//        onCodeButt.setTitle(name, for: .normal)
//
//        txtFieldUnderline.isHidden = false
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            
            self.txtCode.becomeFirstResponder()
        })
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
