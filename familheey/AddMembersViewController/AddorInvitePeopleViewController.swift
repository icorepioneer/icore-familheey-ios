//
//  AddorInvitePeopleViewController.swift
//  familheey
//
//  Created by familheey on 08/11/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Moya
import Alamofire
import SwiftyJSON

class MemberInviteCell: UITableViewCell{
    @IBOutlet weak var imgPropic: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblLoc: UILabel!
    @IBOutlet weak var lblFamilyHead: UILabel!
    @IBOutlet weak var lblFamilyCount: UILabel!
    @IBOutlet weak var lblmutalCount: UILabel!
    @IBOutlet weak var lblMutalHead: UILabel!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var lblAddHead: UILabel!
    
    @IBOutlet weak var lblType_hieght: NSLayoutConstraint!
    @IBOutlet weak var lblDesc_height: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
    }
}


class AddorInvitePeopleViewController: UIViewController, popUpDelegate {
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var btomView: UIView!
    @IBOutlet weak var btmView_height: NSLayoutConstraint!
    
    @IBOutlet weak var imgPeople: UIImageView!
    @IBOutlet weak var btnPeople: UIButton!
    @IBOutlet weak var peopleSelVw: UIView!
    @IBOutlet weak var imgOther: UIImageView!
    @IBOutlet weak var btnOther: UIButton!
    @IBOutlet weak var otherSelVw: UIView!
    @IBOutlet weak var btnSearchReset: UIButton!
    
     var selectedIndex = 0
    var groupId = ""
    var resultArr : [listMemberResult]?
    var usersList = [SearchResultUser]()
    
    var isFromGlobalSearch = false
    
    var countryCode = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtSearch.delegate = self
        //self.tblList.reloadData()
        Helpers.setleftView(textfield: txtSearch, customWidth: 40)
        let button = UIButton()
        button.tag = 101
        self.onClickTabAction(button)
    }
    

    
    //MARK:- ButtonAction
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickTabAction(_ sender: UIButton) {
         if sender.tag == 102{
            selectedIndex = 1
            btnOther.setTitleColor(UIColor(named: "greenBackgrounf"), for: .normal)
            otherSelVw.isHidden = false
            peopleSelVw.isHidden = true
            btnPeople.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            
            imgOther.backgroundColor  = UIColor(named: "greenBackgrounf")
            imgPeople.backgroundColor  = UIColor(named: "lightGrayTextColor")
            
            btomView.isHidden = false
            btmView_height.constant = 89
            
            self.tblList.delegate = self
            self.tblList.dataSource = self
            
            self.tblList.reloadData()
            self.tblList.scrollToTop()

        }
        else{
            selectedIndex = 0
            btnOther.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            otherSelVw.isHidden = true
            peopleSelVw.isHidden = false
            btnPeople.setTitleColor(UIColor(named: "greenBackgrounf"), for: .normal)
            
            imgOther.backgroundColor  = UIColor(named: "lightGrayTextColor")
            imgPeople.backgroundColor  = UIColor(named: "greenBackgrounf")
            
            btomView.isHidden = true
            btmView_height.constant = 0
            getAllMembers(groupId: self.groupId)
        }
    }
    @IBAction func onClickSearchReset(_ sender: Any) {
        txtSearch.text = ""
        self.btnSearchReset.isHidden = true
        self.getAllMembers(groupId: self.groupId)
        self.txtSearch.endEditing(true)
    }
    @IBAction func inviteAction(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "Contacts", bundle: nil)
        let addMember = storyboard.instantiateViewController(withIdentifier: "ContactListingViewController") as! ContactListingViewController
        addMember.groupID = self.groupId
        self.navigationController?.pushViewController(addMember, animated: true)
        
    }
    
    //MARK:- Custom functions
    func callSearchApi(){
        isFromGlobalSearch = true
        let param = [
        "userid" : UserDefaults.standard.value(forKey: "userId") as! String,
        "searchtxt" : txtSearch.text!,
        "offset":"0",
        "limit":"100",
        "phonenumbers":[],
        "type":"users",
        "group_id":self.groupId
        ] as [String : Any]
        
        ActivityIndicatorView.show("Please wait!")
        APIServiceManager.callServer.searchRequest(url: EndPoint.search, param: param, fromSearchTabViewController: false, success: { (response) in
            
            ActivityIndicatorView.hiding()
            self.usersList = [SearchResultUser]()
            
            
            if let results = response as! SearchList?{
                self.usersList = results.searchResult!
                
            }
            
            if self.usersList.isEmpty {
                
                self.tblList.isHidden = true
            } else {
                self.tblList.isHidden = false
                
                self.tblList.reloadData()
            }
            
            
        }) { (error) in
            ActivityIndicatorView.hiding()
            self.usersList = [SearchResultUser]()
            //self.FamilyList = [SearchResultGroup]()
            
            if self.usersList.isEmpty {
                
                self.tblList.isHidden = true
            } else {
                
                self.tblList.reloadData()
            }
        }
    }
    
    //MARK:- WEB Services
    func getAllMembers(groupId:String){
        isFromGlobalSearch = false
        APIServiceManager.callServer.getAllMembersList(url: EndPoint.listMembers, groupId: groupId, userId: UserDefaults.standard.value(forKey: "userId") as! String, query: "", success: { (responseMdl) in
            
            guard let result = responseMdl as? listAllResult else{
                return
            }
            ActivityIndicatorView.hiding()
            if result.statuscode == 200{
                self.resultArr = result.result
                
                if self.resultArr!.isEmpty {
                    
                    self.tblList.isHidden = true
                } else {
                    
                    self.tblList.isHidden = false
                    self.tblList.delegate = self
                    self.tblList.dataSource = self
                    self.tblList.reloadData()
                }
            }
            
        }) { (error) in
            
        }
    }
    
    @IBAction func onClickAddToFamily(_ sender: UIButton) {
        var toId = Int()
        if isFromGlobalSearch{
            toId = usersList[sender.tag].userid!
        }
        else{
            toId = (self.resultArr?[sender.tag].id)!
        }
        
        let tempId = "\(toId)"
        print(tempId)
        
        APIServiceManager.callServer.sendMemberInvitation(url: EndPoint.inviteMember, fromId: UserDefaults.standard.value(forKey: "userId") as! String, toId:tempId  , groupId: groupId, success: { (responseMdl) in
            
            guard let result = responseMdl as? inviteMemberResult else{
                return
            }
            ActivityIndicatorView.hiding()
            if result.statuscode == 200{
                if self.isFromGlobalSearch{
                    var fam = self.usersList[sender.tag]
                    if result.statusStr.lowercased() == "pending"{
                        fam.type = "pending"
                        fam.status = 1
                        fam.exist = 0
                    }
                    else{
                        fam.type = "member"
                        fam.status = 0
                        fam.exist = 1
                    }
                    self.usersList[sender.tag] = fam
                    self.tblList.beginUpdates()
                    self.tblList.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .none)
                    self.tblList.endUpdates()
                }
                else{
                    var fam = self.resultArr?[sender.tag]
                    if result.statusStr.lowercased() == "pending"{
                        fam?.exist = 0
                        fam?.status = 1
                    }
                    else{
                        fam?.exist = 1
                        fam?.status = 0
                    }
                    self.resultArr![sender.tag] = fam!
                    self.tblList.beginUpdates()
                    self.tblList.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .none)
                    self.tblList.endUpdates()
                }
                
                // self.displayAlert(alertStr: "Request Send", title: "")
            }
            
        }) { (error) in
            
        }
    }
    
    @IBAction func onClickInvite(_ sender: Any) {
         let cell: OtherInviteCell = tblList.cellForRow(at: IndexPath(row: 0, section: 0)) as! OtherInviteCell
        var phone = ""
        if cell.nameField.text == ""{
            
            Helpers.showAlertDialog(message: "Please enter contact's name", target: self)
            
        }else if cell.phoneCodeField.text == ""{
            
            Helpers.showAlertDialog(message: "Please enter contact's country code", target: self)
            
        }else if cell.phoneNumberField.text == ""{
            
            Helpers.showAlertDialog(message: "Please enter contact's phone number", target: self)
            
        }
//        else if !Helpers.validatePHnumber(enteredNumber: cell.phoneNumberField.text!){
//            
//            Helpers.showAlertDialog(message: "Please verify phone number", target: self)
//        }
        else if cell.emailField.text == ""{
            
            Helpers.showAlertDialog(message: "Please enter contact's email", target: self)
            
        }else if !Helpers.validateEmail(enteredEmail: cell.emailField.text!.trimmingCharacters(in: .whitespacesAndNewlines)){
            
            Helpers.showAlertDialog(message: "Please verify email address", target: self)
        }
        else{
            let tempStr = cell.emailField.text!.prefix(1)
            if !String(tempStr).isAlphanumeric{
                self.displayAlert(alertStr: "Please start email with a alphanumeric characters", title: "")
                return
            }
            
            phone = cell.phoneCodeField.text!+cell.phoneNumberField.text!
            APIServiceManager.callServer.inviteMailorPhone(url: EndPoint.inviteViaMsg, email: cell.emailField.text!, phone: phone, userId: UserDefaults.standard.value(forKey: "userId") as! String, groupId:groupId, urlEncode: "", name: cell.nameField.text!, fromName: UserDefaults.standard.value(forKey: "user_fullname") as! String, success: { (responseMdl) in
                
                guard let result = responseMdl as? requestSuccessModel else{
                    return
                }
                ActivityIndicatorView.hiding()
                if result.status_code == 200{
                    //self.dismiss(animated: true, completion: nil)
                    //                    self.displayAlert(alertStr: "Invitation send", title: "Success")
//                    self.txtName.text = ""
//                    self.txtCode.text = ""
//                    self.txtPhone.text = ""
//                    self.txtEmail.text = ""
                    self.displayAlertChoice(alertStr: "Invitation send", title: "Success", completion: { (result) in
                        self.navigationController?.popViewController(animated: true)
                    })
                }
            }) { (error) in
                ActivityIndicatorView.hiding()
            }
        }
    }
    
    @IBAction func onClickCodeSelection(_ sender: Any) {
        self.view.endEditing(true)
        
        let popUp = popupTableViewController()
        self.addChild(popUp)
        // popUp.frmGndrFlag = fromGndr
        popUp.view.frame = UIScreen.main.bounds
        self.view.addSubview(popUp.view)
        popUp.delegate = self
        self.view.bringSubviewToFront(popUp.view)
    }
    
    
    //MARK:- Protocol PopupDelegate
    func selectCountry(id: String, name: String) {
        
       // txtPhone.text = id
        //        onCodeButt.setTitle(name, for: .normal)
        //
        //        txtFieldUnderline.isHidden = false
        
        self.countryCode = id
        tblList.reloadData()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
           // self.txtCode.becomeFirstResponder()
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddorInvitePeopleViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedIndex == 0{
            if isFromGlobalSearch{
                return usersList.count
            }
            else{
                return resultArr!.count
            }
           
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if selectedIndex == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MemberInviteCell", for: indexPath) as! MemberInviteCell
            
            cell.btnAdd.tag = indexPath.row
            
            if self.isFromGlobalSearch{
                cell.lblName.text = usersList[indexPath.row].full_name
                cell.lblLoc.text = usersList[indexPath.row].origin
                
                if usersList[indexPath.row].propic?.count != 0{
                    let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+usersList[indexPath.row].propic!
                    let imgUrl = URL(string: temp)
                    cell.imgPropic.kf.indicatorType = .activity
                    let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+imgUrl!.relativeString
                    let url = URL(string: newUrlStr)
                    
                    cell.imgPropic.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
                }
                else{
                    cell.imgPropic.image = #imageLiteral(resourceName: "Male Colored")
                }
                
                let tempCount = usersList[indexPath.row].type
                let inviteStatus = usersList[indexPath.row].status
                let exist = usersList[indexPath.row].exist
                if exist == 0{
                    if inviteStatus == 1{
                        cell.lblAddHead.text = "Pending"
                        cell.btnAdd.isUserInteractionEnabled = false
                    }
                    else{
                        cell.lblAddHead.text = "Add to Family"
                        cell.btnAdd.isUserInteractionEnabled = true
                    }
                }
                else{
                    if tempCount.lowercased() == "member"{
                        cell.lblAddHead.text = "MEMBER"
                        cell.btnAdd.isUserInteractionEnabled = false
                    }
                    else if tempCount.lowercased() == "pending"{
                        cell.lblAddHead.text = "Pending"
                        cell.btnAdd.isUserInteractionEnabled = false
                        
                    }
                    else if inviteStatus == 1{
                        cell.lblAddHead.text = "Pending"
                        cell.btnAdd.isUserInteractionEnabled = false
                    }
                    else{
                        cell.lblAddHead.text = "Add to Family"
                        cell.btnAdd.isUserInteractionEnabled = true
                    }
                }
                
     
                
                /*  if resultArr?[indexPath.row].exist == 1{
                    cell.lblAddHead.text = "MEMBER"
                    cell.btnAdd.isUserInteractionEnabled = false
                }
                else{
                    if resultArr?[indexPath.row].status == 1 {
                        cell.lblAddHead.text = "PENDING"
                        cell.btnAdd.isUserInteractionEnabled = false
                    }
                    else{
                        cell.lblAddHead.text = "ADD TO FAMILY"
                        cell.btnAdd.isUserInteractionEnabled = true
                    }
                }*/
                
            }
            else{
                cell.lblName.text = resultArr?[indexPath.row].fullname
                cell.lblLoc.text = resultArr?[indexPath.row].location
                
                if resultArr?[indexPath.row].photo.count != 0{
                    let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+(resultArr?[indexPath.row].photo)!
                    let imgUrl = URL(string: temp)
                    cell.imgPropic.kf.indicatorType = .activity
                    let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+imgUrl!.relativeString
                    let url = URL(string: newUrlStr)
                    
                    cell.imgPropic.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
                }
                else{
                    cell.imgPropic.image = #imageLiteral(resourceName: "Male Colored")
                }
                
                if resultArr?[indexPath.row].exist == 1{
                    cell.lblAddHead.text = "MEMBER"
                    cell.btnAdd.isUserInteractionEnabled = false
                }
                else{
                    if resultArr?[indexPath.row].status == 1 {
                        cell.lblAddHead.text = "PENDING"
                        cell.btnAdd.isUserInteractionEnabled = false
                    }
                    else{
                        cell.lblAddHead.text = "Add to Family"
                        cell.btnAdd.isUserInteractionEnabled = true
                    }
                }
                
            }
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell3") as! OtherInviteCell
            cell.phoneCodeField.text = countryCode
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectedIndex == 0{
            return 120
        }
        
        return 570
    }
}
extension AddorInvitePeopleViewController:UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        if textField == txtSearch
        {
            // selectWebAPI()
            if textField.text!.count > 0{
                btnSearchReset.isHidden = false
            }
            else{
                btnSearchReset.isHidden = true
            }
            textField.endEditing(true)
        }
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        self.view.endEditing(true)
        if textField == txtSearch
        {
            if textField.text!.count > 0{
                btnSearchReset.isHidden = false
                callSearchApi()
            }
            else{
                btnSearchReset.isHidden = true
                getAllMembers(groupId: self.groupId)
            }
            
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
        }
        return  true
    }
}

