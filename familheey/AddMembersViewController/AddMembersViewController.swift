//
//  AddMembersViewController.swift
//  familheey
//
//  Created by familheey on 20/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
protocol FolderCreateDelagate:class {
    func folderCreated()
}

class AddMembersViewController: UIViewController {

    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var FrntBgView: UIView!
    @IBOutlet weak var inviteView: UIView!
    @IBOutlet weak var sharablrView: UIView!
    @IBOutlet weak var folderView: UIView!
    @IBOutlet weak var txtFolderName: UITextField!
    @IBOutlet weak var imgPublic_type: UIImageView!
    @IBOutlet weak var imgPrivate_type: UIImageView!
    @IBOutlet weak var imgShare: UIImageView!
    
    weak var delegate : FolderCreateDelagate?

    var groupId = ""
    var isPublic = "public"
    var isSharable = "true"
    
    var isFromInvite = "false"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if isFromInvite.lowercased() == "true"{
            inviteView.isHidden = false
            folderView.isHidden = true
        }
        else{
            inviteView.isHidden = true
            folderView.isHidden = false
        }
    }
    

    
    //MARK:- Button Actions
    @IBAction func onClickDoneActions(_ sender: Any) {
        
        if txtEmail.text?.isEmpty != true || txtPhone.text?.isEmpty != true{
            APIServiceManager.callServer.inviteMailorPhone(url: EndPoint.inviteViaMsg, email: txtEmail.text!, phone: txtPhone.text!, userId: UserDefaults.standard.value(forKey: "userId") as! String, groupId: groupId, urlEncode: "innovationinc.familheey://", name: "", fromName: "", success: { (responseMdl) in
                
                guard let result = responseMdl as? requestSuccessModel else{
                    return
                }
                ActivityIndicatorView.hiding()
                if result.status_code == 200{
                    self.dismiss(animated: true, completion: nil)
                }
            }) { (error) in
                ActivityIndicatorView.hiding()
            }
        }
        else{
            self.displayAlert(alertStr: "Enter atleast one ", title: "")
        }
    }
    @IBAction func onClickCancelAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickFolderType(_ sender: UIButton) {
        if sender.tag == 1{
            imgPublic_type.image = UIImage(named: "imgTick_gray")
            imgPrivate_type.image = UIImage(named: "")
            isPublic = "public"
            sharablrView.isHidden = true
            isSharable = "true"
        }
        else{
            imgPublic_type.image = UIImage(named: "")
            imgPrivate_type.image = UIImage(named: "imgTick_gray")
            isPublic = "private"
            sharablrView.isHidden = false
//            imgShare.image = UIImage(named: "imgTick_gray")
//            isSharable = "true"
        }
    }
    @IBAction func onClickShare(_ sender: Any) {
        if imgShare.image == nil{
            imgShare.image = UIImage(named: "imgTick_gray")
            isSharable = "true"
        }
        else{
            imgShare.image = UIImage(named: "")
            isSharable = "false"
        }
    }
    @IBAction func onClickFolderSaveAction(_ sender: Any) {
        if txtFolderName.text?.isEmpty != true{
            APIServiceManager.callServer.createNewFolder(url: EndPoint.createFolder, folderName: txtFolderName.text!, userId: UserDefaults.standard.value(forKey: "userId") as! String, groupId: groupId, type: isPublic, isSearchable: isSharable, success: { (responseMdl) in
                guard let result = responseMdl as? FolderListResult else{
                    return
                }
                ActivityIndicatorView.hiding()
                
                if result.status_code == 200{
                    self.delegate?.folderCreated()
                    self.dismiss(animated: true, completion: nil)
                }
                
            }) { (error) in
                
            }
        }
        else{
            self.displayAlert(alertStr: "Enter a folder name", title: "")
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
