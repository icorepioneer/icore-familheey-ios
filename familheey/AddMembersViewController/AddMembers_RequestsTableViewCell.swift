//
//  AddMembers-RequestsTableViewCell.swift
//  
//
//  Created by Giri on 20/09/19.
//

import UIKit

class AddMembers_RequestsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageViewAvatar: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var imageViewGender: UIImageView!
    @IBOutlet weak var lblNumberOfMutualConnections: UILabel!
    @IBOutlet weak var lblNumberOfMutualFamilies: UILabel!
    @IBOutlet weak var viewAcceptRequest: UIView!
    @IBOutlet weak var viewRejectRequest: UIView!
    @IBOutlet weak var imageViewAccept: UIImageView!
    @IBOutlet weak var imageViewReject: UIImageView!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnReject: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
