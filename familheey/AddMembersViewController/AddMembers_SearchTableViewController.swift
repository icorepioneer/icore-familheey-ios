//
//  AddMembers_SearchTableViewController.swift
//  familheey
//
//  Created by Giri on 23/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class AddMembers_SearchTableViewController: UITableViewController {
    @IBOutlet weak var txtSearchMember: UITextField!
    @IBOutlet var tbtList: UITableView!
    
    var groupId = ""
    var resultArr : [listMemberResult]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        getMembersListFromDb(groupId: groupId)
    }
    
    
    func getMembersListFromDb(groupId:String){
        APIServiceManager.callServer.getAllMembersList(url: EndPoint.listMembers, groupId: groupId, userId: UserDefaults.standard.value(forKey: "userId") as! String, query: "", success: { (responseMdl) in
            
            guard let result = responseMdl as? listAllResult else{
                return
            }
            ActivityIndicatorView.hiding()
            if result.statuscode == 200{
                self.resultArr = result.result
                
                self.tbtList.reloadData()
            }
            
        }) { (error) in
            
        }
    }
    
    //MARK:- Button Actions
    
    @IBAction func OnClickAddToFamilyAction(_ sender: UIButton) {
        print(sender.tag)
        let toId = self.resultArr?[sender.tag].id
        
        let tempId = "\(toId!)"
        
        APIServiceManager.callServer.sendMemberInvitation(url: EndPoint.inviteMember, fromId: UserDefaults.standard.value(forKey: "userId") as! String, toId:tempId  , groupId: groupId, success: { (responseMdl) in
            
            guard let result = responseMdl as? inviteMemberResult else{
                return
            }
            ActivityIndicatorView.hiding()
            if result.statuscode == 200{
                var fam = self.resultArr?[sender.tag]
                fam?.exist = 0
                fam?.status = 1
                self.resultArr![sender.tag] = fam!
                self.tbtList.beginUpdates()
                self.tbtList.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .none)
                self.tbtList.endUpdates()
               // self.displayAlert(alertStr: "Request Send", title: "")
            }
            
        }) { (error) in
            
        }
    }
    
    @IBAction func onClickInviteNew(_ sender: Any) {
//        showAlertController { (result) in
//                    self.requestEmailorPhone(result: result)
//        }
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let popOverVc = storyboard.instantiateViewController(withIdentifier: "addMember") as! AddMembersViewController
        popOverVc.groupId = groupId
        popOverVc.isFromInvite = "true"
        popOverVc.modalTransitionStyle = .crossDissolve
        popOverVc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(popOverVc, animated: true)
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:Request via email or phone
     /*   func  requestEmailorPhone(result:String){
            let tempArr = result.components(separatedBy: " ")
            print(tempArr)
            let email = tempArr.first!
            let phone = tempArr.last!
            
            print("\(email)  \(phone)")
            
        }*/
    
    //MARK:- Tableview Delegates
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = resultArr?.count else {
            return 0
        }
        return count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddMembers_SearchTableViewCell", for: indexPath) as! AddMembers_SearchTableViewCell
        cell.btnAddMember.tag = indexPath.row
        
        cell.lblName.text = resultArr?[indexPath.row].fullname
        cell.lblGender.text = resultArr?[indexPath.row].gender
        cell.lblLocation.text = resultArr?[indexPath.row].location
        
        if resultArr?[indexPath.row].photo.count != 0{
            let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+(resultArr?[indexPath.row].photo)!
            let imgUrl = URL(string: temp)
            cell.imageViewAvatar.kf.indicatorType = .activity
            
            cell.imageViewAvatar.kf.setImage(with: imgUrl, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
        }
    
        if resultArr?[indexPath.row].exist == 1{
            cell.lblAddMember.text = "JOINED"
            cell.btnAddMember.isUserInteractionEnabled = false
        }
        else{
            if resultArr?[indexPath.row].status == 1 {
                cell.lblAddMember.text = "PENDING"
                cell.btnAddMember.isUserInteractionEnabled = false
            }
            else{
                cell.lblAddMember.text = "Add to Family"
                cell.btnAddMember.isUserInteractionEnabled = true
            }
        }
        
        return cell

    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 110
    }
    
}
