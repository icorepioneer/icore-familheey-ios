//
//  AddMembers-MembersTableViewCell.swift
//  familheey
//
//  Created by Giri on 20/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class AddMembers_MembersTableViewCell: UITableViewCell {
    @IBOutlet weak var imageViewAvatar: UIImageView!
    @IBOutlet weak var viewAdmin: UIView!
    @IBOutlet weak var lblAdmin: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDesignation: UILabel!
    @IBOutlet weak var imageViewDesignation: UIImageView!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var imageViewGender: UIImageView!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblNumberOfContributions: UILabel!
    @IBOutlet weak var lblMemberSince: UILabel!
    @IBOutlet weak var viewRightButton: UIView!
    @IBOutlet weak var imageViewRightButton: UIImageView!
    @IBOutlet weak var btnRight: UIButton!
    @IBOutlet weak var btnAddDesignation: UIButton!
    @IBOutlet weak var vewRelation: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
