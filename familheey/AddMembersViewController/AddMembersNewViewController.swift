//
//  AddMembersNewViewController.swift
//  familheey
//
//  Created by Giri on 06/02/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit

class AddMembersNewViewController: UIViewController, SelectRelationDelagate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate,UITableViewDelegate,UITableViewDataSource  {
    
    @IBOutlet weak var viewActiveMembers: UIView!
    @IBOutlet weak var viewActiveRequest: UIView!
    @IBOutlet weak var viewActiveContributions: UIView!
    @IBOutlet weak var lblContributions: UILabel!
    @IBOutlet weak var lblRequests: UILabel!
    @IBOutlet weak var lblMembers: UILabel!
    @IBOutlet weak var viewRequestsToHide: UIView!
    
    @IBOutlet weak var viewSearchToHide: UIView!
    @IBOutlet weak var lblTotalRequests: UILabel!
    @IBOutlet weak var txtSearchMembers: UITextField!
    @IBOutlet weak var imgInvite: UIImageView!
    @IBOutlet weak var imgRequts: UIImageView!
    @IBOutlet weak var imgMembers: UIImageView!
    @IBOutlet weak var lblMembersCount: UILabel!
    @IBOutlet weak var lblAdminsCount: UILabel!
    @IBOutlet weak var btnSearchReset: UIButton!
    @IBOutlet weak var lblHead: UILabel!
    
    @IBOutlet weak var View_btm: NSLayoutConstraint!
    @IBOutlet weak var memberStack: UIStackView!
    @IBOutlet weak var btnFrntView: UIView!
    var btnFloat = UIButton(type: .custom)
    @IBOutlet var viewNoMembers: UIView! //No Members Helper
    @IBOutlet weak var noMembersLbl: UILabel!
    @IBOutlet weak var imageViewNoData: UIImageView!
    @IBOutlet var tblListView: UITableView!
    @IBOutlet weak var imgMore: UIImageView!
    @IBOutlet weak var lblMore: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var aboutClctnVew: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    
    
    
    var ActiveTab = 0
    var titlesForAction = [String]()
    var faCate = ""
    var searchTxt = ""
    
    var groupId = ""
    var isPrimaryAdmin = ""
    var memberJoining = 0
    var link_type = 0
    var isAdmin = ""
    var islinkable = 0
    var famSettings = 0
    var announcementSet = 0
    var postId                          = 0
    var announcementId                  = 0
    var isMembershipActive = false
    
    var familyArr : [familyModel]?
    var memberArr = [viewMemberDetailsResult]()
    var requestArr = [groupRequest]()
    @IBOutlet weak var imgCover: UIImageView!
    var aboutArr = ["FEED","ANNOUNCEMENT","REQUEST","EVENTS","ALBUMS","ABOUT US","DOCUMENTS","LINKED FAMILIES","MEMBERS"]
    var ActiveTabs = 8
    
    var toRequest = false
    var famArr : [familyModel]?
    var memberJoiningStatus = 0
    var invitationStatus = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        aboutClctnVew.delegate = self
        aboutClctnVew.dataSource = self
        aboutClctnVew.reloadData()

        setActiveButton(index: 0)
        print(groupId)
//        getMemberListAPI(groupId: groupId)
        
        if isAdmin.lowercased() == "not-member"{
            self.memberStack.isHidden = true
            self.View_btm.constant = 30
        }
        else{
            if isAdmin.lowercased() == "admin"{
                if isMembershipActive{
                    ActiveTabs = 9
                    aboutArr = ["FEED","ANNOUNCEMENT","REQUEST","EVENTS","ALBUMS","ABOUT US","DOCUMENTS","LINKED FAMILIES","MEMBERSHIP","MEMBERS"]
                }
                else{
                    ActiveTabs = 8
                    aboutArr = ["FEED","ANNOUNCEMENT","REQUEST","EVENTS","ALBUMS","ABOUT US","DOCUMENTS","LINKED FAMILIES","MEMBERS"]
                }
            }
            else{
                ActiveTabs = 8
                aboutArr = ["FEED","ANNOUNCEMENT","REQUEST","EVENTS","ALBUMS","ABOUT US","DOCUMENTS","LINKED FAMILIES","MEMBERS"]
            }
            self.memberStack.isHidden = false
            self.View_btm.constant = 0
        }
        txtSearchMembers.delegate = self
        lblHead.text = appDel.groupNamePublic
        
        let imgUrl = URL(string: appDel.groupImageUrlPublic)
        if imgUrl != nil{
            self.imgCover.kf.indicatorType = .activity
            
            let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=400&height=300&url="+imgUrl!.relativeString
            let urlImg = URL(string: newUrlStr)
            
            self.imgCover.kf.setImage(with: urlImg, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        else{
            self.imgCover.image = #imageLiteral(resourceName: "family_default_cover.png")
        }
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
        tableView.tableFooterView = UIView.init()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        btnFloat.removeFromSuperview()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if toRequest{
            if isAdmin.lowercased() == "admin"{
                isPrimaryAdmin = isAdmin
            }
            self.setActiveButton(index: 1)
            self.getGroupRequests(grpId: groupId)
        }
        else{
            self.setActiveButton(index: 0)
            self.getMemberListAPI(groupId: self.groupId)
        }
        self.navigationController?.navigationBar.isHidden = true
        setFloatingButton()
    }
    
    override func viewDidLayoutSubviews(){
        super.viewDidLayoutSubviews()
        let indexPath = IndexPath(item: ActiveTabs, section: 0)
        aboutClctnVew.scrollToItem(at: indexPath, at:.centeredHorizontally, animated: true)
    }
    
    //MARK:- Web API
    func getMemberListAPI(groupId:String){
        APIServiceManager.callServer.getMembersList(url: EndPoint.viewMember, groupId: groupId, userId: UserDefaults.standard.value(forKey: "userId") as! String, query: searchTxt, success: { (responseMdl) in
            
            guard let result = responseMdl as? viewFamilyMembersResult else{
                return
            }
            ActivityIndicatorView.hiding()
            if result.status_code == 200{
                
                self.memberArr = result.joinData!
                if self.memberArr.count > 0{
                    self.isPrimaryAdmin = self.memberArr[0].isAdmin
                    self.lblMembersCount.text = "\(self.memberArr.count - result.adminCount)"
                    self.lblAdminsCount.text = "\(result.adminCount)"
                    self.btnFloat.isHidden = false
//                    self.tableView.reloadData()
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                        
                        self.tableView.reloadData()
                    })
                }
                else{
                    self.lblMembersCount.text = "0"
                    self.lblAdminsCount.text = "0"
                    self.btnFloat.isHidden = true
                    self.tableView.reloadData()
                }
            }
            
        }) { (error) in
            ActivityIndicatorView.hiding()
        }
    }
    
    func getGroupRequests(grpId:String){
        APIServiceManager.callServer.getAllGroupRequest(url: EndPoint.groupRequestList, groupId: grpId, success: { (responseMdl) in
            guard let result = responseMdl as? groupReqestResult else{
                return
            }
            ActivityIndicatorView.hiding()
            if result.status_code == 200{
                self.requestArr = result.result!
                if self.requestArr.isEmpty {
                    self.tableView.reloadData()
                    self.noMembersLbl.text = "No requests"
                    self.tableView.backgroundView = self.viewNoMembers
                    
                } else {
                    
                    self.tableView.reloadData()
                }
            }
            
        }) { (error) in
            ActivityIndicatorView.hiding()
        }
    }
    
    func requestAccept(tag:Int){
        let tempDic = requestArr[tag]
        
        APIServiceManager.callServer.groupRequestResponse(url: EndPoint.groupRequestResp, tableId: "\(tempDic.id)", userId: UserDefaults.standard.value(forKey: "userId") as! String, groupId: "\(tempDic.groupId)", type: tempDic.type, status: "accepted", success: { (responseMdl) in
            
            guard let result = responseMdl as? requestSuccessModel else{
                return
            }
            
            if result.status_code == 200 {
                //                self.tableView.isHidden = true
                self.getGroupRequests(grpId: self.groupId)
            }
            else{
                ActivityIndicatorView.hiding()
            }
            
        }) { (error) in
            ActivityIndicatorView.hiding()
        }
    }
    
    func requestReject(tag:Int){
        let tempDic = requestArr[tag]
        APIServiceManager.callServer.groupRequestResponse(url: EndPoint.groupRequestResp, tableId: "\(tempDic.id)", userId: UserDefaults.standard.value(forKey: "userId") as! String, groupId: "\(tempDic.groupId)", type: tempDic.type, status: "rejected", success: { (responseMdl) in
            
            guard let result = responseMdl as? requestSuccessModel else{
                return
            }
            
            if result.status_code == 200{
                self.getGroupRequests(grpId: self.groupId)
            }
            else{
                ActivityIndicatorView.hiding()
            }
            
        }) { (error) in
            ActivityIndicatorView.hiding()
        }
    }
    
    //MARK:- Button Actions
    
    @IBAction func onClickMembersAction(_ sender: Any) {
        setActiveButton(index: 0)
        getMemberListAPI(groupId: groupId)
    }
    
    @IBAction func onClickRequestsAction(_ sender: Any) {
        setActiveButton(index: 1)
        getGroupRequests(grpId: groupId)
    }
    
    @IBAction func onClickContributionsAction(_ sender: Any) {
        //        setActiveButton(index: 2) InviteFriendsViewController
       /* let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let addMember = storyboard.instantiateViewController(withIdentifier: "AddorInvitePeopleViewController") as! AddorInvitePeopleViewController
        addMember.groupId = self.groupId
        self.navigationController?.pushViewController(addMember, animated: true)*/
        //********** Modified Jeena 22-02-2021 -- add if condition
        if self.isAdmin.lowercased() != "admin"{
            self.displayAlert(alertStr: "You are not an admin. Only admin can invite new members.", title: "")
            return
        }else{
            let storyboard = UIStoryboard.init(name: "InviteMember", bundle: nil)
            let addMember = storyboard.instantiateViewController(withIdentifier: "NewInviteMemberViewController") as! NewInviteMemberViewController
            addMember.groupId = groupId
            self.navigationController?.pushViewController(addMember, animated: true)
        }
        //********************* end **********************
    }
    
    @IBAction func onClickBulkUploadAction(_ sender: Any) {
        
    }
    @IBAction func onClickSearchReset(_ sender: Any) {
        txtSearchMembers.text = ""
        searchTxt = ""
        self.btnSearchReset.isHidden = true
        self.getMemberListAPI(groupId: self.groupId)
        self.txtSearchMembers.endEditing(true)
    }
    
    @IBAction func onClickShowAllAction(_ sender: Any) {
        
    }
    @objc func onclickFloatingAction(){
        
        switch ActiveTab {
        case 0:
            pushToAddMember()
            break
        case 1:
            //            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            //            let addMember = storyboard.instantiateViewController(withIdentifier: "AddMembers_SearchTableViewController") as! AddMembers_SearchTableViewController
            //            self.navigationController?.pushViewController(addMember, animated: true)
            break
        default:
            break
        }
        
    }
    @IBAction func onClickAddMemberEmptyListAction(_ sender: Any) {
        pushToAddMember()
    }
    
    @IBAction func onClickBulkUploadEmptyListAction(_ sender: Any) {
        
    }
    
    @IBAction func onClickRightmenuAction(_ sender: UIButton) {
        btnFloat.isHidden = true
        
        /*if  self.memberArr[sender.tag].isAdmin.lowercased() == "admin"{
         let userId = self.memberArr[sender.tag].userId as! Int
         
         if "\(userId)" == UserDefaults.standard.value(forKey: "userId") as! String{
         titlesForAction = [""]
         }
         else{
         if self.memberArr[sender.tag].is_blocked{
         titlesForAction = ["UnBlock","Remove"]
         
         }
         else{
         if memberArr[indexPath.row].userType.lowercased() == "admin"
         }
         }
         }
         else{
         titlesForAction = [""]
         
         }*/
        if memberArr[sender.tag].is_blocked{
            titlesForAction = ["UnBlock","Remove"]
        }
        else{
            titlesForAction = ["Block","Remove"]
        }
        
        if memberArr[sender.tag].userType.lowercased() == "admin"{
            titlesForAction.insert("Remove Admin", at: 2)
        }
        else{
            titlesForAction.insert("Make Admin", at: 2)
        }
        titlesForAction.insert("Payment History", at: 3)

        showActionSheet(titleArr: titlesForAction as NSArray, title: "Choose option" ) { (selectdIndex) in
            
            if selectdIndex == 0{ //relationship
                
                self.btnFloat.isHidden = false
                if selectdIndex == 0 && self.memberArr[sender.tag].is_blocked{ //unblock user
                    
                    let params = ["id":"\(self.memberArr[sender.tag].id)","is_blocked":NSNumber(value: false)] as [String : Any]
                    APIServiceManager.callServer.updateGroupMaps(url: EndPoint.update_groupmaps, params: params, success: { (response) in
                        ActivityIndicatorView.hiding()
                        guard let result = response as? requestSuccessModel else{
                            return
                        }
                        
                        if result.status_code == 200{
                            var model = self.memberArr[sender.tag]
                            model.is_blocked = false
                            self.memberArr[sender.tag] = model
                            self.tableView.beginUpdates()
                            self.tableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .none)
                            self.tableView.endUpdates()
                            self.displayAlert(alertStr: "User unblocked successfully", title: "")
                        }
                    }, failure: { (error) in
                        ActivityIndicatorView.hiding()
                    })
                }
                else if selectdIndex == 0 && !self.memberArr[sender.tag].is_blocked{ //block user
                    
                    let params = ["id":"\(self.memberArr[sender.tag].id)","is_blocked":NSNumber(value: true)] as [String : Any]
                    APIServiceManager.callServer.updateGroupMaps(url: EndPoint.update_groupmaps, params: params, success: { (response) in
                        ActivityIndicatorView.hiding()
                        guard let result = response as? requestSuccessModel else{
                            return
                        }
                        
                        if result.status_code == 200{
                            var model = self.memberArr[sender.tag]
                            model.is_blocked = true
                            self.memberArr[sender.tag] = model
                            self.tableView.beginUpdates()
                            self.tableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .none)
                            self.tableView.endUpdates()
                            
                            self.getMemberListAPI(groupId: self.groupId)
                            self.displayAlert(alertStr: "User blocked successfully", title: "")
                        }
                    }, failure: { (error) in
                        ActivityIndicatorView.hiding()
                    })
                }
                
            }
            else if selectdIndex == 100{
                self.btnFloat.isHidden = false
            }
                
            else if selectdIndex == 1{
                self.btnFloat.isHidden = false
                if  self.memberArr[sender.tag].isAdmin.lowercased() == "admin"{
                    
                    if selectdIndex == 1{ // remove
                        
                        let params = ["id":"\(self.memberArr[sender.tag].id)","is_removed":NSNumber(value: true)] as [String : Any]
                        APIServiceManager.callServer.updateGroupMaps(url: EndPoint.update_groupmaps, params: params, success: { (response) in
                            ActivityIndicatorView.hiding()
                            guard let result = response as? requestSuccessModel else{
                                return
                            }
                            if result.status_code == 200{
                                //                                var model = self.memberArr[sender.tag]
                                //                                model.is_removed = true
                                //                                self.memberArr[sender.tag] = model
                                //                                self.tableView.beginUpdates()
                                //                                self.tableView.deleteRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .left)
                                //                                self.tableView.endUpdates()
                                
//                                self.memberArr.remove(at: sender.tag)
//
//                                self.tblListView.reloadData()
                                
                                self.displayAlert(alertStr: "User removed successfully", title: "")
                                self.getMemberListAPI(groupId: self.groupId)
                            }
                        }, failure: { (error) in
                            ActivityIndicatorView.hiding()
                        })
                    }
                }
            }
                else if selectdIndex == 3{
                let storyboard = UIStoryboard.init(name: "PaymentHistory", bundle: nil)
                             
                let vc = storyboard.instantiateViewController(withIdentifier: "PaymentHistoryMainViewController") as! PaymentHistoryMainViewController
//                vc.isFromFamily = true
                appDel.paymentHistoryFromFamily = true
                appDel.memberIdForPaymentHistory = (self.memberArr[sender.tag].userId)
                       appDel.groupIdForPaymentHistory = self.groupId
//                vc.groupId = self.groupId
//                vc.memberId = (self.memberArr[sender.tag].userId)
                vc.user_name = self.memberArr[sender.tag].fullname
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else{
                self.btnFloat.isHidden = false
                var params = [String:Any]()
                //                if  self.memberArr[sender.tag].isAdmin.lowercased() == "admin"{
                //                     params = ["id":"\(self.memberArr[sender.tag].id)","is_admin":NSNumber(value: false)] as [String : Any]
                //                }
                //                else{
                //                    params = ["id":"\(self.memberArr[sender.tag].id)","is_admin":NSNumber(value: false)] as [String : Any]
                //                }
                if self.memberArr[sender.tag].userType.lowercased() == "admin"{
                    params = ["id":"\(self.memberArr[sender.tag].id)","type":"member"] as [String : Any]
                }
                else{
                    params = ["id":"\(self.memberArr[sender.tag].id)","type":"admin"] as [String : Any]
                }
                APIServiceManager.callServer.updateGroupMaps(url: EndPoint.update_groupmaps, params: params, success: { (response) in
                    ActivityIndicatorView.hiding()
                    guard let result = response as? requestSuccessModel else{
                        return
                    }
                    if result.status_code == 200{
                        self.getMemberListAPI(groupId: self.groupId)
                       /* var model = self.memberArr[sender.tag]
                        model.is_removed = true
                        self.memberArr[sender.tag] = model
                        // self.memberArr.remove(at: sender.tag)
                        self.tableView.beginUpdates()
                        self.tableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .none)
                        // self.tableView.deleteRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .left)
                        self.tableView.endUpdates()*/
                        // self.displayAlert(alertStr: "User removed successfully", title: "")
                    }
                }, failure: { (error) in
                    ActivityIndicatorView.hiding()
                })
            }
        }
        
    }
    
    @IBAction func onClickBtnAccept(_ sender: UIButton) {
        // let tempDic = requestArr[sender.tag]
        
        if memberJoining == 4{
            if isPrimaryAdmin.lowercased() == "admin"{
                requestAccept(tag: sender.tag)
            }
            else{
                self.displayAlert(alertStr: "No access", title: "")
            }
        }
        else{
            requestAccept(tag: sender.tag)
        }
        
    }
    @IBAction func onClickBtnReject(_ sender: UIButton) {
        _ = requestArr[sender.tag]
        if memberJoining == 4{
            if isPrimaryAdmin.lowercased() == "admin"{
                requestReject(tag: sender.tag)
            }
            else{
                self.displayAlert(alertStr: "No access", title: "")
            }
        }
        else{
            
            requestReject(tag: sender.tag)
        }
        
    }
    
    @IBAction func onClickAddRelation(_ sender: UIButton) {
        self.btnFloat.isHidden = false
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SelectRelationTableViewController") as! SelectRelationTableViewController
        let nav = UINavigationController.init(rootViewController: vc)
        vc.SelectedUserId = "\(self.memberArr[sender.tag].userId)"
        vc.tableId = "\(self.memberArr[sender.tag].tableId)"
        vc.currentRelationShip = "\(self.memberArr[sender.tag].relationShip)"
        vc.isUpdate = false
        vc.cate = self.faCate
        vc.groupId = self.groupId
        vc.delegate = self
        
        if !self.memberArr[sender.tag].relationShip.isEmpty{
            vc.isUpdate = true
        }
        self.present(nav, animated: true, completion: nil)
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        // self.navigationController?.popViewController(animated: true)
        self.navigationController?.popToViewController(ofClass: FamilyDetailsTableViewController.self)
    }
    
    @IBAction func onClickEventsActions(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "third", bundle: nil)
        let addMember = storyboard.instantiateViewController(withIdentifier: "EventListingViewController") as! EventListingViewController
        addMember.groupID = self.groupId
        addMember.isAdmin = self.isAdmin
        addMember.link_type = self.link_type
        addMember.islinkable = self.islinkable
        addMember.facte = self.faCate
        addMember.famSettings = famSettings
        addMember.announcementSet = announcementSet
        addMember.postId                    = self.postId
        addMember.announcementId            = self.announcementId
        addMember.isMembershipActive = self.isMembershipActive
        addMember.famArr = famArr
        addMember.memberJoining = self.memberJoiningStatus
        addMember.invitationStatus = self.invitationStatus
        self.navigationController?.pushViewController(addMember, animated: false)
    }
    @IBAction func onClickAlbumsActions(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "third", bundle: nil)
        let addMember = storyboard.instantiateViewController(withIdentifier: "AlbumListingViewController") as! AlbumListingViewController
        addMember.groupID = self.groupId
        addMember.isAdmin = self.isAdmin
        addMember.link_type = self.link_type
        addMember.islinkable = self.islinkable
        addMember.facate = self.faCate
        addMember.famSettings = famSettings
        addMember.announcementSet = announcementSet
        addMember.postId                    = self.postId
        addMember.announcementId            = self.announcementId
        addMember.isMembershipActive = self.isMembershipActive
        addMember.famArr = famArr
        addMember.memberJoining = self.memberJoiningStatus
        addMember.invitationStatus = self.invitationStatus
        self.navigationController?.pushViewController(addMember, animated: false)
    }
    @IBAction func onClickAboutActions(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
        intro.groupId = self.groupId
        intro.isAboutUsClicked = true
        self.navigationController?.pushViewController(intro, animated: false)
    }
    @IBAction func onClickMore(_ sender: Any) {
        let arr = ["Documents","Linked Families","Post","Announcement"]
        
        self.showActionSheet(titleArr: arr as NSArray, title: "Choose option") { (index) in
            if index == 0{
                let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "DocumentListingViewController") as! DocumentListingViewController
                addMember.groupID = self.groupId
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.isFromdocument = true
                addMember.facate = self.faCate
                addMember.postId                    = self.postId
                addMember.announcementId            = self.announcementId
                self.navigationController?.pushViewController(addMember, animated: false)
                //setActiveButton(index: 2)
            }
            else if index == 100{
            }
            else if index == 2{
                let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "PostedInGroupsViewController") as! PostedInGroupsViewController
                addMember.groupId = self.groupId
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.facate = self.faCate
                self.navigationController?.pushViewController(addMember, animated: false)
            }
            else if index == 3{
                let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "AnnouncementInFamilyViewController") as! AnnouncementInFamilyViewController
                addMember.groupId = self.groupId
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.facate = self.faCate
                self.navigationController?.pushViewController(addMember, animated: false)
            }
            else{
                let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "LinkedFamiliesListViewController") as! LinkedFamiliesListViewController
                addMember.groupID = self.groupId
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.isFromBlocked = false
                addMember.facate = self.faCate
                // addMember.faCate = self.familyArr![0].faCategory
                //                addMember.memberJoining = self.familyArr![0].memberApproval
                self.navigationController?.pushViewController(addMember, animated: false)
            }
        }
    }
    
    @IBAction func onClickAddMoreAction(_ sender: Any) {
//        setActiveButton(index: 3)
        let titleArr = ["Blocked Members","Pending Invites", "Payment History"]
        self.showActionSheet(titleArr: titleArr as NSArray, title: "Choose option") { (index) in
        if index == 0{
            if self.isAdmin.lowercased() != "admin"{
                self.displayAlert(alertStr: "You are not an admin. Only admin can view the blocked members.", title: "")
                return
            }
            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            let addMember = storyboard.instantiateViewController(withIdentifier: "LinkedFamiliesListViewController") as! LinkedFamiliesListViewController
            addMember.groupID = self.groupId
            addMember.isAdmin = self.isAdmin
            addMember.link_type = self.link_type
            addMember.islinkable = self.islinkable
            addMember.isFromBlocked = true
            addMember.facate = self.faCate
            // addMember.faCate = self.familyArr![0].faCategory
            //                addMember.memberJoining = self.familyArr![0].memberApproval
            self.navigationController?.pushViewController(addMember, animated: false)
        }
        else if index == 1{
            if self.isAdmin.lowercased() != "admin"{
                self.displayAlert(alertStr: "You are not an admin. Only admin can view the pending request.", title: "")
                return
            }
            let stryboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
            let vc = stryboard.instantiateViewController(withIdentifier: "PendingRequestViewController") as! PendingRequestViewController
            vc.isAdmin = true
            vc.groupId = "\(self.groupId)"
            vc.isFromFamily = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
            else if index == 2
        {
            if self.isAdmin.lowercased() != "admin"{
                           self.displayAlert(alertStr: "You are not an admin. Only admin can view the Payment History.", title: "")
                           return
                       }
               let storyboard = UIStoryboard.init(name: "PaymentHistory", bundle: nil)
                                         
                            let vc = storyboard.instantiateViewController(withIdentifier: "PaymentHistoryMainViewController") as! PaymentHistoryMainViewController
            //                vc.isFromFamily = true
                            appDel.paymentHistoryFromFamily = true
                            appDel.memberIdForPaymentHistory = 1000
                                   appDel.groupIdForPaymentHistory = self.groupId
            //                vc.groupId = self.groupId
            //                vc.memberId = (self.memberArr[sender.tag].userId)
                            vc.user_name = ""
            vc.family_name = appDel.groupNamePublic
                            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        else if index == 100{
            self.setActiveButton(index: 0)
            self.getMemberListAPI(groupId: self.groupId)
        }
        else{
            self.setActiveButton(index: 0)
            self.getMemberListAPI(groupId: self.groupId)
            }
        }
    }
    
    //MARK:- Custom Actions
    
    func setActiveButton(index:Int){
        switch index {
        case 0: //Members
            
            lblMembers.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            lblRequests.textColor = UIColor.black
            lblContributions.textColor = UIColor.black
            lblMore.textColor = .black
            
            imgMembers.image = #imageLiteral(resourceName: "imgMember_green")
            imgInvite.image = #imageLiteral(resourceName: "imgInvite_black")
            imgRequts.image = #imageLiteral(resourceName: "imgRequst_black")
            imgMore.image = #imageLiteral(resourceName: "imgMore_black")
            
            //            viewActiveMembers.isHidden = false
            //            viewActiveRequest.isHidden = true
            //            viewActiveContributions.isHidden = true
            
            viewRequestsToHide.isHidden = true
            viewSearchToHide.isHidden = false
            
            ActiveTab = 0
            
            break
        case 1: //Request
            
            lblMembers.textColor = UIColor.black
            lblRequests.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            lblContributions.textColor = UIColor.black
            lblMore.textColor = .black
            
            imgMembers.image = #imageLiteral(resourceName: "imgMember_black")
            imgInvite.image = #imageLiteral(resourceName: "imgInvite_black")
            imgRequts.image = #imageLiteral(resourceName: "imgRequest_green")
            imgMore.image = #imageLiteral(resourceName: "imgMore_black")
            
            //            viewActiveMembers.isHidden = true
            //            viewActiveRequest.isHidden = false
            //            viewActiveContributions.isHidden = true
            
            viewRequestsToHide.isHidden = true
            viewSearchToHide.isHidden = true
            ActiveTab = 1
            
            break
        case 2: //Contributions
            
            lblMembers.textColor = UIColor.black
            lblRequests.textColor = UIColor.black
            lblMore.textColor = .black
            lblContributions.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            
            imgMembers.image = #imageLiteral(resourceName: "imgMember_black")
            imgInvite.image = #imageLiteral(resourceName: "imgInvite_green")
            imgRequts.image = #imageLiteral(resourceName: "imgRequst_black")
            imgMore.image = #imageLiteral(resourceName: "imgMore_black")
            
            //            viewActiveMembers.isHidden = true
            //            viewActiveRequest.isHidden = true
            //            viewActiveContributions.isHidden = false
            
            viewRequestsToHide.isHidden = true
            viewSearchToHide.isHidden = true
            
            ActiveTab = 2
            
            break
            
        case 3: //More
            
            lblMembers.textColor = UIColor.black
            lblRequests.textColor = UIColor.black
            lblContributions.textColor = .black
            lblMore.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            
            imgMembers.image = #imageLiteral(resourceName: "imgMember_black")
            imgInvite.image = #imageLiteral(resourceName: "imgInvite_black")
            imgRequts.image = #imageLiteral(resourceName: "imgRequst_black")
            imgMore.image = #imageLiteral(resourceName: "imgMore_green")
            
            //            viewActiveMembers.isHidden = true
            //            viewActiveRequest.isHidden = true
            //            viewActiveContributions.isHidden = false
            
            viewRequestsToHide.isHidden = true
            viewSearchToHide.isHidden = true
            
            ActiveTab = 2
            
            break
            
        default:
            break
        }
        setFloatingButton()
        tableView.reloadData()
    }
    
    func setFloatingButton(){
        btnFloat.frame = CGRect(x: UIScreen.main.bounds.size.width - 60, y: UIScreen.main.bounds.size.height - 90, width: 40, height: 40)
        if ActiveTab == 0{//Members
            //btnFloat.setTitle("+", for: .normal)
            btnFloat.setImage(UIImage(named: "flotingAdd"), for: .normal)
            //  btnFloat.titleLabel?.font = UIFont.boldSystemFont(ofSize: 28)
        }
        else if ActiveTab == 1{//Requests
            // btnFloat.setTitle("=", for: .normal)
            btnFloat.isHidden = true
        }
        else{//Contributions
            // btnFloat.setTitle("..", for: .normal)
            btnFloat.isHidden = true
        }
        
        btnFloat.setTitleColor(UIColor.white, for: .normal)
        btnFloat.backgroundColor = UIColor(named: "purpleBackground")
        btnFloat.clipsToBounds = true
        btnFloat.layer.cornerRadius = 8
        btnFloat.addTarget(self,action: #selector(onclickFloatingAction), for: UIControl.Event.touchUpInside)
        if let window = UIApplication.shared.keyWindow {
            window.addSubview(btnFloat)
        }
    }
    private func pushToAddMember(){
        /* let storyboard = UIStoryboard.init(name: "second", bundle: nil)
         let addMember = storyboard.instantiateViewController(withIdentifier: "AddorInvitePeopleViewController") as! AddorInvitePeopleViewController
         addMember.groupId = groupId
         self.navigationController?.pushViewController(addMember, animated: true)*/
        let storyboard = UIStoryboard.init(name: "InviteMember", bundle: nil)
        let addMember = storyboard.instantiateViewController(withIdentifier: "NewInviteMemberViewController") as! NewInviteMemberViewController
        addMember.groupId = groupId
        self.navigationController?.pushViewController(addMember, animated: true)
    }
    
    //MARK:- Relationship Delegates
    func relationSelected(selectRelation: Relations) {
        print(selectRelation)
        getMemberListAPI(groupId: groupId)
    }
    func fromNewRelation(){
        getMemberListAPI(groupId: groupId)
    }
    
    
    //MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if ActiveTab == 0 {
            return 280
        }
        return 220
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if ActiveTab == 0 {
            return 110
        }
        return 110
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        switch ActiveTab {
        case 0: //Check for array count and add background view if zero
            
            if memberArr.count > 0{
                tableView.backgroundView = nil
                return 1
            }
            else{
                //                self.noMembersLbl.text = "No Members"
                tableView.backgroundView = nil
                return 1
            }
            
            //            tableView.backgroundView = viewNoMembers
            //            return 0
            
            //            tableView.backgroundView = nil
            //            return 1
            
        case 1:
            tableView.backgroundView = nil
            return 1
            
        default:
            return 0
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch ActiveTab {
        case 0:
            return memberArr.count + 1
            
        case 1:
            return requestArr.count
            
        default:
            return 0
            
        }
    }
    
    func convertDateToDisplayDate(dateFromResponse:String)->String{
        if dateFromResponse != ""
        {
            let dateAsString               = dateFromResponse
            let dateFormatter              = DateFormatter()
            dateFormatter.dateFormat       = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            
            if let datee =  dateFormatter.date(from: dateAsString){
                dateFormatter.dateFormat       = "MMM dd yyyy"
                let DateFormatted = dateFormatter.string(from: datee)
                return DateFormatted
            }
            return ""
        }else
        {
            return ""
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if ActiveTab == 0 {
           
            if memberArr.count == indexPath.row{
                return UITableViewCell()
            }
            else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddMembers_MembersTableViewCell", for: indexPath) as! AddMembers_MembersTableViewCell
                
                cell.lblName.text = memberArr[indexPath.row].fullname
                cell.lblGender.text = memberArr[indexPath.row].gender
                let userId = UserDefaults.standard.value(forKey: "userId") as! String
                
                let user = memberArr[indexPath.row].userId
                
                
                
                //            let tDate = memberArr[indexPath.row].memberSince
                //            let tArr = tDate.components(separatedBy: "T")
                //
                //            cell.lblMemberSince.text = tArr.first
                cell.lblMemberSince.text = self.convertDateToDisplayDate(dateFromResponse:memberArr[indexPath.row].memberSince)
                
                if self.faCate.lowercased() == "regular"{
                    cell.btnAddDesignation.isEnabled = true
                    if memberArr[indexPath.row].relationShip.count > 0{
                        DispatchQueue.main.async {
                            cell.lblDesignation.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                            cell.lblDesignation.text = self.memberArr[indexPath.row].relationShip
                        }
                    }
                    else{
                        if userId == "\(user)"{
                            cell.vewRelation.isHidden = true
                            
                        }
                        else{
                            DispatchQueue.main.async {
                                cell.vewRelation.isHidden = false
                                cell.lblDesignation.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                                cell.lblDesignation.text = "Add Relationship"
                            }
                        }
                    }
                }
                else{
                    if memberArr[indexPath.row].relationShip.count > 0{
                        DispatchQueue.main.async {
                            cell.lblDesignation.text          = self.memberArr[indexPath.row].relationShip
                            cell.lblDesignation.textColor     = .lightGray
                        }
                    }
                    else{
                        let user = memberArr[indexPath.row].userId
                        if "\(user)" == UserDefaults.standard.value(forKey: "userId") as! String{
                            DispatchQueue.main.async {
                                cell.lblDesignation.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                                cell.lblDesignation.text = "Add Role"
                                cell.btnAddDesignation.isEnabled = true
                            }
                        }
                        else{
                            if memberArr[indexPath.row].isAdmin.lowercased() == "admin"{
                                DispatchQueue.main.async {
                                    cell.lblDesignation.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                                    cell.lblDesignation.text = "Add Role"
                                    cell.btnAddDesignation.isEnabled = true
                                }
                            }
                            else{
                                DispatchQueue.main.async {
                                    
                                    cell.lblDesignation.textColor = UIColor.gray
                                    cell.lblDesignation.text = "Add Role"
                                    cell.lblDesignation.textColor = UIColor.gray
                                    cell.btnAddDesignation.isEnabled = false
                                }
                            }
                        }
                    }
                }
                
                
                
                if memberArr[indexPath.row].propic.count != 0{
                    
                    let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+memberArr[indexPath.row].propic
                    let imgUrl = URL(string: temp)
                    //                cell.imageViewAvatar.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
                    let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+imgUrl!.relativeString
                    let url = URL(string: newUrlStr)
                    DispatchQueue.main.async {
                        cell.imageViewAvatar.kf.indicatorType = .activity

                        cell.imageViewAvatar.kf.setImage(with: url, placeholder:#imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
                    }
                    
                }else{
                    DispatchQueue.main.async {
                        cell.imageViewAvatar.image = #imageLiteral(resourceName: "Male Colored")
                    }
                }
                
                if memberArr[indexPath.row].isAdmin.lowercased() == "admin"{
                    let user = memberArr[indexPath.row].userId
                    if "\(user)" == UserDefaults.standard.value(forKey: "userId") as! String{
                        cell.btnRight.isEnabled = false
                    }
                    else{
                        cell.btnRight.isEnabled = true
                    }
                }
                else{
                    cell.btnRight.isEnabled = false
                }
                
                cell.btnRight.tag = indexPath.row
                cell.btnAddDesignation.tag = indexPath.row
                
                cell.viewAdmin.clipsToBounds = true
                cell.viewAdmin.layer.cornerRadius = 10
                cell.viewAdmin.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
                
                if isPrimaryAdmin.lowercased() == "admin"{
                    if userId == "\(user)"{
                        cell.imageViewRightButton.isHidden = true
                        cell.btnRight.isEnabled = false
                    }
                    else{
                        cell.imageViewRightButton.isHidden = false
                        cell.btnRight.isEnabled = true
                    }
                    
                }
                else{
                    cell.imageViewRightButton.isHidden = true
                    cell.btnRight.isEnabled = false
                }
                
                if memberArr[indexPath.row].userType.lowercased() == "admin"{
                    cell.lblAdmin.text = "Admin"
                }
                else{
                    cell.lblAdmin.text = "Member"
                }
                
                return cell
            }
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddMembers_RequestsTableViewCell", for: indexPath) as! AddMembers_RequestsTableViewCell
            cell.lblName.text = requestArr[indexPath.row].fullname
            
            if requestArr[indexPath.row].type.lowercased() == "request"{
                cell.lblLocation.text = "wants to join this family"
            }
            else{
              //  cell.lblLocation.text = "wants to link his family \(requestArr[indexPath.row].groupName) to this group"
                
                cell.lblLocation.text = "has requested to link \"\(requestArr[indexPath.row].groupName)\" family with this family"
            }
            
            if requestArr[indexPath.row].propic.count != 0{
                
                let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+requestArr[indexPath.row].propic
                let imgUrl = URL(string: temp)
                //                cell.imageViewAvatar.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
                let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+imgUrl!.relativeString
                let url = URL(string: newUrlStr)
                DispatchQueue.main.async {
                    cell.imageViewAvatar.kf.indicatorType = .activity

                    cell.imageViewAvatar.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                }
            }
            else{
                DispatchQueue.main.async {
                    cell.imageViewAvatar.image = #imageLiteral(resourceName: "Male Colored")
                }
            }
            
            cell.btnAccept.tag = indexPath.row
            cell.btnReject.tag = indexPath.row
            
            return cell
            
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if ActiveTab == 0{
            if memberArr.count == indexPath.row{
                return
            }
            else
            {
            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            let addMember = storyboard.instantiateViewController(withIdentifier: "UserProfileTableViewController") as! UserProfileTableViewController
            addMember.userID = "\(memberArr[indexPath.row].userId)"
//             if memberArr[indexPath.row].isAdmin.lowercased() == "admin"{
//                addMember.hidePaymentHistory = false
//
//            }
//            else
//             {
//           addMember.hidePaymentHistory = true
//            }
    self.navigationController?.pushViewController(addMember, animated: true)
            }
        }
        else{
            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            let addMember = storyboard.instantiateViewController(withIdentifier: "UserProfileTableViewController") as! UserProfileTableViewController
            addMember.userID = "\(requestArr[indexPath.row].user_id)"
//            if memberArr[indexPath.row].isAdmin.lowercased() == "admin"{
//                          addMember.hidePaymentHistory = false
//
//                      }
//                      else
//                       {
//                     addMember.hidePaymentHistory = true
//                      }
            self.navigationController?.pushViewController(addMember, animated: true)
        }
    }
    //MARK:- collectionView Delegates
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return aboutArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AboutFamilyCollectionViewCell", for: indexPath as IndexPath) as! AboutFamilyCollectionViewCell
        cell.lblName.text = aboutArr[indexPath.item]
            if indexPath.row == ActiveTabs{
                 cell.lblName.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                 cell.lblName.font = .boldSystemFont(ofSize: 15)
                 cell.imgUnderline.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)

             }
             else{
                 cell.lblName.textColor = .black
                 cell.lblName.font = .systemFont(ofSize: 14)
                 cell.imgUnderline.backgroundColor = .white
             }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0{ // Feeds
            let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
            let addMember = storyboard.instantiateViewController(withIdentifier: "PostedInGroupsViewController") as! PostedInGroupsViewController
            addMember.groupId = self.groupId
            addMember.isAdmin = self.isAdmin
            addMember.link_type = self.link_type
            addMember.islinkable = self.islinkable
            addMember.facate = self.faCate
            addMember.famSettings = famSettings
            addMember.announcementSet = announcementSet
            addMember.postId                    = self.postId
            addMember.announcementId            = self.announcementId
            addMember.isMembershipActive = self.isMembershipActive
            addMember.famArr = famArr
            addMember.memberJoining = self.memberJoiningStatus
            addMember.invitationStatus = self.invitationStatus
            self.navigationController?.pushViewController(addMember, animated: false)
        }
        else if indexPath.row == 1{ // Announcement
            let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
            let addMember = storyboard.instantiateViewController(withIdentifier: "AnnouncementInFamilyViewController") as! AnnouncementInFamilyViewController
            addMember.groupId = self.groupId
            addMember.isAdmin = self.isAdmin
            addMember.link_type = self.link_type
            addMember.islinkable = self.islinkable
            addMember.facate = self.faCate
            addMember.famSettings = famSettings
            addMember.announcementSet = announcementSet
            addMember.postId                    = self.postId
            addMember.announcementId            = self.announcementId
            addMember.isMembershipActive = self.isMembershipActive
            addMember.famArr = famArr
            addMember.memberJoining = self.memberJoiningStatus
            addMember.invitationStatus = self.invitationStatus
            self.navigationController?.pushViewController(addMember, animated: false)
        }
        else if indexPath.row == 2{ // Request
            let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
            let addMember = storyboard.instantiateViewController(withIdentifier: "RequestInFamilyViewController") as! RequestInFamilyViewController
            addMember.groupID = self.groupId
            addMember.isAdmin = self.isAdmin
            addMember.link_type = self.link_type
            addMember.islinkable = self.islinkable
            addMember.facate = self.faCate
            addMember.famSettings = famSettings
            addMember.announcementSet = announcementSet
            addMember.postId                    = self.postId
            addMember.announcementId            = self.announcementId
            addMember.isMembershipActive = self.isMembershipActive
            addMember.famArr = famArr
            addMember.memberJoining = self.memberJoiningStatus
            addMember.invitationStatus = self.invitationStatus
            self.navigationController?.pushViewController(addMember, animated: false)
        }
        else if indexPath.row == 3{ // Events
            onClickEventsActions(self)
        }
        else if indexPath.row == 5{ // About us
            onClickAboutActions(self)
        }
        else if indexPath.row == 8{ // Members
            if isMembershipActive{
                if isAdmin.lowercased() == "admin"{
                    let storyboard = UIStoryboard.init(name: "MembershipMngmnt", bundle: nil)
                    let addMember = storyboard.instantiateViewController(withIdentifier: "MemebrshipDashboardViewController") as! MemebrshipDashboardViewController
                    addMember.groupID = self.groupId
                    addMember.isAdmin = self.isAdmin
                    addMember.link_type = self.link_type
                    addMember.islinkable = self.islinkable
                    //                        addMember.isFromdocument = true
                    addMember.facate = self.faCate
                    addMember.famSettings = famSettings
                    addMember.announcementSet = announcementSet
                    addMember.postId                    = self.postId
                    addMember.announcementId            = self.announcementId
                    addMember.isMembershipActive = self.isMembershipActive
                    addMember.famArr = famArr
                    addMember.memberJoining = self.memberJoiningStatus
                    addMember.invitationStatus = self.invitationStatus
                    self.navigationController?.pushViewController(addMember, animated: true)
                }
                else{
                   getMemberListAPI(groupId: groupId)
                }
            }
            else{
               getMemberListAPI(groupId: groupId)
            }
        }
        else if indexPath.row == 9{
            getMemberListAPI(groupId: groupId)
        }
        else if indexPath.row == 4{ // Albums
            onClickAlbumsActions(self)
        }
        else if indexPath.row == 6{ // Documents
            let storyboard = UIStoryboard.init(name: "third", bundle: nil)
            let addMember = storyboard.instantiateViewController(withIdentifier: "DocumentListingViewController") as! DocumentListingViewController
            addMember.groupID = self.groupId
            addMember.isAdmin = self.isAdmin
            addMember.link_type = self.link_type
            addMember.islinkable = self.islinkable
            addMember.isFromdocument = true
            addMember.facate = self.faCate
            addMember.famSettings = famSettings
            addMember.announcementSet = announcementSet
            addMember.postId                    = self.postId
            addMember.announcementId            = self.announcementId
            addMember.isMembershipActive = self.isMembershipActive
            addMember.famArr = famArr
            addMember.memberJoining = self.memberJoiningStatus
            addMember.invitationStatus = self.invitationStatus
            self.navigationController?.pushViewController(addMember, animated: false)
        }
        else{ // Linked families
            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            let addMember = storyboard.instantiateViewController(withIdentifier: "LinkedFamiliesListViewController") as! LinkedFamiliesListViewController
            addMember.groupID = self.groupId
            addMember.isAdmin = self.isAdmin
            addMember.link_type = self.link_type
            addMember.islinkable = self.islinkable
            addMember.facate = self.faCate
            addMember.famSettings = famSettings
            addMember.announcementSet = announcementSet
            addMember.postId                    = self.postId
            addMember.announcementId            = self.announcementId
            addMember.isMembershipActive = self.isMembershipActive
            addMember.famArr = famArr
            addMember.memberJoining = self.memberJoiningStatus
            addMember.invitationStatus = self.invitationStatus
            self.navigationController?.pushViewController(addMember, animated: false)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = aboutArr[indexPath.row].size(withAttributes: nil)
        return CGSize(width: w.width, height: 60)
    }
}
extension AddMembersNewViewController:UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.searchTxt = textField.text!
        if textField.text!.count > 0{
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
        }
        // self.getMemberListAPI(groupId: self.groupId)
        textField.endEditing(true)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        self.searchTxt = textField.text!
        if textField.text!.count > 0{
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
        }
        self.getMemberListAPI(groupId: self.groupId)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text!.count > 0{
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text?.count == 0{
            self.searchTxt = ""
            btnSearchReset.isHidden = true
            //self.getMemberListAPI(groupId: self.groupId)
        }
        else{
            btnSearchReset.isHidden = false
        }
        return true
        
    }
}
