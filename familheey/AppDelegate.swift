//
//  AppDelegate.swift
//  familheey
//
//  Created by familheey on 03/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import CoreData
import FacebookCore
import FBSDKCoreKit
import GoogleSignIn
import LinkedinSwift
import IQKeyboardManagerSwift
import GooglePlaces
import UserNotifications

import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import SwiftyJSON
//import DropDown
import Moya
import SwiftyJSON
import Fabric
import Crashlytics
import AFNetworking
import Photos

import AWSS3
import CoreLocation
import MessageUI
//import FirebaseMessaging

let appDel  = UIApplication.shared.delegate as! AppDelegate


@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate ,CLLocationManagerDelegate,MFMailComposeViewControllerDelegate{
    var fromFamilyDetailBack = false
    var fromNavigationBack = false
    var notification_auto_delete = false
    var paymentHistoryFromFamily = false
    var memberIdForPaymentHistory = 0
      var groupIdForPaymentHistory = ""
    var fromInvite = false
    var isfromConversationAnnouncement       = false
    var getLocationInFirstTime = false
    let locationManager = CLLocationManager()
    var currentLocation : String!
    var AllRelations = [Relations]()
    var arrayOfAllCategory = [String]()
    var subscriptionArr = [[String:JSON]]()
    var arrayOfNotificationList = [[String:Any]]()
    var tempNotifi = [[String:Any]]()
    var notiUnreadCount:Int = 0
    var firstTimeNot = false
    var groupNamePublic   = String()
    var groupImageUrlPublic = String()
    var isFromNotification = false
    var notificationData = [String:Any]()
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    var window: UIWindow?
    var currentConversationId = ""
    var s3BaseUrl = ""
    var launchFromNotification = false
    var isFromEventDetails = false
    var isRequestUpdated = false
    var requestCreated = false
    var isFromShare = false
    var quickTour = false
    var editContact = false
    
    
    //    let linkedinHelper = LinkedinSwiftHelper(configuration: LinkedinSwiftConfiguration(clientId: "81hujmio4xeb6y", clientSecret: "ELTAbHgW5TNa3V4P", state: "DLKDJF45DIWOERCM", permissions: ["r_liteprofile", "r_emailaddress"], redirectUrl: "https://www.familheey.com/signin"))
    
    var selectedPlaceName1                   = String()
    var selectedPlaceName2                   = String()
    var selectedBaseName                     = String()
    var selectedBaseLat : Double!
    var selectedBAseLong :Double!
    var selectedEventVenue                   = String()
    var calendarFromDate                     = Int()
    var calendarToDate                       = Int()
    var userToken                            = String()
    var noFamily                             = false
    var postcreatedInPublic                   = false
    
    var isBackToCalendar                     = false
    var isConversationUpdated                = false
    var isUserHistoryAdded                   = false
    var createAcntFromGrp  = false
    var phoneHasVideos = false
    var isFromDiscoverTab = false
    var iscreation = ""
    var paymentStart = false
    var invitetype = ""
    var invitetypeId = ""
    var appVersion = ""
    var gifData = Data()
    var isFromLinkedFam = false
    var currentGrpID = ""
    var crntGrpName = ""
    var crntgroupImageUrlPublic = ""
    var isFamilyFromSearch = false
    
    var PreloadFamilyList = [SearchResultGroup]()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        let path = Bundle.main.path(forResource: "loading-33", ofType: "gif")!
        gifData = try! Data(contentsOf: URL(fileURLWithPath: path))
        Fabric.sharedSDK().debug = true
        
        appDel.iscreation = isFromCreation.myfamily
        
//        Fabric.with([Crashlytics.self, Answers.self])
        
        // Facebook login
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        //Google Login
        GIDSignIn.sharedInstance()?.clientID = Consts.googleClient_ID
        GMSPlacesClient.provideAPIKey(Consts.googlePlacesApiKey)
        
        IQKeyboardManager.shared.enable = true
        FirebaseApp.configure()
        
        application.applicationIconBadgeNumber = 0 // For Clear Badge Counts
        let center = UNUserNotificationCenter.current()
        center.removeAllDeliveredNotifications() // To remove all delivered notifications
        center.removeAllPendingNotificationRequests()
        
        //MARK:-Push Notifications
        if #available(iOS 10.0, *) {
            let authOptions : UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_,_ in })
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            // For iOS 10 data message (sent via FCM)
            Messaging.messaging().delegate = self
            //application.registerForRemoteNotifications()
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            //application.registerForRemoteNotifications()
        }
        /*****************************************************************/
        // Override point for customization after application launch.
           UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {(granted, error) in
              // Use granted to check if permission is granted
           }
        /*****************************************************************/
        application.registerForRemoteNotifications()
        
        
//        UserDefaults.standard.set("", forKey: "user_fullname")
        
        //DropDown.startListeningToKeyboard()
        
        let isLogin = UserDefaults.standard.value(forKey: "isLogin") as? String ?? "true"
        
        
        //setUserDefaultValues.setUserDefaultValue(userDefaultValue: "151", userDefaultKey: "userId") //Jeena 02-02-2021

        //Modified below if condition 02-02-2021
        if isLogin == "true"{
                    Helpers.getAccessToken { (accessToken) in
                         if accessToken!.isEmpty{
                             let domain = Bundle.main.bundleIdentifier!
                             UserDefaults.standard.removePersistentDomain(forName: domain)
                             UserDefaults.standard.synchronize()
                             
                             let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                             let loginView = mainStoryBoard.instantiateViewController(withIdentifier: "FirstViewController") as! FirstViewController
                             
                             let navView = UINavigationController(rootViewController: loginView)
                             let appDelegate = UIApplication.shared.delegate as! AppDelegate
                             appDelegate.window?.rootViewController = navView
                            
                         }
                         else{
                            // add if cond jeena 01-02-2021
                            if !self.isUserHistoryAdded{
                                self.callCurrentLocationForUserHistoryUpdate()
                            }
                             setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                             self.getPreloadFamilyList2()
                             self.getProfile()
                             self.getNotificationWhenChildAdded()
                             self.getAllKeys()
                         }
                    }
                }
    
        
        /*
         ***************** Commented below code jeena 02-02-2021*******
         if isLogin == "true"{
            if !isUserHistoryAdded{
                self.callCurrentLocationForUserHistoryUpdate()
            }
            
            let id =  UserDefaults.standard.value(forKey: "userId") as? String
           
            if !id!.isEmpty{

               Helpers.getAccessToken { (accessToken) in
                    if accessToken!.isEmpty{
                        let alert = UIAlertController(title: "", message: "Opps! Something went wrong", preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: { (acion) in
                            let domain = Bundle.main.bundleIdentifier!
                            UserDefaults.standard.removePersistentDomain(forName: domain)
                            UserDefaults.standard.synchronize()
                            
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let loginView = mainStoryBoard.instantiateViewController(withIdentifier: "FirstViewController") as! FirstViewController
                            
                            let navView = UINavigationController(rootViewController: loginView)
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = navView
                            
                            
                        }))
                        self.window?.rootViewController?.present(alert, animated: false, completion: nil)
                    }
                    else{
                        setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                        self.getPreloadFamilyList2()
                        self.getProfile()
                        self.getNotificationWhenChildAdded()
                        self.getAllKeys()
                    }
                }
            }
            
            //            let mainStoryBoard = UIStoryboard(name: "PostSection", bundle: nil)
            //                                  let vc = mainStoryBoard.instantiateViewController(withIdentifier: "RequestDetailsViewController") as! RequestDetailsViewController
            //                   let appDelegate = UIApplication.shared.delegate as! AppDelegate
            //
            //                   appDelegate.window?.rootViewController = vc
        }
        else{
            
        }
         *************************************************
        */
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        application.applicationIconBadgeNumber = 0
        if UserDefaults.standard.value(forKey: "userId") != nil{
            setupSocketForMessaging()
        }
        
        if launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] != nil {
            self.launchFromNotification = true
            self.isFromNotification = true
        }
        AFNetworkReachabilityManager.shared().setReachabilityStatusChange { (status: AFNetworkReachabilityStatus) -> Void in
            switch status {
            case .notReachable:
                if let currentVC = UIApplication.topViewController(){
                    currentVC.displayAlert(alertStr: "Please check your internet connection!", title: "Oops!")
                }
                break
            case .reachableViaWiFi, .reachableViaWWAN:
                print("und monee :D")
            case .unknown:
                print("enthuanno entho :p")
            default:
                break
            }
            self.initializeS3()
        }
        
        
        
        AFNetworkReachabilityManager.shared().startMonitoring()
        fetchAllVideos()
        return true
    }
    func initializeS3() {
        let poolId = "us-east-1:7780560d-b205-4634-89e9-e65ed40624a6"
        let credentialsProvider = AWSCognitoCredentialsProvider(regionType: .USEast1, identityPoolId: poolId)
        let configuration = AWSServiceConfiguration(region: .USEast1, credentialsProvider: credentialsProvider)
        AWSServiceManager.default().defaultServiceConfiguration = configuration
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        let stringUrl = url.absoluteString
        
        if let dynamic = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url){
            print(dynamic)
            return true
        }
        else{
            if stringUrl.contains("fb"){
                let sourceApplication: String? = options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String
                return ApplicationDelegate.shared.application(app, open: url, sourceApplication: sourceApplication, annotation: nil)
            }
            else if stringUrl.contains("li"){
                let sourceApplication: String? = options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String
                return LinkedinSwiftHelper.application(app, open: url, sourceApplication: sourceApplication, annotation: nil)
            }
            else if stringUrl.contains("familheey://share"){
                let isLogin = UserDefaults.standard.value(forKey: "isLogin") as? String ?? ""
                if isLogin == "true"{
                    
                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                    let tabbar = mainStoryBoard.instantiateViewController(withIdentifier: "mainTabView") as! HomeViewTabController
                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "HomeNavigation") as! HomeNavigation
                    tabbar.isFromShare = true
                    self.isFromShare = true
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = redViewController
                }
                return true
            }
                
            else if stringUrl.lowercased().contains("familheey") {
                let isLogin = UserDefaults.standard.value(forKey: "isLogin") as? String ?? ""
                if isLogin == "true"{
                    
                    let queryItems = URLComponents(string: stringUrl)?.queryItems
                    let type = queryItems?.first(where: { $0.name == "type" })?.value
                    let itemId = queryItems?.first(where: { $0.name == "id" })?.value
                    
                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                    let tabbar = mainStoryBoard.instantiateViewController(withIdentifier: "mainTabView") as! HomeViewTabController
                    tabbar.isFromInvite = true
                    self.invitetype = type ?? ""
                    self.invitetypeId = itemId ?? ""
                    self.fromInvite = true
                    
                    //                let navView = UINavigationController(rootViewController: tabbar)
                    //                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    //                appDelegate.window?.rootViewController = navView
                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "HomeNavigation") as! HomeNavigation
                    self.noFamily = false
                    self.postcreatedInPublic = false
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = redViewController
                    
                    return true
                }
                else{
                    return true
                }
            }
            else{
                //            return GIDSignIn.sharedInstance().handle(url as URL?,
                //                                                     sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                //                                                     annotation: options[UIApplication.OpenURLOptionsKey.annotation])
                return (GIDSignIn.sharedInstance()?.handle(url))!
            }
        }
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        if let url = userActivity.webpageURL{
            print("Incoming \(url)")
            let dynamicLink = DynamicLinks.dynamicLinks().handleUniversalLink(url) { (dynamicLink, error) in
                guard error == nil else{
                    print("Found Error \(error?.localizedDescription)")
                    return
                }
                if let dynamic = dynamicLink{
                    print("DynamicLink \(dynamic)")
                    self.handleIncomingDynamicLinks(dynamic)
                }
            }
            if dynamicLink{
                return true
            }
            else{
                return false
            }
        }
        
        return true
    }
    
    func handleIncomingDynamicLinks(_ dynamicLink:DynamicLink){
        guard let url = dynamicLink.url else{
            print("Oops!! Something went wrong")
            return
        }
        print("url absolute string---\(url.absoluteString)")
        let isLogin = UserDefaults.standard.value(forKey: "isLogin") as? String ?? ""
        if isLogin == "true"{
            
            let queryItems = URLComponents(string: url.absoluteString)?.queryItems
            let type = queryItems?.first(where: { $0.name == "type" })?.value
            let itemId = queryItems?.first(where: { $0.name == "type_id" })?.value
            
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let tabbar = mainStoryBoard.instantiateViewController(withIdentifier: "mainTabView") as! HomeViewTabController
            tabbar.isFromInvite = true
            self.invitetype = type?.components(separatedBy: CharacterSet.alphanumerics.inverted).joined() ?? ""
            self.invitetypeId = itemId ?? ""
            self.fromInvite = true
            
            //                let navView = UINavigationController(rootViewController: tabbar)
            //                let appDelegate = UIApplication.shared.delegate as! AppDelegate
            //                appDelegate.window?.rootViewController = navView
            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "HomeNavigation") as! HomeNavigation
            self.noFamily = false
            self.postcreatedInPublic = false
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = redViewController
            
            
        }
        else{
            
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface
        if self.isRequestUpdated && self.paymentStart{
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: Notification.Name("RequestUpdates"), object: nil)
            }
        }
        else if createAcntFromGrp{
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: Notification.Name("AccountCreate"), object: nil)
            }
        }

    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    //MARK:- Custom Methods
    func convertVersionNumber(verNum:String) -> Int{
        let temArr = verNum.components(separatedBy: ".")
        var item1 = 0
        var item2 = 0
        var item3 = 0
        if temArr.count == 3{
            item1 = Int(temArr[0])! * 1000
            item2 = Int(temArr[1])! * 100
            item3 = Int(temArr[2])!
        }
        return item1+item2+item3
    }
    
    
    private func createEmailUrl(to: String, subject: String, body: String) -> URL? {
        let subjectEncoded = subject.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        let bodyEncoded = body.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        
        let gmailUrl = URL(string: "googlegmail://co?to=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let outlookUrl = URL(string: "ms-outlook://compose?to=\(to)&subject=\(subjectEncoded)")
        let yahooMail = URL(string: "ymail://mail/compose?to=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let sparkUrl = URL(string: "readdle-spark://compose?recipient=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let defaultUrl = URL(string: "mailto:\(to)?subject=\(subjectEncoded)&body=\(bodyEncoded)")
        
        if let gmailUrl = gmailUrl, UIApplication.shared.canOpenURL(gmailUrl) {
            return gmailUrl
        } else if let outlookUrl = outlookUrl, UIApplication.shared.canOpenURL(outlookUrl) {
            return outlookUrl
        } else if let yahooMail = yahooMail, UIApplication.shared.canOpenURL(yahooMail) {
            return yahooMail
        } else if let sparkUrl = sparkUrl, UIApplication.shared.canOpenURL(sparkUrl) {
            return sparkUrl
        }
        
        return defaultUrl
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    //MARK:- WEB API
    
    
    func getPreloadFamilyList2(){
        let param = [
            "userid" : UserDefaults.standard.value(forKey: "userId") as! String,
            "searchtxt" : "",
            "offset": 0,
            "limit": 30,
            "phonenumbers":[],
            "type":"family"
            ] as [String : Any]
       // ActivityIndicatorView.show("Loading.....")
        networkProvider.request(.globalSearch(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
//                    let jsonData =  JSON(response.data)
                    
                    if response.statusCode == 200{
//                        print("@@@####!!!!---\(response.data)--\(response)")
                        
                        let someDictionaryFromJSON = try JSONSerialization.jsonObject(with: response.data, options: .allowFragments) as! [String: Any]
//                        print(someDictionaryFromJSON)
                        let tempArr = someDictionaryFromJSON
//                        print(tempArr)
                        do{
                            let searchResult = try SearchList(tempArr)
//                            print(searchResult)

                            if let results = searchResult as SearchList?{
                                print(results)
                                
                                
                                if results.searchResult2 != nil{
                                    appDel.PreloadFamilyList.append(contentsOf: results.searchResult2!)
                                    DispatchQueue.main.async {
                                        NotificationCenter.default.post(name: Notification.Name("NotificationForDiscoverTab"), object: nil)
                                    }
                                }
                            }
                        }catch{
                            //                                       print("catched")
                            return
                        }

//                        self.getProfile()
                    }
                    else if response.statusCode == 401{
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getProfile()
                        }
                    }
                        
                    else{
                        //                                       self.displayAlert(alertStr: "Something went wrong! Please try again later", title: "Error")
//                        ActivityIndicatorView.hiding()
                        self.getProfile()

                    }
                    
                    
                }catch let err {
                    self.getProfile()

//                    ActivityIndicatorView.hiding()
                    //                        Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
//                ActivityIndicatorView.hiding()
                self.getProfile()
                //                    Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    func getProfile(){
        if let id =  UserDefaults.standard.value(forKey: "userId") as? String{
            let params = ["user_id":id] as [String : Any]
            print(params)
            self.networkProvider.request(.onBoardCheck(parameter: params)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                        print("view contents : \(jsonData)")
                         appDel.notification_auto_delete = jsonData["notification_auto_delete"].boolValue
                        if response.statusCode == 200{
                            
                            
                            if jsonData["user_is_blocked"].boolValue{
                                let alert = UIAlertController(title: "", message: "Sorry, you account have been suspended. Please email us on contact@familheey.com to reactivate your account", preferredStyle: .alert)
                                
                                alert.addAction(UIAlertAction(title: "Compose Mail", style: .destructive, handler: { (acion) in
                                    
                                    
                                    
                                    // Modify following variables with your text / recipient
                                    let recipientEmail = "contact@familheey.com"
                                    let subject = "Reactivate Account - \(id) "
                                    let body = "Please reactivate Account , User id - \(id)"
                                    
                                    // Show default mail composer
                                    if MFMailComposeViewController.canSendMail() {
                                        let mail = MFMailComposeViewController()
                                        mail.mailComposeDelegate = self
                                        mail.setToRecipients([recipientEmail])
                                        mail.setSubject(subject)
                                        mail.setMessageBody(body, isHTML: false)
                                        
                                        self.window?.rootViewController?.present(mail, animated: true)
                                        
                                        // Show third party email composer if default Mail app is not present
                                    } else if let emailUrl = self.createEmailUrl(to: recipientEmail, subject: subject, body: body) {
                                        UIApplication.shared.open(emailUrl)
                                    }
                                    
                                    
                                }))
                                
                                alert.addAction(UIAlertAction(title: "Login", style: .destructive, handler: { (acion) in
                                    
                                    let domain = Bundle.main.bundleIdentifier!
                                    UserDefaults.standard.removePersistentDomain(forName: domain)
                                    UserDefaults.standard.synchronize()
                                    
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let loginView = mainStoryBoard.instantiateViewController(withIdentifier: "FirstViewController") as! FirstViewController
                                    
                                    let navView = UINavigationController(rootViewController: loginView)
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = navView
 
                                }))
                                
                                self.window?.rootViewController?.present(alert, animated: false, completion: nil)
                                return
                            }
                            
                            self.appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
                            //isLogedOut
                            if jsonData["ios_force"].boolValue{
                                let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
                                let convertedAppVersion = self.convertVersionNumber(verNum: appVersion!)
                                let versionFromDB = self.convertVersionNumber(verNum: jsonData["ios_version"].stringValue)
                                
                                print("#### Version: \(convertedAppVersion) && \(versionFromDB) ")
                                
                                if convertedAppVersion >= versionFromDB{
                                    let isLogedOut = UserDefaults.standard.value(forKey: "isLogedOut") as? String ?? ""
                                    if isLogedOut == "1"{
                                        self.checkNavigation(jsonData: jsonData)
                                    }
                                    else{
                                        self.checkNavigation(jsonData: jsonData)
                                    }
                                }
                                else{
                                    let alert = UIAlertController(title: "New version available", message: "Please update the application", preferredStyle: .alert)
                                    
                                    alert.addAction(UIAlertAction(title: "Update", style: .destructive, handler: { (acion) in
                                        //                                    UIApplication.shared.openURL(NSURL(string: "https://apps.apple.com/us/app/familheey/id1485617876?ls=1")! as URL)
                                        UIApplication.shared.open(URL(string: "https://apps.apple.com/us/app/familheey/id1485617876?ls=1")!, options: [:], completionHandler: nil)
                                        
                                    }))
                                    
                                    self.window?.rootViewController?.present(alert, animated: false, completion: nil)
                                }
                                
                            }
                            else{
                                self.checkNavigation(jsonData: jsonData)
                                //                                let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
                                //                                let temp = Int(appVersion!)
                                //                                if jsonData["ios_version"].int! > temp!{
                                
                                
                                //                                    let alert = UIAlertController(title: "New version available", message: "Please update the application", preferredStyle: .alert)
                                //
                                //                                    alert.addAction(UIAlertAction(title: "Update", style: .destructive, handler: { (acion) in
                                //                                        UIApplication.shared.openURL(NSURL(string: "https://apps.apple.com/us/app/familheey/id1485617876?ls=1") as! URL)
                                //
                                //                                    }))
                                //
                                //                                    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                                //
                                //                                    }))
                                //
                                //                                    self.window?.rootViewController?.present(alert, animated: false, completion: nil)
                                //                                }
                                //                                else{
                                
                                
                                //                                }
                            }
                        }
                        else if response.statusCode == 401{
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                self.getProfile()
                            }
                        }
                        else {
                            let alert = UIAlertController(title: "", message: "Opps! Something went wrong", preferredStyle: .alert)
                            
                            alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: { (acion) in
                                let domain = Bundle.main.bundleIdentifier!
                                UserDefaults.standard.removePersistentDomain(forName: domain)
                                UserDefaults.standard.synchronize()
                                
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let loginView = mainStoryBoard.instantiateViewController(withIdentifier: "FirstViewController") as! FirstViewController
                                
                                let navView = UINavigationController(rootViewController: loginView)
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = navView
                                
                                
                            }))
                            self.window?.rootViewController?.present(alert, animated: false, completion: nil)
                        }
                        
                    }catch let err {
                        ActivityIndicatorView.hiding()
                        // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                    }
                case .failure( let error):
                    ActivityIndicatorView.hiding()
                    //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                    break
                }
            }
        }
        
    }
    
    func checkNavigation(jsonData:JSON){
        let tempArr = jsonData["subscriptions"].arrayValue
        if tempArr.count > 0{
            self.subscriptionArr = [tempArr[0].dictionaryValue]
        }
        print(self.subscriptionArr)
        setUserDefaultValues.setUserDefaultValue(userDefaultValue: "0", userDefaultKey: "notification")
       
        UserDefaults.standard.set(jsonData["notification"].boolValue, forKey: "notification")
        UserDefaults.standard.synchronize()
        
        //        if !jsonData["is_verified"].boolValue{
        //            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        //            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "registrationView") as! ViewController
        //            let navView = UINavigationController(rootViewController: redViewController)
        //                       let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //                       appDelegate.window?.rootViewController = navView
        //        }
        
        /*else if !jsonData["is_active"].boolValue ||*/
        if jsonData["full_name"].stringValue.isEmpty || jsonData["email"].stringValue.isEmpty || jsonData["location"].stringValue.isEmpty || jsonData["origin"].stringValue.isEmpty   {
            let mainStoryBoard = UIStoryboard(name: "OnboardUser", bundle: nil)
            let loginView = mainStoryBoard.instantiateViewController(withIdentifier: "ProfileEditNewViewController") as! ProfileEditNewViewController
            if UserDefaults.standard.value(forKey: "userId") != nil{
                setupSocketForMessaging()
            }
            let navView = UINavigationController(rootViewController: loginView)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = navView
        }
        else{
            
            if jsonData["family_count"].intValue == 0{
                appDel.noFamily = true
            }
            else{
                appDel.noFamily = false
            }
            
            let isGuideShow = UserDefaults.standard.value(forKey: "showGuide") as? String ?? ""
            
            if isGuideShow == "1"{
                if jsonData["family_count"].intValue == 0{
                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                    setUserDefaultValues.setUserDefaultValue(userDefaultValue: "0", userDefaultKey: "isFirst_Time")
                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "HomeNavigation") as! HomeNavigation
                    self.noFamily = true
                    self.postcreatedInPublic = false
                    
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = redViewController
                }
                else{
                    setUserDefaultValues.setUserDefaultValue(userDefaultValue: "0", userDefaultKey: "isFirst_Time")
                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "HomeNavigation") as! HomeNavigation
                    self.noFamily = false
                    self.postcreatedInPublic = false
                    
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = redViewController
                }
            }
            else{
                setUserDefaultValues.setUserDefaultValue(userDefaultValue: "0", userDefaultKey: "isFirst_Time")
                let mainStoryBoard = UIStoryboard(name: "Subscription", bundle: nil)
                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "WalkthorughViewController") as! WalkthorughViewController
                self.noFamily = false
                self.postcreatedInPublic = false
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = redViewController
            }
        }
    }
    
        func getNotifications(){
            self.arrayOfNotificationList = []
            self.tempNotifi = []
            
            let userId = UserDefaults.standard.value(forKey: "userId") as! String
            var dbHandleQuery: DatabaseHandle?
            let noti_String :String = "\(Helpers.notificationUploadstring)\(userId)_notification"
            let ref = Database.database().reference(withPath: noti_String).queryLimited(toLast: 1200)
    //        dbHandleQuery = Database.database().reference(withPath: noti_String).queryLimited(toLast: <#T##UInt#>)
            ref.observeSingleEvent(of: .value, with: { snapshot in
                ActivityIndicatorView.hiding()

                if !snapshot.exists() {
                    
                    return
                }
                
                let dict:[String:Any] = snapshot.value as! [String:Any]
            //    print(dict)
                if dict.count != 0
                {
                for (key, value) in dict {
                    var dictionary = [String:Any]()
                    dictionary["key"] = key
                    dictionary["value"] = value as! [String:Any]
                    self.arrayOfNotificationList.append(dictionary)
                    self.tempNotifi.append(value as! [String:Any])
                }
                }
//                self.tempNotifi.sort{ ($0["create_time"] as! String) > ($1["create_time"] as! String) }

              
//                self.updateUI()
//                self.tblviewNotificationList.reloadData()
            })
        }
    
    func getNotificationWhenChildAdded(){
        let userId = UserDefaults.standard.value(forKey: "userId") as! String
        var dbHandleQuery: DatabaseHandle?
        let noti_String :String = "\(Helpers.notificationUploadstring)\(userId)_notification"
        let ref = Database.database().reference(withPath: noti_String).queryLimited(toLast: 1200)
        
        ref.observe(DataEventType.childAdded) { (snapshot) in
            if !snapshot.exists() {
                return
            }
            
            let dict:[String:Any] = snapshot.value as! [String:Any]
//            print(dict)
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: Notification.Name("ChildAdded"), object: nil)
            }
            
        }
    }
    
    func getAllKeys(){
        let param = [
            "user_id" : UserDefaults.standard.value(forKey: "userId") as! String
        ] as [String : Any]
        
        networkProvider.request(.GetKeys(parameter: param), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                do{
                    let jsonData =  JSON(response.data)
                    if response.statusCode == 200{
                        print(jsonData["s3_base_url"].stringValue)
                        if let s3url  = jsonData["s3_base_url"].string, !s3url.isEmpty{
                            self.s3BaseUrl = s3url
                        }
                        else{
                            
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getAllKeys()
                        }
                    }
                    else {
                        let alert = UIAlertController(title: "", message: "Opps! Something went wrong", preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: { (acion) in
                            let domain = Bundle.main.bundleIdentifier!
                            UserDefaults.standard.removePersistentDomain(forName: domain)
                            UserDefaults.standard.synchronize()
                            
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let loginView = mainStoryBoard.instantiateViewController(withIdentifier: "FirstViewController") as! FirstViewController
                            
                            let navView = UINavigationController(rootViewController: loginView)
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = navView
                            
                            
                        }))
                        self.window?.rootViewController?.present(alert, animated: false, completion: nil)
                    }

                }catch _ {
                    
                }
            case .failure(let _):
                break
                
            }
        })

    }

    
    func getUserUpdateDetails() {
        
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        print(appVersion!)
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        print(deviceID)
        
        let parameter = ["user_id":UserDefaults.standard.value(forKey: "userId") as! String,"login_type":"iOS","login_device":UIDevice.current.name,"app_version":appVersion!,"deviceID":deviceID,"login_location":self.currentLocation!]
        print(parameter)
        
        networkProvider.request(.addUserHistory(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    if response.statusCode == 200{
                        do {
                            if let json = try JSONSerialization.jsonObject(with: response.data, options: []) as? [String: Any] {
                                self.isUserHistoryAdded = true
                                
                            }
                        } catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                    else if response.statusCode == 401{
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getAllKeys()
                        }
                    }

                } catch let err {
                    
                    //                             Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let _):
                break
                
                //                         Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
        })
    }
    
    // MARK:- Location
    
    func callCurrentLocationForUserHistoryUpdate(){
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access location")
                self.callCurrentLocation()
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access location")
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
                
            }
        }
        else{
            self.callCurrentLocation()
            
        }
    }
    
    func callCurrentLocation(){
        let currentLocale = NSLocale.current as NSLocale
        let countryCode = currentLocale.object(forKey: .countryCode) as! String//get the set country name, code of your iphone
        let countryName = currentLocale.displayName(forKey: NSLocale.Key.countryCode, value: countryCode)
        print("from current location")
        print(countryName)
        
        self.currentLocation = "\(countryName)"
        self.getUserUpdateDetails()
    }
    
    //MARK: - location delegate methods
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        
        guard let location: CLLocation = manager.location else { return }
        fetchCityAndCountry(from: location) { city, country, error in
            guard let city = city, let country = country, error == nil else { return }
//            print("from corelocation")
//            print(city + ", " + country)
            
            
            
            if !self.getLocationInFirstTime
            {
                self.getLocationInFirstTime = true
                self.currentLocation = "\(city) , \(country)"
                self.locationManager.stopUpdatingLocation()
                self.getUserUpdateDetails()
            }
            
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }
    
    
    func fetchCityAndCountry(from location: CLLocation, completion: @escaping (_ city: String?, _ country:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            completion(placemarks?.first?.locality,
                       placemarks?.first?.country,
                       error)
            
        }
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "familheey")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    
    // creating fcm token
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        //Login check code
        if let _: Data = UserDefaults.standard.value(forKey: "userData") as? Data {
            Messaging.messaging().apnsToken = deviceToken
        }
        
    }
    
    // The callback to handle data message received via FCM for devices running iOS 10 or above.
    func applicationReceivedRemoteMessage(_ remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    
    func fetchAllVideos(){
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "mediaType = %d ", PHAssetMediaType.video.rawValue )
        let allVideo = PHAsset.fetchAssets(with: .video, options: fetchOptions)
        if allVideo.count > 0{
            self.phoneHasVideos = true
        }
    }
}



// MARK: firebase
extension AppDelegate: MessagingDelegate {
    
    //when refreshing token
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print(fcmToken)
        self.userToken = fcmToken
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print(fcmToken)
        self.userToken = fcmToken
    }
}


// MARK: Notification
@available(iOS 10.0, *)
extension AppDelegate: UNUserNotificationCenterDelegate {
    
    
    // While app is open
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        let userInfo = notification.request.content.userInfo
        print(userInfo)
        /*Jeena 24-02-2021 to change visible status when click on the push notf.*/
            let currentKey = (userInfo["notification_key"]) as! String
            let userId = UserDefaults.standard.value(forKey: "userId") as! String
            let noti_String :String = "\(Helpers.notificationUploadstring)\(userId)_notification"
            let ref = Database.database().reference(withPath: noti_String)
            ref.child(currentKey).child("visible_status").setValue("read")
        /* **********************  end  ********************/
        if "\(userInfo["type"] ?? "")".lowercased() == "chat_notification"{
            
            if let currentVC = UIApplication.topViewController(),let id = userInfo["type_id"] {
                
                if currentVC is ConversationsViewController   {
                    if "\(id)" == currentConversationId{
                        return
                    }
                }
            }
            completionHandler([.alert, .badge, .sound])
            
        }
        else if "\(userInfo["type"] ?? "")".lowercased() == "post" && "\(userInfo["sub_type"] ?? "")".lowercased() == "conversation"{
            if let currentVC = UIApplication.topViewController(),let id = userInfo["type_id"] {
                
                if currentVC is ConversationsViewController   {
                    if "\(id)" == currentConversationId{
                        return
                    }
                }
            }
            completionHandler([.alert, .badge, .sound])
        }
        else if "\(userInfo["type"] ?? "")".lowercased() == "home"{
            completionHandler([.alert, .badge, .sound])
        }
        
        //Commented by Giri for updating Socket push
        //
        //        print(userInfo)
        //        let notiInfo = userInfo as! [String:Any]
        //        print(notiInfo)
        //        let messDict = notiInfo["aps"] as! [String:Any]
        //        print(messDict)
        //        let finalDict = messDict["alert"] as! [String:Any]
        //              print(finalDict)
        
        /*
         
         let alert = UIAlertController(title: NSLocalizedString(finalDict["title"]as! String, comment: ""), message: (finalDict["body"]as! String), preferredStyle: UIAlertController.Style.alert)
         alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: { _ -> Void in
         
         //            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Action", bundle: nil)
         //            let nextViewController = mainStoryboard.instantiateViewController(withIdentifier: "ActionAlertViewController") as! ActionAlertViewController
         //            nextViewController.fromNoti = true
         //            let nextNavi = UINavigationController(rootViewController: nextViewController)
         //            nextNavi.isNavigationBarHidden = true
         //            self.window?.rootViewController = nextNavi
         }))
         self.window?.rootViewController?.present(alert, animated: true, completion: nil)
         */
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("!!!!Notification did receive")
        let userInfo = response.notification.request.content.userInfo
        // Print full message.
        print(userInfo)
        /*Jeena 25-02-2021 to change visible status when click on the push notf.*/
        //if "\(userInfo["type"] ?? "")".lowercased() != "event"{
            let currentKey = (userInfo["notification_key"]) as! String
            let userId = UserDefaults.standard.value(forKey: "userId") as! String
            let noti_String :String = "\(Helpers.notificationUploadstring)\(userId)_notification"
            let ref = Database.database().reference(withPath: noti_String)
            ref.child(currentKey).child("visible_status").setValue("read")
        //}
        /* **********************  end  ********************/
        if launchFromNotification {
            print("from push")
            self.launchFromNotification = false
            if let data = response.notification.request.content.userInfo  as? [String: Any]{
                self.notificationData = data
            }
        }
        else{
            if let data = response.notification.request.content.userInfo  as? [String: Any]{
                self.notificationData = data
            }
            
            if "\(userInfo["type"] ?? "")".lowercased() == "chat_notification"{
                
                
                if let Data = userInfo["result"] as? NSArray, let postData = Data[0] as? [String:Any]{
                    
                    let post = JSON.init(arrayLiteral: postData)
                    
                    if let currentVC = UIApplication.topViewController() {
                       
                        
                        let mainStoryBoard = UIStoryboard(name: "Conversations", bundle: nil)
                        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "ConversationsViewController") as! ConversationsViewController
                        vc.post = post[0]
                        vc.conversationT = .none
                        currentVC.navigationController?.pushViewController(vc, animated: true)
                        return
                    }
                    
                }
                
            }
            if let currentVC = UIApplication.topViewController() {
                
                if let data = response.notification.request.content.userInfo  as? [String: Any]{
                    self.notificationData = data
                    self.isFromNotification = true
                                        
                    let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
                    vc.arrayOfNotificationList = self.arrayOfNotificationList
                    currentVC.navigationController?.pushViewController(vc, animated: false)
                    
                }
                
            }
        }
        
        //TODO: -
        
        //        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Action", bundle: nil)
        //        let nextViewController = mainStoryboard.instantiateViewController(withIdentifier: "ActionAlertViewController") as! ActionAlertViewController
        //        nextViewController.fromNoti = true
        //        let nextNavi = UINavigationController(rootViewController: nextViewController)
        //        nextNavi.isNavigationBarHidden = true
        //        self.window?.rootViewController = nextNavi
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        
        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func application(
      _ application: UIApplication,
      didFailToRegisterForRemoteNotificationsWithError error: Error
    ) {
      print("Failed to register: \(error)")
    }
}

