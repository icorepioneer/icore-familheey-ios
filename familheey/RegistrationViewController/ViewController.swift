//
//  ViewController.swift
//  familheey
//
//  Created by familheey on 03/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import ActiveLabel
import SafariServices

class ViewController: UIViewController, popUpDelegate, UITextFieldDelegate{
    @IBOutlet weak var imgTick             : UIImageView!
    @IBOutlet weak var btnNext             : UIButton!
    @IBOutlet weak var txtCode             : UITextField!
    @IBOutlet weak var txtMobile           : UITextField!
    
    @IBOutlet weak var onCodeButt          : UIButton!
    @IBOutlet weak var codeUbderline       : UILabel!
    @IBOutlet weak var txtFieldUnderline   : UILabel!
    
    @IBOutlet weak var continueButt        : UIButton!
    
    var countryPhoneCode = ""
    
    let privacyURL = "https://familheey.com/policy"
    let TermsURL = "https://familheey.com/termsofservice"
    
    @IBOutlet weak var lblbottom: ActiveLabel!
    //    override func isBackBackButtonRequired() -> Bool {
//        return false
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imgTick.layer.borderWidth = 1
        imgTick.layer.borderColor = UIColor.lightGray.cgColor
        
        txtFieldUnderline.isHidden = false
        
        continueButt.cornerRadius = 13
        continueButt.clipsToBounds = true

        continueButt.backgroundColor = .lightGray
        continueButt.isEnabled = false
        

        let currentLocale = NSLocale.current as NSLocale
        let countryCode = currentLocale.object(forKey: .countryCode) as! String//get the set country name, code of your iphone
        let countryName = currentLocale.displayName(forKey: NSLocale.Key.countryCode, value: countryCode)
        
        print("country code is \(countryCode)")
        print(ViewController.getCountryCallingCode(countryRegionCode: countryCode))
        
        self.countryPhoneCode = ViewController.getCountryCallingCode(countryRegionCode: countryCode)
        
        if self.countryPhoneCode.count > 0{
            onCodeButt.setTitle(countryName, for: .normal)
            txtCode.text = ViewController.getCountryCallingCode(countryRegionCode: countryCode)
            txtFieldUnderline.isHidden = false
            txtMobile.becomeFirstResponder()
        }
        txtMobile.delegate = self
//        self.checkTxtField()
        
        setupActiveLabel()
        
    }
    func setupActiveLabel(){
        
        let customType = ActiveType.custom(pattern: "\\sterms and conditions\\b")
        let customType1 = ActiveType.custom(pattern: "\\sprivacy policy\\b")
        
        lblbottom.enabledTypes = [customType, customType1]
        lblbottom.text = "By tapping 'Continue', you agree to Familheey's terms and conditions and privacy policy"
        lblbottom.customColor[customType] = #colorLiteral(red: 0.4941176471, green: 0.3411764706, blue: 0.7607843137, alpha: 1)
        lblbottom.customColor[customType1] = #colorLiteral(red: 0.4941176471, green: 0.3411764706, blue: 0.7607843137, alpha: 1)
        lblbottom.customSelectedColor[customType] = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
        lblbottom.customSelectedColor[customType1] = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
        lblbottom.handleCustomTap(for: customType) { element in
//            let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
//            let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementDocPreviewViewController") as! AnnouncementDocPreviewViewController
//            vc.UrlToLoad = self.TermsURL
//            self.present(vc, animated: true, completion: nil)
            let TermUrl = URL(string: self.TermsURL)
            let vc = SFSafariViewController(url: TermUrl!)
            self.present(vc, animated: true, completion: nil)
        }
        lblbottom.handleCustomTap(for: customType1) { element in
//                let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
//                let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementDocPreviewViewController") as! AnnouncementDocPreviewViewController
//                vc.UrlToLoad = self.privacyURL
//                self.present(vc, animated: true, completion: nil)
            let privacyUrl = URL(string: self.privacyURL)
            let vc = SFSafariViewController(url: privacyUrl!)
            self.present(vc, animated: true, completion: nil)
        }
        
    }

    func alert(_ title: String, message: String) {
        let vc = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
           vc.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
           present(vc, animated: true, completion: nil)
       }
  
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
   static func getCountryCallingCode(countryRegionCode:String)->String{
        
        let prefixCodes = ["AF": "+93", "AE": "+971", "AL": "+355", "AN": "+599", "AS":"+1", "AD": "+376", "AO": "+244", "AI": "+1", "AG":"+1", "AR": "+54","AM": "+374", "AW": "+297", "AU":"+61", "AT": "43","AZ": "994", "BS": "1", "BH":"+973", "BF": "+226","BI": "+257", "BD": "+880", "BB": "+1", "BY": "+375", "BE":"+32","BZ": "+501", "BJ": "+229", "BM": "+1", "BT":"+975", "BA": "+387", "BW": "+267", "BR": "+55", "BG": "+359", "BO": "+591", "BL": "+590", "BN": "+673", "CC": "+61", "CD":"+243","CI": "+225", "KH":"+855", "CM": "+237", "CA": "+1", "CV": "+238", "KY":"+345", "CF":"+236", "CH": "+41", "CL": "+56", "CN":"+86","CX": "+61", "CO": "+57", "KM": "+269", "CG":"+242", "CK": "+682", "CR": "+506", "CU":"+53", "+CY":"+537","CZ": "+420", "DE": "+49", "DK": "+45", "DJ":"+253", "DM": "+1", "DO": "+1", "DZ": "+213", "EC": "+593", "EG":"+20", "ER": "+291", "EE":"+372","ES": "+34", "ET": "+251", "FM": "+691", "FK": "+500", "FO": "+298", "FJ": "+679", "FI":"+358", "FR": "+33", "GB":"+44", "GF": "+594", "GA":"+241", "GS": "+500", "GM":"+220", "GE":"+995","GH":"+233", "GI": "+350", "GQ": "+240", "GR": "+30", "GG": "+44", "GL": "+299", "GD":"+1", "GP": "+590", "GU": "+1", "GT": "+502", "GN":"+224","GW": "+245", "GY": "+592", "HT": "+509", "HR": "+385", "HN":"+504", "HU": "+36", "HK": "+852", "IR": "+98", "IM": "+44", "IL": "+972", "IO":"+246", "IS": "+354", "IN": "+91", "ID":"+62", "IQ":"+964", "IE": "+353","IT":"+39", "JM":"+1", "JP": "+81", "JO": "+962", "JE":"+44", "KP": "+850", "KR": "+82","KZ":"+77", "KE": "+254", "KI": "+686", "KW": "+965", "KG":"+996","KN":"+1", "LC": "+1", "LV": "+371", "LB": "+961", "LK":"+94", "LS": "+266", "LR":"+231", "LI": "+423", "LT": "+370", "LU": "+352", "LA": "+856", "LY":"+218", "MO": "+853", "MK": "+389", "MG":"+261", "MW": "+265", "MY": "+60","MV": "+960", "ML":"+223", "MT": "+356", "MH": "+692", "MQ": "+596", "MR":"+222", "MU": "+230", "MX": "+52","MC": "+377", "MN": "+976", "ME": "+382", "MP": "+1", "MS": "+1", "MA":"+212", "MM": "+95", "MF": "+590", "MD":"+373", "MZ": "+258", "NA":"+264", "NR":"+674", "NP":"+977", "NL": "+31","NC": "+687", "NZ":"+64", "NI": "+505", "NE": "+227", "NG": "+234", "NU":"+683", "NF": "+672", "NO": "+47","OM": "+968", "PK": "+92", "PM": "+508", "PW": "+680", "PF": "+689", "PA": "+507", "PG":"+675", "PY": "+595", "PE": "+51", "PH": "+63", "PL":"+48", "PN": "+872","PT": "+351", "PR": "+1","PS": "+970", "QA": "+974", "RO":"+40", "RE":"+262", "RS": "+381", "RU": "+7", "RW": "+250", "SM": "+378", "SA":"+966", "SN": "+221", "SC": "+248", "SL":"+232","SG": "+65", "SK": "+421", "SI": "+386", "SB":"+677", "SH": "+290", "SD": "+249", "SR": "+597","SZ": "+268", "SE":"+46", "SV": "+503", "ST": "+239","SO": "+252", "SJ": "+47", "SY":"+963", "TW": "+886", "TZ": "+255", "TL": "+670", "TD": "+235", "TJ": "+992", "TH": "+66", "TG":"+228", "TK": "+690", "TO": "+676", "TT": "+1", "TN":"+216","TR": "+90", "TM": "+993", "TC": "+1", "TV":"+688", "UG": "+256", "UA": "+380", "US": "+1", "UY": "+598","UZ": "+998", "VA":"+379", "VE":"+58", "VN": "+84", "VG": "+1", "VI": "+1","VC":"+1", "VU":"+678", "WS": "+685", "WF": "+681", "YE": "+967", "YT": "+262","ZA": "+27" , "ZM": "+260", "ZW":"+263"]
        let countryDialingCode = prefixCodes[countryRegionCode]
        return countryDialingCode!
        
    }
    
   /* override func onScreenLoad(){
        print("viewcontroller")
    
        imgTick.layer.borderWidth = 1
        imgTick.layer.borderColor = UIColor.lightGray.cgColor
        
    }*/
    
    //MARK: - Text field Delegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {

            self.checkTxtField()
        })
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func checkTxtField(){
        
        if txtMobile.text?.count == 0{
            
            txtFieldUnderline.isHidden = false
            continueButt.isEnabled = false
            continueButt.backgroundColor = .lightGray
        }else{
            
            txtFieldUnderline.isHidden = false
            continueButt.isEnabled = true
            continueButt.backgroundColor = #colorLiteral(red: 0.4941176471, green: 0.3411764706, blue: 0.7607843137, alpha: 1)
        }
    }
    
    //MARK:- Protocol PopupDelegate
    func selectCountry(id: String, name: String) {
        print(id)
        txtCode.text = id
        onCodeButt.setTitle(name, for: .normal)
        
        txtFieldUnderline.isHidden = false
        self.txtMobile.becomeFirstResponder()
        self.txtMobile.becomeFirstResponder()

//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
//
//        })
    }

    
    //MARK:- Button Actions
    
    @IBAction func onClickPrivacy(_ sender: Any) {
        if imgTick.image == nil{
            imgTick.image = UIImage(named: "imgTick_gray")
        }
        else{
            imgTick.image = UIImage(named: "")
        }
    }
    @IBAction func onClickCode(_ sender: Any) {
        
        self.view.endEditing(true)
        
        let popUp = popupTableViewController()
        self.addChild(popUp)
       // popUp.frmGndrFlag = fromGndr
        popUp.view.frame = UIScreen.main.bounds
        self.view.addSubview(popUp.view)
        popUp.delegate = self 
        self.view.bringSubviewToFront(popUp.view)
    }
    
    @IBAction func onClickNext(_ sender: Any) {
        
        
//        let otpView = self.storyboard?.instantiateViewController(withIdentifier: "OTPView") as! OTPViewController
//        otpView.phone = "\(self.txtCode.text!)\(self.txtMobile.text!)"
//        self.navigationController?.pushViewController(otpView, animated: true)
//
//        return
        
//        if !Helpers.validatePHnumber(enteredNumber: txtMobile.text!){
//
//            Helpers.showAlertDialog(message: "Please enter a valid mobile number", target: self)
//
//            return
//        }
       
          if txtCode.text?.isEmpty != true && txtMobile.text?.isEmpty != true{
            
            
            self.view.endEditing(true)
            
               APIServiceManager.callServer.registerUser(url: EndPoint.signup, code: txtCode.text!, phone: txtMobile.text!, success: { (responseMdl) in
                    ActivityIndicatorView.hiding()
                    print(responseMdl)
                    
                    guard let loginMdl = responseMdl as? userModel else{
                        return
                    }
                    
                    if loginMdl.status == 200{
                        print(responseMdl!)
                        let temp = String(loginMdl.id)
                        
                       setUserDefaultValues.setUserDefaultValue(userDefaultValue: loginMdl.fullname, userDefaultKey: "user_fullname")
                        setUserDefaultValues.setUserDefaultValue(userDefaultValue: temp, userDefaultKey: "userId")
                        setUserDefaultValues.setUserDefaultValue(userDefaultValue: loginMdl.phone, userDefaultKey: "userPhone")
                        
                        
                        let otpView = self.storyboard?.instantiateViewController(withIdentifier: "OTPView") as! OTPViewController
                        otpView.phone = "\(self.txtCode.text!)\(self.txtMobile.text!)"
                        self.navigationController?.pushViewController(otpView, animated: true)
                    }
                    else if loginMdl.status == 500{
                        let strArr = loginMdl.signupMsg.components(separatedBy: "//")
                        let strMsg = strArr.first
                        if strMsg == "User already exists in db"{
                            let otpView = self.storyboard?.instantiateViewController(withIdentifier: "OTPView") as! OTPViewController
                            otpView.phone = "\(self.txtCode.text!)\(self.txtMobile.text!)"
                            self.navigationController?.pushViewController(otpView, animated: true)
                        }else{
                            self.displayAlert(alertStr: "Your number is already registered with us!", title: "")
                        }
                    }
                    
                }) { (error) in
                    self.displayAlert(alertStr:"Something went wrong",title:"")
                }
            }
          else{
            self.displayAlert(alertStr:"All fileds are mandatory",title:"")
            }
        
    }
    
}

