//
//  SearchTableViewController.swift
//  familheey
//
//  Created by Giri on 23/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Firebase

class SearchTableViewController: UITableViewController,UICollectionViewDelegate,UICollectionViewDataSource,UITextFieldDelegate,UICollectionViewDelegateFlowLayout {
    
    var arrayOfNotificationList = [[String:Any]]()
    var ref: DatabaseReference!
    var notiUnreadCount:Int = 0

    @IBOutlet weak var lblNoti_Count: UILabel!
    @IBOutlet weak var viewOfNoti_Count: UIView!
    @IBOutlet weak var imgOfNoti_Bell: UIImageView!
    let titleArray = ["FAMILY","PEOPLE"]
    var selectedIndex = 1000 //0 - Family, 1 - People, 2 - Events, 3 - Post
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var collectionViewTitle: UICollectionView!
    @IBOutlet weak var txtSearch: UITextField!
    var searchTxt = ""
    var usersList = [SearchResultUser]()
    var FamilyList = [SearchResultGroup]()
    var EventList = [SearchResultsEvents]()
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnSearchReset: UIButton!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionViewTitle.delegate = self
        collectionViewTitle.dataSource = self
       // tableView.tableHeaderView = UIView.init()
        tableView.tableFooterView = UIView.init()
        //        self.navigationController?.title = "Search"
        self.navigationController?.navigationBar.topItem?.title = "Search"
        addRightNavigationButton()
        onClickRight()
        txtSearch.delegate = self
        Helpers.setleftView(textfield: txtSearch, customWidth: 30)
        self.navigationController?.navigationBar.isHidden = true
//        self.arrayOfNotificationList =  appDel.arrayOfNotificationList
//
//        if self.arrayOfNotificationList.count == 0
//        {
//            self.lblNoti_Count.isHidden = true
//            self.viewOfNoti_Count.isHidden = true
//        }
//        else
//        {
//
//            if appDel.firstTimeNot == false
//            {
//                self.lblNoti_Count.isHidden = false
//                self.viewOfNoti_Count.isHidden = false
//                self.lblNoti_Count.text = "\(appDel.notiUnreadCount)"
//            }
//            else
//            {
//                self.lblNoti_Count.isHidden = true
//                self.viewOfNoti_Count.isHidden = true
//            }
//        }
       
    }

    func NotificationArrayListing()
    {
        
        self.arrayOfNotificationList = []
        let userId = UserDefaults.standard.value(forKey: "userId") as! String
        
        let noti_String :String = "\(Helpers.notificationUploadstring)\(userId)_notification"
        //        let noti_String :String = "\(userId)_notification"
        let ref = Database.database().reference(withPath: noti_String).queryLimited(toLast: 1200)
        ref.observeSingleEvent(of: .value, with: { snapshot in
            
            if !snapshot.exists() {
                self.arrayOfNotificationList = []
                appDel.arrayOfNotificationList = []
                appDel.tempNotifi = []
                return
                
            }
            
        //    print(snapshot) // Its print all values including Snap (User)
            
            let dict:[String:Any] = snapshot.value as! [String:Any]
         //   print(dict)
            
            for (key, value) in dict {
                var dictionary = [String:Any]()
                dictionary["key"] = key
                dictionary["value"] = value as! [String:Any]
                self.arrayOfNotificationList.append(dictionary)
            }
          //  print(self.arrayOfNotificationList)
            //            appDel.arrayOfNotificationList = self.arrayOfNotificationList
            
            
         //   print(self.arrayOfNotificationList)
          //  print(self.arrayOfNotificationList.count)
            self.notiUnreadCount = 0
            for data in self.arrayOfNotificationList
            {
                let dict  = data["value"] as! [String:Any]
                if dict["visible_status"] as! String == "unread"
                {
                    self.notiUnreadCount = self.notiUnreadCount + 1
                }
            }
          //  print(self.notiUnreadCount)
            
            //            appDel.notiUnreadCount = self.notiUnreadCount
            //            appDel.arrayOfNotificationList = self.arrayOfNotificationList
            
            if self.notiUnreadCount == 0
            {
                self.lblNoti_Count.isHidden = true
                self.viewOfNoti_Count.isHidden = true
            }
            else
            {
                
                self.lblNoti_Count.isHidden = false
                self.viewOfNoti_Count.isHidden = false
                if self.notiUnreadCount > 99{
                    self.lblNoti_Count.text = "99+"
                }
                else{
                    self.lblNoti_Count.text = "\(self.notiUnreadCount)"
                }
                
            }
            
            
            
        })
        
        
    }
    @IBAction func btnNotificationOnClick(_ sender: Any) {
       // appDel.firstTimeNot = true
        self.lblNoti_Count.isHidden = true
        self.viewOfNoti_Count.isHidden = true
        let storyboard = UIStoryboard.init(name: "third", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        vc.arrayOfNotificationList = self.arrayOfNotificationList
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func feedBackClicked(_ sender: Any) {
        let postoptionsTittle =  ["Calendar","Announcements","Feedback"]
        showActionSheet(titleArr: postoptionsTittle as NSArray, title: "Select options") { (index) in
            if index == 0{
                self.viewCalendar()

            }
            else if index == 1{
                self.viewAnnouncements()
            }
            else if index == 100{
            }
            else{
                self.sendFeedBack()

            }
        }
    }
    
    func sendFeedBack(){
        let stryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = stryboard.instantiateViewController(withIdentifier: "FeedbackViewController") as! FeedbackViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func viewAnnouncements(){
        let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsViewController") as! AnnouncementsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func viewCalendar(){
        let stryboard = UIStoryboard.init(name: "third", bundle: nil)
        let vc = stryboard.instantiateViewController(withIdentifier: "CalendarEventsViewController") as! CalendarEventsViewController
        vc.isFromExplore = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //Show the navigation bar
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        // Hide the navigation bar
        self.lblNoti_Count.isHidden = true
        self.viewOfNoti_Count.isHidden = true
        self.NotificationArrayListing()
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.tabBarController?.tabBar.isHidden = false

        self.callSearchApi()
        
        if UserDefaults.standard.value(forKey: "userProfile") != nil{
            let pic = UserDefaults.standard.value(forKey: "userProfile") as! String
            let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+pic
            let imgUrl = URL(string: temp)
            let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=50&height=50&url="+imgUrl!.relativeString
            let url = URL(string: newUrlStr)
            imgProfile.kf.indicatorType = .activity

            imgProfile.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        else{
            imgProfile.image = #imageLiteral(resourceName: "Male Colored")
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
       // self.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK:- Right navigation button
    
    func addRightNavigationButton(){
        
        let rightButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(onClickrightButtonAction))
        self.navigationItem.rightBarButtonItem = rightButtonItem
        
    }
    
    //MARK:- custom
    func onClickRight(){
      //  self.tableView.tableHeaderView = searchView
        selectedIndex = 0
        collectionViewTitle.reloadData()
        tableView.reloadData()
        if let button = self.navigationItem.rightBarButtonItem {
            button.isEnabled = false
            button.tintColor = UIColor.clear
        }
        txtSearch.text = ""
        
       // txtSearch.becomeFirstResponder()
    }
    
    //MARK:- Button Actions
    @objc func onClickrightButtonAction(sender: UIBarButtonItem){
        self.tableView.tableHeaderView = searchView
        selectedIndex = 0
        collectionViewTitle.reloadData()
        tableView.reloadData()
        if let button = self.navigationItem.rightBarButtonItem {
            button.isEnabled = false
            button.tintColor = UIColor.clear
        }
        txtSearch.becomeFirstResponder()
    }
    @IBAction func onClickMemberAction(_ sender: UIButton) {
        switch selectedIndex {
        case 0://Family
            
            if let id = FamilyList[sender.tag].group_id{
            ActivityIndicatorView.show("Please wait!")
                APIServiceManager.callServer.joinFamilyRequest(url: EndPoint.joinFamily, userId:  UserDefaults.standard.value(forKey: "userId") as! String, groupId: "\(id)", success: { (response) in
                    ActivityIndicatorView.hiding()
                    var fam = self.FamilyList[sender.tag]
                    
                    if fam.member_joining == 3{
                        fam.status = "accepeted"
                        fam.is_joined = "1"
                    }
                    else{
                       fam.status = "pending"
                    }
                    
                    self.FamilyList[sender.tag] = fam
                    self.tableView.beginUpdates()
                    self.tableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .none)
                    self.tableView.endUpdates()
                    
                }) { (error) in
                    ActivityIndicatorView.hiding()

                }
            }
        case 1://Users
            
            if let id = usersList[sender.tag].userid {
          //  print(id)
                        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            let popOverVc = storyboard.instantiateViewController(withIdentifier: "SelectFamilyViewController") as! SelectFamilyViewController
            popOverVc.selectedUserID = "\(id)"
                self.navigationController?.pushViewController(popOverVc, animated: true)
//                popOverVc.modalTransitionStyle = .crossDissolve
//                popOverVc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//                self.present(popOverVc, animated: true)
                
//            let nav = UINavigationController(rootViewController: popoverContent)
//            nav.modalPresentationStyle = UIModalPresentationStyle.popover
//            let popover = nav.popoverPresentationController
////            popoverContent.preferredContentSize = CGSizeMake(500,600)
////            popover?.delegate = self
//            popover?.sourceView = self.view
////            popover?.sourceRect = CGRectMake(100,100,0,0)
//
//            self.present(nav, animated: true, completion: nil)
            }
            
        default:
            break
        }
    }
    
    @IBAction func onClickProfilePicClicked(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "UserProfileTableViewController") as! UserProfileTableViewController
        //vc.mutialFamilies =  "\(usersList[indexPath.row].familycount ?? "0")"
        //  vc.mutualConnections =  "\(usersList[indexPath.row].mutualfamilycount ?? "0")"
        var userId = UserDefaults.standard.value(forKey: "userId") as! String
        vc.userID = userId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickSearchReset(_ sender: Any) {
        txtSearch.text = ""
        searchTxt = ""
        callSearchApi()
        btnSearchReset.isHidden = true
        self.txtSearch.endEditing(true)
    }
    
    //MARK:- Custom functions
    func callSearchApi(){
        ActivityIndicatorView.show("Please wait!")
        let param = [
             "userid" : UserDefaults.standard.value(forKey: "userId") as! String,
             "searchtxt" : txtSearch.text!,
             "offset":"0",
             "limit":"100",
             "phonenumbers":[],
             "type":""
             ] as [String : Any]
        
        APIServiceManager.callServer.searchRequest(url: EndPoint.search, param: param, fromSearchTabViewController: false, success: { (response) in
            
            ActivityIndicatorView.hiding()
            self.usersList = [SearchResultUser]()
            self.FamilyList = [SearchResultGroup]()
            self.EventList = [SearchResultsEvents]()
            
            if let results = response as! SearchList?{
                self.usersList = results.searchResult!
                self.FamilyList = results.searchResult2!
              //  self.EventList = results.searchResult3!
            }
            self.tableView.reloadData()
            
        }) { (error) in
            ActivityIndicatorView.hiding()
            self.usersList = [SearchResultUser]()
            self.FamilyList = [SearchResultGroup]()
            
            self.tableView.reloadData()
        }
    }
    //MARK:- Textfield Delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        if textField == txtSearch
        {
            // selectWebAPI()
            if textField.text!.count > 0{
                btnSearchReset.isHidden = false
            }
            else{
                btnSearchReset.isHidden = true
            }
            textField.endEditing(true)
        }
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        self.view.endEditing(true)
        if textField == txtSearch
        {
            if textField.text!.count > 0{
                btnSearchReset.isHidden = false
            }
            else{
                btnSearchReset.isHidden = true
            }
            callSearchApi()
        }
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
        }
        return  true
    }
    
    
    //MARK:- collectionView Delegates
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return titleArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchTitlesCollectionViewCell", for: indexPath as IndexPath) as! SearchTitlesCollectionViewCell
        
        cell.lblTitle.text = titleArray[indexPath.item]
        
        if indexPath.item == selectedIndex{
            cell.viewSelection.isHidden = false
           // cell.viewSelection.backgroundColor = UIColor(named: "greenBackground")
            cell.lblTitle.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
          //  cell.lblTitle.font      = UIFont.boldSystemFont(ofSize: 16)
            
        }
        else{
            cell.viewSelection.isHidden = true
            cell.lblTitle.textColor = .black
          //  cell.lblTitle.font      = UIFont.systemFont(ofSize: 14)
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if indexPath.item > 2 {
//            return
//        }
        selectedIndex = indexPath.item
        collectionView.reloadData()
        tableView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberOfCellInRow  = self.titleArray.count
        let padding : Int      = 10
        let collectionCellWidth : CGFloat = (self.collectionViewTitle.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
        return CGSize(width: collectionCellWidth, height: 60)
    }
    
    //MARK: - Tableview Delegates
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        
        switch selectedIndex {
        case 0:
            
            if FamilyList.count == 0{
                TableViewHelper.EmptyMessage(message: "No results in Family!", viewController: self)
                return 0
            }
            else{
                tableView.backgroundView = nil
                return 1
            }
            
        case 1:
            
            if usersList.count == 0{
                TableViewHelper.EmptyMessage(message: "No results in Users!", viewController: self)
                return 0
            }
            else{
                tableView.backgroundView = nil
                return 1
            }
        case 2:
            if EventList.count == 0{
                TableViewHelper.EmptyMessage(message: "No results in Users!", viewController: self)
                return 0
            }
            else{
                tableView.backgroundView = nil
                return 1
            }
            
        default:
            TableViewHelper.EmptyMessage(message: "Tap on The find icon to start finding!", viewController: self)
            return 0
        }
        
        //        //        if projects.count > 0 {
        //        return 1
        //        //        } else {
        //        //            TableViewHelper.EmptyMessage(message: "Tap on The find icon to start finding!", viewController: self)
        //        //            return 0
        //        //        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch selectedIndex {
        case 0:
            return FamilyList.count
            
        case 1:
            return usersList.count
        case 2:
            return EventList.count
            
        default:
            return 0
        }
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch selectedIndex {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchResultTableViewCell", for: indexPath) as! SearchResultTableViewCell
            cell.btnRightDetail.tag = indexPath.row
            cell.lblName.text = FamilyList[indexPath.row].group_name
            cell.lblDesc.isHidden = false
            cell.lblType.isHidden = false
            cell.lblBy.isHidden = false
            cell.type_hight.constant = 20
            cell.desc_height.constant = 20
            cell.lblType.text = FamilyList[indexPath.row].createdBy
            cell.lblDesc.text = FamilyList[indexPath.row].group_category
            
            if FamilyList[indexPath.row].logo.count != 0 {
                let temp = "\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+FamilyList[indexPath.row].logo
                let imgUrl = URL(string: temp)
                let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+imgUrl!.relativeString
                let url = URL(string: newUrlStr)
                cell.imageViewAvatar.backgroundColor = .clear
                cell.imageViewAvatar.kf.indicatorType = .activity

                cell.imageViewAvatar.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
            }
            else{
                
                cell.imageViewAvatar.image = #imageLiteral(resourceName: "Family Logo")
            }
            
            if let orign = FamilyList[indexPath.row].base_region{
                if orign == ""{
                    cell.lblLocation.text = "Unknown Origin"
                }
                else{
                    cell.lblLocation.text = FamilyList[indexPath.row].base_region
                }
            }
            if let count = FamilyList[indexPath.row].membercount{
                if count == "" {
                    cell.lblCountLeftDetails.text = "0"
                    cell.lblCountLeftDetails.isHidden = true
                    cell.titleLeftDetails.isHidden = true
                }
                else if count == "0"{
                    cell.lblCountLeftDetails.text = "0"
                    cell.lblCountLeftDetails.isHidden = true
                    cell.titleLeftDetails.isHidden = true
                }
                else{
                    cell.lblCountLeftDetails.text = count
                    cell.lblCountLeftDetails.isHidden = false
                    cell.titleLeftDetails.isHidden = false
                }
            }
            else{
                cell.lblCountLeftDetails.text = "0"
                cell.lblCountLeftDetails.isHidden = true
                cell.titleLeftDetails.isHidden = true
            }
            if let countKnown = FamilyList[indexPath.row].knowncount{
                if countKnown == "" {
                    cell.countRightDetails.text = "0"
                    cell.countRightDetails.isHidden = true
                    cell.titleRightDetails.isHidden = true
                }
                else if countKnown == "0"{
                    cell.countRightDetails.text = "0"
                    cell.countRightDetails.isHidden = true
                    cell.titleRightDetails.isHidden = true
                }
                else{
                    cell.countRightDetails.text = countKnown
                    cell.countRightDetails.isHidden = false
                    cell.titleRightDetails.isHidden = false
                }
            }
            else{
                cell.countRightDetails.text = "0"
                cell.countRightDetails.isHidden = true
                cell.titleRightDetails.isHidden = true
            }
            cell.titleLeftDetails.text = "Members"
            cell.titleRightDetails.text = "Known"
            
            //            if let type =  FamilyList[indexPath.row].group_type{
            //                if type == "Public"{
            //                    cell.lblRightButtonTitle.text = "JOIN"
            //                }
            //                else{
            //                    cell.lblRightButtonTitle.text = "REQUEST"
            //                }
            //            }
            //            else{
            //                cell.lblRightButtonTitle.text = "REQUEST"
            //            }
            
            let joinStatus = FamilyList[indexPath.row].is_joined
            let member_joining = FamilyList[indexPath.row].member_joining
            
            
            if  let Status = FamilyList[indexPath.row].status {
                
                if joinStatus == "1"{
                    if FamilyList[indexPath.row].isRemoved == 1{
                        cell.lblRightButtonTitle.text = "JOIN"
                        cell.btnRightDetail.isUserInteractionEnabled  = true
                    }
                    else{
                        cell.lblRightButtonTitle.text = "MEMBER"
                        cell.btnRightDetail.isUserInteractionEnabled  = false
                    }
                    
                }
                else if member_joining == 1{
                    cell.lblRightButtonTitle.text = "PRIVATE"
                    cell.btnRightDetail.isUserInteractionEnabled  = false
                }
                else if Status.lowercased() == "pending"{
                    cell.lblRightButtonTitle.text = "PENDING"
                    cell.btnRightDetail.isUserInteractionEnabled  = false
                }
                else if Status.lowercased() == "rejected"{
                    cell.lblRightButtonTitle.text = "REJECTED"
                    cell.btnRightDetail.isUserInteractionEnabled  = false
                }
                else{
                    cell.lblRightButtonTitle.text = "JOIN"
                    cell.btnRightDetail.isUserInteractionEnabled  = true
                }
                
            }
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchResultTableViewCell", for: indexPath) as! SearchResultTableViewCell
            cell.btnRightDetail.isUserInteractionEnabled  = true
            cell.btnRightDetail.tag = indexPath.row
            cell.lblName.text = usersList[indexPath.row].full_name
            cell.lblDesc.isHidden = true
            cell.lblType.isHidden = true
            cell.lblBy.isHidden = true
            cell.imageViewAvatar.backgroundColor = .clear
            cell.type_hight.constant = 0
            cell.desc_height.constant = 0
            if usersList[indexPath.row].propic?.count != 0{
                let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+usersList[indexPath.row].propic!
                let imgUrl = URL(string: temp)
                
                let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+imgUrl!.relativeString
                let url = URL(string: newUrlStr)
                cell.imageViewAvatar.kf.indicatorType = .activity

                cell.imageViewAvatar.kf.setImage(with: url, placeholder:#imageLiteral(resourceName: "Male Colored") , options: nil, progressBlock: nil, completionHandler: nil)
            }else{
                cell.imageViewAvatar.image = #imageLiteral(resourceName: "Male Colored")
            }
            
            if let origin = usersList[indexPath.row].origin{
                if origin == ""{
                    cell.lblLocation.text = "Unknown Location"
                }
                else{
                    cell.lblLocation.text = usersList[indexPath.row].origin
                }
            }
            if let count = usersList[indexPath.row].familycount{
                if count == "" {
                    cell.lblCountLeftDetails.text = "0"
                    cell.lblCountLeftDetails.isHidden = true
                    cell.titleLeftDetails.isHidden = true
                }
                else if count == "0"{
                    cell.lblCountLeftDetails.text = "0"
                    cell.lblCountLeftDetails.isHidden = true
                    cell.titleLeftDetails.isHidden = true
                }
                else{
                  //  print(count)
                    cell.lblCountLeftDetails.text = count
                    cell.lblCountLeftDetails.isHidden = false
                    cell.titleLeftDetails.isHidden = false
                }
            }
            else{
                cell.lblCountLeftDetails.text = "0"
                cell.lblCountLeftDetails.isHidden = true
                cell.titleLeftDetails.isHidden = true
            }
            //cell.lblCountLeftDetails.text = "\(usersList[indexPath.row].faCount)"
            
            if let countKnown = usersList[indexPath.row].mutualfamilycount{
                if countKnown == "" {
                    cell.countRightDetails.text = "0"
                    cell.countRightDetails.isHidden = true
                    cell.titleRightDetails.isHidden = true
                }
                else if countKnown == "0"{
                    cell.countRightDetails.text = "0"
                    cell.countRightDetails.isHidden = true
                    cell.titleRightDetails.isHidden = true
                }
                else{
                    cell.countRightDetails.text = countKnown
                    cell.countRightDetails.isHidden = false
                    cell.titleRightDetails.isHidden = false
                }
            }
            else{
                cell.countRightDetails.text = "0"
                cell.countRightDetails.isHidden = true
                cell.titleRightDetails.isHidden = true
            }
            
            cell.titleLeftDetails.text = "Families"
            cell.titleRightDetails.text = "Known Families"
            
            cell.lblRightButtonTitle.text = "Add to Family"
            if appDelegate.noFamily {
                cell.rightButtonView.isHidden = true
            }
            else{
                cell.rightButtonView.isHidden = false
            }
            return cell
            
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchResultTableViewCell", for: indexPath) as! SearchResultTableViewCell
            
            cell.familyView.isHidden = true
            cell.btnView.isHidden = true
            cell.conectnView.isHidden = true
            cell.lblDesc.isHidden = false

            cell.btnRightDetail.isUserInteractionEnabled  = true
            cell.btnRightDetail.tag = indexPath.row
            cell.lblName.text = EventList[indexPath.row].eventName
            cell.lblLocation.text = EventList[indexPath.row].location
            cell.lblDesc.text = "created by, \(EventList[indexPath.row].created)"
            cell.imageViewAvatar.backgroundColor = .clear
            if EventList[indexPath.row].propic.count != 0{
                let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.groupCover)"+EventList[indexPath.row].propic
                let imgUrl = URL(string: temp)
                cell.imageViewAvatar.kf.indicatorType = .activity

                cell.imageViewAvatar.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
            }
            else{
               cell.imageViewAvatar.image = #imageLiteral(resourceName: "Male Colored")
            }
            
            
            return cell
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchResultTableViewCell", for: indexPath) as! SearchResultTableViewCell
            return cell
        }
        
    }
//    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//       
//            return 200
//      
//        
//    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectedIndex == 0{
            return 170
        }
        return 145
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch selectedIndex {
        case 0:
           // tableView.deselectRow(at: indexPath, animated: true)
            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
            let gropuId = "\(String(describing: FamilyList[indexPath.row].group_id!))"
            vc.groupId = gropuId
            self.navigationController?.pushViewController(vc, animated: true)
            break
            //return
        case 1:
            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "UserProfileTableViewController") as! UserProfileTableViewController
            vc.mutialFamilies =  "\(usersList[indexPath.row].familycount ?? "0")"
            vc.mutualConnections =  "\(usersList[indexPath.row].mutualfamilycount ?? "0")"
            vc.userID = "\( usersList[indexPath.row].userid ?? 0)"
            self.navigationController?.pushViewController(vc, animated: true)
            break
            
        case 2:
            let stryboard = UIStoryboard.init(name: "third", bundle: nil)
            let vc = stryboard.instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController

           // vc.eventDetails = data
            let temp = "\(EventList[indexPath.row].id)"
            vc.eventId = temp as! String
            vc.fromCreate = false
            self.navigationController?.pushViewController(vc, animated: true)
            break
        default:
            return
        }
        
    }
    
}
