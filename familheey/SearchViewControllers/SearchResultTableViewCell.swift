//
//  SearchResultTableViewCell.swift
//  familheey
//
//  Created by Giri on 23/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import ActiveLabel

class SearchResultTableViewCell: UITableViewCell {
    @IBOutlet weak var imageViewAvatar: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblRightButtonTitle: UILabel!
    @IBOutlet weak var lblCountLeftDetails: UILabel!
    @IBOutlet weak var titleLeftDetails: UILabel!
    @IBOutlet weak var countRightDetails: UILabel!
    @IBOutlet weak var titleRightDetails: UILabel!
    @IBOutlet weak var btnRightDetail: UIButton!
    @IBOutlet weak var familyView: UIView!
    @IBOutlet weak var btnView: UIView!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var type_hight: NSLayoutConstraint!
    @IBOutlet weak var desc_height: NSLayoutConstraint!
    @IBOutlet weak var lblBy: UILabel!
    @IBOutlet weak var rightButtonView: UIView!
    
    @IBOutlet weak var btnTopicStart: UIButton!
    @IBOutlet weak var chatView: UIView!
    @IBOutlet weak var createdByView: UIView!
    @IBOutlet weak var descView: UIView!
    @IBOutlet weak var conectnView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class searchResultNewTablleViewCell: UITableViewCell{
    
    @IBOutlet weak var imgFamilyLogo: UIImageView!
    @IBOutlet weak var lblCreatedBy: UILabel!
    @IBOutlet weak var lblFamilyName: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var membersView: UIView!
    @IBOutlet weak var postView: UIView!
    @IBOutlet weak var lblMembersCount: UILabel!
    @IBOutlet weak var lblPostCount: UILabel!
    
    @IBOutlet weak var btnJoin: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

class searchResultNewPeopleTableViewCell: UITableViewCell{
 
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPlace: UILabel!
    @IBOutlet weak var lblFamilyCount: UILabel!
    @IBOutlet weak var btnChat: UIButton!
    @IBOutlet weak var btnAddtoFamily: UIButton!
    @IBOutlet weak var familyCountView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}

class searchResultNewEventTableViewCell: UITableViewCell{
    @IBOutlet weak var imgEventCover: UIImageView!
    
    @IBOutlet weak var lblEventNAme: UILabel!
    @IBOutlet weak var lblEventDate: UILabel!
    @IBOutlet weak var lblEventLocation: UILabel!
    @IBOutlet weak var lblEventType: UILabel!
    
    @IBOutlet weak var viewOfLocation: UIView!
    
    @IBOutlet weak var goingView: UIView!
    @IBOutlet weak var lblGoingCount: UILabel!
    @IBOutlet weak var intrestedView: UIView!
    @IBOutlet weak var lblIntrestedCount: UILabel!
    
    
    @IBOutlet weak var imgProPic: UIImageView!
    @IBOutlet weak var lblCreatedBy: UILabel!
    @IBOutlet weak var lblCreatedTime: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

class searchResultNewPostTableViewCell: UITableViewCell{
    
    
    @IBOutlet weak var attachmentView: UIView!
    @IBOutlet weak var imgPostAttachment: UIImageView!
    @IBOutlet weak var playIconView: UIView!
    
    @IBOutlet weak var postDescView: UIView!
    
    @IBOutlet weak var lblPostDesc: ActiveLabel!
    @IBOutlet weak var lblPostType: UILabel!
    
    @IBOutlet weak var imgPropic: UIImageView!
    
    @IBOutlet weak var lblPostedBy: UILabel!
    @IBOutlet weak var lblPostedTime: UILabel!
    
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnChat: UIButton!
    @IBOutlet weak var shareView: UIView!
    @IBOutlet weak var chatView: UIView!
    @IBOutlet weak var lblChat_count: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

