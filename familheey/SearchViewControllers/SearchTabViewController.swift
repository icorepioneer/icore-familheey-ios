//
//  SearchTabViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 15/11/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Firebase
import ContactsUI
import Contacts
import Moya
import SwiftyJSON
import Kingfisher
import SafariServices
import SideMenu

class SearchTabViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate, postUpdateDelegate  {
    var preLoadComplete = false
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblNoti_Count: UILabel!
    @IBOutlet weak var viewOfNoti_Count: UIView!
    @IBOutlet weak var imgOfNoti_Bell: UIImageView!
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnSearchReset: UIButton!
    
    
    @IBOutlet weak var imgFamily: UIImageView!
    @IBOutlet weak var btnFamily: UIButton!
    @IBOutlet weak var btnPeople: UIButton!
    @IBOutlet weak var imgPeople: UIImageView!
    @IBOutlet weak var imgEvents: UIImageView!
    @IBOutlet weak var btnEvents: UIButton!
    @IBOutlet weak var imgPost: UIImageView!
    @IBOutlet weak var btnPost: UIButton!
    
    
    @IBOutlet weak var searchConstraintheight: NSLayoutConstraint!
    @IBOutlet weak var searchview: UIView!
    @IBOutlet weak var lblSearchResult: UILabel!
    @IBOutlet weak var noDataView: UIView!
    
    
    @IBOutlet weak var txtSearch: UITextField!
    
    @IBOutlet weak var notificationHeight: NSLayoutConstraint!
    
    @IBOutlet weak var notificationView: UIView!
    
    var postoptionsTittle = ["Mute conversation","Edit post","Delete post","Report"]
    var ShareOptionArray = [String]()
    var arrayOfNotificationList = [[String:Any]]()
    var ref: DatabaseReference!
    var notiUnreadCount:Int = 0
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    var ArrayPosts = [JSON]()
    let maxNumberOfLines = 2
    var imgClicked = false
    let titleArray = ["FAMILY","PEOPLE"]
    var selectedIndex = Int() //0 - Family, 1 - People, 2 - Events, 3 - Post
    var searchTxt = ""
    var usersList = [SearchResultUser]()
    var FamilyList = [SearchResultGroup]()
    var EventList = [SearchResultsEvents]()
    var contactArr = [String]()
    var offset = 0
    var limit = 30
    var tabIndex = Int()
    var stopPagination = false
    var isNewDataLoading = true
    var previousPageXOffset: CGFloat = 0.0
    let headerHeight: CGFloat = 56
    var contactsParams = NSMutableArray()
    var tempOffset = CGFloat()
    //    @IBOutlet weak var searchView: UIView!
    //    @IBOutlet weak var collectionViewTitle: UICollectionView!
     var documentInteractionController: UIDocumentInteractionController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationForDiscoverTab"), object: nil)
        
        
        tableView.tableHeaderView = UIView.init()
        tableView.tableFooterView = UIView.init()
        tableView.dataSource = self
        tableView.delegate = self
        self.tableView.contentInset = UIEdgeInsets(top: -15, left: 0, bottom: 0, right: 0)
        //        self.navigationController?.title = "Search"
        
        //        self.navigationController?.navigationBar.topItem?.title = "Search"
        addRightNavigationButton()
        onClickRight()
        
        txtSearch.delegate = self
        
        Helpers.setleftView(textfield: txtSearch, customWidth: 30)
        self.navigationController?.navigationBar.isHidden = true
        
        
        
        let right: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action:nil)
        right.cancelsTouchesInView = false
        right.direction = .right
        right.delegate = self
        let left: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action:nil)
        left.cancelsTouchesInView = false
        left.direction = .left
        left.delegate = self
        view.addGestureRecognizer(left)
        view.addGestureRecognizer(right)
        initViews()
        searchConstraintheight.constant = self.headerHeight
        
        //self.setupRightView()
    }
    func setupRightView(){
        let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "topicListingViewController") as! topicListingViewController
        
        let rightMenuNavigationController = SideMenuNavigationController(rootViewController: vc)
        SideMenuManager.default.leftMenuNavigationController = rightMenuNavigationController
        SideMenuManager.default.addPanGestureToPresent(toView: self.view)
        SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: self.view)
        rightMenuNavigationController.statusBarEndAlpha = 0
        rightMenuNavigationController.menuWidth = self.view.frame.width
        rightMenuNavigationController.dismissOnPush = false
        rightMenuNavigationController.dismissOnPresent = false
        rightMenuNavigationController.pushStyle = .subMenu
    }
    
    func initViews(){
        
        if appDel.PreloadFamilyList.count != 0 && txtSearch.text!.isEmpty{
            self.removeSpinner()
        }
        else{
            self.showSpinner(onView: self.view)
        }
        
        self.viewOfNoti_Count.isHidden = true
        
        self.txtSearch.text = ""
        
        let btn = UIButton()
        btn.tag = tabIndex
        self.onClickTabSelection(btn)
                
        if appDel.noFamily{
            if appDel.PreloadFamilyList.count != 0 && txtSearch.text!.isEmpty{
                self.tableView.hideLoading()
                
                self.stopPagination = false
                self.isNewDataLoading = false
                self.FamilyList = appDel.PreloadFamilyList
                self.tableView.reloadData()
            }
        }
        else{
            if appDel.PreloadFamilyList.count != 0 && txtSearch.text!.isEmpty{
                self.tableView.hideLoading()
                
                self.FamilyList = appDel.PreloadFamilyList
                self.stopPagination = false
                self.isNewDataLoading = false
                self.tableView.reloadData()
            }
        }
        
        
        if UserDefaults.standard.value(forKey: "userProfile") != nil{
            let pic = UserDefaults.standard.value(forKey: "userProfile") as! String
            let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+pic
            let imgUrl = URL(string: temp)
            let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+imgUrl!.relativeString
            let url = URL(string: newUrlStr)
            imgProfile.kf.indicatorType = .activity
            
            imgProfile.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        else{
            self.getUserDetails()
        }
    }
    
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        self.removeSpinner()
        self.preLoadComplete = true
        if appDel.noFamily{
            if appDel.PreloadFamilyList.count != 0 && txtSearch.text!.isEmpty{
                self.stopPagination = false
                self.isNewDataLoading = false
                self.FamilyList = appDel.PreloadFamilyList
                self.tableView.reloadData()
            }
        }
        else{
            if appDel.PreloadFamilyList.count != 0 && txtSearch.text!.isEmpty{
                self.FamilyList = appDel.PreloadFamilyList
                self.stopPagination = false
                self.isNewDataLoading = false
                self.tableView.reloadData()
            }
        }
    }
    
    func getUserDetails(){
        let crntUser = UserDefaults.standard.value(forKey: "userId") as! String
        ActivityIndicatorView.show("Please wait!")
        APIServiceManager.callServer.getUserDetails(url: EndPoint.userDetails, userId: crntUser, profileId: crntUser ,success: { (response) in
            
            ActivityIndicatorView.hiding()
            guard let loginMdl = response as? ExternalUserModel else{
                return
            }
            
            
            UserDefaults.standard.set(loginMdl.User?.fullname, forKey: "user_fullname")
            
            if loginMdl.User?.propic.count != 0 {
                UserDefaults.standard.set(loginMdl.User?.propic, forKey: "userProfile")
                let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+(loginMdl.User!.propic)
                let imgUrl = URL(string: temp)
                
                let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+imgUrl!.relativeString
                let urlImg = URL(string: newUrlStr)
                self.imgProfile.kf.indicatorType = .activity
                self.imgProfile.kf.setImage(with: urlImg, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
            }
            else{
                self.imgProfile.image = #imageLiteral(resourceName: "Male Colored")
            }
            
            
            
        }) { (error) in
            ActivityIndicatorView.hiding()
            
        }
        
    }
    func openSearch(){
        
        //        self.notificationView.isHidden = false
        //        self.notificationHeight.constant = self.headerHeight
        //        self.searchview.isHidden = false
        //        self.searchConstraintheight.constant = self.headerHeight
        //
        //        self.btnSearchReset.isHidden = false
        //
        //        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
        //            self.searchview.alpha = 1.0
        //        }, completion: nil)
    }
    func closeSearch(){
        
        //        UIView.animate(withDuration: 0.5, animations: {
        //            self.searchview.alpha = 0.0
        //
        //        }) { (true) in
        //
        //            self.searchview.isHidden = true
        //            self.searchConstraintheight.constant = 0
        //            self.notificationHeight.constant = self.headerHeight
        //            self.notificationView.isHidden = false
        //
        //        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //Show the navigation bar
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDel.isFamilyFromSearch = false
        
        self.NotificationArrayListing()
        
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.tabBarController?.tabBar.isHidden = true
        
        
        if UserDefaults.standard.value(forKey: "userProfile") != nil{
            let pic = UserDefaults.standard.value(forKey: "userProfile") as! String
            let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+pic
            let imgUrl = URL(string: temp)
            let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+imgUrl!.relativeString
            let url = URL(string: newUrlStr)
            imgProfile.kf.indicatorType = .activity
            
            imgProfile.kf.setImage(with: url, placeholder: UIImage(named: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        else{
            imgProfile.image = UIImage(named: "Male Colored")
        }
        
        
        // Hide the navigation bar
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
    }
    
    //MARK:- Custom Methods
    
    /* private func fetchContacts() {
     print("Attempting to fetch contacts today..")
     
     let store = CNContactStore()
     self.contactsParams = NSMutableArray.init()
     store.requestAccess(for: .contacts) { (granted, err) in
     if let err = err {
     print("Failed to request access:", err)
     self.displayAlertChoice(alertStr: "Failed to get contacts", title: "") { (str) in
     self.navigationController?.popViewController(animated: true)
     
     }
     return
     }
     
     if granted {
     print("Access granted")
     //                ActivityIndicatorView.show("Please wait!")
     
     let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey]
     let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
     
     do {
     
     try store.enumerateContacts(with: request, usingBlock: { (contact, stopPointerIfYouWantToStopEnumerating) in
     
     let con = NSMutableDictionary()
     
     
     if let number = Int(contact.phoneNumbers.first?.value.stringValue.components(separatedBy: CharacterSet.decimalDigits.inverted).joined() ?? "") {
     
     self.contactsParams.add("\(number)")
     
     }
     
     
     })
     if self.contactsParams.count != 0{
     //                        ActivityIndicatorView.hiding()
     DispatchQueue.main.async {
     self.callSearchApi()
     }
     
     }
     else{
     //                        ActivityIndicatorView.hiding()
     
     self.displayAlertChoice(alertStr: "No contacts found to sync", title: "") { (str) in
     self.navigationController?.popViewController(animated: true)
     
     }
     }
     
     } catch let err {
     //                    ActivityIndicatorView.hiding()
     
     print("Failed to enumerate contacts:", err)
     self.displayAlertChoice(alertStr: "Failed to get contacts", title: "") { (str) in
     self.navigationController?.popViewController(animated: true)
     
     }
     }
     
     } else {
     //                ActivityIndicatorView.hiding()
     
     print("Access denied..")
     self.displayAlertChoice(alertStr: "Can't access contacts", title: "") { (str) in
     self.navigationController?.popViewController(animated: true)
     }
     
     }
     }
     }*/
    
    private func fetchContacts(granted:Bool){
        let store = CNContactStore()
        self.contactsParams = NSMutableArray.init()
        if granted {
            //            print("Access granted")
            //                ActivityIndicatorView.show("Please wait!")
            
            let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey]
            let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
            
            do {
                
                try store.enumerateContacts(with: request, usingBlock: { (contact, stopPointerIfYouWantToStopEnumerating) in
                    
                    let con = NSMutableDictionary()
                    
                    
                    if let number = Int(contact.phoneNumbers.first?.value.stringValue.components(separatedBy: CharacterSet.decimalDigits.inverted).joined() ?? "") {
                        
                        self.contactsParams.add("\(number)")
                        
                    }
                    
                    
                })
                if self.contactsParams.count != 0{
                    //                        ActivityIndicatorView.hiding()
                    DispatchQueue.main.async {
                        //remya
                        self.callSearchApi2()
                        //callSearchApi()
                        
                    }
                    
                }
                else{
                    //                        ActivityIndicatorView.hiding()
                    
                    self.displayAlertChoice(alertStr: "No contacts found to sync", title: "") { (str) in
                        self.navigationController?.popViewController(animated: true)
                        
                    }
                }
                
            } catch let err {
                //                    ActivityIndicatorView.hiding()
                
                //                print("Failed to enumerate contacts:", err)
                self.displayAlertChoice(alertStr: "Failed to get contacts", title: "") { (str) in
                    self.navigationController?.popViewController(animated: true)
                    
                }
            }
            
        } else {
            //                ActivityIndicatorView.hiding()
            
            //            print("Access denied..")
            self.displayAlertChoice(alertStr: "Can't access contacts", title: "") { (str) in
                self.navigationController?.popViewController(animated: true)
            }
            
        }
    }
    
    func requestAccess(completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        let store = CNContactStore()
        self.contactsParams = NSMutableArray.init()
        switch CNContactStore.authorizationStatus(for: .contacts) {
        case .authorized:
            completionHandler(true)
        case .denied:
            showSettingsAlert(completionHandler)
        case .restricted, .notDetermined:
            store.requestAccess(for: .contacts) { granted, error in
                if granted {
                    completionHandler(true)
                } else {
                    DispatchQueue.main.async {
                        self.showSettingsAlert(completionHandler)
                    }
                }
            }
        }
    }
    
    private func showSettingsAlert(_ completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        let alert = UIAlertController(title: nil, message: "This app requires access to Contacts to proceed. Go to Settings to grant access.", preferredStyle: .alert)
        if
            let settings = URL(string: UIApplication.openSettingsURLString),
            UIApplication.shared.canOpenURL(settings) {
            alert.addAction(UIAlertAction(title: "Open Settings", style: .default) { action in
                completionHandler(false)
                UIApplication.shared.open(settings)
            })
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { action in
            completionHandler(false)
        })
        present(alert, animated: true)
    }
    
    func trimString(phone:String) -> String{
        //caseInsensitive or the regex will be much longer
        let regex = try! NSRegularExpression(pattern: "()-", options: NSRegularExpression.Options.caseInsensitive)
        let range = NSMakeRange(0, phone.count)
        let modString = regex.stringByReplacingMatches(in: phone, options: [], range: range, withTemplate: "")
        //        let modString = first.replacingCharacters(in: range, with: "")
        
        //        print(modString)
        
        return modString
    }
    
    func NotificationArrayListing()
    {
        
        self.arrayOfNotificationList = []
        appDel.arrayOfNotificationList = []
        appDel.tempNotifi = []
        let userId = UserDefaults.standard.value(forKey: "userId") as! String
        
        let noti_String :String = "\(Helpers.notificationUploadstring)\(userId)_notification"
        let ref = Database.database().reference(withPath: noti_String).queryLimited(toLast: 1200)
        ref.observeSingleEvent(of: .value, with: { snapshot in
            
            if !snapshot.exists() { return }
            
            //            print(snapshot) // Its print all values including Snap (User)
            
            let dict:[String:Any] = snapshot.value as! [String:Any]
            
            for (key, value) in dict {
                var dictionary = [String:Any]()
                dictionary["key"] = key
                dictionary["value"] = value as! [String:Any]
                appDel.arrayOfNotificationList.append(dictionary)
                appDel.tempNotifi.append(value as! [String:Any])
                
            }
            //            print(self.arrayOfNotificationList)
            self.notiUnreadCount = 0
            for data in self.arrayOfNotificationList
            {
                let dict  = data["value"] as! [String:Any]
                if dict["visible_status"] as! String == "unread"
                {
                    self.notiUnreadCount = self.notiUnreadCount + 1
                }
            }
            
            if self.notiUnreadCount == 0
            {
                self.lblNoti_Count.isHidden = true
                self.viewOfNoti_Count.isHidden = true
            }
            else
            {
                
                self.lblNoti_Count.isHidden = false
                self.viewOfNoti_Count.isHidden = false
                if self.notiUnreadCount > 99{
                    self.lblNoti_Count.text = "99+"
                }
                else{
                    self.lblNoti_Count.text = "\(self.notiUnreadCount)"
                }
                
            }
            
            
            
        })
        
        
    }
    
    @IBAction func onClickTabSelection(_ sender: UIButton){
        if sender.tag == 100{
            self.selectedIndex = 0
            self.offset = 0
            limit = 30
            btnPeople.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            btnFamily.setTitleColor(UIColor(named: "greenBackgrounf"), for: .normal)
            btnEvents.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            btnPost.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            
            imgPeople.backgroundColor  = UIColor(named: "lightGrayTextColor")
            imgFamily.backgroundColor  = UIColor(named: "greenBackgrounf")
            imgEvents.backgroundColor  = UIColor(named: "lightGrayTextColor")
            imgPost.backgroundColor  = UIColor(named: "lightGrayTextColor")
            //            tableView.reloadData()
            //            if txtSearch.text! == ""{
            //                closeSearch()
            //            }
            //            else{
            //                openSearch()
            //            }
            if appDel.PreloadFamilyList.count != 0 && txtSearch.text!.isEmpty{
//                self.FamilyList = appDel.PreloadFamilyList
                self.stopPagination = false
                self.isNewDataLoading = false
                self.tableView.reloadData()
            }
            else{
                //remya
                if self.preLoadComplete
                {
                    callSearchApi2()
                }
                else
                {
                    self.tableView.reloadData()
                    
                    
                }
                //callSearchApi()
                
            }
        }
        else if sender.tag == 102{
            self.selectedIndex = 2
            self.offset = 0
            limit = 30
            btnPeople.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            btnFamily.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            btnEvents.setTitleColor(UIColor(named: "greenBackgrounf"), for: .normal)
            btnPost.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            
            imgPeople.backgroundColor  = UIColor(named: "lightGrayTextColor")
            imgFamily.backgroundColor  = UIColor(named: "lightGrayTextColor")
            imgEvents.backgroundColor  = UIColor(named: "greenBackgrounf")
            imgPost.backgroundColor  = UIColor(named: "lightGrayTextColor")
            tableView.register(UINib(nibName: "EventListsTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
            
            //            tableView.reloadData()
            //            if txtSearch.text! == ""{
            //                closeSearch()
            //            }
            //            else{
            //                openSearch()
            //            }
            //remya
            callSearchApi2()
            //callSearchApi()
            
        }
        else if sender.tag == 103{
            self.selectedIndex = 3
            self.offset = 0
            limit = 30
            btnPeople.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            btnFamily.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            btnEvents.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            btnPost.setTitleColor(UIColor(named: "greenBackgrounf"), for: .normal)
            
            imgPeople.backgroundColor  = UIColor(named: "lightGrayTextColor")
            imgFamily.backgroundColor  = UIColor(named: "lightGrayTextColor")
            imgEvents.backgroundColor  = UIColor(named: "lightGrayTextColor")
            imgPost.backgroundColor  = UIColor(named: "greenBackgrounf")
            offset = 0
            //            if txtSearch.text! == ""{
            //                closeSearch()
            //            }
            //            else{
            //                openSearch()
            //            }
            self.callSearchApiforPost()
        }
        else  {
            self.selectedIndex = 1
            self.offset = 0
            limit = 30
            btnFamily.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            btnPeople.setTitleColor(UIColor(named: "greenBackgrounf"), for: .normal)
            btnEvents.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            btnPost.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            
            imgFamily.backgroundColor  = UIColor(named: "lightGrayTextColor")
            imgPeople.backgroundColor  = UIColor(named: "greenBackgrounf")
            imgEvents.backgroundColor  = UIColor(named: "lightGrayTextColor")
            imgPost.backgroundColor  = UIColor(named: "lightGrayTextColor")
            //            tableView.reloadData()
            //            if txtSearch.text! == ""{
            //                closeSearch()
            //            }
            //            else{
            //                openSearch()
            //            }
            //remya
            callSearchApi2()
            //callSearchApi()
            //        tableView.reloadData()
            
        }
        
        
    }
    @IBAction func btnNotificationOnClick(_ sender: Any) {
        // appDel.firstTimeNot = true
        self.lblNoti_Count.isHidden = true
        self.viewOfNoti_Count.isHidden = true
        let storyboard = UIStoryboard.init(name: "third", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        vc.arrayOfNotificationList = self.arrayOfNotificationList
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func feedBackClicked(_ sender: Any) {
        let postoptionsTittle =  ["Calendar","Announcements","Feedback","Quick tour"]
        showActionSheet(titleArr: postoptionsTittle as NSArray, title: "Select options") { (index) in
            if index == 0{
                self.viewCalendar()
                
            }
            else if index == 1{
                self.viewAnnouncements()
            }
            else if index == 3{
                self.viewSubscription()
            }
            else if index == 100{
            }
            else{
                self.sendFeedBack()
                
            }
        }
    }
    
    func sendFeedBack(){
        let stryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = stryboard.instantiateViewController(withIdentifier: "FeedbackViewController") as! FeedbackViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func viewAnnouncements(){
        let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsViewController") as! AnnouncementsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func viewSubscription(){
        let stryboard = UIStoryboard.init(name: "Subscription", bundle: nil)
        let vc = stryboard.instantiateViewController(withIdentifier: "WalkthorughViewController") as! WalkthorughViewController
        appDel.quickTour = true
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func viewCalendar(){
        let stryboard = UIStoryboard.init(name: "third", bundle: nil)
        let vc = stryboard.instantiateViewController(withIdentifier: "CalendarEventsViewController") as! CalendarEventsViewController
        vc.isFromExplore = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    //MARK:- Right navigation button
    
    func addRightNavigationButton(){
        
        let rightButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(onClickrightButtonAction))
        self.navigationItem.rightBarButtonItem = rightButtonItem
        
    }
    
    //MARK:- custom
    func onClickRight(){
        selectedIndex = 0
        if let button = self.navigationItem.rightBarButtonItem {
            button.isEnabled = false
            button.tintColor = UIColor.clear
        }
        txtSearch.text = ""
        
    }
    
    //MARK:- Button Actions
    @IBAction func searchAction(_ sender: Any) {
        openSearch()
    }
    @objc func onClickrightButtonAction(sender: UIBarButtonItem){
        selectedIndex = 0
        if let button = self.navigationItem.rightBarButtonItem {
            button.isEnabled = false
            button.tintColor = UIColor.clear
        }
        txtSearch.becomeFirstResponder()
    }
    
    @IBAction func onClickMemberAction(_ sender: UIButton) {
        switch selectedIndex {
        case 0://Family
            
            let type = FamilyList[sender.tag].reqType
            if type.lowercased() == "invite"{
                let isRemoved = FamilyList[sender.tag].isRemoved
                print("isRemoved  .....  \(isRemoved)")

                if isRemoved == 1{
                    if FamilyList[sender.tag].status?.lowercased() == "pending"{
                        if let id = FamilyList[sender.tag].group_id{
                            APIServiceManager.callServer.invitationResponse(url: EndPoint.invitationResp, tableId: "\(FamilyList[sender.tag].reqId)", user_id: UserDefaults.standard.value(forKey: "userId") as! String, group_id: "\(id)", status: "accepted", fromId: "\(FamilyList[sender.tag].reqFromId)", success: { (response) in
                                //            ActivityIndicatorView.hiding()
                                // print(response)
                                if let results = response as! requestSuccessModel?{
                                    if results.status_code == 200{
                                        self.offset = 0
                                        //remya
                                        self.callSearchApi2()
                                        //callSearchApi()
                                        
                                    }
                                }
                                
                            }) { (error) in
                                ActivityIndicatorView.hiding()
                                
                            }
                        }
                    }
                    else{
                        if let id = FamilyList[sender.tag].group_id{
                            ActivityIndicatorView.show("Please wait!")
                            APIServiceManager.callServer.joinFamilyRequest(url: EndPoint.joinFamily, userId:  UserDefaults.standard.value(forKey: "userId") as! String, groupId: "\(id)", success: { (response) in
                                ActivityIndicatorView.hiding()
                                guard let responseResult = response as? JoinResult else{
                                    return
                                }
                                
                                var fam = self.FamilyList[sender.tag]
                                
                                
                                if fam.member_joining == 3{
                                    fam.status = "accepeted"
                                    fam.is_joined = "1"
                                    fam.reqType = responseResult.joinData!.type
                                }
                                else{
                                    fam.status = "pending"
                                    fam.reqType = responseResult.joinData!.type
                                }
                                
                                self.FamilyList[sender.tag] = fam
                                self.tableView.beginUpdates()
                                self.tableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .none)
                                self.tableView.endUpdates()
                                
                            }) { (error) in
                                ActivityIndicatorView.hiding()
                                
                            }
                        }
                    }
                }
                else{
                    if let id = FamilyList[sender.tag].group_id{
                        APIServiceManager.callServer.invitationResponse(url: EndPoint.invitationResp, tableId: "\(FamilyList[sender.tag].reqId)", user_id: UserDefaults.standard.value(forKey: "userId") as! String, group_id: "\(id)", status: "accepted", fromId: "\(FamilyList[sender.tag].reqFromId)", success: { (response) in
                            //            ActivityIndicatorView.hiding()
                            // print(response)
                            if let results = response as! requestSuccessModel?{
                                if results.status_code == 200{
                                    self.offset = 0
                                    //remya
                                    self.callSearchApi2()
                                    //callSearchApi()
                                    
                                }
                            }
                            
                        }) { (error) in
                            ActivityIndicatorView.hiding()
                            
                        }
                    }
                }
            }
            else{
                if let id = FamilyList[sender.tag].group_id{
                    ActivityIndicatorView.show("Please wait!")
                    APIServiceManager.callServer.joinFamilyRequest(url: EndPoint.joinFamily, userId:  UserDefaults.standard.value(forKey: "userId") as! String, groupId: "\(id)", success: { (response) in
                        ActivityIndicatorView.hiding()
                        
                        guard let responseResult = response as? JoinResult else{
                            return
                        }
                        if responseResult.error == 1
                        {
                          Helpers.showAlertDialog(message: responseResult.message, target: self)
                        }
                        else {
                        
                        var fam = self.FamilyList[sender.tag]
                        
                        if fam.member_joining == 3{
                            fam.status = "accepeted"
                            fam.is_joined = "1"
                            fam.reqType = responseResult.joinData!.type
                        }
                        else{
                            fam.status = "pending"
                            fam.reqType = responseResult.joinData!.type

                        }
                        
                        self.FamilyList[sender.tag] = fam
                        //appDel.PreloadFamilyList[sender.tag] = fam
                        self.tableView.beginUpdates()
                        self.tableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .none)
                        self.tableView.endUpdates()
                    }
                        
                    }) { (error) in
                        ActivityIndicatorView.hiding()
                        
                    }
                }
            }
        case 1://Users
            
            if let id = usersList[sender.tag].userid {
                // print(id)
                let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                let popOverVc = storyboard.instantiateViewController(withIdentifier: "SelectFamilyViewController") as! SelectFamilyViewController
                popOverVc.selectedUserID = "\(id)"
                self.navigationController?.pushViewController(popOverVc, animated: true)
            }
            
        default:
            break
        }
    }
    
    @IBAction func onClickProfilePicClicked(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "UserProfileTableViewController") as! UserProfileTableViewController
        //vc.mutialFamilies =  "\(usersList[indexPath.row].familycount ?? "0")"
        //  vc.mutualConnections =  "\(usersList[indexPath.row].mutualfamilycount ?? "0")"
        var userId = UserDefaults.standard.value(forKey: "userId") as! String
        vc.userID = userId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickSearchReset(_ sender: Any) {
        txtSearch.text = ""
        searchTxt = ""
        if self.selectedIndex == 3{
            offset = 0
            
            callSearchApiforPost()
        }
        else{
            offset = 0
            //remya
            if selectedIndex == 0{
                if appDel.PreloadFamilyList.count != 0 && txtSearch.text!.isEmpty{
                    self.FamilyList = appDel.PreloadFamilyList
                    self.stopPagination = false
                    self.isNewDataLoading = false
                    
                    self.tableView.reloadData()
                }
                else{
                    callSearchApi2()
                }
            }
            else{
                
            }
            
            
            //callSearchApi()
            
        }
        //        closeSearch()
        //        btnSearchReset.isHidden = true
        self.txtSearch.endEditing(true)
    }
    
    @IBAction func onClickTopicStart(_ sender: UIButton) {
        var userArr = [String]()
        let uId = usersList[sender.tag].userid
        
        userArr.append("\(uId ?? 0)")
        let storyboard = UIStoryboard.init(name: "Topic", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CreateTopicViewController") as! CreateTopicViewController
        vc.isFromCreate = false
        vc.isFromDiscover  = true
        vc.toUsers = userArr
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- Custom WEB API functions
    func callSearchApi2(){
        
        var type = ""
        if selectedIndex == 0{
            type = "family"
        }
        else if selectedIndex == 1{
            type = "users"
        }else if selectedIndex == 2{
            type = "events"
        }
        
        if offset == 0{
            
            if self.selectedIndex == 0{
                self.FamilyList.removeAll()
            }
            else if self.selectedIndex == 1{
                self.usersList.removeAll()
            }
            else if self.selectedIndex == 2{
                self.EventList.removeAll()
            }
            ActivityIndicatorView.show("Please wait....")
        }else{
            tableView.showLoading()
        }
        
        self.stopPagination = false
        
        let param = [
            "userid" : UserDefaults.standard.value(forKey: "userId") as! String,
            "searchtxt" : txtSearch.text!.trimmingCharacters(in: .whitespacesAndNewlines),
            "offset": offset,
            "limit": limit,
            "phonenumbers":contactsParams,
            "type":type
            ] as [String : Any]
        print(param)
        networkProvider.request(.globalSearch(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                ActivityIndicatorView.hiding()
                if self.selectedIndex == 0{
                    if self.offset == 0{
                        self.FamilyList = [SearchResultGroup]()
                    }
                }
                else if self.selectedIndex == 1{
                    if self.offset == 0{
                        self.usersList = [SearchResultUser]()
                    }
                    
                }
                else if self.selectedIndex == 2{
                    if self.offset == 0{
                        self.EventList = [SearchResultsEvents]()
                    }
                    
                }
                
                do{
                    
                    if response.statusCode == 200{
                        print("@@@####!!!!---\(response.data)--\(response)")
                        
                        let someDictionaryFromJSON = try JSONSerialization.jsonObject(with: response.data, options: .allowFragments) as! [String: Any]
                        print(someDictionaryFromJSON)
                        let tempArr = someDictionaryFromJSON
                        print(tempArr)
                        do{
                            let searchResult = try SearchList(tempArr)
                            
                            print(searchResult)
                            
                            if let results = searchResult as! SearchList?{
                                
                                if self.offset == 0{
                                    ActivityIndicatorView.hiding()
                                }else{
                                    self.tableView.hideLoading()
                                }
                                self.isNewDataLoading = false
                                
                                //                                                        if results.status_code == 200{
                                
                                if self.selectedIndex == 0{
                                    if results.searchResult2 != nil{
                                        if results.searchResult2!.count == 0{
                                            self.stopPagination = true
                                        }
                                        
                                        if self.offset == 0{
                                            self.FamilyList = results.searchResult2!
                                            //                                                appDel.PreloadFamilyList = results.searchResult2!
                                            //                                print(self.FamilyList.count)
                                            //                                print(self.FamilyList)
                                            // self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
                                        }else{
                                            self.FamilyList.append(contentsOf: results.searchResult2!)
                                            if results.searchResult2!.count > 0 {
                                                self.insertRowsAtTableView(responseCount:results.searchResult2!.count, fullArrayCount: self.FamilyList.count)
                                            }
                                        }
                                    }
                                }
                                else if self.selectedIndex == 1{
                                    if results.searchResult != nil{
                                        if results.searchResult!.count == 0{
                                            self.stopPagination = true
                                        }
                                        
                                        if self.offset == 0{
                                            self.usersList = results.searchResult!
                                            // self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
                                        }else{
                                            self.usersList.append(contentsOf: results.searchResult!)
                                            if results.searchResult!.count > 0 {
                                                self.insertRowsAtTableView(responseCount:results.searchResult!.count, fullArrayCount: self.usersList.count)
                                            }
                                            
                                        }
                                    }
                                }
                                else if self.selectedIndex == 2{
                                    if results.searchResult3 != nil{
                                        if results.searchResult3!.count == 0{
                                            self.stopPagination = true
                                        }
                                        
                                        if self.offset == 0{
                                            self.EventList = results.searchResult3!
                                            // self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
                                        }else{
                                            self.EventList.append(contentsOf: results.searchResult3!)
                                            if results.searchResult3!.count > 0 {
                                                self.insertRowsAtTableView(responseCount:results.searchResult3!.count, fullArrayCount: self.EventList.count)
                                            }
                                            
                                        }
                                    }
                                }
                                //                print("SEarch Result \(results)")
                            }
                            
                            
                            if self.selectedIndex == 0
                            {
                                if self.FamilyList.count == 0{
                                    self.tableView.isHidden = true
                                    self.noDataView.isHidden = false
                                    if self.txtSearch.text!.isEmpty{
                                        self.lblSearchResult.isHidden = true
                                    }
                                    else{
                                        self.lblSearchResult.isHidden = false
                                        self.lblSearchResult.text = "Search result for \"\(self.txtSearch.text!)\""
                                    }
                                    // self.changeScroll()
                                }
                                else{
                                    self.noDataView.isHidden = true
                                    self.tableView.isHidden = false
                                }
                            }
                            else if self.selectedIndex == 1{
                                if self.usersList.count == 0{
                                    self.tableView.isHidden = true
                                    self.noDataView.isHidden = false
                                    if self.txtSearch.text!.isEmpty{
                                        self.lblSearchResult.isHidden = true
                                    }
                                    else{
                                        self.lblSearchResult.isHidden = false
                                        self.lblSearchResult.text = "Search result for \"\(self.txtSearch.text!)\""
                                    }
                                    // self.changeScroll()
                                }
                                else{
                                    self.noDataView.isHidden = true
                                    self.tableView.isHidden = false
                                }
                            }
                            else {
                                if self.EventList.count == 0{
                                    self.tableView.isHidden = true
                                    self.noDataView.isHidden = false
                                    if self.txtSearch.text!.isEmpty{
                                        self.lblSearchResult.isHidden = true
                                    }
                                    else{
                                        self.lblSearchResult.isHidden = false
                                        self.lblSearchResult.text = "Search result for \"\(self.txtSearch.text!)\""
                                    }
                                    // self.changeScroll()
                                }
                                else{
                                    self.noDataView.isHidden = true
                                    self.tableView.isHidden = false
                                }
                            }
                            
                            if self.offset == 0{
                                self.tableView.reloadData()
                            }else{
                                // Condition result < limit
                                // print("STOP LOADING")
                            }
                            
                            if self.selectedIndex == 0
                            {
                                if self.FamilyList.count != 0{
                                    // self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
                                }
                                //                                if self.txtSearch.text! == ""{
                                //                                    self.closeSearch()
                                //                                }
                                //                                else{
                                //                                    self.openSearch()
                                //                                }
                                
                            }
                            else if self.selectedIndex == 1{
                                if self.usersList.count != 0{
                                    // self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
                                }
                                //                                if self.txtSearch.text! == ""{
                                //                                    self.closeSearch()
                                //                                }
                                //                                else{
                                //                                    self.openSearch()
                                //                                }
                            }
                            else {
                                if self.EventList.count != 0{
                                    // self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
                                }
                                //                                if self.txtSearch.text! == ""{
                                //                                    self.closeSearch()
                                //                                }
                                //                                else{
                                //                                    self.openSearch()
                                //                                }
                            }
                        }catch{
                            return
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.callSearchApi2()
                        }
                    }
                    else{
                        self.displayAlert(alertStr: "Something went wrong! Please try again later", title: "Error")
                        ActivityIndicatorView.hiding()
                    }
                    
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    self.usersList = [SearchResultUser]()
                    self.FamilyList = [SearchResultGroup]()
                    
                    
                    self.tableView.reloadData()
                    //                                Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //                            Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                self.usersList = [SearchResultUser]()
                self.FamilyList = [SearchResultGroup]()
                
                
                self.tableView.reloadData()
                break
            }
        }
        
    }
    //    func callSearchApi(){
    //
    //        var type = ""
    //        if selectedIndex == 0{
    //            type = "family"
    //        }
    //        else if selectedIndex == 1{
    //            type = "users"
    //        }else if selectedIndex == 2{
    //            type = "events"
    //        }
    //
    //        if offset == 0{
    //
    //            if self.selectedIndex == 0{
    //                self.FamilyList.removeAll()
    //            }
    //            else if self.selectedIndex == 1{
    //                self.usersList.removeAll()
    //            }
    //            else if self.selectedIndex == 2{
    //                self.EventList.removeAll()
    //            }
    //            ActivityIndicatorView.show("Please wait....")
    //        }else{
    //            tableView.showLoading()
    //        }
    //
    //        self.stopPagination = false
    //
    //        let param = [
    //            "userid" : UserDefaults.standard.value(forKey: "userId") as! String,
    //            "searchtxt" : txtSearch.text!.trimmingCharacters(in: .whitespacesAndNewlines),
    //            "offset": offset,
    //            "limit": limit,
    //            "phonenumbers":contactsParams,
    //            "type":type
    //            ] as [String : Any]
    //
    //        APIServiceManager.callServer.searchRequest(url: EndPoint.search, param: param, fromSearchTabViewController: true, success: { (response) in
    //
    //            ActivityIndicatorView.hiding()
    //            if self.selectedIndex == 0{
    //                if self.offset == 0{
    //                    self.FamilyList = [SearchResultGroup]()
    //                }
    //            }
    //            else if self.selectedIndex == 1{
    //                if self.offset == 0{
    //                    self.usersList = [SearchResultUser]()
    //                }
    //
    //            }
    //            else if self.selectedIndex == 2{
    //                if self.offset == 0{
    //                    self.EventList = [SearchResultsEvents]()
    //                }
    //
    //            }
    //
    //            if let results = response as! SearchList?{
    //
    //                if self.offset == 0{
    //                    ActivityIndicatorView.hiding()
    //                }else{
    //                    self.tableView.hideLoading()
    //                }
    //                self.isNewDataLoading = false
    //
    //                if results.status_code == 200{
    //
    //                    if self.selectedIndex == 0{
    //                        if results.searchResult2 != nil{
    //                            if results.searchResult2!.count == 0{
    //                                self.stopPagination = true
    //                            }
    //
    //                            if self.offset == 0{
    //                                self.FamilyList = results.searchResult2!
    //                                appDel.PreloadFamilyList = results.searchResult2!
    ////                                print(self.FamilyList.count)
    ////                                print(self.FamilyList)
    //                                // self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
    //                            }else{
    //                                self.FamilyList.append(contentsOf: results.searchResult2!)
    //                                if results.searchResult2!.count > 0 {
    //                                    self.insertRowsAtTableView(responseCount:results.searchResult2!.count, fullArrayCount: self.FamilyList.count)
    //                                }
    //                            }
    //                        }
    //                    }
    //                    else if self.selectedIndex == 1{
    //                        if results.searchResult != nil{
    //                            if results.searchResult!.count == 0{
    //                                self.stopPagination = true
    //                            }
    //
    //                            if self.offset == 0{
    //                                self.usersList = results.searchResult!
    //                                // self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
    //                            }else{
    //                                self.usersList.append(contentsOf: results.searchResult!)
    //                                if results.searchResult!.count > 0 {
    //                                    self.insertRowsAtTableView(responseCount:results.searchResult!.count, fullArrayCount: self.usersList.count)
    //                                }
    //
    //                            }
    //                        }
    //                    }
    //                    else if self.selectedIndex == 2{
    //                        if results.searchResult3 != nil{
    //                            if results.searchResult3!.count == 0{
    //                                self.stopPagination = true
    //                            }
    //
    //                            if self.offset == 0{
    //                                self.EventList = results.searchResult3!
    //                                // self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
    //                            }else{
    //                                self.EventList.append(contentsOf: results.searchResult3!)
    //                                if results.searchResult3!.count > 0 {
    //                                    self.insertRowsAtTableView(responseCount:results.searchResult3!.count, fullArrayCount: self.EventList.count)
    //                                }
    //
    //                            }
    //                        }
    //                    }
    //                }
    //
    //
    ////                print("SEarch Result \(results)")
    //
    //            }
    //
    //            if self.selectedIndex == 0
    //            {
    //                if self.FamilyList.count == 0{
    //                    self.tableView.isHidden = true
    //                    self.noDataView.isHidden = false
    //                    if self.txtSearch.text!.isEmpty{
    //                        self.lblSearchResult.isHidden = true
    //                    }
    //                    else{
    //                        self.lblSearchResult.isHidden = false
    //                        self.lblSearchResult.text = "Search result for \"\(self.txtSearch.text!)\""
    //                    }
    //                    // self.changeScroll()
    //                }
    //                else{
    //                    self.noDataView.isHidden = true
    //                    self.tableView.isHidden = false
    //                }
    //            }
    //            else if self.selectedIndex == 1{
    //                if self.usersList.count == 0{
    //                    self.tableView.isHidden = true
    //                    self.noDataView.isHidden = false
    //                    if self.txtSearch.text!.isEmpty{
    //                        self.lblSearchResult.isHidden = true
    //                    }
    //                    else{
    //                        self.lblSearchResult.isHidden = false
    //                        self.lblSearchResult.text = "Search result for \"\(self.txtSearch.text!)\""
    //                    }
    //                    // self.changeScroll()
    //                }
    //                else{
    //                    self.noDataView.isHidden = true
    //                    self.tableView.isHidden = false
    //                }
    //            }
    //            else {
    //                if self.EventList.count == 0{
    //                    self.tableView.isHidden = true
    //                    self.noDataView.isHidden = false
    //                    if self.txtSearch.text!.isEmpty{
    //                        self.lblSearchResult.isHidden = true
    //                    }
    //                    else{
    //                        self.lblSearchResult.isHidden = false
    //                        self.lblSearchResult.text = "Search result for \"\(self.txtSearch.text!)\""
    //                    }
    //                    // self.changeScroll()
    //                }
    //                else{
    //                    self.noDataView.isHidden = true
    //                    self.tableView.isHidden = false
    //                }
    //            }
    //
    //            if self.offset == 0{
    //                self.tableView.reloadData()
    //            }else{
    //                // Condition result < limit
    //                // print("STOP LOADING")
    //            }
    //
    //
    //
    //            if self.selectedIndex == 0
    //            {
    //                if self.FamilyList.count != 0{
    //                    // self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
    //                }
    //                if self.txtSearch.text! == ""{
    //                    self.closeSearch()
    //                }
    //                else{
    //                    self.openSearch()
    //                }
    //
    //            }
    //            else if self.selectedIndex == 1{
    //                if self.usersList.count != 0{
    //                    // self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
    //                }
    //                if self.txtSearch.text! == ""{
    //                    self.closeSearch()
    //                }
    //                else{
    //                    self.openSearch()
    //                }
    //            }
    //            else {
    //                if self.EventList.count != 0{
    //                    // self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
    //                }
    //                if self.txtSearch.text! == ""{
    //                    self.closeSearch()
    //                }
    //                else{
    //                    self.openSearch()
    //                }
    //            }
    //
    //        }) { (error) in
    //            ActivityIndicatorView.hiding()
    //            self.usersList = [SearchResultUser]()
    //            self.FamilyList = [SearchResultGroup]()
    //
    //
    //            self.tableView.reloadData()
    //
    //        }
    //    }
    func insertRowsAtTableView(responseCount: Int, fullArrayCount:Int){
        //        print(responseCount)
        var indexPaths = [IndexPath]()
        
        for i in 0 ..< responseCount{
            let indx = fullArrayCount  - responseCount
            let indexPath = indx + i
            indexPaths.append(IndexPath(row: indexPath, section: 0))
        }
        
        //        print(indexPaths)
        self.tableView.beginUpdates()
        self.tableView.insertRows(at: indexPaths, with: .none)
        self.tableView.endUpdates()
        
    }
    func callElasticSearchApiforPost(){
    }
    
    func callSearchApiforPost(){
        self.stopPagination = false
        
        if offset == 0{
            ArrayPosts.removeAll()
            ActivityIndicatorView.show("Please wait....")
        }
        else{
            tableView.showLoading()
        }
        
        let param = [
            "userid" : UserDefaults.standard.value(forKey: "userId") as! String,"searchtxt" : txtSearch.text!.trimmingCharacters(in: .whitespacesAndNewlines),"offset":offset,"limit":limit,"phonenumbers":contactsParams,"type":"posts"
            ] as [String : Any]
                print("###########\(param)")
        //        ActivityIndicatorView.show("")
        
        networkProvider.request(.globalSearch(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    self.isNewDataLoading = false
                    
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    
                    if response.statusCode == 200{
                        
                        let response = jsonData["posts"]
                        
                        if response.count == 0{
                            self.stopPagination = true
                        }
                        
                        let dataArr = response["data"].array
                        let publicArr = response["by_public_feed"].array
                        let familyArr  = response["by_family"].array
                        
                        if dataArr?.count == 0 && publicArr?.count == 0 && familyArr?.count == 0{
                             self.stopPagination = true
                        }
                        
//                        if dataArr!.count > 0{
//                            self.ArrayPosts.append(contentsOf: dataArr!)
//                        }
                        
                        if publicArr!.count > 0{
                            self.ArrayPosts.append(contentsOf: publicArr!)
                        }
//                        if familyArr!.count > 0{
//                            self.ArrayPosts.append(contentsOf: familyArr!)
//                        }
                        if self.selectedIndex == 3{
                            if self.offset != 0{
                                if publicArr!.count > 0 {
                                    self.insertRowsAtTableView(responseCount: publicArr!.count)
                                }
                            }
                            else{

                            }
                        }

                        
                        if self.offset == 0{
                            ActivityIndicatorView.hiding()
                        }
                        else{
                            self.tableView.hideLoading()
                        }
                        
                        if self.ArrayPosts.count > 0 {
                            
                            self.noDataView.isHidden = true
                            self.tableView.isHidden = false
                        
                            if self.offset == 0{
                                self.tableView.reloadData()
                            }
                            
                            if self.ArrayPosts.count <= 0{
                                self.tableView.hideLoading()
                                return
                            }
                            
                            if publicArr!.count < 30{
                            }
                            else{
                                if self.selectedIndex == 3{
                                    if !self.isNewDataLoading && !self.stopPagination{
                                        self.isNewDataLoading = true
                                        self.offset = self.ArrayPosts.count + 1
                                        self.callSearchApiforPost()
                                    }
                                }
                            }
 
                        }
                        else{
                            if self.txtSearch.text!.isEmpty {
                                self.lblSearchResult.isHidden = true
                            }
                            else{
                                self.lblSearchResult.isHidden = false
                                self.lblSearchResult.text = "Search result for \"\(self.txtSearch.text!)\""
                            }
                            self.tableView.isHidden = true
                            self.noDataView.isHidden = false
                            self.changeScroll()
                        }
                        

                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.callSearchApiforPost()
                        }
                    }
                }catch let err {
                    ActivityIndicatorView.hiding()
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    func changeScroll(){
        //        if  self.notificationView.isHidden{
        //            //                self.searchview.isHidden = false
        //            self.notificationView.isHidden = false
        //            self.searchConstraintheight.constant = self.headerHeight
        //        }
    }
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: 20, y: self.view.frame.size.height/2-30, width: self.view.frame.width-40, height: 60))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.numberOfLines = 2
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 10.0)
        toastLabel.adjustsFontSizeToFitWidth = true
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 6.0, delay: 0.3, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    //MARK:- Custom Methods
    func insertRowsAtTableView(responseCount: Int){
        
        var indexPaths = [IndexPath]()
        
        for i in 0 ..< responseCount{
            let indx = ArrayPosts.count - responseCount
            let indexPath = indx + i
            indexPaths.append(IndexPath(row: indexPath, section: 0))
        }
        
        print(indexPaths)
        self.tableView.beginUpdates()
        self.tableView.insertRows(at: indexPaths, with: .none)
        self.tableView.endUpdates()
    }
    
    func deletePostForCreator(tag:Int){
        let alert = UIAlertController(title: "Familheey", message: "Do you really want to delete this post? Deleting this post will delete it from everywhere you posted it to and you will lose all associated conversations", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "DELETE", style: .destructive, handler: { _ in
            let post = self.ArrayPosts[tag]
            
            
            let param = ["id" : post["post_id"].stringValue,"user_id":UserDefaults.standard.value(forKey: "userId") as! String]
            
            ActivityIndicatorView.show("Please wait....")
            self.networkProvider.request(.deletePost(parameter: param)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                        // print("view contents : \(jsonData)")
                        if response.statusCode == 200{
                            if tag < self.ArrayPosts.count{
                                self.ArrayPosts.remove(at: tag)
                                self.tableView.deleteRows(at: [IndexPath.init(row: tag, section: 0)], with: .automatic)
                            }
                        }
                        else if response.statusCode == 401 {
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                self.deletePostForCreator(tag: tag)
                            }
                        }
                        ActivityIndicatorView.hiding()
                        
                    }catch let err {
                        ActivityIndicatorView.hiding()
                        // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                    }
                case .failure( let error):
                    ActivityIndicatorView.hiding()
                    //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                    break
                }
            }
            
        }))
        alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func removePostFromTimeline(tag:Int){
        let post = ArrayPosts[tag]
        
        let param = ["post_id" : post["post_id"].stringValue, "user_id": UserDefaults.standard.value(forKey: "userId") as! String]
        
        ActivityIndicatorView.show("Please wait....")
        networkProvider.request(.removePost(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    //   print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        
                        if tag < self.ArrayPosts.count{
                            self.ArrayPosts.remove(at: tag)
                            self.tableView.deleteRows(at: [IndexPath.init(row: tag, section: 0)], with: .automatic)
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.removePostFromTimeline(tag: tag)
                        }
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    func muteConversation(tag:Int){
        let post = ArrayPosts[tag]
        let param = ["post_id" : post["post_id"].stringValue, "user_id": UserDefaults.standard.value(forKey: "userId") as! String]
        
        ActivityIndicatorView.show("Please wait....")
        networkProvider.request(.mute_conversation(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    //  print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        ///   print("view contents : \(jsonData)")
                        let temp = jsonData["data"].dictionaryValue
                        if temp["is_active"]!.boolValue{
                            var temp = post
                            temp["muted"] = true
                            self.ArrayPosts[tag] = temp
                            //  print("#######NEW ARR - \(self.ArrayPosts[tag])")
                        }
                        else{
                            var temp = post
                            temp["muted"] = false
                            self.ArrayPosts[tag] = temp
                            //  print("#######NEW ARR - \(self.ArrayPosts[tag])")
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.muteConversation(tag: tag)
                        }
                    }
                    ActivityIndicatorView.hiding()
                }catch let err {
                    ActivityIndicatorView.hiding()
                    //  Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    func viewCurrentPost(index:Int){
        
        if  ArrayPosts.count > index  {
            
            let post = ArrayPosts[index]
            
            let param = ["post_id" : post["post_id"].stringValue, "user_id": UserDefaults.standard.value(forKey: "userId") as! String]
            
            //               ActivityIndicatorView.show("Please wait....")
            networkProvider.request(.add_view_count(parameter: param)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                        //   print("view contents : \(jsonData)")
                        if response.statusCode == 200{
                            //    print("view contents : \(jsonData)")
                        }
                        else if response.statusCode == 401 {
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                self.viewCurrentPost(index: index)
                            }
                        }
                    }catch let err {
                        //                           ActivityIndicatorView.hiding()
                        // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                    }
                case .failure( let error):
                    //                       ActivityIndicatorView.hiding()
                    //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                    break
                }
            }
        }
    }
    
    //MARK:- Time stamp Convertion
    func convertTimeStamp(timestamp:String) -> String {
        let date = Date(timeIntervalSince1970: Double(timestamp) as! TimeInterval)
        //    dateFormatter.dateFormat       = "EEE MMM dd yyy"
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MMM/yyyy hh:mma"
        //        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.timeZone = TimeZone.current
        
        
        let localDatef = formatter.string(from: date)
        //        print(localDatef)
        return localDatef
    }
    
    func convertDateToDisplayDate(dateFromResponse:String)->String{
        if dateFromResponse != ""
        {
            let dateAsString               = dateFromResponse
            let dateFormatter              = DateFormatter()
            dateFormatter.dateFormat       = "dd/MMM/yyyy hh:mma"
            
            let date = dateFormatter.date(from: dateAsString)
            print(date as Any)
            dateFormatter.dateFormat       = "MMM dd yyyy"
            let DateFormatted = dateFormatter.string(from: date!)
            print(DateFormatted)
            return DateFormatted
        }else
        {
            return ""
        }
    }
    
    func filterDayOnly(timestamp:String) -> String {
        let date = Date(timeIntervalSince1970: Double(timestamp) as! TimeInterval)
        //    dateFormatter.dateFormat       = "EEE MMM dd yyy"
        
        let formatter = DateFormatter()
        formatter.dateFormat = "EEE"
        //        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.timeZone = TimeZone.current
        
        
        let localDatef = formatter.string(from: date)
        // print(localDatef)
        return localDatef
    }
    
    //MARK:- Textfield Delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        
        textField.endEditing(true)
        if textField.text!.count > 0{
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
        }
        //        }
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        self.view.endEditing(true)
        if textField == txtSearch
        {
            if textField.text!.count > 0{
                btnSearchReset.isHidden = false
            }
            else{
                btnSearchReset.isHidden = true
            }
            //            }
            if self.selectedIndex == 3{
                offset = 0
                callSearchApiforPost()
                
            }
            else{
                offset = 0
                //remya
                callSearchApi2()
                //callSearchApi()
            }
            
        }
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
        }
        return  true
    }
    
    
    //MARK:- collectionView Delegates
    
    //    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    //        return titleArray.count
    //    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchTitlesCollectionViewCell", for: indexPath as IndexPath) as! SearchTitlesCollectionViewCell
        
        cell.lblTitle.text = titleArray[indexPath.item]
        
        if indexPath.item == selectedIndex{
            cell.viewSelection.isHidden = false
            // cell.viewSelection.backgroundColor = UIColor(named: "greenBackground")
            cell.lblTitle.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            //  cell.lblTitle.font      = UIFont.boldSystemFont(ofSize: 16)
            
        }
        else{
            cell.viewSelection.isHidden = true
            cell.lblTitle.textColor = .black
            //  cell.lblTitle.font      = UIFont.systemFont(ofSize: 14)
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //        if indexPath.item > 2 {
        //            return
        //        }
        selectedIndex = indexPath.item
        collectionView.reloadData()
        tableView.reloadData()
    }
    
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    //        let numberOfCellInRow  = self.titleArray.count
    //        let padding : Int      = 10
    ////        let collectionCellWidth : CGFloat = (self.collectionViewTitle.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
    //        return CGSize(width: collectionCellWidth, height: 60)
    //    }
    
    //MARK: - Tableview Delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
        switch selectedIndex {
        case 0:
            if FamilyList.count == 0{
                //                TableViewHelper.EmptyMessage(message: "No results in Family!", viewController: self)
                //                Helpers.showAlertDialog(message: "No results in Family!", target: self)
                return 0
            }
            else{
                tableView.backgroundView = nil
                return 1
            }
            
        case 1:
            
            if usersList.count == 0{
                //                TableViewHelper.EmptyMessage(message: "No results in Users!", viewController: self)
                //                Helpers.showAlertDialog(message: "No results in Users!", target: self)
                return 0
            }
            else{
                tableView.backgroundView = nil
                return 1
            }
        case 2:
            if EventList.count == 0{
                //                TableViewHelper.EmptyMessage(message: "No results in Users!", viewController: self)
                self.tableView.isHidden = true
                return 0
            }
            else{
                tableView.backgroundView = nil
                return 1
            }
        case 3:
            if ArrayPosts.count == 0{
                self.tableView.isHidden = true
                return 0
            }
            else{
                tableView.backgroundView = nil
                return 1
            }
            
        default:
            //            TableViewHelper.EmptyMessage(message: "Tap on The find icon to start finding!", viewController: self)
            Helpers.showAlertDialog(message: "Tap on The find icon to start finding!", target: self)
            return 0
        }
        
        //        //        if projects.count > 0 {
        //        return 1
        //        //        } else {
        //        //            TableViewHelper.EmptyMessage(message: "Tap on The find icon to start finding!", viewController: self)
        //        //            return 0
        //        //        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch selectedIndex {
        case 0:
            return FamilyList.count
            
        case 1:
            return usersList.count
        case 2:
            return EventList.count
        case 3:
            return ArrayPosts.count
            
        default:
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
           print("updating slide........")
           print(FamilyList)
        
        switch selectedIndex {
        case 0:
            guard FamilyList.count > indexPath.row else{
                return UITableViewCell.init()
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "searchResultNewTablleViewCell", for: indexPath) as! searchResultNewTablleViewCell
            
            cell.btnJoin.tag = indexPath.row
            
            cell.lblFamilyName.text = FamilyList[indexPath.row].group_name
            cell.lblCreatedBy.text = "By \(FamilyList[indexPath.row].createdBy)"
            
            if FamilyList[indexPath.row].logo.count != 0 {
                let temp = "\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+FamilyList[indexPath.row].logo
                let imgUrl = URL(string: temp)
                let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+imgUrl!.relativeString
                let url = URL(string: newUrlStr)
                cell.imgFamilyLogo.backgroundColor = .clear
                cell.imgFamilyLogo.kf.indicatorType = .activity
                
                cell.imgFamilyLogo.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
            }
            else{
                cell.imgFamilyLogo.image = #imageLiteral(resourceName: "Family Logo")
            }
            
            if let orign = FamilyList[indexPath.row].base_region{
                if orign == ""{
                    cell.lblLocation.text = "Unknown"
                }
                else{
                    let tempArr = FamilyList[indexPath.row].base_region?.components(separatedBy: ",")
                    cell.lblLocation.text = tempArr![0]
                }
            }
            if let count = FamilyList[indexPath.row].membercount{
                if count == "" {
                    cell.lblMembersCount.text = "0"
                    cell.membersView.isHidden = true
                }
                else if count == "0"{
                    cell.lblMembersCount.text = "0"
                    cell.membersView.isHidden = true
                }
                else{
                    cell.lblMembersCount.text = count
                    cell.membersView.isHidden = false
                }
            }
            else{
                cell.lblMembersCount.text = "0"
                cell.membersView.isHidden = true
            }
            
            /*let post_count = FamilyList[indexPath.row].post_count
            if post_count > 0{
                cell.lblPostCount.text = "\(post_count)"
                 cell.postView.isHidden = false
            }
            else{
                cell.lblPostCount.text = "0"
                cell.postView.isHidden = true
            }*/
            
            if let Pcount = FamilyList[indexPath.row].post_count{
                if Pcount == "" {
                    cell.lblPostCount.text = "0"
                    cell.postView.isHidden = true
                }
                else if Pcount == "0"{
                    cell.lblPostCount.text = "0"
                    cell.postView.isHidden = true
                }
                else{
                    cell.lblPostCount.text = Pcount
                    cell.postView.isHidden = false
                }
            }
            else{
                cell.lblPostCount.text = "0"
                cell.postView.isHidden = true
            }
            print(FamilyList[indexPath.row])
            
            let joinStatus = FamilyList[indexPath.row].is_joined
            let member_joining = FamilyList[indexPath.row].member_joining
            if  let Status = FamilyList[indexPath.row].status {
                
                if joinStatus == "1"{
                    if FamilyList[indexPath.row].isRemoved == 1{
                        if Status.lowercased() == "pending"{
                            let type = FamilyList[indexPath.row].reqType
                            if type.lowercased().contains("invite"){
                                cell.btnJoin.setTitle("Accept", for: .normal)
                                cell.btnJoin.isUserInteractionEnabled  = true
                                cell.btnJoin.setTitleColor(.white, for: .normal)
                                cell.btnJoin.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                            }
                            else{
                                cell.btnJoin.setTitle("Pending", for: .normal)
                                cell.btnJoin.isUserInteractionEnabled  = false
                                cell.btnJoin.setTitleColor(.white, for: .normal)
                                cell.btnJoin.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                            }
                        }
                        else if Status.lowercased() == "accepeted" || Status.lowercased() == "joined"{
                            cell.btnJoin.setTitle("Joined", for: .normal)
                            cell.btnJoin.isUserInteractionEnabled  = false
                            cell.btnJoin.backgroundColor = .clear
                            cell.btnJoin.setTitleColor(#colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1), for: .normal)
                        }
                        else
                        {
                            if member_joining == 1{
                                cell.btnJoin.setTitle("Private", for: .normal)
                                cell.btnJoin.isUserInteractionEnabled  = false
                                cell.btnJoin.setTitleColor(.white, for: .normal)
                                cell.btnJoin.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                            }
                            else{
                                cell.btnJoin.setTitle("Join", for: .normal)
                                cell.btnJoin.isUserInteractionEnabled  = true
                                cell.btnJoin.setTitleColor(.white, for: .normal)
                                cell.btnJoin.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                            }

                        }
                    }
                    else{
                        cell.btnJoin.setTitle("Joined", for: .normal)
                        cell.btnJoin.isUserInteractionEnabled  = false
                        cell.btnJoin.backgroundColor = .clear
                        cell.btnJoin.setTitleColor(#colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1), for: .normal)
                    }
                    
                }
                else if member_joining == 1{
                    cell.btnJoin.setTitle("Private", for: .normal)
                    cell.btnJoin.isUserInteractionEnabled  = false
                    cell.btnJoin.setTitleColor(.white, for: .normal)
                    cell.btnJoin.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                }
                else if Status.lowercased() == "pending"{
                    let type = FamilyList[indexPath.row].reqType
                    if type.lowercased() == "invite"{
                        cell.btnJoin.setTitle("Accept", for: .normal)
                        cell.btnJoin.isUserInteractionEnabled  = true
                        cell.btnJoin.setTitleColor(.white, for: .normal)
                        cell.btnJoin.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                    }
                    else{
                        cell.btnJoin.setTitle("Pending", for: .normal)
                        cell.btnJoin.isUserInteractionEnabled  = false
                        cell.btnJoin.setTitleColor(.white, for: .normal)
                        cell.btnJoin.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                    }
                }
                else if Status.lowercased() == "rejected"{
                    cell.btnJoin.setTitle("Join", for: .normal)
                    cell.btnJoin.isUserInteractionEnabled  = true
                    cell.btnJoin.setTitleColor(.white, for: .normal)
                    cell.btnJoin.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                }
                else{
                    cell.btnJoin.setTitle("Join", for: .normal)
                    cell.btnJoin.isUserInteractionEnabled  = true
                }
                
            }
            return cell
        case 1:
            guard usersList.count > indexPath.row else{
                return UITableViewCell.init()
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "searchResultNewPeopleTableViewCell", for: indexPath) as! searchResultNewPeopleTableViewCell
            cell.btnChat.tag = indexPath.row
            cell.btnAddtoFamily.tag = indexPath.row
            
            cell.lblName.text = usersList[indexPath.row].full_name
            cell.imgProfile.backgroundColor = .clear
            
            if usersList[indexPath.row].propic?.count != 0{
                let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+usersList[indexPath.row].propic!
                let imgUrl = URL(string: temp)
                
                let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+imgUrl!.relativeString
                let url = URL(string: newUrlStr)
                cell.imgProfile.kf.indicatorType = .activity
                
                cell.imgProfile.kf.setImage(with: url, placeholder:#imageLiteral(resourceName: "Male Colored") , options: nil, progressBlock: nil, completionHandler: nil)
            }else{
                cell.imgProfile.image = #imageLiteral(resourceName: "Male Colored")
            }
            
            if let origin = usersList[indexPath.row].origin{
                if origin == ""{
                    cell.lblPlace.text = "Unknown Location"
                }
                else{
                    cell.lblPlace.text = usersList[indexPath.row].origin
                }
            }
            
            if let count = usersList[indexPath.row].familycount{
                if count == "" {
                    cell.lblFamilyCount.text = "0"
                    cell.familyCountView.isHidden = true
                }
                else if count == "0"{
                    cell.lblFamilyCount.text = "0"
                    cell.familyCountView.isHidden = true
                }
                else{
                    cell.lblFamilyCount.text = count
                    cell.familyCountView.isHidden = false
                }
            }
            else{
                cell.lblFamilyCount.text = "0"
                cell.familyCountView.isHidden = true
            }
            
            //cell.lblCountLeftDetails.text = "\(usersList[indexPath.row].faCount)"
            
            if appDel.noFamily{
                cell.btnAddtoFamily.isHidden = true
            }
            else{
                cell.btnAddtoFamily.isHidden = false
            }
            
            return cell
            
        case 2:
            guard EventList.count > indexPath.row else{
                return UITableViewCell.init()
            }
            let cellid = "searchResultNewEventTableViewCell"
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! searchResultNewEventTableViewCell
            
            cell.selectionStyle = .none
            
            cell.lblCreatedBy.text = EventList[indexPath.row].created
            cell.lblEventType.text = EventList[indexPath.row].eventType
            
            var convertedDate = ""
            if EventList[indexPath.row].createdAt > 0  {
                convertedDate =  EventsTabViewController.convertTimeStampToDate(timestamp: "\(EventList[indexPath.row].createdAt)")
            }
            else{
            }
            cell.lblCreatedTime.text = "\(convertedDate)"
            cell.lblEventNAme.text = EventList[indexPath.row].eventName
            
            var fDate = ""
            
            if EventList[indexPath.row].fromDate > 0{
                fDate = convertTimeStamp(timestamp: "\(EventList[indexPath.row].fromDate)")
                print(fDate)
                cell.lblEventDate.text = self.convertDateToDisplayDate(dateFromResponse: fDate)
            }
            
            let loc =  EventList[indexPath.row].location
            if loc.isEmpty{
                let type = EventList[indexPath.row].eventType
                if type.lowercased() == "online"{
                    cell.viewOfLocation.isHidden = true
                }
                else{
                    cell.viewOfLocation.isHidden = false
                    cell.lblEventLocation.text = "unknown"
                }
                
            }
            else{
                cell.viewOfLocation.isHidden = false
                let tArr = loc.components(separatedBy: ",")
                cell.lblEventLocation.text = tArr[0]
            }
            
            let proPic = EventList[indexPath.row].propic
            
            if proPic.count != 0{
                let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic
                let imgUrl = URL(string: temp)
                
                let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+imgUrl!.relativeString
                let url = URL(string: newUrlStr)
                cell.imgProPic.kf.indicatorType = .activity
                
                cell.imgProPic.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
            }
            else{
                cell.imgProPic.image = #imageLiteral(resourceName: "Male Colored")
            }
            
            let eventImage = EventList[indexPath.row].event_imag
            if eventImage.count != 0{
                let temp = "\(Helpers.imageURl)"+"\(BaseUrl.event_image)"+eventImage
                let imgUrl = URL(string: temp)
                let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=200&height=200&url="+imgUrl!.relativeString
                let url = URL(string: newUrlStr)
                cell.imgEventCover.kf.indicatorType = .activity
                
                cell.imgEventCover.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
            }//[.forceRefresh]
            else{
                cell.imgEventCover.image = #imageLiteral(resourceName: "default_event_image")
            }
            
            
            //            let firstPerson = EventList[indexPath.row].firstPerson
            let GoneCount = EventList[indexPath.row].goingCount
            // print(GoneCount)
            if Int(GoneCount)! > 0{
                cell.lblGoingCount.text = "\(GoneCount)"
                cell.goingView.isHidden = false
                
                /*cell.lblGoneCount.isHidden = false
                 cell.lblGoingHead.isHidden = false
                 cell.goingView.isHidden = false
                 cell.lblKnownCount.isHidden = false
                 if Int(GoneCount)! > 1{
                 cell.lblKnownCount.text = firstPerson+" and \(Int(GoneCount)!-1) others are going"
                 }
                 else if Int(GoneCount)! == 2{
                 cell.lblKnownCount.text = firstPerson+" and \(Int(GoneCount)!-1) other is going"
                 }
                 else{
                 cell.lblKnownCount.text = firstPerson+" is going"
                 }*/
            }
            else{
                cell.goingView.isHidden = true
                /*cell.lblGoneCount.isHidden = true
                 cell.lblGoingHead.isHidden = true
                 cell.goingView.isHidden = true
                 cell.lblKnownCount.isHidden = true*/
            }
            
            let IntrstedCount = EventList[indexPath.row].interstedCount
            if Int(IntrstedCount)! > 0{
                cell.lblIntrestedCount.text = "\(IntrstedCount)"
                cell.intrestedView.isHidden = false
                //                cell.lblIntrstdHead.isHidden = false
                //                cell.lblIntrestedCount.isHidden = false
                
            }
            else{
                cell.intrestedView.isHidden = true
                
                //                cell.lblIntrstdHead.isHidden = true
                //                cell.lblIntrestedCount.isHidden = true
            }
            
   
            /* var fDate = ""
             var tDate = ""
             
             var fdateOnly = [String]()
             var tdateOnly = [String]()
             var fDateArray = [String]()
             var tDateArray = [String]()
             var fYear = [String]()
             var tYear = [String]()
             
             if EventList[indexPath.row].fromDate > 0{
             fDate = convertTimeStamp(timestamp: "\(EventList[indexPath.row].fromDate)")
             fdateOnly = fDate.components(separatedBy: " ")
             fDateArray = fDate.components(separatedBy: "/")
             fYear = fDateArray[2].components(separatedBy: " ")
             }
             if EventList[indexPath.row].toDate > 0{
             tDate = convertTimeStamp(timestamp: "\(EventList[indexPath.row].toDate)")
             tdateOnly = tDate.components(separatedBy: " ")
             tDateArray = tDate.components(separatedBy: "/")
             tYear = tDateArray[2].components(separatedBy: " ")
             }
             
             if fdateOnly.count > 0 && tdateOnly.count > 0{
             
             if fdateOnly[0] == tdateOnly[0]
             {
             cell.lblTime.text = "\(filterDayOnly(timestamp: "\(EventList[indexPath.row].fromDate)")) , \(fdateOnly[1]) - \(tdateOnly[1])"
             }
             else
             {
             if fYear[0] == tYear[0]
             {
             cell.lblTime.text = "\(filterDayOnly(timestamp: "\(EventList[indexPath.row].fromDate)")) \(fDateArray[1]) \(fDateArray[0]) - \(filterDayOnly(timestamp: "\(EventList[indexPath.row].toDate)")) \(tDateArray[1]) \(tDateArray[0]) , \(tYear[0]) "
             
             }
             else
             {
             cell.lblTime.text = "\(filterDayOnly(timestamp: "\(EventList[indexPath.row].fromDate)")) \(fDateArray[1]) \(fDateArray[0]) \(fYear[0])  - \(filterDayOnly(timestamp: "\(EventList[indexPath.row].toDate)")) \(tDateArray[1]) \(tDateArray[0]) \(tYear[0]) "
             }
             }
             }
             
             if fDateArray.count > 0{
             cell.lblMonth.text = fDateArray[1].uppercased()
             cell.lblDate.text = fDateArray[0]
             }
             if EventList[indexPath.row].eventType.lowercased() == "online"{
             if EventList[indexPath.row].location.isEmpty{
             cell.lblLocation.text = EventList[indexPath.row].meeting_link
             }
             else{
             cell.lblLocation.text = "Unknown location"
             cell.imgLocationorUrl.image = #imageLiteral(resourceName: "ic_location_details_")
             }
             if EventList[indexPath.row].meeting_logo != ""{
             let temp = "\(Helpers.imageURl)"+EventList[indexPath.row].meeting_logo
             let imgUrl = URL(string: temp)
             let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=30&height=30&url="+imgUrl!.relativeString
             
             let url = URL(string: newUrlStr)
             
             
             cell.imgLocationorUrl.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
             }
             else{
             cell.imgLocationorUrl.image = #imageLiteral(resourceName: "pointer_icon")
             }
             }
             else{
             cell.lblLocation.text = EventList[indexPath.row].location
             cell.imgLocationorUrl.image = #imageLiteral(resourceName: "ic_location_details_")
             }
             
             cell.lblEventCate.text = EventList[indexPath.row].eventType.uppercased()
             cell.lblEventType.text = EventList[indexPath.row].eventType */
            
            
            
            /* cell.lblLocation.handleURLTap { (url) in
             //  print(url)
             self.showActionSheet(titleArr: ["Open URL","Copy URL","Share URL"], title: "Select option") { (index) in
             if index == 100{}
             else if index == 0{
             UIPasteboard.general.string = self.EventList[indexPath.row].meeting_pin
             self.showToast(message: "Participant pin copied to clipboard")
             DispatchQueue.main.asyncAfter(deadline: .now()+3) {
             UIApplication.shared.open(url)
             }
             }
             else if index == 1{
             UIPasteboard.general.string = url.absoluteString
             }
             else if index == 2{
             let textToShare = [ url.absoluteString ]
             let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
             activityViewController.popoverPresentationController?.sourceView = self.view
             self.present(activityViewController, animated: true, completion: nil)
             }
             }
             }*/
            
            return cell
            
        case 3:
            guard ArrayPosts.count > indexPath.row else{
                return UITableViewCell.init()
            }
            let post = ArrayPosts[indexPath.row]
            
            let cellid = "searchResultNewPostTableViewCell"
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! searchResultNewPostTableViewCell
            cell.btnShare.tag = indexPath.row
            cell.btnChat.tag = indexPath.row
            
            if let desc = post["snap_description"].string, desc.isEmpty{
                cell.postDescView.isHidden = true
            }
            else{
                cell.lblPostDesc.text = post["snap_description"].string ?? ""
                cell.postDescView.isHidden = false
            }
            
            var proPic = ""
            
            if let sharedusers = post["shared_user_name"].string , !sharedusers.isEmpty{
                proPic = post["parent_post_created_user_propic"].stringValue
                cell.lblPostedBy.text = post["parent_post_created_user_name"].stringValue
            }
            else{
                proPic = post["propic"].stringValue
                cell.lblPostedBy.text = post["created_user_name"].stringValue
            }
            
            DispatchQueue.main.async {
                
                if proPic.count != 0{
                    let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic
                    let imgUrl = URL(string: temp)
                    cell.imgPropic.kf.indicatorType = .activity
                    
                    cell.imgPropic.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                }
                else{
                    cell.imgPropic.image = #imageLiteral(resourceName: "Male Colored")
                }
            }
            
            cell.lblPostedTime.text = HomeTabViewController.dateFormatterCommon(dateStr: post["createdAt"].stringValue)
            cell.lblPostType.text = post["post_type"].stringValue.firstUppercased
            
            if let Attachment = post["post_attachment"].array, Attachment.count != 0{
                cell.attachmentView.isHidden = false
                let attachment = Attachment[0]
                if let type = attachment["type"].string ,type.contains("image"){
                    var  temp  = ""
                    let height = attachment["height"].floatValue
                    let width = attachment["width"].floatValue
                    temp = "\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+attachment["filename"].stringValue.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                    
                    var imgurl = URL(string:Helpers.imaginaryImageBaseUrl+"width=400&height=400&url=" + temp )
//                    var imgurl = URL(string: BaseUrl.imaginaryURLForDetail2X + temp )
                    cell.playIconView.isHidden = true
                    if type.lowercased().contains("gif"){
                        imgurl = URL(string: temp )
                    }
                    DispatchQueue.main.async {
                        cell.imgPostAttachment.kf.indicatorType = .image(imageData: appDel.gifData)
                        cell.imgPostAttachment.kf.setImage(with: imgurl, placeholder:nil, options: nil)
                    }
                }
                else if let type = attachment["type"].string ,type.contains("video"){
                    var temp = ""
                    //"\(Helpers.imageURl)"+"default_video.jpg"
                    cell.playIconView.isHidden = false
                    if let thumb = attachment["video_thumb"].string, !thumb.isEmpty {
                        temp = "\(Helpers.imageURl)"+thumb
                        let imgurl = URL(string: Helpers.imaginaryImageBaseUrl+"width=400&height=400&url=" + temp )
                        DispatchQueue.main.async {
                            cell.imgPostAttachment.kf.indicatorType = .image(imageData: appDel.gifData)
                            cell.imgPostAttachment.kf.setImage(with: imgurl, placeholder:nil, options: nil)
                        }
                    }
                }
                
            }
            else{
                cell.attachmentView.isHidden = true
            }
            
            cell.lblChat_count.text = post["conversation_count"].stringValue
            
             if post["conversation_enabled"].bool ?? false{
                cell.chatView.isHidden = false
            }
             else{
                cell.chatView.isHidden = true
            }
            
            if post["is_shareable"].bool ?? false {
                
                cell.shareView.isHidden = false
                
            }
            else{
                
                cell.shareView.isHidden = true
                
            }
            
            cell.lblPostDesc.handleHashtagTap { hashtag in
                //   print("Success. You just tapped the \(hashtag) hashtag")
                self.txtSearch.text  = "#"+hashtag
                //                self.openSearch()
                //                    self.searchview.isHidden = false
                //                    self.searchConstraintheight.constant = 36
                self.txtSearch.becomeFirstResponder()
                
            }
            cell.lblPostDesc.handleMentionTap { (mension) in
                //   print("Success. You just tapped the \(mension) mension")
                self.txtSearch.text  = mension
                //                self.openSearch()
                //                    self.searchview.isHidden = false
                //                    self.searchConstraintheight.constant = 36
                self.txtSearch.becomeFirstResponder()
                
            }
            cell.lblPostDesc.handleURLTap { (url) in
                print(url)
                //                    UIApplication.shared.open(url)
                
                if url.absoluteString.contains("familheey"){
                    self.openAppLink(url: url.absoluteString)
                    //                        Helpers.openAppLinkCommon(url: url.absoluteString, currentVC: self)
                }
                else{
                    if !(["http", "https"].contains(url.scheme?.lowercased())) {
                        var strUrl = url.absoluteString
                        strUrl = "http://"+strUrl
                        let Turl = URL(string: strUrl)
                        
                        let vc = SFSafariViewController(url: Turl!)
                        self.present(vc, animated: true, completion: nil)
                    }
                    else{
                        let vc = SFSafariViewController(url: url)
                        self.present(vc, animated: true, completion: nil)
                    }
                }
                
            }
            
            return cell
            
            
            
            /*if let sharedusers = post["shared_user_name"].string , !sharedusers.isEmpty{
             
             
             let cellid = "SharedPostTableViewCell"
             
             let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! SharedPostTableViewCell
             
             let post = ArrayPosts[indexPath.row]
             
             cell.readmoreButton.tag = indexPath.row
             cell.buttonShare.tag = indexPath.row
             cell.buttonSharedUsers.tag = indexPath.row
             cell.btnRightMrnu.tag = indexPath.row
             //            cell.commentsButton.tag = indexPath.row
             cell.buttonComments.tag = indexPath.row
             cell.viewsButtons.tag = indexPath.row
             cell.sharedUserButton.tag = indexPath.row
             
             
             //User Details
             
             if post["common_share_count"].intValue != 0{
             
             if post["common_share_count"].intValue == 1{
             cell.labelUserName.text = "\(sharedusers) shared a post"
             }
             else if post["common_share_count"].intValue == 2{
             cell.labelUserName.text = "\(sharedusers) and 1 other shared a post"
             }
             else {
             cell.labelUserName.text = "\(sharedusers) and \(post["common_share_count"].intValue-1) others shared a post"
             }
             }
             if post["to_user_id"].stringValue == UserDefaults.standard.value(forKey: "userId") as! String{
             cell.lblShared.text = "Shared with"
             //                cell.lblShare_width.constant = 78.0
             cell.labelPostedIn.text = " you"
             }
             else{
             cell.lblShared.text = "Shared in"
             //                cell.lblShare_width.constant = 65.0
             cell.labelPostedIn.text = post["group_name"].string ?? ""
             }
             
             cell.labelUserNameInner.text = post["parent_post_created_user_name"].string ?? ""
             if post["parent_post_grp_name"].stringValue.count > 0 {
             cell.labelPostedInInner.text = post["parent_post_grp_name"].string ?? ""
             }
             else{
             cell.labelPostedInInner.text = post["privacy_type"].string ?? ""
             }
             
             
             //            cell.labelDate.text = post["createdAt"].string ?? ""
             cell.labelDate.text = HomeTabViewController.dateFormatterCommon(dateStr: post["createdAt"].stringValue)
             
             let proPic = post["parent_post_created_user_propic"].stringValue
             
             DispatchQueue.main.async {
             
             if proPic.count != 0{
             let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic
             let imgUrl = URL(string: temp)
             cell.imageviewProfilePicInner.kf.indicatorType = .activity
             
             cell.imageviewProfilePicInner.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
             }
             else{
             cell.imageviewProfilePicInner.image = #imageLiteral(resourceName: "Male Colored")
             }
             }
             cell.imageviewProfilePicInner.isTappable(id: post["parent_post_created_user_id"].intValue)
             //Post
             cell.labelPost.text = post["snap_description"].string ?? ""
             
             //Details
             cell.lableNumberOfConversations.text = post["conversation_count"].stringValue
             cell.lblNumberOfShares.text = post["shared_user_count"].stringValue
             cell.lblNumberOfViews.text = post["views_count"].stringValue
             
             if let Attachment = post["post_attachment"].array, Attachment.count != 0{
             
             cell.updateSlider(data: Attachment)
             }
             else{
             
             cell.viewPostBg.isHidden = true
             cell.viewPageControllBG.isHidden = true
             }
             
             
             
             if cell.labelPost.calculateMaxLines() > maxNumberOfLines{
             cell.labelPost.numberOfLines = maxNumberOfLines
             cell.readMoreView.isHidden = false
             
             //            tableView.layoutIfNeeded()
             }
             else{
             cell.labelPost.numberOfLines = 0
             //                cell.readMoreView.isHidden = true
             cell.readmoreButton.isHidden = true
             cell.readMore_height.constant = 5
             //            tableView.layoutIfNeeded()
             }
             cell.delegate = self
             
             
             let user = UserDefaults.standard.value(forKey: "userId") as! String
             if  user  == post["created_by"].stringValue{
             cell.viewViews.isHidden = false
             cell.viewShared.isHidden = false
             if post["conversation_enabled"].bool ?? false{
             cell.ViewComments.isHidden = false
             }
             else{
             cell.ViewComments.isHidden = true
             }
             }
             else{
             cell.viewViews.isHidden = true
             cell.viewShared.isHidden = true
             if post["conversation_enabled"].bool ?? false{
             cell.ViewComments.isHidden = false
             }
             else{
             cell.ViewComments.isHidden = true
             }
             }
             if  user  == post["created_by"].stringValue{
             cell.rightMenuVew.isHidden = false
             }
             else{
             cell.rightMenuVew.isHidden = true
             }
             if post["conversation_count_new"].intValue > 0{
             cell.lblNewConverstn.isHidden = false
             }
             else{
             cell.lblNewConverstn.isHidden = true
             }
             
             if post["is_shareable"].bool ?? false {
             
             cell.shareView.isHidden = false
             
             }
             else{
             
             cell.shareView.isHidden = true
             
             }
             cell.labelPost.handleHashtagTap { hashtag in
             //  print("Success. You just tapped the \(hashtag) hashtag")
             self.txtSearch.text  = "#"+hashtag
             self.openSearch()
             //                    self.searchview.isHidden = false
             //                    self.searchConstraintheight.constant = 36
             self.txtSearch.becomeFirstResponder()
             
             }
             cell.labelPost.handleMentionTap { (mension) in
             // print("Success. You just tapped the \(mension) mension")
             self.txtSearch.text  = mension
             self.openSearch()
             //                    self.searchview.isHidden = false
             //                    self.searchConstraintheight.constant = 36
             self.txtSearch.becomeFirstResponder()
             
             }
             cell.labelPost.handleURLTap { (url) in
             //  print(url)
             //                    UIApplication.shared.open(url)
             let vc = SFSafariViewController(url: url)
             self.present(vc, animated: true, completion: nil)
             }
             
             //            if tableView.isRowCompletelyVisible(at: indexPath){
             //
             //            }
             //            else{
             //
             //            }
             return cell
             }
             
             
             else{
             let cellid = "PostImageTableViewCell"
             
             let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! PostImageTableViewCell
             
             
             cell.readmoreButton.tag = indexPath.row
             cell.buttonShare.tag = indexPath.row
             cell.buttonRightMenu.tag = indexPath.row
             cell.commentsButton.tag = indexPath.row
             cell.viewsButton.tag = indexPath.row
             cell.sharedUsersButton.tag = indexPath.row
             cell.buttonComments.tag = indexPath.row
             
             //User Details
             cell.labelUserName.text = post["created_user_name"].string ?? ""
             cell.labelPostedIn.text = post["group_name"].string ?? "Public"
             //            cell.labelDate.text = post["createdAt"].string ?? ""
             cell.labelDate.text = HomeTabViewController.dateFormatterCommon(dateStr: post["createdAt"].stringValue)
             
             let proPic = post["propic"].string ?? ""
             
             DispatchQueue.main.async {
             
             if proPic.count != 0{
             let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic
             let imgUrl = URL(string: temp)
             cell.imageViewProfilePic.kf.indicatorType = .activity
             
             cell.imageViewProfilePic.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
             }
             else{
             cell.imageViewProfilePic.image = #imageLiteral(resourceName: "Male Colored")
             }
             }
             cell.imageViewProfilePic.isTappable(id: post["created_by"].intValue)
             //Post
             cell.labelPost.text = post["snap_description"].string ?? ""
             
             //Details
             cell.lableNumberOfConversations.text = post["conversation_count"].stringValue
             cell.lblNumberOfShares.text = post["shared_user_count"].stringValue
             cell.labelNumberOfViews.text = post["views_count"].stringValue
             
             if let Attachment = post["post_attachment"].array, Attachment.count != 0{
             //                    print("updating slide........")
             //                    print(indexPath.row)
             cell.updateSlider(data: Attachment)
             }
             else{
             //                    print("No Slide........")
             //                    print(indexPath.row)
             cell.viewPostBg.isHidden = true
             cell.viewPageControllBG.isHidden = true
             }
             
             if cell.labelPost.calculateMaxLines() > maxNumberOfLines{
             cell.labelPost.numberOfLines = maxNumberOfLines
             cell.readMoreView.isHidden = false
             
             //            tableView.layoutIfNeeded()
             }
             else{
             cell.labelPost.numberOfLines = 0
             cell.readMoreView.isHidden = true
             //            tableView.layoutIfNeeded()
             }
             cell.delegate = self
             
             
             // String(describing: UserDefaults.standard.value(forKey: "userId"))
             let user = UserDefaults.standard.value(forKey: "userId") as! String
             if  user  == post["created_by"].stringValue{
             
             cell.viewViews.isHidden = false
             cell.viewShared.isHidden = false
             if post["conversation_enabled"].bool ?? false{
             cell.viewComments.isHidden = false
             }
             else{
             cell.viewComments.isHidden = true
             }
             }
             else{
             
             cell.viewViews.isHidden = true
             cell.viewShared.isHidden = true
             if post["conversation_enabled"].bool ?? false{
             cell.viewComments.isHidden = false
             }
             else{
             cell.viewComments.isHidden = true
             }
             }
             
             if post["conversation_count_new"].intValue > 0{
             cell.lblNewConvrstn.isHidden = false
             }
             else{
             cell.lblNewConvrstn.isHidden = true
             }
             
             
             if post["is_shareable"].bool ?? false {
             
             cell.shareView.isHidden = false
             
             }
             else{
             
             cell.shareView.isHidden = true
             
             }
             
             cell.labelPost.handleHashtagTap { hashtag in
             //   print("Success. You just tapped the \(hashtag) hashtag")
             self.txtSearch.text  = "#"+hashtag
             self.openSearch()
             //                    self.searchview.isHidden = false
             //                    self.searchConstraintheight.constant = 36
             self.txtSearch.becomeFirstResponder()
             
             }
             cell.labelPost.handleMentionTap { (mension) in
             //   print("Success. You just tapped the \(mension) mension")
             self.txtSearch.text  = mension
             self.openSearch()
             //                    self.searchview.isHidden = false
             //                    self.searchConstraintheight.constant = 36
             self.txtSearch.becomeFirstResponder()
             
             }
             cell.labelPost.handleURLTap { (url) in
             //                    print(url)
             //                    UIApplication.shared.open(url)
             let vc = SFSafariViewController(url: url)
             self.present(vc, animated: true, completion: nil)
             }
             
             return cell
             }*/
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchResultTableViewCell", for: indexPath) as! SearchResultTableViewCell
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        // pagination
        /*if (selectedIndex == 3){
            if tableView.isLast(for: indexPath , arrayCount: 0) {
                
                if !isNewDataLoading && !self.stopPagination{
                    isNewDataLoading = true
                    if ArrayPosts.count != 0{
                        offset = ArrayPosts.count + 1
                    }
                    //                    print("---------------------------------------------------\(offset)")
                    callSearchApiforPost()
                }
            }
        }else*/ if (selectedIndex == 0){
            if tableView.isLast(for: indexPath, arrayCount: 0) {
                
                if !isNewDataLoading && !self.stopPagination{
                    isNewDataLoading = true
                    if FamilyList.count != 0{
                        offset = FamilyList.count + 1
                    }
                    //                    print("---------------------------------------------------\(offset)")
                    //remya
                    callSearchApi2()
                    //callSearchApi()
                }
            }
        }else if (selectedIndex == 1){
            if tableView.isLast(for: indexPath, arrayCount: 0) {
                
                if !isNewDataLoading && !self.stopPagination{
                    isNewDataLoading = true
                    if usersList.count != 0{
                        offset = usersList.count + 1
                    }
                    //                    print("---------------------------------------------------\(offset)")
                    //remya
                    callSearchApi2()
                    //callSearchApi()
                }
            }
        }else if (selectedIndex == 2){
            if tableView.isLast(for: indexPath, arrayCount: 0) {
                
                if !isNewDataLoading && !self.stopPagination{
                    isNewDataLoading = true
                    if EventList.count != 0{
                        offset = EventList.count + 1
                    }
                    //                    print("---------------------------------------------------\(offset)")
                    //remya
                    callSearchApi2()
                    //callSearchApi()
                }
            }
        }
        
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectedIndex == 0{
            return UITableView.automaticDimension
        }
        else if selectedIndex == 2{
            return UITableView.automaticDimension
        }
        else if selectedIndex == 3{
            return UITableView.automaticDimension
        }
        return UITableView.automaticDimension
    }
    
    //    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    //        if txtSearch.text!.isEmpty{
    //            return "Suggested"
    //        }
    //        else{
    //            return "Search result for \(txtSearch.text!)"
    //        }
    //    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewTemp = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 20), reuseIdentifier: "cellvew")
        let label = UILabel(frame: CGRect(x: 10, y: 0, width: viewTemp.frame.width - 16, height: 20))
        if txtSearch.text!.isEmpty{
            label.text = "Suggested"
        }
        else{
            label.text = "Search result for \"\(txtSearch.text!)\""
        }
        label.textColor = .black
        viewTemp.addSubview(label)
        viewTemp.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.9411764706, blue: 0.9450980392, alpha: 1)
        return viewTemp
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch selectedIndex {
        case 0:
            guard FamilyList.count > indexPath.row else {
                return
            }
            // tableView.deselectRow(at: indexPath, animated: true)
            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
            let gropuId = "\(String(describing: FamilyList[indexPath.row].group_id!))"
            vc.groupId = gropuId
            appDel.isFamilyFromSearch = true
            self.navigationController?.pushViewController(vc, animated: true)
            break
        //return
        case 1:
            guard usersList.count > indexPath.row  else {
                return
            }
            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "UserProfileTableViewController") as! UserProfileTableViewController
            vc.mutialFamilies =  "\(usersList[indexPath.row].familycount ?? "0")"
            vc.mutualConnections =  "\(usersList[indexPath.row].mutualfamilycount ?? "0")"
            vc.userID = "\( usersList[indexPath.row].userid ?? 0)"
            self.navigationController?.pushViewController(vc, animated: true)
            break
            
        case 2:
            guard EventList.count > indexPath.row  else {
                return
            }
            
            let stryboard = UIStoryboard.init(name: "third", bundle: nil)
            let vc = stryboard.instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
            
            // vc.eventDetails = data
            let temp = "\(EventList[indexPath.row].id)"
            vc.eventId = temp
            vc.fromCreate = false
            vc.isFromSearch = true
            self.navigationController?.pushViewController(vc, animated: true)
            break
            
        case 3:
            guard ArrayPosts.count > indexPath.row  else {
                return
            }
            
            let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
            
            let vc = storyboard.instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
            vc.postId = ArrayPosts[indexPath.row]["post_id"].stringValue
            self.navigationController?.pushViewController(vc, animated: false)
            break
        default:
            return
        }
        
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y
        tempOffset = offset
        //        if !searchview.isHidden{
        //            self.notificationHeight.constant = 0
        //            self.notificationView.isHidden = true
        //            return
        //        }
        if(scrollView.panGestureRecognizer.translation(in: scrollView.superview).y >= 0)
        {
            DispatchQueue.main.async {
                if  !self.notificationView.isHidden && self.notificationHeight.constant == self.headerHeight{
                    return
                }
                if self.notificationHeight.constant <= self.headerHeight{
                    if  self.notificationView.isHidden{
                        self.notificationView.isHidden = false
                    }
                    if self.notificationHeight.constant + (self.previousPageXOffset  - offset) >= self.headerHeight || self.notificationHeight.constant + (self.previousPageXOffset  - offset) <= 0 {
                        self.notificationHeight.constant = self.headerHeight
                    }
                    else{
                        self.notificationHeight.constant = self.notificationHeight.constant + (self.previousPageXOffset  - offset)
                    }
                }
                else if self.notificationHeight.constant > self.headerHeight{
                    self.notificationHeight.constant = self.headerHeight
                }
                self.previousPageXOffset = offset
                
            }
        }
        else{
            DispatchQueue.main.async {
                if  self.notificationView.isHidden && self.notificationHeight.constant == 0{
                    return
                }
                if self.notificationHeight.constant <= self.headerHeight {
                    if self.notificationHeight.constant <= 0{
                        self.notificationHeight.constant = 0
                        if  !self.notificationView.isHidden{
                            self.notificationView.isHidden = true
                            
                        }
                    }
                    if self.notificationHeight.constant - (offset - self.previousPageXOffset) >= self.headerHeight || self.notificationHeight.constant - (offset - self.previousPageXOffset) <= 0 {
                        self.notificationHeight.constant = 0
                        self.notificationView.isHidden = true
                        
                        
                    }
                    else{
                        self.notificationHeight.constant = self.notificationHeight.constant - (offset - self.previousPageXOffset)
                        
                    }
                }
                else if self.notificationHeight.constant > self.headerHeight{
                    self.notificationHeight.constant = self.headerHeight
                }
                self.previousPageXOffset = offset
                
            }
        }
        guard let indepaths = self.tableView.indexPathsForVisibleRows else { return  }
        DispatchQueue.main.async {
            
            for index in indepaths{
                
                if let cell = self.tableView.cellForRow(at: index) as? PostImageTableViewCell{
                    cell.stopPlaying()
                    break
                }
                else if let cell = self.tableView.cellForRow(at: index) as? SharedPostTableViewCell {
                    cell.stopPlaying()
                    break
                }
            }
        }
        
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        //        if fromRefresh{
        //            fromRefresh = false
        //        }
        //        else{
        var currentCellOffset = scrollView.contentOffset
        currentCellOffset.x += (self.tableView.bounds.width);
        
        let indepath = self.tableView.indexPathsForVisibleRows?.first
        if indepath != nil{
            self.viewCurrentPost(index: (indepath?.row)!)
        }
        //        }
    }
    
    // Open App Link
    func openAppLink(url:String){
        
        let param = ["url" : url]
        
        networkProvider.request(.openAppGetParams(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("@@##!!! REsult\(jsonData)")
                    if response.statusCode == 200{
                        let dataArr = jsonData["data"]
                        let subType = dataArr["sub_type"].stringValue
                        let type =  dataArr["type"].stringValue
                        
                        if subType == notificationSubTypes.publicfeed || subType == notificationSubTypes.familyfeed && type == notificationTypes.home{
                            let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
                            vc.postId = "\(dataArr["type_id"].intValue)"
                            self.navigationController?.pushViewController(vc, animated: false)
                            
                        }
                        else if type == notificationTypes.family{
                            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                            let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
                            intro.groupId = "\(dataArr["type_id"].intValue)"
                            self.navigationController?.pushViewController(intro, animated: false)
                        }
                        else{
                            let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
                            vc.enableEventEdit = false
                            vc.isFromNotification = "true"
                            vc.eventId = "\(dataArr["type_id"].intValue)"
                            self.navigationController?.pushViewController(vc, animated: false)
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.openAppLink(url: url)
                        }
                    }
                    else{
                        
                    }
                    
                }catch _ {
                    
                    
                }
            case .failure( _):
                break
            }
        }
    }
    
    //MARK:- Post actions
    func postupdateSuccess(index:Int , SuccessData:JSON)
    {
        
    }
    
    @IBAction func onClickReadmoreAction(_ sender: UIButton) {
        
        let cell = tableView.cellForRow(at: IndexPath.init(row: sender.tag, section: 0)) as! PostImageTableViewCell
        if cell.labelPost.calculateMaxLines() > maxNumberOfLines &&  cell.labelPost.numberOfLines == maxNumberOfLines{
            
            tableView.beginUpdates()
            cell.labelPost.numberOfLines = 0
            cell.readmoreButton.setTitle("view less", for: .normal)
            tableView.endUpdates()
            
            
        }
        else if cell.labelPost.calculateMaxLines() > maxNumberOfLines &&  cell.labelPost.numberOfLines == 0 {
            UIView.performWithoutAnimation {
                tableView.beginUpdates()
                cell.labelPost.numberOfLines = maxNumberOfLines
                cell.readmoreButton.setTitle("view more", for: .normal)
                tableView.endUpdates()
            }
        }
        
    }
    
    @IBAction func onClickReadmoreShareAction(_ sender: UIButton) {
        
        let cell = tableView.cellForRow(at: IndexPath.init(row: sender.tag, section: 0)) as! SharedPostTableViewCell
        if cell.labelPost.calculateMaxLines() > maxNumberOfLines &&  cell.labelPost.numberOfLines == maxNumberOfLines{
            
            tableView.beginUpdates()
            cell.labelPost.numberOfLines = 0
            cell.readmoreButton.setTitle("view less", for: .normal)
            tableView.endUpdates()
            
            
        }
        else if cell.labelPost.calculateMaxLines() > maxNumberOfLines &&  cell.labelPost.numberOfLines == 0 {
            UIView.performWithoutAnimation {
                tableView.beginUpdates()
                cell.labelPost.numberOfLines = maxNumberOfLines
                cell.readmoreButton.setTitle("view more", for: .normal)
                tableView.endUpdates()
            }
        }
        
    }
    
    @IBAction func onClickSharedUsersShareAction(_ sender: UIButton) {
        
        /* let post = ArrayPosts[sender.tag]
         
         if let sharedusers = post["shared_user_names"].string , !sharedusers.isEmpty{
         
         let array = sharedusers.components(separatedBy: ",")
         
         showActionSheet(titleArr: array as NSArray, title: "Select option", completion: { (index) in
         
         })
         
         }*/
        let post = ArrayPosts[sender.tag]
        let mainStoryBoard = UIStoryboard(name: "fourth", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "UserFamilyListViewController") as! UserFamilyListViewController
        if post["orgin_id"].stringValue.count > 0{
            vc.postId = post["orgin_id"].stringValue
        }
        else{
            vc.postId = post["post_id"].stringValue
        }
        vc.isFrompost = true
        vc.isFrom = "groupshare"
        vc.familyID = post["to_group_id"].stringValue
        self.navigationController?.pushViewController(vc, animated:true)
    }
    
    @IBAction func onClickShareSharedPostAction(_ sender: UIButton) {
        
        /* let post = ArrayPosts[sender.tag]
         
         let mainStoryBoard = UIStoryboard(name: "third", bundle: nil)
         let vc = mainStoryBoard.instantiateViewController(withIdentifier: "PostShareViewController") as! PostShareViewController
         vc.postId = post["post_id"].stringValue
         self.navigationController?.pushViewController(vc, animated:true)*/
        self.ShareOptionArray = ["Share in Familheey","Share in Social Media"]
        showActionSheet(titleArr: ShareOptionArray as NSArray, title: "Select options") { (index) in
            if index == 0{
                let post = self.ArrayPosts[sender.tag]
                //                print(post)
                //                 DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                let mainStoryBoard = UIStoryboard(name: "third", bundle: nil)
                let vc = mainStoryBoard.instantiateViewController(withIdentifier: "PostShareViewController") as! PostShareViewController
                vc.postId = post["post_id"].stringValue
                self.navigationController?.pushViewController(vc, animated: false)
                //                }
                //                 self.navigationController?.pushViewController(vc, animated: false)
            }
            else if index == 1{
                let post = self.ArrayPosts[sender.tag]
                let urlString =
                    Helpers.showActivityViewcontroller(url: "\(BaseUrl.PostUrlPrd)\(post["post_id"].stringValue)", VC: self)
            }
            else{
                
            }
        }
    }
    
    @IBAction func onClickCommentsShareAction(_ sender: UIButton) {
        
        let post = ArrayPosts[sender.tag]
        let mainStoryBoard = UIStoryboard(name: "Conversations", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "ConversationsViewController") as! ConversationsViewController
        //               vc.post = post
        self.navigationController?.pushViewController(vc, animated:true)
    }
    
    @IBAction func onClickViewsAction(_ sender: UIButton) {
        let post = ArrayPosts[sender.tag]
        let mainStoryBoard = UIStoryboard(name: "fourth", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "UserFamilyListViewController") as! UserFamilyListViewController
        vc.postId = post["post_id"].stringValue
        vc.isFrompost = true
        vc.isFrom = "postview"
        self.navigationController?.pushViewController(vc, animated:true)
        
    }
    
    @IBAction func onClickSharedUsersAction(_ sender: UIButton) {
        let post = ArrayPosts[sender.tag]
        let mainStoryBoard = UIStoryboard(name: "fourth", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "UserFamilyListViewController") as! UserFamilyListViewController
        if post["orgin_id"].stringValue.count > 0{
            vc.postId = post["orgin_id"].stringValue
        }
        else{
            vc.postId = post["post_id"].stringValue
        }
        vc.isFrompost = true
        vc.isFrom = "usershare"
        self.navigationController?.pushViewController(vc, animated:true)
    }
    
    @IBAction func onClickCommentAction(_ sender: UIButton) {
        
        var post = ArrayPosts[sender.tag]
        post["conversation_count_new"] = "0"
        if  let cell = tableView.cellForRow(at: IndexPath.init(row: sender.tag, section: 0)) as? SharedPostTableViewCell{
            
            tableView.beginUpdates()
            cell.lblNewConverstn.isHidden = true
            tableView.endUpdates()
        }
        else if  let cell = tableView.cellForRow(at: IndexPath.init(row: sender.tag, section: 0)) as? PostImageTableViewCell{
            tableView.beginUpdates()
            cell.lblNewConvrstn.isHidden = true
            tableView.endUpdates()
        }
        
        let mainStoryBoard = UIStoryboard(name: "Conversations", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "ConversationsViewController") as! ConversationsViewController
        vc.post = post
        vc.conversationT = .post
        self.navigationController?.pushViewController(vc, animated:true)
    }
    
    @IBAction func onClickPostMenu(_ sender: UIButton) {
        let post = ArrayPosts[sender.tag]
        
        postoptionsTittle.removeAll()
        
        
        if post["created_by"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")" {
            //                  postoptionsTittle =  ["Edit post","Share social media","Delete post"]
            if let sharedusers = post["shared_user_name"].string , !sharedusers.isEmpty{
                postoptionsTittle =  ["Delete"]
            }
            else{
                postoptionsTittle =  ["Edit","Delete"]
            }
            
            
        }
        else{
            //                  postoptionsTittle =  ["Remove Post","Share social media","Report"]
            postoptionsTittle =  ["Hide","Report"]
            
        }
        
        if post["muted"].stringValue == "true"{
            postoptionsTittle.insert("Unmute", at: 0)
        }
        else{
            postoptionsTittle.insert("Mute", at: 0)
        }
        
        showActionSheet(titleArr: postoptionsTittle as NSArray, title: "Select options") { (index) in
            
            if index == 2{
                if post["created_by"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")"{
                    self.deletePostForCreator(tag: sender.tag)
                }
                else
                {
                    let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "ReportPostViewController") as! ReportPostViewController
                    
                    vc.postDetails =  self.ArrayPosts[sender.tag]
                    vc.isFrom = "post"
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }
            }
            else if index == 1{
                if post["created_by"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")"{
                    if let sharedusers = post["shared_user_name"].string , !sharedusers.isEmpty{
                        self.deletePostForCreator(tag: sender.tag)
                    }
                    else{
                        let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "CreatePostViewController") as! CreatePostViewController
                        
                        vc.fromEdit = true
                        vc.editPostDetails = post
                        vc.callBackDelegate = self
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    
                }else{
                    self.removePostFromTimeline(tag: sender.tag)
                }
            }
            else if index == 100{
            }
                //                  else if index == 2{
                //                      Helpers.showActivityViewcontroller(url: "http://www.sampleurl.com", VC: self)
                //                  }
            else{
                self.muteConversation(tag: sender.tag)
            }
        }
    }
    
    @IBAction func onClickShareAction(_ sender: UIButton) {
        self.ShareOptionArray = ["Share in Familheey","Share in Social Media"]
        
        showActionSheet(titleArr: ShareOptionArray as NSArray, title: "Select options") { (index) in
            if index == 0{
                let post = self.ArrayPosts[sender.tag]
                //                print(post)
                
                let mainStoryBoard = UIStoryboard(name: "third", bundle: nil)
                let vc = mainStoryBoard.instantiateViewController(withIdentifier: "PostShareViewController") as! PostShareViewController
                vc.postId = post["post_id"].stringValue
                self.navigationController?.pushViewController(vc, animated:true)
            }
            else if index == 1{
                let post = self.ArrayPosts[sender.tag]
                let urlString =
                    Helpers.showActivityViewcontroller(url: "\(BaseUrl.PostUrlPrd)\(post["post_id"].stringValue)", VC: self)
                
                
                //                     Helpers.showActivityViewcontroller(url: "http://www.sampleurl.com", VC: self)
            }
            else{
                
            }
        }
    }
    
}
var vSpinner : UIView?

extension UIViewController {
    
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            vSpinner?.removeFromSuperview()
            vSpinner = nil
        }
    }
}
extension SearchTabViewController:UIGestureRecognizerDelegate{
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if let swipeGesture = gestureRecognizer as? UISwipeGestureRecognizer {
            let btn = UIButton()
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.right:
                //                print("Swiped right")
                
                
                
                if selectedIndex == 3{
                    btn.tag = 102
                    self.onClickTabSelection(btn)
                    
                }
                else if selectedIndex == 2{
                    btn.tag = 101
                    self.onClickTabSelection(btn)
                    
                }
                else if selectedIndex == 1{
                    btn.tag = 100
                    self.onClickTabSelection(btn)
                    
                }
                
                
            case UISwipeGestureRecognizer.Direction.left:
                
                if selectedIndex == 0{
                    btn.tag = 101
                    self.onClickTabSelection(btn)
                    
                }
                else if selectedIndex == 1{
                    btn.tag = 102
                    self.onClickTabSelection(btn)
                }
                else if selectedIndex == 2{
                    btn.tag = 103
                    self.onClickTabSelection(btn)
                }
                
            //                print("Swiped left")
            default:
                break
            }
        }
        return true
    }
    
}
extension SearchTabViewController : PostImageTableViewCellDelegate,UIDocumentInteractionControllerDelegate{
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    func share(url: URL) {
        print(url)
        documentInteractionController = UIDocumentInteractionController(url: url)
        //        documentInteractionController.uti = url.typeIdentifier ?? "public.data, public.content"
        //        documentInteractionController.name = url.localizedName ?? url.lastPathComponent
        documentInteractionController.delegate = self
        documentInteractionController.presentPreview(animated: true)
    }
    
    func gotoPreview(data: [JSON],index:Int){
        
        if let type = data[index]["type"].string ,( type.contains("text") || type.contains("pdf") || type.contains("doc") || type.contains("docx") || type.contains("xls") || type.contains("xlsx") || type.contains("msword") || type.contains("txt")) {
            var temp:String!
            temp = "\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+data[index]["filename"].stringValue
            let url = URL(string: temp)!
             ActivityIndicatorView.show("Loading....")
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else { return }
                let tmpURL = FileManager.default.temporaryDirectory
                    .appendingPathComponent(response?.suggestedFilename ?? "fileName.png")
                do {
                    try data.write(to: tmpURL)
                } catch {
                    print(error)
                    ActivityIndicatorView.hiding()
                }
                DispatchQueue.main.async {
                    /// STOP YOUR ACTIVITY INDICATOR HERE
                    ActivityIndicatorView.hiding()
                    print(tmpURL)
                    self.share(url: tmpURL)
                }
            }.resume()
            /*let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
            
            let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementDocPreviewViewController") as! AnnouncementDocPreviewViewController
            vc.UrlToLoad = temp
            self.present(vc, animated: true, completion: nil)*/
        }
        else{
            let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsPreviewViewController") as! AnnouncementsPreviewViewController
            vc.attachments = data
            vc.isFromPost = true
            vc.selectedIndex = index
            self.navigationController?.addFadeAnimation()
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    //    {
    //
    //        let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
    //        let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsPreviewViewController") as! AnnouncementsPreviewViewController
    //        vc.attachments = data
    //        vc.isFromPost = true
    //        vc.selectedIndex = index
    //        self.navigationController?.addFadeAnimation()
    //        self.navigationController?.pushViewController(vc, animated: false)
    //    }
    
    func gotoSinglePreview(data: String) {
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: "ChangePicturesViewController") as! ChangePicturesViewController
        vc.isCoverOrProfilePic = "CoverPic"
        vc.ids           = UserDefaults.standard.value(forKey: "userId") as! String
        vc.isFrom = "post"
        vc.inFor = "cover"
        vc.isAdmin = ""
        vc.imageUrl = data
        self.imgClicked = true
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

extension SearchTabViewController : SharedPostTableViewCellDelegate{
    
    func gotoSharedPreview(data: [JSON], index: Int){
        
        if let type = data[index]["type"].string ,( type.contains("text") || type.contains("pdf") || type.contains("doc") || type.contains("docx") || type.contains("xls") || type.contains("xlsx") || type.contains("msword") || type.contains("txt")) {
            var temp:String!
            temp = "\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+data[index]["filename"].stringValue
           let url = URL(string: temp)!
             ActivityIndicatorView.show("Loading....")
           URLSession.shared.dataTask(with: url) { data, response, error in
               guard let data = data, error == nil else { return }
               let tmpURL = FileManager.default.temporaryDirectory
                   .appendingPathComponent(response?.suggestedFilename ?? "fileName.png")
               do {
                   try data.write(to: tmpURL)
               } catch {
                   print(error)
                    ActivityIndicatorView.hiding()
               }
               DispatchQueue.main.async {
                   /// STOP YOUR ACTIVITY INDICATOR HERE
                    ActivityIndicatorView.hiding()
                   print(tmpURL)
                   self.share(url: tmpURL)
               }
           }.resume()
            
            /*let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
            
            let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementDocPreviewViewController") as! AnnouncementDocPreviewViewController
            vc.UrlToLoad = temp
            self.present(vc, animated: true, completion: nil)*/
        }
        else{
            let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsPreviewViewController") as! AnnouncementsPreviewViewController
            vc.attachments = data
            vc.isFromPost = true
            vc.selectedIndex = index
            self.navigationController?.addFadeAnimation()
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    //    {
    //
    //        let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
    //        let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsPreviewViewController") as! AnnouncementsPreviewViewController
    //        vc.attachments = data
    //        vc.isFromPost = true
    //        vc.selectedIndex = index
    //        self.imgClicked = true
    //        self.navigationController?.addFadeAnimation()
    //        self.navigationController?.pushViewController(vc, animated: false)
    //    }
    
    func gotoSharedSinglePreview(data: String)
    {
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: "ChangePicturesViewController") as! ChangePicturesViewController
        vc.isCoverOrProfilePic = "CoverPic"
        vc.ids           = UserDefaults.standard.value(forKey: "userId") as! String
        vc.isFrom = "post"
        vc.inFor = "cover"
        vc.isAdmin = ""
        self.imgClicked = true
        vc.imageUrl = data
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}
