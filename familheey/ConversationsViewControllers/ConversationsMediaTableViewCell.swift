//
//  ConversationsMediaTableViewCell.swift
//  familheey
//
//  Created by Giri on 29/11/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class ConversationsMediaTableViewCell: UITableViewCell {
    @IBOutlet weak var imageviewMedia: UIImageView!
    @IBOutlet weak var buttonMedia: UIButton!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var playIcon: UIImageView!
    @IBOutlet weak var viewOfMultipleImages: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
class ConversationsMediaReceivedTableViewCell: UITableViewCell {
    @IBOutlet weak var imageviewMedia: UIImageView!
    @IBOutlet weak var buttonMedia: UIButton!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imageViewUserPropic: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var playIcon: UIImageView!
    @IBOutlet weak var viewOfMultipleImage: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
