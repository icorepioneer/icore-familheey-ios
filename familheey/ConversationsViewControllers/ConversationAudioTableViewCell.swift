//
//  ConversationAudioTableViewCell.swift
//  familheey
//
//  Created by Giri on 21/01/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit
import AVKit

protocol ConversationAudioDelegate:class {
    func playerStatusChange(isPlaying:Bool, at index:Int)
}

class ConversationAudioTableViewCell: UITableViewCell {
    @IBOutlet weak var imageViewUserProPic: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var sliderPlayer: UISlider!
    weak var delegate : ConversationAudioDelegate?
    var currentURL = ""
    var currentIndex :Int?
    var progressTimer:Timer?
    {
        willSet {
            progressTimer?.invalidate()
        }
    }
    var playerStream: AVPlayer?
    var playerItem: AVPlayerItem?
    @IBOutlet weak var playerView: UIView!
    
    @IBOutlet weak var buttonImage: UIImageView!
    @IBOutlet weak var nameToLeftConstarint: NSLayoutConstraint!
    
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    var playbackLikelyToKeepUpKeyPathObserver: NSKeyValueObservation?
    var playbackBufferEmptyObserver: NSKeyValueObservation?
    var playbackBufferFullObserver: NSKeyValueObservation?

    private func observeBuffering() {

        let playbackLikelyToKeepUpKeyPath = \AVPlayerItem.isPlaybackLikelyToKeepUp
        playbackLikelyToKeepUpKeyPathObserver = playerItem?.observe(playbackLikelyToKeepUpKeyPath, options: [.new]) { [weak self] (_, _) in
            
            if (self?.playerStream?.currentItem!.isPlaybackLikelyToKeepUp)! {
                self?.loader.isHidden = true
                self?.loader.stopAnimating()
            }
            else {
                self?.loader.isHidden = false
                self?.loader.startAnimating()
            }
    
        }
    }

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    @IBAction func playAction(_ sender: Any) {
        
        if let playerStream = playerStream
        {
            //            if let url = playerStream.getVideoUrl() , url.absoluteString != currentURL {
            //                stopProgressTimer()
            //                playerStream.pause()
            //
            //            }
            
            if playerStream.isPlaying
            {
                pause()
            }
            else
            {
                play()
                
            }
        }
    }
    func play(){
        if let playerStream = playerStream{
            buttonImage.isHighlighted = true
            startProgressTimer()
            playerStream.play()
        }
    }
    func pause(){
        if let playerStream = playerStream{
            buttonImage.isHighlighted = false
            stopProgressTimer()
            playerStream.pause()
        }
    }
    
    func playerStream(urlStream : String, for index: Int)
    {
        if let urlStr = urlStream.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        {
            
            if let TempURL = URL.init(string: urlStr)
            {
                playerItem  = AVPlayerItem(url: TempURL)
                playerStream = AVPlayer(playerItem: playerItem)
//                observeBuffering()
                NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidPlayToEndTime), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
                currentIndex = index
            }
            
        }
    }
    
    @objc func playerItemDidPlayToEndTime() {
        buttonImage.isHighlighted = false

        stopProgressTimer()
        self.sliderPlayer.value = 0.0
        if let playerStream = self.playerStream
        {
            playerStream.replaceCurrentItem(with: playerItem)
            playerStream.seek(to: .zero)
        }
        
    }
    
    func stopProgressTimer() {
        progressTimer?.invalidate()
        progressTimer = nil
        delegate?.playerStatusChange(isPlaying: false, at: currentIndex ?? 0)
    }
    
    func startProgressTimer()
    {
        delegate?.playerStatusChange(isPlaying: true, at: currentIndex ?? 0)
        progressTimer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true){_ in
            self.updateProgressTimer()
        }
        
    }
    
    @objc func updateProgressTimer()
    {
        if let playerItem = playerItem
        {
            if let pa = playerStream
            {
                let floatTime = Float(CMTimeGetSeconds(pa.currentTime()))
                let floatTimeDu = Float(CMTimeGetSeconds(playerItem.duration))
                
                sliderPlayer.value = floatTime / floatTimeDu
            }
        }
    }
    
}
extension AVPlayer {
    var isPlaying: Bool {
        return rate != 0 && error == nil
    }
    
    func getVideoUrl() -> URL? {
        let asset = self.currentItem?.asset
        if asset == nil {
            return nil
        }
        if let urlAsset = asset as? AVURLAsset {
            return urlAsset.url
        }
        return nil
    }
    
    
}

