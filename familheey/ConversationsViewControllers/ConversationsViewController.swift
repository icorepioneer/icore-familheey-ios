//
//  ConversationsViewController.swift
//  familheey
//
//  Created by Giri on 28/11/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON
import SocketIO
import YPImagePicker
import AVKit
import MobileCoreServices
import PDFKit
import SafariServices
import Kingfisher
import DKImagePickerController


enum conversationType : Int{
    case post = 0
    case announcement = 1
    case publicpost = 3
    case none = 2
    
}

class ConversationsViewController: UIViewController,AVAudioRecorderDelegate,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var heightOfTxtView: NSLayoutConstraint!
    @IBOutlet weak var botomvConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var heightOfOuterView: NSLayoutConstraint!
    var pickerController: DKImagePickerController!
    @IBOutlet weak var messageTextView: UITextView!
    let messageTextViewMaxHeight: CGFloat = 100
    @IBOutlet weak var tableviewConversations: UITableView!
    @IBOutlet weak var lblConversationTitle: UILabel!
    @IBOutlet weak var imageviewConversation: UIImageView!
    @IBOutlet weak var imgView: UIView!
    @IBOutlet weak var imgView_width: NSLayoutConstraint!
    
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    var arrayConversations = [JSON]()
    var manager : SocketManager!
    var  socket: SocketIOClient!
    var initiallimit = 20
    var offset = 0
    var limit = 0
    var lastFetchedIndex = 0;
    var envStr : Any = NetworkManager.environment
    var post = JSON()
    var selectedItems : [YPMediaItem] = []
    var selectedAttachments = NSMutableArray()
    
    var imgDataArr = [Data]()
    var videoDataArr = [Data]()
    var documentDataArr = [Data]()
    
    var pathExt = ""
    var postString = ""
    var arrayofCID = [String]()
    //    var postoptions = ["Media","Document"]
    var postoptions = ["Photos","Videos","Document"]
    
    var documentInteractionController: UIDocumentInteractionController!
    var unreadViewShown = false
    var unreadIndex : Int? = nil
    var isScrolling = false
    @IBOutlet weak var viewExternalDate: UIView!
    @IBOutlet weak var labelExternalDate: UILabel!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var recorderView: RecordView!
    @IBOutlet weak var sendButtonView: UIView!
    @IBOutlet weak var recorderButtonView: UIView!
    @IBOutlet weak var typingView: UIView!
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    @IBOutlet weak var recordButton: RecordButton!
    var conversationT: conversationType?
    var typeID = ""
    var timer : Timer?
    
    var currentPlayingIndex : Int?
    //
    @IBOutlet weak var quoteView: UIView!
    @IBOutlet weak var quoteImageViewBG: UIView!
    @IBOutlet weak var quoteImageView: UIImageView!
    @IBOutlet weak var quoteLabel: UILabel!
    @IBOutlet weak var quoteUserDetailsLabel: UILabel!
    @IBOutlet weak var quoteLeft: UILabel!
    @IBOutlet weak var quoteRight: UILabel!
    var QuotedConv : JSON?
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        pickerController = DKImagePickerController()
        self.tableviewConversations.delegate = self
        self.tableviewConversations.dataSource = self
        let socketStr = BaseUrl.socketUrl()
        manager = SocketManager(socketURL: URL(string: socketStr)!, config: [.log(true), .compress])
        socket = manager.defaultSocket
        print(socket ?? "")
        
        
        socket.on(clientEvent: .connect) {data, ack in
            print(data)
            print("socket connected")
        }
        
        socket.on(clientEvent: .error) { (data, eck) in
            print(data)
            print("socket error")
        }
        
        socket.on(clientEvent: .disconnect) { (data, eck) in
            print(data)
            print("socket disconnect")
        }
        
        socket.on(clientEvent: SocketClientEvent.reconnect) { (data, eck) in
            print(data)
            print("socket reconnect")
        }
        
        
        socket.on("post_channel_\(post["post_id"].stringValue)") {data, ack in
            
            let jsonData =  JSON(data[0])
            print(jsonData)
            
            if let type = jsonData[0]["type"].string, type.lowercased() == "delete_comment", let Cids = jsonData[0]["delete_id"].array{
                print("Deleted",Cids)
                
                for items in Cids{
                    
                    //                    let filtered = JSON(self.arrayConversations).arrayValue.filter({
                    //                        $0["comment_id"].arrayValue.map({ $0.stringValue }).contains(items.stringValue)
                    //                    })
                    //                    print ("filterdData: \(filtered)")
                    //
                    //                    if let indexe = self.arrayConversations.firstIndex(of: filtered[0]) {
                    //                        self.deleteRow(index: [IndexPath.init(row: indexe, section: 0)])
                    //
                    //                    }
                    
                    if let indexe = self.arrayofCID.firstIndex(of: items.stringValue), indexe >= 0{
                        self.deleteRow(index: [IndexPath.init(row: indexe, section: 0)])
                    }
                    
                }
                
                //                let searchPredicate = NSPredicate(format: "comment_id CONTAINS[C] %@", Cid)
                //                let  arrrDict = self.arrayConversations.filter { searchPredicate.evaluate(with: $0) };
            }
            else{
                self.arrayConversations.append(jsonData[0])
                if let Cid = jsonData[0]["comment_id"].int{
                    self.arrayofCID.append("\(Cid)")
                }
                let index = IndexPath.init(row: self.arrayConversations.count-1, section: 0)
                self.tableviewConversations.insertRows(at: [index], with: .bottom)
                self.tableviewConversations.scrollToRow(at: index, at: .bottom, animated: true)
            }
            self.appDelegate.isConversationUpdated = true
            
        }
        
        lblConversationTitle.text =  post["snap_description"].string ?? ""
        if let attachments = post["post_attachment"].array{
            
            if attachments.count != 0{
                imageviewConversation.isHidden = false
                imgView.isHidden = false
                 imgView_width.constant = 40
                let attachment = attachments[0]
                if let type = attachment["type"].string ,type.contains("image"){
                    let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+attachment["filename"].stringValue
                    imageviewConversation.kf.indicatorType = .activity
                    imageviewConversation.kf.setImage(with: URL.init(string: temp), placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                    
                }
                else{
                    imageviewConversation.image = #imageLiteral(resourceName: "videoThumbnail")
                }
            }
            else{
                imageviewConversation.image = #imageLiteral(resourceName: "Family Logo")
                imageviewConversation.isHidden = true
                imgView.isHidden = true
                imgView_width.constant = 0
            }
        }
       
        setupRecorder()
        messageTextView.delegate = self
        settextview()
        getConversation()
        appDelegate.currentConversationId = post["post_id"].stringValue
        
        
        addGuesture()
        activeChatApi()
        
        if conversationT == .post{
            appDel.iscreation = isFromCreation.myfamily
        }
        else if conversationT == .publicpost{
            appDel.iscreation = isFromCreation.publicfeed
        }
        self.quoteView.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        envStr = NetworkManager.environment
        self.tabBarController?.tabBar.isHidden = true
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        scheduledTimerWithTimeInterval()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        socket.connect()
    }
    override func viewWillDisappear(_ animated: Bool) {
        if (currentPlayingIndex != nil){
            if let tableViewCell = tableviewConversations.cellForRow(at: IndexPath(row: currentPlayingIndex!, section: 0)) as? ConversationAudioTableViewCell{
                tableViewCell.pause()
            }
        }
        socket.disconnect()
        NetworkManager.environment = envStr as! APIEnvironment
        appDelegate.currentConversationId = ""
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        stoptimer()
        self.deactiveChatApi()
        
    }
    
    //MARK:- timer
    
    func scheduledTimerWithTimeInterval(){
        guard timer == nil else { return }
        timer = Timer.scheduledTimer(timeInterval: 20, target: self, selector: #selector(self.updateCounting), userInfo: nil, repeats: true)
    }
    func stoptimer(){
        timer?.invalidate()
        timer = nil
    }
    //MARK: timer trigger
    
    @objc func updateCounting(){
        self.activeChatApi()
    }
    func downloadDocumentFromUrl(index:Int){
        
        let conversation = arrayConversations[index]
        print(conversation)
        let attachments = conversation["attachment"].array
        
        //        let attachment = attachments[0]
        
        var temp:String!
        ActivityIndicatorView.show("Please wait downloading.......")
        
        temp = "\(Helpers.imageURl)"+"\(BaseUrl.conversation)"+attachments![0]["filename"].stringValue
        
        
        let fileURL = URL(string: temp)!
        let dateStr = "\(Date().timeIntervalSince1970)"
        let fileName1 = String((fileURL.lastPathComponent)) as NSString
        //        let tempStr = dateStr.trimmingCharacters(in: .whitespacesAndNewlines)
        //        let strArr = tempStr.components(separatedBy: "+")
        let dateArr = dateStr.components(separatedBy: ".")
        var strDateTemp = ""
        if dateArr.count > 0{
            strDateTemp = dateArr[0]
        }
        else{
            strDateTemp = dateStr
        }
        let fileName =  strDateTemp + "_" + (fileName1 as String)
        
        // Create destination URL
        let documentsUrl:URL =  (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL?)!
        let destinationFileUrl = documentsUrl.appendingPathComponent("\(fileName)")
        //Create URL to the source file you want to download
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        let request = URLRequest(url:fileURL)
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            if let tempLocalUrl = tempLocalUrl, error == nil {
                // Success
                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                    print("Successfully downloaded. Status code: \(statusCode)")
                    
                }
                ActivityIndicatorView.hiding()
                //self.displayAlertChoice(alertStr: "Please choose where to save image", title: "") { (result) in
                //  self.saveToDevice(documentsUrl: documentsUrl, tempLocalUrl: tempLocalUrl, destinationFileUrl: destinationFileUrl)
                //}
                do {
                    try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                    
                    print("tempLocalUrl : \(tempLocalUrl)")
                    print("destinationFileUrl : \(destinationFileUrl)")
                    
                    do {
                        //Show UIActivityViewController to save the downloaded file
                        let contents  = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
                        for indexx in 0..<contents.count {
                            if contents[indexx].lastPathComponent == destinationFileUrl.lastPathComponent {
                                
                                if FileManager.default.fileExists(atPath: destinationFileUrl.absoluteString) {
                                    // Delete file
                                    try FileManager.default.removeItem(atPath: destinationFileUrl.absoluteString)
                                } else {
                                    print("File does not exist")
                                }
                                
                                DispatchQueue.main.async {
                                    
                                    let activityViewController = UIActivityViewController(activityItems: [contents[indexx]], applicationActivities: nil)
                                    self.present(activityViewController, animated: true, completion: nil)
                                }
                                
                            }
                        }
                    }
                    catch (let err) {
                        print("error: \(err)")
                    }
                    
                } catch (let writeError) {
                    print("Error creating a file \(destinationFileUrl) : \(writeError)")
                }
                
            } else {
                
                print("Error took place while downloading a file. Error description: \(error?.localizedDescription ?? "")")
            }
        }
        task.resume()
        
    }
    //MARk:- guesture
    func addGuesture(){
        let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        lpgr.minimumPressDuration = 0.5
        lpgr.delaysTouchesBegan = true
        lpgr.delegate = self
        self.tableviewConversations.addGestureRecognizer(lpgr)
    }
    //MARK: - UILongPressGestureRecognizer Action -
    @objc func handleLongPress(gestureReconizer: UILongPressGestureRecognizer) {
        if gestureReconizer.state != UIGestureRecognizer.State.ended {
            self.view.endEditing(true)
            var arraytittles = ["Delete","Copy","Forward","Quote"]
            let touchPoint = gestureReconizer.location(in: self.tableviewConversations)
            if let indexPath = tableviewConversations.indexPathForRow(at: touchPoint) {
                let conversation = arrayConversations[indexPath.row]
                if "\(UserDefaults.standard.value(forKey: "userId") ?? "0")" != conversation["commented_by"].stringValue {
                    arraytittles.removeFirst()
                }
                if let attachments = conversation["attachment"].array, attachments.count != 0{
                    arraytittles.remove(at: arraytittles.firstIndex(of:"Copy")!)
                    arraytittles.remove(at: arraytittles.firstIndex(of:"Quote")!)
                    if let type = attachments[0]["type"].string ,type.contains("audio"){
                        arraytittles.remove(at: arraytittles.firstIndex(of:"Forward")!)
                        arraytittles.append("Download")
                    }
                }
                if let id = conversation["comment_id"].int{
                    
                    showActionSheet(titleArr: arraytittles as NSArray, title: "Select options") { (selection) in
                        if selection == 0{
                            if arraytittles[selection] == "Delete"{
                                let alert = UIAlertController(title: "Familheey",
                                                              message: "Do you really want to delete this conversation?",
                                                              preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "YES",
                                                              style: .destructive,
                                                              handler: { _ in
                                                                self.deleteConversation(id: ["\(id)"], index: [indexPath])
                                }))
                                alert.addAction(UIAlertAction(title: "NO",
                                                              style: .cancel,
                                                              handler: nil))
                                self.present(alert, animated: true,
                                             completion: nil)
                            }
                            else if arraytittles[selection] == "Copy"{
                                UIPasteboard.general.string = conversation["comment"].string ?? ""
                            }
                            else if arraytittles[selection] == "Forward"{
                                self.forwardAction(Conv: conversation)
                            }
                            else if arraytittles[selection] == "Quote"{
                                self.quoteAction(Conv: conversation)
                            }
                            else if arraytittles[selection] == "Download"{
                                self.downloadDocumentFromUrl(index:indexPath.row)
                            }
                        }
                        else if selection == 1{
                            if arraytittles[selection] == "Copy"{
                                UIPasteboard.general.string = conversation["comment"].string ?? ""
                            }
                            else if arraytittles[selection] == "Forward"{
                                self.forwardAction(Conv: conversation)
                            }
                            else if arraytittles[selection] == "Quote"{
                                self.quoteAction(Conv: conversation)
                            }
                            else if arraytittles[selection] == "Download"{
                                self.downloadDocumentFromUrl(index:indexPath.row)
                            }
                        }
                        else if selection == 2{
                            if arraytittles[selection] == "Forward"{
                                self.forwardAction(Conv: conversation)
                            }
                            else if arraytittles[selection] == "Quote"{
                                self.quoteAction(Conv: conversation)
                            }
                        }
                        else if selection == 3{
                            if arraytittles[selection] == "Quote"{
                                self.quoteAction(Conv: conversation)
                            }
                        }
                    }
                }
                
            }
            
        }
        
    }
    func quoteAction(Conv:JSON){
        self.QuotedConv = Conv
        quoteView.isHidden = false
        if let attachments = Conv["attachment"].array, attachments.count != 0{
            self.quoteLeft.isHidden = true
            self.quoteRight.isHidden = true
            if let type = attachments[0]["type"].string ,type.contains("image"){
                
            }
        }
        else{
            self.quoteLeft.isHidden = false
            self.quoteRight.isHidden = false
            self.quoteLabel.text = Conv["comment"].string ?? ""
            self.messageTextView.becomeFirstResponder()
            self.quoteUserDetailsLabel.text = "\(Conv["full_name"].string ?? "Unknown") \(ConversationsViewController.formatDateString(dateStr: Conv["createdAt"].stringValue))"
        }
    }
    func closeQuoteAction(){
        self.QuotedConv = nil
        quoteView.isHidden = true
    }
    
    func QuoteActive()-> Bool{
        if self.quoteView.isHidden{
            return false
        }
        else{
            return true
        }
    }
    
    func forwardAction(Conv:JSON){
        var dic = [String:Any]()
        if let attachments = Conv["attachment"].array, attachments.count != 0{
            let attachment = attachments[0]
            
            if let type = attachment["type"].string ,type.contains("image"){
                
                let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.conversation)"+attachment["filename"].stringValue
                
                KingfisherManager.shared.retrieveImage(with: URL.init(string: temp)!, options: nil, progressBlock: nil) { (image, error, Ctype, url) in
                    if let imge = image as UIImage?{
                        dic["image"] = imge.pngData()
                        dic["imageURL"] = attachment["filename"].stringValue
                        dic["type"] = type
                        
                        let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "CreatePostViewController") as! CreatePostViewController
                        vc.isFromForward = true
                        vc.forwardDic = dic
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    
                }
                
            }
            else if let type = attachment["type"].string ,type.contains("video"){
                
                dic["type"] = type
                dic["videoURL"] = attachment["filename"].stringValue
                //  dic["videoThumb"] =
                
                let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "CreatePostViewController") as! CreatePostViewController
                vc.isFromForward = true
                vc.forwardDic = dic
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else{
                
                dic["type"] = attachment["type"].string ?? ""
                dic["docURL"] = attachment["filename"].stringValue
                let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "CreatePostViewController") as! CreatePostViewController
                vc.isFromForward = true
                vc.forwardDic = dic
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
        else{
            dic["stringDesc"] = Conv["comment"].string ?? ""
            let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CreatePostViewController") as! CreatePostViewController
            vc.isFromForward = true
            vc.forwardDic = dic
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }
    //MARK:- Delete conversation from table
    func deleteRow(index: [IndexPath]){
        
        for items in index{
            if self.arrayConversations.indices.contains(items.row){
                self.arrayConversations.remove(at: items.row)
                self.arrayofCID.remove(at: items.row)
                
                DispatchQueue.main.async{
                    if self.tableviewConversations!.hasRow(at: items){
                        //                self.tableviewConversations.beginUpdates()
                        //                self.tableviewConversations.deleteRows(at: index, with: .automatic)
                        //                self.tableviewConversations.endUpdates()
                        self.tableviewConversations.reloadData()
                    }
                }
                
            }
        }
        
    }
    
    
    //MARK:- call conversation api
    
    func deleteConversation(id:[String], index:[IndexPath]){
        self.view.endEditing(true)
        
        if let env =  envStr as? APIEnvironment, env == APIEnvironment.production{
            NetworkManager.environment = .conversation
            print(env)
        }
        else if let env =  envStr as? APIEnvironment, env == APIEnvironment.preProduction{
            NetworkManager.environment = .preProdConversation
            print(env)
        }
        
        ActivityIndicatorView.show("Please wait....")
        let param = ["user_id" : UserDefaults.standard.value(forKey: "userId") as! String,"comment_id":id,"post_id":post["post_id"].stringValue] as [String : Any]
        
        networkProvider.request(.delete_comment(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    if response.statusCode == 200{
                        print("view contents : \(jsonData)")
                        
                        //                        if let response = jsonData["data"].array{
                        //                            self.deleteRow(index: index)
                        //                        }
                    }
                        
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.deleteConversation(id: id, index: index)
                        }
                    }
                    else{
                        self.displayAlert(alertStr: "Unable to delete conversation now, please try again later", title: "")
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let _ {
                    ActivityIndicatorView.hiding()
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let _):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
        
    }
    
    
    //MARK:- keyboardWillShow
    //*********************************************************
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            if isLastVisibleCell(at: IndexPath.init(row: arrayConversations.count - 1, section: 0)){
                botomvConstraint.constant = 0 - keyboardHeight
                let deadlineTime = DispatchTime.now() + .seconds(1)
                DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                    self.tableviewConversations.scrollToRow(at: IndexPath.init(row: self.arrayConversations.count - 1, section: 0), at: .bottom, animated: true)
                }
            }
            else{
                botomvConstraint.constant = 0 - keyboardHeight
            }
        }
    }
    
    
    //MARK:- Recorder Setup
    
    func setupRecorder(){
        recordButton.recordView = recorderView
        recorderView.delegate = self
        
        recordingSession = AVAudioSession.sharedInstance()
        
        do {
            try recordingSession.setCategory(.playAndRecord, mode: .default)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        
                    } else {
                        // failed to record!
                    }
                }
            }
        } catch {
            // failed to record!
        }
        
    }
    func startRecording() {
        

        
        let audioFilename = getDocumentsDirectory().appendingPathComponent("recording.wav")
        
        let recordSettings = [AVEncoderAudioQualityKey: AVAudioQuality.min.rawValue,
                              AVEncoderBitRateKey: 16,
                              AVNumberOfChannelsKey: 2,
                              AVSampleRateKey: 44100.0] as [String : Any]
        print(audioFilename)
        
        let session = AVAudioSession.sharedInstance()
        do {
            try session.setCategory(AVAudioSession.Category.playAndRecord)
            audioRecorder = try AVAudioRecorder(url: audioFilename, settings: recordSettings as [String : AnyObject])
        } catch _ {
            print("Error")
        }
        
        audioRecorder.delegate = self
        audioRecorder.isMeteringEnabled = true
        audioRecorder.prepareToRecord()
        audioRecorder.record()
    }
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    func finishRecording(success: Bool) {
        if (audioRecorder != nil){
            audioRecorder.stop()
            audioRecorder = nil
        }
        if success {
            let url = getDocumentsDirectory().appendingPathComponent("recording.wav")
            print(url)
            do{
                let documentData = try NSData(contentsOf: url, options: NSData.ReadingOptions())
                uploadAudio(data: [(documentData as Data)])
            }
            catch{
                
            }
            
            //finish
        } else {
            
        }
    }
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
    
    func checkMicPermission() -> Bool {

        var permissionCheck: Bool = false

        switch AVAudioSession.sharedInstance().recordPermission {
        case AVAudioSessionRecordPermission.granted:
            permissionCheck = true
        case AVAudioSessionRecordPermission.denied:
            permissionCheck = false
        case AVAudioSessionRecordPermission.undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
                if granted {
                    permissionCheck = true
                } else {
                    permissionCheck = false
                }
            })
        default:
            break
        }

        return permissionCheck
    }
    
    //*********************************************************
    
    
    //MARK:- get announcement details
    
    func getAnnouncementDetails(id: String){
        
        ActivityIndicatorView.show("Please wait....")
        let param = ["type":"announcement","post_id":id,"user_id":UserDefaults.standard.value(forKey: "userId") as! String]
        
        networkProvider.request(.post_by_user(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        if let response = jsonData["data"].array{
                            
                            if response.count != 0{
                                let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementDetailViewController") as! AnnouncementDetailViewController
                                vc.ArrayAnnouncements = response
                                vc.selectedindex = IndexPath.init(row: 0, section: 0)
                                self.navigationController?.pushViewController(vc, animated: false)
                            }
                            else{
                                self.displayAlert(alertStr: "Oops! This content is no longer available.", title: "")
                            }
                        }
                        else{
                            self.displayAlert(alertStr: "Oops! This content is no longer available.", title: "")
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getAnnouncementDetails(id: id)
                        }
                    }
                    else{
                        self.displayAlert(alertStr: "Oops! This content is no longer available.", title: "")
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
        
    }
    //MARK:- Button Actions
    @IBAction func closeQuoteAction(_ sender: Any) {
        closeQuoteAction()
    }
    @IBAction func topBarbuttonAction(_ sender: Any) {
        
        if conversationT == .post || conversationT == .publicpost{
            /*     if let id = post["orgin_id"].int{
             typeID = "\(id)"
             }
             else */ if let id = post["post_id"].int{
                typeID = "\(id)"
            }
            if typeID != "" || !typeID.isEmpty{
                let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
                vc.postId = typeID
                self.navigationController?.pushViewController(vc, animated: false)
            }
        }
        else if conversationT == .announcement{
            if let id = post["post_id"].int{
                typeID = "\(id)"
            }
            if typeID != "" || !typeID.isEmpty{
                getAnnouncementDetails(id: typeID)
            }
        }
        
    }
    @IBAction func sendAction(_ sender: Any) {
        //        self.view.endEditing(true)
        
        if QuoteActive(){
            self.quoteView.isHidden = true
            postConversationWithQuote()
        }
        else{
            PostConversation()
        }
    }
    @IBAction func attachAction(_ sender: Any) {
        self.view.endEditing(true)
        closeQuoteAction()
        showActionSheet(titleArr: postoptions as NSArray, title: "Select options") { (index) in
            if index == 0{
                self.selectImage(index: index)
            }
            else if  index == 1{
                self.selectImage(index: index)
            }
            else
            {
                
                let types = [kUTTypePDF, kUTTypeText, kUTTypeRTF, kUTTypeSpreadsheet, kUTTypeGIF, "com.microsoft.word.doc" as CFString, "org.openxmlformats.wordprocessingml.document" as CFString]
                let documentPicker = UIDocumentPickerViewController(documentTypes: types as [String], in: .import)
                
                documentPicker.allowsMultipleSelection = true
                
                documentPicker.delegate = self
                
                self.present(documentPicker, animated: true)
                
                
            }
        }
        
        
        
    }
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func mediaAction(_ sender: UIButton) {
        
        let conversation = arrayConversations[sender.tag]
        
        if let attachments = conversation["attachment"].array, attachments.count != 0{
            if let type = attachments[0]["type"].string ,(type.contains("text") || type.contains("pdf") ||  type.contains("application") || type.contains("doc") || type.contains("docx") || type.contains("msword") || type.contains("txt") || type.contains("xls")){
                let temp = "\(Helpers.imageURl)"+"\(BaseUrl.conversation)"+attachments[0]["filename"].stringValue
                let url = URL(string: temp)!
                
                
                /* let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
                 let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementDocPreviewViewController") as! AnnouncementDocPreviewViewController
                 vc.UrlToLoad = temp
                 self.present(vc, animated: true, completion: nil)*/
                ActivityIndicatorView.show("Loading....")
                URLSession.shared.dataTask(with: url) { data, response, error in
                    guard let data = data, error == nil else { return }
                    let tmpURL = FileManager.default.temporaryDirectory
                        .appendingPathComponent(response?.suggestedFilename ?? "fileName.png")
                    do {
                        try data.write(to: tmpURL)
                    } catch {
                        print(error)
                        ActivityIndicatorView.hiding()
                    }
                    DispatchQueue.main.async {
                        /// STOP YOUR ACTIVITY INDICATOR HERE
                        ActivityIndicatorView.hiding()
                        print(tmpURL)
                        self.share(url: tmpURL)
                    }
                }.resume()
                
            }
            else{
                let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsPreviewViewController") as! AnnouncementsPreviewViewController
                vc.attachments = attachments
                vc.isFromConversation = true
                self.navigationController?.addFadeAnimation()
                self.navigationController?.pushViewController(vc, animated: false)
            }
        }
        
    }
    func share(url: URL) {
        print(url)
        documentInteractionController = UIDocumentInteractionController(url: url)
        //        documentInteractionController.uti = url.typeIdentifier ?? "public.data, public.content"
        //        documentInteractionController.name = url.localizedName ?? url.lastPathComponent
        documentInteractionController.delegate = self
        documentInteractionController.presentPreview(animated: true)
    }
    
    func selectImage(index:Int){
        self.selectedAttachments.removeAllObjects()
        pickerController = DKImagePickerController()
        DKImageExtensionController.unregisterExtension(for:.camera)
        
        if index == 0{
            pickerController.assetType = .allPhotos
            pickerController.maxSelectableCount = 5
        }
        else{
            pickerController.assetType = .allVideos
            pickerController.maxSelectableCount = 1
            DKImageExtensionController.registerExtension(extensionClass: CustomCameraExtension.self, for: .camera)
        }
        
        pickerController.exportsWhenCompleted = true
        
        pickerController.deselectAll()
        
        pickerController.didSelectAssets = { [unowned self] (assets: [DKAsset]) in
            
            self.imgDataArr = []
            if assets.count != 0{
                
                ActivityIndicatorView.show("Loading....")
                DKImageAssetExporter.sharedInstance.exportAssetsAsynchronously(assets: assets) { (info) in
                    //                ActivityIndicatorView.hiding()
                    for asset in assets {
                        switch asset.type {
                        case .photo:
                            var tempPath = ""
                            if let localTemporaryPath = asset.localTemporaryPath,
                                let imageData = try? Data(contentsOf: localTemporaryPath) {
                                
                                let img = UIImage.init(data: imageData)
                                let tempImg = CreatePostViewController.self.resize(img!)
                                if let fixedData = tempImg.fixedOrientation.jpegData(compressionQuality: 0.75){
                                    self.imgDataArr.append(fixedData)
                                }
                                tempPath = self.saveImageToDocumentsDirectory(image: tempImg, withName: localTemporaryPath.lastPathComponent)!
                            }
                            
                            
                            if tempPath.isEmpty{
                                self.displayAlert(alertStr: "Unable to upload file, please try again", title: "")
                            }
                            else{
                                let tempurl = URL(string: tempPath)
                                //                            self.uploadImageMultipleImages(arr: self.imgDataArr)
                                AWSS3Manager.shared.uploadVideo(videoUrl: tempurl!, folder: "file_name/", progress: { (progress) in
                                    print(progress)
                                    //                                guard let strongSelf = self else { return }
                                    
                                }) { [weak self] (uploadedFileUrl, error) in
                                    
                                    if let finalPath = uploadedFileUrl as? URL {
                                        print("Uploaded file url: " , finalPath)
                                        
                                        var dict = [String:String]()
                                        dict["filename"] = finalPath.lastPathComponent
                                        dict["type"] = "image"
                                        //                                    dict["video_thumb"] = "video_thumb/\(finalPathThumb.lastPathComponent)"
                                        self?.selectedAttachments.add(dict)
                                        print(dict)
                                        if self!.selectedAttachments.count == assets.count{
                                            self?.PostImageConversationWithAttachment(dic:self?.selectedAttachments ?? NSMutableArray())
                                        }
                                        
                                    } else {
                                        print("\(String(describing: error?.localizedDescription))")
                                        self?.displayAlert(alertStr: "Unable to upload file, please try again", title: "")
                                    }
                                }
                            }
                            
                            
                        case .video:
                            if let localTemporaryPath = asset.localTemporaryPath{
                                
                                
                                asset.fetchFullScreenImage { (image, info) in
                                    AWSS3Manager.shared.uploadImage(image: image!, file_name: "video_thumb/", progress: { (progress) in
                                        ActivityIndicatorView.show("Uploading...")
                                        
                                    }) { [weak self] (uploadedThumbUrl, error) in
                                        
                                        if let finalPathThumb = uploadedThumbUrl as? URL {
                                            
                                            print("Uploaded file url: " , finalPathThumb)
                                            AWSS3Manager.shared.uploadVideo(videoUrl: localTemporaryPath, folder: "file_name/", progress: { (progress) in
                                                print(progress)
                                                guard let strongSelf = self else { return }
                                                
                                            }) { [weak self] (uploadedFileUrl, error) in
                                                
                                                if let finalPath = uploadedFileUrl as? URL {
                                                    print("Uploaded file url: " , finalPath)
                                                    
                                                    var dict = [String:String]()
                                                    dict["filename"] = finalPath.lastPathComponent
                                                    dict["type"] = "video"
                                                    dict["video_thumb"] = "video_thumb/\(finalPathThumb.lastPathComponent)"
                                                    print(dict)
                                                    
                                                    self?.PostConversationWithAttachment(dic:dict)
                                                    
                                                } else {
                                                    print("\(String(describing: error?.localizedDescription))")
                                                    self?.displayAlert(alertStr: "Unable to upload file, please try again", title: "")
                                                }
                                            }
                                            
                                        } else {
                                            print("\(String(describing: error?.localizedDescription))")
                                            self?.displayAlert(alertStr: "Unable to upload file, please try again", title: "")
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
        }
        
        if pickerController.UIDelegate == nil {
            pickerController.UIDelegate = AssetClickHandler()
        }
        
        self.present(pickerController, animated: true) {}
        
    }
    //MARK:- Preview
    //    func gotoPreview(url: String) {
    //        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
    //
    //        let vc = storyboard.instantiateViewController(withIdentifier: "ChangePicturesViewController") as! ChangePicturesViewController
    //        vc.isCoverOrProfilePic = "CoverPic"
    //        vc.ids           = UserDefaults.standard.value(forKey: "userId") as! String
    //        vc.isFrom = "post"
    //        vc.inFor = "cover"
    //        vc.isAdmin = ""
    //
    //        vc.imageUrl = url
    //        self.navigationController?.pushViewController(vc, animated: true)
    //
    //    }
    
    //MARK:- Open App Link
    func openAppLink(url:String){
        
        let param = ["url" : url]
        
        networkProvider.request(.openAppGetParams(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("@@##!!! REsult\(jsonData)")
                    if response.statusCode == 200{
                        let dataArr = jsonData["data"]
                        let subType = dataArr["sub_type"].stringValue
                        let type =  dataArr["type"].stringValue
                        
                        if subType == notificationSubTypes.publicfeed || subType == notificationSubTypes.familyfeed && type == notificationTypes.home{
                            let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
                            vc.postId = "\(dataArr["type_id"].intValue)"
                            self.navigationController?.pushViewController(vc, animated: false)
                            
                        }
                        else if type == notificationTypes.family{
                            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                            let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
                            intro.groupId = "\(dataArr["type_id"].intValue)"
                            self.navigationController?.pushViewController(intro, animated: false)
                        }
                        else{
                            let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
                            vc.enableEventEdit = false
                            vc.isFromNotification = "true"
                            vc.eventId = "\(dataArr["type_id"].intValue)"
                            self.navigationController?.pushViewController(vc, animated: false)
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.openAppLink(url: url)
                        }
                    }
                    else{
                        
                    }
                    
                }catch _ {
                    
                    
                }
            case .failure( _):
                break
            }
        }
    }
    
    func saveImageToDocumentsDirectory(image: UIImage, withName: String) -> String? {
        if let data = image.jpegData(compressionQuality: 0.6) {
            let dirPath = getDocumentDirectoryPath()
            let imageFileUrl = URL(fileURLWithPath: dirPath.appendingPathComponent(withName) as String)
            do {
                try data.write(to: imageFileUrl)
                print("Successfully saved image at path: \(imageFileUrl)")
                return imageFileUrl.absoluteString
            } catch {
                print("Error saving image: \(error)")
            }
        }
        return nil
    }
    
    func getDocumentDirectoryPath() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory as NSString
    }
    
    //MARK: - Multiple Image Uploading
    
    func uploadImageMultipleImages(arr : [Data]){
        //        NetworkManager.environment = .conversation
        postString = ""
        if messageTextView.text != "Your words mean more. Say something.."{ //Your words mean more. Say something..
            postString = messageTextView.text
        }
        
        ActivityIndicatorView.show("Sending....")
        
        let param = ["post_id" : post["post_id"].stringValue, "comment" : postString, "commented_by" : UserDefaults.standard.value(forKey: "userId") as! String,"group_id":post["to_group_id"].stringValue]
        
        Helpers.requestWithMultipleImages(fileParamName: "file_name", urlAppendString: "posts/post_comment", imageData: arr, parameters: param, mimeType: "image/png") { (response) in
            print("Response : \(response)")
            //            let arrayOfData = response["data"].arrayValue
            //
            //            for currentdata in arrayOfData{
            //                var dict = [String:String]()
            //                dict["filename"] = currentdata["filename"].stringValue
            //                dict["type"] = currentdata["type"].stringValue
            //                print(dict)
            //
            //
            //            }
            
            ActivityIndicatorView.hiding()
        }
    }
    
    
    //MARK:- Multiple Image Uploading
    
    func uploadVideoMultiple(arr : [Data]){
        //        NetworkManager.environment = .conversation
        postString = ""
        if messageTextView.text != "Your words mean more. Say something.."{
            postString = messageTextView.text
        }
        
        ActivityIndicatorView.show("Uploading...")
        
        let param = ["post_id" : post["post_id"].stringValue, "comment" :postString, "commented_by" : UserDefaults.standard.value(forKey: "userId") as! String,"group_id":post["to_group_id"].stringValue]
        
        Helpers.requestWithMultipleImages(fileParamName: "file_name", urlAppendString: "posts/post_comment", imageData: arr, parameters: param, mimeType: "video/quicktime/m4v") { (response) in
            print("Response : \(response)")
            
            
            //               let arrayOfData = response["data"].arrayValue
            
            //                         for currentdata in arrayOfData{
            //                             var dict = [String:String]()
            //                             dict["filename"] = currentdata["filename"].stringValue
            //                             dict["type"] = currentdata["type"].stringValue
            //                             print(dict)
            //
            //
            //                         }
            ActivityIndicatorView.hiding()
            
        }
    }
    
    //MARK:- Doc uploading
    func uploadDocumentMultiple(arr : [Data]){
        //        NetworkManager.environment = .conversation
        if let env =  envStr as? APIEnvironment, env == APIEnvironment.production{
            NetworkManager.environment = .conversation
            print(env)
        }
        else if let env =  envStr as? APIEnvironment, env == APIEnvironment.preProduction{
            NetworkManager.environment = .preProdConversation
            print(env)
        }
        
        postString = ""
        if messageTextView.text != "Your words mean more. Say something.."{
            postString = messageTextView.text
        }
        
        let param = ["post_id" : post["post_id"].stringValue, "comment" :postString, "commented_by" : UserDefaults.standard.value(forKey: "userId") as! String,"group_id":post["to_group_id"].stringValue]
        
        ActivityIndicatorView.show("Uploading...")
        Helpers.requestWithMultipleImages(fileParamName: "file_name", urlAppendString: "posts/post_comment", imageData: arr, parameters: param, mimeType: pathExt) { (response) in
            print("Response : \(response)")
            
            let arrayOfData = response["data"].arrayValue
            
            for currentdata in arrayOfData{
                var dict = [String:String]()
                dict["filename"] = currentdata["filename"].stringValue
                dict["type"] = currentdata["type"].stringValue
                print(dict)
            }
            ActivityIndicatorView.hiding()
        }
    }
    //MARK:- Audio uploading
    
    func uploadAudio(data : [Data]){
        //        NetworkManager.environment = .conversation
        
        if let env =  envStr as? APIEnvironment, env == APIEnvironment.production{
            NetworkManager.environment = .conversation
            print(env)
        }
        else if let env =  envStr as? APIEnvironment, env == APIEnvironment.preProduction{
            NetworkManager.environment = .preProdConversation
            print(env)
        }
        
        postString = ""
        if messageTextView.text != "Your words mean more. Say something.."{
            postString = messageTextView.text
        }
        
        let param = ["post_id" : post["post_id"].stringValue, "comment" :postString, "commented_by" : UserDefaults.standard.value(forKey: "userId") as! String,"group_id":post["to_group_id"].stringValue]
        
        ActivityIndicatorView.show("Uploading...")
        Helpers.requestWithMultipleImages(fileParamName: "file_name", urlAppendString: "posts/post_comment", imageData: data, parameters: param, mimeType: "audio/wav") { (response) in
            print("Response : \(response)")
            
            let arrayOfData = response["data"].arrayValue
            
            for currentdata in arrayOfData{
                var dict = [String:String]()
                dict["filename"] = currentdata["filename"].stringValue
                dict["type"] = currentdata["type"].stringValue
                print(dict)
            }
            ActivityIndicatorView.hiding()
        }
    }
    
    func settextview(){
        messageTextView.text = "Your words mean more. Say something.."
        messageTextView.textColor = UIColor.lightGray
    }
    
    //MARK:- find visible cell
    func isLastVisibleCell(at indexPath: IndexPath) -> Bool {
        guard let lastIndexPath = tableviewConversations.indexPathsForVisibleRows?.last else {
            return false
        }
        return lastIndexPath == indexPath
    }
    
    //MARK:- Active chat api
    func activeChatApi(){
        let param = ["post_id" : post["post_id"].int ?? 0, "device_type" : "ios", "user_id" : UserDefaults.standard.value(forKey: "userId") as! String,"group_id":post["to_group_id"].stringValue] as [String : Any]
        
        networkProvider.request(.activatePost(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.activeChatApi()
                        }
                    }
                    
                }catch let err {
                    
                }
            case .failure( let error):
                break
            }
        }
    }
    func deactiveChatApi(){
        let param = ["post_id" : post["post_id"].int ?? 0, "device_type" : "ios", "user_id" : UserDefaults.standard.value(forKey: "userId") as! String,"group_id":post["to_group_id"].stringValue] as [String : Any]
        
        networkProvider.request(.deactivatePost(prameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.deactiveChatApi()
                        }
                    }
                    
                }catch let err {
                    
                }
            case .failure( let error):
                break
            }
        }
    }
    
    
    //MARK:-PostConversation
    func PostConversation(){
        
        
        if let env =  envStr as? APIEnvironment, env == APIEnvironment.production{
            NetworkManager.environment = .conversation
            print(env)
        }
        else if let env =  envStr as? APIEnvironment, env == APIEnvironment.preProduction{
            NetworkManager.environment = .preProdConversation
            print(env)
        }
        
        postString = ""
        if messageTextView.text != "Your words mean more. Say something.."{
            postString = messageTextView.text
            //            recorderButtonView.isHidden = false
        }
        
        postString = postString.trimmingCharacters(in: .whitespacesAndNewlines)
        messageTextView.text = ""
        if postString.isEmpty || postString == ""{  return}
        ActivityIndicatorView.show("Sending....")
        let param = ["post_id" : post["post_id"].stringValue, "comment" : postString, "commented_by" : UserDefaults.standard.value(forKey: "userId") as! String,"group_id":post["to_group_id"].stringValue]
        
        networkProvider.request(.PostComment(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.PostConversation()
                        }
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
        
        
    }
    
    func PostImageConversationWithAttachment(dic:NSMutableArray){
        if let env =  envStr as? APIEnvironment, env == APIEnvironment.production{
            NetworkManager.environment = .conversation
            print(env)
        }
        else if let env =  envStr as? APIEnvironment, env == APIEnvironment.preProduction{
            NetworkManager.environment = .preProdConversation
            print(env)
        }
        
        let temp = dic[0] as! NSDictionary
        ActivityIndicatorView.show("Sending....")
        let param = ["post_id" : post["post_id"].stringValue,
                     "comment" : "",
                     "commented_by" : UserDefaults.standard.value(forKey: "userId") as! String,
                     "group_id":post["to_group_id"].stringValue,
                     "attachment":dic,
                     "file_name":temp["filename"] as? NSString ?? "",
                     "file_type":"image"
            ] as [String : Any]
        networkProvider.request(.PostComment(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.PostImageConversationWithAttachment(dic: dic)
                        }
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                break
            }
        }
    }
    
    func PostConversationWithAttachment(dic:[String:Any]){
        
        if let env =  envStr as? APIEnvironment, env == APIEnvironment.production{
            NetworkManager.environment = .conversation
            print(env)
        }
        else if let env =  envStr as? APIEnvironment, env == APIEnvironment.preProduction{
            NetworkManager.environment = .preProdConversation
            print(env)
        }
        
        ActivityIndicatorView.show("Sending....")
        let param = ["post_id" : post["post_id"].stringValue,
                     "comment" : "",
                     "commented_by" : UserDefaults.standard.value(forKey: "userId") as! String,
                     "group_id":post["to_group_id"].stringValue,
                     "attachment":[dic],
                     "file_name":"\(String(describing: dic["filename"]!))",
            "file_type":"\(String(describing: dic["type"]!))",
            "video_thumb":"\(String(describing: dic["video_thumb"]!))"
            ] as [String : Any]
        
        networkProvider.request(.PostComment(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.PostConversationWithAttachment(dic: dic)
                        }
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                break
            }
        }
        
        
    }
    //MARK:- post conversation with quote
    func postConversationWithQuote(){
        
        if let env =  envStr as? APIEnvironment, env == APIEnvironment.production{
            NetworkManager.environment = .conversation
            print(env)
        }
        else if let env =  envStr as? APIEnvironment, env == APIEnvironment.preProduction{
            NetworkManager.environment = .preProdConversation
            print(env)
        }
        
        postString = ""
        if messageTextView.text != "Your words mean more. Say something.."{
            postString = messageTextView.text
            //            recorderButtonView.isHidden = false
        }
        
        postString = postString.trimmingCharacters(in: .whitespacesAndNewlines)
        messageTextView.text = ""
        if postString.isEmpty || postString == ""{  return}
        print(postString)
        //        self.settextview()
        
        
        ActivityIndicatorView.show("Sending....")
        let param = ["post_id" : post["post_id"].stringValue,
                     "group_id":post["to_group_id"].stringValue,
                     "comment" : postString,
                     "commented_by" : UserDefaults.standard.value(forKey: "userId") as? String ?? "",
                     "quoted_date" : QuotedConv?["createdAt"].stringValue ?? "",
                     "quoted_user" : QuotedConv?["full_name"].stringValue ?? "",
                     "quoted_item" : QuotedConv?["comment"].stringValue ?? "",
                     "quoted_id" : QuotedConv?["comment_id"].stringValue ?? ""]  as [String : Any]
        
        
        networkProvider.request(.PostComment(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.postConversationWithQuote()
                        }
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
        
        
    }
    //MARK:- get conversation
    func getConversation(){
        
        arrayConversations.removeAll()
        
        ActivityIndicatorView.show("Please wait....")
        let param = ["user_id" : UserDefaults.standard.value(forKey: "userId") as! String,"offset":"\(offset)","limit":"\(limit)","post_id":post["post_id"].stringValue]
        
        networkProvider.request(.getCommentsByPost(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        print("view contents : \(jsonData)")
                        
                        if let response = jsonData["data"].array{
                            
                            for (index,conversation) in response.enumerated() {
                                self.tableviewConversations.beginUpdates()
                                self.arrayConversations.append(conversation)
                                if let Cid = conversation["comment_id"].int{
                                    self.arrayofCID.append("\(Cid)")
                                }
                                self.tableviewConversations.insertRows(at: [IndexPath.init(row: index, section: 0)], with: .fade)
                                self.tableviewConversations.endUpdates()
                                
                                if index == response.endIndex-1 {
                                    if conversation["unread"].boolValue || conversation["unread"].stringValue.isEmpty{
                                        self.readConversation(msgId: conversation["comment_id"].stringValue)
                                    }
                                }
                                self.tableviewConversations.scrollToBottom()
                                
                            }
                            
                        }
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getConversation()
                        }
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let _ {
                    ActivityIndicatorView.hiding()
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let _):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
        
        
        
    }
    //MARK:- Read Conversarion
    
    func readConversation(msgId : String){
        
        print("**********************************************************************************")
        let param = ["user_id" : UserDefaults.standard.value(forKey: "userId") as! String,"post_id":post["post_id"].stringValue,"last_read_message_id":"\(msgId)"]
        
        networkProvider.request(.updateLastReadMessage(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.readConversation(msgId: msgId)
                        }
                    }
                    
                }catch let _ {
                }
            case .failure( let _):
                break
            }
        }
        
        
        
    }
    
    
    static func formatDateString(dateStr:String) -> String{
        
        if !dateStr.isEmpty{
            let dateFormatter              = DateFormatter()
            dateFormatter.dateFormat       = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
            let date = dateFormatter.date(from: dateStr)
            dateFormatter.dateFormat       = "MMM dd yyyy hh:mm a"
            dateFormatter.timeZone = NSTimeZone.local
            return date?.timeAgoSinceDate(toReturn: dateFormatter.string(from: date ?? Date())) ?? ""
        }
        else{
            return "a moment ago"
        }
    }
    
    func formatDateStringForTitle(dateStr:String) -> String{
        
        if !dateStr.isEmpty{
            let dateFormatter              = DateFormatter()
            dateFormatter.dateFormat       = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
            let date = dateFormatter.date(from: dateStr)
            if date != nil{
                if Calendar.current.isDateInToday(date!){
                    return "Today"
                }
                else if Calendar.current.isDateInYesterday(date!){
                    return "Yesterday"
                }
            }
            dateFormatter.dateFormat       = "MMM dd yyyy"
            dateFormatter.timeZone = NSTimeZone.local
            return dateFormatter.string(from: date ?? Date())
        }
        else{
            return ""
        }
    }
    
    func getThumbnailFromVideoUrl(urlString: String) {
        DispatchQueue.global().async {
            let asset = AVAsset(url: URL(string: urlString)!)
            let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
            assetImgGenerate.appliesPreferredTrackTransform = true
            let time = CMTimeMake(value: 1, timescale: 20)
            let img = try? assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            if img != nil {
                let frameImg  = UIImage(cgImage: img!)
                DispatchQueue.main.async(execute: {
                    // assign your image to UIImageView
                })
            }
        }
    }
    
    
}
extension ConversationsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayConversations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let conversation = arrayConversations[indexPath.row]
        
        if indexPath.row == unreadIndex {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ConversationReadTableViewCell", for: indexPath) as! ConversationReadTableViewCell
            return cell
        }
        
        labelExternalDate.text = formatDateStringForTitle(dateStr: conversation["createdAt"].stringValue)
        
        if "\(UserDefaults.standard.value(forKey: "userId") ?? "0")" == conversation["commented_by"].stringValue {
            
            
            if let attachments = conversation["attachment"].array, attachments.count != 0{
                
                let attachment = attachments[0]
                if let type = attachment["type"].string ,type.contains("audio"){
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ConversationAudioSendTableViewCell", for: indexPath) as! ConversationAudioTableViewCell
                    
                    let url = "\(Helpers.imageURl)"+"\(BaseUrl.conversation)"+attachment["filename"].stringValue
                    
                    cell.lblDate.text = ConversationsViewController.formatDateString(dateStr: conversation["createdAt"].stringValue)
                    cell.playerStream(urlStream: url, for: indexPath.row)
                    cell.delegate = self
                    
                    return cell
                    
                }
                    
                else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ConversationsMediaTableViewCell", for: indexPath) as! ConversationsMediaTableViewCell
                    cell.playIcon.isHidden = true
                    cell.viewOfMultipleImages.isHidden = true
                    if let type = attachment["type"].string ,type.contains("image"){
                        
                        let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.conversation)"+attachment["filename"].stringValue.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                        cell.imageviewMedia.kf.indicatorType = .activity
                        cell.imageviewMedia.setImageWithRetry(url: temp, tryCount: 10)
                        
                        if attachments.count > 1{
                            cell.viewOfMultipleImages.isHidden = false
                        }
                        else{
                            cell.viewOfMultipleImages.isHidden = true
                        }
                    }
                        
                    else if let type = attachment["type"].string ,type.contains("video"){
                        var temp = ""
                        if let thumb = attachment["video_thumb"].string, !thumb.isEmpty {
                            cell.playIcon.isHidden = false
                            temp = "\(Helpers.imaginaryImageBaseUrl)width=100&height=100&url=\(Helpers.imageURl)\(thumb)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                            
                            cell.imageviewMedia.kf.indicatorType = .activity
                            cell.imageviewMedia.setImageWithRetry(url: temp, tryCount: 10)
                        }
                        else{
                            cell.imageviewMedia.image = #imageLiteral(resourceName: "videoThumbnail")
                        }
                    }
                    else{
                        cell.imageviewMedia.image = #imageLiteral(resourceName: "DocumentTumb")
                    }
                    cell.buttonMedia.tag = indexPath.row
                    
                    cell.lblDate.text = ConversationsViewController.formatDateString(dateStr: conversation["createdAt"].stringValue)
                    return cell
                }
            }
            else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "ConversationTextSendTableViewCell", for: indexPath) as! ConversationTextSendTableViewCell
                
                if let qString = conversation["quoted_item"].string{
                    cell.QuotedView.isHidden = false
                    cell.lblQuotedConversation.text = "\"\(qString)\""
                    cell.lblQuotedUserDetails.text = "\(conversation["quoted_user"].string ?? "Unknown"), \(ConversationsViewController.formatDateString(dateStr: conversation["quoted_date"].stringValue))"
                }
                else{
                    cell.QuotedView.isHidden = true
                    cell.lblQuotedConversation.text = ""
                    cell.lblQuotedUserDetails.text = ""
                }
                
                cell.lblConversation.text = conversation["comment"].string ?? ""
                cell.lblDate.text = ConversationsViewController.formatDateString(dateStr: conversation["createdAt"].stringValue)
                
                cell.lblConversation.handleURLTap { (url) in
                    print(url)
                    //                    UIApplication.shared.open(url)
                    
                    if url.absoluteString.contains("familheey"){
                        self.openAppLink(url: url.absoluteString)
                        //                        Helpers.openAppLinkCommon(url: url.absoluteString, currentVC: self)
                    }
                    else{
                        if !(["http", "https"].contains(url.scheme?.lowercased())) {
                            var strUrl = url.absoluteString
                            strUrl = "http://"+strUrl
                            let Turl = URL(string: strUrl)
                            
                            let vc = SFSafariViewController(url: Turl!)
                            self.present(vc, animated: true, completion: nil)
                        }
                        else{
                            let vc = SFSafariViewController(url: url)
                            self.present(vc, animated: true, completion: nil)
                        }
                    }
                    
                }
                return cell
                
            }
            
            
            
        }
        else{
            if let attachments = conversation["attachment"].array, attachments.count != 0{
                
                let attachment = attachments[0]
                if let type = attachment["type"].string ,type.contains("audio"){
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ConversationAudioTableViewCell", for: indexPath) as! ConversationAudioTableViewCell
                    
                    
                    let url = "\(Helpers.imageURl)"+"\(BaseUrl.conversation)"+attachment["filename"].stringValue
                    cell.lblUserName.text = conversation["full_name"].string ?? "Unknown"
                    cell.lblDate.text = ConversationsViewController.formatDateString(dateStr: conversation["createdAt"].stringValue)
                    let proPic = conversation["propic"].stringValue
                    cell.imageViewUserProPic.isHidden = false
                    cell.nameToLeftConstarint.priority = UILayoutPriority(rawValue: 1000)
                    
                    if proPic.count != 0{
                        let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic
                        let imgUrl = URL(string: temp)
                        cell.imageViewUserProPic.kf.indicatorType = .activity
                        
                        cell.imageViewUserProPic.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                    }
                    else{
                        cell.imageViewUserProPic.image = #imageLiteral(resourceName: "Male Colored")
                    }
                    cell.imageViewUserProPic.isTappable(id: conversation["commented_by"].intValue)
                    
                    cell.playerStream(urlStream: url, for: indexPath.row)
                    cell.delegate = self
                    
                    return cell
                    
                }
                    
                else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ConversationsMediaReceivedTableViewCell", for: indexPath) as! ConversationsMediaReceivedTableViewCell
                    
                    cell.playIcon.isHidden = true
                    cell.viewOfMultipleImage.isHidden = true
                    cell.lblUserName.text = conversation["full_name"].string ?? "Unknown"
                    cell.lblDate.text = ConversationsViewController.formatDateString(dateStr: conversation["createdAt"].stringValue)
                    let proPic = conversation["propic"].stringValue
                    
                    if proPic.count != 0{
                        let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic
                        let imgUrl = URL(string: temp)
                        cell.imageViewUserPropic.kf.indicatorType = .activity
                        
                        cell.imageViewUserPropic.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                    }
                    else{
                        cell.imageViewUserPropic.image = #imageLiteral(resourceName: "Male Colored")
                    }
                    cell.imageViewUserPropic.isTappable(id: conversation["commented_by"].intValue)
                    
                    if let type = attachment["type"].string ,type.contains("image"){
                        let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.conversation)"+attachment["filename"].stringValue.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                        cell.imageviewMedia.kf.indicatorType = .activity
                        cell.imageviewMedia.setImageWithRetry(url: temp, tryCount: 10)
                        
                        if attachments.count > 1{
                            cell.viewOfMultipleImage.isHidden = false
                        }
                        else{
                            cell.viewOfMultipleImage.isHidden = true
                        }
                        
                    }
                    else if let type = attachment["type"].string ,type.contains("video"){
                        var temp = ""
                        if let thumb = attachment["video_thumb"].string, !thumb.isEmpty {
                            cell.playIcon.isHidden = false
                            temp = "\(Helpers.imaginaryImageBaseUrl)width=100&height=100&url=\(Helpers.imageURl)\(thumb)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                            cell.imageviewMedia.kf.indicatorType = .activity
                            cell.imageviewMedia.setImageWithRetry(url:  temp, tryCount: 10)
                        }
                        else{
                            cell.imageviewMedia.image = #imageLiteral(resourceName: "videoThumbnail")
                        }
                    }
                    else{
                        cell.imageviewMedia.image = #imageLiteral(resourceName: "DocumentTumb")
                    }
                    cell.buttonMedia.tag = indexPath.row
                    
                    return cell
                }
            }
            else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "ConversationTextTableViewCell", for: indexPath) as! ConversationTextTableViewCell
                
                if let qString = conversation["quoted_item"].string{
                    cell.QuotedView.isHidden = false
                    cell.lblQuotedConversation.text = "\"\(qString)\""
                    cell.lblQuotedUserDetails.text = "\(conversation["quoted_user"].string ?? "Unknown"), \(ConversationsViewController.formatDateString(dateStr: conversation["quoted_date"].stringValue))"
                }
                else{
                    cell.QuotedView.isHidden = true
                    cell.lblQuotedConversation.text = ""
                    cell.lblQuotedUserDetails.text = ""
                }
                
                cell.lblUserName.text = conversation["full_name"].string ?? "Unknown"
                cell.lblConversation.text = conversation["comment"].string ?? ""
                cell.lblDate.text = ConversationsViewController.formatDateString(dateStr: conversation["createdAt"].stringValue)
                let proPic = conversation["propic"].stringValue
                
                
                if proPic.count != 0{
                    let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic
                    let imgUrl = URL(string: temp)
                    cell.imageviewUserProPic.kf.indicatorType = .activity
                    
                    cell.imageviewUserProPic.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                }
                else{
                    cell.imageviewUserProPic.image = #imageLiteral(resourceName: "Male Colored")
                }
                cell.imageviewUserProPic.isTappable(id: conversation["commented_by"].intValue)
                
                cell.lblConversation.handleURLTap { (url) in
                    print(url)
                    //                    UIApplication.shared.open(url)
                    
                    if url.absoluteString.contains("familheey"){
                        self.openAppLink(url: url.absoluteString)
                        //                        Helpers.openAppLinkCommon(url: url.absoluteString, currentVC: self)
                    }
                    else{
                        if !(["http", "https"].contains(url.scheme?.lowercased())) {
                            var strUrl = url.absoluteString
                            strUrl = "http://"+strUrl
                            let Turl = URL(string: strUrl)
                            
                            let vc = SFSafariViewController(url: Turl!)
                            self.present(vc, animated: true, completion: nil)
                        }
                        else{
                            let vc = SFSafariViewController(url: url)
                            self.present(vc, animated: true, completion: nil)
                        }
                    }
                    
                }
                return cell
            }
        }
        
        
    }
    
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        if !labelExternalDate.text!.isEmpty{
            self.viewExternalDate.isHidden = false
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            
            self.viewExternalDate.isHidden = true
            
            
        }
        
    }
    
    /*
     func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
     if let lastVisibleIndexPath = tableView.indexPathsForVisibleRows?.last {
     if indexPath == lastVisibleIndexPath {
     let conversation = arrayConversations[indexPath.row]
     if conversation["unread"].boolValue {
     if let last = lastReadInex , last < indexPath.row{
     lastReadInex = indexPath.row
     }
     else if lastReadInex == nil{
     lastReadInex = indexPath.row
     }
     }
     }
     }
     
     }
     
     func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
     
     
     
     }
     func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
     if lastReadInex != nil{
     let conversation = arrayConversations[lastReadInex!]
     self.readConversation(msgId: conversation["comment_id"].stringValue)
     }
     }
     func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
     
     }
     
     */
    
    func addUnreadCell(at index : IndexPath){
        let newObject = JSON.init(rawValue: "")!
        //        if arrayConversations[index.row] != newObject{
        tableviewConversations.beginUpdates()
        unreadViewShown = true
        arrayConversations.insert(newObject, at: index.row)
        tableviewConversations.insertRows(at: [index,IndexPath.init(row: index.row, section: 0)], with: UITableView.RowAnimation.automatic)
        //            tableviewConversations.layoutIfNeeded()
        tableviewConversations.endUpdates()
        //            tableviewConversations.reloadData()
        tableviewConversations.scrollToRow(at: index, at: .top, animated: true)
        //        }
    }
    
}
extension ConversationsViewController: UITextViewDelegate{
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.contentSize.height >= self.messageTextViewMaxHeight - 16
        {
            textView.isScrollEnabled = true
            
            //remya code
            
            heightOfTxtView.constant = 100
            heightOfOuterView.constant = 150
            
        }
        else
        {
            heightOfTxtView.constant = 101
            heightOfOuterView.constant = 61
            textView.frame.size.height = textView.contentSize.height
            textView.isScrollEnabled = false
            
            
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        recorderButtonView.isHidden = true
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Your words mean more. Say something.."
            textView.textColor = UIColor.lightGray
            recorderButtonView.isHidden = false
        }
        botomvConstraint.constant = 0
    }
    
}
extension UITableView {
    
    func scrollToBottom(){
        
        DispatchQueue.main.async {
            let indexPath = IndexPath(
                row: self.numberOfRows(inSection:  self.numberOfSections - 1) - 1,
                section: self.numberOfSections - 1)
            self.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
    
    func scrollToTop() {
        
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: 0, section: 0)
            self.scrollToRow(at: indexPath, at: .top, animated: false)
        }
    }
}
extension Date {
    
    func timeAgoSinceDate(toReturn : String) -> String {
        
        // From Time
        let fromDate = self
        
        // To Time
        let toDate = Date()
        
        // Estimation
        // Year
        if let interval = Calendar.current.dateComponents([.year], from: fromDate, to: toDate).year, interval > 0  {
            
            //            return toReturn //Comment this to show year ago
            return interval == 1 ? "\(interval)" + " " + "year ago" : "\(interval)" + " " + "years ago"
        }
        
        // Month
        if let interval = Calendar.current.dateComponents([.month], from: fromDate, to: toDate).month, interval > 0  {
            //            return toReturn //Comment this to show month ago
            return interval == 1 ? "\(interval)" + " " + "month ago" : "\(interval)" + " " + "months ago"
        }
        
        // Day
        if let interval = Calendar.current.dateComponents([.day], from: fromDate, to: toDate).day, interval > 0  {
            //            return toReturn //Comment this to show Day ago
            return interval == 1 ? "\(interval)" + " " + "days ago" : "\(interval)" + " " + "days ago"
        }
        
        
        // Hours
        if let interval = Calendar.current.dateComponents([.hour], from: fromDate, to: toDate).hour, interval > 0 {
            
            return interval == 1 ? "\(interval)" + " " + "hours ago" : "\(interval)" + " " + "hours ago"
        }
        
        // Minute
        if let interval = Calendar.current.dateComponents([.minute], from: fromDate, to: toDate).minute, interval > 0 {
            
            return interval == 1 ? "\(interval)" + " " + "minute ago" : "\(interval)" + " " + "minute ago"
        }
        
        return "a moment ago"
    }
    func timeAgoSinceDateForNotification(toReturn : String) -> String
    {
        
        // From Time
        let fromDate = self
        
        // To Time
        let toDate = Date()
        
        // Estimation
        // Year
        if let interval = Calendar.current.dateComponents([.year], from: fromDate, to: toDate).year, interval > 0  {
            
            //            return toReturn //Comment this to show year ago
            return interval == 1 ? "\(interval)" + " " + "Y" : "\(interval)" + " " + "Y"
        }
        
        //            // Month
        if let interval = Calendar.current.dateComponents([.month], from: fromDate, to: toDate).month, interval > 0  {
            //            return toReturn //Comment this to show month ago
            return interval == 1 ? "\(interval)" + " " + "month" : "\(interval)" + " " + "months"
        }
        
        // week
        if let interval = Calendar.current.dateComponents([.weekday], from: fromDate, to: toDate).month, interval > 0  {
            //            return toReturn //Comment this to show month ago
            return interval == 1 ? "\(interval)" + " " + "week" : "\(interval)" + " " + "weeks"
        }
        
        // Day
        if let interval = Calendar.current.dateComponents([.day], from: fromDate, to: toDate).day, interval > 0  {
            //            return toReturn //Comment this to show Day ago
            return interval == 1 ? "\(interval)" + " " + "d" : "\(interval)" + " " + "d"
        }
        
        
        // Hours
        if let interval = Calendar.current.dateComponents([.hour], from: fromDate, to: toDate).hour, interval > 0 {
            
            return interval == 1 ? "\(interval)" + " " + "hr" : "\(interval)" + " " + "hr"
        }
        
        // Minute
        if let interval = Calendar.current.dateComponents([.minute], from: fromDate, to: toDate).minute, interval > 0 {
            
            return interval == 1 ? "\(interval)" + " " + "m" : "\(interval)" + " " + "m"
        }
        if let interval = Calendar.current.dateComponents([.second], from: fromDate, to: toDate).minute, interval > 0 {
            
            return interval == 1 ? "\(interval)" + " " + "s" : "\(interval)" + " " + "s"
        }
        
        return "a moment ago"
    }
}
extension ConversationsViewController : UIDocumentPickerDelegate,UIDocumentInteractionControllerDelegate{
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        
        print(urls)
        
        self.documentDataArr = []
        for currentUrl in urls
        {
            print(currentUrl.pathExtension)
            pathExt = currentUrl.pathExtension
            do {
                let documentData = try NSData(contentsOf: currentUrl, options: NSData.ReadingOptions())
                self.documentDataArr.append(documentData as Data)
                
                var dict = [String:Any]()
                dict["existing"] = false
                dict["document"] = currentUrl
                //                  self.selectedItemsexisted.append(dict)
                
            } catch {
                print(error)
            }
        }
        if self.documentDataArr.count != 0
        {
            self.uploadDocumentMultiple(arr: self.documentDataArr)
            
        }
        
        //          if self.selectedItemsexisted.count != 0
        //          {
        //              self.collViewOfAttachments.isHidden = false
        //              self.lblNoFiles.isHidden = true
        //              self.collViewOfAttachments.reloadData()
        //
        //          }
        //          else
        //          {
        //              self.collViewOfAttachments.isHidden = true
        //              self.lblNoFiles.isHidden = false
        //
        //          }
        //
    }
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
}
extension ConversationsViewController : ConversationAudioDelegate{
    
    func playerStatusChange(isPlaying: Bool, at index: Int) {
        
        if (currentPlayingIndex != nil){
            
            if currentPlayingIndex != index{
                if let tableViewCell = tableviewConversations.cellForRow(at: IndexPath(row: currentPlayingIndex!, section: 0)) as? ConversationAudioTableViewCell{
                    tableViewCell.pause()
                }
                currentPlayingIndex = index
            }
            
        }
        else{
            currentPlayingIndex = index
        }
        print("Playing status :", isPlaying)
        
    }
    
}
extension ConversationsViewController : RecordViewDelegate{
    func onStart() {
        typingView.isHidden = true
        self.view.endEditing(true)
        closeQuoteAction()
        startRecording()
        if (currentPlayingIndex != nil){
            if let tableViewCell = tableviewConversations.cellForRow(at: IndexPath(row: currentPlayingIndex!, section: 0)) as? ConversationAudioTableViewCell{
                tableViewCell.pause()
            }
        }
    }
    
    func onCancel() {
        
        
        
    }
    
    func onFinished(duration: CGFloat) {
        if duration > 1.0{
            finishRecording(success: true)
        }
        typingView.isHidden = false
    }
    
    func onAnimationEnd() {
        typingView.isHidden = false
        finishRecording(success: false)
    }
    
    
}
extension UITableView {
    func hasRow(at indexPath: IndexPath) -> Bool {
        return indexPath.section < self.numberOfSections && indexPath.row < self.numberOfRows(inSection: indexPath.section)
    }
}
