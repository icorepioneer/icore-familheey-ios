//
//  ConversationTextTableViewCell.swift
//  familheey
//
//  Created by Giri on 28/11/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import ActiveLabel

class ConversationTextTableViewCell: UITableViewCell {

    @IBOutlet weak var imageviewUserProPic: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblConversation: ActiveLabel!
    @IBOutlet weak var lblQuotedConversation: ActiveLabel!
    @IBOutlet weak var QuotedView: UIView!
    @IBOutlet weak var lblQuotedUserDetails: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class ConversationTextSendTableViewCell: UITableViewCell {

    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblConversation: ActiveLabel!
    @IBOutlet weak var lblQuotedConversation: ActiveLabel!
    @IBOutlet weak var QuotedView: UIView!
    @IBOutlet weak var lblQuotedUserDetails: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class ConversationReadTableViewCell: UITableViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
