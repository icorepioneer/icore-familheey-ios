//
//  StatusProgressCollectionViewCell.swift
//  familheey
//
//  Created by Giri on 27/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class StatusProgressCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var progressView: UIProgressView!
    
}
