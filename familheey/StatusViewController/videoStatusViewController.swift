//
//  videoStatusViewController.swift
//  familheey
//
//  Created by Giri on 27/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import AVKit
import Kingfisher

class videoStatusViewController: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

//    let arrayVideoUrl = ["https://familheey.s3.amazonaws.com/videos/SampleVideo_1280x720_20mb.mp4",
//    "https://familheey.s3.amazonaws.com/videos/file_example_MP4_1280_10MG.mp4",
//    "https://familheey.s3.amazonaws.com/videos/jellyfish-25-mbps-hd-hevc.mp4"]
    
    @IBOutlet weak var btnprevious: UIButton!
    @IBOutlet weak var btnnext: UIButton!
    var urltoPlay = ""
    var currentTrack = 0
    var player = AVPlayer()
    var observer:Any?
    var playbackLikelyToKeepUpKeyPathObserver: NSKeyValueObservation?
    var playbackBufferEmptyObserver: NSKeyValueObservation?
    var playbackBufferFullObserver: NSKeyValueObservation?
    var playerItem: AVPlayerItem?
    
    @IBOutlet weak var viewVideoPlay: UIView!
    @IBOutlet weak var viewImageviewer: UIView!
    @IBOutlet weak var collectionViewTop: UICollectionView!
    var usersList = [userStatus]()
    @IBOutlet weak var imageviewStatus: UIImageView!
    var Imagetimer:Timer? = Timer()

    var isPaused = true //timer

    override func viewDidLoad() {
        super.viewDidLoad()
    
        do{
         try usersList.insert(userStatus.init(["":""]), at: 0)
            try usersList.insert(userStatus.init(["":""]), at: 1)
            try usersList.insert(userStatus.init(["":""]), at: 2)
            try usersList.insert(userStatus.init(["":""]), at: 3)
            try usersList.insert(userStatus.init(["":""]), at: 4)
            try usersList.insert(userStatus.init(["":""]), at: 5)
        }
        catch{
            print("catched")
        }
        

        usersList[0].type = "image"
        usersList[0].url = "https://familheey.s3.amazonaws.com/propic/sample01.jpg"
        
        usersList[1].type = "video"
        usersList[1].url = "https://familheey.s3.amazonaws.com/videos/SampleVideo_1280x720_20mb.mp4"
        
        usersList[2].type = "image"
        usersList[2].url = "https://familheey.s3.amazonaws.com/propic/sample02.jpg"
        
        usersList[3].type = "image"
        usersList[3].url = "https://familheey.s3.amazonaws.com/propic/sample03.jpg"
        
        usersList[4].type = "video"
        usersList[4].url = "https://familheey.s3.amazonaws.com/videos/file_example_MP4_1280_10MG.mp4"
        
        usersList[5].type = "video"
        usersList[5].url = "https://familheey.s3.amazonaws.com/videos/jellyfish-25-mbps-hd-hevc.mp4"
        
        
        if usersList.count != 0{
            if usersList[0].type == "image"{
                showImage()
            }
            else{
                playVideo()
            }
        }
        
        
//        playVideo()
        collectionViewTop.delegate = self
        collectionViewTop.dataSource = self
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionViewTop!.collectionViewLayout = layout
        
//        ActivityIndicatorView.show("Please wait!")
        player = AVPlayer.init()
        let playerLayer = AVPlayerLayer(player: player)
//        playerLayer.videoGravity = .resizeAspectFill
        playerLayer.frame = viewVideoPlay.bounds
        viewVideoPlay.layer.addSublayer(playerLayer)
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)

    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
//        self.tabBarController?.tabBar.isHidden = false
        NotificationCenter.default.removeObserver(self)
    }
    //MARK:- Custom functions
    
    func playVideo(){
//        let videoURL =  URL(string: "\(usersList[currentTrack].url)")
//        player = AVPlayer(url: videoURL!)
//        let playerLayer = AVPlayerLayer(player: player)
//        playerLayer.frame = self.view.bounds
//        self.view.layer.addSublayer(playerLayer)
        
        if let asset = (player.currentItem?.asset as? AVURLAsset)?.url{
            if URL(string: usersList[currentTrack].url) == asset{
                player.play()
                ActivityIndicatorView.hiding()
                return
            }
        }
        
        urltoPlay = usersList[currentTrack].url
        player.replaceCurrentItem(with: AVPlayerItem(url: URL(string: urltoPlay)!))
        player.play()
        self.player.addPeriodicTimeObserver(forInterval: CMTimeMake(value: 1, timescale: 600), queue: DispatchQueue.main, using: { time in
            if self.player.currentItem?.status == AVPlayerItem.Status.readyToPlay {
                if  let duration = self.player.currentItem?.duration{
                self.setProgressAtIndex(index: self.currentTrack, withProgress: Float((CMTimeGetSeconds(time)/(CMTimeGetSeconds(duration)))))
                }
                if (self.player.currentItem?.isPlaybackLikelyToKeepUp) != nil {
                    ActivityIndicatorView.hiding()
                }
            }
        })
    }
    func setProgressAtIndex(index: NSInteger, withProgress progress: Float) { //video
        if usersList[currentTrack].type == "video"{
            if let cell: StatusProgressCollectionViewCell = self.collectionViewTop.cellForItem(at: NSIndexPath(row: index, section: 0) as IndexPath) as? StatusProgressCollectionViewCell {
                cell.progressView.setProgress(progress, animated: true)
            }
        }
    }
 
    func showImage(){
        ActivityIndicatorView.hiding()

//        let url = URL(string: "\(usersList[currentTrack].url)")
//        imageviewStatus.kf.indicatorType = .activity
//        let processor = RoundCornerImageProcessor(cornerRadius: 0)
//        imageviewStatus.kf.setImage(
//            with: url,
//            placeholder: nil ,
//            options: [
//                .processor(processor),
//                .scaleFactor(UIScreen.main.scale),
//                .transition(.fade(1)),
//                .cacheOriginalImage
//            ])
//
        
        
            let url = URL(string: "\(usersList[currentTrack].url)")
            let processor = RoundCornerImageProcessor(cornerRadius: 0)
            imageviewStatus.kf.indicatorType = .activity
    
        imageviewStatus.kf.setImage(with: url, placeholder: nil, options: [
            .processor(processor),
            .scaleFactor(UIScreen.main.scale),
            .transition(.fade(1)),
            .cacheOriginalImage
        ], progressBlock: nil) { (image, error, cache, url) in
            if (error == nil){
                if self.Imagetimer != nil{
                self.Imagetimer!.invalidate()
                self.Imagetimer = nil
                }
                    if let cell: StatusProgressCollectionViewCell = self.collectionViewTop.cellForItem(at: NSIndexPath(row: self.currentTrack, section: 0) as IndexPath) as? StatusProgressCollectionViewCell {
                        cell.progressView.setProgress(0.0, animated: true)
                        UIView.animate(withDuration: 5.0, animations: { () -> Void in
                            cell.progressView.setProgress(1.0, animated: true)
                        })

                    }
                self.Imagetimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.playNext), userInfo: nil, repeats: false)
            }
        }

    }

    @objc func playNext(){
        if usersList[currentTrack].type == "video"{
            player.pause()
        }
        if usersList.count > 0 && currentTrack < usersList.count-1{
            if self.Imagetimer != nil{
                self.Imagetimer!.invalidate()
                self.Imagetimer = nil
            }
            currentTrack = currentTrack + 1
            if usersList[currentTrack].type == "image"{
                viewVideoPlay.isHidden = true
                viewImageviewer.isHidden = false
                showImage()
            }
            else{
                ActivityIndicatorView.show("Please wait!")
               
                viewVideoPlay.isHidden = false
                viewImageviewer.isHidden = true
                playVideo()

            }
        }
        else{
            self.navigationController?.popViewController(animated: true)
        }
//        collectionViewTop.reloadData()
    }
    func playPrev(){
        
        if usersList[currentTrack].type == "video"{
            player.pause()
        }
        if currentTrack != 0{
            if self.Imagetimer != nil{
                self.Imagetimer!.invalidate()
                self.Imagetimer = nil
            }
            if usersList[currentTrack].type == "image"{
                
                if let cell: StatusProgressCollectionViewCell = self.collectionViewTop.cellForItem(at: NSIndexPath(row: self.currentTrack, section: 0) as IndexPath) as? StatusProgressCollectionViewCell {
                    UIView.animate(withDuration: 0.0, delay: 0.0, options: .beginFromCurrentState, animations: {
                        cell.progressView.setProgress(1.0, animated: true)
                    }, completion: nil)
                    
                }
            }
            currentTrack = currentTrack - 1
            if usersList[currentTrack].type == "image"{
                viewVideoPlay.isHidden = true
                viewImageviewer.isHidden = false
                showImage()
            }
            else{
                viewVideoPlay.isHidden = false
                viewImageviewer.isHidden = true
                ActivityIndicatorView.show("Please wait!")
//                urltoPlay = usersList[currentTrack].url
//                player.replaceCurrentItem(with: AVPlayerItem(url: URL(string: urltoPlay)!))
                playVideo()
            }
        
        }
//        collectionViewTop.reloadData()
    }
    @objc func playerDidFinishPlaying(note: NSNotification) {
        playNext()
    }

    
    //MARK:-  Buttom Actions
    @IBAction func onClickNextAction(_ sender: Any) {
        playNext()
    }
    @IBAction func onClickPreviousAction(_ sender: Any) {
        playPrev()
    }
    @IBAction func onClickPlayPause(_ sender: UIButton) {
        if usersList[currentTrack].type == "image"{
//            if isPaused{
//                Imagetimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.playNext), userInfo: nil, repeats: false)
//                isPaused = false
//            } else {
//                Imagetimer.invalidate()
//                isPaused = true
//            }
        }
        else{
        if sender.isSelected{
            player.play()
            sender.isSelected = false
        }
        else{
            player.pause()
            sender.isSelected = true
        }
        }
    }

    
    //MARK:- collectionView Delegates
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return usersList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StatusProgressCollectionViewCell", for: indexPath as IndexPath) as! StatusProgressCollectionViewCell
     
        if indexPath.item <= currentTrack{
            cell.progressView.progress = 1.0
        }
        else{
            cell.progressView.progress = 0.0
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        currentTrack = indexPath.item
        
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:((collectionViewTop.bounds.width/CGFloat(usersList.count))), height: 2)
    }
    
    
    
}
