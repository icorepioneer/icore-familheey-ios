//
//  PendingRequestViewController.swift
//  familheey
//
//  Created by Giri on 28/01/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON
import Kingfisher

class PendingRequestViewController: UIViewController {
    @IBOutlet weak var tittleLabel: UILabel!
    @IBOutlet weak var tableViewRequests: UITableView!
    var ArrayRequests = [JSON]()
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    var initiallimit = 20
    var offset = 0
    var limit = 100
    var isAdmin = false
    var groupId : String?
    var isFromFamily = false
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var lblNoData: UILabel!
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getRequests()
        tableViewRequests.tableFooterView = UIView.init()
        
        if isAdmin{
            tittleLabel.text = "Pending invites"
            lblNoData.text = "No pending invites"
        }
        else{
            tittleLabel.text = "Pending requests"
            lblNoData.text = "No pending requests"
        }
        //        refreshControl.attributedTitle = NSAttributedString(string: "Please wait...")
        //        refreshControl.addTarget(self, action: #selector(refreshPost), for: .valueChanged)
        //        tableViewRequests.addSubview(refreshControl)
    }
    @objc func refreshPost(refreshControl: UIRefreshControl) {
        refreshControl.endRefreshing()
        getRequests()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        
    }
    
    //MARK:- Get requests
    
    func getRequests(){
        ArrayRequests.removeAll()
        
        ActivityIndicatorView.show("Please wait....")
        var param = [String:Any]()
        if isAdmin{
            guard let gID = groupId else{return}
            param = ["group_id" : gID,"type":"invite","offset":"\(offset)","limit":"\(limit)"]
        }
        else{
            param = ["user_id" : UserDefaults.standard.value(forKey: "userId") as! String,"type":"request","offset":"\(offset)","limit":"\(limit)"]
            
        }
        networkProvider.request(.pendingRequests(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        print("view contents : \(jsonData)")
                        
                        if let response = jsonData["data"].array{
                            if response.count != 0{
                                self.tableViewRequests.isHidden = false
                                self.noDataView.isHidden = true
                                self.ArrayRequests = response
                                self.tableViewRequests.delegate = self
                                self.tableViewRequests.dataSource = self
                                self.tableViewRequests.reloadData()
                            }
                            else{
                                self.tableViewRequests.isHidden = true
                                self.noDataView.isHidden = false
                            }
                            
                        }
                        else{
                            self.tableViewRequests.isHidden = true
                            self.noDataView.isHidden = false
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getRequests()
                        }
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    func deleteRequest(index:Int){
        guard let id = ArrayRequests[index]["id"].int else{return}
        
        ActivityIndicatorView.show("Please wait....")
        var param = [String:Any]()
        
        param = ["id" : "\(id)"]
        
        networkProvider.request(.deletePendingRequests(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        print("view contents : \(jsonData)")
                        
                        if index < self.ArrayRequests.count{
                            self.ArrayRequests.remove(at: index)
                            //                                self.tableViewRequests.deleteRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
                            self.tableViewRequests.reloadData()
                        }
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.deleteRequest(index: index)
                        }
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    
    //MARK:- Button Actions
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension PendingRequestViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ArrayRequests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let request =  ArrayRequests[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PendingRequestsTableViewCell", for: indexPath) as! PendingRequestsTableViewCell
        
        cell.delegate = self
        
        cell.btnAccept.tag = indexPath.row
        
        if isAdmin{
            let proPic = request["propic"].stringValue
            
            if !proPic.isEmpty{
                let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic
                let imgUrl = URL(string: temp)
                DispatchQueue.main.async {
                    cell.imageViewUser.kf.indicatorType = .activity
                    cell.imageViewUser.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                }
            }
            else{
                cell.imageViewUser.image = #imageLiteral(resourceName: "Male Colored")
            }
            
        }
        else{
            let proPic = request["logo"].stringValue
            
            if !proPic.isEmpty{
                
                let temp = "\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+proPic
                let imgurl = URL(string: temp)
                let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+imgurl!.relativeString
                let url = URL(string: newUrlStr)
                cell.imageViewUser.kf.indicatorType = .activity
                cell.imageViewUser.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "Family Logo"), options: nil, progressBlock: nil, completionHandler: nil)
            }
            else{
                cell.imageViewUser.image = #imageLiteral(resourceName: "Family Logo")
            }
            
        }
        if isAdmin{
            cell.lblSubText.text = "To "
            cell.location.text = request["location"].string ?? ""
            cell.lblCreatedBy.text = request["group_name"].string ?? ""
            cell.lblName.text = request["full_name"].string ?? "Unknown"
            
        }
        else{
            cell.lblSubText.text = "By "
            cell.lblName.text = request["group_name"].string ?? ""
            cell.location.text = request["base_region"].string ?? ""
            cell.lblCreatedBy.text =  request["created_by"].string ?? "Unknown"
            
        }
        return cell
        
    }
    
}
extension PendingRequestViewController : PendingRequestsTableViewCellDelegates{
    func acceptAction(index: Int) {
        var title = "invitation"
        if !isFromFamily{
            title = "request"
        }
        let alert = UIAlertController(title: "Familheey", message: "Do you really want to cancel \(title)?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "YES", style: .destructive, handler: { (acion) in
            self.deleteRequest(index: index)
        }))
        
        alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
}
