//
//  PendingRequestsTableViewCell.swift
//  familheey
//
//  Created by Giri on 28/01/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit

protocol PendingRequestsTableViewCellDelegates:class {
    func acceptAction(index:Int)
}

class PendingRequestsTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewUser: UIImageView!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblSubText: UILabel!
    @IBOutlet weak var lblCreatedBy: UILabel!
    @IBOutlet weak var location: UILabel!
    weak var delegate : PendingRequestsTableViewCellDelegates?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func AcceptAction(_ sender: UIButton) {
        delegate?.acceptAction(index: sender.tag)
    }
}
