//
//  AddFamilyScreenSixViewController.swift
//  familheey
//
//  Created by familheey on 19/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class AddFamilyScreenSixViewController: BaseClassViewController {
    @IBOutlet weak var tblListView: UITableView!
    @IBOutlet weak var topView_height: NSLayoutConstraint!
    @IBOutlet weak var btnAddNew: UIButton!
    @IBOutlet weak var lblFooter: UILabel!
    @IBOutlet weak var btnAddNewFamily: UIButton!
    @IBOutlet weak var noFamilyView: UIView!
    
    var isFromLink = Bool()
    var FamilyArr: familyListResponse!
    var linkFamilyArr : [familyModel]?
    var groupId = ""
    var listArr = [String]()
    var isFromLinked = false
    
    @IBOutlet weak var lblHead: UILabel!
    @IBOutlet weak var lblSimilar_top: NSLayoutConstraint!
    @IBOutlet weak var lblName_height: NSLayoutConstraint!
    @IBOutlet weak var lblSimilar: UILabel!
    @IBOutlet weak var lblSubHead: UILabel!
    
    
    override func onScreenLoad() {
        //tblListView.register(UINib(nibName: "FamilyListingTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        let headerNib = UINib.init(nibName: "FamilyListingView", bundle: Bundle.main)
        tblListView.register(headerNib, forHeaderFooterViewReuseIdentifier: "FamilyListingView")
        
        if isFromLink{
            if groupId.count == 0{
                let defaultValue = UserDefaults.init(suiteName: "createFamily")
                groupId = defaultValue?.value(forKey: "groupId") as! String
            }
            getLinkFamilyList(groupId:groupId)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = true
        
//        if UserDefaults.standard.value(forKey: "userName") != nil{
            
//            nameInfoLbl.text = "\(UserDefaults.standard.value(forKey: "userName") as! String), would you like to join any of these families instead of creating a new one?"
//        }else{
//
//            nameInfoLbl.text = "would you like to join any of these families instead of creating a new one?"
//        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = true
        
        if isFromLink{
           // self.topView_height.constant = 0
            
            lblFooter.text = "Please select families from above for linking and"
            btnAddNew.setTitle("Add Links", for: .normal)
            lblHead.text = "Link Families"
            lblName_height.constant = 0
            lblSimilar_top.constant = 0
            lblSimilar.isHidden = true
            lblSubHead.isHidden = true
            if isFromLinked{
                btnAddNewFamily.isHidden = false
            }
             self.navigationItem.setHidesBackButton(false, animated: true)
//            self.tblListView.delegate = self
//            self.tblListView.dataSource = self
            self.tblListView.isMultipleTouchEnabled = true
            
        }
        else{
          //  topView_height.constant = 50
            lblSimilar.isHidden = true
            lblSimilar_top.constant = 0
            lblSubHead.isHidden = false
            lblHead.text = "Create Family"
            btnAddNew.setTitle("+ Add New Family", for: .normal)
            lblFooter.text = "If you are not part of any of the familie above \n Touch to add a new family"
            tblListView.delegate = self
            tblListView.dataSource = self
            self.navigationItem.setHidesBackButton(false, animated: true)
            self.tblListView.isMultipleTouchEnabled = false
        }
    }
    
    
    
    override func isNavigationControllerVisible() -> Bool {
        return false
    }
    
    //MARK:- Custom Method
    
    @objc func onClickSkip(){
        if isFromLink{
            if listArr.count > 0{
               saveLinks(array: listArr)
            }
            else{
                self.displayAlert(alertStr: "Select atleast one family", title: "")
            }
        }
        else{
            createFamily()
        }
    }
    
    //MARK:- Web API Call
    
    func getLinkFamilyList(groupId:String){
        APIServiceManager.callServer.getFamilyForLinking(url: EndPoint.linkList, groupId: groupId, userId: UserDefaults.standard.value(forKey: "userId") as! String, success: { (responseMdl) in
            
            guard let responseMdl = responseMdl as? familyListResponse else{
                return
            }
            
            ActivityIndicatorView.hiding()
            
            if responseMdl.statusCode == 200{
                print(responseMdl)
                
                self.linkFamilyArr = responseMdl.familyList
                if self.linkFamilyArr!.count > 0{
                    self.tblListView.isHidden = false
                    self.tblListView.delegate = self
                    self.tblListView.dataSource = self
                    self.tblListView.reloadData()
                    self.btnAddNew.isEnabled = true
                    self.btnAddNew.backgroundColor =  #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                }
                else{
                    self.tblListView.isHidden = true
                    self.btnAddNew.isEnabled = false
                    self.btnAddNew.backgroundColor =  .gray
                }
                
            }
            
        }) { (error) in
            
        }
    }
    
    func createFamily(){ // Change Storyboard
        
        let userdefault = UserDefaults.init(suiteName: "createFamily")
        let param = [
                   "group_name":userdefault?.value(forKey: "groupName") as! String,
                   "group_category":userdefault?.value(forKey: "groupCategory") as! String,
                   "base_region":userdefault?.value(forKey: "baseRegion") as! String,
                   "created_by":UserDefaults.standard.value(forKey: "userId") as! String,
                   "lat":userdefault?.value(forKey: "famLat") as! Double,
                   "long":userdefault?.value(forKey: "famLong") as! Double
        ] as [String:Any]
        
        APIServiceManager.callServer.createFamily(url: EndPoint.createFamily, param: param,  success: { (responseMdl) in
            
            guard let responseMdl = responseMdl as? familyListResponse else{
                return
            }
            
            ActivityIndicatorView.hiding()
            
            if responseMdl.statusCode == 200{
                if responseMdl.familyList!.count > 0 {
                    setUserDefaultValues.setUserDefaultValueWithName(name: "createFamily", userDefaultValue: String(responseMdl.familyList![0].faId), userDefaultKey: "groupId")
                    
                    let intro = self.storyboard?.instantiateViewController(withIdentifier: "addFamilyTwo") as! AddFamilyScreenTwoViewController
                    self.navigationController?.pushViewController(intro, animated: true)
                }
            }
            
        }) { (error) in
            
        }
//        let intro = self.storyboard?.instantiateViewController(withIdentifier: "addFamilyTwo") as! AddFamilyScreenTwoViewController
//        self.navigationController?.pushViewController(intro, animated: true)
    }
    
    func joinFamily(group:String){
        APIServiceManager.callServer.joinFamilyRequest(url: EndPoint.joinFamily, userId: UserDefaults.standard.value(forKey: "userId") as! String, groupId: group, success: { (responseMdl) in
            
            guard let joinResponse = responseMdl as? JoinResult else{
                return
            }
            ActivityIndicatorView.hiding()
            
            if joinResponse.status_code == 200{
                let tempStatus = joinResponse.joinData?.status as! String
                if tempStatus.lowercased() == "private_group"{
                    self.displayAlertChoice(alertStr: "Private group. Members can add by Admins only", title: "", completion: { (result) in
                        self.navigationController?.popToViewController(ofClass: FamiliesTabViewController.self)
//                         self.navigationController?.popToRootViewController(animated: true)

                    })
                }
                else{
                    self.displayAlertChoice(alertStr: "Request Sent", title: "", completion: { (result) in
                        self.navigationController?.popToViewController(ofClass: FamiliesTabViewController.self)
//                         self.navigationController?.popToRootViewController(animated: true)
                    })
                }
            }
            
            
        }) { (error) in
            self.displayAlert(alertStr: error!, title: "")
        }
    }
    
    func saveLinks(array:[String]){
        APIServiceManager.callServer.saveFamilyLinking(url: EndPoint.saveLink, groupId: groupId, userId: UserDefaults.standard.value(forKey: "userId") as! String, toGroup: listArr, success: { (responseMdl) in
            guard let joinResponse = responseMdl as? requestSuccessModel else{
                return
            }
            ActivityIndicatorView.hiding()
            
            if joinResponse.status_code == 200 {
                self.displayAlertChoice(alertStr: "Request Sent", title: "", completion: { (result) in
                    self.navigationController?.popViewController(animated: true)
                })
            }
            
        }) { (error) in
            
        }
    }
    
    
    //MARK:- UIButton Actions
    
    @IBAction func onClickAddNew(_ sender: Any) {
        onClickSkip()
    }
    
    @IBAction func onClickJoinFamily(_ sender: UIButton){
        if isFromLink{
            var fam = self.linkFamilyArr?[sender.tag]
            if listArr.count > 0{
                let clickedId = linkFamilyArr![sender.tag].faId
                for i in 0..<listArr.count{
                    if listArr[i] == "\(clickedId)"{
                        listArr.remove(at: i)
                        fam?.isLinked = ""
                    }
                    else{
                        listArr.append("\(clickedId)")
                        fam?.isLinked = "selected"
                    }
                }
                
            }
            else{
              listArr.append("\(linkFamilyArr![sender.tag].faId)")
                fam?.isLinked = "selected"
            }
            
            self.linkFamilyArr![sender.tag] = fam!
            self.tblListView.beginUpdates()
            //self.tblListView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .none)
            self.tblListView.reloadSections(IndexSet.init(integer: sender.tag), with: .none)
            self.tblListView.endUpdates()
        }
        else{
            let grpId = "\(FamilyArr.familyList![sender.tag].faId)"
            joinFamily(group: grpId)
        }
    }
    
    @IBAction func onClickViewFamily(_ sender: UIButton){
        var grpId = ""
        if isFromLink{
            grpId = "\(linkFamilyArr![sender.tag].faId)"
        }
        else{
            grpId = "\(FamilyArr.familyList![sender.tag].faId)"
        }
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
        intro.groupId = grpId
        intro.isFromFamilyList = true
        self.navigationController?.pushViewController(intro, animated: true)
    }

    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickCreateandLink(_ sender: Any) {
        let story = UIStoryboard.init(name: "CreateFamily", bundle: nil)
        let linked =  story.instantiateViewController(withIdentifier: "addFamilyOne") as! AddFamilyScreenOneViewController
        linked.isFromLinked = true
        linked.fromId = self.groupId
        self.navigationController?.pushViewController(linked, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddFamilyScreenSixViewController : UITableViewDelegate,UITableViewDataSource{
  
    func numberOfSections(in tableView: UITableView) -> Int {
        if isFromLink{
            return linkFamilyArr!.count
        }
        else{
            return FamilyArr.familyList!.count
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        // Dequeue with the reuse identifier
        let header = self.tblListView.dequeueReusableHeaderFooterView(withIdentifier: "FamilyListingView") as! FamilyListingView
       // header.frame = CGRect(x: 5, y: 0, width: self.view.frame.width - 5, height: self.view.frame.height)
      //  print(FamilyArr.familyList![section])
        
        header.btnAction.tag = section
        header.btnView.tag = section
        header.byTitle.text = "By"
        if isFromLink{
            header.lblTitle.text = linkFamilyArr![section].faName
            header.lblType.text = linkFamilyArr![section].faType
            header.lblRegion.text = linkFamilyArr![section].faRegion
            header.lblCreatedBy.text = linkFamilyArr![section].faAdmin
            
          //  let memberCount = linkFamilyArr![section].memberCount
            //let knownCount = linkFamilyArr![section].knownCount
            
            header.lblMemberHead.isHidden = true
            header.lblMembersCount.isHidden = true
            
            header.lblKnownHead.isHidden = true
            header.lblKnown.isHidden = true
            
          /*  if Int(memberCount)! > 0{
                header.lblMembersCount.text = linkFamilyArr![section].memberCount
                header.lblMemberHead.isHidden = false
                header.lblMembersCount.isHidden = false
            }
            else{
                header.lblMemberHead.isHidden = true
                header.lblMembersCount.isHidden = true
            }
            
            if Int(knownCount)! > 0{
                header.lblKnown.text = linkFamilyArr![section].knownCount
                header.lblKnownHead.isHidden = false
                header.lblKnown.isHidden = false
            }
            else{
                header.lblKnownHead.isHidden = true
                header.lblKnown.isHidden = true
            }*/
            

            if linkFamilyArr![section].faLogo.count != 0{
                let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=150&height=150&url="+"\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+linkFamilyArr![section].faLogo.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                let imgurl = URL(string: temp)
                header.imgLogo.kf.indicatorType = .activity

                header.imgLogo.kf.setImage(with: imgurl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
            }
            else{
               header.imgLogo.image = #imageLiteral(resourceName: "imgNoImage")
            }
            
            if linkFamilyArr![section].isLinked == ""{
                header.btnAction.setTitle("Link", for: .normal)
                header.btnAction.isUserInteractionEnabled = true
            }
            else if linkFamilyArr![section].isLinked.lowercased() == "pending"{
                header.btnAction.setTitle("Pending", for: .normal)
                header.btnAction.isUserInteractionEnabled = false
            }
            else if linkFamilyArr![section].isLinked.lowercased() == "accepted"{
                header.btnAction.setTitle("Linked", for: .normal)
                header.btnAction.isUserInteractionEnabled = false
            }
            else if linkFamilyArr![section].isLinked.lowercased() == "rejected"{
                header.btnAction.setTitle("Rejected", for: .normal)
                header.btnAction.isUserInteractionEnabled = true
            }
            else if linkFamilyArr![section].isLinked.lowercased() == "selected"{
                header.btnAction.setTitle("Selected", for: .normal)
                header.btnAction.isUserInteractionEnabled = true
            }
            
        }
        else{
            header.lblTitle.text    = FamilyArr.familyList![section].faName
            header.lblType.text     = FamilyArr.familyList![section].faCategory
            header.lblRegion.text   = FamilyArr.familyList![section].faRegion
            header.lblCreatedBy.text = FamilyArr.familyList![section].createdByName
            
            let memberCount = FamilyArr.familyList![section].memberCount
            let knownCount = FamilyArr.familyList![section].knownCount
            
            header.lblKnownHead.isHidden = true
            header.lblKnown.isHidden = true
            header.lblMemberHead.isHidden = true
            header.lblMembersCount.isHidden = true
            
          /*  if Int(memberCount!)! > 0{
             //   header.lblMembersCount.text = linkFamilyArr![section].memberCount ?? "0"
                header.lblMemberHead.isHidden = false
                header.lblMembersCount.isHidden = false
            }
            else{
                header.lblMemberHead.isHidden = true
                header.lblMembersCount.isHidden = true
            }
            
            if Int(knownCount!)! > 0{
               // header.lblKnown.text = linkFamilyArr![section].knownCount
                header.lblKnownHead.isHidden = false
                header.lblKnown.isHidden = false
            }
            else{
                header.lblKnownHead.isHidden = true
                header.lblKnown.isHidden = true
            }
            
            header.lblMembersCount.text = FamilyArr.familyList![section].memberCount
            header.lblKnown.text = FamilyArr.familyList![section].knownCount*/
            
            if FamilyArr.familyList![section].faLogo.count != 0{
                let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=150&height=150&url="+"\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+FamilyArr.familyList![section].faLogo.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                let imgurl = URL(string: temp)
                header.imgLogo.kf.indicatorType = .activity

                header.imgLogo.kf.setImage(with: imgurl, placeholder: UIImage(named: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
            }
            else{
                header.imgLogo.image = #imageLiteral(resourceName: "Family Logo")
            }
            
            let filePath = FamilyArr.familyList![section].faLogo
            
            if filePath.count == 0{
                header.imgLogo.image = #imageLiteral(resourceName: "Family Logo")
            }
            
            let joined = FamilyArr.familyList![section].isJoined
            let member_joined = FamilyArr.familyList![section].memberJoining
            let status = FamilyArr.familyList![section].req_status
            
            if joined == 1{
                header.btnAction.setTitle("Joined", for: .normal)
                header.btnAction.isUserInteractionEnabled = false
            }
            else{
                if member_joined == 1{
                    header.btnAction.setTitle("Private", for: .normal)
                    header.btnAction.isUserInteractionEnabled = false
                }
                else if (status.lowercased() == "pending"){
                    header.btnAction.setTitle("Pending", for: .normal)
                    header.btnAction.isUserInteractionEnabled = false
                }
                else if (status.lowercased() == "rejected"){
                    header.btnAction.setTitle("Rejected", for: .normal)
                   // header.btnAction.isUserInteractionEnabled = false
                }
                else{
                    header.btnAction.setTitle("Join", for: .normal)
                    header.btnAction.isUserInteractionEnabled = true
                }
            }
        }
        header.btnAction.addTarget(self, action: #selector(onClickJoinFamily(_:)), for: .touchUpInside)
        header.btnView.addTarget(self, action: #selector(onClickViewFamily(_:)), for: .touchUpInside)

        return header
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
}
