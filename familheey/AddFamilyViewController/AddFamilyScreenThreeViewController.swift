//
//  AddFamilyScreenThreeViewController.swift
//  familheey
//
//  Created by familheey on 18/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import CropViewController

class AddFamilyScreenThreeViewController: BaseClassViewController, CropViewControllerDelegate {
    
    @IBOutlet weak var btnNext_bottom: NSLayoutConstraint!
//    @IBOutlet weak var btnBack_bottom: NSLayoutConstraint!
    @IBOutlet weak var imgCoverPhoto: UIImageView!
    @IBOutlet weak var imgLogo: UIImageView!
    
    var imagePicker = UIImagePickerController()
    var btnTag: Int = Int()
    var originalImage = UIImageView()
    var originalData = NSData()
    var isfromLinked = false
    
    
    @IBOutlet weak var uploadCoverPicVw          : UIView!
    @IBOutlet weak var uploadLogoVw              : UIView!
    
    @IBOutlet weak var skipButt                  : UIButton!
    
    @IBOutlet weak var groupInfoLbl              : UILabel!
    
    
    override func onScreenLoad() {
        if self.view.frame.height < 600{
//            btnBack_bottom.constant = 20
//            btnNext_bottom.constant = 20
        }
        imgCoverPhoto.image = #imageLiteral(resourceName: "family_default_cover.png")
        imgLogo.image = #imageLiteral(resourceName: "Family Logo")
        skipButt.underline()
    }
    override func isNavigationControllerVisible() -> Bool {
        return false
    }

    override func viewDidAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden    = true
        self.tabBarController?.tabBar.isHidden               = true
        
        originalImage.image = #imageLiteral(resourceName: "family_default_cover.png")
//        if UserDefaults.standard.value(forKey: "groupName") != nil{
//            
//            groupInfoLbl.text = "Please take an moment to upload \(UserDefaults.standard.value(forKey: "groupName") as! String) family cover pic and logo"
//        }else{
//            
//            groupInfoLbl.text = "Please take an moment to upload family cover pic and logo"
//        }
    }
    
    //MARK:- Custom Methods
    
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else{
            // self.displayAlert(alertStr: "You don't have camera", title: "")
        }
    }
    
    func openGallary(){
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func selectedImage(img:UIImage){
        // let cropView = CropViewController(image: img)
        let cropView = CropViewController(croppingStyle: .default, image: img)
        cropView.aspectRatioLockEnabled = true
        cropView.aspectRatioLockDimensionSwapEnabled = true
        cropView.aspectRatioPickerButtonHidden = true
        if btnTag == 1{
            // cropView.customAspectRatio = CGSize(width: 4, height: 3)
            cropView.aspectRatioPreset = TOCropViewControllerAspectRatioPreset.preset4x3
            originalImage.image = img
            originalData = img.jpegData(compressionQuality: 0.75)! as NSData
            setUserDefaultValues.setUserDefaultValueDataWithName(name: "createFamily", userDefaultValue: originalData, userDefaultKey: "coverOriginal")
        }
        else{
            cropView.aspectRatioPreset = TOCropViewControllerAspectRatioPreset.presetSquare
        }
        cropView.delegate = self
        self.navigationController?.pushViewController(cropView, animated: true)
    }
    
    
    //MARK:- CropView SDK Delegate
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        // 'image' is the newly cropped version of the original image
        
        if btnTag == 1{
            
            uploadCoverPicVw.isHidden = true
            imgCoverPhoto.image = image
        }
        else{
            uploadLogoVw.isHidden     = true
            imgLogo.image = image
        }
       
        self.navigationController?.popViewController(animated: true)
    }

    
    
    //MARK:- Button actions
    @IBAction func onClickSkip(_ sender: Any) { // Change storyboard
        
        let imgCoverData:NSData = imgCoverPhoto.image?.jpegData(compressionQuality: 0.75)! as! NSData
        let imgLogoData:NSData = imgLogo.image?.jpegData(compressionQuality: 0.75) as! NSData
       
        originalData = imgCoverPhoto.image?.jpegData(compressionQuality: 0.75)! as! NSData
        
        setUserDefaultValues.setUserDefaultValueDataWithName(name: "createFamily", userDefaultValue: imgCoverData, userDefaultKey: "groupCover")
        setUserDefaultValues.setUserDefaultValueDataWithName(name: "createFamily", userDefaultValue: imgLogoData, userDefaultKey: "groupLogo")
        setUserDefaultValues.setUserDefaultValueDataWithName(name: "createFamily", userDefaultValue: originalData, userDefaultKey: "coverOriginal")
        
        let famSettings = self.storyboard?.instantiateViewController(withIdentifier: "addFamilyFour") as! AddFamilyScreenFourViewController
        famSettings.isfromLinked = self.isfromLinked
        self.navigationController?.pushViewController(famSettings, animated: true)
    }
    
    @IBAction func onClickNext(_ sender: Any) {
        
        let imgCoverData:NSData = imgCoverPhoto.image?.jpegData(compressionQuality: 0.75)! as! NSData
        let imgLogoData:NSData = imgLogo.image?.jpegData(compressionQuality: 0.75) as! NSData
        
        if originalData.count == 0 {
            originalData = imgCoverPhoto.image?.jpegData(compressionQuality: 0.75)! as! NSData
            
            setUserDefaultValues.setUserDefaultValueDataWithName(name: "createFamily", userDefaultValue: originalData, userDefaultKey: "coverOriginal")
        }
        
        setUserDefaultValues.setUserDefaultValueDataWithName(name: "createFamily", userDefaultValue: imgCoverData, userDefaultKey: "groupCover")
        setUserDefaultValues.setUserDefaultValueDataWithName(name: "createFamily", userDefaultValue: imgLogoData, userDefaultKey: "groupLogo")
        
        
        let famSettings = self.storyboard?.instantiateViewController(withIdentifier: "addFamilyFour") as! AddFamilyScreenFourViewController
        self.navigationController?.pushViewController(famSettings, animated: true)
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        var array : [UIViewController] = (self.navigationController?.viewControllers)!
        print(array)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickImagePick(_ sender: Any) {
        let button = sender as! UIButton
        btnTag = button.tag
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension  AddFamilyScreenThreeViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker .dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let choosenImage = info[.originalImage] as? UIImage{
            selectedImage(img: choosenImage)
            picker.dismiss(animated: true, completion: nil)
        }
    }
}
