//
//  AddFamilyScreenTwoViewController.swift
//  familheey
//
//  Created by familheey on 18/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
protocol onClickBackDelegate : class{
    func backClicked()
}

class AddFamilyScreenTwoViewController: BaseClassViewController {
    
    @IBOutlet weak var btnNext_bottom      : NSLayoutConstraint!
//    @IBOutlet weak var btnBack_bottom      : NSLayoutConstraint!
    @IBOutlet weak var txtIntro            : UITextView!
    @IBOutlet weak var lblHead: UILabel!
    
    weak var delegate: onClickBackDelegate?

    var isFromSettings = false
    var isfromLinked = false
    //MARK:- Viwe UI methods
    
    override func onScreenLoad() {
        if self.view.frame.height < 600{
//            btnBack_bottom.constant = 20
//            btnNext_bottom.constant = 20
        }
        
        txtIntro.layer.borderWidth = 1
        txtIntro.layer.borderColor = UIColor.lightGray.cgColor
//        txtIntro.delegate = self
    }
    override func isNavigationControllerVisible() -> Bool {
        return false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden    = true
        self.tabBarController?.tabBar.isHidden               = true
    }
        

    //MARK:- Button Actions
    @IBAction func onClickNext(_ sender: Any) { // Change Storyboard
//
//        if txtIntro.text.count > 250{
//
//            self.displayAlert(alertStr:"Max 250 charecters are allowed",title:"")
//
//            return
//        }
//
        let strIntro = txtIntro.text as String
        
        setUserDefaultValues.setUserDefaultValueWithName(name: "createFamily", userDefaultValue: strIntro, userDefaultKey: "groupIntro")
        
        let pickImg = self.storyboard?.instantiateViewController(withIdentifier: "addFamilyThree") as! AddFamilyScreenThreeViewController
        //  loginView.regType = regType
        pickImg.isfromLinked = self.isfromLinked
        self.navigationController?.pushViewController(pickImg, animated: true)
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        delegate?.backClicked()
        self.navigationController?.popViewController(animated: true)
    }


}
//extension AddFamilyScreenTwoViewController : UITextFieldDelegate{
//
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        let maxLength = 250
//        let currentString: NSString = textField.text! as NSString
//        let newString: NSString =
//            currentString.replacingCharacters(in: range, with: string) as NSString
//        return newString.length <= maxLength
//    }
//
//}

