//
//  AddFamilyScreenOneViewController.swift
//  familheey
//
//  Created by familheey on 17/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import GooglePlaces

class AddFamilyScreenOneViewController: BaseClassViewController, UITableViewDataSource, UITableViewDelegate,onClickBackDelegate {
    @IBOutlet weak var txtFamilyName: UITextField!
    @IBOutlet weak var txtFamilyType: UITextField!
    @IBOutlet weak var txtFamilyRegion: UITextField!
    @IBOutlet weak var txtFamilyTypeOther: UITextField!
    @IBOutlet weak var heightOfFamilyOtherType: NSLayoutConstraint!
    @IBOutlet weak var blackVwTopConstrain           : NSLayoutConstraint!
    @IBOutlet weak var blackVwHeightConstrain        : NSLayoutConstraint!
    @IBOutlet weak var backgroundBlueVw              : UIView!
    @IBOutlet weak var blackVw                       : UIView!
    @IBOutlet weak var tbl                           : UITableView!
    
    var userIndex                                    = 100
    let arrValue                                     = ["Regular","Company","Individual firm", "Non Profit Organisation", "Institute", "Religious/Spiritual", "Community", "Association", "Team", "Project", "Branch or division", "Others"]
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var familyArr : NSMutableArray!
    var isFromLinked = false
    var fromId = ""
    var familyTypeOther = false
    var isFromBack = false
    var selectedFamilyType:String! = ""
    var selectedLat : Double!
    var selectedLong :Double!
    
    override func onScreenLoad() {
       
        navigationController?.navigationBar.isTranslucent = false
        txtFamilyType.delegate = self
        txtFamilyTypeOther.delegate = self

        familyArr = []
        self.txtFamilyTypeOther.isHidden = true
        self.familyTypeOther = false
           
        heightOfFamilyOtherType.constant = 0
        
        
        let leftView1                             = UILabel(frame: CGRect(x: 5, y: 0, width: 15, height: 26))
        leftView1.backgroundColor                 = .clear
        txtFamilyName.leftView                    = leftView1
        txtFamilyName.leftViewMode                = .always
        txtFamilyName.contentVerticalAlignment    = .center
        txtFamilyName.autocapitalizationType      = .words
        
        let leftView2                             = UILabel(frame: CGRect(x: 5, y: 0, width: 15, height: 26))
        leftView2.backgroundColor                 = .clear
        txtFamilyType.leftView                    = leftView2
        txtFamilyType.leftViewMode                = .always
        txtFamilyType.contentVerticalAlignment    = .center
        
        let leftView3                             = UILabel(frame: CGRect(x: 5, y: 0, width: 15, height: 26))
        leftView3.backgroundColor                 = .clear
        txtFamilyRegion.leftView                  = leftView3
        txtFamilyRegion.leftViewMode              = .always
        txtFamilyRegion.contentVerticalAlignment  = .center
        
        blackVw.isHidden = true
        
      //  blackVwHeightConstrain.constant           = 0
        
     //  blackVwTopConstrain.constant              = self.view.frame.height
//        UIView.animate(withDuration: 0.5) {
//            self.view.layoutIfNeeded()
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: self.txtFamilyName.frame.size.height))
        //               txtFamilyName.leftView = paddingView
        //               txtFamilyName.leftViewMode = .always
        
        txtFamilyName.setLeftPaddingPoints(10)
        txtFamilyType.setLeftPaddingPoints(10)
        txtFamilyRegion.setLeftPaddingPoints(10)
        txtFamilyTypeOther.setLeftPaddingPoints(10)
        
        
        self.navigationController?.navigationBar.isHidden = true
        
        //        if !UIDevice.current.systemVersion.contains("13"){
        //
        //            txtFamilyRegion.text = appDel.selectedBaseName
        //        }
        
        guard #available(iOS 13.0, *) else {
            txtFamilyRegion.text = appDel.selectedBaseName
            return
        }
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.navigationController?.hidesBottomBarWhenPushed   = false
        self.tabBarController?.tabBar.isHidden                = true
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        appDel.selectedBaseName                   = ""
        self.tabBarController?.tabBar.isHidden    = false
    }
    
    override func viewWillLayoutSubviews() {
        
        backgroundBlueVw.roundCorners(corners: [.topLeft, .topRight], radius: 25)
        backgroundBlueVw.clipsToBounds = true
    }
    
    override func isNavigationControllerVisible() -> Bool {
        return false
    }
    
    //MARK:- Custom Methods
    func createFamily(){ // Change Storyboard
        print(self.selectedFamilyType!)
        var param = [String:Any]()
        if isFromLinked{
            param = [
                "group_name":txtFamilyName.text!,
                "group_category":self.selectedFamilyType!,
                "base_region":txtFamilyRegion.text!,
                "created_by":UserDefaults.standard.value(forKey: "userId") as! String,
                "is_active":true,
                "is_linkable":"true",
                "lat":self.selectedLat as Any,
                "long":self.selectedLong as Any
            ]
        }
        else{
            param = [
                "group_name":txtFamilyName.text!,
                "group_category":self.selectedFamilyType!,
                "base_region":txtFamilyRegion.text!,
                "is_active":false,
                "created_by":UserDefaults.standard.value(forKey: "userId") as! String,
                "lat":self.selectedLat as Any,
                "long":self.selectedLong as Any
            ]
        }
         
        
        APIServiceManager.callServer.createFamily(url: EndPoint.createFamily, param: param, success: { (responseMdl) in
            
            guard let responseMdl = responseMdl as? familyListResponse else{
                return
            }
            
            ActivityIndicatorView.hiding()
            if responseMdl.statusCode == 200{
                
                print(responseMdl.familyList![0].faName)
                self.familyArr.addObjects(from: responseMdl.familyList ?? [])
                print(self.familyArr)
                self.appDelegate.noFamily = false
                self.appDelegate.postcreatedInPublic = false

                if responseMdl.familyList!.count > 0 {
                    
                    setUserDefaultValues.setUserDefaultValueWithName(name: "createFamily", userDefaultValue: String(responseMdl.familyList![0].faId), userDefaultKey: "groupId")
                    
                    if self.isFromLinked{
                        var arr = [String]()
                        let ids = responseMdl.familyList![0].faId
                        arr.append("\(ids)")
                        self.saveLinks(array: arr)
                    }
                    else{
                        let intro = self.storyboard?.instantiateViewController(withIdentifier: "addFamilyTwo") as! AddFamilyScreenTwoViewController
                        intro.delegate = self
                        self.navigationController?.pushViewController(intro, animated: true)
                    }
                }
            }
            
        }) { (error) in
            
        }
    }
    
    func saveLinks(array:[String]){
        APIServiceManager.callServer.saveFamilyLinking(url: EndPoint.saveLink, groupId: self.fromId, userId: UserDefaults.standard.value(forKey: "userId") as! String, toGroup: array, success: { (responseMdl) in
            guard let joinResponse = responseMdl as? requestSuccessModel else{
                return
            }
            ActivityIndicatorView.hiding()
            
            if joinResponse.status_code == 200 {
//                self.displayAlertChoice(alertStr: "Request Sent", title: "", completion: { (result) in
//                    self.navigationController?.popViewController(animated: true)
                    let intro = self.storyboard?.instantiateViewController(withIdentifier: "addFamilyTwo") as! AddFamilyScreenTwoViewController
                    intro.delegate = self
                    intro.isfromLinked = self.isFromLinked
                    self.navigationController?.pushViewController(intro, animated: true)
//                })
            }
            
        }) { (error) in
            
        }
    }
    
    //MARK:- Delegate
    func backClicked() {
        isFromBack = true
    }
       
    
    //MARK:- Button Actions
    
    @IBAction func onClickContinue(_ sender: Any) {
        let temName = txtFamilyName.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        
        if temName.isEmpty != true && txtFamilyType.text?.isEmpty != true && txtFamilyRegion.text?.isEmpty != true{
            if self.familyTypeOther && txtFamilyTypeOther.text!.isEmpty == true{
                self.displayAlert(alertStr:"Please enter Family Type",title:"")
            }
            else {
                setUserDefaultValues.setUserDefaultValueWithName(name: "createFamily", userDefaultValue: txtFamilyName.text!, userDefaultKey: "groupName")
                setUserDefaultValues.setUserDefaultValueWithName(name: "createFamily", userDefaultValue: self.selectedFamilyType!, userDefaultKey: "groupCategory")
                setUserDefaultValues.setUserDefaultValueWithName(name: "createFamily", userDefaultValue: txtFamilyRegion.text!, userDefaultKey: "baseRegion")
                if selectedLong != nil{
                    setUserDefaultValues.setUserDefaultLatLongWithName(name: "createFamily", itemValue: self.selectedLong, itemKey: "famLong")
                }
                else{
                    
                }
                if selectedLat != nil{
                    setUserDefaultValues.setUserDefaultLatLongWithName(name: "createFamily", itemValue: self.selectedLat, itemKey: "famLat")
                }
                else{
                    
                }
               
                if isFromLinked{
                    self.createFamily()
                }
                else{
                    APIServiceManager.callServer.fetchFamily(url: EndPoint.fetchFamily, name: txtFamilyName.text!, type: self.selectedFamilyType!, base: txtFamilyRegion.text!, userId: UserDefaults.standard.value(forKey: "userId") as! String,  success: { (responseMdl) in
                                    
                    guard let responseMdl = responseMdl as? familyListResponse else{
                            return
                    }
                    ActivityIndicatorView.hiding()
                                    
                    if responseMdl.statusCode == 200{
                        if responseMdl.familyList!.count > 0{
                            print(responseMdl.familyList![0].faId)
                            let familyList = self.storyboard?.instantiateViewController(withIdentifier: "addFamilySix") as! AddFamilyScreenSixViewController
                            if self.isFromLinked{
                                familyList.isFromLink = true
                                familyList.groupId = self.fromId
                            }
                            else{
                                familyList.isFromLink = false
                            }
                            familyList.FamilyArr = responseMdl
                            self.navigationController?.pushViewController(familyList, animated: true)
                        }
                        else{
                            if self.isFromBack{
                    //                            setUserDefaultValues.setUserDefaultValueWithName(name: "createFamily", userDefaultValue: String(responseMdl.familyList![0].faId), userDefaultKey: "groupId")
                                let intro = self.storyboard?.instantiateViewController(withIdentifier: "addFamilyTwo") as! AddFamilyScreenTwoViewController
                                intro.delegate = self
                                self.navigationController?.pushViewController(intro, animated: true)
                            }
                            else{
                                self.createFamily()
                            }
                        
                        }
                    }
                                    
                }) { (error) in
                                    
                    }
                }

//            let pickImg = self.storyboard?.instantiateViewController(withIdentifier: "addFamilyFour") as! AddFamilyScreenFourViewController
//            //  loginView.regType = regType
//            self.navigationController?.pushViewController(pickImg, animated: true)
           }
        }
        else{
            self.displayAlert(alertStr:"All fields are mandatory",title:"")
        }
    }
    
    @IBAction func onClickGroupType(_ sender: Any) {
        
        self.view.endEditing(true)
        
        if txtFamilyType.text != ""{
            userIndex = arrValue.lastIndex(of: txtFamilyType.text!)!
        
        }else{
            userIndex = 100
        }
        
        print("userIndex \(userIndex)")
        
        tbl.reloadData()
        
        self.tabBarController?.tabBar.isHidden = true
        
        blackVw.isHidden = false
       // blackVw.animShow()
        
//        blackVwTopConstrain.constant = -20
//
//        UIView.animate(withDuration: 0.5) {
//            self.view.layoutIfNeeded()
//
//        }

        
        return
        
        let arr = ["Regular","Organization","Community"]
        self.showActionSheet(titleArr: arr as NSArray, title: "Choose option") { (index) in
            if index == 100{
                
            }
            else{
            self.txtFamilyType.text = arr[index]
            }
        }
    }
    
    @IBAction func baseRegionClicked(_ sender: Any) {
        self.view.endEditing(true)
        
//        let transition = CATransition()
//        transition.duration = 0.3
//        
//        transition.timingFunction = CAMediaTimingFunction(name: .linear)
//        
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromBottom
//        self.navigationController?.view.layer.add(transition, forKey: kCATransition)
        
        print("OS version : \(UIDevice.current.systemVersion)")
        
        guard #available(iOS 13.0, *) else {
            // Code for earlier iOS versions
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let SearchPlacesViewController = storyboard.instantiateViewController(withIdentifier: "SearchPlacesViewController") as! SearchPlacesViewController
            SearchPlacesViewController.selectedField = "base"
            
            self.present(SearchPlacesViewController, animated: true, completion: nil)
            
            return
        }
       
        let acController = GMSAutocompleteViewController()
        
        acController.delegate = self
        present(acController, animated: true, completion: nil)
        
       /* if UIDevice.current.systemVersion.contains("13"){
       
            let acController = GMSAutocompleteViewController()
                   
                   acController.delegate = self
                   present(acController, animated: true, completion: nil)
        }
        else{

                   let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                   let SearchPlacesViewController = storyboard.instantiateViewController(withIdentifier: "SearchPlacesViewController") as! SearchPlacesViewController
                    SearchPlacesViewController.selectedField = "base"
            
                    self.present(SearchPlacesViewController, animated: true, completion: nil)
        }*/
       
    }
    
    @IBAction func hideBlackVw(_ sender: Any) {
        
        blackVw.isHidden = true
    }
    
    
    @IBAction func backClicked(_ sender: Any) {
        
        appDel.selectedBaseName = ""
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: - Table View
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrValue.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        
        if let img = cell?.viewWithTag(1) as? UIImageView{
            
            if userIndex != indexPath.row{
                img.image                   = nil
                img.layer.borderColor       = UIColor.lightGray.cgColor
                img.layer.borderWidth       = 0.5
                img.backgroundColor         = .clear
            }else{
                
                img.image                   = #imageLiteral(resourceName: "roundTick")
                img.layer.borderWidth       = 0
                img.backgroundColor         = .white
            }
            
            
        }
        
        if let lbl = cell?.viewWithTag(2) as? UILabel{
            
            lbl.text = arrValue[indexPath.row]
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.txtFamilyType.text = arrValue[indexPath.row]
        if arrValue[indexPath.row] == "Others"
        {
            self.txtFamilyTypeOther.isHidden = false
            self.familyTypeOther = true
            heightOfFamilyOtherType.constant = 40
//            self.selectedFamilyType = txtFamilyTypeOther.text
            self.selectedFamilyType = ""
            txtFamilyTypeOther.text = ""
        }else
        {
            self.txtFamilyTypeOther.isHidden = true
            self.familyTypeOther = false
            self.selectedFamilyType = arrValue[indexPath.row]

            heightOfFamilyOtherType.constant = 0

        }
        self.tabBarController?.tabBar.isHidden = false
        
        blackVw.isHidden = true
       // blackVw.animHide()
        
//        blackVwTopConstrain.constant = self.view.frame.height + 1000
//
//        UIView.animate(withDuration: 0.5) {
//            self.view.layoutIfNeeded()
//            self.view.layoutSubviews()
//        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 40
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddFamilyScreenOneViewController: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
     func textFieldShouldReturn(_ textField: UITextField) -> Bool {
           
           textField.resignFirstResponder()
           return true
       }
       
       func textFieldDidEndEditing(_ textField: UITextField) {
           if textField == txtFamilyTypeOther
           {
            self.selectedFamilyType = txtFamilyTypeOther.text!
           }
       }
}

extension AddFamilyScreenOneViewController: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        // Get the place name from 'GMSAutocompleteViewController'
        // Then display the name in textField
        
//        txtFamilyRegion.text = place.name!
        txtFamilyRegion.text = "\(place.formattedAddress!)"

        self.selectedLat = place.coordinate.latitude
        self.selectedLong = place.coordinate.longitude
        
        print(place.coordinate.latitude)
        print(place.coordinate.longitude)
        
        dismiss(animated: true, completion: nil)
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // Handle the error
        print("Error: ", error.localizedDescription)
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        // Dismiss when the user canceled the action
        dismiss(animated: true, completion: nil)
    }
}
