//
//  AddFamilyScreenFourViewController.swift
//  familheey
//
//  Created by familheey on 18/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class AddFamilyCell_2: UITableViewCell {
    
    @IBOutlet weak var butt1         : UIButton!
    @IBOutlet weak var butt2         : UIButton!
    
    
}

class AddFamilyCell_3: UITableViewCell {
    
    @IBOutlet weak var butt1         : UIButton!
    @IBOutlet weak var butt2         : UIButton!
}

class AddFamilyCell_4: UITableViewCell {
    
    @IBOutlet weak var butt1         : UIButton!
    @IBOutlet weak var butt2         : UIButton!
}

class AddFamilyScreenFourViewController: BaseClassViewController {
    @IBOutlet weak var tblSettings: UITableView!
//    @IBOutlet weak var btnNext_bottom: NSLayoutConstraint!
//    @IBOutlet weak var btnBack_bottom: NSLayoutConstraint!
    @IBOutlet weak var btnAdvncSettings: UIButton!
    
//    var member_joining:String          = "2"
//    var member_approval:String         = "4"
//    var post_create:String             = "7"
//    var post_approval:String           = "10"
//    var post_visibilty:String          = "8"
//    var link_family:String             = "13"
//    var link_approval:String           = "16"
    
    @IBOutlet weak var tbl             : UITableView!
    var cell2CheckValue                = 1
    var cell3CheckValue                = 1
    var cell4CheckValue                = 2
    
    var isfromLinked                  = false
    var groupType                     = "Private"
    var isSearchable                  = "true"
    var isLinkable                    = "false"
    
  /*  var valueArr1 = [["key":"member_joining","text":"Joining","value":"Anyone can join - with approval"],["key":"member_approval","text":"Join approval","value":"Admin"]]
    
        var valueArr2 = [["key":"post_create","text":"Create post","value":"Members only"],["key":"post_approval","text":"Post approval","value":"Not needed"], ["key":"post_visibilty","text":"Post visibility","value":"Members only"], ["key":"link_family","text":"Link family","value":"Admin"], ["key":"link_approval","text":"Link family approval","value":"Admin"]]*/
    
    var isCellHiden = Bool()
    var isFromSettings = false
    @IBOutlet weak var lblHead: UILabel!
    
    override func onScreenLoad() {
       // getFamilySettings()
        
        if self.view.frame.height < 600{
//            btnBack_bottom.constant = 20
//            btnNext_bottom.constant = 20
        }
        
        if isfromLinked{
            isLinkable = "true"
        }
        tblSettings.register(UINib(nibName: "SettingsLabelTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        tblSettings.delegate = self
        tblSettings.dataSource = self
    }
    
    override func isNavigationControllerVisible() -> Bool {
        return false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        
        self.navigationController?.navigationBar.isHidden    = true
        self.tabBarController?.tabBar.isHidden               = true
    }
    
    //MARK:- Custom Methods
    
   /* func getFamilySettings(){
        if let path = Bundle.main.path(forResource: "FamilySettings", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                
                print(jsonResult)
            }
            catch let error {
                print(error.localizedDescription)
            }
        }
    }*/
    
  /*  func updateSettings(indexPath:Int, value:String, section:Int){
        
        var array:[[String:String]] = [[:]]
        if section == 0{
            array = valueArr1
        }
        else{
            array = valueArr2
        }
        let tempDic = array[indexPath] as NSDictionary
        let temp = tempDic.mutableCopy()
        
        (temp as AnyObject).removeObject(forKey: "value")
        (temp as AnyObject).setValue(value, forKey: "value")
        
        array.remove(at: indexPath)
        array.insert(temp as! [String : String], at: indexPath)
        
        if section == 0{
            valueArr1 = array
        }
        else{
            valueArr2 = array
        }
        tblSettings.reloadData()
    }*/
    
    //MARK:- Web API
    func UpdateSettings(){
        
    }
    
    //MARK:- Button Actions
    @IBAction func onClickSkip(_ sender: Any) {
    }
    
    //MARK: Call ActionActions
    
    @IBAction func cell2Action(sender : UIButton){
        
        if sender.tag == 2{
            
            cell2CheckValue = 1
            groupType       = "Private"
        }
        else{
            
            cell2CheckValue  = 2
            groupType       = "Public"
        }
        
        tbl.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .none)
    }
    
    @IBAction func cell3Action(sender : UIButton){
        
        if sender.tag == 1{
            
            cell3CheckValue = 1
            isSearchable    = "true"
            
        }
        else{
            
            cell3CheckValue = 2
            isSearchable    = "false"
        }
        
        tbl.reloadRows(at: [IndexPath(row: 2, section: 0)], with: .none)
    }
    
    @IBAction func cell4Action(sender : UIButton){
        
        if sender.tag == 1{
            
            cell4CheckValue = 1
            isLinkable      = "true"
            let famSettings = self.storyboard?.instantiateViewController(withIdentifier: "addFamilySix") as! AddFamilyScreenSixViewController
            famSettings.isFromLink = true
            self.navigationController?.pushViewController(famSettings, animated: true)
        }
        else{
            
            cell4CheckValue = 2
            isLinkable      = "false"
        }
        
        tbl.reloadRows(at: [IndexPath(row: 3, section: 0)], with: .none)
    }
    
    //MARK:- On click Next
    @IBAction func onClickAdvncSettings(_ sender: Any) {
        let defaultValue = UserDefaults.init(suiteName: "createFamily")
        defaultValue?.set(groupType, forKey: "groupType")
        print(defaultValue!)
        
        var groupIntro = ""
        var groupLogo = NSData()
        var groupCover = NSData()
        var coverOriginal = NSData()
        
        if let intro = defaultValue?.value(forKey: "groupIntro") as? String, !intro.isEmpty{
            groupIntro = intro
        }
        else{
            groupIntro = ""
        }
        
        if let logo = defaultValue?.value(forKey: "groupLogo") as? NSData, logo.count > 0{
            groupLogo = logo
        }
        else{
            groupLogo = NSData()
        }
        if let cover = defaultValue?.value(forKey: "groupCover") as? NSData, cover.count > 0{
            groupCover = cover
        }
        else{
            groupCover = NSData()
        }
        if let coverOrigi = defaultValue?.value(forKey: "coverOriginal") as? NSData, coverOrigi.count > 0{
            coverOriginal = coverOrigi
        }
        else{
            coverOriginal = NSData()
        }
        
        APIServiceManager.callServer.updateFamilyDetails(url: EndPoint.updateFamily, faId: defaultValue?.value(forKey: "groupId") as! String, faIntro: groupIntro, faType: groupType, isSearch: isSearchable, isLinkable: isLinkable, coverPic: groupCover, logo: groupLogo, originalImg: coverOriginal, contentType: "Image", active: "true", success: { (responseMdl) in
            
            guard let responseMdl = responseMdl as? familyListResponse else{
                return
            }
            ActivityIndicatorView.hiding()
            
            if responseMdl.statusCode == 200{
                print(responseMdl.familyList![0].faId)
                if responseMdl.familyList!.count > 0{
                    print(responseMdl.familyList![0].faId)
                    let intro = self.storyboard?.instantiateViewController(withIdentifier: "addFamilyFive") as! AddFamilyScreenFiveViewController
                    intro.groupType = self.groupType
                    self.navigationController?.pushViewController(intro, animated: true)
                }
            }
            
        }) { (error) in
            
        }
    }
    
    @IBAction func onClickNext(_ sender: Any) {
    
        let defaultValue = UserDefaults.init(suiteName: "createFamily")
        defaultValue?.set(groupType, forKey: "groupType")
//        print(defaultValue!)
        var groupIntro = ""
        var groupLogo = NSData()
        var groupCover = NSData()
        var coverOriginal = NSData()
        
        if let intro = defaultValue?.value(forKey: "groupIntro") as? String, !intro.isEmpty{
            groupIntro = intro
        }
        else{
            groupIntro = ""
        }
        
        if let logo = defaultValue?.value(forKey: "groupLogo") as? NSData, logo.count > 0{
            groupLogo = logo
        }
        else{
            groupLogo = NSData()
        }
        if let cover = defaultValue?.value(forKey: "groupCover") as? NSData, cover.count > 0{
            groupCover = cover
        }
        else{
            groupCover = NSData()
        }
        if let coverOrigi = defaultValue?.value(forKey: "coverOriginal") as? NSData, coverOrigi.count > 0{
            coverOriginal = coverOrigi
        }
        else{
            coverOriginal = NSData()
        }
        
        APIServiceManager.callServer.updateFamilyDetails(url: EndPoint.updateFamily, faId: defaultValue?.value(forKey: "groupId") as! String, faIntro: groupIntro, faType: groupType, isSearch: isSearchable, isLinkable: isLinkable, coverPic: groupCover, logo: groupLogo, originalImg: coverOriginal, contentType: "Image", active: "true", success: { (responseMdl) in
            
            guard let responseMdl = responseMdl as? familyListResponse else{
                return
            }
            ActivityIndicatorView.hiding()
            
            if responseMdl.statusCode == 200{
                print(responseMdl.familyList![0].faId)
                if responseMdl.familyList!.count > 0{
                    let stryboard = UIStoryboard.init(name: "second", bundle: nil)
                                       
                    let family = stryboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
                    family.groupId = defaultValue?.value(forKey: "groupId") as! String
                    family.isFromCreate = true
                    self.navigationController?.pushViewController(family, animated: true)
                }
            }
            
        }) { (error) in
            
        }
        
   /*     let defaultValue = UserDefaults.init(suiteName: "createFamily")
        APIServiceManager.callServer.updateFamilySettings(url: EndPoint.updateSettings, groupId: defaultValue?.value(forKey: "groupId") as! String, memberJoin: member_joining, memberApproval: member_approval, postCreate: post_create, postVisible: post_visibilty, postApproval: post_approval, linkFamily: link_family, linkApproval: link_approval, success: { (responseMdl) in
            
            guard let responseMdl = responseMdl as? familyListResponse else{
                return
            }
            ActivityIndicatorView.hiding()
            if responseMdl.statusCode == 200{
                
                let stryboard = UIStoryboard.init(name: "second", bundle: nil)
                
                let family = stryboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
                family.groupId = defaultValue?.value(forKey: "groupId") as! String
                family.isFromCreate = true
                self.navigationController?.pushViewController(family, animated: true)
            }
            
        }) { (error) in
            
        }
        */
    }
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddFamilyScreenFourViewController: UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if tableView == tbl{
            
            return 1
        }
        
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tbl{
            
            return 5
        }
        else{
            return 1
        }
       /* if section == 0{
          //  return valueArr1.count
        }
        else{
          //  return valueArr2.count
        }*/
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if tableView == tbl{
            
            var cell = tbl.dequeueReusableCell(withIdentifier: "cell1")!
            
            if indexPath.row == 0{
                cell = tbl.dequeueReusableCell(withIdentifier: "cell1")!
                
            }
            if indexPath.row == 1{
               let cell2 = tbl.dequeueReusableCell(withIdentifier: "cell2")! as! AddFamilyCell_2
                
                if cell2CheckValue == 1{
                    
                    cell2.butt1.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
                    cell2.butt2.setImage(nil, for: .normal)
                    
                    cell2.butt1.backgroundColor    = .white
                    cell2.butt2.backgroundColor    = .clear
                }
                else{
                    
                    cell2.butt2.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
                    cell2.butt1.setImage(nil, for: .normal)
                    
                    cell2.butt1.backgroundColor    = .clear
                    cell2.butt2.backgroundColor    = .white
                }
                
                return cell2
            }
            if indexPath.row == 2{
                
                let cell3 = tbl.dequeueReusableCell(withIdentifier: "cell3")! as! AddFamilyCell_3
                
                if cell3CheckValue == 1{
                    
                    cell3.butt1.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
                    cell3.butt2.setImage(nil, for: .normal)
                    
                    cell3.butt1.backgroundColor    = .white
                    cell3.butt2.backgroundColor    = .clear
                }
                else{
                    
                    cell3.butt2.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
                    cell3.butt1.setImage(nil, for: .normal)
                    
                    cell3.butt1.backgroundColor    = .clear
                    cell3.butt2.backgroundColor    = .white
                }
                
                return cell3
                
            }
            if indexPath.row == 3{
                
                let cell4 = tbl.dequeueReusableCell(withIdentifier: "cell4")! as! AddFamilyCell_4
                
                if cell4CheckValue == 1{
                    
                    cell4.butt1.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
                    cell4.butt2.setImage(nil, for: .normal)
                    
                    cell4.butt1.backgroundColor    = .white
                    cell4.butt2.backgroundColor    = .clear
                }
                else{
                    
                    cell4.butt2.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
                    cell4.butt1.setImage(nil, for: .normal)
                    
                    cell4.butt1.backgroundColor    = .clear
                    cell4.butt2.backgroundColor    = .white
                }
                
                return cell4
                
            }
            if indexPath.row == 4{
                cell = tbl.dequeueReusableCell(withIdentifier: "cell5")!
                
            }
            
            return cell
        }
        
        let cellId = "cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! SettingsLabelTableViewCell
        
       /* if indexPath.section == 0{
            let tempDic = valueArr1[indexPath.row] as NSDictionary
            cell.lblTitle.text = tempDic.value(forKey: "text") as? String
            cell.lblValue.text = tempDic.value(forKey: "value") as? String
            
            if indexPath.row == 0{
                if cell.lblValue.text == "Anyone can join - with approval"{
                    isCellHiden = false
                }
                else{
                    isCellHiden = true
                }
            }
            
            if isCellHiden{
                if indexPath.row == 1{
                    cell.isHidden = true
                }
            }
            else{
                if indexPath.row == 1{
                    cell.isHidden = false
                }
            }
        }
        else{
            let tempDic = valueArr2[indexPath.row] as NSDictionary
            cell.lblTitle.text = tempDic.value(forKey: "text") as? String
            cell.lblValue.text = tempDic.value(forKey: "value") as? String
        }*/
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if tableView == tbl{
            
            return 0
        }else{
            
            return 20
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0{
            return "Membership"
        }
        else{
            return "Posts"
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == tbl{
            
            if indexPath.row == 0{
                
                return 64
            }
            if indexPath.row == 1{
                
                return 185
            }
            
            if indexPath.row == 2{
                
                return 185
            }
            
            if indexPath.row == 3{
                
                return 185
            }
            
            if indexPath.row == 4{
                
                return 110
            }
        }
        
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tbl{
            
            return
        }
        
      /*  if indexPath.section == 0{
            if indexPath.row == 0{
                let arr1 = [["key":"Anyone can join", "value":"3"],["key":"Anyone can join - with approval", "value":"2"],["key":"Invitation only","value":"1"]]
                
                self.showActionSheetDictionary(titleArr: arr1 as NSArray) { (index) in
                    print(index)
                    let temp = arr1[index] as NSDictionary
                    self.member_joining = temp.value(forKey: "value") as! String
                    self.updateSettings(indexPath: indexPath.row, value: temp.value(forKey: "key") as! String, section: indexPath.section)
                }
            }
            else{
                let arr1 = [["key":"Admin", "value":"4"],["key":"Any members", "value":"5"]]
                self.showActionSheetDictionary(titleArr: arr1 as NSArray) { (index) in
                    print(index)
                    let temp = arr1[index] as NSDictionary
                    self.member_approval = temp.value(forKey: "value") as! String
                    self.updateSettings(indexPath: indexPath.row, value: temp.value(forKey: "key") as! String, section: indexPath.section)
                }
            }
        }
        else{
            if indexPath.row == 0{
                let arr1 = [["key":"Anyone", "value":"6"],["key":"Members only", "value":"7"]]
                self.showActionSheetDictionary(titleArr: arr1 as NSArray) { (index) in
                    print(index)
                    let temp = arr1[index] as NSDictionary
                    self.post_create = temp.value(forKey: "value") as! String
                    self.updateSettings(indexPath: indexPath.row, value: temp.value(forKey: "key") as! String, section: indexPath.section)
                }
            }
            else if indexPath.row == 1{
                let arr1 = [["key":"Members", "value":"12"],["key":"Not Needed", "value":"10"],["key":"Admins", "value":"11"]]
                self.showActionSheetDictionary(titleArr: arr1 as NSArray) { (index) in
                    print(index)
                    let temp = arr1[index] as NSDictionary
                    self.post_approval = temp.value(forKey: "value") as! String
                    self.updateSettings(indexPath: indexPath.row, value: temp.value(forKey: "key") as! String, section: indexPath.section)
                }
            }
            else if indexPath.row == 2{
                let arr1 = [["key":"Anyone", "value":"9"],["key":"Members only", "value":"8"]]
                self.showActionSheetDictionary(titleArr: arr1 as NSArray) { (index) in
                    print(index)
                    let temp = arr1[index] as NSDictionary
                    self.post_visibilty = temp.value(forKey: "value") as! String
                    self.updateSettings(indexPath: indexPath.row, value: temp.value(forKey: "key") as! String, section: indexPath.section)
                }
            }
            else if indexPath.row == 3{
                let arr1 = [["key":"Admins", "value":"13"],["key":"Members", "value":"14"]]
                self.showActionSheetDictionary(titleArr: arr1 as NSArray) { (index) in
                    print(index)
                    let temp = arr1[index] as NSDictionary
                    self.link_family = temp.value(forKey: "value") as! String
                    self.updateSettings(indexPath: indexPath.row, value: temp.value(forKey: "key") as! String, section: indexPath.section)
                }
            }
            else{
                let arr1 = [["key":"Not needed", "value":"15"],["key":"Admins", "value":"16"]]
                self.showActionSheetDictionary(titleArr: arr1 as NSArray) { (index) in
                    print(index)
                    let temp = arr1[index] as NSDictionary
                    self.link_approval = temp.value(forKey: "value") as! String
                    self.updateSettings(indexPath: indexPath.row, value: temp.value(forKey: "key") as! String, section: indexPath.section)
                }
            }
        }*/
    }
}
