//
//  AddFamilyScreenFiveViewController.swift
//  familheey
//
//  Created by familheey on 19/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

protocol familySettingUpdateDelegate {
    func updateFamilySetting(famArr : [familyModel])
}
class SettingsCell_0: UITableViewCell {
    
    @IBOutlet weak var skipButt         : UIButton!
    
    override func awakeFromNib() {
        
        skipButt.underline()
    }
}

class SettingsCell_1: UITableViewCell {
    
    @IBOutlet weak var butt1         : UIButton!
    @IBOutlet weak var butt2         : UIButton!
    @IBOutlet weak var butt3         : UIButton!
}

class SettingsCell_2: UITableViewCell {
    
    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var butt1         : UIButton!
    @IBOutlet weak var butt2         : UIButton!
    @IBOutlet weak var btnValue1: UIButton!
    @IBOutlet weak var btnValue2: UIButton!
    @IBOutlet weak var btnValue2_top: NSLayoutConstraint!
    
}
class SettingsCell_3: UITableViewCell {
    
    @IBOutlet weak var lblHead: UILabel!
    @IBOutlet weak var butt1         : UIButton!
    @IBOutlet weak var butt2         : UIButton!
    @IBOutlet weak var butt3         : UIButton!
    @IBOutlet weak var btt1Value: UIButton!
    @IBOutlet weak var butt2Value: UIButton!
    @IBOutlet weak var butt3Value: UIButton!
    
}
class SettingsCell_4: UITableViewCell {
    
    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var butt1         : UIButton!
    @IBOutlet weak var butt2         : UIButton!
    @IBOutlet weak var btnValue1: UIButton!
    @IBOutlet weak var btnValue2: UIButton!
    
}
class SettingsCell_5: UITableViewCell {
    
    @IBOutlet weak var butt1         : UIButton!
    @IBOutlet weak var butt2         : UIButton!
    
}
class SettingsCell_6: UITableViewCell {
    
    @IBOutlet var docSwitch          : UISwitch!
    @IBOutlet var hisSwitch          : UISwitch!
    @IBOutlet var famSwitch          : UISwitch!
    @IBOutlet var progSwitch         : UISwitch!
    @IBOutlet var vacSwitch          : UISwitch!
    
}

class SettingsCell_8: UITableViewCell {
    @IBOutlet weak var lblHead: UILabel!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btnAnyone: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btnAdminsOnly: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btnApproval: UIButton!
    
}

class AddFamilyScreenFiveViewController: BaseClassViewController, UITableViewDataSource, UITableViewDelegate {
    var request_visibility:String          = "27"
    var callBackdelegate:familySettingUpdateDelegate!

    var isSearchable:String = ""
    var groupType:String = ""
    
    var famArr : [familyModel]?
    var isFromGroup = ""
    //Private family
    var member_joining:String          = "2"
    var member_approval:String         = "4"
    var post_create:String             = "6"
    var post_approval:String           = "10" // not needed
    var post_visibilty:String          = "8"
   
    var announcement_create            = "17"
    var announcement_visibility        = "21"
    var announcement_approval          = "24" //not needed
   
    var link_family:String             = "13"
    var link_approval:String           = "16"
    
    var isCellHidden = "0"
    var isPostApproveCellHidden = "1"
    var isAnnouncementCellHidden = "1"
    var isFromSettings = false
    
    @IBOutlet weak var lblHead: UILabel!
    @IBOutlet weak var btnPrivacy: UIButton!
    @IBOutlet weak var swSearch: UISwitch!
    @IBOutlet weak var imgTemp: UIImageView!
    
    @IBOutlet weak var tbl                 : UITableView!
    
    var memberJoinFlag                     = 3
    var createPostFlag                     = 1
    var approvePostFlag                    = 1
    var linkFamilyFlag                     = 1
    var familyApprovalFlag                 = 2
    var memberApprovalFlag                 = 1
    var postVisibilityFlag                 = 2
    var createAnnouncementFlag             = 2
    var approveAnnouncementFlag            = 2
    var announcementVisibleFlag            = 2
    
    var switchDict                         = NSMutableDictionary()
    
    override func onScreenLoad() {
        isSearchable = "true"
        btnPrivacy.setTitle(groupType, for: .normal)
        
        print("groupType : \(groupType)")
        
        if groupType == "Public"{
            
            member_joining                 = "3"
            member_approval                = "5"
            post_create                    = "6"
            post_approval                  = "10" // not needed
            post_visibilty                 = "9"
            announcement_create            = "17"
            announcement_visibility        = "22"
            announcement_approval          = "24" //not needed
            link_family                    = "13"
            link_approval                  = "16"
            
            memberJoinFlag                 = 2
            createPostFlag                 = 1
            approvePostFlag                = 1
            linkFamilyFlag                 = 1
            familyApprovalFlag             = 2
            memberApprovalFlag             = 2
            postVisibilityFlag             = 1
            createAnnouncementFlag         = 2
            approveAnnouncementFlag        = 2
            announcementVisibleFlag        = 1
            request_visibility             = "27"
//            isCellHidden                   = "1"
        }
        
        
        switchDict.setValue(true, forKey: "doc")
        switchDict.setValue(false, forKey: "his")
        switchDict.setValue(true, forKey: "fam")
        switchDict.setValue(true, forKey: "prog")
        switchDict.setValue(true, forKey: "vac")
        
        if isFromGroup == "1"{
            settingsFromGroup()
        }
        self.navigationController?.navigationBar.isHidden    = true
    }
    
    override func isNavigationControllerVisible() -> Bool {
        return true
    }

    
    override func viewDidAppear(_ animated: Bool) {
        if isFromGroup == "1"{
            self.navigationController?.navigationBar.isHidden    = true
            self.tabBarController?.tabBar.isHidden               = false
            lblHead.text = "Advance Settings"
        }
        else{
            self.navigationController?.navigationBar.isHidden    = true
            self.tabBarController?.tabBar.isHidden               = true
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
        
            if self.groupType == "Public"{
                
                let btn = UIButton()
                btn.tag = 2
//                self.membersJoinClicked(sender: btn)
            }
            
        })
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        //self.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK:- Custom Method
    func settingsFromGroup(){
      //  print(famArr![0])
        member_joining = "\(famArr![0].memberJoining)"
        member_approval = "\(famArr![0].memberApproval)"
        post_create = "\(famArr![0].postCreate)"
        post_approval = "\(famArr![0].postApproval)"
        post_visibilty = "\(famArr![0].postVisibility)"
        link_family = "\(famArr![0].linkFamily)"
        link_approval = "\(famArr![0].linkFamilyApproval)"
        announcement_create = "\(famArr![0].announcementCreate)"
        announcement_visibility = "\(famArr![0].announcementVisibility)"
        announcement_approval = "\(famArr![0].announcementApproval)"
        request_visibility = "\(famArr![0].request_visibility)"
        print(request_visibility)
        if member_joining == "1"{
            memberJoinFlag = 1
            isCellHidden = "1"
        }
        else if member_joining == "2"{
            memberJoinFlag = 3
            isCellHidden = "0"
        }
        else if member_joining == "3"{
            memberJoinFlag = 2
            isCellHidden = "1"
        }
        
        if member_approval == "4"{
            memberApprovalFlag = 1
        }
        else if member_approval == "5"{
            memberApprovalFlag = 2
        }
        
        if post_create == "6"{
            createPostFlag = 1
            isPostApproveCellHidden = "1"
        }
        else if post_create == "7"{
            createPostFlag = 2
            isPostApproveCellHidden = "1"
        }
        else if post_create == "20"{
            createPostFlag = 3
            isPostApproveCellHidden = "0"
        }
        
        if post_visibilty == "8"{
            postVisibilityFlag = 2
        }
        else if post_visibilty == "9"{
            postVisibilityFlag = 1
        }
        
        if post_approval == "10"{
            approvePostFlag = 1
        }
        else if post_approval == "11"{
            approvePostFlag = 2
        }
        else if post_approval == "12"{
            approvePostFlag = 3
        }
        
        if link_family == "13"{
            linkFamilyFlag = 1
        }
        else if link_family == "14"{
            linkFamilyFlag = 2
        }
        
        if link_approval == "15"{
            familyApprovalFlag = 1
        }
        else if link_approval == "16"{
            familyApprovalFlag = 2
        }
        
        if announcement_create == "17"{
            createAnnouncementFlag = 2
            isAnnouncementCellHidden = "1"
        }
        else if announcement_create == "18"{
            createAnnouncementFlag = 1
            isAnnouncementCellHidden = "1"
        }
        else if announcement_create == "19"{
            createAnnouncementFlag = 3
            isAnnouncementCellHidden = "0"
        }
        if announcement_approval == "23"{
            approveAnnouncementFlag = 1
        }
        else if announcement_approval == "24"{
            approveAnnouncementFlag = 2
        }
        else if announcement_approval == "25"{
            approveAnnouncementFlag = 3
        }
        if announcement_visibility == "21"{
            announcementVisibleFlag = 1
        }
        else if announcement_visibility == "22"{
            announcementVisibleFlag = 2
        }
        

        
        tbl.reloadData()
    }
    
    //MARK:- Table View
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 12 //return 12
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tbl.dequeueReusableCell(withIdentifier: "cell0")!
        
        if indexPath.row == 0{
            
          let cell0 = tbl.dequeueReusableCell(withIdentifier: "cell0") as! SettingsCell_0
            
            return cell0
        }
        
        if indexPath.row == 1{
            
           let cell1 = tbl.dequeueReusableCell(withIdentifier: "cell1") as! SettingsCell_1
            
            if memberJoinFlag == 1{
                member_joining = "1"
                cell1.butt1.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
                cell1.butt2.setImage(nil, for: .normal)
                cell1.butt3.setImage(nil, for: .normal)
            }
            else if memberJoinFlag == 2{
                member_joining = "3"
                cell1.butt1.setImage(nil, for: .normal)
                cell1.butt2.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
                cell1.butt3.setImage(nil, for: .normal)
                
            }else{
                member_joining = "2"
                cell1.butt1.setImage(nil, for: .normal)
                cell1.butt2.setImage(nil, for: .normal)
                cell1.butt3.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
            }
            
            return cell1
        }
        if indexPath.row == 2{
            
            let cell8 = tbl.dequeueReusableCell(withIdentifier: "cell4") as! SettingsCell_4
            
            if isCellHidden == "1"{
                cell8.isHidden = true
            }
            else{
                cell8.isHidden = false
            }
            
            cell8.lblValue.text = "Member approval"
            cell8.butt1.tag = 3
            cell8.butt2.tag = 4
            cell8.btnValue1.tag = 3
            cell8.btnValue2.tag = 4
            
            if memberApprovalFlag == 1{
                
                cell8.butt1.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
                cell8.butt2.setImage(nil, for: .normal)
                
            }else{
                
                cell8.butt1.setImage(nil, for: .normal)
                cell8.butt2.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
            }
            
            return cell8
        }
        
        if indexPath.row == 3{
            
            let cell2 = tbl.dequeueReusableCell(withIdentifier: "cell8") as! SettingsCell_8
            
            cell2.lblHead.text = "Who can create posts, events, albums and \ndocuments?"
            cell2.lblHead.font = .systemFont(ofSize: 14)
            cell2.lblHead.numberOfLines = 2
            cell2.btn1.tag = 1
            cell2.btn2.tag = 2
            cell2.btn3.tag = 3
            cell2.btnAnyone.tag = 1
            cell2.btnAdminsOnly.tag = 2
            cell2.btnApproval.tag = 3
           
            cell2.btn3.isHidden = true
            cell2.btnApproval.isHidden = true
            
            if createPostFlag == 1{
                cell2.btn1.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
                cell2.btn2.setImage(nil, for: .normal)
                cell2.btn3.setImage(nil, for: .normal)
            }
            else if createPostFlag == 3{
                cell2.btn3.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
                cell2.btn2.setImage(nil, for: .normal)
                cell2.btn1.setImage(nil, for: .normal)
            }
            else{
                cell2.btn1.setImage(nil, for: .normal)
                cell2.btn2.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
                cell2.btn3.setImage(nil, for: .normal)
            }
            
            return cell2
        }
        
        if indexPath.row == 4{
            let cell9 = tbl.dequeueReusableCell(withIdentifier: "cell2") as! SettingsCell_2
            
            cell9.lblValue.text = "Who can see the post"
            cell9.butt1.tag = 4
            cell9.butt2.tag = 3
            cell9.btnValue1.tag = 4
            cell9.btnValue2.tag = 3
            
            if postVisibilityFlag == 1{
                cell9.butt1.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
//                cell9.butt2.setImage(nil, for: .normal)
                cell9.butt2.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
                
            }else{
                cell9.butt1.setImage(nil, for: .normal)
                cell9.butt2.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
            }
            
            if groupType.lowercased() == "public"{
                cell9.btnValue2.setTitle("Public", for: .normal)
            }
            
            cell9.butt1.isHidden = true
            cell9.btnValue1.isHidden = true
            return cell9
        }
        
        if indexPath.row == 5{
            
            let cell3 = tbl.dequeueReusableCell(withIdentifier: "cell3") as! SettingsCell_3
            
            cell3.lblHead.text = "Who can approve post?"
            cell3.butt1.tag = 1
            cell3.butt2.tag = 2
            cell3.butt3.tag = 3
            cell3.btt1Value.tag = 1
            cell3.butt2Value.tag = 2
            cell3.butt3Value.tag = 3
            
            if isPostApproveCellHidden == "1"{
                cell3.isHidden = true
            }
            else{
                cell3.isHidden = false
            }
            
            if approvePostFlag == 1{
                cell3.butt1.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
                cell3.butt2.setImage(nil, for: .normal)
                cell3.butt3.setImage(nil, for: .normal)
            }
            else if approvePostFlag == 2{
                
                cell3.butt1.setImage(nil, for: .normal)
                cell3.butt2.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
                cell3.butt3.setImage(nil, for: .normal)
                
            }else{
                
                cell3.butt1.setImage(nil, for: .normal)
                cell3.butt2.setImage(nil, for: .normal)
                cell3.butt3.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
            }
            
            return cell3
        }
        
        if indexPath.row == 6{
            
            let cell2 = tbl.dequeueReusableCell(withIdentifier: "cell8") as! SettingsCell_8
            
            cell2.lblHead.text = "Who can create announcement?"
            cell2.lblHead.font = .systemFont(ofSize: 15)
            cell2.btn1.tag = 5
            cell2.btn2.tag = 4
            cell2.btn3.tag = 6
            cell2.btnAnyone.tag = 5
            cell2.btnAdminsOnly.tag = 4
            cell2.btnApproval.tag = 6
            
            cell2.btn3.isHidden = true
            cell2.btnApproval.isHidden = true
            
            if createAnnouncementFlag == 1{
                cell2.btn1.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
                cell2.btn2.setImage(nil, for: .normal)
                cell2.btn3.setImage(nil, for: .normal)
            }
            else if createAnnouncementFlag == 3{
                cell2.btn3.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
                cell2.btn2.setImage(nil, for: .normal)
                cell2.btn1.setImage(nil, for: .normal)
            }
            else{
                cell2.btn1.setImage(nil, for: .normal)
                cell2.btn2.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
                cell2.btn3.setImage(nil, for: .normal)
            }
            
            return cell2
        }
        
        if indexPath.row == 7{
            let cell9 = tbl.dequeueReusableCell(withIdentifier: "cell2") as! SettingsCell_2
                   
            cell9.lblValue.text = "Who can see the announcement"
            cell9.butt1.tag = 5
            cell9.butt2.tag = 6
            cell9.btnValue1.tag = 5
            cell9.btnValue2.tag = 6
                   
            if announcementVisibleFlag == 1{
                cell9.butt1.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
//                cell9.butt2.setImage(nil, for: .normal)
                cell9.butt2.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
            }else{
                cell9.butt1.setImage(nil, for: .normal)
                cell9.butt2.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
            }
            if groupType.lowercased() == "public"{
                cell9.btnValue2.setTitle("Public", for: .normal)
            }
            
            cell9.butt1.isHidden = true
            cell9.btnValue1.isHidden = true
            return cell9
        }
        
        /*if indexPath.row == 8{
            
            let cell11 = tbl.dequeueReusableCell(withIdentifier: "cell3") as! SettingsCell_3
            
            cell11.lblHead.text = "Who can approve announcement?"
            cell11.butt1.tag = 4
            cell11.butt2.tag = 5
            cell11.butt3.tag = 6
            cell11.btt1Value.tag = 4
            cell11.butt2Value.tag = 5
            cell11.butt3Value.tag = 6
            
            if isAnnouncementCellHidden == "1"{
                cell11.isHidden = true
            }
            else{
                cell11.isHidden = false
            }
            
            if approveAnnouncementFlag == 1{
                cell11.butt1.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
                cell11.butt2.setImage(nil, for: .normal)
                cell11.butt3.setImage(nil, for: .normal)
            }
            else if approveAnnouncementFlag == 2{
                
                cell11.butt1.setImage(nil, for: .normal)
                cell11.butt2.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
                cell11.butt3.setImage(nil, for: .normal)
                
            }else{
                
                cell11.butt1.setImage(nil, for: .normal)
                cell11.butt2.setImage(nil, for: .normal)
                cell11.butt3.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
            }
            
            return cell11
        } */
        
        if indexPath.row == 8 { //9
            
            let cell4 = tbl.dequeueReusableCell(withIdentifier: "cell4") as! SettingsCell_4
            
            cell4.lblValue.text = "Who can link family?"
            cell4.butt1.tag = 1
            cell4.butt2.tag = 2
            cell4.btnValue1.tag = 1
            cell4.btnValue2.tag = 2
            
            if linkFamilyFlag == 1{
                
                cell4.butt1.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
                cell4.butt2.setImage(nil, for: .normal)
                
            }else{
                
                cell4.butt1.setImage(nil, for: .normal)
                cell4.butt2.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
            }
            
            return cell4
        }
        
        if indexPath.row == 9 { //10
            
            let cell5 = tbl.dequeueReusableCell(withIdentifier: "cell5") as! SettingsCell_5
            
            if familyApprovalFlag == 1{
                
                cell5.butt1.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
                cell5.butt2.setImage(nil, for: .normal)
                
            }else{
                
                cell5.butt1.setImage(nil, for: .normal)
                cell5.butt2.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
            }
            
            return cell5
        }
        
     /*   if indexPath.row == 6{
            
            let cell6 = tbl.dequeueReusableCell(withIdentifier: "cell6") as! SettingsCell_6
            
            if switchDict.value(forKey: "doc") as! Bool == true{
                cell6.docSwitch.setOn(true, animated: true)
            }else{
                 cell6.docSwitch.setOn(false, animated: true)
            }
            
            if switchDict.value(forKey: "his") as! Bool == true{
                cell6.hisSwitch.setOn(true, animated: true)
            }else{
                cell6.hisSwitch.setOn(false, animated: true)
            }
            
            if switchDict.value(forKey: "fam") as! Bool == true{
                cell6.famSwitch.setOn(true, animated: true)
            }else{
                cell6.famSwitch.setOn(false, animated: true)
            }
            
            
            if switchDict.value(forKey: "prog") as! Bool == true{
                cell6.progSwitch.setOn(true, animated: true)
            }else{
                cell6.progSwitch.setOn(false, animated: true)
            }
            
            if switchDict.value(forKey: "vac") as! Bool == true{
                cell6.vacSwitch.setOn(true, animated: true)
            }else{
                cell6.vacSwitch.setOn(false, animated: true)
            }
            
            
            return cell6
        }
        */
        
        if indexPath.row == 11 { // 11
            
            let cell7 = tbl.dequeueReusableCell(withIdentifier: "cell7")!
            
            return cell7
        }
        if indexPath.row == 10 {
            
            let cell11 = tbl.dequeueReusableCell(withIdentifier: "cell4") as! SettingsCell_4
                       
//                       if isCellHidden == "1"{
//                           cell8.isHidden = true
//                       }
//                       else{
//                           cell8.isHidden = false
//                       }
//
                       cell11.lblValue.text = "Request Visibility"
                       cell11.butt1.tag = 100
                       cell11.butt2.tag = 101
                       cell11.btnValue1.tag = 100
                       cell11.btnValue2.tag = 101
                       
            if self.request_visibility == "26"{
                           
                           cell11.butt1.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
                           cell11.butt2.setImage(nil, for:.normal)
                           
                       }else{
                           
                           cell11.butt1.setImage(nil, for: .normal)
                           cell11.butt2.setImage(#imageLiteral(resourceName: "roundTick"), for: .normal)
                       }
                       
                       return cell11
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if indexPath.row == 2{
//            return 0
//        }
        
        
        if indexPath.row == 0{
            
            return 64
        }
        if indexPath.row == 1 { //|| indexPath.row == 6
            return 230
        }
        
        if indexPath.row == 3{
            
            return 188
        }
        
        if indexPath.row == 8{ //11
            
            return 188//295
        }
        
        if indexPath.row == 8{ //11
            
            return 188
        }
        
        if isCellHidden == "1"{
            if indexPath.row == 2{
                return 0
            }
        }
        else if isCellHidden == "0"{
            if indexPath.row == 2{
                return 188
            }
        }
        
        if isPostApproveCellHidden == "1"{
            if indexPath.row == 5{
                return  0
            }
        }
        else if isPostApproveCellHidden == "0"{
            if indexPath.row == 5{
                return  241
            }
        }
        if isAnnouncementCellHidden == "1"{
            if indexPath.row == 8{
                return  0
            }
        }
        else if isAnnouncementCellHidden == "0"{
            if indexPath.row == 8{
                return  241
            }
        }
        
        if link_family == "13"{
            
            if indexPath.row == 9{
                
                return 0
            }
            
        }else if link_family == "14"{
            
            if indexPath.row == 9{
                
                return 188
            }
            
        }
        
        return 188
    }
    
    
    //MARK:- Cell ActionActions
    
    @IBAction func membersJoinClicked(sender : UIButton){
        
        if sender.tag == 1{
            memberJoinFlag = 1
            isCellHidden = "1"
            member_joining = "1"
        }else if sender.tag == 2{
            isCellHidden = "1"
            memberJoinFlag = 2
            member_joining = "3"
        }
        else{
            isCellHidden = "0"
            memberJoinFlag = 3
            member_joining = "2"
        }
        
        //tbl.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .none)
        tbl.reloadData()
    }
    
    @IBAction func createPostClicked(sender : UIButton){
        
        if sender.tag == 1{
            createPostFlag = 1
            post_create = "6"
//            tbl.reloadRows(at: [IndexPath(row: 3, section: 0)], with: .none)
        }else if sender.tag == 2{
            createPostFlag = 2
            post_create = "7"
//            tbl.reloadRows(at: [IndexPath(row: 3, section: 0)], with: .none)
        }
        else if sender.tag == 3{
            postVisibilityFlag = 2
            post_visibilty = "8"
            tbl.reloadRows(at: [IndexPath(row: 4, section: 0)], with: .none)
        }
        else if sender.tag == 4{
            postVisibilityFlag = 1
            post_visibilty = "9"
            tbl.reloadRows(at: [IndexPath(row: 4, section: 0)], with: .none)
        }
        else if sender.tag == 5{
            announcementVisibleFlag = 1
            announcement_visibility = "21"
            tbl.reloadRows(at: [IndexPath(row: 7, section: 0)], with: .none)
        }
        else if sender.tag == 6{
            announcementVisibleFlag = 2
            announcement_visibility = "22"
            tbl.reloadRows(at: [IndexPath(row: 7, section: 0)], with: .none)
        }
        
    }
    
    @IBAction func createPostSettingsClicked(sender : UIButton){
        
        
        if sender.tag == 1{
            createPostFlag = 1
            isPostApproveCellHidden = "1"
            post_create = "6"
            tbl.reloadRows(at: [IndexPath(row: 3, section: 0)], with: .none)
        }else if sender.tag == 2{
            createPostFlag = 2
            isPostApproveCellHidden = "1"
            post_create = "7"
            tbl.reloadRows(at: [IndexPath(row: 3, section: 0)], with: .none)
        }
        else if sender.tag == 3{
            createPostFlag = 3
            isPostApproveCellHidden = "0"
            post_create = "20"
            tbl.reloadRows(at: [IndexPath(row: 3, section: 0)], with: .none)
        }
       
        else if sender.tag == 4{
            createAnnouncementFlag = 2
            isAnnouncementCellHidden = "1"
            announcement_create = "17"
            tbl.reloadRows(at: [IndexPath(row: 6, section: 0)], with: .none)
          //  tbl.reloadData()
        }
        else if sender.tag == 5{
            createAnnouncementFlag = 1
            isAnnouncementCellHidden = "1"
            announcement_create = "18"
            tbl.reloadRows(at: [IndexPath(row: 6, section: 0)], with: .none)
          //  tbl.reloadData()
        }
        else if sender.tag == 6{
            createAnnouncementFlag = 3
            isAnnouncementCellHidden = "0"
            announcement_create = "19"
            tbl.reloadRows(at: [IndexPath(row: 6, section: 0)], with: .none)
           // tbl.reloadData()
        }
    }
    
    @IBAction func approvePostClicked(sender : UIButton){
        
        
        if sender.tag == 1{
            approvePostFlag = 1
            post_approval = "10"
            tbl.reloadRows(at: [IndexPath(row: 5, section: 0)], with: .none)
        }else if sender.tag == 2{
            approvePostFlag = 2
            post_approval = "11"
            tbl.reloadRows(at: [IndexPath(row: 5, section: 0)], with: .none)
        }else if sender.tag == 3{
            approvePostFlag = 3
            post_approval = "12"
            tbl.reloadRows(at: [IndexPath(row: 5, section: 0)], with: .none)
        }
        
        else if sender.tag == 4{
            approveAnnouncementFlag = 1
            announcement_approval = "23"
            tbl.reloadRows(at: [IndexPath(row: 8, section: 0)], with: .none)
        }
        else if sender.tag == 5{
            approveAnnouncementFlag = 2
            announcement_approval = "24"
            tbl.reloadRows(at: [IndexPath(row: 8, section: 0)], with: .none)
        }
        else if sender.tag == 6{
            approveAnnouncementFlag = 3
            announcement_approval = "25"
            tbl.reloadRows(at: [IndexPath(row: 8, section: 0)], with: .none)
        }
        
    }
    
    @IBAction func linkFamClicked(sender : UIButton){
        
        if sender.tag == 1{
            linkFamilyFlag = 1
            link_family = "13"
            tbl.reloadRows(at: [IndexPath(row: 8, section: 0)], with: .none)
        }else if sender.tag == 2{
            linkFamilyFlag = 2
            link_family = "14"
            tbl.reloadRows(at: [IndexPath(row: 8, section: 0)], with: .none)
        }
        else if sender.tag == 3{
            memberApprovalFlag = 1
            member_approval = "4"
            tbl.reloadRows(at: [IndexPath(row: 2, section: 0)], with: .none)
        }
            else if sender.tag == 100{
                       request_visibility = "26"
                       tbl.reloadRows(at: [IndexPath(row: 10, section: 0)], with: .none)
                   }
            else if sender.tag == 101{
                       request_visibility = "27"
                       tbl.reloadRows(at: [IndexPath(row: 10, section: 0)], with: .none)
                   }
        else{
            memberApprovalFlag = 2
            member_approval = "5"
            tbl.reloadRows(at: [IndexPath(row: 2, section: 0)], with: .none)
        }
        
        //tbl.reloadRows(at: [IndexPath(row: 5, section: 0)], with: .none)
        //tbl.reloadData()
    }
    
    @IBAction func familyApprovedClicked(sender : UIButton){
        
        if sender.tag == 1{
            familyApprovalFlag = 1
            link_approval = "15"
        }else{
            familyApprovalFlag = 2
            link_approval = "16"
        }
        
        tbl.reloadRows(at: [IndexPath(row: 9, section: 0)], with: .none)
    }
    
    //MARK: - Cell Switches
  /*
    @IBAction func docSwitchChanged(sender : UISwitch){
        
        if sender.isOn{
            switchDict.setValue(true, forKey: "doc")
        }else{
            switchDict.setValue(false, forKey: "doc")
        }
        
        tbl.reloadRows(at: [IndexPath(row: 6, section: 0)], with: .none)
    }
    
    @IBAction func historySwitchChanged(sender : UISwitch){
        
        if sender.isOn{
            switchDict.setValue(true, forKey: "his")
        }else{
            switchDict.setValue(false, forKey: "his")
        }
        
        tbl.reloadRows(at: [IndexPath(row: 6, section: 0)], with: .none)
    }
    
    @IBAction func familySwitchChanged(sender : UISwitch){
        
        if sender.isOn{
            switchDict.setValue(true, forKey: "fam")
        }else{
            switchDict.setValue(false, forKey: "fam")
        }
        
        tbl.reloadRows(at: [IndexPath(row: 6, section: 0)], with: .none)
    }
    
    @IBAction func programSwitchChanged(sender : UISwitch){
        
        if sender.isOn{
            switchDict.setValue(true, forKey: "prog")
        }else{
            switchDict.setValue(false, forKey: "prog")
        }
        
        tbl.reloadRows(at: [IndexPath(row: 6, section: 0)], with: .none)
    }
    
    @IBAction func vacancySwitchChanged(sender : UISwitch){
        
        if sender.isOn{
            switchDict.setValue(true, forKey: "vac")
        }else{
            switchDict.setValue(false, forKey: "vac")
        }
        
        tbl.reloadRows(at: [IndexPath(row: 6, section: 0)], with: .none)
    }
    */
    //MARK:- Button Actions
    
    @IBAction func onClickPrivacy(_ sender: Any) {
        let arr = ["Public","Private"]
        self.showActionSheet(titleArr: arr as NSArray, title: "Choose option") { (index) in
            if index == 100{
            }
            else{
                self.btnPrivacy.setTitle(arr[index] , for: .normal)
                self.groupType = arr[index]
            }
        }
    }
    
    @IBAction func onClickLink(_ sender: Any) {
        let famSettings = self.storyboard?.instantiateViewController(withIdentifier: "addFamilySix") as! AddFamilyScreenSixViewController
        famSettings.isFromLink = true
        self.navigationController?.pushViewController(famSettings, animated: true)
    }
    
    @IBAction func onClickSwitch(_ sender: Any) {
        if swSearch.isOn {
            isSearchable = "true"
        }
        else{
            isSearchable = "false"
        }
    }
    
    @IBAction func onClickNext(_ sender: Any) {
       /* let defaultValue = UserDefaults.init(suiteName: "createFamily")
        defaultValue?.set(groupType, forKey: "groupType")
        
        APIServiceManager.callServer.updateFamilyDetails(url: EndPoint.updateFamily, faId: defaultValue?.value(forKey: "groupId") as! String, faIntro: defaultValue?.value(forKey: "groupIntro") as! String, faType: groupType, isSearch: isSearchable, isLinkable: "true", coverPic: defaultValue?.value(forKey: "groupCover") as! NSData, logo: defaultValue?.value(forKey: "groupLogo") as! NSData, contentType: "Image", success: { (responseMdl) in
            
            guard let responseMdl = responseMdl as? familyListResponse else{
                return
            }
            ActivityIndicatorView.hiding()
            
            if responseMdl.statusCode == 200{
                print(responseMdl.familyList![0].faId)
                if responseMdl.familyList!.count > 0{
                    print(responseMdl.familyList![0].faId)
                    let intro = self.storyboard?.instantiateViewController(withIdentifier: "addFamilyFour") as! AddFamilyScreenFourViewController
                    self.navigationController?.pushViewController(intro, animated: true)
                }
            }
            
        }) { (error) in
            
        }*/
        let defaultValue = UserDefaults.init(suiteName: "createFamily")
        var groupId = String()
        if isFromGroup == "1"{
            groupId = "\(famArr![0].faId)"
            
        }
        else{
            groupId = defaultValue?.value(forKey: "groupId") as! String
        }
        print(self.request_visibility)
        APIServiceManager.callServer.updateFamilySettings(url: EndPoint.updateSettings, groupId: groupId, memberJoin: member_joining, memberApproval: member_approval, postCreate: post_create, postVisible: post_visibilty, postApproval: post_approval, announcementCreate: announcement_create, announcementVisible: announcement_visibility, announcementApproval: announcement_approval, linkFamily: link_family, linkApproval: link_approval,request_visibility:self.request_visibility , active: "true", success: { (responseMdl) in
            
            guard let responseMdl = responseMdl as? familyListResponse else{
                return
            }
            self.famArr = responseMdl.familyList

            ActivityIndicatorView.hiding()
            if responseMdl.statusCode == 200{
                
                if self.isFromGroup == "1"{
                    var array : [UIViewController] = (self.navigationController?.viewControllers)!
                    array.remove(at: array.count - 1)
                    array.remove(at: array.count - 1)
                    self.callBackdelegate.updateFamilySetting(famArr: self.famArr!)
                    self.navigationController?.viewControllers = array
                    self.navigationController?.popViewController(animated: true)
                    
                }
                else{
                    let stryboard = UIStoryboard.init(name: "second", bundle: nil)
                    
                    let family = stryboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
                    family.groupId = defaultValue?.value(forKey: "groupId") as! String
                    family.isFromCreate = true
                    self.navigationController?.pushViewController(family, animated: true)
                }
            }
            
        }) { (error) in
            
        }
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickSkip(_ sender : UIButton){
        
        let defaultValue = UserDefaults.init(suiteName: "createFamily")
        var groupId = String()
        if isFromGroup == "1"{
            groupId = "\(famArr![0].faId)"
            
        }
        else{
            groupId = defaultValue?.value(forKey: "groupId") as! String
        }
        
        APIServiceManager.callServer.updateFamilySettings(url: EndPoint.updateSettings, groupId: groupId, memberJoin: member_joining, memberApproval: member_approval, postCreate: post_create, postVisible: post_visibilty, postApproval: post_approval, announcementCreate: announcement_create, announcementVisible: announcement_visibility, announcementApproval: announcement_approval, linkFamily: link_family, linkApproval: link_approval,request_visibility:self.request_visibility , active: "true", success: { (responseMdl) in
            
            guard let responseMdl = responseMdl as? familyListResponse else{
                return
            }
            ActivityIndicatorView.hiding()
            if responseMdl.statusCode == 200{
                if self.isFromGroup == "1"{
                    self.navigationController?.popViewController(animated: true)

                }
                else{
                    let stryboard = UIStoryboard.init(name: "second", bundle: nil)
                    
                    let family = stryboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
                    family.groupId = defaultValue?.value(forKey: "groupId") as! String
                    family.isFromCreate = true
                    self.navigationController?.pushViewController(family, animated: true)
                }
                
            }
            
        }) { (error) in
            
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
