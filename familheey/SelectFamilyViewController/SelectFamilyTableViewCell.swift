//
//  SelectFamilyTableViewCell.swift
//  familheey
//
//  Created by Giri on 25/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class SelectFamilyTableViewCell: UITableViewCell {

    @IBOutlet weak var lblFamilyName: UILabel!
    @IBOutlet weak var viewRightAction: UIView!
    @IBOutlet weak var lblRightAction: UILabel!
    @IBOutlet weak var btnrightAction: UIButton!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var imgLogo           : UIImageView!

    
    
      @IBOutlet weak var lblType           : UILabel!
      @IBOutlet weak var lblMembersCount: UILabel!
      @IBOutlet weak var lblKnown: UILabel!
      @IBOutlet weak var lblKnownHead: UILabel!
      @IBOutlet weak var lblMemberHead: UILabel!
      @IBOutlet weak var imgLocation: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
