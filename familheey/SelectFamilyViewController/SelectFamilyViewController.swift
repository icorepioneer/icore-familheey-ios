//
//  SelectFamilyViewController.swift
//  familheey
//
//  Created by Giri on 25/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class SelectFamilyViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noDataView: UIView!
    
    @IBOutlet weak var btnSearchReset: UIButton!
    @IBOutlet weak var searchField: UITextField!
    var FamilyList : [GroupDetail]?
    var selectedUserID = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchField.delegate = self
        
        if selectedUserID.isEmpty{
            self.navigationController?.popViewController(animated: true)
//            self.dismiss(animated: true, completion: nil)
        }
        else{
            getAllFamilies()
        }
        tableView.tableFooterView = UIView.init()
        self.definesPresentationContext = true
        self.tabBarController?.tabBar.isHidden = true
    }
    
    //MARK:- Button Actions
    
    @IBAction func onClickRightAction(_ sender: UIButton) {
        let family = FamilyList?[sender.tag]
        if let groupId = family?.id{
            ActivityIndicatorView.show("Please wait!")
            APIServiceManager.callServer.sendMemberInvitation(url: EndPoint.inviteMember, fromId: UserDefaults.standard.value(forKey: "userId") as! String , toId: selectedUserID, groupId: "\(groupId)", success: { (response) in
                ActivityIndicatorView.hiding()
                self.displayAlertChoice(alertStr: "Member invited successfully!!!", title: "") { (g) in
                    self.navigationController?.popViewController(animated: true)
                }
            }) { (error) in
                ActivityIndicatorView.hiding()
            }
        }
    }
    @IBAction func onClickBG(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func clearSearchAction(_ sender: Any) {
        
        searchField.text = ""
        getAllFamilies()
        
    }
    

    
    //MARK:- Custom functions
    func getAllFamilies(){
        ActivityIndicatorView.show("Please wait!")
        
        APIServiceManager.callServer.getGroupsToAddMember(url: EndPoint.GroupsToAddMember, memberId: selectedUserID, userId: UserDefaults.standard.value(forKey: "userId") as! String,searchTxt:searchField.text!, success: { (response) in
            ActivityIndicatorView.hiding()
            
            if let results = response as! GroupsToAddMembersList?{
                self.FamilyList = results.result
            }
            if self.FamilyList?.count != 0{
                self.tableView.isHidden = false
                self.noDataView.isHidden = true
                self.tableView.dataSource = self
                self.tableView.delegate = self
                self.tableView.reloadData()
            }
            else{
                self.tableView.isHidden = true
                self.noDataView.isHidden = false
            }
        }) { (error) in
            ActivityIndicatorView.hiding()
            self.tableView.isHidden = true
            self.noDataView.isHidden = false


        }
        
    }
    //MARK:- Tableview Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return  FamilyList?.count ?? 0
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectFamilyTableViewCell", for: indexPath) as! SelectFamilyTableViewCell
        let family = FamilyList?[indexPath.row]
        tableView.allowsSelection = false
        cell.lblFamilyName.text = family?.group_name
        cell.lblLocation.text = family?.base_region
        
        
        if family?.logo.count != 0{
                   let temp = "\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+family!.logo.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                   let imgurl = URL(string: temp)
                   let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+imgurl!.relativeString
                   let url = URL(string: newUrlStr)
                   cell.imgLogo.kf.indicatorType = .activity

                   cell.imgLogo.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
               }
               else{
                   cell.imgLogo.image = #imageLiteral(resourceName: "Family Logo")
               }
                cell.lblType.text = family?.group_category

        let member_count = family!.member_count
              
        if Int(member_count) ?? 0  > 0{
                  cell.lblMembersCount.text = member_count
                  cell.lblMemberHead.isHidden = false
                  cell.lblMembersCount.isHidden = false
              }
              else{
                  cell.lblMemberHead.isHidden = true
                  cell.lblMembersCount.isHidden = true
              }
              
        let knowncount = family!.knowncount
              
if Int(knowncount) ?? 0 > 0{
                  cell.lblKnown.text = knowncount
                  cell.lblKnownHead.isHidden = false
                  cell.lblKnown.isHidden = false
              }
              else{
                  cell.lblKnownHead.isHidden = true
                  cell.lblKnown.isHidden = true
              }
       

        
        
    
        
        cell.btnrightAction.tag = indexPath.row
        if let status = family?.JOINED{
            if status == true{
                cell.lblRightAction.text = "MEMBER"
                cell.btnrightAction.isUserInteractionEnabled = false
                cell.btnrightAction.backgroundColor = UIColor.init(white: 1.0, alpha: 0.5)

            }
            else{
                if let status = family?.request_status {
                    if status.lowercased() == "pending"{
                        cell.lblRightAction.text = "PENDING"
                        cell.btnrightAction.isUserInteractionEnabled = false
                        cell.btnrightAction.backgroundColor = UIColor.init(white: 1.0, alpha: 0.5)
                    }
                    else if status.lowercased() == "rejected"{
                        cell.lblRightAction.text = "REJECTED"
                        cell.btnrightAction.isUserInteractionEnabled = false
                        cell.btnrightAction.backgroundColor = UIColor.init(white: 1.0, alpha: 0.5)

                    }
                    else{
                        cell.lblRightAction.text = "ADD"
                        cell.btnrightAction.isUserInteractionEnabled = true
                        cell.btnrightAction.backgroundColor = .clear

                    }
                }
            }
        }
        else{
            cell.lblRightAction.text = ""
            cell.btnrightAction.isUserInteractionEnabled = false
            cell.btnrightAction.backgroundColor = UIColor.init(white: 1.0, alpha: 0.5)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    
}



extension SelectFamilyViewController : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
            if textField.text!.count > 0{
                btnSearchReset.isHidden = false
            }
            else{
                btnSearchReset.isHidden = true
            }
            textField.endEditing(true)
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        self.view.endEditing(true)
            if textField.text!.count > 0{
                btnSearchReset.isHidden = false
            }
            else{
                btnSearchReset.isHidden = true
            }
            getAllFamilies()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
        }
        return true
    }
    
}
