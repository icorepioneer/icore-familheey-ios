//
//  EditTopicsViewController.swift
//  familheey
//
//  Created by ANIL K on 22/04/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON
import DKImagePickerController
import Photos


class EditTopicsViewController: UIViewController,SelectedUsersForTopicsDelegate {
    
    @IBOutlet weak var viewUserSelection : UIView!
    @IBOutlet weak var textfieldPosthisTo: UITextField!
    @IBOutlet weak var textViewDesc: UITextView!
    @IBOutlet weak var textfieldtittle: UITextField!
    @IBOutlet weak var lblNoFiles: UILabel!
    @IBOutlet weak var collViewOfAttachments: UICollectionView!
    
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    
    var isFromCreate = false
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var pickerController: DKImagePickerController!
    var uploadStatus = false
    var selectedAttachments = NSMutableArray()
    var imgDataArr = [Data]()
    //    var videoDataArr = [Data]()
    //    var documentDataArr = [Data]()
    var selectedItemsexisted = Array<[String:Any]>()
    var toUsers = [String]()
    var topicId = ""
    var isFromEdit = false
    var ArrayTopics = [JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getTopicDetails()
    }
    
    //MARK:- Web API
    func getTopicDetails(){
        let param = ["user_id" : UserDefaults.standard.value(forKey: "userId") as! String, "topic_id":self.topicId]
        ActivityIndicatorView.show("Please wait....")
        networkProvider.request(.topic_detail(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    ActivityIndicatorView.hiding()
                    let jsonData =  JSON(response.data)
                    
                    if response.statusCode == 200{
                        print(jsonData)
                        if let response = jsonData["data"].array{
                            self.ArrayTopics.append(contentsOf: response)
                            
                            self.textfieldtittle.text = self.ArrayTopics[0]["title"].stringValue
                            self.textViewDesc.text = self.ArrayTopics[0]["description"].stringValue
                            
                            if let toUserId = self.ArrayTopics[0]["to_users"].array {
                                if toUserId.count  == 1{
                                    self.textfieldPosthisTo.text = "1 people selected"
                                }
                                else{
                                    self.textfieldPosthisTo.text = "\(toUserId.count) people selected"
                                }
                                
                                for index in toUserId{
                                    let userIds = index["user_id"].stringValue
                                    self.toUsers.append(userIds)
                                }
                                print("1 people selected")
                                print(self.toUsers)
                            }
                            
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getTopicDetails()
                        }
                    }
                    else{
                        
                    }
                    
                    
                }catch _ {
                    ActivityIndicatorView.hiding()
                }
            case .failure( _):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    func updateTopic(){
        guard self.textfieldtittle.text!.trimmingCharacters(in: .whitespacesAndNewlines).count > 0 else {
            Helpers.showAlertDialog(message: "Please add tittle for topic", target: self)
            return
        }
        guard toUsers.count != 0 else {
            Helpers.showAlertDialog(message: "Please add atleast one person to this topic", target: self)
            return
        }
        var desc = self.textViewDesc.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if desc == "Enter here"{
            desc = ""
        }
        let users = toUsers.compactMap { Int($0) }
        
        let params = ["updated_by":UserDefaults.standard.value(forKey: "userId") as! String,
                      "to_user_id":users,
                      "title":self.textfieldtittle.text ?? "",
                      "description":desc,
                      "topic_attachment":self.selectedAttachments,"topic_id":self.topicId] as [String:Any]
        
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.update_topic(parameter: params), completion: {(result) in
            
            switch result {
            case .success(let response):
                do {
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    print(jsonData["status"])
                    print(response.statusCode)
                    if response.statusCode == 200{
                        let AlertCus = CustomAlertVC.show(in: self, title: "Success", message: "Topic Updated", image:UIImage.init(named: "Green_tick")!)
                        AlertCus.addAction(CPAlertAction.init(title: "OK", type: .normal, handler: {
                            self.navigationController?.popViewController(animated: true)
                        }))
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.updateTopic()
                        }
                    }
                    else{
                        Helpers.showAlertDialog(message: "Error in Topic upload", target: self)
                    }
                } catch let err {
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
    
    //MARK:- Button actions
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickUpdate(_ sender: Any) {
        updateTopic()
    }
    @IBAction func onClickPostthisTo(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
        let intro = storyboard.instantiateViewController(withIdentifier: "selectUsersForTopicViewController") as! selectUsersForTopicViewController
        intro.delegate = self
        intro.topic_id = self.topicId
        self.navigationController?.pushViewController(intro, animated: true)
    }
    
    //MARK:- Custom Methods
    func selectedUserCallBack(SelectedUserId: NSMutableArray) {
        toUsers = SelectedUserId as! [String]
        if SelectedUserId.count == 0{
            self.textfieldPosthisTo.text = ""
        }
        else if SelectedUserId.count == 1{
            self.textfieldPosthisTo.text = "1 people selected"
        }
        else{
            self.textfieldPosthisTo.text = "\(SelectedUserId.count) peoples selected"
        }
        
    }
}
