//
//  CreateTopicViewController.swift
//  familheey
//
//  Created by GiRi on 20/04/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit
import DKImagePickerController
import Photos
import Moya
import SwiftyJSON


class CreateTopicViewController: UIViewController,UITextViewDelegate,UICollectionViewDelegateFlowLayout,SelectedUsersForTopicsDelegate {
    
    @IBOutlet weak var viewUserSelection : UIView!
    @IBOutlet weak var textfieldPosthisTo: UITextField!
    @IBOutlet weak var textViewDesc: UITextView!
    @IBOutlet weak var textfieldtittle: UITextField!
    @IBOutlet weak var lblNoFiles: UILabel!
    @IBOutlet weak var collViewOfAttachments: UICollectionView!
    @IBOutlet weak var tblTopicHistory: UITableView!
    @IBOutlet weak var lblHead: UILabel!
    
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    
    var isFromCreate = false
    var isFromProfile = false
    var isFromDiscover = false
    var isFromMembership = false
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var pickerController: DKImagePickerController!
    var uploadStatus = false
    var selectedAttachments = NSMutableArray()
    var imgDataArr = [Data]()
    //    var videoDataArr = [Data]()
    //    var documentDataArr = [Data]()
    var selectedItemsexisted = Array<[String:Any]>()
    var toUsers = [String]()
    var ArrayTopics = [JSON]()
    let maxNumberOfLines = 2
    var refreshControl = UIRefreshControl()
    var initiallimit = 1000
    var offset = 0
    var limit = 0
    var stopPagination = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textViewDesc.delegate = self
        textViewDesc.text = "Enter here"
        textViewDesc.textColor = UIColor.lightGray
        //        toUsers = ["940"]
        self.collViewOfAttachments.delegate = self
        self.collViewOfAttachments.dataSource = self
        self.lblHead.isHidden = true
        
        if !isFromCreate{
            viewUserSelection.isHidden = true
            tblTopicHistory.isHidden = true
            getTopicHistory()
//            selectedUserCallBack(SelectedUserId: toUsers as! NSMutableArray)
            
        }
        else{
            tblTopicHistory.isHidden = true
        }
        
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func sendInviteAction(_ sender: Any) {
        createtopic()
    }
    
    @IBAction func postThistoAction(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
        let intro = storyboard.instantiateViewController(withIdentifier: "selectUsersForTopicViewController") as! selectUsersForTopicViewController
        intro.delegate = self
        self.navigationController?.pushViewController(intro, animated: true)
    }
    @IBAction func attachAction(_ sender: Any) {
        
        if selectedItemsexisted.count > 0{
            
            return
        }
        if self.imgDataArr.count != 0  && !uploadStatus
        {
            
            Helpers.showAlertDialog(message: "Uploading images Please Wait a moment.", target: self)
        }
        else{
            self.selectImage(index: 0)
            
        }
    }
    @IBAction func btnCloseImage_Onclick(_ sender: UIButton) {
        
        if self.imgDataArr.count != 0  && !uploadStatus{
            
            let alert = UIAlertController(title: "Familheey", message: "Do you really want to cancel upload?", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "YES", style: .destructive, handler: { (acion) in
                
                if  self.uploadStatus
                {
                    self.deleteAlreadyUploaddedFiles(index:sender.tag)
                }
                else
                {
                    Helpers.StopAPICALL()
                }
                
            }))
            
            alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
            
        }

        else
        {
            self.deleteAlreadyUploaddedFiles(index: sender.tag)
        }
        
        
    }
    //MARK:- Custom methods
    func selectedUserCallBack(SelectedUserId: NSMutableArray) {
        toUsers = SelectedUserId as! [String]
        if SelectedUserId.count == 0{
            self.textfieldPosthisTo.text = ""
        }
        else if SelectedUserId.count == 1{
            self.textfieldPosthisTo.text = "1 people selected"
        }
        else{
            self.textfieldPosthisTo.text = "\(SelectedUserId.count) peoples selected"
        }
        self.getTopicHistory()
    }
    func deleteAlreadyUploaddedFiles(index:Int){
        if uploadStatus  {
            
            self.imgDataArr = []
            self.selectedItemsexisted.removeLast()
            self.selectedAttachments.removeLastObject()
            if self.selectedItemsexisted.count != 0{
                self.collViewOfAttachments.isHidden = false
                self.lblNoFiles.isHidden = true
                self.collViewOfAttachments.reloadData()
                
            }
            else{
                self.collViewOfAttachments.isHidden = true
                self.lblNoFiles.isHidden = false
            }
            
        }
        else{
            self.selectedItemsexisted.remove(at: index)
            self.selectedAttachments.removeObject(at: index)
            if self.selectedItemsexisted.count != 0
            {
                self.collViewOfAttachments.isHidden = false
                self.lblNoFiles.isHidden = true
                self.collViewOfAttachments.reloadData()
                
            }
            else
            {
                self.collViewOfAttachments.isHidden = true
                self.lblNoFiles.isHidden = false
                
            }
        }
    }
    
    //MARK:- create topic
    
    func createtopic(){
        
        guard self.textfieldtittle.text!.trimmingCharacters(in: .whitespacesAndNewlines).count > 0 else {
            Helpers.showAlertDialog(message: "Please add title for topic", target: self)
            return
        }
        guard toUsers.count != 0 else {
            Helpers.showAlertDialog(message: "Please add atleast one person to this topic", target: self)
            return
        }
        //        if !uploadStatus
        //        {
        //            Helpers.showAlertDialog(message: "Uploading images Please Wait a moment.", target: self)
        //            return
        //        }
        var desc = self.textViewDesc.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if desc == "Enter here"{
            desc = ""
        }
        let users = toUsers.compactMap { Int($0) }
        
        let params = ["created_by":UserDefaults.standard.value(forKey: "userId") as! String,
                      "to_user_id":users,
                      "title":self.textfieldtittle.text ?? "",
                      "description":desc,
                      "topic_attachment":self.selectedAttachments] as [String:Any]
        
        print(params)
        
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.topic_create(parameter: params), completion: {(result) in
            
            switch result {
            case .success(let response):
                do {
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    print(jsonData["status"])
                    print(response.statusCode)
                    if response.statusCode == 200{
                        let AlertCus = CustomAlertVC.show(in: self, title: "Success", message: "Successfully created Topic", image:UIImage.init(named: "Green_tick")!)
                        AlertCus.addAction(CPAlertAction.init(title: "OK", type: .normal, handler: {
                            let storyboard = UIStoryboard.init(name: "Topic", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "TopicsConversationViewController") as! TopicsConversationViewController
                            let temp = jsonData["data"]
                            vc.topicID = temp["id"].stringValue
                            vc.isFromCreate = true
                            vc.isFromProfile = self.isFromProfile
                            vc.isFromDiscover = self.isFromDiscover
                            vc.isFromMembership = self.isFromMembership
                            self.navigationController?.pushViewController(vc, animated: true)
                        }))
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.createtopic()
                        }
                    }
                    else{
                        Helpers.showAlertDialog(message: "Error in Topic upload", target: self)
                    }
                } catch let err {
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
        
        
    }
    
    //MARK:- List history
    func getTopicHistory(){
        
        ActivityIndicatorView.show("Please wait....")
        if offset == 0{
            ArrayTopics.removeAll()
            ActivityIndicatorView.show("Please wait....")
        }else{
            tblTopicHistory.showLoading()
        }
        self.stopPagination = false
        
        let param = ["login_user" : UserDefaults.standard.value(forKey: "userId") as! String, "second_user":self.toUsers[0]] as [String : Any]
        print(param)
        
        networkProvider.request(.commonTopicListByUsers(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    
                    if self.offset == 0{
                        ActivityIndicatorView.hiding()
                    }else{
                        self.tblTopicHistory.hideLoading()
                    }
                    
                    let jsonData =  JSON(response.data)
                    
                    if response.statusCode == 200{
                        
                        if let response = jsonData["data"].array{
                            
                            print("Json data reponse contents : \(response.count) Offset = \(self.offset) Limit = \(response)")
                            if response.count == 0{
                                self.stopPagination = true
                            }
                            self.ArrayTopics.append(contentsOf: response)
                            
                            
                            
                            if self.ArrayTopics.count > 0 {
                                self.lblHead.isHidden = false
                                self.tblTopicHistory.isHidden = false
                                
                                if self.offset == 0{
                                    self.tblTopicHistory.delegate = self
                                    self.tblTopicHistory.dataSource = self
                                    self.tblTopicHistory.reloadData()
                                }
                                
                                if self.offset <= self.limit{
                                }
                            }
                            else{
                                self.tblTopicHistory.isHidden = true
                                self.lblHead.isHidden = true
                            }
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getTopicHistory()
                        }
                    }
                    
                }catch _ {
                    if self.offset == 0{
                        ActivityIndicatorView.hiding()
                    }
                    else{
                        self.tblTopicHistory.hideLoading()
                        
                    }
                }
            case .failure( _):
                if self.offset == 0{
                    ActivityIndicatorView.hiding()
                }
                else{
                    self.tblTopicHistory.hideLoading()
                }                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    
    //MARK:- show picker
    
    func selectImage(index:Int){
        pickerController = DKImagePickerController()
        DKImageExtensionController.unregisterExtension(for:.camera)
        
        if index == 0{
            pickerController.assetType = .allPhotos
        }
        else{
            pickerController.assetType = .allVideos
            DKImageExtensionController.registerExtension(extensionClass: CustomCameraExtension.self, for: .camera)
        }
        pickerController.maxSelectableCount = 1
        pickerController.deselectAll()
        
        pickerController.didSelectAssets = { [unowned self] (assets: [DKAsset]) in
            
            self.imgDataArr = []
            //            self.videoDataArr = []
            //            self.sizeArray = []
            for item in assets {
                switch item.type {
                case .photo:
                    let photo = item
                    self.getDatafrom(asset: photo.originalAsset, count: assets.count)
                default:
                    break
                    //                case .video:
                    //                    let video = item
                    //                    self.getDatafrom(asset: video.originalAsset, count: assets.count)
                }
            }
            self.uploadStatus = false
            for item in assets{
                var dict = [String:Any]()
                //                dict["existing"] = false
                dict["value"] = item
                self.selectedItemsexisted.append(dict)
                
            }
            
            if self.selectedItemsexisted.count != 0{
                self.collViewOfAttachments.isHidden = false
                self.lblNoFiles.isHidden = true
                self.collViewOfAttachments.reloadData()
            }
            else{
                self.collViewOfAttachments.isHidden = true
                self.lblNoFiles.isHidden = false
            }
        }
        
        if pickerController.UIDelegate == nil {
            pickerController.UIDelegate = AssetClickHandler()
        }
        
        self.present(pickerController, animated: true) {}
        
    }
    func getDatafrom(asset: PHAsset?, count:Int){
        if asset == nil{
            return
        }
        let manager = PHImageManager.default()
        if asset?.mediaType == .image{
            manager.requestImageData(for: asset!, options: nil) { (data, string, orientation, info) in
                guard let imageData = data else {
                    return
                }
                let img = UIImage.init(data: imageData)
                self.imgDataArr.append(imageData)
                if self.imgDataArr.count == count{
                    self.uploadStatus = false
                    self.uploadImageMultipleImages(arr: self.imgDataArr, sizeArray: [["W":"\(String(describing: img!.size.width))","H":"\(String(describing:img!.size.height))"]])
                }
            }
        }
        //         else if asset?.mediaType == .video{
        //             manager.requestAVAsset(forVideo: asset!, options: nil, resultHandler: { (avasset, audio, info) in
        //                 if let avassetURL = avasset as? AVURLAsset {
        //                     guard let video = try? Data(contentsOf: avassetURL.url) else {
        //                         return
        //                     }
        //                     self.videoDataArr.append(video)
        //                     if self.videoDataArr.count == count{
        //                         self.vedioUploadStatus = false
        //                         self.uploadVideoMultiple(arr: self.videoDataArr)
        //                     }
        //                 }
        //             })
        //         }
        
    }
    func generateThumbnail(asset: AVAsset) -> UIImage? {
        do {
            let imageGenerator = AVAssetImageGenerator.init(asset: asset)
            imageGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imageGenerator.copyCGImage(at: .zero,
                                                         actualTime: nil)
            return UIImage(cgImage: cgImage)
        } catch {
            return #imageLiteral(resourceName: "Family Logo")
        }
    }
    
    
    //MARK: - upload images
    
    func uploadImageMultipleImages(arr : [Data],sizeArray:[[String:String]]){
        
        let param = ["name" :"post"] as [String : Any]
        
        
        //        ActivityIndicatorView.show("Uploading...")
        Helpers.requestWithMultipleImages(fileParamName: "file", urlAppendString: "upload_file_tos3", imageData: arr, parameters: param, mimeType: "image/png") { (response) in
            print("Response : \(response)")
            print(response["data"].arrayValue)
            if response["data"].arrayValue.count != 0
            {
                self.uploadStatus = true
                
                self.collViewOfAttachments.reloadData()
                //success condition
                let arrayOfData = response["data"].arrayValue
                
                for currentdata in arrayOfData{
                    var dict = [String:String]()
                    dict["filename"] = currentdata["filename"].stringValue
                    dict["type"] = currentdata["type"].stringValue
                    dict["width"] = sizeArray[0]["W"]
                    dict["height"] = sizeArray[0]["H"]
                    print(dict)
                    self.selectedAttachments.add(dict)
                }
                
            }
            else
            {
                //failed Condition
                self.uploadStatus = true
                self.imgDataArr = []
                if self.selectedItemsexisted.count != 0{
                    self.selectedItemsexisted.removeAll()
                }
                
                if self.selectedItemsexisted.count != 0{
                    self.collViewOfAttachments.isHidden = false
                    self.lblNoFiles.isHidden = true
                    self.collViewOfAttachments.reloadData()
                    
                }
                else{
                    self.collViewOfAttachments.isHidden = true
                    self.lblNoFiles.isHidden = false
                }
                
            }
        }
    }
    
    //MARK:- Textview Delegates
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Enter here"
            textView.textColor = UIColor.lightGray
        }
    }
    
}
extension CreateTopicViewController:UICollectionViewDelegate,UICollectionViewDataSource{
    //MARK:- collectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.selectedItemsexisted.count
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PostImageCollectionViewCell
        let item = self.selectedItemsexisted[indexPath.row]
        cell.btnClose.tag = indexPath.row
        let currentItem = item["value"] as! DKAsset
        
        switch currentItem.type {
        case .photo:
            
            currentItem.fetchImage(with: CGSize.init(width: 200, height: 200), completeBlock: { image, info in
                cell.imgOfPostItems.image = image
            })
            
            cell.imgOfPlayVideo.isHidden = true
            if !uploadStatus
            {
                
                let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "imageProgressor", withExtension: "gif")!)
                let advTimeGif = UIImage.gifImageWithData(imageData!)
                cell.imgOfProgress.image = advTimeGif
                cell.activityIndicator.isHidden = false
                
            }
            else
            {
                cell.imgOfProgress.image = #imageLiteral(resourceName: "completed_progress.png")
                cell.activityIndicator.isHidden = true
                
            }
            
        case .video:
            currentItem.fetchImage(with: CGSize.init(width: 200, height: 200), completeBlock: { image, info in
                //                    self.selectedImageV.image = image
                cell.imgOfPostItems.image = image
                
            })
            
            cell.imgOfPlayVideo.isHidden = false
            if !uploadStatus
            {
                
                let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "imageProgressor", withExtension: "gif")!)
                let advTimeGif = UIImage.gifImageWithData(imageData!)
                cell.imgOfProgress.image = advTimeGif
                cell.activityIndicator.isHidden = false
                
            }
            else
            {
                cell.imgOfProgress.image = #imageLiteral(resourceName: "completed_progress.png")
                cell.activityIndicator.isHidden = true
                
            }
            
        }
        //        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize.init(width: 90, height:  90)
    }
    
}

extension CreateTopicViewController :UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ArrayTopics.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard ArrayTopics.count != 0 else {
            return UITableViewCell.init()
        }
        
        let post = ArrayTopics[indexPath.row]
        
        
        let cellid = "PostImageTableViewCell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! PostImageTableViewCell
        
        cell.readmoreButton.tag = indexPath.row
        cell.buttonShare.tag = indexPath.row
        cell.buttonRightMenu.tag = indexPath.row
        cell.commentsButton.tag = indexPath.row
        cell.viewsButton.tag = indexPath.row
        cell.sharedUsersButton.tag = indexPath.row
        cell.buttonComments.tag = indexPath.row
        //        cell.btnFamilyClick.tag = indexPath.row
        
        //Active label color
        cell.labelPost.mentionColor = .black
        cell.labelPost.hashtagColor = .black
        cell.labelPost.URLColor = .black
        
        //User Details
        if let users = post["to_users"].array, users.count > 0{
            if users.count == 1{
                cell.labelUserName.text = users[0].string ?? ""
            }
            else if users.count == 2{
                cell.labelUserName.text = "\(users[0].string ?? ""), \(users[1].string ?? "")"
            }
            else{
                cell.labelUserName.text = "\(users[0].string ?? ""), \(users[1].string ?? "") and \(users.count - 2) others"
            }
        }
        else{
            cell.labelUserName.text = ""
        }
        cell.labelDate.text = ConversationsViewController.formatDateString(dateStr: post["created_at"].stringValue)
        //timeAgoSinceDate(dateStr: post["created_at"].stringValue)
        //Post
        cell.labelPost.text = post["title"].string ?? ""
        
        //Details
        cell.lableNumberOfConversations.text = post["conversation_count"].stringValue
        if post["conversation_count_new"].intValue > 0{
            cell.lblNewConvrstn.isHidden = false
        }
        else{
            cell.lblNewConvrstn.isHidden = true
        }
        if let Attachment = post["topic_attachment"].array, Attachment.count != 0{
            print("updating slide........")
            print(indexPath.row)
            cell.updateSlider(data: Attachment)
        }
        else{
            print("No Slide........")
            print(indexPath.row)
            cell.viewPostBg.isHidden = true
            cell.viewPageControllBG.isHidden = true
        }
        if cell.labelPost.calculateMaxLines() > maxNumberOfLines{
            cell.labelPost.numberOfLines = maxNumberOfLines
            cell.readMoreView.isHidden = false
            
            //            tableView.layoutIfNeeded()
        }
        else{
            cell.labelPost.numberOfLines = 0
            cell.readMoreView.isHidden = true
            //            tableView.layoutIfNeeded()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = ArrayTopics[indexPath.row]
        
        let storyboard = UIStoryboard.init(name: "Topic", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "TopicsConversationViewController") as! TopicsConversationViewController
        vc.topicID = data["topic_id"].stringValue
        //        vc.Topic = data
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
