//
//  NeedsRequestListTableViewCell.swift
//  familheey
//
//  Created by GiRi on 27/03/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit

protocol NeedsRequestListTableViewCellDelegate:class {
    func viewMoreAction(index:Int)
    func rightMenuAction(index:Int)
}

class NeedsRequestListTableViewCell: UITableViewCell {
    
    @IBOutlet var viewUserDetails: UIView!
    @IBOutlet var viewNeedOne: UIView!
    @IBOutlet var viewNeedTwo: UIView!
    @IBOutlet var viewNeedThree: UIView!
    
    @IBOutlet var ImageViewuserProfile: UIImageView!
    @IBOutlet var lblPostedIn: UILabel!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var lblpostedInFamilyName: UILabel!
    @IBOutlet var lblDate: UILabel!
    
    @IBOutlet var lblNeedOnetitle: UILabel!
    @IBOutlet var lblNeedOneDesc: UILabel!
    @IBOutlet var lblNeedOneQuantity: UILabel!
    @IBOutlet var lblNeedOneStatus: UILabel!
    
    @IBOutlet var lblNeedTwotitle: UILabel!
    @IBOutlet var lblNeedTwoDesc: UILabel!
    @IBOutlet var lblNeedTwoQuantity: UILabel!
    @IBOutlet var lblNeedTwoStatus: UILabel!
    
    
    @IBOutlet var lblNeedThreetitle: UILabel!
    @IBOutlet var lblNeedThreeDesc: UILabel!
    @IBOutlet var lblNeedThreeQuantity: UILabel!
    @IBOutlet var lblNeedThreeStatus: UILabel!
    
    @IBOutlet var lblNeedDate: UILabel!
    @IBOutlet var lblNeedLocation: UILabel!
    
    @IBOutlet var lblNumberofSupporters: UILabel!
    
    @IBOutlet weak var lblHeadingSupporter: UILabel!
    @IBOutlet var needTwoView: UIView!
    @IBOutlet var needThreeView: UIView!
    @IBOutlet var needOneView: UIView!
    
    @IBOutlet weak var viewOfThreeDot: UIView!
    @IBOutlet weak var buttonViewMore: UIButton!
    @IBOutlet weak var buttonMenu: UIButton!
    @IBOutlet weak var butttongotoDetails: UIButton!
    
    weak var delegate: NeedsRequestListTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func viewMoreAction(_ sender: UIButton) {
        delegate?.viewMoreAction(index: sender.tag)
    }
    @IBAction func buttonRightMenuAction(_ sender: UIButton) {
        delegate?.rightMenuAction(index: sender.tag)

    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
