//
//  ReportPostViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 30/01/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON

class ReportPostViewController: UIViewController {
    var isFrom:String = ""
    var postDetails = JSON()
    var requestDetails = JSON()
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    
    @IBOutlet weak var lblNavHeading: UILabel!
    @IBOutlet weak var headingDescription: UILabel!
    @IBOutlet weak var txtDescription: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isFrom == "post"{
            self.lblNavHeading.text = "Report Post"
        }
        else if isFrom == "request_needs"
        {
            self.lblNavHeading.text = "Report Request"
            
        }
        else
        {
            self.lblNavHeading.text = "Report Announcement"
            
        }
        self.headingDescription.attributedText = Helpers.setAttributedPlaceHolder(placeHolderName: "Description")
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnReportOnClick(_ sender: Any) {
        
        if self.txtDescription.text == "" {
            
            Helpers.showAlertDialog(message: "Please enter valid reason", target: self)
        }
        else
        {
            self.apiCallForReportSpam()
        }
    }
    
    func apiCallForReportSpam(){
        let post = self.postDetails
        
        var param = [String:String]()
        if isFrom == "request_needs"
        {
            
            param = ["type_id" : self.requestDetails["post_request_id"].stringValue, "user_id": UserDefaults.standard.value(forKey: "userId") as! String,"spam_page_type":self.isFrom,"description":self.txtDescription.text!]
            
        }
        else
        {
            param = ["type_id" : post["post_id"].stringValue, "user_id": UserDefaults.standard.value(forKey: "userId") as! String,"spam_page_type":self.isFrom,"description":self.txtDescription.text!]
        }
        
        
        print(param)
        
        ActivityIndicatorView.show("Please wait....")
        networkProvider.request(.ReportSpam(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        print("view contents : \(jsonData)")
                        
                        var message:String!
                        if self.isFrom == "post"{
                            message = "Reported successfully.We will review the post and do the needful,thank you"
                        }
                        else if self.isFrom == "request_needs"
                        {
                            
                            message = "Reported successfully.We will review the request and do the needful,thank you"
                        }
                        else
                        {
                            message = "Reported successfully.We will review the announcement and do the needful,thank you"
                        }
                        
                        let AlertCus = CustomAlertVC.show(in: self, title: "Success", message: message, image:UIImage.init(named: "Green_tick")!)
                        AlertCus.addAction(CPAlertAction.init(title: "OK", type: .normal, handler: {
                            self.navigationController?.popViewController(animated: true)
                        }))
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.apiCallForReportSpam()
                        }
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
