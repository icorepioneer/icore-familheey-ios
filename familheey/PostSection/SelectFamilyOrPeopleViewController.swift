//
//  SelectFamilyOrPeopleViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 21/11/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Moya
import Alamofire
import SwiftyJSON

protocol FamilyOrPeopleSelectionDelegate {
    func selctionCallback(SelectedUserId:NSMutableArray,currentIndex:Int)
}

class SelectFamilyOrPeopleViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var imgNoData: UIImageView!
    @IBOutlet weak var viewOfNoData: UIView!
    @IBOutlet weak var tblListView: UITableView!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var lblNavHeading: UILabel!
    @IBOutlet weak var btnSearchReset: UIButton!
    
    var selectedUserIDArr                     = NSMutableArray()
    var selectedIdGroup   = NSMutableDictionary()
    var selectedGroups = [[String:AnyObject]]()
    var selectionDelegate:FamilyOrPeopleSelectionDelegate!
    var searchTxt = ""
    var fromAnnouncement = false
    
    private var networkProvider               = MoyaProvider<FamilyheeyApi>()
    var typeOfSelection                    = String()
    
    var selectedIndex = 0
    
    var isFromEdit = false
    var postRefId = ""
    var postId = ""
    
    //    var FamilyArr : familyListResponse!
    var FamilyArr = JSON()
    var peopleListArr                         = JSON()
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedUserIDArr.removeAllObjects()
        
        txtSearch.delegate = self
        Helpers.setleftView(textfield: txtSearch, customWidth: 40)
        self.tabBarController?.tabBar.isHidden = true
        if isFromEdit{
            if self.selectedIndex == 0{
                lblNavHeading.text = "Select Families"
                self.selectedIndex = 0
            }
        }
        else{
            if self.selectedIndex == 0{
                lblNavHeading.text = "Select Families"
                self.selectedIndex = 0
            }
            else{
                lblNavHeading.text = "Select People"
                self.selectedIndex = 1
                
            }
        }
        
        Helpers.setleftView(textfield: txtSearch, customWidth: 30)
        print(selectedGroups)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let headerNib = UINib.init(nibName: "FamilyListingView", bundle: Bundle.main)
        tblListView.register(headerNib, forHeaderFooterViewReuseIdentifier: "FamilyListingView")
        
        if isFromEdit{
            getFamilyList()
        }
        else{
            if selectedIndex == 0{
                getFamilyList()
                
            }
            else{
                callGroupMembers()
            }
        }
        
    }
    
    
    //MARK:- WEBAPI
    func getFamilyList(){
        ActivityIndicatorView.show("Loading....")
        
        //    let userId = UserDefaults.standard.value(forKey: "userId") as! Int
        var parameter = [String: Any]()
        if fromAnnouncement{
            parameter = ["user_id" : "\(UserDefaults.standard.value(forKey: "userId") as! String)" ,"query":self.txtSearch.text!,"ref_id":self.postRefId,"type":"announcement"]
            
        }
        else{
            parameter = ["user_id" : "\(UserDefaults.standard.value(forKey: "userId") as! String)" ,"query":self.txtSearch.text!,"ref_id":self.postRefId,]
        }

        ActivityIndicatorView.show("Loading...")
        
        networkProvider.request(.getFamilyListForPost(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    let jsonData = JSON(response.data)
                    print(jsonData)
                    if response.statusCode == 200{
                        self.FamilyArr = jsonData["data"]
                        if self.FamilyArr.count > 0{
                            self.tblListView.delegate = self
                            self.tblListView.dataSource = self
                            
                            self.viewOfNoData.isHidden = true
                            self.lblNoData.isHidden = true
                            self.imgNoData.isHidden = true
                            
                            self.tblListView.isHidden = false
                            self.tblListView.reloadData()
                        }
                        else{
                            self.tblListView.isHidden = true
                            self.viewOfNoData.isHidden = false
                            self.lblNoData.isHidden = false
                            self.imgNoData.isHidden = false
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getFamilyList()
                        }
                    }
                    else{
                        self.tblListView.isHidden = true
                        
                        self.viewOfNoData.isHidden = false
                        self.lblNoData.isHidden = false
                        self.imgNoData.isHidden = false
                    }
                } catch let err {
                    
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
    
    //MARK: - Get Group members
    
    func callGroupMembers(){
        
        ActivityIndicatorView.show("Loading....")
        //    let userId = UserDefaults.standard.value(forKey: "userId") as! Int
        
        let parameter = ["user_id" : "\(UserDefaults.standard.value(forKey: "userId") as! String)" ,"query":self.txtSearch.text!]
        
        print("param : \(parameter)")
        ActivityIndicatorView.show("Loading...")
        
        networkProvider.request(.user_group_members(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    let jsonData = JSON(response.data)
                    if response.statusCode == 200
                    {
                        self.peopleListArr = jsonData["data"]
                        
                        if self.peopleListArr.count > 0{
                            self.tblListView.delegate = self
                            self.tblListView.dataSource = self
                            
                            self.viewOfNoData.isHidden = true
                            self.lblNoData.isHidden = true
                            self.imgNoData.isHidden = true
                            
                            self.tblListView.isHidden = false
                            
                            
                            
                            
                            self.tblListView.reloadData()
                        }
                        else{
                            self.tblListView.isHidden = true
                            
                            self.viewOfNoData.isHidden = false
                            self.lblNoData.isHidden = false
                            self.imgNoData.isHidden = false
                            
                        }
                        
                        
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.callGroupMembers()
                        }
                    }
                    else
                    {
                        self.tblListView.isHidden = true
                        
                        self.viewOfNoData.isHidden = false
                        self.lblNoData.isHidden = false
                        self.imgNoData.isHidden = false
                    }
                    
                    
                    
                } catch let err {
                    
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    @IBAction func btnBack_OnClick(_ sender: Any) {
        if isFromEdit{
            self.selectedUserIDArr.addObjects(from: selectedGroups)
            if selectedUserIDArr.count > 0{
                self.selectionDelegate.selctionCallback(SelectedUserId: selectedUserIDArr,currentIndex:selectedIndex)
            }
            else{
                Helpers.showAlertDialog(message:"Please select atleast one family", target: self)
            }
        }
        else{
            self.selectionDelegate.selctionCallback(SelectedUserId: [],currentIndex:3)
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickSearchRset(_ sender: Any) {
        self.txtSearch.text = ""
        
        if self.selectedIndex == 1{
            callGroupMembers()
        }
        else if self.selectedIndex == 0{
            getFamilyList()
        }
        else{
        }
        btnSearchReset.isHidden = true
        self.txtSearch.endEditing(true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    @IBAction func onClickDone(_ sender: Any) {
        
        if selectedIndex == 0{
            self.selectedUserIDArr.addObjects(from: selectedGroups)
            //            print(selectedUserIDArr)
            if selectedUserIDArr.count > 0{
                self.selectionDelegate.selctionCallback(SelectedUserId: selectedUserIDArr,currentIndex:selectedIndex)
                self.navigationController?.popViewController(animated: true)
                //                let params: Parameters = ["event_id" : eventId, "group_id" : selectedUserIDArr as NSArray, "view" : "template1", "from_user" : UserDefaults.standard.value(forKey: "userId") as! String, "type": typeOfInvitations]
                //                callinvite(param: params)
                
            }
            else
            {
                Helpers.showAlertDialog(message:"Please select atleast one family", target: self)
                
            }
        }
        else if selectedIndex == 1{
            
            if selectedUserIDArr.count > 0{
                
                //                let params: Parameters = ["event_id" : eventId, "user_id" : selectedUserIDArr as NSArray, "view" : "template1", "from_user" : UserDefaults.standard.value(forKey: "userId") as! String, "type": typeOfInvitations]
                //                callinvite(param: params)
                self.selectionDelegate.selctionCallback(SelectedUserId: selectedUserIDArr,currentIndex:selectedIndex)
                self.navigationController?.popViewController(animated: true)
                
                
            }
            else
            {
                Helpers.showAlertDialog(message:"Please select atleast one member", target: self)
                
            }
        }
        
    }
}


extension SelectFamilyOrPeopleViewController : UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        if selectedIndex == 0{
            return FamilyArr.count
        }else if selectedIndex == 1{
            return peopleListArr.count
        }
        else{
            return 1
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedIndex == 0{
            return 0
        }else if selectedIndex == 2{
            
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if selectedIndex == 2{
            let cell = tblListView.dequeueReusableCell(withIdentifier: "cell3") as! OtherInviteCell
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        // if selectedIndex == 0{
        let header = self.tblListView.dequeueReusableHeaderFooterView(withIdentifier: "FamilyListingView") as! FamilyListingView
        
        header.btnAction.tag       = section
        header.btnView.tag         = section
        header.btnView.addTarget(self, action: #selector(inviteFamilySelection(sender:)), for: .touchUpInside)
        header.btnAction.addTarget(self, action: #selector(inviteFamilySelection(sender:)), for: .touchUpInside)
        
        
        if selectedIndex == 1{
            
            header.lblTitle.text = self.peopleListArr.arrayValue[section]["full_name"].stringValue.firstUppercased
            header.byTitle.text = ""
            header.lblType.text = ""//FamilyArr.familyList![section].faType
            header.lblType.isHidden = true
            header.lblCreatedBy.isHidden = true
            header.lblKnownHead.isHidden = true
            header.lblMemberHead.isHidden = true
            //            header.lblCreated_height.constant = 0
            header.lblRegion.text = self.peopleListArr.arrayValue[section]["location"].stringValue.firstUppercased
            header.lblCreatedBy.text = self.peopleListArr.arrayValue[section]["created_by"].stringValue
            //FamilyArr.familyList![section].createdByName
            header.lblMembersCount.text = ""//FamilyArr.familyList![section].memberCount
            header.lblKnown.text = ""//FamilyArr.familyList![section].knownCount
            
            if selectedUserIDArr.contains(self.peopleListArr.arrayValue[section]["user_id"].intValue){
                header.btnAction.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
                header.btnAction.setTitle("Selected", for: .normal)
                
            }else{
                
                header.btnAction.titleLabel?.font = UIFont.systemFont(ofSize: 15)
                header.btnAction.setTitle("Select", for: .normal)
                
            }
            
            let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+self.peopleListArr.arrayValue[section]["propic"].stringValue
            let imgurl = URL(string: temp)
            header.imgLogo.kf.indicatorType = .activity
            
            header.imgLogo.kf.setImage(with: imgurl, placeholder: UIImage(named: "imgNoImage") , options: nil, progressBlock: nil, completionHandler: nil)
            
            
        }
        else if selectedIndex == 2{
        }
        else{
            header.lblType.isHidden = false
            header.lblCreatedBy.isHidden = false
            header.lblKnownHead.isHidden = false
            header.lblMemberHead.isHidden = false
            header.lblMembersCount.isHidden = true
            header.lblMemberHead.isHidden = true
            header.lblKnownHead.isHidden = true
            header.lblKnown.isHidden = true
            //            header.lblCreated_height.constant = 18
            header.lblTitle.text = self.FamilyArr.arrayValue[section]["group_name"].stringValue.firstUppercased
            header.lblType.text = self.FamilyArr.arrayValue[section]["group_type"].stringValue
            header.lblRegion.text = self.FamilyArr.arrayValue[section]["base_region"].stringValue
            header.lblCreatedBy.text = self.FamilyArr.arrayValue[section]["created_by"].stringValue ?? ""
            
            
            
            if isFromEdit{
                for item in selectedGroups{
                    print(item)
                    let temp = item["id"]
                    print(temp)
                    let te = String(describing: temp!)
                    if self.FamilyArr.arrayValue[section]["id"].intValue == Int(te){
                        header.btnAction.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
                        header.btnAction.setTitle("Selected", for: .normal)
                        break
                    }
                    else{
                        header.btnAction.titleLabel?.font = UIFont.systemFont(ofSize: 15)
                        header.btnAction.setTitle("Select", for: .normal)
                    }
                }
                
            }
            else{
                if selectedUserIDArr.contains(self.FamilyArr.arrayValue[section]["id"].intValue){
                    header.btnAction.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
                    header.btnAction.setTitle("Selected", for: .normal)
                    
                }else{
                    header.btnAction.titleLabel?.font = UIFont.systemFont(ofSize: 15)
                    header.btnAction.setTitle("Select", for: .normal)
                }
                
                let foundItems = selectedGroups.filter { $0["id"]!.intValue == self.FamilyArr.arrayValue[section]["id"].intValue }
                if foundItems.count > 0{
                    header.btnAction.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
                    header.btnAction.setTitle("Selected", for: .normal)
                }else{
                    header.btnAction.titleLabel?.font = UIFont.systemFont(ofSize: 15)
                    header.btnAction.setTitle("Select", for: .normal)
                }
            }
            
            //            }
            
            if FamilyArr.arrayValue[section]["logo"].stringValue.count != 0{
                let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=150&height=150&url="+"\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+FamilyArr.arrayValue[section]["logo"].stringValue
                let imgurl = URL(string: temp)
                print(imgurl)
                header.imgLogo.kf.indicatorType = .activity
                
                header.imgLogo.kf.setImage(with: imgurl, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
            }
            else{
                header.imgLogo.image = UIImage(named: "Family Logo")
            }
            
            //            header.byTitle.text = self.FamilyArr.arrayValue[section]["group_type"].stringValue
            
            // header.byTitle.text = "By \(FamilyArr.familyList![section].faName)"
            /*header.lblTitle.text = FamilyArr.familyList![section].faName
             header.lblType.text = FamilyArr.familyList![section].faType
             header.lblRegion.text = FamilyArr.familyList![section].faRegion
             header.lblCreatedBy.text = FamilyArr.familyList![section].faAdmin
             
             let memberCount = FamilyArr.familyList![section].memberCount
             let knownCount = FamilyArr.familyList![section].knownCount
             
             if Int(memberCount!)! > 0{
             header.lblMembersCount.text = FamilyArr.familyList![section].memberCount
             header.lblMemberHead.isHidden = false
             header.lblMembersCount.isHidden = false
             }
             else{
             header.lblMemberHead.isHidden = true
             header.lblMembersCount.isHidden = true
             }
             
             if Int(knownCount!)! > 0{
             header.lblKnown.text = FamilyArr.familyList![section].memberCount
             header.lblKnownHead.isHidden = false
             header.lblKnown.isHidden = false
             }
             else{
             header.lblKnownHead.isHidden = true
             header.lblKnown.isHidden = true
             }
             
             header.lblKnown.text = FamilyArr.familyList![section].knownCount
             
             if selectedUserIDArr.contains(FamilyArr.familyList![section].faId){
             header.btnAction.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
             header.btnAction.setTitle("Selected", for: .normal)
             
             //                header.btnAction.backgroundColor          = .white
             //                header.btnAction.layer.borderWidth        = 1
             //                header.btnAction.borderColor              = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
             //                header.btnAction.titleLabel?.textColor    = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
             
             }else{
             
             header.btnAction.titleLabel?.font = UIFont.systemFont(ofSize: 15)
             header.btnAction.setTitle("Select", for: .normal)
             
             //                header.btnAction.backgroundColor          = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
             //                header.btnAction.titleLabel?.textColor    = .white
             }
             
             if FamilyArr.familyList![section].faLogo.count != 0{
             let temp = "\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+FamilyArr.familyList![section].faLogo
             let imgurl = URL(string: temp)
             print(imgurl)
             header.imgLogo.kf.setImage(with: imgurl, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
             }
             else{
             header.imgLogo.image = UIImage(named: "Family Logo")
             }*/
        }
        
        
        return header
        // }
        //        else{
        //
        //        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if selectedIndex == 2{
            
            return 570
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if  selectedIndex == 0{
            return 150
        }
        else if selectedIndex == 1{
            return 110
        }
        else{
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    //MARK: - Invite famy selection
    
    @IBAction func inviteFamilySelection(sender: UIButton){
        
        //        print("tag : \(sender.tag)")
        
        if selectedIndex == 1{
            
            if selectedUserIDArr.contains(self.peopleListArr.arrayValue[sender.tag]["user_id"].intValue){
                
                selectedUserIDArr.remove(self.peopleListArr.arrayValue[sender.tag]["user_id"].intValue)
            }else{
                selectedUserIDArr.add(self.peopleListArr.arrayValue[sender.tag]["user_id"].intValue)
            }
            
        }else{
            //            if self.FamilyArr.arrayValue[sender.tag]["post_create"].intValue == 7{
            //                if self.FamilyArr.arrayValue[sender.tag]["user_type"].stringValue.lowercased() != "admin"{
            //                    self.displayAlert(alertStr: "You have no permission for this", title: "")
            //                    return
            //                }
            //            }
            
            if selectedGroups.count > 0{
                var i = 0
                if isFromEdit{
                    for item in selectedGroups{
                        print(item)
                        let temp = item["id"]
                        print(temp)
                        let te = String(describing: temp!)
                        if self.FamilyArr.arrayValue[sender.tag]["id"].stringValue == te{
                            break
                        }
                        else{
                            i = i + 1
                        }
                    }
                }
                else{
                    for n in 0..<self.selectedGroups.count {
                        let temp = self.selectedGroups[n] as NSDictionary
                        let str = temp.value(forKey: "id") as! NSNumber
                        if str.intValue == self.FamilyArr.arrayValue[sender.tag]["id"].intValue{
                            break
                        }
                        else{
                            i = i + 1
                        }
                    }
                }
                
                if i == selectedGroups.count{
                    
                    selectedIdGroup.setValue(self.FamilyArr.arrayValue[sender.tag]["id"].intValue, forKey: "id")
                    
                    if fromAnnouncement{
                        selectedIdGroup.setValue(self.FamilyArr.arrayValue[sender.tag]["announcement_create"].intValue, forKey: "announcement_create")
                    }
                    else{
                        selectedIdGroup.setValue(self.FamilyArr.arrayValue[sender.tag]["post_create"].intValue, forKey: "post_create")
                    }
                    
                    selectedGroups.append(selectedIdGroup as! [String:AnyObject])
                }
                else{
                    selectedGroups.remove(at: i)
                }
            }
            else{
                selectedIdGroup.setValue(self.FamilyArr.arrayValue[sender.tag]["id"].intValue, forKey: "id")
                
                if fromAnnouncement{
                    selectedIdGroup.setValue(self.FamilyArr.arrayValue[sender.tag]["announcement_create"].intValue, forKey: "announcement_create")
                }
                else{
                    selectedIdGroup.setValue(self.FamilyArr.arrayValue[sender.tag]["post_create"].intValue, forKey: "post_create")
                }
                selectedGroups.append(selectedIdGroup as! [String:AnyObject])
            }
            //            var temp = self.FamilyArr.arrayValue[sender.tag]
            //            if temp["is_shared"].stringValue.lowercased() == "true"{
            //                temp["is_shared"] = "false"
            //            }
            //            else{
            //                temp["is_shared"] = "true"
            //            }
            //            self.FamilyArr.
            
        }
        
        tblListView.reloadSections(IndexSet(integer : sender.tag), with: .none)
        
        //print(FamilyArr.familyList![sender.tag].faId)
    }
    
    //MARK:- textfield dellegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        textField.resignFirstResponder()
        /* if self.selectedIndex == 1{
         if textField == txtSearch
         {
         callGroupMembers()
         }
         }
         else if self.selectedIndex == 0{
         if textField == txtSearch
         {
         getFamilyList()
         }
         
         }
         else{
         }*/
        if textField.text!.count > 0{
            self.btnSearchReset.isHidden = false
        }
        else{
            self.btnSearchReset.isHidden = true
        }
        textField.endEditing(true)
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        self.view.endEditing(true)
        if textField.text!.count > 0{
            self.btnSearchReset.isHidden = false
        }
        else{
            self.btnSearchReset.isHidden = true
        }
        
        
        if self.selectedIndex == 1{
            if textField == txtSearch
            {
                callGroupMembers()
            }
        }
        else if self.selectedIndex == 0{
            if textField == txtSearch
            {
                getFamilyList()
            }
            
        }
        else{
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text!.count > 0{
            self.btnSearchReset.isHidden = false
        }
        else{
            self.btnSearchReset.isHidden = true
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
        }
        return true
    }
    
}
