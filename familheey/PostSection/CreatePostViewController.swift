//
//  CreatePostViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 20/11/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
//import iOSDropDown
import Alamofire
import Moya
import SwiftyJSON
import YPImagePicker
import AVKit
import iOSDropDown
import ImageIO
import MobileCoreServices
import PDFKit
import QuickLookThumbnailing
import AVFoundation
import DKImagePickerController
import Photos

protocol postUpdateDelegate : class{
    func postupdateSuccess(index:Int , SuccessData:JSON)
}
class CreatePostViewController: UIViewController,UITextFieldDelegate,FamilyOrPeopleSelectionDelegate ,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIDocumentPickerDelegate{
    var callBackDelegate:postUpdateDelegate?
    var selectedTag:Int! = 0
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var heightOfDropDown: NSLayoutConstraint!
    @IBOutlet weak var lblNoFiles: UILabel!
    @IBOutlet weak var viewOfShade: UIView!
    @IBOutlet weak var tblViewDropList: UITableView!
    @IBOutlet weak var imgEditFamily: UIImageView!
    @IBOutlet weak var collViewOfAttachments: UICollectionView!
    @IBOutlet weak var selectedImageV: UIImageView!
    @IBOutlet weak var lblPostTypeStatus: UILabel!
    @IBOutlet weak var lblHeading2: UILabel!
    @IBOutlet weak var lblHeading1: UILabel!
    @IBOutlet weak var txtPostType: DropDown!
    @IBOutlet weak var heightOfPostSelection: NSLayoutConstraint!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var btnPostType: UIButton!
    @IBOutlet weak var btnPostedFamilies: UIButton!
    @IBOutlet weak var imgPostedFamilies: UIImageView!
    @IBOutlet weak var swithSharable: UISwitch!
    @IBOutlet weak var swithConversation: UISwitch!
    @IBOutlet weak var btnPost: UIButton!
    @IBOutlet weak var lblNavHeading: UILabel!
    @IBOutlet weak var extractActivity: UIActivityIndicatorView!
    
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    
    //    var selectedItems = [YPMediaItem]()
    var selectedAttachments = NSMutableArray()
    var post_images:[JSON] = []
    var selectedItemsexisted = Array<[String:Any]>()
    var previousSelectedItemsexisted = Array<[String:Any]>()
    
    //    var selectedItems = [YPMediaItem]()
    
    //    let selectedImageV = UIImageView()
    var arrayOfSelectedUser = NSMutableArray()
    var ArrayPosts = [JSON]()
    var imgDataArr = [Data]()
    var videoDataArr = [Data]()
    var documentDataArr = [Data]()
    var selectedIdsOriginal = [String]()
    var deletedArr = [String]()
    var activeInactive = [Int]()
    var selectedUserIDArr    = NSMutableArray()
    var selectedIdGroups  = NSMutableDictionary()
    var selectedGroups = [[String:AnyObject]]()
    var arrayOfPostTypewitharrow = [["text":"Selected families","hidebool":false],["text":"Everyone (Public)","hidebool":true],["text":"Only Me","hidebool":true]]
    //    let dropDown = DropDown()
    //    @IBOutlet weak var dropDownView: UIView!
    
    var imageUploadStatus = false
    var vedioUploadStatus = false
    var documentUploadStatus = false
    var postoptions = ["Images","Videos","Documents"]
    var deleteAttachment:Bool = false
    var fromEdit = false
    var fromGroup = false
    var editPostDetails = JSON()
    var selectedPostType:String = ""
    var privacyType:String = ""
    var isSharing:Bool!
    var isCreatedby = ""
    var postType = "single"
    var sizeArray : [[String:String]] = [[:]]
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var pickerController: DKImagePickerController!
    var isFromForward = false
    var forwardDic = [String:Any]()
    var arrayOfPostType = ["Selected families", "Everyone (Public)", "Only Me"]
    var isConversation:Bool!
    var pathExt = ""
    
    override func viewDidLoad() {
        self.tabBarController?.tabBar.isHidden = true
        self.vedioUploadStatus = true
        
        super.viewDidLoad()
        pickerController = DKImagePickerController()
        
        print("######\(editPostDetails)")
        self.txtDescription.autocapitalizationType      = .sentences
        
        self.collViewOfAttachments.delegate = self
        self.collViewOfAttachments.dataSource = self
        Helpers.setleftView(textfield: txtPostType, customWidth: 15)
        txtPostType.delegate = self
        
        if appDelegate.noFamily{
            
            arrayOfPostType = [
                "Everyone (Public)",
                "Only Me"]
            
            arrayOfPostTypewitharrow = [["text":"Everyone (Public)","hidebool":true],["text":"Only Me","hidebool":true]]
        }
        
        self.heightOfDropDown.constant = CGFloat(40 * arrayOfPostTypewitharrow.count)
        
        txtPostType.optionArray = self.arrayOfPostType.sorted()
        
        txtPostType.didSelect {(selectedText , index ,id) in
            print(selectedText)
            
            
            if selectedText == "All Families"{
                self.heightOfPostSelection.constant = 75
                self.lblPostTypeStatus.isHidden = true
                self.arrayOfSelectedUser = []
                self.selectedPostType = "all_family"
                self.privacyType = "public"
            }
            else if selectedText == "Selected families"{
                self.selectedPostType = "only_groups"
                self.privacyType = "public"
                let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "SelectFamilyOrPeopleViewController") as! SelectFamilyOrPeopleViewController
                vc.selectedIndex = 0
                vc.selectionDelegate = self
                self.navigationController?.pushViewController(vc, animated: true)
            }
                /* else if selectedText == "Selected Connections"{
                 self.selectedPostType = "only_users"
                 self.privacyType = "public"
                 let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
                 let vc = storyboard.instantiateViewController(withIdentifier: "SelectFamilyOrPeopleViewController") as! SelectFamilyOrPeopleViewController
                 vc.selectedIndex = 1
                 vc.selectionDelegate = self
                 
                 self.navigationController?.pushViewController(vc, animated: true)
                 }*/
            else  if selectedText == "Everyone (Public)"{
                self.selectedPostType = "public"
                self.privacyType = "public"
                self.heightOfPostSelection.constant = 75
                self.lblPostTypeStatus.isHidden = true
                self.arrayOfSelectedUser = []
            }
            else{
                self.selectedPostType = "private"
                self.privacyType = "private"
                self.heightOfPostSelection.constant = 75
                self.lblPostTypeStatus.isHidden = true
                self.arrayOfSelectedUser = []
            }
        }
        
        if fromEdit{
            self.heightOfPostSelection.constant = 0
            self.txtDescription.text = editPostDetails["snap_description"].stringValue
            if editPostDetails["is_shareable"].boolValue{
                self.swithSharable.isOn = true
                self.isSharing = true
            }
            else{
                self.swithSharable.isOn = false
                self.isSharing = false
            }
            
            if editPostDetails["conversation_enabled"].boolValue{
                self.swithConversation.isOn = true
                self.isConversation = true
            }
            else{
                self.swithConversation.isOn = false
                self.isConversation = false
            }
            
            self.txtPostType.isHidden = true
            self.lblHeading1.isHidden = true
            self.btnPostType.isHidden = true
            tblViewDropList.isHidden = true
            self.viewOfShade.isHidden = true
            
            self.lblNavHeading.text = "Edit Post"
            self.btnPost.setTitle("Update", for: .normal)
            
            self.post_images = editPostDetails["post_attachment"].arrayValue
            
            
            if post_images.count != 0{
                for itemsRespone in post_images{
                    print(itemsRespone)
                    var dict = [String:Any]()
                    dict["existing"] = true
                    var dict2 = [String:String]()
                    dict2["filename"] = itemsRespone["filename"].stringValue
                    dict2["type"] = itemsRespone["type"].stringValue
                    if  itemsRespone["video_thumb"].stringValue  != ""
                    {
                        dict2["video_thumb"] = itemsRespone["video_thumb"].stringValue
                    }
                    if  itemsRespone["width"].stringValue  != ""
                    {
                        dict2["width"] = itemsRespone["width"].stringValue
                    }
                    if  itemsRespone["height"].stringValue  != ""
                    {
                        dict2["height"] = itemsRespone["height"].stringValue
                    }
                    print(dict2)
                    dict["value"] = dict2
                    self.selectedAttachments.add(dict2)
                    self.selectedItemsexisted.append(dict)
                }
                
                print(selectedAttachments)
                print(selectedItemsexisted)
                
                self.collViewOfAttachments.isHidden = false
                self.lblNoFiles.isHidden = true
                self.collViewOfAttachments.reloadData()
            }
            else{
                self.selectedAttachments = []
                self.selectedItemsexisted = []
                self.collViewOfAttachments.isHidden = true
                self.lblNoFiles.isHidden = false
            }
            self.getPostDetails(PostId: editPostDetails["post_id"].stringValue)
        }
        else{
            self.lblNavHeading.text = "Create Post"
            self.btnPost.setTitle("Post", for: .normal)
            self.txtPostType.isHidden = false
            self.lblHeading1.isHidden = false
            self.btnPostType.isHidden = false
            
            self.arrayOfSelectedUser = []
            
            self.tblViewDropList.dataSource = self
            self.tblViewDropList.delegate = self
            tblViewDropList.register(UINib(nibName: "postTypeDropdownCell", bundle: nil), forCellReuseIdentifier: "cell")
            tblViewDropList.isHidden = true
            self.viewOfShade.isHidden = true
            tblViewDropList.tableFooterView = UIView.init()
            
            tblViewDropList.reloadData()
            
            self.heightOfPostSelection.constant = 75
            self.lblPostTypeStatus.isHidden = true
            
            self.isConversation = true
            self.isSharing = true
            self.collViewOfAttachments.isHidden = true
            self.lblNoFiles.isHidden = false
            
            self.selectedAttachments = []
            self.selectedItemsexisted = []
            
            print(self.selectedUserIDArr)
            if fromGroup {
                self.selctionCallback(SelectedUserId: self.selectedUserIDArr,currentIndex:0)
                self.selectedPostType = "only_groups"
                self.privacyType = "public"
                self.txtPostType.text = "Selected families"
                self.txtPostType.isUserInteractionEnabled = false
            }
        }
        self.setTextFiledPlaceholder()
        //MARK: from share to app
        if appDel.isFromShare{
            appDel.isFromShare = false
            var Desc = ""
            let defaults = UserDefaults(suiteName: "group.com.familheey.app")
            defaults?.synchronize()
            
            
            if let url = defaults?.string(forKey: "urlString"){
                Desc = url
            }
            else if let str = defaults?.string(forKey: "stringDesc"){
                Desc = str
            }
            
            if !Desc.isEmpty{
                self.txtDescription.text = Desc
            }
            else{
                checkPhotoLibraryPermission()
            }
        }
        //MARK: from chat forward
        if isFromForward{
            
            var Desc = ""
            self.selectedItemsexisted.removeAll()
            
            if let desc = forwardDic["stringDesc"]{
                Desc = desc as! String
                if !Desc.isEmpty{
                    self.txtDescription.text = Desc
                }
            }
            if let imageData = forwardDic["image"] as? Data{
                if let imgurl = forwardDic["imageURL"]{
                    
                    changeFolderinS3(itemName: imgurl as! String)
                    //
                    //                    let dic = [String:Any]()
                    //                    dic["existing"] = true
                    //                    dic["value"]["type"] = "image"
                    //                    dic["value"]["filename"] = imgurl
                    //                    self.selectedItemsexisted.append(dic)
                    //                    self.collViewOfAttachments.isHidden = false
                    //                    self.lblNoFiles.isHidden = true
                    //                    self.collViewOfAttachments.reloadData()
                    let image = UIImage.init(data: imageData)
                    //                             if image != nil{
                    //                                 self.sizeArray = [["W":"\(String(describing: image!.size.width))","H":"\(String(describing:image!.size.height))"]]
                    //                             }
                    //
                    var dict = [String:Any]()
                    dict["existing"] = true
                    var dict2 = [String:String]()
                    dict2["filename"] = imgurl as? String
                    dict2["type"] = forwardDic["type"] as? String
                    
                    if image != nil{
                        dict2["width"] = "\(String(describing: image!.size.width))"
                        dict2["height"] = "\(String(describing:image!.size.height))"
                    }
                    dict["value"] = dict2
                    self.selectedAttachments.add(dict2)
                    self.selectedItemsexisted.append(dict)
                    self.collViewOfAttachments.isHidden = false
                    self.lblNoFiles.isHidden = true
                    self.collViewOfAttachments.reloadData()
                    
                }
            }
            else if let videourl = forwardDic["videoURL"]{
                if let videoThumbimage = forwardDic["videoThumb"] as? Data{
                    
                }
                var dict = [String:Any]()
                dict["existing"] = true
                var dict2 = [String:String]()
                dict2["type"] = forwardDic["type"] as? String
                dict2["filename"] = videourl as? String
                //                           dict2["video_thumb"] = videourl
                dict["value"] = dict2
                self.selectedAttachments.add(dict2)
                self.selectedItemsexisted.append(dict)
                self.collViewOfAttachments.isHidden = false
                self.lblNoFiles.isHidden = true
                self.collViewOfAttachments.reloadData()
                
                
                changeFolderinS3(itemName: videourl as! String)
                
            }
            else if let docURL = forwardDic["docURL"]{
                var dict = [String:Any]()
                dict["existing"] = true
                var dict2 = [String:String]()
                dict2["type"] = forwardDic["type"] as? String
                dict2["filename"] = docURL as? String
                dict["value"] = dict2
                self.selectedAttachments.add(dict2)
                self.selectedItemsexisted.append(dict)
                self.collViewOfAttachments.isHidden = false
                self.lblNoFiles.isHidden = true
                self.collViewOfAttachments.reloadData()
                
                changeFolderinS3(itemName: docURL as! String)
            }
            
            
        }
        
    }
    override func viewDidLayoutSubviews() {
        
        self.scrollView.translatesAutoresizingMaskIntoConstraints = true
        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.width, height: 750);
    }
    //MARK:- Change folder in s3
    
    func changeFolderinS3(itemName:String){
        let conversationSource = "\(BaseUrl.conversation)\(itemName)"
        let postSource = "\(BaseUrl.post_image)\(itemName)"
        
        let param = ["source_file":conversationSource,"destination_file":postSource]
        
        ActivityIndicatorView.show("Please wait....")
        networkProvider.request(.copy_file_between_bucket(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    ActivityIndicatorView.hiding()
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.changeFolderinS3(itemName: itemName)
                        }
                    }
                    else{
                        self.selectedAttachments.removeAllObjects()
                        self.selectedItemsexisted.removeAll()
                        self.collViewOfAttachments.reloadData()
                        self.collViewOfAttachments.isHidden = true
                        self.lblNoFiles.isHidden = false
                        self.displayAlert(alertStr: "Unable to forward the file, please try again", title: "")
                        self.isFromForward = false
                    }
                }catch let err {
                    ActivityIndicatorView.hiding()
                    self.selectedAttachments.removeAllObjects()
                    self.selectedItemsexisted.removeAll()
                    self.collViewOfAttachments.reloadData()
                    self.collViewOfAttachments.isHidden = true
                    self.lblNoFiles.isHidden = false
                    self.displayAlert(alertStr: "Unable to forward the file, please try again", title: "")
                    self.isFromForward = false
                    //  Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                self.selectedAttachments.removeAllObjects()
                self.selectedItemsexisted.removeAll()
                self.collViewOfAttachments.reloadData()
                self.collViewOfAttachments.isHidden = true
                self.lblNoFiles.isHidden = false
                self.displayAlert(alertStr: "Unable to forward the file, please try again", title: "")
                self.isFromForward = false
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
        
    }
    
    
    //MARK:- permission check
    func checkPhotoLibraryPermission() {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            self.processShareRequest()
            break
        case .denied, .restricted :
            self.displayAlert(alertStr: "We dont have access to photos!, please turn on the access and try again", title: "")
            break
        case .notDetermined:
            // ask for permissions
            PHPhotoLibrary.requestAuthorization { status in
                switch status {
                case .authorized:
                    self.processShareRequest()
                    break
                case .denied, .restricted:
                    self.displayAlert(alertStr: "We dont have access to photos!, please turn on the access and try again", title: "")
                    break
                case .notDetermined:
                    // won't happen but still
                    break
                @unknown default:
                    break
                }
            }
        @unknown default:
            break
        }
    }
    //MARK:- Share to app
    func processShareRequest(){
        var imageData : Data?
        var videoData : Data?
        
        let defaults = UserDefaults(suiteName: "group.com.familheey.app")
        defaults?.synchronize()
        
        if let image = defaults?.data(forKey: "imageData") /*self.loadImageFromDiskWith(fileName: "imageData")*/{
            imageData = image
        }
        if let video =  defaults?.data(forKey: "videoData") {
            videoData = video
        }
        
        if videoData != nil{
            var dict = [String:Any]()
            dict["existing"] = false
            dict["value"] = videoData
            dict["type"] = "video"
            self.selectedItemsexisted.append(dict)
            self.collViewOfAttachments.isHidden = false
            self.lblNoFiles.isHidden = true
            self.collViewOfAttachments.reloadData()
            self.videoDataArr.append(videoData!)
            self.uploadVideoMultiple(arr: self.videoDataArr)
        }
            
        else if imageData != nil{
            var dict = [String:Any]()
            dict["existing"] = false
            dict["value"] = imageData
            dict["type"] = "image"
            //            do{
            //            if let fileData = FileManager.default.contents(atPath: imageData!.path){
            let image = UIImage(data: imageData!)
            self.sizeArray = [["W":"\(String(describing: image!.size.width))","H":"\(String(describing:image!.size.height))"]]
            self.imgDataArr.append(imageData!)
            self.uploadImageMultipleImages(arr:self.imgDataArr , sizeArray: self.sizeArray)
            //                }
            //            }
            //            catch{
            //                print("Unable to load data: \(error)")
            //
            //            }
            self.selectedItemsexisted.append(dict)
            self.collViewOfAttachments.isHidden = false
            self.lblNoFiles.isHidden = true
            self.collViewOfAttachments.reloadData()
        }
        
    }
    
    //MARK:- clear share data
    
    func clearShareData(){
        let defaults = UserDefaults(suiteName: "group.com.familheey.app")
        defaults?.set(nil, forKey: "imageData")
        defaults?.set(nil, forKey: "videoData")
        defaults?.setValue(nil, forKey: "urlString")
        defaults?.setValue(nil, forKey: "stringDesc")
    }
    
    
    //    func loadImageFromDiskWith(fileName: String) -> URL? {
    //
    //        guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil }
    //
    //        let fileURL = documentsDirectory.appendingPathComponent(fileName)
    //        if FileManager.default.fileExists(atPath: fileURL.path) {
    //            let url = URL(fileURLWithPath: fileURL.path).appendingPathComponent(fileName)
    //            return url
    //        }
    ////      let documentDirectory = FileManager.SearchPathDirectory.documentDirectory
    //
    ////        let userDomainMask = FileManager.SearchPathDomainMask.userDomainMask
    ////        let paths = NSSearchPathForDirectoriesInDomains(documentDirectory, userDomainMask, true)
    //
    //
    //
    //        return nil
    //    }
    //    func retrieveImage(forKey key: String,
    //                                inStorageType storageType: StorageType) -> UIImage? {
    //
    //            if let filePath = self.filePath(forKey: key),
    //                let fileData = FileManager.default.contents(atPath: filePath.path),
    //                let image = UIImage(data: fileData) {
    //                return image
    //            }
    //
    //        return nil
    //    }
    //     func filePath(forKey key: String) -> URL? {
    //        let fileManager = FileManager.default
    //        guard let documentURL = fileManager.urls(for: .documentDirectory,
    //                                                in: FileManager.SearchPathDomainMask.userDomainMask).first else { return nil }
    //
    //        return documentURL.appendingPathComponent(key)
    //    }
    
    //MARK:- Custom methods
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.tblViewDropList.isHidden = true
        self.viewOfShade.isHidden = true
        
    }
    
    @IBAction func hideButton(_ sender: Any) {
        self.tblViewDropList.isHidden = true
        self.viewOfShade.isHidden = true
    }
    
    func getPostDetails(PostId:String){
        let param = ["type":"post","id":PostId,"user_id":UserDefaults.standard.value(forKey: "userId") as! String]
        
        ActivityIndicatorView.show("Please wait....")
        networkProvider.request(.post_by_id(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    ActivityIndicatorView.hiding()
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        if let response = jsonData["data"].array{
                            self.ArrayPosts.append(contentsOf: response)
                            if self.ArrayPosts[0]["selected_family_count"].intValue > 0{
                                if self.ArrayPosts[0]["selected_family_count"].intValue == 1{
                                    self.btnPostedFamilies.setTitle("\(self.ArrayPosts[0]["selected_family_count"].intValue) family is selected", for: .normal)
                                }
                                else{
                                    self.btnPostedFamilies.setTitle("\(self.ArrayPosts[0]["selected_family_count"].intValue) families are selected", for: .normal)
                                }
                                self.btnPostedFamilies.setTitleColor(.black, for: .normal)
                                self.lblHeading1.text = "Selected Families"
                                self.lblHeading1.isHidden = false
                                self.imgEditFamily.isHidden = false
                                self.btnPostedFamilies.isHidden = false // Change after API update to false
                                self.heightOfPostSelection.constant = 75 // Change after API update to 75
                                self.postType = "multiple" // Change after API update to multiple
                            }
                            
                            self.privacyType = self.ArrayPosts[0]["privacy_type"].stringValue
                            self.isCreatedby = self.ArrayPosts[0]["created_by"].stringValue
                            self.selectedIdsOriginal.removeAll()
                            if let response = self.ArrayPosts[0]["familyList"].array{
                                for i in 0 ..< response.count{
                                    //                                            self.selectedIdsOriginal.append(response[i].stringValue)
                                    print(response[i].dictionaryValue)
                                    let dic = response[i].dictionaryValue
                                    let temp = dic["id"]?.stringValue
                                    self.selectedIdsOriginal.append(temp!)
                                    self.selectedIdGroups.setValue( dic["id"]?.intValue, forKey: "id")
                                    self.selectedIdGroups.setValue( dic["post_create"]?.intValue, forKey: "post_create")
                                    
                                    self.selectedGroups.append(self.selectedIdGroups as! [String:AnyObject])
                                }
                                print(self.selectedGroups)
                                print(self.selectedIdsOriginal)
                            }
                            if let active = self.ArrayPosts[0]["inactive_active_array"].array{
                                for i in 0 ..< active.count{
                                    let temp = active[i].intValue
                                    self.activeInactive.append(temp)
                                }
                            }
                        }
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getPostDetails(PostId: PostId)
                        }
                    }
                    
                    //                        self.navigationController?.popViewController(animated: false)
                }catch let err {
                    ActivityIndicatorView.hiding()
                    //  Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    @IBAction func postTypeBtnPressed(_ sender: Any) {
        self.viewOfShade.isHidden = false
        tblViewDropList.isHidden = false
    }
    func setTextFiledPlaceholder(){
        self.lblHeading1.attributedText = Helpers.setAttributedPlaceHolder(placeHolderName: "Post this to")
        
        //        self.lblHeading2.attributedText = Helpers.setAttributedPlaceHolder(placeHolderName: "What do you want to post?")
    }
    
    //MARK:- DK Image
    
    func selectImage(index:Int){
        pickerController = DKImagePickerController()
        DKImageExtensionController.unregisterExtension(for:.camera)
        
        if index == 0{
            pickerController.assetType = .allPhotos
            pickerController.maxSelectableCount = 10
        }
        else{
            pickerController.assetType = .allVideos
            pickerController.maxSelectableCount = 1
            DKImageExtensionController.registerExtension(extensionClass: CustomCameraExtension.self, for: .camera)
        }
        
        pickerController.exportsWhenCompleted = true
        
        pickerController.deselectAll()
        
        pickerController.didSelectAssets = { [unowned self] (assets: [DKAsset]) in
            
            self.imgDataArr = []
            self.videoDataArr = []
            self.sizeArray = []
            
            self.previousSelectedItemsexisted = self.selectedItemsexisted
            for item in assets{
                var dict = [String:Any]()
                dict["existing"] = false
                dict["value"] = item
                self.selectedItemsexisted.append(dict)
                
            }
            if assets.count != 0{
                //            ActivityIndicatorView.show("Loading....")
                self.extractActivity.startAnimating()
                DKImageAssetExporter.sharedInstance.exportAssetsAsynchronously(assets: assets) { (info) in
                    //                    ActivityIndicatorView.hiding()
                    self.extractActivity.stopAnimating()
                    for asset in assets {
                        switch asset.type {
                        case .photo:
                            if let localTemporaryPath = asset.localTemporaryPath,
                                let imageData = try? Data(contentsOf: localTemporaryPath) {
                                
                                let img = UIImage.init(data: imageData)
                                
                                self.sizeArray.append(["W":"\(String(describing: img!.size.width))","H":"\(String(describing:img!.size.height))"])
                                
                                let tempImg = CreatePostViewController.self.resize(img!)
                                if let fixedData = tempImg.fixedOrientation.jpegData(compressionQuality: 1){
                                    self.imgDataArr.append(fixedData)
                                }
                                
                            }
                            
                            if self.imgDataArr.count == assets.count{
                                self.imageUploadStatus = false
                                self.uploadImageMultipleImages(arr: self.imgDataArr, sizeArray: self.sizeArray)
                                
                                if self.selectedItemsexisted.count != 0{
                                    self.collViewOfAttachments.isHidden = false
                                    self.lblNoFiles.isHidden = true
                                    self.collViewOfAttachments.reloadData()
                                    
                                }
                                else{
                                    self.collViewOfAttachments.isHidden = true
                                    self.lblNoFiles.isHidden = false
                                    
                                }
                            }
                            
                        case .video:
                            if let localTemporaryPath = asset.localTemporaryPath{
                                
                                self.vedioUploadStatus = false
                                
                                asset.fetchFullScreenImage { (image, info) in
                                    AWSS3Manager.shared.uploadImage(image: image!, file_name: "video_thumb/", progress: { (progress) in
                                        
                                    }) { [weak self] (uploadedThumbUrl, error) in
                                        
                                        if let finalPathThumb = uploadedThumbUrl as? URL {
                                            
                                            print("Uploaded file url: " , finalPathThumb)
                                            AWSS3Manager.shared.uploadVideo(videoUrl: localTemporaryPath, folder: "post/", progress: { (progress) in
                                                print(progress)
                                                
                                            }) { [weak self] (uploadedFileUrl, error) in
                                                
                                                if let finalPath = uploadedFileUrl as? URL {
                                                    print("Uploaded file url: " , finalPath)
                                                    self?.vedioUploadStatus = true
                                                    self?.collViewOfAttachments.reloadData()
                                                    
                                                    var dict = [String:String]()
                                                    dict["filename"] = finalPath.lastPathComponent
                                                    dict["type"] = "video"
                                                    dict["video_thumb"] = "video_thumb/\(finalPathThumb.lastPathComponent)"
                                                    print(dict)
                                                    
                                                    self?.selectedAttachments.add(dict)
                                                    
                                                } else {
                                                    print("\(String(describing: error?.localizedDescription))")
                                                    self?.displayAlert(alertStr: "Unable to upload file, please try again", title: "")
                                                    
                                                    self?.deleteAttachment = true
                                                    self?.vedioUploadStatus = true
                                                    self?.selectedItemsexisted =  self!.previousSelectedItemsexisted
                                                    self?.deleteAttachment = false
                                                    
                                                    if self?.selectedItemsexisted.count != 0{
                                                        self?.collViewOfAttachments.isHidden = false
                                                        self?.lblNoFiles.isHidden = true
                                                        self?.collViewOfAttachments.reloadData()
                                                        
                                                    }
                                                    else{
                                                        self?.collViewOfAttachments.isHidden = true
                                                        self?.lblNoFiles.isHidden = false
                                                        
                                                    }
                                                }
                                            }
                                            
                                        } else {
                                            print("\(String(describing: error?.localizedDescription))")
                                            self?.displayAlert(alertStr: "Unable to upload file, please try again", title: "")
                                            self?.deleteAttachment = true
                                            self?.vedioUploadStatus = true
                                            self?.selectedItemsexisted =  self!.previousSelectedItemsexisted
                                            self?.deleteAttachment = false
                                            
                                            if self?.selectedItemsexisted.count != 0{
                                                self?.collViewOfAttachments.isHidden = false
                                                self?.lblNoFiles.isHidden = true
                                                self?.collViewOfAttachments.reloadData()
                                                
                                            }
                                            else{
                                                self?.collViewOfAttachments.isHidden = true
                                                self?.lblNoFiles.isHidden = false
                                                
                                            }
                                            
                                        }
                                    }
                                }
                                
                                if self.selectedItemsexisted.count != 0{
                                    self.collViewOfAttachments.isHidden = false
                                    self.lblNoFiles.isHidden = true
                                    self.collViewOfAttachments.reloadData()
                                    
                                }
                                else{
                                    self.collViewOfAttachments.isHidden = true
                                    self.lblNoFiles.isHidden = false
                                    
                                }
                            }
                        }
                    }
                }
            }
            
        }
        
        if pickerController.UIDelegate == nil {
            pickerController.UIDelegate = AssetClickHandler()
        }
        
        self.present(pickerController, animated: true) {}
        
    }
    
    func getDatafrom(asset: PHAsset?, count:Int){
        if asset == nil{
            return
        }
        let manager = PHImageManager.default()
        if asset?.mediaType == .image{
            manager.requestImageData(for: asset!, options: nil) { (data, string, orientation, info) in
                guard let imageData = data else {
                    return
                }
                let img = UIImage.init(data: imageData)
                self.sizeArray.append(["W":"\(String(describing: img!.size.width))","H":"\(String(describing:img!.size.height))"])
                
                self.imgDataArr.append(imageData)
                if self.imgDataArr.count == count{
                    self.imageUploadStatus = false
                    self.uploadImageMultipleImages(arr: self.imgDataArr, sizeArray: self.sizeArray)
                }
            }
        }else if asset?.mediaType == .video{
            manager.requestAVAsset(forVideo: asset!, options: nil, resultHandler: { (avasset, audio, info) in
                if let avassetURL = avasset as? AVURLAsset {
                    guard let video = try? Data(contentsOf: avassetURL.url) else {
                        return
                    }
                    self.videoDataArr.append(video)
                    if self.videoDataArr.count == count{
                        self.vedioUploadStatus = false
                        self.uploadVideoMultiple(arr: self.videoDataArr)
                    }
                }
            })
        }
        
    }
    func generateThumbnail(asset: AVAsset) -> UIImage? {
        do {
            let imageGenerator = AVAssetImageGenerator.init(asset: asset)
            imageGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imageGenerator.copyCGImage(at: .zero,
                                                         actualTime: nil)
            return UIImage(cgImage: cgImage)
        } catch {
            return #imageLiteral(resourceName: "imgNoImage_landscape")
        }
    }
    
    //MARK:- resize
    static func resize(_ image: UIImage) -> UIImage {
        var actualHeight = Float(image.size.height)
        var actualWidth = Float(image.size.width)
        let maxHeight: Float = actualHeight
        let maxWidth: Float = 1600.0
        var imgRatio: Float = actualWidth / actualHeight
        let maxRatio: Float = maxWidth / maxHeight
        let compressionQuality: Float = 0.90
        //50 percent compression
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        let rect = CGRect(x: 0.0, y: 0.0, width: CGFloat(actualWidth), height: CGFloat(actualHeight))
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = img?.jpegData(compressionQuality: CGFloat(compressionQuality))
        UIGraphicsEndImageContext()
        return UIImage(data: imageData!) ?? UIImage()
    }
    
    //MARK:- button actions
    
    @IBAction func onClickEditSelectedFamilies(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SelectFamilyOrPeopleViewController") as! SelectFamilyOrPeopleViewController
        vc.selectedIndex = 0
        vc.selectionDelegate = self
        vc.postRefId = self.ArrayPosts[0]["post_ref_id"].stringValue
        vc.postId = self.ArrayPosts[0]["post_id"].stringValue
        vc.selectedGroups = self.selectedGroups
        vc.isFromEdit = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnBack_Onclick(_ sender: Any) {
        clearShareData()
        //        NotificationCenter.default.removeObserver(self, name: UIApplication.didEnterBackgroundNotification, object: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnAttachFile_OnClick(_ sender: Any) {
        print(self.imgDataArr.count,self.videoDataArr.count)
        if self.imgDataArr.count != 0  && !imageUploadStatus
        {
            
            Helpers.showAlertDialog(message: "Uploading images Please Wait a moment.", target: self)
        }
        //jeena 23-02-2021 to solve crash issue when close uploading video commented below else if line
        else if !vedioUploadStatus{//self.videoDataArr.count != 0 &&
            
            Helpers.showAlertDialog(message: "Uploading video Please Wait a moment.", target: self)
            
            
        }
        else if self.documentDataArr.count != 0 && !documentUploadStatus{
            
            Helpers.showAlertDialog(message: "Uploading document Please Wait a moment.", target: self)
        }
        else{
            showActionSheet(titleArr: postoptions as NSArray, title: "Select options") { (index) in
                if index == 0{
                    //                    self.checkAuthorisationPermission()
                    self.selectImage(index: index)
                }
                else if index == 1{
                    //                    self.checkAuthorisationPermission()
                    self.selectImage(index: index)
                }
                else if index == 100{
                }
                else
                {
                    
                    
                    //                    let types = [kUTTypePDF, kUTTypeText, kUTTypeRTF, kUTTypeSpreadsheet, "com.microsoft.word.doc" as CFString, "org.openxmlformats.wordprocessingml.document" as CFString]
                    let types = [kUTTypePDF, kUTTypeText, kUTTypeRTF, kUTTypeSpreadsheet, "com.microsoft.word.doc" as CFString, "org.openxmlformats.wordprocessingml.document" as CFString]
                    
                    let documentPicker = UIDocumentPickerViewController(documentTypes: types as [String], in: .import)
                    
                    documentPicker.allowsMultipleSelection = true
                    //                    let documentPicker = UIDocumentPickerViewController(documentTypes: [String(kUTTypeText),String(kUTTypeContent),String(kUTTypeItem),String(kUTTypeData)], in: .import)
                    
                    //Call Delegate
                    
                    
                    documentPicker.delegate = self
                    
                    self.present(documentPicker, animated: true)
                    
                    
                }
            }
        }
    }
    /* func checkAuthorisationPermission() {
     //        let mediaType = AVMediaType.video
     //        let authStatus = AVCaptureDevice.authorizationStatus(for: mediaType)
     //        if authStatus == .authorized {
     //            // do your logic
     //        } else if authStatus == .denied {
     //            // denied
     //        } else if authStatus == .restricted {
     //            // restricted, normally won't happen
     //        } else if authStatus == .notDetermined {
     //            // not determined?!
     //            AVCaptureDevice.requestAccess(for: mediaType, completionHandler: { granted in
     //                if granted {
     //                    print("Granted access to \(mediaType)")
     //                } else {
     //                    print("Not granted access to \(mediaType)")
     //                }
     //            })
     //        } else {
     //            // impossible, unknown authorization status
     //        }
     if AVCaptureDevice.authorizationStatus(for: .video) == .denied {
     NotificationCenter.default.addObserver(
     self,
     selector: #selector(self.applicationEnterBAckground),
     name: UIApplication.didEnterBackgroundNotification,
     object: nil)
     }
     }
     @objc func applicationEnterBAckground(_ notification: Notification) {
     
     UserDefaults.standard.setValue("CreatePostViewController", forKey: "PermissionsReloadClass")
     NotificationCenter.default.removeObserver(self, name: UIApplication.didEnterBackgroundNotification, object: nil)
     } */
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        
        print(urls)
        
        self.documentDataArr = []
        self.previousSelectedItemsexisted = self.selectedItemsexisted
        
        for currentUrl in urls
        {
            //        let videoUrl = video.url
            print(currentUrl.pathExtension)
            pathExt = currentUrl.pathExtension
            do {
                let documentData = try NSData(contentsOf: currentUrl, options: NSData.ReadingOptions())
                self.documentDataArr.append(documentData as Data)
                
                var dict = [String:Any]()
                dict["existing"] = false
                dict["document"] = currentUrl
                self.selectedItemsexisted.append(dict)
                
            } catch {
                print(error)
            }
        }
        print(self.selectedItemsexisted)
        if self.documentDataArr.count != 0
        {
            self.documentUploadStatus = false
            var pickedFileName = ""
            if let filename = urls.first?.lastPathComponent {
                pickedFileName.append(filename)
            }
            self.uploadDocumentMultiple(arr: self.documentDataArr,fileName:pickedFileName)
            
        }
        
        
        if self.selectedItemsexisted.count != 0
        {
            self.collViewOfAttachments.isHidden = false
            self.lblNoFiles.isHidden = true
            self.collViewOfAttachments.reloadData()
            
        }
        else
        {
            self.collViewOfAttachments.isHidden = true
            self.lblNoFiles.isHidden = false
            
        }
        
    }
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
    
    //    func documentPicker(controller: UIDocumentPickerViewController, didPickDocumentAtURL url: NSURL) {
    //        if controller.documentPickerMode == UIDocumentPickerMode.import {
    //            // This is what it should be
    ////            self.newNoteBody.text = String(contentsOfFile: url.path!)
    //
    //
    //
    //            do{
    // let response:String = try String(contentsOfFile: url.path!)
    //                print(response)
    //            } catch let error {
    //                print("Error: \(error)")
    //            }
    //
    //        }
    //    }
    
    //MARK: - Multiple Image Uploading
    
    //    func uploadImageMultipleImages(arr : [Data]){
    //
    //        let param = ["name" :"post"]
    //
    //
    ////        ActivityIndicatorView.show("Uploading...")
    //        Helpers.requestWithMultipleImages(fileParamName: "file", urlAppendString: "upload_file_tos3", imageData: arr, parameters: param, mimeType: "image/png") { (response) in
    //            print("Response : \(response)")
    //            self.imageUploadStatus = true
    //            self.collViewOfAttachments.reloadData()
    //            let arrayOfData = response["data"].arrayValue
    //
    //            for currentdata in arrayOfData{
    //                var dict = [String:String]()
    //                dict["filename"] = currentdata["filename"].stringValue
    //                dict["type"] = currentdata["type"].stringValue
    //                print(dict)
    //
    //                self.selectedAttachments.add(dict)
    //
    //            }
    //
    //
    //
    //            print(self.selectedAttachments)
    ////            ActivityIndicatorView.hiding()
    //
    //
    //
    //        }
    //    }
    func uploadImageMultipleImages(arr : [Data],sizeArray:[[String:String]]){
        
        let param = ["name" :"post"] as [String : Any]
        
        
        //        ActivityIndicatorView.show("Uploading...")
        Helpers.requestWithMultipleImages(fileParamName: "file", urlAppendString: "upload_file_tos3", imageData: arr, parameters: param, mimeType: "image/png") { (response) in
            print("Response : \(response)")
            print(response["data"].arrayValue)
            if response["data"].arrayValue.count != 0
            {
                self.imageUploadStatus = true
                
                self.collViewOfAttachments.reloadData()
                //success condition
                let arrayOfData = response["data"].arrayValue
                
                for currentdata in arrayOfData{
                    var dict = [String:String]()
                    dict["filename"] = currentdata["filename"].stringValue
                    dict["type"] = currentdata["type"].stringValue
                    dict["width"] = sizeArray[0]["W"]
                    dict["height"] = sizeArray[0]["H"]
                    print(dict)
                    
                    self.selectedAttachments.add(dict)
                    
                    
                    
                }
                
                print(self.selectedAttachments)
                //            ActivityIndicatorView.hiding()
            }
            else
            {
                self.displayAlert(alertStr: "Unable to upload file, please try again", title: "")
                
                //failed Condition
                self.imageUploadStatus = true
                self.imgDataArr = []
                
                //                if self.deleteAttachment
                //                {
                self.selectedItemsexisted =  self.previousSelectedItemsexisted
                
                //                self.imageUploadStatus = true
                self.deleteAttachment = false
                
                print(self.selectedItemsexisted)
                print(self.selectedItemsexisted.count)
                
                
                
                if self.selectedItemsexisted.count != 0{
                    self.collViewOfAttachments.isHidden = false
                    self.lblNoFiles.isHidden = true
                    self.collViewOfAttachments.reloadData()
                    
                }
                else{
                    self.collViewOfAttachments.isHidden = true
                    self.lblNoFiles.isHidden = false
                    
                }
                
            }
            //            }
        }
    }
    
    
    
    
    func uploadVideoMultiple(arr : [Data]){
        
        let param = ["name" :"post"]
        
        
        //        ActivityIndicatorView.show("Uploading...")
        Helpers.requestWithMultipleImages(fileParamName: "file", urlAppendString: "upload_file_tos3", imageData: arr, parameters: param, mimeType: "video/quicktime/m4v") { (response) in
            print(" video Response : \(response)")
            if response["data"].arrayValue.count != 0
            {
                self.vedioUploadStatus = true
                self.collViewOfAttachments.reloadData()
                
                let arrayOfData = response["data"].arrayValue
                
                for currentdata in arrayOfData{
                    var dict = [String:String]()
                    dict["filename"] = currentdata["filename"].stringValue
                    dict["type"] = currentdata["type"].stringValue
                    dict["video_thumb"] = currentdata["video_thumb"].stringValue
                    print(dict)
                    
                    self.selectedAttachments.add(dict)
                    
                }
                print(self.selectedAttachments)
            }
                //            ActivityIndicatorView.hiding()
                
            else
            {
                self.displayAlert(alertStr: "Unable to upload file, please try again", title: "")
                
                //failed Condition
                self.vedioUploadStatus = true
                self.videoDataArr = []
                
                //                if self.deleteAttachment
                //                {
                self.selectedItemsexisted =  self.previousSelectedItemsexisted
                
                //                 self.vedioUploadStatus = true
                self.deleteAttachment = false
                
                print(self.selectedItemsexisted)
                print(self.selectedItemsexisted.count)
                
                
                
                if self.selectedItemsexisted.count != 0{
                    self.collViewOfAttachments.isHidden = false
                    self.lblNoFiles.isHidden = true
                    self.collViewOfAttachments.reloadData()
                    
                }
                else{
                    self.collViewOfAttachments.isHidden = true
                    self.lblNoFiles.isHidden = false
                    
                }
                
            }
            //            }
            
        }
    }
    
    func uploadDocumentMultiple(arr : [Data],fileName:String){
        
        let param = ["name" :"post"]
        
        
        //        ActivityIndicatorView.show("Uploading...")
        Helpers.requestWithMultipleImages(fileParamName: "file", urlAppendString: "upload_file_tos3", imageData: arr, parameters: param, mimeType: pathExt) { (response) in
            print("Response : \(response)")
            
            if response["data"].arrayValue.count != 0
            {
                self.documentUploadStatus = true
                self.collViewOfAttachments.reloadData()
                
                let arrayOfData = response["data"].arrayValue
                
                for currentdata in arrayOfData{
                    var dict = [String:String]()
                    dict["filename"] = currentdata["filename"].stringValue
                    dict["type"] = currentdata["type"].stringValue
                    dict["original_name"] = fileName
                    print(dict)
                    
                    self.selectedAttachments.add(dict)
                    
                }
                
                print(self.selectedAttachments)
            }
            else
            {
                self.displayAlert(alertStr: "Unable to upload file, please try again", title: "")
                //failed Condition
                self.documentUploadStatus = true
                self.documentDataArr = []
                
                //                if self.deleteAttachment
                //                {
                self.selectedItemsexisted =  self.previousSelectedItemsexisted
                
                //                self.imageUploadStatus = true
                self.deleteAttachment = false
                
                print(self.selectedItemsexisted)
                print(self.selectedItemsexisted.count)
                
                
                
                if self.selectedItemsexisted.count != 0{
                    self.collViewOfAttachments.isHidden = false
                    self.lblNoFiles.isHidden = true
                    self.collViewOfAttachments.reloadData()
                    
                }
                else{
                    self.collViewOfAttachments.isHidden = true
                    self.lblNoFiles.isHidden = false
                    
                }
                
                //                }
            }
            //            ActivityIndicatorView.hiding()
            
            
            
        }
    }
    //MARK: - Upload Image
    
    //    func uploadImage(img : UIImage){
    //
    //        var param = [String:Any]()
    //        var imgData = Data()
    //
    //        let mime    = "image/png"
    //
    //
    //                let img2 = img.resized(withPercentage: 0.5)
    //
    //                imgData = (img2?.pngData())!
    //
    //                param = ["name" :"post"]
    //
    //
    //
    //
    //
    //        ActivityIndicatorView.show("Uploading...")
    //        let tData = NSData()
    //        Helpers.requestWith(fileParamName: "file", originalCover: "", urlAppendString: "upload_file_tos3", imageData: imgData, originalImg: tData as Data, parameters: param, mimeType: mime) { (response) in
    //
    //            print("response : \(response)")
    //            var dict = [String:String]()
    //            dict["filename"] = response["filename"].stringValue
    //            dict["type"] = response["type"].stringValue
    //            print(dict)
    //
    //            self.selectedAttachments.add(dict)
    //
    //            print(self.selectedAttachments)
    //
    //            ActivityIndicatorView.hiding()
    ////            self.callViewContents()
    //        }
    //    }
    
    //    func uploadVideo(vedioUrl : URL){
    //
    //        var param = [String:Any]()
    //        param = ["name" :"post"]
    //
    //
    //
    //        let mime    = "video/quicktime/m4v"
    //
    //        do {
    //            let videoData = try NSData(contentsOf: vedioUrl, options: NSData.ReadingOptions())
    ////            print(videoData)
    //            ActivityIndicatorView.show("Uploading...")
    //            let tData = NSData()
    //            Helpers.requestWith(fileParamName: "file", originalCover: "", urlAppendString: "upload_file_tos3", imageData: videoData as Data, originalImg: tData as Data, parameters: param, mimeType: mime) { (response) in
    //
    //                print("response : \(response)")
    //
    //                ActivityIndicatorView.hiding()
    //                var dict = [String:String]()
    //                dict["filename"] = response["filename"].stringValue
    //                dict["type"] = response["type"].stringValue
    //                print(dict)
    //
    //                self.selectedAttachments.add(dict)
    //                print(self.selectedAttachments)
    //
    //
    //                //            self.callViewContents()
    //            }
    //        } catch {
    //            print(error)
    //        }
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //    }
    @IBAction func btnSharableOnClick(_ sender: UISwitch) {
        if (sender.isOn == true){
            isSharing = true
            
        }
        else{
            isSharing = false
            
        }
        
    }
    @IBAction func btnConversation_OnClick(_ sender: UISwitch) {
        if (sender.isOn == true){
            isConversation = true
            
        }
        else{
            isConversation = false
        }
        
    }
    
    @IBAction func btnPost_OnClick(_ sender: Any) {
        if fromEdit
        {
            if DatavalidationForUpdatePost()
            {
                self.apiForUpdatePost()
                
            }
        }
        else
        {
            
            
            if DatavalidationForSubmit()
            {
                self.apiForCreatePost()
                
            }
        }
        
    }
    
    
    func apiForCreatePost() {
        
        //        "ticket_type":dataFromPrevious["ticket_type"]!,
        
        var parameter = [String:Any]()
        //        :all_family/only_users/only_groups/public
        
        if self.selectedPostType == "only_groups"
        {
            parameter = ["category_id":"3","created_by":UserDefaults.standard.value(forKey: "userId") as! String, "post_info":[],
                         "snap_description":txtDescription.text!.trimmingCharacters(in: .whitespacesAndNewlines),
                         "title":"",
                         "post_attachment":self.selectedAttachments,
                         "type":"post",
                         "post_type":self.selectedPostType,
                         "selected_groups" : self.arrayOfSelectedUser,"selected_users":[],"is_shareable":self.isSharing!,"conversation_enabled":self.isConversation!,"privacy_type":self.privacyType] as [String : Any]
            
        }
        else  if self.selectedPostType == "only_users"
        {
            parameter = ["category_id":"3","created_by":UserDefaults.standard.value(forKey: "userId") as! String, "post_info":[],
                         "snap_description":txtDescription.text!.trimmingCharacters(in: .whitespacesAndNewlines),
                         "title":"",
                         "post_attachment":self.selectedAttachments,
                         "type":"post",
                         "post_type":self.selectedPostType,
                         "selected_groups":[],"selected_users":self.arrayOfSelectedUser,"is_shareable":self.isSharing!,"conversation_enabled":self.isConversation!,"privacy_type":self.privacyType] as [String : Any]
        }
        else
        {
            parameter = ["category_id":"3","created_by":UserDefaults.standard.value(forKey: "userId") as! String, "post_info":[],
                         "snap_description":txtDescription.text!.trimmingCharacters(in: .whitespacesAndNewlines),
                         "title":"",
                         "post_attachment":self.selectedAttachments,
                         "type":"post",
                         "post_type":self.selectedPostType,
                         "selected_groups" : [],"selected_users":[],"is_shareable":self.isSharing!,"conversation_enabled":self.isConversation!,"privacy_type":self.privacyType] as [String : Any]
        }
        
        
        print(parameter)
        if self.selectedAttachments.count > 0{
            
        }else{
            
            guard self.txtDescription.text!.trimmingCharacters(in: .whitespacesAndNewlines).count > 0 else {
                Helpers.showAlertDialog(message: "Please add description or attachments", target: self)
                return
            }
        }
        
        
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.CreatePost(parameter: parameter), completion: {(result) in
            
            switch result {
            case .success(let response):
                do {
                    let jsonData = JSON(response.data as Any)
                    if response.statusCode == 200{
                        let AlertCus = CustomAlertVC.show(in: self, title: "Success", message: "Successfully created Post", image:UIImage.init(named: "Green_tick")!)
                        AlertCus.addAction(CPAlertAction.init(title: "OK", type: .normal, handler: {
                            
                            if self.fromGroup{
                                self.navigationController?.popViewController(animated: true)
                                return
                            }
                            if self.isFromForward{
                                self.navigationController?.popViewController(animated: true)
                                return
                            }
                                
                            else{
                                //                                self.navigationController?.popToViewController(ofClass: HomeTabViewController.self)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let tabBar = mainStoryBoard.instantiateViewController(withIdentifier: "mainTabView") as! HomeViewTabController
                                //  loginView.regType = regType
                                appDel.noFamily = false
                                if self.selectedPostType == "public"{
                                    appDel.postcreatedInPublic = true
                                    appDel.iscreation = isFromCreation.publicfeed
                                    
                                }else{
                                    appDel.iscreation = isFromCreation.myfamily
                                    
                                    appDel.postcreatedInPublic = false
                                }
                                self.navigationController?.pushViewController(tabBar, animated: true)
                            }
                            self.appDelegate.isConversationUpdated = true
                        }))
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.apiForCreatePost()
                        }
                    }
                    else{
                        print(jsonData["code"])
                        Helpers.showAlertDialog(message: "Error in Post Upload", target: self)
                    }
                } catch let err {
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
    
    func apiForUpdatePost() {
        
        //        "ticket_type":dataFromPrevious["ticket_type"]!,
        
        var parameter = [String:Any]()
        //        :all_family/only_users/only_groups/public
        
        parameter = ["id" : editPostDetails["post_id"].stringValue,
                     "snap_description":txtDescription.text!.trimmingCharacters(in: .whitespacesAndNewlines),
                     "title":"",
                     "post_attachment":self.selectedAttachments,
                     "type":"post","is_shareable":self.isSharing!,"conversation_enabled":self.isConversation!,"post_ref_id":self.editPostDetails["post_ref_id"].stringValue, "user_id":UserDefaults.standard.value(forKey: "userId") as! String,"to_group_id_array":self.arrayOfSelectedUser,"delete_post":self.deletedArr,"created_by":isCreatedby,"privacy_type":self.privacyType,"update_type":self.postType,"type_id":editPostDetails["post_id"].stringValue,"inactive_active_array":self.activeInactive] as [String : Any]
        
        
        print(parameter)
        
        
        
        if self.selectedAttachments.count > 0{
            
        }else{
            
            guard self.txtDescription.text!.trimmingCharacters(in: .whitespacesAndNewlines).count > 0 else {
                Helpers.showAlertDialog(message: "Please add description or attachments", target: self)
                return
            }
        }
        
        
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.UpdatePost(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    print(jsonData["status"])
                    print(response.statusCode)
                    if response.statusCode == 200
                    {
                        //
                        let AlertCus = CustomAlertVC.show(in: self, title: "Success", message: "Successfully Updated Post", image:UIImage.init(named: "Green_tick")!)
                        AlertCus.addAction(CPAlertAction.init(title: "OK", type: .normal, handler: {
                            if self.selectedPostType == "public"{
                                appDel.postcreatedInPublic = true
                                appDel.iscreation = isFromCreation.publicfeed
                                self.navigationController?.popViewController(animated: true)
                                
                                
                                
                            }
                            else if self.privacyType.lowercased() == "private"{
                                self.navigationController?.popViewController(animated: true)
                                return
                            }
                            else{
                                appDel.iscreation = isFromCreation.myfamily
                                
                                appDel.postcreatedInPublic = false
                                
                                self.editPostDetails["snap_description"] = JSON(self.txtDescription.text!)
                                self.editPostDetails["is_shareable"] = JSON(self.isSharing!)
                                self.editPostDetails["conversation_enabled"] = JSON(self.isConversation!)
                                self.editPostDetails["post_attachment"] = JSON(self.selectedAttachments)
                                self.editPostDetails["to_group_id_array"] = JSON(self.arrayOfSelectedUser)
                                
                                print(self.editPostDetails)
                                
                                
                                self.callBackDelegate!.postupdateSuccess(index:self.selectedTag , SuccessData:self.editPostDetails)
                                self.navigationController?.popViewController(animated: true)
                                
                                
                            }
                            //                            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                            //                            let vc = storyboard.instantiateViewController(withIdentifier: "mainTabView") as! HomeViewTabController
                            //                            self.navigationController?.pushViewController(vc, animated: true)
                            
                            
                        }))
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.apiForUpdatePost()
                        }
                    }
                    else
                    {
                        
                        print(jsonData["code"])
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                    
                    
                } catch let err {
                    
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    //MARK:- VALIDATION
    func DatavalidationForSubmit()->Bool{
        
        if txtPostType.text == "" {
            
            Helpers.showAlertDialog(message: "Please select where you want to Post this to", target: self)
            return false
        }
        else
            if self.imgDataArr.count != 0  && !imageUploadStatus
            {
                
                Helpers.showAlertDialog(message: "Uploading images Please Wait a moment.", target: self)
                return false
                
                
                
            }
            else if !vedioUploadStatus{
                
                Helpers.showAlertDialog(message: "Uploading video Please Wait a moment.", target: self)
                
                return false
                
            }
            else if self.documentDataArr.count != 0 && !documentUploadStatus{
                
                Helpers.showAlertDialog(message: "Uploading document Please Wait a moment.", target: self)
                
                return false
                
            }
            else {
                
                return true
                
        }
    }
    
    func DatavalidationForUpdatePost()->Bool{
        
        
        if self.imgDataArr.count != 0  && !imageUploadStatus
        {
            
            Helpers.showAlertDialog(message: "Uploading document Please Wait a moment.", target: self)
            return false
            
            
            
        }
        else if !vedioUploadStatus{
            
            Helpers.showAlertDialog(message: "Uploading video Please Wait a moment.", target: self)
            
            return false
            
        }
        else if self.documentDataArr.count != 0 && !documentUploadStatus{
            
            Helpers.showAlertDialog(message: "Wait for document uploading", target: self)
            
            return false
            
        }
        else {
            
            return true
            
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    func selctionCallback(SelectedUserId:NSMutableArray,currentIndex:Int)
    {
        if self.fromEdit{
            if selectedIdsOriginal.count > 0{
                
                for item in selectedIdsOriginal{
                    var found = false
                    for tempItem in SelectedUserId{
                        let dic = tempItem as! NSDictionary
                        let ids = dic.value(forKey: "id")!
                        if item == "\(ids)"{
                            found = true
                            break
                        }
                    }
                    if found{
                        
                    }
                    else{
                        self.deletedArr.append(item)
                    }
                }
                self.arrayOfSelectedUser = SelectedUserId
                
                for(index,item) in self.arrayOfSelectedUser.enumerated(){
                    for temp in self.selectedIdsOriginal{
                        let dic = item as! NSDictionary
                        let id = dic.value(forKey: "id")
                        
                        if temp == "\(id!)"{
                            var newDic = NSMutableDictionary()
                            newDic = dic.mutableCopy() as! NSMutableDictionary
                            newDic.setValue("old", forKey: "type")
                            self.arrayOfSelectedUser.replaceObject(at: index, with: newDic)
                            
                        }
                        else{
                            var newDic = NSMutableDictionary()
                            newDic = dic.mutableCopy() as! NSMutableDictionary
                            newDic.setValue("new", forKey: "type")
                            self.arrayOfSelectedUser.replaceObject(at: index, with: newDic)
                        }
                    }
                }
                print(arrayOfSelectedUser)
                
                self.btnPostedFamilies.setTitle("\(self.arrayOfSelectedUser.count) families are selected", for: .normal)
                self.selectedGroups = arrayOfSelectedUser as! [[String : AnyObject]]
                
            }
        }
        else{
            print(SelectedUserId)
            if currentIndex == 0{
                self.heightOfPostSelection.constant = 90
                self.lblPostTypeStatus.isHidden = false
                self.arrayOfSelectedUser = SelectedUserId
                self.lblPostTypeStatus.text = "\(self.arrayOfSelectedUser.count) Families Selected"
            }
            else if currentIndex == 1{
                self.heightOfPostSelection.constant = 90
                self.lblPostTypeStatus.isHidden = false
                self.arrayOfSelectedUser = SelectedUserId
                self.lblPostTypeStatus.text = "\(self.arrayOfSelectedUser.count) Peoples Selected"
            }
            else{
                self.heightOfPostSelection.constant = 75
                self.lblPostTypeStatus.isHidden = true
                self.arrayOfSelectedUser = []
                self.txtPostType.text = ""
                self.selectedPostType = ""
            }
        }
    }
    
    
    
    //MARK:- collectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        //        return self.selectedItems.count
        return self.selectedItemsexisted.count
        
    }
    
    
    func generateThumbnail(for asset:AVAsset) -> UIImage? {
        let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        let time = CMTimeMake(value: 1, timescale: 2)
        let img = try? assetImgGenerate.copyCGImage(at: time, actualTime: nil)
        if img != nil {
            let frameImg  = UIImage(cgImage: img!)
            return frameImg
        }
        return nil
    }
    func generatePdfThumbnail(of thumbnailSize: CGSize , for documentUrl: URL, atPage pageIndex: Int) -> UIImage? {
        let pdfDocument = PDFDocument(url: documentUrl)
        let pdfDocumentPage = pdfDocument?.page(at: pageIndex)
        return pdfDocumentPage?.thumbnail(of: thumbnailSize, for: PDFDisplayBox.trimBox)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PostImageCollectionViewCell
        let item = self.selectedItemsexisted[indexPath.row]
        cell.btnClose.tag = indexPath.row
        
        //         for item in self.selectedItems{
        
        if item["existing"]as! Bool == true
        {
            var ImageURL = ""
            if isFromForward{
                ImageURL = "\(BaseUrl.conversation)"
            }
            else{
                ImageURL = "\(BaseUrl.post_image)"
            }
            
            let dict = item["value"] as! [String:String]
            let history_pic = dict["filename"]!
            let history_type = dict["type"]!
            
            
            if history_pic != ""
            {
                if history_type.contains("image")
                {
                    let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(ImageURL)"+history_pic
                    let imgUrl = URL(string: temp)
                    cell.imgOfPostItems.kf.indicatorType  = .activity
                    cell.imgOfPlayVideo.isHidden = true
                    cell.viewOfVideoBg.isHidden = true
                    cell.imgOfPostItems.kf.setImage(with: imgUrl, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
                    cell.imgOfProgress.image = #imageLiteral(resourceName: "completed_progress.png")
                    cell.activityIndicator.isHidden = true
                }
                else  if history_type.contains("text") || history_type.contains("pdf") || history_type.contains("doc") || history_type.contains("docx") || history_type.contains("xls") || history_type.contains("xlsx") || history_type.contains("msword") || history_type.contains("txt")
                {
                    cell.imgOfPostItems.kf.indicatorType  = .activity
                    cell.imgOfPlayVideo.isHidden = true
                    cell.viewOfVideoBg.isHidden = true
                    if history_type.contains("pdf"){
                        cell.imgOfPostItems.image = #imageLiteral(resourceName: "pdfIcon")
                    }
                    else if history_type.contains("doc") || history_type.contains("docx") || history_type.contains("msword"){
                        cell.imgOfPostItems.image = #imageLiteral(resourceName: "DocumentImage")
                    }
                    else{
                       cell.imgOfPostItems.image = #imageLiteral(resourceName: "DocumentIcon")
                    }
                     
                    
                    let temp = "\(Helpers.imageURl)"+"\(ImageURL)"+history_pic
                    let docUrl = URL(string: temp)
                    print(temp)
                    
                    
                  /*  if let pdf = CGPDFDocument(docUrl as! CFURL)
                    {
                        print("it is pdf")
                        let thumbnailSize = CGSize(width: cell.imgOfPostItems.frame.width, height: cell.imgOfPostItems.frame.height)
                        
                        let thumbnail = generatePdfThumbnail(of: thumbnailSize, for: docUrl!, atPage: 0)
                        
                        cell.imgOfPostItems.image = thumbnail as! UIImage
                    }
                    else
                    {
                        print("not pdf")
                        let asset = AVAsset(url: docUrl!)
                        
                        if let img = self.generateThumbnail(for: asset)
                        {
                            cell.imgOfPostItems.image = img
                            //
                        }else {
                            print("Error: Thumbnail can't be generated.")
                            cell.imgOfPostItems.image = #imageLiteral(resourceName: "imgNoImage")
                        }
                        
                    }*/
                    cell.imgOfProgress.image = #imageLiteral(resourceName: "completed_progress.png")
                    cell.activityIndicator.isHidden = true
                }
                else{
                    if let thumb = dict["video_thumb"]{
                        
                        let temp = "\(Helpers.imageURl)"+thumb
                        let imgUrl = URL(string:BaseUrl.imaginaryURLForDetail2X + temp )
                        cell.imgOfPostItems.kf.indicatorType  = .activity
                        cell.imgOfPlayVideo.isHidden = false
                        cell.viewOfVideoBg.isHidden = false
                        cell.imgOfPostItems.kf.setImage(with: imgUrl, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
                        
                    }
                        
                        //                    let asset = AVAsset(url: imgUrl!)
                        //                    if let img = self.generateThumbnail(for: asset)
                        //                    {
                        //                        cell.imgOfPostItems.image = img
                        //                        //
                        //                    }
                    else {
                        print("Error: Thumbnail can be generated.")
                        cell.imgOfPostItems.image = #imageLiteral(resourceName: "imgNoImage")
                        cell.imgOfPlayVideo.isHidden = false
                        
                        //                                                           return
                    }
                    
                    //                                            cell.imgOfPostItems.kf.setImage(with: imgUrl, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
                    cell.imgOfProgress.image = #imageLiteral(resourceName: "completed_progress.png")
                    cell.activityIndicator.isHidden = true
                    
                }
                
            }
        }
        else{
            print(item["value"])
            cell.imgOfPlayVideo.isHidden = true
            cell.viewOfVideoBg.isHidden = true
            
            if let docUrl = item["document"] , docUrl != nil
            {
                //                print(docUrl)
                
                print(docUrl)
                
                let size: CGSize = CGSize(width: cell.imgOfPostItems.frame.width, height: cell.imgOfPostItems.frame.height)
                let scale = UIScreen.main.scale
                
                // Create the thumbnail request.
                if #available(iOS 13.0, *) {
                    let request = QLThumbnailGenerator.Request(fileAt: docUrl as! URL,
                                                               size: size,
                                                               scale: scale,
                                                               representationTypes: .all)
                    
                    // Retrieve the singleton instance of the thumbnail generator and generate the thumbnails.
                    let generator = QLThumbnailGenerator.shared
                    generator.generateRepresentations(for: request) { (thumbnail, type, error) in
                        DispatchQueue.main.async {
                            if thumbnail == nil || error != nil {
                                print("error in thumbnail generation")
                                cell.imgOfPostItems.image = #imageLiteral(resourceName: "imgNoImage")
                                
                                // Handle the error case gracefully.
                            } else {
                                //                                    print(thumbnail?.uiImage)
                                cell.imgOfPostItems.image = thumbnail?.uiImage
                                
                                // Display the thumbnail that you created.
                            }
                        }
                    }
                } else {
                    // Fallback on earlier versions
                    
                    
                    
                    if let pdf = CGPDFDocument(docUrl as! CFURL)
                    {
                        print("it is pdf")
                        let thumbnailSize = CGSize(width: cell.imgOfPostItems.frame.width, height: cell.imgOfPostItems.frame.height)
                        
                        let thumbnail = generatePdfThumbnail(of: thumbnailSize, for: docUrl as! URL, atPage: 0)
                        
                        cell.imgOfPostItems.image = (thumbnail as! UIImage)
                    }
                    else
                    {
                        print("not pdf")
                        let asset = AVAsset(url: docUrl as! URL)
                        
                        if let img = self.generateThumbnail(for: asset)
                        {
                            cell.imgOfPostItems.image = img
                            //
                        }else {
                            print("Error: Thumbnail can be generated.")
                            cell.imgOfPostItems.image = #imageLiteral(resourceName: "imgNoImage")
                            //                                                           return
                        }
                        
                    }
                    
                    
                }
                
                if !documentUploadStatus{
                    let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "imageProgressor", withExtension: "gif")!)
                    let advTimeGif = UIImage.gifImageWithData(imageData!)
                    cell.imgOfProgress.image = advTimeGif
                    
                    
                    //                                    cell.imgOfProgress.image = #imageLiteral(resourceName: "ongoing_progress.png")
                    cell.activityIndicator.isHidden = false
                    
                }
                else
                {
                    cell.imgOfProgress.image = #imageLiteral(resourceName: "completed_progress.png")
                    cell.activityIndicator.isHidden = true
                    
                }
                
            }
            else
            {
                if let url = item["value"] as? Data{
                    if let type = item["type"] as? String, !type.isEmpty, type == "image"{
                        //                        do{
                        //                            let imageData = try Data.init(contentsOf: url)
                        cell.imgOfPostItems.image = UIImage(data: url)
                        cell.imgOfPlayVideo.isHidden = true
                        if !imageUploadStatus
                        {
                            let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "imageProgressor", withExtension: "gif")!)
                            let advTimeGif = UIImage.gifImageWithData(imageData!)
                            cell.imgOfProgress.image = advTimeGif
                            cell.activityIndicator.isHidden = false
                            cell.activityIndicator.startAnimating()
                        }
                        else
                        {
                            cell.imgOfProgress.image = #imageLiteral(resourceName: "completed_progress.png")
                            cell.activityIndicator.isHidden = true
                        }
                        
                        //                    }
                        //                        catch{
                        //                            print("Error")
                        //                        }
                    }
                    if let type = item["type"] as? String, !type.isEmpty, type == "video"{
                        cell.imgOfPostItems.image = generateThumbnail(asset: url.getAVAsset())
                        self.selectedImageV.image = cell.imgOfPostItems.image
                        
                        
                        cell.imgOfPlayVideo.isHidden = false
                        if !vedioUploadStatus
                        {
                            let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "imageProgressor", withExtension: "gif")!)
                            let advTimeGif = UIImage.gifImageWithData(imageData!)
                            cell.imgOfProgress.image = advTimeGif
                            cell.activityIndicator.isHidden = false
                            cell.activityIndicator.startAnimating()
                            
                        }
                        else
                        {
                            cell.imgOfProgress.image = #imageLiteral(resourceName: "completed_progress.png")
                            cell.activityIndicator.isHidden = true
                        }
                        
                    }
                }
                else{
                    
                    let currentItem = item["value"] as! DKAsset
                    
                    switch currentItem.type {
                    case .photo:
                        
                        currentItem.fetchImage(with: CGSize.init(width: 200, height: 200), completeBlock: { image, info in
                            cell.imgOfPostItems.image = image
                        })
                        
                        cell.imgOfPlayVideo.isHidden = true
                        
                        if !imageUploadStatus
                                               {
                                                   let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "imageProgressor", withExtension: "gif")!)
                                                   let advTimeGif = UIImage.gifImageWithData(imageData!)
                                                   cell.imgOfProgress.image = advTimeGif
                                                   cell.activityIndicator.isHidden = false
                                                   cell.activityIndicator.startAnimating()
                                               }
                                               else
                                               {
                                                   cell.imgOfProgress.image = #imageLiteral(resourceName: "completed_progress.png")
                                                   cell.activityIndicator.isHidden = true
                                               }
//                        if !imageUploadStatus
//                        {
//                            if cell.imgOfProgress.image == #imageLiteral(resourceName: "completed_progress.png"){
//
//                            }
//                            else{
//                                let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "imageProgressor", withExtension: "gif")!)
//                                let advTimeGif = UIImage.gifImageWithData(imageData!)
//                                cell.imgOfProgress.image = advTimeGif
//                                cell.activityIndicator.isHidden = false
//                                cell.activityIndicator.startAnimating()
//                            }
//                        }
//                        else
//                        {
//                            cell.imgOfProgress.image = #imageLiteral(resourceName: "completed_progress.png")
//                            cell.activityIndicator.isHidden = true
//
//                        }
                        
                    case .video:
                        currentItem.fetchImage(with: CGSize.init(width: 200, height: 200), completeBlock: { image, info in
                            self.selectedImageV.image = image
                            cell.imgOfPostItems.image = image
                            
                        })
                        
                        cell.imgOfPlayVideo.isHidden = false
                        if !vedioUploadStatus
                        {
//                            if cell.imgOfProgress.image == #imageLiteral(resourceName: "completed_progress.png"){
//
//                            }
//                            else{
                                
                                let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "imageProgressor", withExtension: "gif")!)
                                let advTimeGif = UIImage.gifImageWithData(imageData!)
                                cell.imgOfProgress.image = advTimeGif
                                cell.activityIndicator.isHidden = false
                                cell.activityIndicator.startAnimating()
                                
//                            }
                            
                        }
                        else
                        {
                            cell.imgOfProgress.image = #imageLiteral(resourceName: "completed_progress.png")
                            cell.activityIndicator.isHidden = true
                            
                        }
                        
                    }
                }
            }
        }
        
        return cell
    }
    
    
    @IBAction func btnCloseImage_Onclick(_ sender: UIButton) {
        //below code for stop current request
        //        Helpers.StopAPICALL()
        if self.imgDataArr.count != 0  && !imageUploadStatus{
            
            //            Helpers.showAlertDialog(message: "Wait for image uploading", target: self)
            
            let alert = UIAlertController(title: "Familheey", message: "Do you really want to cancel upload?", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "YES", style: .destructive, handler: { (acion) in
                if  self.imageUploadStatus{
                    self.deleteAlreadyUploadingFiles(index:sender.tag)
                }
                //Jeena 23-02-2021  //fixed: close all uploading images when we close one image while uploading
                /*************************************************/
                else if !self.imageUploadStatus{
                    self.selectedItemsexisted.remove(at: sender.tag)
                    if self.selectedItemsexisted.count != 0
                    {
                        self.collViewOfAttachments.isHidden = false
                        self.lblNoFiles.isHidden = true
                        self.collViewOfAttachments.reloadData()
                        
                    }
                    else
                    {
                        self.collViewOfAttachments.isHidden = true
                        self.lblNoFiles.isHidden = false
                        self.imageUploadStatus = true
                        self.vedioUploadStatus = true
                    }
                }
                /***************************************************/
                else
                {
                    self.deleteAttachment = true
                    Helpers.StopAPICALL()

                }
                
            }))
            
            alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
            
        }
        else if  !vedioUploadStatus {//&& self.videoDataArr.count != 0 : commented jeena 04-02-2021 to solve crash issue when close uploading video
            
            //            Helpers.showAlertDialog(message: "Wait for video uploading", target: self)
            
            
            let alert = UIAlertController(title: "Familheey", message: "Do you really want to cancel upload?", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "YES", style: .destructive, handler: { (acion) in
                
                if  self.vedioUploadStatus
                {
                    self.deleteAlreadyUploadingFiles(index:sender.tag)
                }
                //Jeena 23-02-2021  //fixed: close uploading video when we close any file while uploading
                /*************************************************/
                else if !self.vedioUploadStatus {
                    self.selectedItemsexisted.remove(at: sender.tag)
                    if self.selectedItemsexisted.count != 0
                    {
                        self.collViewOfAttachments.isHidden = false
                        self.lblNoFiles.isHidden = true
                        self.collViewOfAttachments.reloadData()
                        
                    }
                    else
                    {
                        self.collViewOfAttachments.isHidden = true
                        self.lblNoFiles.isHidden = false
                        self.imageUploadStatus = true
                        self.vedioUploadStatus = true
                    }
                }
                /***************************************************/
                else
                {
                    self.deleteAttachment = true
                    AWSS3Manager.shared.cancelAllUploads()
                    self.vedioUploadStatus = true
                    self.videoDataArr = []
                    self.selectedItemsexisted =  self.previousSelectedItemsexisted
                    self.deleteAttachment = false
                    
                    if self.selectedItemsexisted.count != 0{
                        self.collViewOfAttachments.isHidden = false
                        self.lblNoFiles.isHidden = true
                        self.collViewOfAttachments.reloadData()
                        
                    }
                    else{
                        self.collViewOfAttachments.isHidden = true
                        self.lblNoFiles.isHidden = false
                        
                    }
                    
                }
                
            }))
            
            alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
            
        }
        else if self.documentDataArr.count != 0 && !documentUploadStatus{
            
            //            Helpers.showAlertDialog(message: "Wait for document uploading", target: self)
            
            
            let alert = UIAlertController(title: "Familheey", message: "Do you really want to delete currently uploading document otherwise wait for document uploading", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "YES", style: .destructive, handler: { (acion) in
                if  self.documentUploadStatus
                {
                    self.deleteAlreadyUploadingFiles(index:sender.tag)
                }
                //Jeena 01-03-2021  //fixed: close all uploading docs when we close one doc while uploading
                /*************************************************/
                else if !self.documentUploadStatus{
                    self.selectedItemsexisted.remove(at: sender.tag)
                    if self.selectedItemsexisted.count != 0
                    {
                        self.collViewOfAttachments.isHidden = false
                        self.lblNoFiles.isHidden = true
                        self.collViewOfAttachments.reloadData()
                        
                    }
                    else
                    {
                        self.collViewOfAttachments.isHidden = true
                        self.lblNoFiles.isHidden = false
                        
                    }
                }
                /***************************************************/
                else
                {
                    self.deleteAttachment = true
                    Helpers.StopAPICALL()
                }
                
            }))
            
            alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
        }
        else
        {
            //            self.selectedItemsexisted.remove(at: sender.tag)
            //            self.selectedAttachments.removeObject(at: sender.tag)
            //            if self.selectedItemsexisted.count != 0
            //            {
            //                self.collViewOfAttachments.isHidden = false
            //                self.lblNoFiles.isHidden = true
            //                self.collViewOfAttachments.reloadData()
            //
            //            }
            //            else
            //            {
            //                self.collViewOfAttachments.isHidden = true
            //                self.lblNoFiles.isHidden = false
            //
            //            }
            
            self.deleteAlreadyUploadingFiles(index: sender.tag)
        }
        //        self.collViewOfAttachments.reloadData()
        //         print("current Agenda: \(currentAgenda)")
        
    }
    
    
    func deleteAlreadyUploadingFiles(index:Int){
        //        if imageUploadStatus || documentUploadStatus || vedioUploadStatus {
        //            //            imageUploadStatus = false
        //            //            vedioUploadStatus =  false
        //            //            documentUploadStatus = false
        //            self.videoDataArr = []
        //            self.selectedItemsexisted =  self.previousSelectedItemsexisted
        //            if self.selectedItemsexisted.count != 0{
        //                self.collViewOfAttachments.isHidden = false
        //                self.lblNoFiles.isHidden = true
        //                self.collViewOfAttachments.reloadData()
        //
        //            }
        //            else{
        //                self.collViewOfAttachments.isHidden = true
        //                self.lblNoFiles.isHidden = false
        //            }
        //
        //        }
        //        else{
        self.selectedItemsexisted.remove(at: index)
        self.selectedAttachments.removeObject(at: index)
        if self.selectedItemsexisted.count != 0
        {
            self.collViewOfAttachments.isHidden = false
            self.lblNoFiles.isHidden = true
            self.collViewOfAttachments.reloadData()
            
        }
        else
        {
            self.collViewOfAttachments.isHidden = true
            self.lblNoFiles.isHidden = false
            
        }
        //        }
        
        //        self.collViewOfAttachments.reloadData()
        
    }
    
    //    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    //        let item = self.selectedItems[indexPath.row]
    //
    //
    //
    //    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //        let numberOfCellInRow                   = 3
        //        let padding : Int                       = 2
        //        let collectionCellWidth : CGFloat       = (self.collViewOfAttachments.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
        //        return CGSize(width: collectionCellWidth, height: collectionCellWidth + 20)
        
        return CGSize.init(width: 90, height:  90)
    }
    
    
}

extension CreatePostViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.arrayOfPostTypewitharrow.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! postTypeDropdownCell
        let dict = arrayOfPostTypewitharrow[indexPath.row]
        if dict["hidebool"] as! Bool == true
        {
            cell.imgOfArrow.isHidden = true
        }
        else
        {
            cell.imgOfArrow.isHidden = false
            
        }
        cell.lblTitle.text = dict["text"] as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = self.arrayOfPostTypewitharrow[indexPath.row]
        let selectedText = dict["text"] as! String
        self.txtPostType.text = selectedText
        self.tblViewDropList.isHidden = true
        self.viewOfShade.isHidden = true
        
        if selectedText == "All Families"
        {
            self.heightOfPostSelection.constant = 75
            self.lblPostTypeStatus.isHidden = true
            self.arrayOfSelectedUser = []
            self.selectedPostType = "all_family"
            self.privacyType = "public"
            
        }
        else if selectedText == "Selected families"
        {
            self.selectedPostType = "only_groups"
            self.privacyType = "public"
            let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SelectFamilyOrPeopleViewController") as! SelectFamilyOrPeopleViewController
            vc.selectedIndex = 0
            vc.selectionDelegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
            /* else if selectedText == "Selected Connections"
             {
             self.selectedPostType = "only_users"
             self.privacyType = "public"
             let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
             let vc = storyboard.instantiateViewController(withIdentifier: "SelectFamilyOrPeopleViewController") as! SelectFamilyOrPeopleViewController
             vc.selectedIndex = 1
             vc.selectionDelegate = self
             
             self.navigationController?.pushViewController(vc, animated: true)
             }*/
        else  if selectedText == "Everyone (Public)"
        {
            self.selectedPostType = "public"
            self.privacyType = "public"
            self.heightOfPostSelection.constant = 75
            self.lblPostTypeStatus.isHidden = true
            self.arrayOfSelectedUser = []
            
        }
        else
        {
            self.selectedPostType = "private"
            self.privacyType = "private"
            self.heightOfPostSelection.constant = 75
            self.lblPostTypeStatus.isHidden = true
            self.arrayOfSelectedUser = []
        }
        
    }
}

// Support methods
extension CreatePostViewController {
    /* Gives a resolution for the video by URL */
    func resolutionForLocalVideo(url: URL) -> CGSize? {
        guard let track = AVURLAsset(url: url).tracks(withMediaType: AVMediaType.video).first else { return nil }
        let size = track.naturalSize.applying(track.preferredTransform)
        return CGSize(width: abs(size.width), height: abs(size.height))
    }
}
// MARK: - DKImagePickerControllerBaseUIDelegate

extension CreatePostViewController : DKImageAssetExporterObserver{
    func exporterWillBeginExporting(exporter: DKImageAssetExporter, asset: DKAsset) {
        
    }
    
    func exporterDidEndExporting(exporter: DKImageAssetExporter, asset: DKAsset) {
        print(DKAsset.self)
        
    }
}

class AssetClickHandler: DKImagePickerControllerBaseUIDelegate {
    override func imagePickerController(_ imagePickerController: DKImagePickerController, didSelectAssets: [DKAsset]) {
        //tap to select asset
        //use this place for asset selection customisation
        print("didClickAsset for selection")
    }
    
    override func imagePickerController(_ imagePickerController: DKImagePickerController, didDeselectAssets: [DKAsset]) {
        //tap to deselect asset
        //use this place for asset deselection customisation
        print("didClickAsset for deselection")
    }
}
extension Data {
    func getAVAsset() -> AVAsset {
        let directory = NSTemporaryDirectory()
        let fileName = "\(NSUUID().uuidString).mov"
        let fullURL = NSURL.fileURL(withPathComponents: [directory, fileName])
        try! self.write(to: fullURL!)
        let asset = AVAsset(url: fullURL!)
        return asset
    }
}
