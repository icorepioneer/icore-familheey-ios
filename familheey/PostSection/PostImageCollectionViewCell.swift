//
//  PostImageCollectionViewCell.swift
//  familheey
//
//  Created by Innovation Incubator on 22/11/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class PostImageCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imgOfPlayVideo: UIImageView!
    @IBOutlet weak var viewOfVideoBg: UIView!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var imgOfProgress: UIImageView!
    @IBOutlet weak var imgOfPostItems: UIImageView!
    
    @IBOutlet weak var imgClose: UIImageView!
    
}
