//
//  NewInviteMemberViewController.swift
//  familheey
//
//  Created by familheey on 13/09/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit
import Moya
import Alamofire
import SwiftyJSON

class selectedInvitiesCollectionViewCell: UICollectionViewCell{
    @IBOutlet weak var imgSelectedProfile: UIImageView!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var lblUserName: UILabel!
    
}

class inviteMemberNewTableViewCell: UITableViewCell{
    @IBOutlet weak var imgPeople: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var btnViewProfile: UIButton!
    @IBOutlet weak var imgSelection: UIImageView!
    @IBOutlet weak var btnSelected: UIButton!
    @IBOutlet weak var lblStatus: UILabel!
    
}

class NewInviteMemberViewController: UIViewController,popUpDelegate {
    
    @IBOutlet weak var lblPeople: UILabel!
    @IBOutlet weak var imgPeople: UIImageView!
    @IBOutlet weak var btnPeople: UIButton!
    
    @IBOutlet weak var lblOther: UILabel!
    @IBOutlet weak var imgOther: UIImageView!
    @IBOutlet weak var btnOther: UIButton!
    
    @IBOutlet weak var tblListView: UITableView!
    
    @IBOutlet weak var heightOf_search: NSLayoutConstraint!
    @IBOutlet weak var viewOfSearch: UIView!
    @IBOutlet weak var btnSearchReset: UIButton!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var clctnSelectedPeople: UICollectionView!
    
    @IBOutlet weak var viewOfOther: UIView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var phoneCodeField         : UITextField!
    var countryCode = ""

    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    var collectionViewLayout = UICollectionViewFlowLayout()
    
    var resultArr : [listMemberResult]?
    var usersList = [SearchResultUser]()
    var selectedIds = NSMutableArray()
    var ArrayForCltn = [SearchResultUser]()
    
    var groupId = ""
    var isFromGlobalSearch = false
    var selectedIndex = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtSearch.delegate = self
        self.callSearchApi()
        
        self.updateUI()
        clctnSelectedPeople.delegate = self
        clctnSelectedPeople.dataSource = self
        
        Helpers.setleftView(textfield: txtName, customWidth: 15)
               Helpers.setleftView(textfield: txtPhone, customWidth: 15)
               Helpers.setleftView(textfield: txtEmail, customWidth: 15)
    }
   
    
    func updateUI(){
        if ArrayForCltn.count > 0{
            heightOf_search.constant = 140
            clctnSelectedPeople.isHidden = false
        }
        else{
            heightOf_search.constant = 60
            clctnSelectedPeople.isHidden = true
        }
    }
    
    
    //MARK:- Button Actions
    @IBAction func onClickTabSelectAction(_ sender: UIButton) {
        if sender.tag == 100{
            selectedIndex = 0
            imgOther.backgroundColor  = UIColor(named: "lightGrayTextColor")
            imgPeople.backgroundColor  = UIColor(named: "greenBackgrounf")
            
            lblPeople.textColor = UIColor(named: "greenBackgrounf")
            lblOther.textColor = .black
            
            tblListView.isHidden = false
            viewOfSearch.isHidden = false
            viewOfOther.isHidden = true
            
            callSearchApi()
        }
        else{
            selectedIndex = 1
            imgOther.backgroundColor  = UIColor(named: "greenBackgrounf")
            imgPeople.backgroundColor  = UIColor(named: "lightGrayTextColor")
            
            lblOther.textColor = UIColor(named: "greenBackgrounf")
            lblPeople.textColor = .black
            
            tblListView.isHidden = true
            viewOfSearch.isHidden = true
            viewOfOther.isHidden = false
            
            let currentLocale = NSLocale.current as NSLocale
            let countryCode = currentLocale.object(forKey: .countryCode) as! String//get the set country name, code of your iphone
            let countryName = currentLocale.displayName(forKey: NSLocale.Key.countryCode, value: countryCode)
            
            print("country code is \(countryCode)")
            print(ViewController.getCountryCallingCode(countryRegionCode: countryCode))
            
            self.countryCode = ViewController.getCountryCallingCode(countryRegionCode: countryCode)
            
            if self.countryCode.count > 0{
//                onCodeButt.setTitle(countryName, for: .normal)
                phoneCodeField.text = self.countryCode
//                ViewController.getCountryCallingCode(countryRegionCode: countryCode)
            }
        }
    }

    @IBAction func onClickProfileAction(_ sender: UIButton) {
         let temp = usersList[sender.tag]
        /* let storyboard = UIStoryboard.init(name: "second", bundle: nil)
         let addMember = storyboard.instantiateViewController(withIdentifier: "UserProfileTableViewController") as! UserProfileTableViewController
         addMember.userID = "\(temp.userid!)"
         self.navigationController?.pushViewController(addMember, animated: true)*/
        let stryboard = UIStoryboard.init(name: "InviteMember", bundle: nil)
        let vc = stryboard.instantiateViewController(withIdentifier: "userProfilePopupViewController") as! userProfilePopupViewController
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.userID = "\(temp.userid!)"
//        let navigationController = UINavigationController(rootViewController: vc)
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func onClickSelectionAction(_ sender: UIButton) {
        let temp = usersList[sender.tag]
        print(temp)
        let strId = "\(temp.userid!)"
        if selectedIds.contains(temp.userid!){
            selectedIds.remove(temp.userid!)
            for (i, item) in ArrayForCltn.enumerated() {
                if item.userid == temp.userid{
                    ArrayForCltn.remove(at: i)
                    break
                }
            }
        }
        else{
            selectedIds.add(temp.userid!)
//            ArrayForCltn.add(temp)
            ArrayForCltn.append(temp)
        }
 
        self.updateUI()
        
//        tblListView.beginUpdates()
//        tblListView.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .none)
        tblListView.reloadData()
//        tblListView.endUpdates()
        tblListView.layer.removeAllAnimations()
        clctnSelectedPeople.reloadData()
        
    }
    
    @IBAction func onClickSearchReset(_ sender: Any) {
        txtSearch.text = ""
        self.btnSearchReset.isHidden = true
        self.callSearchApi()
        self.txtSearch.endEditing(true)
    }
    
    @IBAction func onClickDeleteSelectedAction(_ sender: UIButton) {
        let tAny = ArrayForCltn[sender.tag]
        print(tAny)
        let strId = "\(tAny.userid!)"
        if selectedIds.contains(tAny.userid!){
            selectedIds.remove(tAny.userid!)
            ArrayForCltn.remove(at: sender.tag)
        }
        self.updateUI()
        tblListView.reloadData()
        clctnSelectedPeople.reloadData()
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickSendInvite(_ sender: UIButton) {
        if selectedIndex == 1{
            self.inviteOtherContact()
        }
        else{
            APIServiceManager.callServer.sendMemberInvitationArray(url: EndPoint.inviteMember, fromId: UserDefaults.standard.value(forKey: "userId") as! String, toId:selectedIds, groupId: groupId, success: { (responseMdl) in
                
                guard let result = responseMdl as? inviteMemberResult else{
                    return
                }
                ActivityIndicatorView.hiding()
                if result.statuscode == 200{
                    
                    self.displayAlertChoice(alertStr: "Invitation sent successfully", title: "") { (result) in
                        self.navigationController?.popViewController(animated: true)
                    }
                    // self.displayAlert(alertStr: "Request Send", title: "")
                }
                else{
                    self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                }
                
            }) { (error) in
                ActivityIndicatorView.hiding()
            }
        }
    }
    
    
    //MARK:- WEB Services
    func getAllMembers(groupId:String){
        APIServiceManager.callServer.getAllMembersList(url: EndPoint.listMembers, groupId: groupId, userId: UserDefaults.standard.value(forKey: "userId") as! String, query: "", success: { (responseMdl) in
            
            guard let result = responseMdl as? listAllResult else{
                return
            }
            ActivityIndicatorView.hiding()
            if result.statuscode == 200{
                self.resultArr = result.result
                
                if self.resultArr!.isEmpty {
                    self.tblListView.isHidden = true
                } else {
                    self.tblListView.isHidden = false
                    self.tblListView.delegate = self
                    self.tblListView.dataSource = self
                    self.tblListView.reloadData()
                }
            }
            
        }) { (error) in
            
        }
    }
    
    func callSearchApi(){
        isFromGlobalSearch = true
        let param = [
            "userid" : UserDefaults.standard.value(forKey: "userId") as! String,
            "searchtxt" : txtSearch.text!,
            "offset":"0",
            "limit":"100",
            "phonenumbers":[],
            "type":"users",
            "group_id":self.groupId
            ] as [String : Any]
        print(param)
        ActivityIndicatorView.show("Please wait!")
        APIServiceManager.callServer.searchRequest(url: EndPoint.search, param: param, fromSearchTabViewController: false, success: { (response) in
            
            ActivityIndicatorView.hiding()
            self.usersList = [SearchResultUser]()
            
            
            if let results = response as! SearchList?{
                self.usersList = results.searchResult!
                
            }
            
            if self.usersList.isEmpty {
                
                self.tblListView.isHidden = true
            } else {
                self.tblListView.isHidden = false
                self.tblListView.delegate = self
                self.tblListView.dataSource = self
                self.tblListView.reloadData()
            }
            
            
        }) { (error) in
            ActivityIndicatorView.hiding()
            self.usersList = [SearchResultUser]()
            //self.FamilyList = [SearchResultGroup]()
            
            if self.usersList.isEmpty {
                
                self.tblListView.isHidden = true
            } else {
                
                self.tblListView.reloadData()
            }
        }
    }
    
    func inviteOtherContact(){
        if txtName.text == ""{
            
            Helpers.showAlertDialog(message: "Please enter contact's name", target: self)
            
        }
        else if self.phoneCodeField.text == ""{
            
            Helpers.showAlertDialog(message: "Please enter contact's country code", target: self)
            
        }
        else if txtPhone.text == ""{
            
            Helpers.showAlertDialog(message: "Please enter contact's phone number", target: self)
            
        }else if txtEmail.text == ""{
            
            Helpers.showAlertDialog(message: "Please enter contact's email", target: self)
            
        }else if !Helpers.validateEmail(enteredEmail: txtEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines)){
            
            Helpers.showAlertDialog(message: "Please verify email address", target: self)
        }
      
        else{
            let tempStr = txtEmail.text!.prefix(1)
            if !String(tempStr).isAlphanumeric{
                self.displayAlert(alertStr: "Please start email with a alphanumeric characters", title: "")
                return
            }
            var phone = ""
            phone = self.phoneCodeField.text! + self.txtPhone.text!
            
            if !phone.isPhoneNumber{
                print("not valid")
                Helpers.showAlertDialog(message: "Please enter a valid  phone number", target: self)
                return
            }

            APIServiceManager.callServer.inviteMailorPhone(url: EndPoint.inviteViaMsg, email: txtEmail.text!, phone: phone, userId: UserDefaults.standard.value(forKey: "userId") as! String, groupId:groupId, urlEncode: "", name: txtName.text!, fromName: UserDefaults.standard.value(forKey: "user_fullname") as! String, success: { (responseMdl) in
                
                guard let result = responseMdl as? requestSuccessModel else{
                    return
                }
                ActivityIndicatorView.hiding()
                if result.status_code == 200{

                    self.displayAlertChoice(alertStr: "Invitation send", title: "Success", completion: { (result) in
                        self.navigationController?.popViewController(animated: true)
                    })
                }
            }) { (error) in
                ActivityIndicatorView.hiding()
            }
        }
    }

    @IBAction func onClickCodeSelection(_ sender: Any) {
          self.view.endEditing(true)
          
          let popUp = popupTableViewController()
          self.addChild(popUp)
          // popUp.frmGndrFlag = fromGndr
          popUp.view.frame = UIScreen.main.bounds
          self.view.addSubview(popUp.view)
          popUp.delegate = self
          self.view.bringSubviewToFront(popUp.view)
      }
      
      
      //MARK:- Protocol PopupDelegate
      func selectCountry(id: String, name: String) {
          
         // txtPhone.text = id
          //        onCodeButt.setTitle(name, for: .normal)
          //
          //        txtFieldUnderline.isHidden = false
          
          self.countryCode = id
          self.phoneCodeField.text = countryCode

          DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
             // self.txtCode.becomeFirstResponder()
          })
      }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension NewInviteMemberViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usersList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "inviteMemberNewTableViewCell", for: indexPath) as! inviteMemberNewTableViewCell
        
        cell.btnSelected.tag = indexPath.row
        cell.btnViewProfile.tag = indexPath.row
        
        
        if self.isFromGlobalSearch{
            cell.lblName.text = usersList[indexPath.row].full_name
            cell.lblLocation.text = usersList[indexPath.row].origin
            cell.selectionStyle  = .none
            
            if usersList[indexPath.row].propic?.count != 0{
                let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+usersList[indexPath.row].propic!
                let imgUrl = URL(string: temp)
                cell.imgPeople.kf.indicatorType = .activity
                let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+imgUrl!.relativeString
                let url = URL(string: newUrlStr)
                
                cell.imgPeople.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
            }
            else{
                cell.imgPeople.image = #imageLiteral(resourceName: "Male Colored")
            }
            
            let tempCount = usersList[indexPath.row].type
            let inviteStatus = usersList[indexPath.row].status
            let exist = usersList[indexPath.row].exist
            
            if exist == 0{
                if inviteStatus == 1{
                    cell.lblStatus.isHidden = false
                    cell.lblStatus.text = "Pending"
                    cell.btnSelected.isUserInteractionEnabled = false
                }
                else{
                    cell.lblStatus.isHidden = true
                    cell.btnSelected.isUserInteractionEnabled = true
                }
            }
            else{
                if tempCount.lowercased() == "member"{
                    cell.lblStatus.isHidden = false
                    cell.lblStatus.text = "MEMBER"
                    cell.btnSelected.isUserInteractionEnabled = false
                }
                else if tempCount.lowercased() == "pending"{
                    cell.lblStatus.isHidden = false
                    cell.lblStatus.text = "Pending"
                    cell.btnSelected.isUserInteractionEnabled = false
                }
                else if inviteStatus == 1{
                    cell.lblStatus.isHidden = false
                    cell.lblStatus.text = "Pending"
                    cell.btnSelected.isUserInteractionEnabled = false
                }
                else{
                    cell.lblStatus.isHidden = true
                    cell.btnSelected.isUserInteractionEnabled = true
                }
            }

            
            if selectedIds.contains(usersList[indexPath.row].userid!){
                cell.imgSelection.image  = #imageLiteral(resourceName: "Green_tick.png")
            }
            else{
                cell.imgSelection.image  = #imageLiteral(resourceName: "gray_circle.png")
            }
        }
        else{
            cell.lblName.text = resultArr?[indexPath.row].fullname
            cell.lblLocation.text = resultArr?[indexPath.row].location
            
            if resultArr?[indexPath.row].photo.count != 0{
                let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+(resultArr?[indexPath.row].photo)!
                let imgUrl = URL(string: temp)
                cell.imgPeople.kf.indicatorType = .activity
                let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+imgUrl!.relativeString
                let url = URL(string: newUrlStr)
                
                cell.imgPeople.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
            }
            else{
                cell.imgPeople.image = #imageLiteral(resourceName: "Male Colored")
            }
            
            
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension NewInviteMemberViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ArrayForCltn.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "selectedInvitiesCollectionViewCell", for: indexPath as IndexPath) as! selectedInvitiesCollectionViewCell
        cell.btnDelete.tag = indexPath.row
        cell.btnProfile.tag = indexPath.row
        
//        let user = ArrayForCltn[indexPath.row] as! [SearchResultUser]
        cell.lblUserName.text = ArrayForCltn[indexPath.row].full_name
        if ArrayForCltn[indexPath.row].propic?.count != 0{
            let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+ArrayForCltn[indexPath.row].propic!
            let imgUrl = URL(string: temp)
            cell.imgSelectedProfile.kf.indicatorType = .activity
            let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+imgUrl!.relativeString
            let url = URL(string: newUrlStr)

            cell.imgSelectedProfile.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
        }
        else{
            cell.imgSelectedProfile.image = #imageLiteral(resourceName: "Male Colored")
        }
//        cell.imgSelectedProfile.backgroundColor = .red
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 70, height: 70)
    }
    
}
extension NewInviteMemberViewController:UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        if textField == txtSearch
        {
            // selectWebAPI()
            if textField.text!.count > 0{
                btnSearchReset.isHidden = false
            }
            else{
                btnSearchReset.isHidden = true
            }
            textField.endEditing(true)
        }
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        self.view.endEditing(true)
        if textField == txtSearch
        {
            if textField.text!.count > 0{
                btnSearchReset.isHidden = false
                callSearchApi()
            }
            else{
                btnSearchReset.isHidden = true
                getAllMembers(groupId: self.groupId)
            }
            
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
        }
        return  true
    }
}

