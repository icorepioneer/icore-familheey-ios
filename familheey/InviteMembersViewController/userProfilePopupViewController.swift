//
//  userProfilePopupViewController.swift
//  familheey
//
//  Created by familheey on 15/09/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit
import Moya
import Alamofire
import SwiftyJSON

class userProfilePopupViewController: UIViewController {
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    
    @IBOutlet weak var viewFamily: UIView!
    @IBOutlet weak var lblFamilyCount: UILabel!
    
    @IBOutlet weak var viewOfMutalConnection: UIView!
    @IBOutlet weak var lblMutlConnnection: UILabel!
    
    @IBOutlet weak var viewOfMutalFamilies: UIView!
    @IBOutlet weak var lblMutalFamilyCount: UILabel!
    
    @IBOutlet weak var txtIntroText: UITextView!
    @IBOutlet weak var txtWorkText: UITextView!
    
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    
    var userID = ""
    var userDetails : ExternalUserModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !userID.isEmpty{
            getUserDetails(UId: userID)
        }
        
    }
    
    //MARK:- Web API
    func getUserDetails(UId:String){
        if !Connectivity.isConnectedToInternet(){
            self.displayAlertChoice(alertStr: "No Internet! Please Check your connectivity", title: "") { (result) in
                self.getUserDetails(UId: self.userID)
            }
        }
        
        let crntUser = UserDefaults.standard.value(forKey: "userId") as! String
        ActivityIndicatorView.show("Please wait!")
        APIServiceManager.callServer.getUserDetails(url: EndPoint.userDetails, userId: crntUser, profileId: UId ,success: { (response) in
            
            ActivityIndicatorView.hiding()
            
            
            guard let loginMdl = response as? ExternalUserModel else{
                return
            }
            print(loginMdl)
            if loginMdl.status_code == 200{
                
                self.userDetails = loginMdl
                
                if (self.userDetails?.count!.mutualConnections)! > 0 {
                    let count = self.userDetails?.count?.connections
                    self.lblMutlConnnection.text = "\(count!)"
                }
                else{
                    self.lblMutlConnnection.text = "0"
                }
                
                if (self.userDetails?.count!.familyCount)! > 0 {
                    let count = self.userDetails?.count?.familyCount
                    self.lblFamilyCount.text = "\(count!)"
                }
                else{
                    self.lblFamilyCount.text = "0"
                }
                
                if (self.userDetails?.count!.mutalFamilies)! > 0 {
                    let count = self.userDetails?.count?.mutalFamilies
                    self.lblMutalFamilyCount.text = "\(count!)"
                }
                else{
                    self.lblMutalFamilyCount.text = "0"
                }
                
                self.lblName.text = self.userDetails?.User?.fullname
                self.lblLocation.text = self.userDetails?.User?.location
                
                if (self.userDetails?.User?.about.isEmpty)!{
                    self.txtIntroText.text = "Not added"
                    self.txtIntroText.textColor = .darkGray
                }
                else{
                    self.txtIntroText.text = self.userDetails?.User?.about
                    self.txtIntroText.textColor = .black
                }
                
                if (self.userDetails?.User?.work.isEmpty)!{
                    self.txtWorkText.text = "Not added"
                    self.txtWorkText.textColor = .darkGray
                }
                else{
                    self.txtWorkText.text = self.userDetails?.User?.work
                    self.txtWorkText.textColor = .black
                }
                
                if self.userDetails?.User?.propic.count != 0 {
                    let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+(self.userDetails?.User!.propic)!
                    let imgUrl = URL(string: temp)
                    
                    let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+imgUrl!.relativeString
                    let urlImg = URL(string: newUrlStr)
                    self.imgProfile.kf.indicatorType = .activity
                    //            imageViewUserAvatar.kf.setImage(with: urlImg, placeholder: #imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
                    self.imgProfile.kf.setImage(with: urlImg, placeholder: #imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
                    
                }
                else{
                    self.imgProfile.image = #imageLiteral(resourceName: "Male Colored")
                }
            }
            else{
                ActivityIndicatorView.hiding()
            }
            
        }) { (error) in
            
            ActivityIndicatorView.hiding()
            
        }
        
    }
    
    
    //MARK:- Button Actions
    @IBAction func onClickBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
