//
//  TopMenuBar.swift
//  CustomTopBar
//
//  Created by George on 11/20/18.
//  Copyright © 2018 Georgekutty. All rights reserved.
//

import UIKit

@objc protocol TopMenuBarDelegates {
    @objc optional func TopMenuSelectedIndex(indexValue:Int)
}

class TopMenuBar: UIView, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
  
    let cellId = "menuCellId"
    var menuNames:[String] = []
    var SelectedIndex:Int = 0
    weak var delegate:TopMenuBarDelegates?

    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
         layout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.showsHorizontalScrollIndicator = false
        cv.bounces = false
        cv.backgroundColor = UIColor.rgb(red: 255, green: 255 , blue: 255)
        cv.dataSource = self
        cv.delegate = self
    
        return cv
    }()
    
    
    required init(menuArray:[String]) {
        super.init(frame: .zero)
        self.menuNames = menuArray
        setupMenuView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    func setupMenuView()  {

        collectionView.register(UINib(nibName: "MenuCellCollectionViewCell", bundle: nil), forCellWithReuseIdentifier:cellId)
        addSubview(collectionView)
        addConstraintsWithFormat("H:|[v0]|", views: collectionView)
        addConstraintsWithFormat("V:|[v0]|", views: collectionView)
        let selectedIndexPath = IndexPath(item: 0, section: 0)
        collectionView.selectItem(at: selectedIndexPath, animated: false, scrollPosition: .bottom)
    }
    
     func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuNames.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! MenuCellCollectionViewCell
        cell.lblMenuTitle.text = menuNames[indexPath.row]
        cell.imgBoarder.backgroundColor = UIColor.clear
        cell.lblMenuTitle.textColor = UIColor.black
        if SelectedIndex == indexPath.row {
             cell.imgBoarder.backgroundColor = UIColor.black
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
      //  let width = menuNames[indexPath.row].width(withConstrainedHeight: 50, font: customFont.mediumFontOfSize15());
        
        let size = menuNames[indexPath.row].size(withAttributes: [.font:UIFont.systemFont(ofSize: 15)])
        let width = size.width

        return CGSize(width: width + 10, height: frame.height);
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 30
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 30
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        collectionView.selectItem(at: indexPath, animated: true, scrollPosition: UICollectionView.ScrollPosition.centeredHorizontally)
        SelectedIndex = indexPath.row
        collectionView.reloadData()
        delegate?.TopMenuSelectedIndex!(indexValue: indexPath.row)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

