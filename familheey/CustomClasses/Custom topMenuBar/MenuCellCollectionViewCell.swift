//
//  MenuCellCollectionViewCell.swift
//  CustomTopBar
//
//  Created by George on 11/21/18.
//  Copyright © 2018 Georgekutty. All rights reserved.
//

import UIKit

class MenuCellCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblMenuTitle: UILabel!
    @IBOutlet weak var imgBoarder: RoundableImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        self.lblMenuTitle.numberOfLines = 1
       // self.lblMenuTitle.font = customFont.mediumFontOfSize15()
        self.lblMenuTitle.font = UIFont.systemFont(ofSize: 15)
    }

}
