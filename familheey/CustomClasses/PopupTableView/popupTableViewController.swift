//
//  popupTableViewController.swift
//  familheey
//
//  Created by familheey on 03/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

protocol popUpDelegate {
    func selectCountry(id:String,name:String)
}

class popupTableViewController: BaseClassViewController {
    @IBOutlet weak var cntlBack: UIControl!
    @IBOutlet weak var viewForeground: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tblListView: UITableView!
    @IBOutlet weak var txtSearchBar: UISearchBar!
    
    @IBOutlet weak var btnSearchReset: UIButton!
    var search = ""
    var cArr:[[String:String]] = []
    var searchArr:NSMutableArray!
    //var cArr:NSMutableArray!
    
    var delegate : popUpDelegate?

    //MARK:- ViewController load
    override func onScreenLoad() {
    
       // cArr = []
        searchArr = []
        getCountryList()
//        Helpers.setleftView(textfield: txtSearch, customWidth: 30)
        
        tblListView.register(UINib(nibName: "sideLabelTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        tblListView.delegate = self
        tblListView.dataSource = self
        txtSearch.delegate = self
//        txtSearchBar.delegate = self
    }
    
    //MARK:- BackButton
    override func isBackBackButtonRequired() -> Bool {
        return false
    }

    @IBAction func onClickSearchReset(_ sender: Any) {
        txtSearch.text = ""
        search = ""
        self.btnSearchReset.isHidden = true
        
        searchArr.removeAllObjects()
        searchArr.addObjects(from: cArr)
        tblListView.reloadData()
    }
    
    @IBAction func onClickCntrl(_ sender: Any) {
        self.view.removeFromSuperview()
//        self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func onClickBack(_ sender: Any) {
//        self.navigationController?.popViewController(animated: true)
        self.view.removeFromSuperview()
    }
    func getCountryList(){
        if let path = Bundle.main.path(forResource: "countryPhoneCode", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                
                let countryModel = try countryResponseModel(jsonResult as! [String : Any])
                
                if countryModel.countryList!.count > 0{
                    for index in countryModel.countryList!{
                        let tempName = index.name as String
                        let tempCode = index.code as String
                        
                        let tempDic = NSMutableDictionary()
                        tempDic.setValue(tempName, forKey: "name")
                        tempDic.setValue(tempCode, forKey: "code")
                        cArr.append(tempDic as! [String : String])
                    }
                    searchArr.addObjects(from: cArr)
                    tblListView.reloadData()
                
                }
            }
            catch let error {
                print(error.localizedDescription)
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension popupTableViewController:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchArr.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellId = "cell"

        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! sideLabelTableViewCell
    
        let tempDic = searchArr[indexPath.row] as! NSDictionary
        cell.lblName.text = tempDic.value(forKey: "name") as? String
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let tempDic = searchArr[indexPath.row] as! NSDictionary
        
        self.delegate?.selectCountry(id: tempDic.value(forKey: "code") as! String, name:tempDic.value(forKey: "name") as! String )
        self.view.removeFromSuperview()
    }
    
}

extension popupTableViewController:UITextFieldDelegate{
//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//         if searchText.count > 0{
//            let namePredicate = NSPredicate(format: "name contains[c] %@", searchText.lowercased())
//            let filteredArray = cArr.filter { namePredicate.evaluate(with: $0) }
//
//            print(filteredArray)
//            searchArr.removeAllObjects()
//            searchArr.addObjects(from: filteredArray)
//        }
//        else{
//            searchArr.removeAllObjects()
//            searchArr.addObjects(from: cArr)
//        }
//        tblListView.reloadData()
//    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        if string.isEmpty{
            search = String(search.dropLast())

        }
        else{
            search=textField.text!+string
        }

        print(search)
//        let predicate=NSPredicate(format: "SELF.name CONTAINS[cd] %@", search)
        if search.isEmpty{
            btnSearchReset.isHidden = true
            searchArr.removeAllObjects()
            searchArr.addObjects(from: cArr)
        }
        else{
            btnSearchReset.isHidden = false
            let namePredicate = NSPredicate(format: "name contains[c] %@", search.lowercased())
                let filteredArray = cArr.filter { namePredicate.evaluate(with: $0) }
            
            print(filteredArray)
            searchArr.removeAllObjects()
            searchArr.addObjects(from: filteredArray)
        }
        
        tblListView.reloadData()
        return true
    }
}
