//
//  displayAlert.swift
//  lumiere
//
//  Created by Panzer Tech on 30/5/19.
//  Copyright © 2019 PanzerTech. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{
    // MARK:- Alert
    func displayAlert(alertStr : String , title : String) {
        let alert = UIAlertController(title: title, message: alertStr, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK:- Alert OK with Control
    func displayAlertChoice(alertStr : String , title : String , completion: @escaping (_ result: String)->())
    {
        let alertController = UIAlertController(title: title, message: alertStr, preferredStyle:.alert)
        let yesPressed = UIAlertAction(title: "OK", style: .default, handler: { (action) in
            
            completion("0")
            
        })
        alertController.addAction(yesPressed)
        present(alertController, animated: true, completion: nil)
    }
    
    // MARK:- Alert Yes nd No with Control
    func alertChoiceDual(alertStr : String , title : String , completion: @escaping (_ result: String)->())
    {
        let alertController = UIAlertController(title: title, message: alertStr, preferredStyle:.alert)
        let yesPressed = UIAlertAction(title: "Yes", style: .default, handler: { (action) in
            
            completion("0")
            
        })
        alertController.addAction(yesPressed)
        alertController.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.cancel, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
    
    //MARK:- Show action with array
    func showActionSheet(titleArr:NSArray, title:String, completion: @escaping (_ result: Int)->()){
        let alert = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
        
        let closure = { (index: Int) in
        { (action: UIAlertAction!) -> Void in
            //print("Index: \(index)")
            completion(index)
            }
        }
        
        for i in 0..<titleArr.count{
           // let temp = titleArr[i] as! NSDictionary
            
            alert.addAction(UIAlertAction(title: titleArr[i] as? String, style: .default, handler: closure(i)))
        }
        
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler:
        
        closure(100)))
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK:- action sheet with dictionary
    func showActionSheetDictionary(titleArr:NSArray, completion: @escaping (_ result: Int)->()){
        let alert = UIAlertController(title: "Choose Option", message: nil, preferredStyle: .actionSheet)
        
        let closure = { (index: Int) in
        { (action: UIAlertAction!) -> Void in
            //print("Index: \(index)")
            completion(index)
            }
        }
        
        for i in 0..<titleArr.count{
            let temp = titleArr[i] as! NSDictionary
            
            alert.addAction(UIAlertAction(title: temp.value(forKey: "key") as? String, style: .default, handler: closure(i)))
        }
        
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK:- Alert controller with text
    func showAlertController(completion: @escaping (_ result: String)->()){
        let alert = UIAlertController(title: "Enter email or phone", message: "Enter phone number with code", preferredStyle: .alert)
        
        alert.setValue(NSAttributedString(string: alert.message!, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 10, weight: UIFont.Weight.medium), NSAttributedString.Key.foregroundColor : UIColor.lightGray]), forKey: "attributedMessage")
        
        //2. Add the text field. You can configure it however you need.
        alert.addTextField { (textField) in
            textField.placeholder = "email"
        }
        
        alert.addTextField { (textFieldPass) in
            textFieldPass.placeholder = "phone"
            textFieldPass.isSecureTextEntry = false
        }
        
        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { [weak alert] (_) in
            print("")
        }))
        
        alert.addAction(UIAlertAction(title: "Done", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0]
            let textFieldPass = alert?.textFields![1]
            
            completion("\(String(describing: textField?.text)) \(String(describing: textFieldPass?.text))")
        }))
        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
    }
    
}
