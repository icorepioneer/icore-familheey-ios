//
//  defaultValue.swift
//  lumiere
//
//  Created by Panzer Tech on 11/6/19.
//  Copyright © 2019 PanzerTech. All rights reserved.
//

import Foundation

class setUserDefaultValues {
    class func setUserDefaultValue(userDefaultValue : String , userDefaultKey : String)
    {
        let defaults = UserDefaults.standard
        defaults.set(userDefaultValue, forKey: userDefaultKey)
    }
    
    class func setUserDefaultValueWithName(name:String,userDefaultValue : String , userDefaultKey : String){
        let defaults = UserDefaults.init(suiteName: name)
        defaults?.set(userDefaultValue, forKey: userDefaultKey)
    }
    
    class func setUserDefaultValueDataWithName(name:String,userDefaultValue : NSData , userDefaultKey : String){
        let defaults = UserDefaults.init(suiteName: name)
        defaults?.set(userDefaultValue, forKey: userDefaultKey)
    }
    
    class func setUserDefaultLatLongWithName(name:String, itemValue:Double, itemKey:String){
        let defaults = UserDefaults.init(suiteName: name)
        defaults?.set(itemValue, forKey: itemKey)
    }
}
