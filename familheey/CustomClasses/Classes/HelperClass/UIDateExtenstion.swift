//
//  UIDateExtenstion.swift
//  RevitsOne
//
//  Created by Innovation Incubator on 21/08/19.
//  Copyright © 2019 Innovation Incubator. All rights reserved.
//

import Foundation

extension Date {
    
    static func getDates(forLastNDays nDays: Int) -> [String] {
        
        let cal = NSCalendar.current
        // start with today
        var date = cal.startOfDay(for: Date())
        
        var arrDates = [String]()
        
        for _ in 1 ... nDays {
            // move back in time by one day:
            date = cal.date(byAdding: Calendar.Component.day, value: -1, to: date)!
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let dateString = dateFormatter.string(from: date)
            arrDates.append(dateString)
        }
        
        return arrDates
    }
    
    static func timeStampToString(dateStr: String) -> String {
        
        let date = Date(timeIntervalSince1970: Double(dateStr) as! TimeInterval)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let finalDateStr = dateFormatter.string(from: date)
        
        return finalDateStr
    }
    
    static func getDateWithMonString(dateStr: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = dateFormatter.date(from: dateStr)
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let finalDateStr = dateFormatter.string(from: date!)
        
        return finalDateStr
    }
    static func longStr2ToDate(dateSend: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = dateFormatter.date(from: dateSend)
        dateFormatter.dateFormat = "MM-dd-yyyy"
        let dateStr = dateFormatter.string(from: date!)
        dateFormatter.dateFormat = "h:mm a"
        let timeStr = dateFormatter.string(from: date!)
        
        return "\(dateStr) at \(timeStr)"
    }
    
    static func longStrToDate(dateSend: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let date = dateFormatter.date(from: dateSend)
        dateFormatter.dateFormat = "MM-dd-yyyy"
        let dateStr = dateFormatter.string(from: date!)
        dateFormatter.dateFormat = "h:mm a"
        let timeStr = dateFormatter.string(from: date!)
        
        return "\(dateStr) at \(timeStr)"
    }
    
    static func secondsToHoursMinutesSeconds (seconds : String) -> String {
        let secondsInt:Int! = Int(seconds)
        
        let (h, m, s) = (secondsInt / 3600, (secondsInt % 3600) / 60, (secondsInt % 3600) % 60)
        if h == 0 && m == 0 && s == 0
        {
            return  "0 Hr"
            
        }
        else  if h == 0 && m != 0 && s != 0
        {
            return  "\(m) Min, \(s) Sec"
            
        }
        else  if h == 0 && m == 0 && s != 0
        {
            return  "\(s) Sec"
            
        }
        else  if h != 0 && m != 0 && s == 0
        {
            return  "\(h) Hr, \(m) Min"
            
        }
        else  if h != 0 && m == 0 && s == 0
        {
            return  "\(h) Hr"
            
        }
        else{
            
            return  "\(h) Hr, \(m) Min, \(s) Sec"
        }
    }
}
