//
//  Helpers.swift
//  RevitsoneSample
//
//  Created by Innovation Incubator on 14/05/19.
//  Copyright © 2019 Innovation Incubator. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Moya
import Photos

class subclassedUIButton: UIButton {
    var indexPath       : Int?
    var indexSection    : Int?
}
struct isFromCreation{
    static let myfamily = "myfamily"
    static let publicfeed = "publicfeed"
    static let request = "request"
}

class Helpers: NSObject {
    
    static var networkProvider = MoyaProvider<FamilyheeyApi>()
    static var navVC = UINavigationController()
    typealias RefreshCompletion = ( _ accessToken: String?) -> Void
    
    
    static var imageURl: String {
        
        switch NetworkManager.environment {
            //            https://s3.eu-west-3.amazonaws.com/revitsone-qa/"
        //        case .production: return "https://prod-familheey.s3.amazonaws.com/"
        case .production: return  appDel.s3BaseUrl            // "https://prod.familheey.com/"
        case .qa: return appDel.s3BaseUrl                     // "https://familheey.s3.amazonaws.com/"
        case .development: return appDel.s3BaseUrl            // "https://familheey.s3.amazonaws.com/"
        case .staging: return appDel.s3BaseUrl                // "https://familheey-prod.s3.amazonaws.com/"
        case .conversation : return appDel.s3BaseUrl          // "https://prod.familheey.com/"
        case .preProduction: return appDel.s3BaseUrl          // "https://prod.familheey.com/"
        case .preProdConversation: return appDel.s3BaseUrl    // "https://prod.familheey.com/"
        }
    }
    //https://prod.familheey.com/
    static var imageUploadUrl: String {
        
        switch NetworkManager.environment {
            
        //        case .production: return "https://prod-commonbackend.familheey.com/api/v1/familheey/"
        case .production: return "https://commonbackend.familheey.com/api/v1/familheey/"
            
        case .preProduction: return "https://preprod-commonbackend.familheey.com/api/v1/familheey/"
            
        case .qa: return "https://5eeeea60b893.ngrok.io/api/v1/familheey/"
            
        case .development: return "https://dev-commonbackend.familheey.com/api/v1/familheey/"
            
        case .staging: return "https://staging-commonbackend.familheey.com/api/v1/familheey/"
            
        case .conversation: return "https://socket-4000.familheey.com/api/v1/familheey/"
            
        case .preProdConversation: return "https://preprod-socket-4000.familheey.com/api/v1/familheey/"
            
        }
    }
    
    static var notificationUploadstring: String {
        
        switch NetworkManager.environment {
            
        case .production: return "prod_"
            
        case .preProduction:return "prod_"
            
        case .qa: return "dev_"
            
        case .development: return "dev_"
            
        case .staging: return "staging_"
            
        case .conversation: return "prod_"
            
        case .preProdConversation: return "prod_"
            
        }
    }
    
    
    static var imaginaryImageBaseUrl: String {
        
        switch NetworkManager.environment {
            
        case .production: return "https://dev-imaginary.familheey.com/crop?"
            
        case .preProduction: return "https://dev-imaginary.familheey.com/crop?"
            
        case .qa: return "https://dev-imaginary.familheey.com/crop?"
            
        case .development: return "https://dev-imaginary.familheey.com/crop?"
            
        case .staging: return "https://dev-imaginary.familheey.com/crop?"
            
        case .conversation: return "https://dev-imaginary.familheey.com/crop?"
            
        case .preProdConversation: return "https://dev-imaginary.familheey.com/crop?"
            
        }
    }
    
    //MARK:- show alert
    class func showAlertDialog(message: String, target: UIViewController) {
        
        if message == "Success Alert" {
            
            //            let alert = UIAlertController(title: NSLocalizedString("Success", comment: ""), message: "Successfully submit Feedback Form", preferredStyle: UIAlertController.Style.alert)
            //            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: { _ -> Void in
            //
            //                let nextViewController = UIStoryboard(name: "Tab", bundle: nil).instantiateViewController(withIdentifier: "SideMenuControllerTab")
            //                target.present(nextViewController, animated: true, completion: nil)
            //            }))
            //
            //            target.present(alert, animated: true, completion: nil)
        } else if message == "user edited successfully" {
            
            let alert = UIAlertController(title: NSLocalizedString("Success", comment: ""), message: "Successfully edited profile", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: { _ -> Void in
                
                let nextViewController = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "SettingsNavController")
                target.present(nextViewController, animated: true, completion: nil)
            }))
            
            target.present(alert, animated: true, completion: nil)
        }
        else{
            
            let alert = UIAlertController(title: NSLocalizedString("", comment: ""), message: message, preferredStyle: UIAlertController.Style.alert)
            
            print(message)
            if message == "Token is not valid"  {
                
                alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: UIAlertAction.Style.default, handler: { _ -> Void in
                    
                    let viewcontroller = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginNavController")
                    UserDefaults.standard.set(nil, forKey: "userData")
                    UserDefaults.standard.set(nil, forKey: "companyId")
                    target.present(viewcontroller, animated: true, completion: nil)
                }))
            } else if message == "The Internet connection appears to be offline." || message == "The request timed out." {
                
                alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: UIAlertAction.Style.default, handler: { _ -> Void in
                    
                    if let _: Data = UserDefaults.standard.value(forKey: "userId") as? Data {
                        
                        
                        //                        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                        //
                        //                        let tabBar = storyboard.instantiateViewController(withIdentifier: "mainTabView") as! HomeViewTabController
                        //                        //  loginView.regType = regType
                        //                        target.present(tabBar, animated: true, completion: nil)
                        
                    } else {
                        //                        let viewcontroller = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginNavController")
                        //                        UserDefaults.standard.set(nil, forKey: "userData")
                        //                        UserDefaults.standard.set(nil, forKey: "companyId")
                        //                        target.present(viewcontroller, animated: true, completion: nil)
                    }
                }))
            }
            else {
                
                alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: UIAlertAction.Style.default, handler: {(alert: UIAlertAction!) in }))
            }
            target.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK:- validate methods
    
    class func validateEmail(enteredEmail:String) -> Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail.trimmingCharacters(in: .whitespacesAndNewlines))
    }
    
    
    class func validateText(txt:String)-> Bool{
        if txt == "" {
            
            return false
        }
        else{
            return true
        }
    }
    
    //written by remya
    class func validatePHnumber(enteredNumber:String) -> Bool {
        let phoneNumberRegex = "^[1-9]\\d{9}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneNumberRegex)
        let isValidPhone = phoneTest.evaluate(with: enteredNumber)
        return isValidPhone
        
    }
    
    
    class func setleftView(textfield:UITextField)
    {
        let leftView2                          = UILabel(frame: CGRect(x: 5, y: 0, width: 10, height: 26))
        leftView2.backgroundColor              = .clear
        textfield.leftView                     = leftView2
        textfield.leftViewMode                 = .always
        textfield.contentVerticalAlignment     = .center
        
    }
    class func setleftView(textfield:UITextField,customWidth:Int)
    {
        let leftView2                          = UIView(frame: CGRect(x: 5, y: 0, width: customWidth, height: 26))
        leftView2.backgroundColor              = .clear
        textfield.leftView                     = leftView2
        textfield.leftViewMode                 = .always
        textfield.contentVerticalAlignment     = .center
    }
    
    
    //MARK:- attribute strings
    
    class func setAttributedString(FirstString:String,SecondString:String) -> NSMutableAttributedString{
        
        let firstAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.lightGray]
        let secondAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.darkGray]
        
        
        let partOne = NSMutableAttributedString(string: FirstString, attributes: firstAttributes)
        let partTwo = NSMutableAttributedString(string: SecondString, attributes: secondAttributes)
        
        partOne.append(partTwo)
        print(partOne)
        return partOne
    }
    
    
    //MARK:- Convertion methods
    
    class func convertTimeStampToDate(timeStamp:Any) -> String {
        print(timeStamp)
        
        let timeResultString:String! = (timeStamp as? String) ?? ""
        print(timeResultString!)
        
        
        
        //            let myInt1 = Int(timeResultString)
        let myInt2 = Int(timeResultString) ?? 0
        print(myInt2)
        if myInt2 != 0
        {
            let dateTimeStamp = NSDate(timeIntervalSince1970:Double(myInt2)/1000)  //UTC time
            
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = .current //Edit
            dateFormatter.dateFormat = " dd MMM yyyy-h:mm a"
            
            
            
            let strDateSelect = dateFormatter.string(from: dateTimeStamp as Date)
            print(strDateSelect) //Local time
            
            return  strDateSelect
            
        }
        else
        {
            return ""
        }
        
    }
    
    
    class func convertTimeStampToFirstMonth(timeStamp:Any) -> String {
        print(timeStamp)
        
        let timeResultString:String! = (timeStamp as? String) ?? ""
        print(timeResultString!)
        
        
        
        //            let myInt1 = Int(timeResultString)
        let myInt2 = Int(timeResultString) ?? 0
        print(myInt2)
        if myInt2 != 0
        {
            let dateTimeStamp = NSDate(timeIntervalSince1970:Double(myInt2)/1000)  //UTC time
            
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = .current //Edit
            dateFormatter.dateFormat = "MMM dd  yyyy-h:mm a"
            
            
            
            let strDateSelect = dateFormatter.string(from: dateTimeStamp as Date)
            print(strDateSelect) //Local time
            
            return  strDateSelect
            
        }
        else
        {
            return ""
        }
        
    }
    
    
    
    class func convert12Hourto24HourFormat(val : String) -> String{
        
        let dateAsString             = val
        let dateFormatter            = DateFormatter()
        dateFormatter.dateFormat     = "h:mm a"
        
        let date = dateFormatter.date(from: dateAsString)
        dateFormatter.dateFormat     = "HH:mm:ss"
        
        let Date24 = dateFormatter.string(from: date!)
        //  print("24 hour formatted Date:",Date24)
        
        return Date24
    }
    
    class func convert24Hourto12HourFormat(val : String) -> String{
        
        let dateAsString               = val
        let dateFormatter              = DateFormatter()
        dateFormatter.dateFormat       = "HH:mm:ss"
        
        let date = dateFormatter.date(from: dateAsString)
        dateFormatter.dateFormat       = "h:mm a"
        let Date12 = dateFormatter.string(from: date!)
        
        return Date12
    }
    
    //MARK:- attribute text
    
    //MARK:- attribute place holder
    class func setAttributedPlaceHolder(placeHolderName:String) -> NSMutableAttributedString{
        
        let yourAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.red]
        //   let yourOtherAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.darkGray]
        
        let partOne = NSMutableAttributedString(string: placeHolderName, attributes: nil)
        let partTwo = NSMutableAttributedString(string: " *", attributes: yourAttributes)
        
        partOne.append(partTwo)
        print(partOne)
        return partOne
    }
    
    class func setAttributedForMandetryTxtString(string1:String,inputTxt:UITextField) {
        
        let firstAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.lightGray, .font: UIFont(name: "Poppins", size: 12)!]
        let secondAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.red, .font: UIFont(name: "Poppins", size: 12)!]
        
        let firstString = NSMutableAttributedString(string: string1, attributes: firstAttributes)
        let secondString = NSAttributedString(string: " *", attributes: secondAttributes)
        
        firstString.append(secondString)
        inputTxt.attributedPlaceholder = firstString
    }
    
    class func setAttributedForMandetryLblString(string1:String,inputLbl:UILabel) {
        
        let firstAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.lightGray, .font: UIFont(name: "Poppins", size: 12)!]
        let secondAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.red, .font: UIFont(name: "Poppins", size: 12)!]
        
        let firstString = NSMutableAttributedString(string: string1, attributes: firstAttributes)
        let secondString = NSAttributedString(string: " *", attributes: secondAttributes)
        
        firstString.append(secondString)
        inputLbl.attributedText = firstString
    }
    
    //MARK: Date without time
    class func dateWithOutTime(datDate: Date) -> Date {
        var calendar          = Calendar(identifier: .gregorian)
        calendar.timeZone     = TimeZone(secondsFromGMT: 0)!
        return calendar.startOfDay(for: datDate)
    }
    
    //Date to milliseconds
    func currentTimeInMiliseconds() -> Int! {
        let currentDate = NSDate()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/mm/yyyy"
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let date = dateFormatter.date(from: dateFormatter.string(from: currentDate as Date))
        let nowDouble = date!.timeIntervalSince1970
        return Int(nowDouble*1000)
    }
    class  func convertTimeStampWithoutYear(dateAsString:String)->String
    {
        
        if dateAsString != ""
        {
            let date = Date(timeIntervalSince1970: Double(dateAsString)!)
            
            let formatter = DateFormatter()
            formatter.dateFormat = "dd MMM"
            //        formatter.timeZone = TimeZone(identifier: "UTC")
            formatter.timeZone = TimeZone.current
            
            
            let localDatef = formatter.string(from: date)
            //            print(localDatef)
            return localDatef
        }
        else
        {
            return ""
        }
    }
    class  func convertTimeStampWithoutTime(dateAsString:String)->String
    {
        
        if dateAsString != ""
        {
            let date = Date(timeIntervalSince1970: Double(dateAsString)!)
            
            let formatter = DateFormatter()
            formatter.dateFormat = "MMM dd yyyy"
            //        formatter.timeZone = TimeZone(identifier: "UTC")
            formatter.timeZone = TimeZone.current
            
            
            let localDatef = formatter.string(from: date)
            //            print(localDatef)
            return localDatef
        }
        else
        {
            return ""
        }
    }
    class  func convertTformatTimetodisplayFormat(dateAsString:String)->String{
        if dateAsString != ""{
            let formatter         = DateFormatter()
            formatter.dateStyle   = .long
            
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            //                    formatter.timeZone = .current
            formatter.timeZone = TimeZone(identifier: "UTC")
            
            
            let result            = formatter.date(from: "\(dateAsString)")
            
            print(result as Any)
            
            let formatter2        = DateFormatter()
            formatter2.dateStyle  = .long
            //            formatter2.dateFormat = "MMM dd yyyy"
            formatter2.dateFormat = "MMM dd yyyy"
            
            
            let val               = formatter2.string(from: result!)
            print("Val :\(val)")
            return val
        }
        else{
            return ""
        }
    }
    class  func sambagTimeDisplay(dateAsString:String)->String
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"
        //        let convertedDate = formatter.date(from: "16-09-2017 2:00 PM")
        let convertedDate = formatter.date(from: dateAsString)
        
        //                formatter.timeZone = TimeZone(identifier: "UTC")
        let date = formatter.string(from: convertedDate!)
        print(date)
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "MMM dd yyyy hh:mm a"
        let convertedDate2 = formatter.date(from: date)
        formatter2.timeZone = TimeZone.current
        //                formatter2.timeZone = TimeZone(identifier: "UTC")
        
        let date2 = formatter2.string(from: convertedDate2!)
        print(date2)
        return date2
    }
    
    class  func sambagTimeDisplaywithoutTime(dateAsString:String)->String
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        //        let convertedDate = formatter.date(from: "16-09-2017 2:00 PM")
        let convertedDate = formatter.date(from: dateAsString)
        
        //                formatter.timeZone = TimeZone(identifier: "UTC")
        let date = formatter.string(from: convertedDate!)
        print(date)
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "MMM dd yyyy"
        let convertedDate2 = formatter.date(from: date)
        formatter2.timeZone = TimeZone.current
        //                formatter2.timeZone = TimeZone(identifier: "UTC")
        
        let date2 = formatter2.string(from: convertedDate2!)
        print(date2)
        return date2
    }
    
    class  func sambagTimeDisplaywithoutTime2(dateAsString:String)->String
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        //        let convertedDate = formatter.date(from: "16-09-2017 2:00 PM")
        let convertedDate = formatter.date(from: dateAsString)
        
        //                formatter.timeZone = TimeZone(identifier: "UTC")
        let date = formatter.string(from: convertedDate!)
        print(date)
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "MMM dd yyyy"
        let convertedDate2 = formatter.date(from: date)
        formatter2.timeZone = TimeZone.current
        //                formatter2.timeZone = TimeZone(identifier: "UTC")
        
        let date2 = formatter2.string(from: convertedDate2!)
        print(date2)
        return date2
    }
    
    //MARK: - Upload Image
    //completion: @escaping (JSON) -> Void
    class func requestWith(fileParamName : String, originalCover:String,urlAppendString:String,imageData: Data?, originalImg: Data?, parameters: [String : Any], mimeType : String, onError: ((Error?) -> Void)? = nil, completion: @escaping (JSON) -> Void){
        
        //        let url = Helpers.imageUploadUrl + "uploadFile"
        let url = Helpers.imageUploadUrl + urlAppendString
        
        print("image upload url : \(url)")
        let token = UserDefaults.standard.string(forKey: "app_token") ?? ""
        var headder: HTTPHeaders = [:]
        
        if( token != ""){
            if let id =  UserDefaults.standard.value(forKey: "userId") as? String{
                headder = [
                    "Accept": "application/json, text/plain, */*",
                    "Content-Type" :"application/json ; charset=utf-8",
                    "Authorization" : "Bearer " + token,  // customise Parameter and Value
                    "logined_user_id":id,
//                    "app_info":tempJSON.stringValue
                ]
            }
            else{
                headder = [
                    "Accept": "application/json, text/plain, */*",
                    "Content-Type" :"application/json ; charset=utf-8",
                    "Authorization" : "Bearer " + token,
//                    "app_info":tempJSON.stringValue // customise Parameter and Value
                ]
            }
        }
        else{
            headder = [
                "Accept": "multipart/form-data",
                "Content-Type" :"application/json; charset=utf-8",
//                "app_info":tempJSON.stringValue
            ]
        }
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            if mimeType == "video/quicktime/m4v"
            {
                if let data = imageData{
                    multipartFormData.append(data, withName: fileParamName, fileName: "video.mov", mimeType: mimeType)
                }
                
            }
                
            else
            {
                if mimeType.lowercased() == "image/png" {
                    if fileParamName.lowercased() == "documents"{
                        if let data = imageData{
                            multipartFormData.append(data, withName: fileParamName, fileName: "image.png", mimeType: mimeType)
                        }
                    }
                    else{
                        if let data = imageData{
                            multipartFormData.append(data, withName: fileParamName, fileName: "image.png", mimeType: mimeType)
                        }
                        
                        if let data1 = originalImg{
                            multipartFormData.append(data1, withName: originalCover, fileName: "image.png", mimeType: mimeType)
                        }
                    }
                }
                else{
                    if let data = imageData{
                        let name = parameters["file_name"] as! String
                        multipartFormData.append(data, withName: fileParamName, fileName: "\(name)", mimeType: mimeType)
                    }
                }
                
            }
            
            
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headder) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.validate().responseJSON { response in
                    
                    print(response.result)
                    print(response)
                    
                    // print("Succesfully uploaded \(response.result.value!)")
                    DispatchQueue.main.async {
                        
                        let jsonData = JSON(response.data!)
                        
                        completion (jsonData)
                    }
                    
                    if let err = response.error{
                        onError?(err)
                        DispatchQueue.main.async {
                            
                            let jsonData = JSON(response.data!)
                            
                            completion (jsonData)
                        }
                        return
                    }
                    //  onCompletion?(nil)
                }
            case .failure(let error):
                //print("Error in upload: \(error.localizedDescription)")
                DispatchQueue.main.async {
                    //  self.Activity.isHidden = true
                   // return JSON()
                }
                onError?(error)
            }
        }
    }
    
    class func requestWithMultipleImages(fileParamName : String,urlAppendString:String,imageData: [Data]?, parameters: [String : Any], mimeType : String, onError: ((Error?) -> Void)? = nil, completion: @escaping (JSON) -> Void){
        
        //        let url = Helpers.imageUploadUrl + "uploadFile"
        var envStr : Any = NetworkManager.environment
        envStr = NetworkManager.environment
        let tempStr = urlAppendString.components(separatedBy: "/").last
        var url = ""
        if tempStr?.lowercased() == "post_comment" || tempStr?.lowercased() == "topic_comment"{
            if let env =  envStr as? APIEnvironment, env == APIEnvironment.production{
                //                NetworkManager.environment = .conversation
                url = "https://socket-4000.familheey.com/api/v1/familheey/" + urlAppendString
                print(env)
            }
            else if let env =  envStr as? APIEnvironment, env == APIEnvironment.preProdConversation{
                url = "https://preprod-socket-4000.familheey.com/api/v1/familheey/" + urlAppendString
                print(env)
            }
            else{
                url = Helpers.imageUploadUrl + urlAppendString
            }
        }
        else{
            url = Helpers.imageUploadUrl + urlAppendString
        }
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 600
        
        print("image upload url : \(url)")
        
        let token = UserDefaults.standard.string(forKey: "app_token") ?? ""
        var headder: HTTPHeaders = [:]
        
        if( token != ""){
            if let id =  UserDefaults.standard.value(forKey: "userId") as? String{
                headder = [
                    "Accept": "application/json, text/plain, */*",
                    "Content-Type" :"application/json ; charset=utf-8",
                    "Authorization" : "Bearer " + token,  // customise Parameter and Value
                    "logined_user_id":id,
//                    "app_info":tempJSON.stringValue
                ]
            }
            else{
                headder = [
                    "Accept": "application/json, text/plain, */*",
                    "Content-Type" :"application/json ; charset=utf-8",
                    "Authorization" : "Bearer " + token,
//                    "app_info":tempJSON.stringValue // customise Parameter and Value
                ]
            }
        }
        else{
            headder = [
                "Accept": "multipart/form-data",
                "Content-Type" :"application/json; charset=utf-8",
//                "app_info":tempJSON.stringValue
            ]
        }
        
        manager.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            if imageData != nil{
                if mimeType == "video/quicktime/m4v"
                {
                    for items in imageData!{
                        //if let data = items{
                        multipartFormData.append(items, withName: fileParamName, fileName: "video.mov", mimeType: mimeType)
                        //  }
                    }
                }
                else if mimeType == "image/png"
                    
                {
                    for items in imageData!{
                        //if let data = items{
                        multipartFormData.append(items, withName: fileParamName, fileName: "image.png", mimeType: mimeType)
                        //  }
                    }
                }
                else
                {
                    for items in imageData!{
                        //if let data = items{
                        multipartFormData.append(items, withName: fileParamName, fileName: mimeType, mimeType: mimeType)
                        //  }
                    }
                }
                
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headder) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.validate().responseJSON { response in
                    
                    print(response.result)
                    
                    upload.uploadProgress{ progress in
                        print("!!!####")
                    }
                    // print("Succesfully uploaded \(response.result.value!)")
                    DispatchQueue.main.async {
                        
                        let jsonData = JSON(response.data!)
                        
                        completion (jsonData)
                    }
                    
                    if let err = response.error{
                        onError?(err)
                        DispatchQueue.main.async {
                            
                            let jsonData = JSON(response.data!)
                            
                            completion (jsonData)
                        }
                        return
                    }
                    //  onCompletion?(nil)
                }
            case .failure(let error):
                //print("Error in upload: \(error.localizedDescription)")
                DispatchQueue.main.async {
                    //  self.Activity.isHidden = true
                    //return JSON()
                }
                onError?(error)
            }
        }
    }
    
    // MARK:- Refresh token
    class  func getAccessToken(completion: @escaping RefreshCompletion) {
        
        let param = ["refresh_token" : UserDefaults.standard.value(forKey: "refresh_token"), "phone":UserDefaults.standard.value(forKey: "userPhone")]
        
        Helpers.networkProvider.request(.getAccessToken(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("@@##!!! REsult\(jsonData)")
                    if response.statusCode == 200{
                        completion(jsonData["accessToken"].stringValue)
                    }
                    else{
                        completion("")
                    }
                    
                }catch _ {
                    
                    
                }
            case .failure( _):
                break
            }
        }
    }
    
    
    //    ler request =Alamofire.upload(.POST, "https://httpbin.org/post", file: fileURL)
    // request.cancel()
    
    //MARK:- Methods
    
    class func StopAPICALL()  {
        let sessionManager = Alamofire.SessionManager.default
        sessionManager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
            dataTasks.forEach { $0.cancel() }
            uploadTasks.forEach { $0.cancel() }
            downloadTasks.forEach { $0.cancel() }
        }
    }
    
    
    
    
    class func showActivityViewcontroller(url: String,VC: UIViewController){
        let trimmed = url.trimmingCharacters(in: .whitespacesAndNewlines)
        if let objectsToShare:URL = URL(string: trimmed){
            
            //           let img: UIImage = #imageLiteral(resourceName: "Family Detail")
            //
            //           guard let url = myWebsite else {
            //               print("nothing found")
            //               return
            //           }
            //
            //           let shareItems:Array = [url]
            //           let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
            //        activityViewController.excludedActivityTypes = [UIActivity.ActivityType.print, UIActivity.ActivityType.postToWeibo, UIActivity.ActivityType.copyToPasteboard, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.postToVimeo, UIActivity.ActivityType.postToFacebook, UIActivity.ActivityType.postToTwitter]
            //           VC.present(activityViewController, animated: true, completion: nil)
            
            let shareText = "\(objectsToShare)"
            let sharedObjects:[AnyObject] = [shareText as AnyObject]
            let activityVC = UIActivityViewController(activityItems: sharedObjects, applicationActivities: nil)
            activityVC .setValue("Check out this in Familheey", forKey: "Subject")
            activityVC.excludedActivityTypes = [.airDrop, .postToFlickr, .assignToContact, .openInIBooks]
            VC.present(activityVC, animated: true, completion: nil)
        }
        
    }
    
    class func dialNumber(number : String,target:UIViewController) {
        
        if let url = URL(string: "tel://\(number)"),
            UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
            Helpers.showAlertDialog(message: "Invalid number", target: target)
            
            // add error message here
        }
    }
    
    //MARK:- Open App Link
    class func openAppLinkCommon(url:String,currentVC:UIViewController){
        
        let param = ["url" : url]
        
        Helpers.networkProvider.request(.openAppGetParams(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("@@##!!! REsult\(jsonData)")
                    if response.statusCode == 200{
                        let dataArr = jsonData["data"]
                        let subType = dataArr["sub_type"].stringValue
                        let type =  dataArr["type"].stringValue
                        
                        //                        Helpers.navVC = UINavigationController(rootViewController: currentVC)
                        if subType == notificationSubTypes.publicfeed || subType == notificationSubTypes.familyfeed && type == notificationTypes.home{
                            let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
                            vc.postId = "\(dataArr["type_id"].intValue)"
                            Helpers.navVC.pushViewController(vc, animated: true)
                            
                        }
                        else if type == notificationTypes.family{
                            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                            let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
                            intro.groupId = "\(dataArr["type_id"].intValue)"
                            Helpers.navVC.pushViewController(intro, animated: true)
                        }
                        else{
                            let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
                            vc.enableEventEdit = false
                            vc.isFromNotification = "true"
                            vc.eventId = "\(dataArr["type_id"].intValue)"
                            Helpers.navVC.pushViewController(vc, animated: true)
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            Helpers.openAppLinkCommon(url: url, currentVC: currentVC)
                        }
                    }
                    else{
                        
                    }
                    
                }catch _ {
                    
                    
                }
            case .failure( _):
                break
            }
        }
    }
    
}

extension Helpers: RequestAdapter, RequestRetrier{
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        print("-------Invalid \(urlRequest)")
        
        var urlRequest = urlRequest
        let token = UserDefaults.standard.string(forKey: "app_token") ?? ""
        
        //            urlRequest.headers.add(.authorization(bearerToken: token))
        urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        return urlRequest
    }
    
    func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion) {
        print("---------Invalid")
        if let response = request.task?.response as? HTTPURLResponse, response.statusCode == 401{
            Helpers.getAccessToken { (accessToken) in
                print(accessToken!)
                if !accessToken!.isEmpty{
                    setUserDefaultValues.setUserDefaultValue(userDefaultValue:accessToken!, userDefaultKey: "app_token")
                    completion(true,0.0)
                }
            }
        }
        else{
            completion(false,0.0)
        }
    }
}

extension String {
    var isPhoneNumber: Bool {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches = detector.matches(in: self, options: [], range: NSMakeRange(0, self.count))
            if let res = matches.first {
                return res.resultType == .phoneNumber && res.range.location == 0 && res.range.length == self.count
            } else {
                return false
            }
        } catch {
            return false
        }
    }
}


extension StringProtocol {
    var firstUppercased: String {
        return prefix(1).uppercased() + dropFirst()
    }
    var firstCapitalized: String {
        return String(prefix(1)).capitalized + dropFirst()
    }
}


//Milliseconds to date
extension Int {
    func dateFromMilliseconds(format:String) -> Date {
        let date : NSDate! = NSDate(timeIntervalSince1970:Double(self) / 1000.0)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone.current
        let timeStamp = dateFormatter.string(from: date as Date)
        
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return ( formatter.date( from: timeStamp ) )!
    }
}

protocol TappableImageViewDelegate: class {
    func profileImageAction(id:Int)
}
class TappableImageView: UIImageView {
    
    weak var delegate: TappableImageViewDelegate?
    
    override func layoutSubviews() {
        addAction(index: self.tag)
    }
    func addAction(index:Int){
        DispatchQueue.main.async {
            
            let button:UIButton = UIButton(frame: self.bounds)
            button.tag = index
            button.addTarget(self, action:#selector(self.buttonzClicked), for: .touchUpInside)
            self.addSubview(button)
            self.bringSubviewToFront(button)
        }
   
    }
    @objc func buttonzClicked(sender:UIButton) {
        delegate?.profileImageAction(id: sender.tag)
    }

}

/*extension UIImage {

    func fixOrientation() -> UIImage {

        // No-op if the orientation is already correct
        if ( self.imageOrientation == UIImage.Orientation.up ) {
            return self;
        }

        // We need to calculate the proper transformation to make the image upright.
        // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
        var transform: CGAffineTransform = CGAffineTransformIdentity

        if ( self.imageOrientation == UIImage.Orientation.down || self.imageOrientation == UIImage.Orientation.DownMirrored ) {
            transform = transform.translatedBy(x: self.size.width, y: self.size.height)
            transform = transform.rotated(by: CGFloat(M_PI))
        }

        if ( self.imageOrientation == UIImage.Orientation.Left || self.imageOrientation == UIImage.Orientation.LeftMirrored ) {
            transform = transform.translatedBy(x: self.size.width, y: 0)
            transform = transform.rotated(by: CGFloat(M_PI_2))
        }

        if ( self.imageOrientation == UIImage.Orientation.Right || self.imageOrientation == UIImageOrientation.RightMirrored ) {
            transform = transform.translatedBy(x: 0, y: self.size.height);
            transform = transform.rotated(by: CGFloat(-M_PI_2));
        }

        if ( self.imageOrientation == UIImage.Orientation.UpMirrored || self.imageOrientation == UIImageOrientation.DownMirrored ) {
            transform = transform.translatedBy(x: self.size.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        }

        if ( self.imageOrientation == UIImage.Orientation.LeftMirrored || self.imageOrientation == UIImageOrientation.RightMirrored ) {
            transform = CGAffineTransformTranslate(transform, self.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
        }

        // Now we draw the underlying CGImage into a new context, applying the transform
        // calculated above.
        let ctx: CGContextRef = CGBitmapContextCreate(nil, Int(self.size.width), Int(self.size.height),
                                                      CGImageGetBitsPerComponent(self.CGImage), 0,
                                                      CGImageGetColorSpace(self.CGImage),
                                                      CGImageGetBitmapInfo(self.CGImage).rawValue)!;

        CGContextConcatCTM(ctx, transform)

        if ( self.imageOrientation == UIImageOrientation.Left ||
            self.imageOrientation == UIImageOrientation.LeftMirrored ||
            self.imageOrientation == UIImageOrientation.Right ||
            self.imageOrientation == UIImageOrientation.RightMirrored ) {
            CGContextDrawImage(ctx, CGRectMake(0,0,self.size.height,self.size.width), self.CGImage)
        } else {
            CGContextDrawImage(ctx, CGRectMake(0,0,self.size.width,self.size.height), self.CGImage)
        }

        // And now we just create a new UIImage from the drawing context and return it
        return UIImage(CGImage: CGBitmapContextCreateImage(ctx)!)
    }
}*/

extension UIImage {
    var fixedOrientation: UIImage {
        guard imageOrientation != .up else { return self }

        var transform: CGAffineTransform = .identity
        switch imageOrientation {
        case .down, .downMirrored:
            transform = transform
                .translatedBy(x: size.width, y: size.height).rotated(by: .pi)
        case .left, .leftMirrored:
            transform = transform
                .translatedBy(x: size.width, y: 0).rotated(by: .pi)
        case .right, .rightMirrored:
            transform = transform
                .translatedBy(x: 0, y: size.height).rotated(by: -.pi/2)
        case .upMirrored:
            transform = transform
                .translatedBy(x: size.width, y: 0).scaledBy(x: -1, y: 1)
        default:
            break
        }

        guard
            let cgImage = cgImage,
            let colorSpace = cgImage.colorSpace,
            let context = CGContext(
                data: nil, width: Int(size.width), height: Int(size.height),
                bitsPerComponent: cgImage.bitsPerComponent, bytesPerRow: 0,
                space: colorSpace, bitmapInfo: cgImage.bitmapInfo.rawValue
            )
        else { return self }
        context.concatenate(transform)

        var rect: CGRect
        switch imageOrientation {
        case .left, .leftMirrored, .right, .rightMirrored:
            rect = CGRect(x: 0, y: 0, width: size.height, height: size.width)
        default:
            rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        }

        context.draw(cgImage, in: rect)
        return context.makeImage().map { UIImage(cgImage: $0) } ?? self
    }
}

extension UIImageView{

    func isTappable(id : Int){
        DispatchQueue.main.async {
            let button:UIButton = UIButton(frame: self.bounds)
            button.tag = id
            button.addTarget(self, action:#selector(self.buttonClicked), for: .touchUpInside)
//            button.backgroundColor = .red
            self.addSubview(button)
            self.bringSubviewToFront(button)
        }

    }
    @objc func buttonClicked(sender:UIButton) {
        
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "UserProfileTableViewController") as! UserProfileTableViewController
        vc.userID = "\(sender.tag)"
        if let currentVC = UIApplication.topViewController() {
            currentVC.navigationController?.pushViewController(vc, animated: true)
        }
    }

    
}
extension UINavigationController {
  func popToViewController(ofClass: AnyClass, animated: Bool = true) {
    if let vc = viewControllers.last(where: { $0.isKind(of: HomeViewTabController.self) }) {
      popToViewController(vc, animated: animated)
        return
    }
    
    if let vc = viewControllers.last(where: { $0.isKind(of: ofClass) }) {
      popToViewController(vc, animated: animated)
        return
    }
    popViewController(animated: animated)
  }
    func addFadeAnimation(){
        let transition = CATransition()
        transition.duration = 0.1
          transition.type = CATransitionType.fade
          self.view.layer.add(transition, forKey:nil)
    }
    

    
}


extension UIView
{
    func showDefaultAnimation(target:UIViewController)
    {
        target.view.addSubview(self)
              target.view.bringSubviewToFront(self)

    }
    func hideDefaultAnimation(target:UIViewController)
    {
        self.removeFromSuperview()

    }
}
extension Date {
    func toMillis() -> Int64! {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}
extension UIButton {

    open override var isEnabled: Bool{
        didSet {
            alpha = isEnabled ? 1.0 : 0.5
        }
    }

}
