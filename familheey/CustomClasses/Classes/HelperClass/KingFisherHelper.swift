//
//  KingFisherHelper.swift
//  familheey
//
//  Created by Giri on 31/12/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import Foundation
import Kingfisher

extension UIImageView {
    func setImageWithRetry(url:String,tryCount:Int){
        print("imageURL:",url)
        let Count = tryCount
        if Count < 1{
            self.kf.indicatorType = .none
            return
        }
        print("Count:",Count)
        self.kf.setImage(with: URL.init(string: url), placeholder:#imageLiteral(resourceName: "imgNoImage_landscape") , options: nil, progressBlock: nil) { (image, error, type, Surl) in
            if error != nil{
                print("error:",error!)
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    self.setImageWithRetry(url: url, tryCount: Count - 1)
                }
            }
            else{
                self.kf.indicatorType = .none
               // print("success:",image!)
            }
        }
    }
}
