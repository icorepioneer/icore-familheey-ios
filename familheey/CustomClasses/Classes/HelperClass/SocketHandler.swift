//
//  SocketHandler.swift
//  familheey
//
//  Created by Giri on 09/12/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import Foundation
import SocketIO
import Moya
import SwiftyJSON

var manager : SocketManager!
 var socket: SocketIOClient!

extension AppDelegate {
    
    
    func setupSocketForMessaging(){
        
        
        let socketStr = BaseUrl.socketUrl()
              
              manager = SocketManager(socketURL: URL(string: socketStr)!, config: [.log(true), .compress])
              socket = manager.defaultSocket
              print(socket ?? "")
        
              
              socket.on(clientEvent: .connect) {data, ack in
                  print(data)
                  print("socket connected")
              }
              
              socket.on(clientEvent: .error) { (data, eck) in
                  print(data)
                  print("socket error")
              }
              
              socket.on(clientEvent: .disconnect) { (data, eck) in
                  print(data)
                  print("socket disconnect")
              }
              
              socket.on(clientEvent: SocketClientEvent.reconnect) { (data, eck) in
                  print(data)
                  print("socket reconnect")
              }
              
              
              socket.on("notification_\(UserDefaults.standard.value(forKey: "userId") as! String)") {data, ack in
                if let currentVC = UIApplication.topViewController(), currentVC is ConversationsViewController {
                        return
                }
                  let jsonData =  JSON(data[0])
                  print(jsonData)
//                if jsonData["type"].string?.lowercased() == "chat_notification"{
//                    self.scheduleNotifications(data: jsonData)
//                }

        }
        socket.connect()
        
    }
    
    func scheduleNotifications(data:JSON) {
        
        let content = UNMutableNotificationContent()
        let requestIdentifier = "\(Int.random(in: 0..<6))"
        content.badge = 0
        content.title = "\(data["comment_data"]["full_name"].string ?? "Unknown") commented in \(data["result"][0]["snap_description"].stringValue)"
        content.body = data["comment_data"]["comment"].stringValue
        if let attachment = data["comment_data"]["attachment"].array{
            if attachment.count != 0{
                if attachment[0]["type"].stringValue .contains("image"){
                    let temp = "\(Helpers.imageURl)"+"\(BaseUrl.conversation)"+attachment[0]["filename"].stringValue
                    
                    let data = try? Data(contentsOf: URL.init(string: temp)!)
                    guard let myImage = UIImage(data: data!) else { return }

                  if let attachment = UNNotificationAttachment.create(identifier: requestIdentifier, image: myImage, options: nil) {
                      content.attachments = [attachment]
                    content.body = ""
                  }
                }
                else if attachment[0]["type"].stringValue .contains("video"){
                    content.body = "Video"
                }
                else{
                    content.body = "Document"
                }
                
            }
        }
        content.userInfo = data.dictionaryObject ?? [:]
        content.sound = UNNotificationSound.default
        
        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 0.1, repeats: false)
        
        let request = UNNotificationRequest(identifier: requestIdentifier, content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request) { (error:Error?) in
            
            if error != nil {
                print(error!.localizedDescription)
            }
            print("Notification Register Success")
        }
    }
    
    
}

extension UIApplication {
    
    static func topViewController(base: UIViewController? = UIApplication.shared.delegate?.window??.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
            return topViewController(base: selected)
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        
        return base
    }
}
extension UNNotificationAttachment {

    static func create(identifier: String, image: UIImage, options: [NSObject : AnyObject]?) -> UNNotificationAttachment? {
        let fileManager = FileManager.default
        let tmpSubFolderName = ProcessInfo.processInfo.globallyUniqueString
        let tmpSubFolderURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(tmpSubFolderName, isDirectory: true)
        do {
            try fileManager.createDirectory(at: tmpSubFolderURL, withIntermediateDirectories: true, attributes: nil)
            let imageFileIdentifier = identifier+".png"
            let fileURL = tmpSubFolderURL.appendingPathComponent(imageFileIdentifier)
            guard let imageData = image.pngData() else {
                return nil
            }
            try imageData.write(to: fileURL)
            let imageAttachment = try UNNotificationAttachment.init(identifier: imageFileIdentifier, url: fileURL, options: options)
            return imageAttachment
        } catch {
            print("error " + error.localizedDescription)
        }
        return nil
    }
}
