//
//  PushNotificationHandler.swift
//  familheey
//
//  Created by Giri on 24/12/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import Foundation
import SocketIO
import Moya
import SwiftyJSON

struct notificationTypes{
    static let family = "family"
    static let user = "user"
    static let home = "home"
    static let announcement = "announcement"
    static let post = "post"
    static let event = "event"
    static let request = "request"
    static let topic = "topic"
    
}
struct notificationSubTypes {
    static let member = "member"
    static let request = "request"
    static let publicfeed = "publicfeed"
    static let familyfeed = "familyfeed"
    static let conversation = "conversation"
    static let guest_interested = "guest_interested"
    static let guest_attending = "guest_attending"
    static let calendar = "calendar"
    static let link_family = "family_link"
    static let fetch_link = "fetch_link"
    static let requestFeed = "requestFeed"
    static let item = "item"
    static let about = "about"
    static let membership = "membership"
}

extension NotificationViewController {
    
    func handleNavigations(){
        
        appDel.isFromNotification = false
        guard let type = appDel.notificationData["type"] as? String else {
            return
        }
        
        if let subType = appDel.notificationData["sub_type"] as? String{
            
            if subType == notificationSubTypes.guest_interested || subType == notificationSubTypes.guest_attending{
                guard let id = appDel.notificationData["type_id"]  as? String else{
                    return
                }
                let story = UIStoryboard(name: "third", bundle: nil)
                let vc = story.instantiateViewController(withIdentifier: "GuestDetailsViewController") as! GuestDetailsViewController
                if subType == notificationSubTypes.guest_interested{
                    vc.selectedIndex = 1
                }
                else{
                    vc.selectedIndex = 0
                }
                vc.eventId = id
                self.navigationController?.pushViewController(vc, animated: false)
                return
            }
            else if subType == notificationSubTypes.calendar{
                let CalendarEventsViewController = self.storyboard?.instantiateViewController(withIdentifier: "CalendarEventsViewController") as! CalendarEventsViewController
                CalendarEventsViewController.isFromExplore = ""
                self.navigationController?.pushViewController(CalendarEventsViewController, animated: false)
                return
            }
            else if subType == notificationSubTypes.member{
                getMemberDetails(toRequest: false)
                return
            }
            else if subType == notificationSubTypes.membership{
                getMemberDetails(toRequest: false)
                return
            }
            
            else if subType == notificationSubTypes.request && type == notificationTypes.user{
                
                let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                let intro = storyboard.instantiateViewController(withIdentifier: "UserProfileTableViewController") as! UserProfileTableViewController
                let userId = UserDefaults.standard.value(forKey: "userId") as! String
                intro.userID = userId
                intro.isFromNotification = true
                self.navigationController?.pushViewController(intro, animated: false)
                return
                
            }
            else if subType == notificationSubTypes.request && type == notificationTypes.family{
                getMemberDetails(toRequest: true)
                return
                
            }
            else  if (subType == notificationSubTypes.publicfeed || subType == notificationSubTypes.familyfeed) && type == notificationTypes.home{
                guard let id = appDel.notificationData["type_id"]  as? String else{
                    return
                }
                let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
                vc.postId = id
                self.navigationController?.pushViewController(vc, animated: false)
                
                return
            }
            else if subType == notificationSubTypes.conversation && type == notificationTypes.post{
                guard let id = appDel.notificationData["type_id"]  as? String else{
                    return
                }
                getPostDetails(id: id)
            }
            else if subType == notificationSubTypes.conversation && type == notificationTypes.announcement{
                if let id = appDel.notificationData["type_id"] {
                    getAnnouncementDetailsConversation(id: "\(String(describing: id))")
                }
            }
            else if subType == notificationSubTypes.link_family && type == notificationTypes.family{
                getMembersDetailsNdGotoLinkedFamilies()
                return
                
            }
            else if subType == notificationSubTypes.fetch_link && type == notificationTypes.family{
                getMembersDetailsNdGotoLinkedFamilies()
                return
                
            }
            else if subType == notificationSubTypes.requestFeed{
                guard let id = appDel.notificationData["type_id"]  as? String else{
                    return
                }
                getRequestDetails(id:id)
                return
            }
            else if subType == notificationSubTypes.about && type == notificationTypes.family{
                guard let id = appDel.notificationData["type_id"]  as? String else{
                    return
                }
                print("!! --- \(id)")
                goToFamilyDetails(id: "\(id)", fromButtonAction: false)
                return
                
            }
            
        }
        
        if type == notificationTypes.family{
            
            guard let id = appDel.notificationData["type_id"]  as? String else{
                return
            }
            print("@@@##### --- \(id)")
            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
            intro.groupId = id
            self.navigationController?.pushViewController(intro, animated: false)
            
        }
        else if type == notificationTypes.user{
            
            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            let intro = storyboard.instantiateViewController(withIdentifier: "UserProfileTableViewController") as! UserProfileTableViewController
            let userId = UserDefaults.standard.value(forKey: "userId") as! String
            intro.userID = userId
            self.navigationController?.pushViewController(intro, animated: false)
            
        }
        else if type == notificationTypes.event{
            guard let id = appDel.notificationData["type_id"]  as? String else{
                return
            }
            let storyboard = UIStoryboard.init(name: "third", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
            vc.isFromNotification = "true"
            vc.enableEventEdit = false
            vc.eventId = id
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else if type == notificationTypes.announcement{
            guard let id = appDel.notificationData["type_id"]  as? String else{
                return
            }
            getAnnouncementDetails(id: id)
        }
        else if type == notificationTypes.topic{
            guard let id = appDel.notificationData["type_id"]  as? String else{
                   return
               }
                    let storyboard = UIStoryboard.init(name: "Topic", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "TopicsConversationViewController") as! TopicsConversationViewController
                    vc.topicID = id
                    self.navigationController?.pushViewController(vc, animated: true)

        }
        
    }
    
    func goToFamilyDetails(id:String,fromButtonAction:Bool){

        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
        intro.groupId = id
        intro.isMemberNotification = true
        intro.fromButtonAction = fromButtonAction
        self.navigationController?.pushViewController(intro, animated: false)
    }
    
    func getRequestDetails(id:String){
        
        ActivityIndicatorView.show("Please wait....")
        let param = ["post_request_id":id,"user_id":UserDefaults.standard.value(forKey: "userId") as! String]
        
        networkProvider.request(.post_need_detail(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        if let need = jsonData["data"].dictionary{

                            if (need["post_request_id"] != nil){

                                let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "RequestDetailsViewController") as! RequestDetailsViewController
                                vc.requestId = id
                                vc.requestDic = jsonData["data"]
                                self.navigationController?.pushViewController(vc, animated: true)
                                
                            }
                            else{
                                self.displayAlert(alertStr: "Oops! This content is no longer available.", title: "")
                            }
                        }
                        else{
                            self.displayAlert(alertStr: "Oops! This content is no longer available.", title: "")
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getRequestDetails(id: id)
                        }
                    }
                    else{
                        self.displayAlert(alertStr: "Oops! This content is no longer available.", title: "")
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
        
    }
    
    func getMemberDetails(toRequest:Bool){
        
        guard let id = appDel.notificationData["type_id"] else{
            return
        }
        let strTemp = "\(id)"

        if strTemp.isEmpty{
            return
        }
        APIServiceManager.callServer.getFamilyDetails(url: EndPoint.getFamily, groupId: "\(id)", userId:UserDefaults.standard.value(forKey: "userId") as! String, success: { (responseMdl) in
            print(responseMdl!)
            guard let familyMdl = responseMdl as? familyListResponse else{
                return
            }
            ActivityIndicatorView.hiding()
            print(familyMdl.familyList as Any)
            if familyMdl.statusCode == 200{
                if familyMdl.familyList!.count > 0{
                    
                    let familyArr = familyMdl.familyList
                    
                    let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                    let addMember = storyboard.instantiateViewController(withIdentifier: "AddMembersNewViewController") as! AddMembersNewViewController
                    appDel.groupNamePublic = familyArr![0].faName
                    appDel.groupImageUrlPublic = familyArr![0].faCoverPic
                    
                    addMember.groupId = "\(id)"
                    addMember.memberJoining = familyArr![0].memberApproval
                    addMember.isAdmin = familyMdl.familyList![0].isAdmin
                    addMember.link_type = familyMdl.familyList![0].linkFamily
                    addMember.isMembershipActive = familyMdl.familyList![0].isMembershipActive
                    addMember.postId                    = familyMdl.familyList![0].postCreate
                    addMember.announcementId            = familyMdl.familyList![0].announcementCreate
                    addMember.memberJoiningStatus = familyMdl.familyList![0].memberJoining
                    addMember.invitationStatus = familyMdl.familyList![0].invitationStatus
                    addMember.memberJoining = familyMdl.familyList![0].memberApproval
                    
                    if familyArr![0].faCategory.lowercased() == "regular"{
                        addMember.faCate = "regular"
                    }
                    addMember.islinkable = familyArr![0].islinkable
                    addMember.toRequest = toRequest
//                    if toRequest{
//                        DispatchQueue.main.asyncAfter(deadline: .now()+1) {
//                            addMember.onClickRequestsAction(self)
//                        }
//                    }
                    self.navigationController?.pushViewController(addMember, animated: false)
                    
                }
                else{
                    self.displayAlertChoice(alertStr: "Oops! This content is no longer available.", title: "", completion: { (result) in
                        self.navigationController?.popViewController(animated: true)
                    })
                }

            }
            else{
                self.displayAlertChoice(alertStr: "Oops! This content is no longer available.", title: "", completion: { (result) in
                    self.navigationController?.popViewController(animated: true)
                })
            }
            
        }) { (error) in
            
        }
    }
    
    func getMembersDetailsNdGotoLinkedFamilies(){
        
        guard let id = appDel.notificationData["type_id"] else{
            return
        }
        let subType =  appDel.notificationData["sub_type"] as! String
        
        APIServiceManager.callServer.getFamilyDetails(url: EndPoint.getFamily, groupId: "\(id)", userId:UserDefaults.standard.value(forKey: "userId") as! String, success: { (responseMdl) in
            print(responseMdl!)
            guard let familyMdl = responseMdl as? familyListResponse else{
                return
            }
            ActivityIndicatorView.hiding()
            print(familyMdl.familyList as Any)
            if familyMdl.statusCode == 200{
                if familyMdl.familyList!.count > 0{
                    
                    let familyArr = familyMdl.familyList
                    
                    let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                    let addMember = storyboard.instantiateViewController(withIdentifier: "LinkedFamiliesListViewController") as! LinkedFamiliesListViewController
                    appDel.groupNamePublic = familyArr![0].faName
                    appDel.groupImageUrlPublic = familyArr![0].faCoverPic
                    
                    addMember.groupID = "\(id)"
                    addMember.isAdmin = familyMdl.familyList![0].isAdmin
                    addMember.link_type = familyMdl.familyList![0].linkFamily
                    addMember.islinkable = familyArr![0].islinkable
                    if familyArr![0].faCategory.lowercased() == "regular"{
                        addMember.facate = "regular"
                    }
                    if subType.lowercased() == "family_link"{
                        addMember.isFromRequest = true
                        addMember.famArr = familyArr
                    }
                    else{
                        addMember.isFromRequest = false
                    }
                    
                    self.navigationController?.pushViewController(addMember, animated: true)
                    
                }
                else{
                    self.displayAlertChoice(alertStr: "Oops! This content is no longer available.", title: "", completion: { (result) in
                        self.navigationController?.popViewController(animated: true)
                    })
                }
            }
            else{
                self.displayAlertChoice(alertStr: "Oops! This content is no longer available.", title: "", completion: { (result) in
                    self.navigationController?.popViewController(animated: true)
                })
            }

            
        }) { (error) in
            
        }
    }
    
    func getAnnouncementDetails(id: String){
        
        ActivityIndicatorView.show("Please wait....")
        let param = ["type":"announcement","post_id":id,"user_id":UserDefaults.standard.value(forKey: "userId") as! String]
        
        networkProvider.request(.post_by_user(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        if let response = jsonData["data"].array{
                            
                            if response.count != 0{
                                let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementDetailViewController") as! AnnouncementDetailViewController
                                vc.ArrayAnnouncements = response
                                vc.selectedindex = IndexPath.init(row: 0, section: 0)
                                self.navigationController?.pushViewController(vc, animated: false)
                            }
                            else{
                                self.displayAlert(alertStr: "Oops! This content is no longer available.", title: "")
                            }
                        }
                        else{
                            self.displayAlert(alertStr: "Oops! This content is no longer available.", title: "")
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getAnnouncementDetails(id: id)
                        }
                    }
                    else{
                        self.displayAlert(alertStr: "Oops! This content is no longer available.", title: "")
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
        
    }
    
    func getAnnouncementDetailsConversation(id: String){
        ActivityIndicatorView.show("Please wait....")
        let param = ["type":"announcement","post_id":id,"user_id":UserDefaults.standard.value(forKey: "userId") as! String]
        
        networkProvider.request(.post_by_user(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        if let response = jsonData["data"].array{
                            
                            if response.count != 0{
                                let mainStoryBoard = UIStoryboard(name: "Conversations", bundle: nil)
                                let vc = mainStoryBoard.instantiateViewController(withIdentifier: "ConversationsViewController") as! ConversationsViewController
                                vc.post = response[0]
                                vc.conversationT = .announcement
                                self.navigationController?.pushViewController(vc, animated: false)
                            }
                            else{
                                self.displayAlert(alertStr: "Oops! This content is no longer available.", title: "")
                            }
                        }
                        else{
                            self.displayAlert(alertStr: "Oops! This content is no longer available.", title: "")
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getAnnouncementDetailsConversation(id: id)
                        }
                    }
                    else{
                        self.displayAlert(alertStr: "Oops! This content is no longer available.", title: "")
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    func getPostDetails(id: String){
        
        ActivityIndicatorView.show("Please wait....")
        let param = ["type":"post","post_id":id,"user_id":UserDefaults.standard.value(forKey: "userId") as! String]
        
        networkProvider.request(.post_by_user(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        if let response = jsonData["data"].array{
                            
                            if response.count != 0{
                                
                                let mainStoryBoard = UIStoryboard(name: "Conversations", bundle: nil)
                                let vc = mainStoryBoard.instantiateViewController(withIdentifier: "ConversationsViewController") as! ConversationsViewController
                                vc.post = response[0]
                                vc.conversationT = .post
                                self.navigationController?.pushViewController(vc, animated: false)
                                
                            }
                            else{
                                self.displayAlert(alertStr: "Oops! This content is no longer available.", title: "")
                            }
                        }
                        else{
                            self.displayAlert(alertStr: "Oops! This content is no longer available.", title: "")
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getPostDetails(id: id)
                        }
                    }
                    else{
                        self.displayAlert(alertStr: "Oops! This content is no longer available.", title: "")
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
        
    }
//    func getTopicDetails(id: String){
//
//        ActivityIndicatorView.show("Please wait....")
//        let param = ["topic_id":id,"user_id":UserDefaults.standard.value(forKey: "userId") as! String]
//
//        networkProvider.request(.topic_detail(parameter: param)) { (result) in
//            switch result{
//
//            case .success( let response):
//                do{
//                    let jsonData =  JSON(response.data)
//                    print("view contents : \(jsonData)")
//                    if response.statusCode == 200{
//                        if let response = jsonData["data"].array{
//
//                            if response.count != 0{
//
//                                let storyboard = UIStoryboard.init(name: "Topic", bundle: nil)
//                                  let vc = storyboard.instantiateViewController(withIdentifier: "TopicsConversationViewController") as! TopicsConversationViewController
//                                  vc.Topic = jsonData["data"][0]
//                                  self.navigationController?.pushViewController(vc, animated: true)
//                            }
//                            else{
//                                self.displayAlert(alertStr: "Oops! This content is no longer available.", title: "")
//                            }
//                        }
//                        else{
//                            self.displayAlert(alertStr: "Oops! This content is no longer available.", title: "")
//                        }
//                    }
//                    else{
//                        self.displayAlert(alertStr: "Oops! This content is no longer available.", title: "")
//                    }
//                    ActivityIndicatorView.hiding()
//
//                }catch let err {
//                    ActivityIndicatorView.hiding()
//                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
//                }
//            case .failure( let error):
//                ActivityIndicatorView.hiding()
//                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
//                break
//            }
//        }
//
//    }
    
}
