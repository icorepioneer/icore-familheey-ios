
import UIKit

protocol DatePickerPopupDelegate {
    func datePickerPopupDidSelectDate(_ dateString: String!)
    func datePickerPopupDidCancel()
}

open class DatePickerPopup: UIView {

    @IBOutlet weak var picker: UIDatePicker!
    fileprivate var parentView: UIView?
    fileprivate var overlayView: UIView?
    var delegate: DatePickerPopupDelegate?
    
    /// Call this method to get the object
    class var getInstance: DatePickerPopup{
        
        let obj = Bundle.main.loadNibNamed("DatePickerPopup",
                                                     owner: self,
                                                     options: nil)
        
        return obj![0] as! DatePickerPopup
    }
    
    open override func awakeFromNib() {
        
        picker.backgroundColor = UIColor.white
        picker.timeZone = TimeZone.autoupdatingCurrent
        picker.layer.cornerRadius = 5.0
        picker.addTarget(self,
                         action: #selector(self.dateChanged(_:)),
                         for: .valueChanged)
    }
    
    open func showInView(_ view: UIView?){
        
        self.parentView = view
        
        overlayView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height))
//            UIView(frame: view!.frame)
        overlayView!.backgroundColor = UIColor.black
        overlayView!.alpha = 0.5
        view!.addSubview(overlayView!)
        
        /*var frameRect = self.frame
        frameRect.origin.x = view!.frame.size.width/2 - self.frame.size.width/2
        frameRect.origin.y = view!.frame.size.height/2 - self.frame.size.height/2
        self.frame = frameRect*/
        
        self.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height - 450, width: (view?.frame.size.width)!, height: 350)
        
//        //Open wall menu
//        let transition = CATransition()
//        transition.duration = 0.1
//        transition.type = CATransitionType.moveIn
//        transition.subtype = CATransitionSubtype.fromBottom
//
//        self.isHidden = true
        view!.addSubview(self)
        view!.bringSubviewToFront(self)
//        self.layer.add(transition, forKey: kCATransition)
//        self.isHidden = false
        
       
    }
    
    @objc open func dateChanged(_ sender: UIDatePicker){
        
    }

    @IBAction func CancelButtonAction(_ sender: AnyObject) {
        
        var frameRect = self.frame
        
        if let parent = parentView{
            frameRect.origin.y = parent.frame.origin.y - self.frame.height
        }
        
        self.frame = frameRect
        
//        //Close wall menu
//        let transition = CATransition()
//        transition.duration = 0.1
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromBottom
//
//        self.layer.add(transition,
//                                forKey: kCATransition)
        
//        self.delay(0.5) {
            self.overlayView!.removeFromSuperview()
            self.removeFromSuperview()
            
            if let delegate = self.delegate{
                delegate.datePickerPopupDidCancel()
            }
//        }
    }
    
    @IBAction func clearButtonAction(_ sender: AnyObject) {
        picker.setDate(Date(), animated: true)
    }
    
    @IBAction func setButtonAction(_ sender: AnyObject) {
        
        let dateFormatter = DateFormatter()
//        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
//        dateFormatter.locale = Locale(identifier: "en_US")
//
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        let newDate = dateFormatter.string(from: picker.date)
        //print(picker.date)
        var frameRect = self.frame
        
        if let parent = parentView{
            frameRect.origin.y = parent.frame.origin.y - self.frame.height
        }
        
        self.frame = frameRect
        
//        //Close wall menu
//        let transition = CATransition()
//        transition.duration = 0.1
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromBottom
//        
//        self.layer.add(transition,
//                                forKey: kCATransition)
        
//        self.delay(0.5) {
            self.overlayView!.removeFromSuperview()
            self.removeFromSuperview()
            
            if let delegate = self.delegate {
                delegate.datePickerPopupDidSelectDate(newDate)
            }
//        }
    }
    
    fileprivate func delay(_ delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
    
    func getMonthAndYear() -> String{
        let dateFormatter = DateFormatter()
//        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
//        dateFormatter.locale = Locale(identifier: "en_US")
        
        dateFormatter.dateFormat = "MM/yy"
        return dateFormatter.string(from: picker.date)

    }
    
    func getMonth() -> String{
        let dateFormatter = DateFormatter()
//        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
//        dateFormatter.locale = Locale(identifier: "en_US")
        
        dateFormatter.dateFormat = "MM"
        return dateFormatter.string(from: picker.date)
        
    }

    func getYear() -> String{
        let dateFormatter = DateFormatter()
//        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
//        dateFormatter.locale = Locale(identifier: "en_US")
        
        dateFormatter.dateFormat = "yyyy"
        return dateFormatter.string(from: picker.date)
        
    }
    
    func setMinimumDate(date: Date){
        self.picker.minimumDate = date
    }
    
    func setMaximumDate(date: Date){
        self.picker.maximumDate = date
    }

    func setCurrentDate(date:Date){
        self.picker.date = date
    }
}
