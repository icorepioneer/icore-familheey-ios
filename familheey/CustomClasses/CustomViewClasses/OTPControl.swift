
import UIKit


protocol OTPControlDelegate {
    func OTPTyped(_ optString: String?)
    func OTPControlDelegateBeginEditing(inputField: UITextField)
    func OTPEntered(otp: String)
}

protocol OTPTextViewDelegate {
    func onBackPresss(textField: UITextField)
}

open class OTPControl : UIView, OTPTextViewDelegate {
    
    @IBOutlet fileprivate weak var otp1: OTPTextBox!
    @IBOutlet fileprivate weak var otp2: OTPTextBox!
    @IBOutlet fileprivate weak var otp3: OTPTextBox!
    @IBOutlet fileprivate weak var otp4: OTPTextBox!
    @IBOutlet fileprivate weak var otp5: OTPTextBox!
    @IBOutlet fileprivate weak var otp6: OTPTextBox!
    
    
    @IBOutlet weak var btnSend: UIButton!
    
    var delegate: OTPControlDelegate?
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    func show(){
        let obj = Bundle.main.loadNibNamed("OTPControl",
                                                     owner: self,
                                                     options: nil)
        let view = obj?[0] as! OTPControl
        
        
        view.frame = CGRect.init(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        
       // view.frame = CGRect.init(x: 0, y: 0, width: 50, height: 50)

        self.addSubview(view)
        
        view.delegate = self.delegate
        
        
        otp1 = view.otp1
        otp2 = view.otp2
        otp3 = view.otp3
        otp4 = view.otp4
        otp5 = view.otp5
        otp6 = view.otp6
      
        
        otp1.addTarget(self, action: #selector(onBeginEdit), for: .editingDidBegin)
        otp2.addTarget(self, action: #selector(onBeginEdit), for: .editingDidBegin)
        otp3.addTarget(self, action: #selector(onBeginEdit), for: .editingDidBegin)
        otp4.addTarget(self, action: #selector(onBeginEdit), for: .editingDidBegin)
        otp5.addTarget(self, action: #selector(onBeginEdit), for: .editingDidBegin)
        otp6.addTarget(self, action: #selector(onBeginEdit), for: .editingDidBegin)
        
        self.backgroundColor = UIColor.clear
        
        
        otp1.textViewDelegate = self
        otp2.textViewDelegate = self
        otp3.textViewDelegate = self
        otp4.textViewDelegate = self
        otp5.textViewDelegate = self
        otp6.textViewDelegate = self
        
        setFocus()
    }
    
    open func getOTP() -> String{
        let temp = otp1.text! + otp2.text! + otp3.text!
        return temp + otp4.text! + otp5.text! + otp6.text!
    }
    
    open func isValidOtp() -> Bool{
        
        if(getOTP().count == 6){
            return true
        }
        return false
    }
    
    @objc func onBeginEdit(textField: UITextField) {
        if let btnDelegate = delegate {
            btnDelegate.OTPControlDelegateBeginEditing(inputField: textField)
        }
    }
    
    func setFocus(){
        otp1.becomeFirstResponder()
    }
    
    func onBackPresss(textField: UITextField){
        if(textField == otp4){
            otp3.becomeFirstResponder()
        }
        else if(textField == otp6){
            otp5.becomeFirstResponder()
        }
        else if(textField == otp5){
            otp4.becomeFirstResponder()
        }
        else if(textField == otp3){
                otp2.becomeFirstResponder()
        }
        else if(textField == otp2){
            otp1.becomeFirstResponder()
        }
        else{
            textField.resignFirstResponder()
        }
    }
    
}

extension OTPControl: UITextFieldDelegate{
    
    
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // On inputing value to textfield
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\n")
        
        if ((textField.text?.count)! <= 1  && string.count > 0){
            let nextTag = textField.tag + 1;
            
            // get next responder
            var nextResponder = textField.superview?.viewWithTag(nextTag);
            
            textField.text = string;
            
            
            if (nextResponder == nil){
                nextResponder = textField.superview?.viewWithTag(1);
                textField.resignFirstResponder()
                
                if let client = delegate{
                    client.OTPEntered(otp: self.getOTP())
                }
            }
            
            let temp = otp1.text! + otp2.text! + otp3.text!
            let otpText = temp + otp4.text! + otp5.text! + otp6.text!
            if let client = delegate{
                client.OTPTyped(otpText)
            }
            
            nextResponder?.becomeFirstResponder();
            return false;
        }
        else if (textField.text?.count == 1  && string.count == 0){
            textField.text = "";
            
            let temp = otp1.text! + otp2.text! + otp3.text!
            let otpText = temp + otp4.text! + otp5.text! + otp6.text!
            if let client = delegate{
                client.OTPTyped(otpText)
            }
            
            if isBackSpace == -92 {
                print("backspace")
                if let client = delegate{
                    client.OTPEntered(otp: self.getOTP())
                }
            }
            //previousResponder?.becomeFirstResponder();
            return false;
        }
        
        return true;
    }
    
    public func keyboardInputShouldDelete(_ textField: UITextField) -> Bool {
        if( textField.text?.count == 0){
            let previousTag = textField.tag - 1;
            
            // get next responder
            var previousResponder = textField.superview?.viewWithTag(previousTag);
            
            if (previousResponder == nil){
                previousResponder = textField.superview?.viewWithTag(1);
            }
            else{
                textField.resignFirstResponder()
            }
            
            previousResponder?.becomeFirstResponder();
        }
        
        return true
    }
    
    
    open func clearPin(){
        otp1.text = ""
        otp2.text = ""
        otp3.text = ""
        otp4.text = ""
        otp5.text = ""
        otp6.text = ""
    }
    
}

class OTPTextBox: UITextField{
    
    var textViewDelegate: OTPTextViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.textAlignment = .center
        
        self.borderStyle = .line
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
        
      //  self.layer.backgroundColor = UIColor.lightGray.cgColor
//        self.layer.borderWidth = 1
//        self.layer.borderColor = UIColor.lightGray.cgColor
        
//        self.layer.opacity = 1
        
        self.textColor = UIColor.white
        
        self.backgroundColor = #colorLiteral(red: 0.2274509804, green: 0.1333333333, blue: 0.3843137255, alpha: 1) //3A2262
        
        self.isSecureTextEntry = false
        
       // self.font = customFont.getTitleFont()

    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        
        if action == #selector(paste(_:)) {
            return false
        }
        return super.canPerformAction(action, withSender: sender)
    }
    
    override func deleteBackward() {
        super.deleteBackward()
        
        textViewDelegate!.onBackPresss(textField: self)
    }
    
    
}


