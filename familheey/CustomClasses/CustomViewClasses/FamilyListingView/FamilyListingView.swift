//
//  FamilyListingView.swift
//  familheey
//
//  Created by familheey on 19/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class FamilyListingView: UITableViewHeaderFooterView {

    @IBOutlet weak var lblTitle          : UILabel!
    @IBOutlet weak var byTitle           : UILabel!
    @IBOutlet weak var frntView          : UIView!
    @IBOutlet weak var imgLogo           : UIImageView!
    @IBOutlet weak var lblType           : UILabel!
    @IBOutlet weak var lblRegion         : UILabel!
    @IBOutlet weak var btnAction         : UIButton!
    @IBOutlet weak var btnView           : UIButton!
    @IBOutlet weak var lblCreatedBy: UILabel!
    @IBOutlet weak var lblMembersCount: UILabel!
    @IBOutlet weak var lblKnown: UILabel!
    @IBOutlet weak var lblKnownHead: UILabel!
    @IBOutlet weak var lblMemberHead: UILabel!
    @IBOutlet weak var imgLocation: UIImageView!
    @IBOutlet weak var btnAction_btm: NSLayoutConstraint!
    @IBOutlet weak var lblCreated_height: NSLayoutConstraint!

    @IBOutlet weak var buttontoRoghtConstraint: NSLayoutConstraint!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
