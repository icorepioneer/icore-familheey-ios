//
//  File.swift
//  familheey
//
//  Created by familheey on 03/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import Foundation
import UIKit

class BaseClassViewController:UIViewController{
    
    //MARK:- Override viewDidLoad
    override func viewDidLoad() {
        print("baseclass")
        
        onScreenLoad()
        loadBackButton()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if(isNavigationControllerVisible()){
            self.navigationController?.setNavigationBarHidden(false, animated: false)
        }
        else{
            self.navigationController?.setNavigationBarHidden(true, animated: false)
        }
        self.tabBarController?.tabBar.isTranslucent = false
        
//        if(isTabBarControllerVisible()){
//         self.tabBarController?.tabBar.isHidden = false
//        }
//        else{
//            self.tabBarController?.tabBar.isHidden = true
//       }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isTranslucent = true
    }
    
    //MARK:- Back button visible
    func isBackBackButtonRequired() -> Bool {
        return false
    }
    
    //MARK:- NavigationB
    func isNavigationControllerVisible() -> Bool {
        return false
    }
    
    //MARK:- Tabbar
//    func isTabBarControllerVisible() -> Bool {
//        return false
//    }
    
    //MARK:- ViewController Loading
    func onScreenLoad(){
        
    }
    
    override func viewWillLayoutSubviews() {
        
    }
    
    
    
    //MARK:- Loading backButton
    private func loadBackButton(){
        if(isBackBackButtonRequired()){
            
            var ypos:CGFloat = 90
            
            if(self.view.frame.size.height < 600){
                ypos = 20
            }
            else if(self.view.frame.size.height > 600 && self.view.frame.size.height < 700){
                ypos = 30
            }
            
            
            let backButton: UIButton = UIButton()
            
            backButton.setTitle("", for: .normal)
            backButton.setImage(UIImage(named: "backArrow"), for: .normal)
            
            backButton.frame = CGRect.init(x: 15, y: ypos, width: 40, height: 40)
            self.view.addSubview(backButton)
            
            backButton.addTarget(self, action: #selector(onBack(_:)), for: .touchUpInside)
        }
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popToViewController(ofClass: HomeViewTabController.self)
//         self.navigationController?.popToRootViewController(animated: true)
    }
    
}
