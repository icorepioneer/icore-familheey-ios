//
//  FamilyListingTableViewCell.swift
//  familheey
//
//  Created by familheey on 19/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class FamilyListingTableViewCell: UITableViewCell {
    @IBOutlet weak var btnAction: UIButton!
    @IBOutlet weak var lblFamilyName: UILabel!
    @IBOutlet weak var lblFamilyType: UILabel!
    @IBOutlet weak var lblFamilyRegion: UILabel!
    @IBOutlet weak var imgLogo: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
