//
//  APIServiceManager.swift
//  lumiere
//
//  Created by Panzer Tech on 30/5/19.
//  Copyright © 2019 PanzerTech. All rights reserved.
//

import Foundation
import UIKit
import Tailor
import Alamofire

struct BusyScreen {
    let isShow:Bool
    let text:String
}

typealias resultClosure  = (_ result: SafeMappable?) -> Void
typealias sucessClosureOfArray = (_ result: NSArray?) -> Void
typealias errorClosure   = (_ error: String?) -> Void
typealias errorClosureWithCode = (_ error: String?, _ code: String?) -> Void
typealias JSONDictionary =   [String : Any]

class APIServiceManager {
    
    static let callServer = APIServiceManager()
    var busy:BusyScreen?
    
    public class func callServer(withBusy busy:BusyScreen) -> APIServiceManager {
        APIServiceManager.callServer.busy = busy
        return APIServiceManager.callServer
    }
    
    func busyOn(){
        guard let busy = self.busy else {
            return
        }
        if busy.isShow {
            ActivityIndicatorView.show(busy.text)
        }
    }
    
    func busyOff(){
        if ActivityIndicatorView.isOnScreen() {
            ActivityIndicatorView.hiding()
        }
    }
    
    class func printOnDebug(function: String = #function,response:Any)  {
        #if DEBUG
        //print("function:\(function) \n response:\(response)")
        #endif
    }
}

class Connectivity {
    class func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
}
