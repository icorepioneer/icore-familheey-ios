//
//  APIServiceModules.swift
//  lumiere
//
//  Created by Panzer Tech on 30/5/19.
//  Copyright © 2019 PanzerTech. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON


extension APIServiceManager{
    
    //MARK:- Register User
    public func registerUser(url:String, code: String, phone:String, success:@escaping resultClosure, failure:@escaping errorClosure){
        let params = [
            "country":code,
            "phone":phone
        ]
        ActivityIndicatorView.show("Loading.....")
        APIPOSTClient.doPostRequest.inPost(method: url, params: params, sucess: { (responsJson) in
            let tempArr = responsJson as! JSONDictionary
            do{
                let user = try userModel(tempArr)
                success(user)
                return
            }catch{
                print("catched")
                return
            }
        }) { (error) in
            self.busyOff()
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
    }
    
    //MARK:- OTP verification
    public func otpVerification(url:String, phone:String, otp:String, success:@escaping resultClosure, failure:@escaping errorClosure){
        let params = [
            "phone":phone,
            "otp":otp
        ]
        ActivityIndicatorView.show("Loading.....")
        APIPOSTClient.doPostRequest.inPost(method: url, params: params, sucess: { (responsJson) in
            
            let tempVeri = responsJson as! JSONDictionary
            do{
                let user = try userModel(tempVeri)
                success(user)
                return
            }catch{
                print("catched")
                return
            }
        }) { (error) in
            self.busyOff()
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
    }
    
    
    //MARK:- Resend OTP
    
    public func resendOTP(url:String, phone:String, success:@escaping resultClosure, failure:@escaping errorClosure){
        let params = [
            "phone":phone,
        ]
        ActivityIndicatorView.show("Loading.....")
        APIPOSTClient.doPostRequest.inPost(method: url, params: params, sucess: { (responsJson) in
            
            let tempVeri = responsJson as! JSONDictionary
            print(tempVeri)
            do{
                let user = try userModel(tempVeri)
                success(user)
                return
            }catch{
                print("catched")
                return
            }
        }) { (error) in
            ActivityIndicatorView.hiding()
            self.busyOff()
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
    }
    
    //MARK:- Profile Edit
    public func updateUserProfile(url:String,param:[String:String], pic:NSData, cntntType:String, success:@escaping resultClosure, failure:@escaping errorClosure){

        ActivityIndicatorView.show("Loading.....")
        APIPOSTClient.doPostRequest.inPostForMultiMediaUpload(method: url, params: param, contentType: cntntType, contentData: pic, sucess: { (response) in
            let tempEdit = response as! JSONDictionary
            print(tempEdit)
            do{
                let user = try userModel(tempEdit)
                success(user)
                return
            }catch{
                print("catched")
                return
            }
        }) { (error) in
            self.busyOff()
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
    }
    
   /* public func EditUserProfile(url:String, params:[String:Any],coverPic:NSData, logo:NSData, originalImg:NSData ,contentType:String, success:@escaping resultClosure, failure:@escaping errorClosure){
        ActivityIndicatorView.show("Loading.....")
        
        APIPOSTClient.doPostRequest.inPostForMultiMediaUploadWithMultipleImage(method: url, params: params, contentType: contentType, contentData1: coverPic, contentData2: logo, contentData3: originalImg, sucess: { (response) in
            let tempEdit = response as! JSONDictionary
            print(tempEdit)
            do{
                let user = try userModel(tempEdit)
                success(user)
                return
            }catch{
                print("catched")
                return
            }
            
        }) { (error) in
            self.busyOff()
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
        
    }*/
    
    public func EditUserProfile(url:String,param:[String:Any], success:@escaping resultClosure, failure:@escaping errorClosure){
        ActivityIndicatorView.show("Loading.....")
        
        APIPOSTClient.doPostRequest.inPostAny(method: url, params: param, sucess: { (response) in
            let tempEdit = response as! JSONDictionary
            print(tempEdit)
            do{
                let user = try userModel(tempEdit)
                success(user)
                return
            }catch{
                print("catched")
                return
            }
        }) { (error) in
             self.busyOff()
                       failure(error?.localizedDescription)
                       APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
                       return
        }
    }
    
    
    
    //MARK:- Photo upload
    public func uploadCoverImages(url:String, params:[String:String], imgData:NSData, originalPicData:NSData, contantType:String, inFor:String, isFrom:String, success:@escaping resultClosure, failure:@escaping errorClosure){
        ActivityIndicatorView.show("Uploading.....")
        
        APIPOSTClient.doPostRequest.inPostForPhotoUpload(method: url, params: params, contentType: contantType, contentData: imgData, inFor: inFor, isFrom: isFrom, contentData1:originalPicData, sucess: { (response) in
            
            let tempArr = response as! JSONDictionary
            print(tempArr)
            do{
                if isFrom.lowercased() == "user"{
                    let user = try userModel (tempArr)
                    success(user)
                }
                else{
                    let family = try familyListResponse (tempArr)
                    print(family.familyList as Any)
                    success(family)
                }
                
                return
            }catch{
                print("catched")
                return
            }
            
        }) { (error) in
            self.busyOff()
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
    }
    
    //MARK:- Create Family
    public func createFamily(url:String,param:[String:Any], success:@escaping resultClosure, failure:@escaping errorClosure){
//        let param = [
//            "group_name":name,
//            "group_category":type,
//            "base_region":baseReg,
//            "created_by":createdBy,
//            "is_active":isActive
//        ]
        ActivityIndicatorView.show("Loading.....")
        APIPOSTClient.doPostRequest.inPostAny(method: url, params: param, sucess: { (response) in
            let tempArr = response as! JSONDictionary
            print(tempArr)
            do{
                let family = try familyListResponse (tempArr)
                print(family.familyList as Any)
                success(family)
                return
            }catch{
                print("catched")
                return
            }
            
        }) { (error) in
            self.busyOff()
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
    }
    
    //MARK:- Fetch Family
    public func fetchFamily(url:String, name:String, type:String, base:String, userId:String, success:@escaping resultClosure, failure:@escaping errorClosure){
        let param = [
            "group_name":name,
            "group_category":type,
            "base_region":base,
            "user_id": userId
        ]
//        print(param)
        ActivityIndicatorView.show("Loading.....")
        APIPOSTClient.doPostRequest.inPost(method: url, params: param, sucess: { (response) in
            let tempArr = response as! JSONDictionary
            do{
                let family = try familyListResponse(tempArr)
                success(family)
                return
            }catch{
                print("catched")
                return
            }
        }) { (error) in
            self.busyOff()
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
    }
    
    //MARK:- Update Basic Family Details & Settings
    public func updateFamilyDetails(url:String, faId:String, faIntro:String, faType:String, isSearch:String, isLinkable : String, coverPic:NSData, logo:NSData, originalImg:NSData ,contentType:String, active:String, success:@escaping resultClosure, failure:@escaping errorClosure){
        
        let params = [
            "id":faId,
            "intro":faIntro,
            "searchable":isSearch,
            "is_linkable" : isLinkable,
            "group_type":faType.lowercased(),
            "user_id":UserDefaults.standard.value(forKey: "userId") as! String,
            "is_active":"true"
        ]
        
        ActivityIndicatorView.show("Loading....")
        
        APIPOSTClient.doPostRequest.inPostForMultiMediaUploadWithMultipleImage(method: url, params: params, contentType: contentType, contentData1: coverPic, contentData2: logo, contentData3: originalImg, sucess: { (response) in
            
            let tempArr = response as! JSONDictionary
            do{
                let family = try familyListResponse(tempArr)
                success(family)
                return
            }catch{
                print("catched")
                return
            }
            
        }) { (error) in
            self.busyOff()
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
    }
    
    //MARK:- Update Family
    public func editFamilyDetails(url:String, params:[String:String], coverPic:NSData, logo:NSData, originalImg:NSData, contentType:String, success:@escaping resultClosure, failure:@escaping errorClosure){
        
        ActivityIndicatorView.show("Loading....")
        
        APIPOSTClient.doPostRequest.inPostForMultiMediaUploadWithMultipleImage(method: url, params: params, contentType: contentType, contentData1: coverPic, contentData2: logo, contentData3: originalImg, sucess: { (response) in
            
            let tempArr = response as! JSONDictionary
            do{
                let family = try familyListResponse(tempArr)
                success(family)
                return
            }catch{
                print("catched")
                return
            }
            
        }) { (error) in
            self.busyOff()
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
    }
   
    
    //MARK:- Update family Intro
    public func updateFamilyIntro(url:String,faId:String,intro:String, success:@escaping resultClosure, failure:@escaping errorClosure){
        let params = [
            "id":faId,
            "intro":intro,
            "user_id":UserDefaults.standard.value(forKey: "userId") as! String
        ]
        ActivityIndicatorView.show("Loading....")
        APIPOSTClient.doPostRequest.inPost(method: url, params: params, sucess: { (response) in
            
            let tempArr = response as! JSONDictionary
            print(tempArr)
            
            do{
                let family = try familyListResponse(tempArr)
                success(family)
                return
            }catch{
                print("catched")
                return
            }
            
        }) { (error) in
            self.busyOff()
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
    }
    
    // MARK:- User Intro Edit
    public func updateUserItro(url:String, userId:String, intro:String, workedit:Bool, success:@escaping resultClosure, failure:@escaping errorClosure){
        var params = [String:String]()
        if workedit{
            params = [
                "id":userId,
                "work":intro
            ]
        }
        else
        {
            params = [
                "id":userId,
                "about":intro
            ]
        }
       
        ActivityIndicatorView.show("Loading....")
        APIPOSTClient.doPostRequest.inPost(method: url, params: params, sucess: { (response) in
            
            let tempArr = response as! JSONDictionary
            print(tempArr)
            
            do{
                let user = try ExternalUserModel(tempArr)
                success(user)
                return
            }catch{
                print("catched")
                return
            }
            
        }) { (error) in
            self.busyOff()
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
    }
    
    //MARK:- Update Settings
    public func updateFamilySettings(url:String, groupId:String, memberJoin:String, memberApproval:String, postCreate:String, postVisible:String, postApproval:String, announcementCreate:String, announcementVisible:String, announcementApproval:String, linkFamily:String, linkApproval:String,request_visibility:String , active:String, success:@escaping resultClosure, failure:@escaping errorClosure){
        
         let para = [
            "member_joining":memberJoin,
            "member_approval":memberApproval,
            "post_create":postCreate,
            "post_visibilty":postVisible,
            "post_approval":postApproval,
            "link_family":linkFamily,
            "link_approval":linkApproval,
            "group_id":groupId,
            "announcement_create":announcementCreate,
            "announcement_visibility": announcementVisible,
            "announcement_approval":announcementApproval,
            "is_active":active,"request_visibility":request_visibility
        ]
        ActivityIndicatorView.show("Loading....")
        APIPOSTClient.doPostRequest.inPost(method: url, params: para, sucess: { (response) in
            
            let tempArr = response as! JSONDictionary
            print(tempArr)
            
            do{
                let family = try familyListResponse(tempArr)
                success(family)
                return
            }catch{
                print("catched")
                return
            }
            
        }) { (error) in
            self.busyOff()
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
    }
    
    //MARK:- Join Family Request
    func joinFamilyRequest(url:String,userId:String,groupId:String ,success:@escaping resultClosure, failure:@escaping errorClosure){
        
        let param = [
            "user_id" : userId,
            "group_id" : groupId
        ]
        print(param)
        ActivityIndicatorView.show("Loading....")
        APIPOSTClient.doPostRequest.inPost(method: url, params: param, sucess: { (response) in
            
            let tempArr = response as! JSONDictionary
            print(tempArr)
            
            do{
                let searchResult = try JoinResult(tempArr)
                success(searchResult)
                return
            }catch{
                print("catched")
                return
            }
            
            
            
        }) { (error) in
            self.busyOff()
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
    }
    
    //MARK:- Search Request
    func searchRequest(url:String, param:[String:Any],fromSearchTabViewController : Bool, success:@escaping resultClosure ,failure:@escaping errorClosure){
        
//        let param = [
//            "userid" : userId,
//            "searchtxt" : searchtxt,
//            "offset":offset,
//            "limit":limit,
//            "phonenumbers":contactArr,
//            "type":type
//            ] as [String : Any]

        if fromSearchTabViewController !=  true {
           ActivityIndicatorView.show("Loading....")
        }


        APIPOSTClient.doPostRequest.inPostAny(method: url, params: param, sucess: { (response) in
            let tempArr = response as! JSONDictionary
            do{
                let searchResult = try SearchList(tempArr)
                success(searchResult)
                return
            }catch{
                print("catched")
                return
            }
        }) { (error) in
            self.busyOff()
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
      
//        APIPOSTClient.doPostRequest.inPost(method: url, params: param , sucess: { (response) in
//
//            let tempArr = response as! JSONDictionary
//            print(tempArr)
//
//            do{
//                let searchResult = try SearchList(tempArr)
//                success(searchResult)
//                return
//            }catch{
//                print("catched")
//                return
//            }
//
//
//        }) { (error) in
//            self.busyOff()
//            failure(error?.localizedDescription)
//            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
//            return
//        }
    }
    
    
    //MARK:- Listout Families
    func getFamilyList(url:String, params:[String:String] , success:@escaping resultClosure, failure:@escaping errorClosure){
        
        ActivityIndicatorView.show("Loading....")
        print(url)
        APIPOSTClient.doPostRequest.inPost(method: url, params: params, sucess: { (responseModel) in
            
            let tempArr = responseModel as! JSONDictionary
//            print(tempArr)
            do{
                let family = try familyListResponse(tempArr)
                success(family)
                return
            }catch{
                print("catched")
                return
            }
            
        }) { (error) in
            self.busyOff()
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
    }
    
    //MARK:- update user details
    func postUserDetailsUpdate(url:String, params:[String:String], success:@escaping resultClosure, failure:@escaping errorClosure){
        
//        let params = [
//            "user_id":userId
//        ]
        
        ActivityIndicatorView.show("Loading....")
        APIPOSTClient.doPostRequest.inPost(method: url, params: params, sucess: { (responseModel) in
            
            let tempArr = responseModel as! JSONDictionary
            print(tempArr)
            do{
                let response = try requestSuccessModel(tempArr)
                success(response)
                return
            }catch{
                print("catched")
                return
            }
            
        }) { (error) in
            self.busyOff()
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
    }
    
    //MARK: Get User Details
    
    func getUserDetails(url:String,userId:String,profileId:String,success:@escaping resultClosure, failure:@escaping errorClosure){
        
        let param = [
            "user_id" : userId,
            "profile_id":profileId
        ]
//        let param = [
//            "id": userId
//        ]
        ActivityIndicatorView.show("Loading....")
        APIPOSTClient.doPostRequest.inPost(method: url, params: param, sucess: { (responseModel) in
            
            let userDetails = responseModel as! JSONDictionary
            do{
                let user = try ExternalUserModel(userDetails)
                success(user)
                return
            }catch{
                print("catched")
                return
            }

            
        }) { (error) in
            
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
    }
    
    //MARK:- Family Details byId
    func getFamilyDetails(url:String, groupId:String, userId:String, success:@escaping resultClosure, failure:@escaping errorClosure){
        let params = [
            "group_id":groupId,
            "user_id":userId
        ]
        ActivityIndicatorView.show("Loading....")
        APIPOSTClient.doPostRequest.inPost(method: url, params: params, sucess: { (responseModel) in
           // print(responseModel!)
            let jsonData =  JSON(responseModel!["data"]!)
         //   print("jsonData : \(jsonData)")

            let userDetails = responseModel as! JSONDictionary
          //  print(userDetails)
            do{
                let user = try familyListResponse(userDetails)
                success(user)
                return
            }catch{
                print("catched")
                return
            }
            
            
        }) { (error) in
            
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
    }
    
    //MARK:- Get Members List from Family
    func getMembersList(url:String,groupId:String,userId:String, query:String, success:@escaping resultClosure, failure:@escaping errorClosure){
        let params = [
            "group_id":groupId,
            "crnt_user_id":userId,
            "query":query
        ]
        print(params)
        ActivityIndicatorView.show("Loading.....")
        APIPOSTClient.doPostRequest.inPost(method: url, params: params, sucess: { (response) in
            
            let tempArr = response as! JSONDictionary
            print(tempArr)
            do{
                let user = try viewFamilyMembersResult(tempArr)
                print(user)
                success(user)
                return
            }catch{
                print("catched")
                return
            }
            
        }) { (error) in
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
        
    }
    
    //MARK:- List all members from application
    func getAllMembersList(url:String,groupId:String,userId:String, query:String, success:@escaping resultClosure, failure:@escaping errorClosure){
        let params = [
            "group_id":groupId,
            "client_id":userId,
        ]
        ActivityIndicatorView.show("Loading.....")
        APIPOSTClient.doPostRequest.inPost(method: url, params: params, sucess: { (response) in
            
            let tempArr = response as! JSONDictionary
            print(tempArr)
            do{
                let user = try listAllResult(tempArr)
                print(user)
                success(user)
                return
            }catch{
                print("catched")
                return
            }
            
        }) { (error) in
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
    }
    
    //MARK:- Invite member to group
    func sendMemberInvitation(url:String, fromId:String, toId:String, groupId:String, success:@escaping resultClosure, failure:@escaping errorClosure){
        let params = [
            "groupId":groupId,
            "userId":toId,
            "from_id":fromId
        ]
        ActivityIndicatorView.show("Loading.....")
        APIPOSTClient.doPostRequest.inPost(method: url, params: params, sucess: { (response) in
            
            let tempArr = response as! JSONDictionary
            print(tempArr)
            
            do{
                let user = try inviteMemberResult(tempArr)
                success(user)
                return
            }catch{
                print("catched")
                return
            }
            
        }) { (error) in
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
    }
    
    func sendMemberInvitationArray(url:String, fromId:String, toId:NSMutableArray, groupId:String, success:@escaping resultClosure, failure:@escaping errorClosure){
        let params = [
            "groupId":groupId,
            "userId":toId,
            "from_id":fromId
            ] as [String:Any]
        print(params)
        ActivityIndicatorView.show("Loading.....")
        APIPOSTClient.doPostRequest.inPostAny(method: url, params: params, sucess: { (response) in
            let tempArr = response as! JSONDictionary
            print(tempArr)
            
            do{
                let user = try inviteMemberResult(tempArr)
                success(user)
                return
            }catch{
                print("catched")
                return
            }
        }) { (error) in
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
    }
    
    
    //MARK:- Family Link List
    func getFamilyForLinking(url:String, groupId:String, userId:String,  success:@escaping resultClosure, failure:@escaping errorClosure){
        let params = [
            "group_id":groupId,
            "user_id":userId
        ]
        ActivityIndicatorView.show("Loading.....")
        APIPOSTClient.doPostRequest.inPost(method: url, params: params, sucess: { (response) in
            
            let tempArr = response as! JSONDictionary
            print(tempArr)
            do{
                let user = try familyListResponse(tempArr)
                success(user)
                return
            }catch{
                print("catched")
                return
            }
            
        }) { (error) in
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
    }
    
    //MARK:- Save Linking
    func saveFamilyLinking(url:String, groupId:String, userId:String, toGroup:[String],  success:@escaping resultClosure, failure:@escaping errorClosure){
        
        let params = [
            "from_group":groupId,
            "to_group":toGroup,
            "requested_by":userId
            ] as? [String:Any]
        
        ActivityIndicatorView.show("Loading.....")
        APIPOSTClient.doPostRequest.inPostAny(method: url, params: params!, sucess: { (responseMdl) in
            
            let userDetails = responseMdl as! JSONDictionary
            print(userDetails)
            do{
                let response = try requestSuccessModel(userDetails)
                success(response)
                return
            }catch{
                print("catched")
                return
            }
            
        }) { (error) in
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
    }
    //MARK:- Get group
    func  getGroupsToAddMember(url:String, memberId:String, userId:String,searchTxt:String,  success:@escaping resultClosure, failure:@escaping errorClosure){
        let params = [
            "user_id":userId,
            "member_to_add":memberId,
            "query":searchTxt
        ]
        ActivityIndicatorView.show("Loading....")
        APIPOSTClient.doPostRequest.inPost(method: url, params: params, sucess: { (responseModel) in
            
            let userDetails = responseModel as! JSONDictionary
            do{
                let memberlist = try GroupsToAddMembersList(userDetails)
                success(memberlist)
                return
            }catch{
                print("catched")
                return
            }
            
            
        }) { (error) in
            
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
    }
    
    //MARK:- Get all relations
    
    func getAllRelations(url:String,type:String,query:String,  success:@escaping resultClosure, failure:@escaping errorClosure){
        
       let params = [
            "type":type,
            "query":query
        ]
        
        ActivityIndicatorView.show("Loading....")
        APIPOSTClient.doPostRequest.inPost(method: url, params: params, sucess: { (responseModel) in
            
            let response = responseModel as! JSONDictionary
            do{
                let result = try RelationsModel(response)
                success(result)
                return
            }catch{
                print("catched")
                return
            }
            
        }) { (error) in
            
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
    }
    //MARK:- Add Relation
//    func addRelationShip(url:String, primaryUser:String, secondaryUser:String,relationId:String, groupId:String, type:String, success:@escaping resultClosure, failure:@escaping errorClosure)
    func addRelationShip(url:String, params:[String:String], success:@escaping resultClosure, failure:@escaping errorClosure){
//        let params = [
//            "primary_user":primaryUser,
//            "secondary_user":secondaryUser,
//            "relation_id":relationId,
//            "type":type,
//            "group_id":groupId
//        ]
        print(params)
        ActivityIndicatorView.show("Loading....")
        APIPOSTClient.doPostRequest.inPost(method: url, params: params, sucess: { (responseModel) in
            
            let userDetails = responseModel as! JSONDictionary
            do{
                let result = try RelationUpdateResponseModel(userDetails)
                success(result)
                return
            }catch{
                print("catched")
                return
            }
            
            
        }) { (error) in
            
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
    }
    //MARK: - Update relation
    
//    func updateRelationShip(url:String, primaryUser:String, secondaryUser:String,relationId:String,tableId:String,groupId:String,type:String,  success:@escaping resultClosure, failure:@escaping errorClosure)
    
      func updateRelationShip(url:String, params:[String:String], success:@escaping resultClosure, failure:@escaping errorClosure){
//        let params = [
//            "primary_user":primaryUser,
//            "secondary_user":secondaryUser,
//            "relation_id":relationId,
//            "id":tableId
//        ]
        ActivityIndicatorView.show("Loading....")
        APIPOSTClient.doPostRequest.inPost(method: url, params: params, sucess: { (responseModel) in
            
            let userDetails = responseModel as! JSONDictionary
            do{
                let result = try RelationUpdateResponseModel(userDetails)
                success(result)
                return
            }catch{
                print("catched")
                return
            }
            
            
        }) { (error) in
            
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
    }

    //MARK:- updateGroupMaps
    func updateGroupMaps(url:String, params:[String:Any],  success:@escaping resultClosure, failure:@escaping errorClosure){
        ActivityIndicatorView.show("Loading....")
        
        APIPOSTClient.doPostRequest.inPostAny(method: url, params: params, sucess: { (responseModel) in
            
            let data = responseModel as! JSONDictionary
            do{
                let result = try requestSuccessModel(data)
                success(result)
                return
            }catch{
                print("catched")
                return
            }
            
            
        }, failure: { (error) in
            
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        })
    }
    
    //MARK:- Follow unFollow
    func familyFollowStatus(url:String, userId:String, gropId:String, success:@escaping resultClosure, failure:@escaping errorClosure){
        let params = [
            "user_id":userId,
            "group_id":gropId
        ]
        
        ActivityIndicatorView.show("Loading.....")
        APIPOSTClient.doPostRequest.inPost(method: url, params: params, sucess: { (response) in
            
            let data = response as! JSONDictionary
            print(data)
            do{
                let result = try requestSuccessModel(data)
                success(result)
                return
            }catch{
                print("catched")
                return
            }
            
        }) { (error) in
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
    }
    
    //MARK:- invitation response
    func invitationResponse(url:String, tableId:String, user_id:String, group_id:String, status:String,fromId:String, success:@escaping resultClosure, failure:@escaping errorClosure){
        let params = [
            "id":tableId,
            "user_id":user_id,
            "group_id":group_id,
            "status":status,
            "from_id":fromId
        ]
        print(params)
        ActivityIndicatorView.show("Loading....")
        APIPOSTClient.doPostRequest.inPost(method: url, params: params, sucess: { (responseModel) in
            
            let data = responseModel as! JSONDictionary
            do{
                let result = try requestSuccessModel(data)
                success(result)
                return
            }catch{
                print("catched")
                return
            }
            
            
        }) { (error) in
            
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            
            return
        }
    }
    //MARK:- Get all invitations
    func getAllinvitations(url:String, user_id:String, success:@escaping resultClosure, failure:@escaping errorClosure){
        let params = [
            "user_id":user_id,
        ]
        ActivityIndicatorView.show("Loading....")
        APIPOSTClient.doPostRequest.inPost(method: url, params: params, sucess: { (responseModel) in
            
            let data = responseModel as! JSONDictionary
            do{
                let result = try InvitationsModel(data)
                success(result)
                return
            }catch{
                print("catched")
                return
            }
            
            
        }) { (error) in
            
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
    }
    
    //MARK:- Get All Group Requests
    func getAllGroupRequest(url:String, groupId:String, success:@escaping resultClosure, failure:@escaping errorClosure){
        
        let params = [
            "group_id":groupId
        ]
        ActivityIndicatorView.show("Loading....")
        APIPOSTClient.doPostRequest.inPost(method: url, params: params, sucess: { (response) in
            
            let data = response as! JSONDictionary
            print(data)
            do{
                let result = try groupReqestResult(data)
                success(result)
                return
            }catch{
                print("catched")
                return
            }
            
            
        }) { (error) in
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
    }
    
    //MARK:- Group Request response
    func groupRequestResponse(url:String, tableId:String, userId:String, groupId:String, type:String, status:String, success:@escaping resultClosure, failure:@escaping errorClosure){
        
        let params = [
            "id":tableId,
            "type":type,
            "status":status,
            "responded_by":userId
        ]
        print(params)
        ActivityIndicatorView.show("Loading.....")
        APIPOSTClient.doPostRequest.inPost(method: url, params: params, sucess: { (response) in
            let data = response as! JSONDictionary
            do{
                let result = try requestSuccessModel(data)
                print("*****\(data)")
                success(result)
                return
            }catch{
                print("catched")
                return
            }
        }) { (error) in
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
    }
    
    //MARK:- Group Request response
    func groupRequestResponseFromNotification(url:String, from_id:String, userId:String, type_id:String, type:String, status:String, success:@escaping resultClosure, failure:@escaping errorClosure){
        
        let params = [
            "from_group":from_id,
            "to_group":type_id,
            "type":type,
            "status":status,
            "responded_by":userId
        ]
        print(params)
        ActivityIndicatorView.show("Loading.....")
        APIPOSTClient.doPostRequest.inPost(method: url, params: params, sucess: { (response) in
            let data = response as! JSONDictionary
            do{
                let result = try requestSuccessModel(data)
                print("*****\(data)")
                success(result)
                return
            }catch{
                print("catched")
                return
            }
        }) { (error) in
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
    }
    //MARK:- Group Request response from Notifications
    func groupRequestResponseNoti(url:String, userId:String, groupId:String, type:String, status:String, respondedId:String, success:@escaping resultClosure, failure:@escaping errorClosure){
        let params = [
            "user_id":userId,
            "type":type,
            "status":status,
            "responded_by":respondedId,
            "group_id":groupId
        ]
        print(params)
        ActivityIndicatorView.show("Loading.....")
        APIPOSTClient.doPostRequest.inPost(method: url, params: params, sucess: { (response) in
            let data = response as! JSONDictionary
            do{
                let result = try requestSuccessModel(data)
                print("*****\(data)")
                success(result)
                return
            }catch{
                print("catched")
                return
            }
        }) { (error) in
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
    }
    
    //MARK:- Invite by mail or phone
    func inviteMailorPhone(url:String, email:String, phone:String, userId:String, groupId:String, urlEncode:String, name:String, fromName:String, success:@escaping resultClosure, failure:@escaping errorClosure){
        let params = [
            "email":email,
            "phone":phone,
            "user_id":userId,
            "group_id":groupId,
            "name":name,
            "from_name":fromName
        ]
        
        ActivityIndicatorView.show("Loading.....")
        APIPOSTClient.doPostRequest.inPost(method: url, params: params, sucess: { (response) in
           let data = response as! JSONDictionary
            do{
                let result = try requestSuccessModel(data)
                print("*****\(data)")
                success(result)
                return
            }catch{
                print("catched")
                return
            }
            
        }) { (error) in
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
    }
    
    //MARK:- Create Folder
    func createNewFolder(url:String, folderName:String, userId:String, groupId:String, type:String, isSearchable:String, success:@escaping resultClosure, failure:@escaping errorClosure){
        let params = [
            "folder_name":folderName,
            "created_by" : userId,
            "group_id":groupId,
            "type":type,
            "is_sharable":isSearchable
        ]
        ActivityIndicatorView.show("Please wait!")
        APIPOSTClient.doPostRequest.inPost(method: url, params: params, sucess: { (response) in
            let data = response as! JSONDictionary
            print(data)
            do{
                let result = try FolderListResult(data)
                print(result)
                success(result)
                return
            }catch{
                print("catched")
                return
            }
            
        }) { (error) in
            
        }
    }
    
    //MARK:- List out Folder
    func listAllFolders(url:String, userId:String, groupId:String, success:@escaping resultClosure, failure:@escaping errorClosure){
        let params = [
            "group_id":groupId,
            "user_id":userId
        ]
        ActivityIndicatorView.show("Please wait!")
        APIPOSTClient.doPostRequest.inPost(method: url, params: params, sucess: { (response) in
            let data = response as! JSONDictionary
            print(data)
            do{
                let result = try FolderListResult(data)
                success(result)
                return
            }catch{
                print("catched")
                return
            }
            
        }) { (error) in
            failure(error?.localizedDescription)
            APIServiceManager.printOnDebug(response: "error:\(error?.localizedDescription ?? "")")
            return
        }
    }
    
    func FolderDetails(url:String, userId:String, groupId:String, folderID:String, success:@escaping resultClosure, failure:@escaping errorClosure){
        
        let params = [
            "user_id":userId,
            "group_id":groupId,
            "folder_id":folderID
        ]
        ActivityIndicatorView.show("Please wait")
        APIPOSTClient.doPostRequest.inPost(method: url, params: params, sucess: { (response) in
            let data = response as! JSONDictionary
            print(data)
            
        }) { (error) in
            
        }
    }
}

