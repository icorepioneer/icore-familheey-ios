//
//  configs.swift
//  lumiere
//
//  Created by Panzer Tech on 30/5/19.
//  Copyright © 2019 PanzerTech. All rights reserved.
//

import Foundation
import UIKit

class BaseUrl {
    
    static let isProduction:Bool = true
    // MARK:- API url Local //https://e35a6a0a870b.ngrok.io
    static let developmentUrl = "https://5eeeea60b893.ngrok.io/api/v1/familheey/"

//   http://192.168.1.70:4000 -- Praveen //82 - deepdil
    
    
    //MARK:- For Production
//    static let productionUrl  = "https://prod-commonbackend.familheey.com/api/v1/familheey/" (Not in Use)
       static let productionUrl  = "https://commonbackend.familheey.com/api/v1/familheey/"

    
    
    //MARK:- For PreProduction (Test Flight build)
//    static let productionUrl  = "https://preprod-commonbackend.familheey.com/api/v1/familheey/"

    

    //MARK:- For staging
//    static let productionUrl  = "https://staging-commonbackend.familheey.com/api/v1/familheey/"

    
    //MARK:- For Development
//    static let productionUrl  = "https://dev-commonbackend.familheey.com/api/v1/familheey/"

    
    //MARK:- Socket Url
    
    // MARK:- Socket url local
    static let SocketQa = "http://192.168.1.70:3001/"
    
    
    //MARK:- For Prod
//    static let SocketDev = "https://prod-socket-commonbackend.familheey.com/" (Not in use)
    static let SocketDev = "https://socket-3001.familheey.com/"
    
    //MARK:- For PreProduction (Test Flight Build)
//    static let SocketDev = "https://preprod-socket-3001.familheey.com/"
    
    //MARK:- For staging
//    static let SocketDev = "https://staging-socket-commonbackend.familheey.com/"

    
    //MARK:- For Dev
//     static let SocketDev = "https://dev-socket-commonbackend.familheey.com/"
    
    
    class func makeUrl() -> String {
           if isProduction {
               return productionUrl
           }else{
               return developmentUrl
           }
       }
       
       class func socketUrl() -> String {
           if isProduction {
               return SocketDev
           }else{
               return SocketQa
           }
       }
    
    static var groupLogo = "logo/"
    static var groupCover = "cover_pic/"
    static var userImage = "propic/"
    static var agenda_pic = "agenda_pic/"
    static var history_images = "history_images/"

    
    static var event_image = "event_image/"
    static var post_image = "post/"
    static var document_image = "Documents/"
    static var event_original = "event_original_image/"
    static var group_origianl = "group_original_image/"
    static var user_original = "user_original_image/"
    static var announcement = "announcement/"
    static var conversation = "file_name/"
    static var videoThumb = "video_thumb/"
    
    static var imaginaryURL = "https://dev-imaginary.familheey.com/resize?width="+"\(UIScreen.main.bounds.size.width)"+"&url="
    static var imaginaryURLForDetail = "https://dev-imaginary.familheey.com/resize?width="+"\(UIScreen.main.bounds.size.width * 3)"+"&url="
    static var imaginaryURLForDetail2X = "https://dev-imaginary.familheey.com/resize?width="+"\(UIScreen.main.bounds.size.width * 1.5)"+"&url="
    static var imaginaryURLForThumb = "https://dev-imaginary.familheey.com/resize?width=300"+"&url="


        // MARK:- Post Share Dev
//        static let PostUrlPrd = "https://dev-share.familheey.com/page/posts/"
        
        //MARK:- Post Share Stag
//        static let PostUrlPrd = "https://staging-share.familheey.com/page/posts/"
        
        //MARK:- Post Share Prod
        static let PostUrlPrd = "https://share.familheey.com/page/posts/"

}

struct Consts {
    static let osV = UIDevice.current.systemVersion
    
    static let googleClient_ID        = "46848536751-v3n4g5e0dkm21tokpco1ob363d3r7fqt.apps.googleusercontent.com"
    static let openWeatherAPI         = "e706134df1098f47edbe2809a04838ca" // Personal
    static let signupToken            = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImJkNDdjZDY1Nz"
    static let googlePlacesApiKey     = "AIzaSyCY7ATYsEBgH4LFYwPmxngMbBm2smQ7bL8"
}

struct EndPoint {
    static let signup = "register"
    static let otpVerify = "confirmotp"
    static let profileEdit = "edit_profile"
    static let resend = "resendOTP"
    
    static let createFamily           = "create_family"
    static let fetchFamily            = "fetch_group"
    static let updateFamily           = "update_family"
    static let joinFamily             = "joinFamily"
    static let updateSettings         = "updateSettings"
    static let viewFamily             = "viewFamily" //"viewFamily"
    static let search                 = "globalsearch"
    static let userDetails            = "view_profile"
    static let getFamily              = "getFamilyById"
    static let viewMember             = "viewMembers"
    static let listMembers            = "listAllMembers"
    static let linkList               = "fetch_link_family"
    static let inviteMember           = "addtogroup"//sendInvitation - for send request, addtogroup - for directly add member to group
    static let newInvite              = "addToGroup"
    static let GroupsToAddMember      = "getGroupsToAddMember"
    static let addRelation            = "addRelation"
    static let getallRelations        = "getallRelations"
    static let updateRelation         = "updateRelation"
    static let saveLink               = "request_link_family"
    static let update_groupmaps       = "update_groupmaps"
    static let unfollow               = "unFollow"
    static let follow                 = "Follow"
    static let invitationResp         = "invitationResp"
    static let listallInvitation      = "listallInvitation"
    static let groupRequestList       = "admin_view_requests"
    static let groupRequestResp       = "admin_request_action"
    static let inviteViaMsg           = "inviteViaSms"
    static let listFolder             = "listGroupFolders"
    static let createFolder           = "createFolder"
    static let viewDocs               = "viewDocuments"
    static let addUserHistory         = "addUserHistory"
}
