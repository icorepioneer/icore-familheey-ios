//
//  APIPostClient.swift
//  lumiere
//
//  Created by Panzer Tech on 30/5/19.
//  Copyright © 2019 PanzerTech. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

final class APIPOSTClient{
    
    typealias sucessClosure = (_ result: NSDictionary?) -> Void
    typealias sucessClosureOfArray = (_ result: NSArray?) -> Void
    typealias failureClosure = (_ error: Error?) -> Void
    static  let doPostRequest = APIPOSTClient()
    private var sessionManager = Alamofire.SessionManager.default
    
    private init(){
        sessionManager.adapter = self
        sessionManager.retrier = self
    }
    
    
    public func inPost(url:String,params:[String:String],sucess:@escaping sucessClosure, failure:@escaping failureClosure){

        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 60 // seconds
        configuration.timeoutIntervalForResource = 60
        let cookieJar = HTTPCookieStorage.shared
        for cookie in cookieJar.cookies! {
            print(cookie.name+"="+cookie.value)
        }
        
        let token = UserDefaults.standard.string(forKey: "app_token") ?? ""
        print("token---\(token)")
        var headders: HTTPHeaders = [:]
        
        
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
        let tempDic = ["app_name":"familheey","app_version":version,"os":"iOS"]
        let tempJSON = JSON(arrayLiteral: tempDic)
        print(tempJSON.stringValue)
        if( token != ""){
            if let id =  UserDefaults.standard.value(forKey: "userId") as? String{
                headders = [
                    "Accept": "application/json, text/plain, */*",
                    "Content-Type" :"application/json ; charset=utf-8",
                    "Authorization" : "Bearer " + token,  // customise Parameter and Value
                    "logined_user_id":id,
                    "app_info":tempJSON.stringValue
                ]
            }
            else{
                headders = [
                    "Accept": "application/json, text/plain, */*",
                    "Content-Type" :"application/json ; charset=utf-8",
                    "Authorization" : "Bearer " + token,
                    "app_info":tempJSON.stringValue // customise Parameter and Value
                ]
            }
        }
        else{
            headders = [
                "Accept": "application/json, text/plain, */*",
                "Content-Type" :"application/json; charset=utf-8"
            ]
        }
        
        let postRequest = self.sessionManager.request(url, method: .post, parameters: params as [String:Any], encoding: JSONEncoding.default, headers:headders)
        
        postRequest.validate().responseJSON { (responseObject) in
            
            print("####### 3 API response :", responseObject,"\n")
            
            let cookieJar = HTTPCookieStorage.shared
            
            for cookie in cookieJar.cookies! {
                print(cookie.name+"="+cookie.value)
            }
            print("_________________________________________________\(responseObject.timeline.totalDuration)\n\n\n")
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            switch  responseObject.result {
            case .success:
                let status = responseObject.response?.statusCode
                if (status == 200 || status == 201) {
                    var json = responseObject.result.value as! [String:Any]
                    json.updateValue(status as Any, forKey: "status_code")
                    
                    sucess(json as NSDictionary?)
                }
                else if status == 401{
                }
                else{
                    var json = responseObject.result.value as! [String:Any]
                    json.updateValue(status as Any, forKey: "status_code")
                
                    sucess(json as NSDictionary?)
                }
                return
            case .failure:
                let status = responseObject.response?.statusCode
                
                if status == 401{
                    return
                }
                else{
                    let error = responseObject.result.error
                    failure(error)
                    return
                }

            }
            
            }.responseString { (jsonString) in
                APIServiceManager.printOnDebug(response: jsonString)
        }
    }
    
    public func inPostAny(url:String,params:[String:Any],sucess:@escaping sucessClosure, failure:@escaping failureClosure){
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 60 // seconds
        configuration.timeoutIntervalForResource = 60
        let cookieJar = HTTPCookieStorage.shared
        for cookie in cookieJar.cookies! {
            print(cookie.name+"="+cookie.value)
        }
        
        let token = UserDefaults.standard.string(forKey: "app_token") ?? ""
        var headders: HTTPHeaders = [:]
        
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
        let tempDic = ["app_name":"familheey","app_version":version,"os":"iOS"]
        let tempJSON = JSON(arrayLiteral: tempDic)
        print(tempJSON.stringValue)
        
        if( token != ""){
           if let id =  UserDefaults.standard.value(forKey: "userId") as? String{
                headders = [
                    "Accept": "application/json, text/plain, */*",
                    "Content-Type" :"application/json ; charset=utf-8",
                    "Authorization" : "Bearer " + token,  // customise Parameter and Value
                    "logined_user_id":id,
                    "app_info":tempJSON.stringValue
                ]
            }
            else{
                headders = [
                    "Accept": "application/json, text/plain, */*",
                    "Content-Type" :"application/json ; charset=utf-8",
                    "Authorization" : "Bearer " + token,
                    "app_info":tempJSON.stringValue// customise Parameter and Value
                ]
            }
        }
        else{
            headders = [
                "Accept": "application/json, text/plain, */*",
                "Content-Type" :"application/json; charset=utf-8",
                "app_info":tempJSON.stringValue
            ]
        }
        
        let postRequest = self.sessionManager.request(url, method: .post, parameters: params as [String:Any], encoding: JSONEncoding.default, headers:headders)
        
        postRequest.validate().responseJSON { (responseObject) in
            
            print("####### 4 API response :", responseObject,"\n")
            
            let cookieJar = HTTPCookieStorage.shared
            
            for cookie in cookieJar.cookies! {
                print(cookie.name+"="+cookie.value)
            }
            print("_________________________________________________\n\n\n")
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            switch  responseObject.result {
            case .success:
                let status = responseObject.response?.statusCode
                if (status == 200 || status == 201) {
                    var json = responseObject.result.value as! [String:Any]
                    json.updateValue(status as Any, forKey: "status_code")
                    
                    sucess(json as NSDictionary?)
                }//else if status == 401{
                else
                {
                   var json = responseObject.result.value as! [String:Any]
                    json.updateValue(status as Any, forKey: "status_code")
                    sucess(json as! NSDictionary?)
                }
                return
            case .failure:
                let error = responseObject.result.error
                failure(error)
                return
            }
            
            }.responseString { (jsonString) in
                APIServiceManager.printOnDebug(response: jsonString)
        }
    }
    
    public func inPostAsArray(url:String,params:[String:Any],sucess:@escaping sucessClosure, failure:@escaping failureClosure){
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 60 // seconds
        configuration.timeoutIntervalForResource = 60
        let cookieJar = HTTPCookieStorage.shared
        for cookie in cookieJar.cookies! {
            print(cookie.name+"="+cookie.value)
        }
        
        let token = UserDefaults.standard.string(forKey: "app_token") ?? ""
        var headders: HTTPHeaders = [:]
        
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
        let tempDic = ["app_name":"familheey","app_version":version,"os":"iOS"]
        let tempJSON = JSON(arrayLiteral: tempDic)
        print(tempJSON.stringValue)
        
        if( token != ""){
           if let id =  UserDefaults.standard.value(forKey: "userId") as? String{
                headders = [
                    "Accept": "application/json, text/plain, */*",
                    "Content-Type" :"application/json ; charset=utf-8",
                    "Authorization" : "Bearer " + token,  // customise Parameter and Value
                    "logined_user_id":id,
                    "app_info":tempJSON.stringValue
                ]
            }
            else{
                headders = [
                    "Accept": "application/json, text/plain, */*",
                    "Content-Type" :"application/json ; charset=utf-8",
                    "Authorization" : "Bearer " + token,  // customise Parameter and Value
                    "app_info":tempJSON.stringValue
                ]
            }
        }
        else{
            headders = [
                "Accept": "application/json, text/plain, */*",
                "Content-Type" :"application/json; charset=utf-8",
                "app_info":tempJSON.stringValue
            ]
        }
        
        let postRequest = self.sessionManager.request(url, method: .post, parameters: params as [String:Any], encoding: JSONEncoding.default, headers:headders)
        
        postRequest.validate().responseJSON { (responseObject) in
            
            print("####### 1 API response :", responseObject,"\n")
            
            let cookieJar = HTTPCookieStorage.shared
            
            for cookie in cookieJar.cookies! {
                print(cookie.name+"="+cookie.value)
            }
            print("_________________________________________________\n\n\n")
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            switch  responseObject.result {
            case .success:
                let status = responseObject.response?.statusCode
                if (status == 200 || status == 201) {
                    let json = responseObject.result.value
                    
                    sucess(json as! NSDictionary?)
                }else if status == 401{
                    let json = responseObject.result.value
                    sucess(json as! NSDictionary?)
                }
                return
            case .failure:
                let error = responseObject.result.error
                failure(error)
                return
            }
            
            }.responseString { (jsonString) in
                APIServiceManager.printOnDebug(response: jsonString)
        }
    }
    
    
    public func inPostForArray(url:String,params: [String:String],sucess:@escaping sucessClosureOfArray, failure:@escaping failureClosure){
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 60 // seconds
        configuration.timeoutIntervalForResource = 60
        let cookieJar = HTTPCookieStorage.shared
        
        for cookie in cookieJar.cookies! {
             print(cookie.name+"="+cookie.value)
        }
        let token = UserDefaults.standard.string(forKey: "app_token") ?? ""
        
        var headders: HTTPHeaders = [:]
        
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
        let tempDic = ["app_name":"familheey","app_version":version,"os":"iOS"]
        let tempJSON = JSON(arrayLiteral: tempDic)
        print(tempJSON.stringValue)
        
        if( token != ""){
            if let id =  UserDefaults.standard.value(forKey: "userId") as? String{
                headders = [
                    "Accept": "application/json, text/plain, */*",
                    "Content-Type" :"application/json ; charset=utf-8",
                    "Authorization" : "Bearer " + token,  // customise Parameter and Value
                    "logined_user_id":id
                ]
            }
            else{
                headders = [
                    "Accept": "application/json, text/plain, */*",
                    "Content-Type" :"application/json ; charset=utf-8",
                    "Authorization" : "Bearer " + token  // customise Parameter and Value
                ]
            }
        }
        else{
            headders = [
                "Accept": "application/json, text/plain, */*",
                "Content-Type" :"application/json; charset=utf-8"
            ]
        }
        
        let postRequest = self.sessionManager.request(url, method: .post, parameters: params as [String:String], encoding: JSONEncoding.default, headers:headders)
        
        postRequest.validate().responseJSON { (responseObject) in
            
            print("####### 2 API response :", responseObject,"\n")
            
            let cookieJar = HTTPCookieStorage.shared
            for cookie in cookieJar.cookies! {
                print(cookie.name+"="+cookie.value)
            }
            print("_________________________________________________\n\n\n")
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            switch  responseObject.result {
            case .success:
                
                let json = responseObject.result.value
                
                sucess(json as! NSArray?)
                return
            case .failure:
                let error = responseObject.result.error
                failure(error)
                return
            }
            }.responseString { (jsonString) in
                APIServiceManager.printOnDebug(response: jsonString)
        }
    }
    

    func inPostForMultiMediaUpload(url:String,params:[String:String], contentType:String, contentData: NSData, sucess:@escaping sucessClosure,failure:@escaping failureClosure){
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 60 // seconds
        configuration.timeoutIntervalForResource = 60
        let multiMediaData = Data(referencing: contentData)
        
        let temp = url.components(separatedBy: "/") as NSArray
        let tempLast = temp.lastObject as! String
        
        let token = UserDefaults.standard.string(forKey: "app_token") ?? ""
        var headder: HTTPHeaders = [:]
        
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
        let tempDic = ["app_name":"familheey","app_version":version,"os":"iOS"]
        let tempJSON = JSON(arrayLiteral: tempDic)
        print(tempJSON.stringValue)
        
        if( token != ""){
           if let id =  UserDefaults.standard.value(forKey: "userId") as? String{
                headder = [
                    "Accept": "application/json, text/plain, */*",
                    "Content-Type" :"application/json ; charset=utf-8",
                    "Authorization" : "Bearer " + token,  // customise Parameter and Value
                    "logined_user_id":id,
                    "app_info":tempJSON.stringValue
                ]
            }
            else{
                headder = [
                    "Accept": "application/json, text/plain, */*",
                    "Content-Type" :"application/json ; charset=utf-8",
                    "Authorization" : "Bearer " + token,
                    "app_info":tempJSON.stringValue // customise Parameter and Value
                ]
            }
        }
        else{
            headder = [
                "Accept": "multipart/form-data",
                "Content-Type" :"application/json; charset=utf-8",
                "app_info":tempJSON.stringValue
            ]
        }
        
        _ = sessionManager.upload(
            multipartFormData: { multipartFormData in
                
                if contentType == "Image" {
                    if (multiMediaData.count > 0) {
                        if(tempLast == "profile"){
                            multipartFormData.append(multiMediaData, withName: "passport_photo", fileName: "image.jpg", mimeType:  "image/jpg/png/jpeg")
                        }
                        else{
                            multipartFormData.append(multiMediaData, withName: "propic", fileName: "image.jpg", mimeType:  "image/jpg/png/jpeg")
                        }
                    }
                }
                else if contentType == "Video" {
                    if (multiMediaData.count > 0) {
                        multipartFormData.append(multiMediaData, withName: "videoName", fileName: "video.mov", mimeType:  "video/quicktime/m4v")
                    }
                }
                else if contentType == "Audio" {
                    if (multiMediaData.count > 0) {
                        multipartFormData.append(multiMediaData, withName: "file", fileName: "audio.m4a", mimeType:  "audio/m4a/mp3")
                    }
                }
                
                for (key, value) in params {
                    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
        },
            to: url,
            headers:headder,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.validate().responseJSON {
                        responseObject in
                        let status = responseObject.response?.statusCode
                        if (status == 200 || status == 201) {
                            var json = responseObject.result.value as! [String:Any]
                            json.updateValue(status as Any, forKey: "status_code")
                            print("JSON: \(json)")
                            sucess(json as NSDictionary?)
                        }//else if status == 401{
                        else{
//                            do {
//                                 var json = try responseObject.result.error as! [String:Any]
//                                    json.updateValue(status as Any, forKey: "status_code")
//                                    print("JSON: \(json)")
//                                    sucess(json as NSDictionary?)
//                            }catch{
//                                print("Catched...")
//                            }
                        }
                        
//                        if json == nil{
//                            failure(responseObject.result.error)
//                        }
//                        else{
//                        sucess(json as! NSDictionary?)
//                        }
                        return
                    }
                case .failure(let encodingError):
                    let error = encodingError
                    print("error: \(error)")
                    failure(error)
                    return
                }
        }
        )
    }
    
    func inPostForMultiMediaUploadWithMultipleImage(url:String,params:[String:String], contentType:String, contentData1: NSData, contentData2:NSData, contentData3:NSData, sucess:@escaping sucessClosure,failure:@escaping failureClosure){
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 60 // seconds
        configuration.timeoutIntervalForResource = 60
        let multiMediaData = Data(referencing: contentData1)
        let multiMediaData1 = Data(referencing: contentData2)
        let multiMediaData3 = Data(referencing: contentData3)
        
        let temp = url.components(separatedBy: "/") as NSArray
        let tempLast = temp.lastObject as! String
        
        let token = UserDefaults.standard.string(forKey: "app_token") ?? ""
        var headder: HTTPHeaders = [:]
        
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
        let tempDic = ["app_name":"familheey","app_version":version,"os":"iOS"]
        let tempJSON = JSON(arrayLiteral: tempDic)
        print(tempJSON.stringValue)
        
        if( token != ""){
            if let id =  UserDefaults.standard.value(forKey: "userId") as? String{
                headder = [
                    "Accept": "application/json, text/plain, */*",
                    "Content-Type" :"application/json ; charset=utf-8",
                    "Authorization" : "Bearer " + token,  // customise Parameter and Value
                    "logined_user_id":id,
                    "app_info":tempJSON.stringValue
                ]
            }
            else{
                headder = [
                    "Accept": "application/json, text/plain, */*",
                    "Content-Type" :"application/json ; charset=utf-8",
                    "Authorization" : "Bearer " + token,  // customise Parameter and Value
                    "app_info":tempJSON.stringValue
                ]
            }
        }
        else{
            headder = [
                "Accept": "multipart/form-data",
                "Content-Type" :"application/json; charset=utf-8",
                "app_info":tempJSON.stringValue
            ]
        }
        
        _ = sessionManager.upload(
            multipartFormData: { multipartFormData in
                
                if contentType == "Image" {
                    if (multiMediaData.count > 0) {
                        if(tempLast == "edit_profile"){
                            multipartFormData.append(multiMediaData, withName: "cover_pic", fileName: "image.jpg", mimeType:  "image/jpg/png/jpeg")
                            
                            multipartFormData.append(multiMediaData1, withName: "propic", fileName: "image.jpg", mimeType:  "image/jpg/png/jpeg")
                            
                            multipartFormData.append(multiMediaData3, withName: "user_original_image", fileName: "image.jpg", mimeType:  "image/jpg/png/jpeg")
                        }
                        else{
                            multipartFormData.append(multiMediaData, withName: "cover_pic", fileName: "image.jpg", mimeType:  "image/jpg/png/jpeg")
                            
                            multipartFormData.append(multiMediaData1, withName: "logo", fileName: "image.jpg", mimeType:  "image/jpg/png/jpeg")
                            
                            multipartFormData.append(multiMediaData3, withName: "group_original_image", fileName: "image.jpg", mimeType:  "image/jpg/png/jpeg")
                        }
                    }
                }
                else if contentType == "Video" {
                    if (multiMediaData.count > 0) {
                        multipartFormData.append(multiMediaData, withName: "videoName", fileName: "video.mov", mimeType:  "video/quicktime/m4v")
                    }
                }
                else if contentType == "Audio" {
                    if (multiMediaData.count > 0) {
                        multipartFormData.append(multiMediaData, withName: "file", fileName: "audio.m4a", mimeType:  "audio/m4a/mp3")
                    }
                }
                
                for (key, value) in params {
                    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
        },
            to: url,
            headers:headder,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.validate().responseJSON {
                        responseObject in
                        let status = responseObject.response?.statusCode
                        if (status == 200 || status == 201) {
                            var json = responseObject.result.value as! [String:Any]
                            json.updateValue(status as Any, forKey: "status_code")
                            print("JSON: \(json)")
                            sucess(json as NSDictionary?)
                        }//else if status == 401{
                        //*********jeena 05-02-2021** to solve crash issue*********
                        else if status == nil{
                            failure(responseObject.result.error)
                        }
                        //***********end jeena **************//
                        else{
                            print(responseObject.result)
                            var json = [String:Any]()
                            if responseObject.result.error != nil{
                              json  = responseObject.result.error as! [String:Any]
                            }
                            json.updateValue(status as Any, forKey: "status_code")
                            print("JSON: \(json)")
                            sucess(json as NSDictionary?)
                        }
                        
                        //                        if json == nil{
                        //                            failure(responseObject.result.error)
                        //                        }
                        //                        else{
                        //                        sucess(json as! NSDictionary?)
                        //                        }
                        return
                    }
                case .failure(let encodingError):
                    let error = encodingError
                    print("error: \(error)")
                    failure(error)
                    return
                }
        }
        )
    }
    
    func inPostForPhotoUpload(url:String,params:[String:String], contentType:String, contentData: NSData, inFor:String, contentData1:NSData, isFrom:String, sucess:@escaping sucessClosure,failure:@escaping failureClosure){
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 60 // seconds
        configuration.timeoutIntervalForResource = 60
        let multiMediaData = Data(referencing: contentData)
        let multiMediaData1 = Data(referencing: contentData1)
        
        let temp = url.components(separatedBy: "/") as NSArray
        let tempLast = temp.lastObject as! String
        
        let token = UserDefaults.standard.string(forKey: "app_token") ?? ""
        var headder: HTTPHeaders = [:]
        
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
        let tempDic = ["app_name":"familheey","app_version":version,"os":"iOS"]
        let tempJSON = JSON(arrayLiteral: tempDic)
        print(tempJSON.stringValue)
        
        if( token != ""){
            if let id =  UserDefaults.standard.value(forKey: "userId") as? String{
                headder = [
                    "Accept": "application/json, text/plain, */*",
                    "Content-Type" :"application/json ; charset=utf-8",
                    "Authorization" : "Bearer " + token,  // customise Parameter and Value
                    "logined_user_id":id,
                    "app_info":tempJSON.stringValue
                ]
            }
            else{
                headder = [
                    "Accept": "application/json, text/plain, */*",
                    "Content-Type" :"application/json ; charset=utf-8",
                    "Authorization" : "Bearer " + token,  // customise Parameter and Value
                    "app_info":tempJSON.stringValue
                ]
            }
        }
        else{
            headder = [
                "Accept": "multipart/form-data",
                "Content-Type" :"application/json; charset=utf-8",
                "app_info":tempJSON.stringValue
            ]
        }
        
        _ = sessionManager.upload(
            multipartFormData: { multipartFormData in
                
                if contentType == "Image" {
                    
                    if isFrom.lowercased() == "user"{
                        if inFor.lowercased() == "profile"{
                            if (multiMediaData.count > 0) {
                              multipartFormData.append(multiMediaData, withName: "propic", fileName: "image.jpg", mimeType:  "image/jpg/png/jpeg")
                            }
                        }
                        else{
                            if (multiMediaData.count > 0) {
                                multipartFormData.append(multiMediaData, withName: "cover_pic", fileName: "image.jpg", mimeType:  "image/jpg/png/jpeg")
                            }
                            if (multiMediaData1.count > 0) {
                                multipartFormData.append(multiMediaData1, withName: "user_original_image", fileName: "image.jpg", mimeType:  "image/jpg/png/jpeg")
                            }
                        }
                    }
                    else{
                        if inFor.lowercased() == "logo"{
                            if (multiMediaData.count > 0) {
                                multipartFormData.append(multiMediaData, withName: "logo", fileName: "image.jpg", mimeType:  "image/jpg/png/jpeg")
                            }
                        }
                        else{
                            if (multiMediaData.count > 0) {
                                multipartFormData.append(multiMediaData, withName: "cover_pic", fileName: "image.jpg", mimeType:  "image/jpg/png/jpeg")
                            }
                            if (multiMediaData1.count > 0) {
                                multipartFormData.append(multiMediaData1, withName: "group_original_image", fileName: "image.jpg", mimeType:  "image/jpg/png/jpeg")
                            }
                        }
                    }
                }
                else if contentType == "Video" {
                    if (multiMediaData.count > 0) {
                        multipartFormData.append(multiMediaData, withName: "videoName", fileName: "video.mov", mimeType:  "video/quicktime/m4v")
                    }
                }
                else if contentType == "Audio" {
                    if (multiMediaData.count > 0) {
                        multipartFormData.append(multiMediaData, withName: "file", fileName: "audio.m4a", mimeType:  "audio/m4a/mp3")
                    }
                }
                
                for (key, value) in params {
                    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
        },
            to: url,
            headers:headder,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.validate().responseJSON {
                        responseObject in
                        let status = responseObject.response?.statusCode
                        if (status == 200 || status == 201) {
                            var json = responseObject.result.value as! [String:Any]
                            json.updateValue(status as Any, forKey: "status_code")
                            print("JSON: \(json)")
                            sucess(json as NSDictionary?)
                        }//else if status == 401{
                        else{
                            var json = [String:Any]()
//                            if responseObject.result.error != nil{
//                                json  = responseObject.result.error as! [String:Any]
//                            }
                            json.updateValue(status as Any, forKey: "status_code")
//                            var json = responseObject.result.error as! [String:Any]
//                            json.updateValue(status as Any, forKey: "status_code")
                            print("JSON: \(json)")
                            sucess(json as NSDictionary?)
                        }
                        
                        //                        if json == nil{
                        //                            failure(responseObject.result.error)
                        //                        }
                        //                        else{
                        //                        sucess(json as! NSDictionary?)
                        //                        }
                        return
                    }
                case .failure(let encodingError):
                    let error = encodingError
                    print("error: \(error)")
                    failure(error)
                    return
                }
        }
        )
    }
    
}

extension APIPOSTClient{

    public func inPost(method:String, params:[String:String],sucess:@escaping sucessClosure,failure:@escaping failureClosure){
        
        self.inPost(url: BaseUrl.makeUrl()+method, params: params, sucess: { (response) in
            sucess(response)
        }) { (error) in
            failure(error)
        }
    }
    
    public func inPostAny(method:String, params:[String:Any],sucess:@escaping sucessClosure,failure:@escaping failureClosure){
        
        self.inPostAny(url: BaseUrl.makeUrl()+method, params: params, sucess: { (response) in
            sucess(response)
        }) { (error) in
            failure(error)
        }
    }
    
    public func inPostAsArray(method:String, params:[String:Any],sucess:@escaping sucessClosure,failure:@escaping failureClosure){
        
        self.inPostAsArray(url: BaseUrl.makeUrl()+method, params: params, sucess: { (response) in
            sucess(response)
        }) { (error) in
            failure(error)
        }
    }
    
    public func inPostForArray(method:String, params:[String:String],sucess:@escaping sucessClosureOfArray,failure:@escaping failureClosure){
        
        self.inPostForArray(url: BaseUrl.makeUrl()+method, params: params, sucess: { (response) in
            sucess(response)
        }) { (error) in
            failure(error)
        }
    }
    
    func inPostForMultiMediaUpload(method:String,params:[String:String], contentType:String, contentData: NSData, sucess:@escaping sucessClosure,failure:@escaping failureClosure){

        inPostForMultiMediaUpload(url: BaseUrl.makeUrl()+method, params: params, contentType:contentType, contentData: contentData, sucess: { (response) in
            sucess(response)
        }) { (error) in
            failure(error)
        }
    }
    
    func inPostForMultiMediaUploadWithMultipleImage(method:String,params:[String:String], contentType:String, contentData1: NSData, contentData2: NSData, contentData3: NSData, sucess:@escaping sucessClosure,failure:@escaping failureClosure){
        
        inPostForMultiMediaUploadWithMultipleImage(url:  BaseUrl.makeUrl()+method, params: params as! [String : String], contentType: contentType, contentData1: contentData1, contentData2: contentData2,contentData3: contentData3, sucess: { (response) in
            sucess(response)
        }) { (error) in
            failure(error)
        }
        
    }

    
    func inPostForPhotoUpload(method:String,params:[String:String], contentType:String, contentData: NSData, inFor:String, isFrom:String, contentData1:NSData, sucess:@escaping sucessClosure,failure:@escaping failureClosure){
   
        inPostForPhotoUpload(url: BaseUrl.makeUrl()+method, params: params, contentType: contentType, contentData: contentData, inFor: inFor, contentData1: contentData1, isFrom: isFrom, sucess: { (response) in
            sucess(response)
        }) { (error) in
            failure(error)
        }
    }
}

extension APIPOSTClient: RequestAdapter, RequestRetrier{
        func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
            print("-------Invalid \(urlRequest)")
            
            var urlRequest = urlRequest
            let token = UserDefaults.standard.string(forKey: "app_token") ?? ""
            
//            urlRequest.headers.add(.authorization(bearerToken: token))
            urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")

            return urlRequest
        }
        
        func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion) {
            print("---------Invalid")
            if let response = request.task?.response as? HTTPURLResponse, response.statusCode == 401{
                Helpers.getAccessToken { (accessToken) in
                    print(accessToken!)
                    if !accessToken!.isEmpty{
                        setUserDefaultValues.setUserDefaultValue(userDefaultValue:accessToken!, userDefaultKey: "app_token")
                        completion(true,0.0)
                    }
                }
            }
            else{
                completion(false,0.0)
            }
        }
        
}
