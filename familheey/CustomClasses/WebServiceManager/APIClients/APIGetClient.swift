//
//  APIGetClient.swift
//  lumiere
//
//  Created by Panzer Tech on 30/5/19.
//  Copyright © 2019 PanzerTech. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

final class APIGetClient{
    
    typealias sucessClosure = (_ result: NSDictionary?) -> Void
    typealias sucessClosureOfArray = (_ result: NSArray?) -> Void
    typealias sucessClosureOfArrayType = (_ result: [String:Any]?) -> Void
    typealias failureClosure = (_ error: Error?) -> Void
    static  let doGetRequest = APIGetClient()
    private var sessionManager = Alamofire.SessionManager.default
    
    public func inGetReq(url:String,params:[String:String],sucess:@escaping sucessClosure,failure:@escaping failureClosure){
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 60 // seconds
        configuration.timeoutIntervalForResource = 60
        
        let sessionManagerNew = Alamofire.SessionManager(configuration: configuration)
        
        let token = UserDefaults.standard.string(forKey: "app_token") ?? ""
        var headders: HTTPHeaders = [:]
        
        if( token != ""){
            headders = [
                "Accept": "application/json, text/plain, */*",
                "Content-Type" :"application/json; charset=utf-8",
                "Authorization" : "Bearer " + token  // customise Parameter and Value
            ]
        }
        else{
            headders = [
                "Accept": "application/json, text/plain, */*",
                "Content-Type" :"application/json; charset=utf-8"
            ]
        }
        
        let getRequest = sessionManagerNew.request(url, method: .get, parameters: params, encoding: URLEncoding.queryString , headers: headders)
        
            getRequest.responseJSON { (responseObject) in
            
            print("####### API response :", responseObject,"\n")
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            switch  responseObject.result {
            case .success:
                let status = responseObject.response?.statusCode
                if (status == 200 || status == 201) {
                    var json = responseObject.result.value as! [String:Any]
                    json.updateValue(status as Any, forKey: "status_code")
                    
                    sucess(json as NSDictionary?)
                }
                sessionManagerNew.session.flush {
                }
                return
            case .failure:
                let error = responseObject.result.error
                failure(error)
                sessionManagerNew.session.flush {
                }
                return
            }
        }
    }
    
    public func inGetReqForArray(url:String,params:[String:String],sucess:@escaping sucessClosureOfArray,failure:@escaping failureClosure){
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 60 // seconds
        configuration.timeoutIntervalForResource = 60
        
        let sessionManagerNew = Alamofire.SessionManager(configuration: configuration)
        let token = UserDefaults.standard.string(forKey: "app_token") ?? ""
        
        var headders: HTTPHeaders = [:]
        
        if( token != ""){
            headders = [
                "Accept": "application/json, text/plain, */*",
                "Content-Type" :"application/json; charset=utf-8",
                "Authorization" : "Bearer " + token  // customise Parameter and Value
            ]
        }
        else{
            headders = [
                "Accept": "application/json, text/plain, */*",
                "Content-Type" :"application/json; charset=utf-8"
            ]
        }

        let getRequest = sessionManagerNew.request(url, method: .get, parameters: params, encoding: URLEncoding.queryString, headers: headders)
        getRequest.responseJSON { (responseObject) in
            
            print("####### API response :", responseObject,"\n")
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            switch  responseObject.result {
            case .success:
                
                let status = responseObject.response?.statusCode
                if (status == 200 || status == 201) {
                    let json = responseObject.result.value
                    
                    sucess(json as! NSArray?)
                }else{
                    ActivityIndicatorView.hiding()
                }
                sessionManagerNew.session.flush {
                    
                }

                return
            case .failure:
                let error = responseObject.result.error
                failure(error)
                sessionManagerNew.session.flush {
                    
                }
                return
            }
        }
    }
    
    public func inGetWeather(url:String,params:[String:String], success:@escaping sucessClosure, failure:@escaping failureClosure){
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 60 // seconds
        configuration.timeoutIntervalForResource = 60
        
        let sessionManagerNew = Alamofire.SessionManager(configuration: configuration)
        let token = UserDefaults.standard.string(forKey: "app_token") ?? ""
        
        var headders: HTTPHeaders = [:]
        
        if( token != ""){
            headders = [
                "Accept": "application/json, text/plain, */*",
                "Content-Type" :"application/json; charset=utf-8",
                "Authorization" : "Bearer " + token  // customise Parameter and Value
            ]
        }
        else{
            headders = [
                "Accept": "application/json, text/plain, */*",
                "Content-Type" :"application/json; charset=utf-8"
            ]
        }
        
        
        let getRequest = sessionManagerNew.request(url, method: .get, parameters: params, encoding: URLEncoding.queryString, headers: nil )
        getRequest.responseJSON { (responseObject) in
          //  print("####### API response :", responseObject,"\n")
            
            switch  responseObject.result {
            case .success:
                
                let status = responseObject.response?.statusCode
                if (status == 200 || status == 201) {
                    let json = responseObject.result.value as? NSDictionary
                   // let temp = json?.value(forKey: "list") as! [[String:Any]]
                    success(json)
                }else{
                    ActivityIndicatorView.hiding()
                }
                sessionManagerNew.session.flush {
                    
                }
                
                return
            case .failure:
                let error = responseObject.result.error
                failure(error)
                sessionManagerNew.session.flush {
                    
                }
                return
            }
        }
    }
}

extension APIGetClient{
    
    public func inGet(method:String, params:[String:String],sucess:@escaping sucessClosure,failure:@escaping failureClosure){
        
        self.inGetReq(url: BaseUrl.makeUrl()+method, params: params, sucess: { (response) in
            sucess(response)
        }) { (error) in
            failure(error)
        }
    }
    
    public func inGetReqForArray(method:String, params:[String:String],sucess:@escaping sucessClosureOfArray,failure:@escaping failureClosure){
        
        self.inGetReqForArray(url: BaseUrl.makeUrl()+method, params: params, sucess: { (response) in
            sucess(response)
        }) { (error) in
            failure(error)
        }
    }
    
}

