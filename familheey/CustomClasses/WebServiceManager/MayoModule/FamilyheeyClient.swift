//
//  Services.swift
//  GokulMoyaSample
//
//  Created by Innovation Incubator on 26/03/19.
//  Copyright © 2019 Innovation Incubator. All rights reserved.
//

import Foundation
import Moya
import Alamofire
import SwiftyJSON



enum FamilyheeyApi {

    case familyJoinFromNoti(parameter        : Parameters)
    case get_notification_settings(parameter        : Parameters)

    case addUserHistory(parameter                   : Parameters)
    case registerToken(parameter                    : Parameters)
    case updateFamilyWithHistory(parameter          : Parameters)
    case onBoardCheck(parameter                     : Parameters)
    
    case createEvent(parameter                      : Parameters)
    case createEventUpdate(parameter                : Parameters)
    case EventList(parameter                        : Parameters)
    case InviteEvents(parameter                     : Parameters)
    case CreatedMeEvents(parameter                  : Parameters)
    case GetAllEventsByGroup(parameter              : Parameters)
    case FetchAllCategory(parameter                 : Parameters)
    case AgendaListing(parameter                    : Parameters)
    case create_agenda(parameter                    : Parameters)
    case eventURL_availabilitychecking(parameter    : Parameters)
    case familyURL_availabilitychecking(parameter   : Parameters)
    
    case guest_count(parameter                      : Parameters)
    
    case GetAllFoldersGroup(parameter               : Parameters)
    case GetRequestContributorsList(parameter       : Parameters)
    
    case createFolder(parameter                     : Parameters)
    case GetAllFoldersEvents(parameter              : Parameters)
    case update_file_name(parameter                 : Parameters)
    
    case deleteFolder(parameter                     : Parameters)
    
    case addSignUps(parameter                       : Parameters)
    case FamilyLinedList(parameter                  : Parameters)
    
    case UpdateSignUps(parameter                    : Parameters)
    case deleteSignUps(parameter                    : Parameters)
    
    case item_contributor_list(parameter            : Parameters)
    case add_eventitems_contribution(parameter      : Parameters)
    case update_eventitems_contribution(parameter   : Parameters)
    
    case ActionUnLink(parameter                     : Parameters)
    
    case ListSignUps(parameter                      : Parameters)
    case BlockedUser(parameter                      : Parameters)
    case LeaveGroup(parameter                       : Parameters)
    case create_reminder(parameter                  : Parameters)
    case update_agenda(parameter                    : Parameters)
    case user_group_members(parameter               : Parameters)
    case getEventDetails(parameter                  : Parameters)
    case respondToRSVP(parameter                    : Parameters)
    case addEventContact(parameter                  : Parameters)
    case editEventContact(parameter                 : Parameters)
    case deleteEventContact(parameter               : Parameters)
    case event_deleteorcancel(parameter             : Parameters)
    
    case fetch_calender(parameter                   : Parameters)
    case group_events(parameter                     : Parameters)
    case event_send_mail(parameter                  : Parameters)
    case make_pic_cover(parameter                   : Parameters)
    case updateFolder(paraeter                      : Parameters)
    case delete_file(parameter                      : Parameters)
    case getGuestAttendingDetails(parameter         : Parameters)
    case viewContents(parameter                     : Parameters)
    case receive_feedback(parameter                 : Parameters)
    case getGuestAllInvitation(parameter            : Parameters)
    
    case getGuestNotAttendingDetails(parameter      : Parameters)
    case getMutualConnections(parameter             : Parameters)
    case getEventShareList(parameter                : Parameters)
    case getMutualConnectionsList(parameter         : Parameters)
    case getConnections(parameter                   : Parameters)
    case getUserMutualGroup(parameter               : Parameters)
    
    case viewedUsers(parameter                      : Parameters)
    case sharedUsers(parameter                      : Parameters)
    case sharedInGroups(parameter                   : Parameters)
    
    case post_by_id(parameter                       : Parameters)
    case post_by_user(parameter                     : Parameters)
    case deletePost(parameter                       : Parameters)
    case removePost(parameter                       : Parameters)
    case add_view_count(parameter                   : Parameters)
    case post_share(parameter                       : Parameters)
    case CreatePost(parameter                       : Parameters)
    case UpdatePost(parameter                       : Parameters)
    case mute_conversation(parameter                : Parameters)
    case getFamilyListForPost(parameter             : Parameters)
    case PostComment(parameter                      : Parameters)
    case postByFamily(parameter                     : Parameters)
    case postPublicFeed(parameter                   : Parameters)
    case approveRejectPost(parameter                : Parameters)
    case pendingApprovals(parameter                 : Parameters)
    case postAnnouncements(parameter                : Parameters)
    case announcementList(parameter                 : Parameters)
    case getPostByID(parameter                      : Parameters)
    case getCommonSharedUserList(parameter          : Parameters)
    case updateLastReadMessage(parameter            : Parameters)
    case getCommentsByPost(parameter                : Parameters)
    case postReadUnread(parameter                   : Parameters)
    case globalSearch(parameter                     : Parameters)
    case elasticSearchPosts(type: String, parameter : Parameters)
    case get_my_post_aggregate(parameter            : Parameters)
    
    case listUserFolders(parameter                  : Parameters)
    case pendingRequests(parameter                  : Parameters)
    case ReportSpam(parameter                       : Parameters)
    case activatePost(parameter                     : Parameters)
    case deactivatePost(prameter                    : Parameters)
    case deletePendingRequests(parameter            : Parameters)
    case deleteNotifications(parameter              : Parameters)
    case deleteSingleNotifications(parameter        : Parameters)
    case readAllNotifications(parameter              : Parameters)

    case update_reminder(parameter                  : Parameters)
    case delete_reminder(parameter                  : Parameters)
    case PhoneContactStatus(parameter               : Parameters)
    case mob_bulk(parameter                         : Parameters)
    case createNeeds(parameter                      : Parameters)
    case delete_comment(parameter                   : Parameters)
    case post_need_list(parameter                   : Parameters)
    case post_need_detail(parameter                 : Parameters)
    case need_delete(parameter                      : Parameters)
    
    case get_contribute_itemAmount_split(parameter  : Parameters)
    
    case item_delete(parameter                      : Parameters)
    case add_newItem(parameter                      : Parameters)
    case item_edit(parameter                        : Parameters)
    case contribution_create(parameter              : Parameters)
    case contribution_update(parameter              : Parameters)
    case update_request(parameter                   : Parameters)
    case get_announcement_banner_count(parameter    : Parameters)
    case addUpdate_Announcement_Seen(parameter      : Parameters)
    case copy_file_between_bucket(parameter         : Parameters)
    case topic_list(parameter                       : Parameters)
    case topic_create(parameter                     : Parameters)
    case commonTopicListByUsers(parameter           : Parameters)
    case add_users_to_topic(parameter               : Parameters)
    case remove_users_from_topic(parameter          : Parameters)
    case update_topic(parameter                     : Parameters)
    case topic_users_list(parameter                 : Parameters)
    case topic_comment(parameter                    : Parameters)
    case topic_detail(parameter                     : Parameters)
    case activateTopic(parameter                    : Parameters)
    case deactivateTopic(parameter                  : Parameters)
    case accept_user_topic(parameter                : Parameters)
    case reject_user_topic(parameter                : Parameters)
    case get_suggested_group_newUser(parameter      : Parameters)
    case joinFamilySuggested(parameter              : Parameters)
    case uploadFile(parameter                       : Parameters)
    
    //Membership
    case add_membership_lookup(parameter            : Parameters)
    case update_membership_lookup(parameter         : Parameters)
    case membership_dashboard(parameter             : Parameters)
    case list_membership_lookup(parameter           : Parameters)
    case user_membership_update(parameter           : Parameters)
    case membership_user_list(parameter             : Parameters)
    case get_membershiptype_ById(parameter          : Parameters)
    case Membership_reminder(parameter              : Parameters)
    case get_membership_lookup_periods(parameter    : Parameters)
    case getFamilies(parameter                      : Parameters)
    case groupMapUpdate(parameter                   : Parameters)
    
    //Thank You Posts
    case publish_post(parameter                      : Parameters)
    case get_post_default_image(parameter            : Parameters)
    case requestContributionlistByUser(parameter     : Parameters)
    case requestcontributionStatusUpdation(parameter : Parameters)
    
    // Stripe API Integration
    case stripe_oauth_link_generation(parameter     : Parameters)
    case getUserAdminGroups(parameter               : Parameters)
    case stripeGetaccountById(parameter             : Parameters)
    case stripeCreatePaymentIntent(parameter        : Parameters)
    
    // Get Banner
    case GetBanners(parameter                       : Parameters)
    
    
    case editProfile(parameter                      : Parameters)
    case paymentHistoryList(parameter               : Parameters)
    case paymentHistoryListInFamily(parameter       : Parameters)
    
    
    // Sticky Post
    case applyStickyPost(parameter                  : Parameters)
    case getStickyPostList(parameter                : Parameters)
    
    //Elastic Search elastic/getRecords
    case getElasticRecords(parameter                : Parameters)
    
    //Feed Messages in topic
    case get_user_commented_post(parameter          : Parameters)
    
    //Get all Keys
    case GetKeys(parameter                          : Parameters)
    
    // Open App Link
    case openAppGetParams(parameter                 : Parameters)
    
    // Get new Token
    case getAccessToken(parameter                   : Parameters)
    
    
}

extension FamilyheeyApi : TargetType {
    var elasticSearchBaseURl: String {
        
        switch NetworkManager.environment {
            
        case .production: return "https://vpc-familheey-dev-3e2zt46rpahzxreqeenbobb3zi.us-east-1.es.amazonaws.com"
        case .qa: return "https://vpc-familheey-dev-3e2zt46rpahzxreqeenbobb3zi.us-east-1.es.amazonaws.com"
        case .development: return "https://vpc-familheey-dev-3e2zt46rpahzxreqeenbobb3zi.us-east-1.es.amazonaws.com"
        case .staging: return "https://vpc-familheey-dev-3e2zt46rpahzxreqeenbobb3zi.us-east-1.es.amazonaws.com"
            
        case .conversation: return "https://vpc-familheey-dev-3e2zt46rpahzxreqeenbobb3zi.us-east-1.es.amazonaws.com"
        case .preProduction: return"https://vpc-familheey-dev-3e2zt46rpahzxreqeenbobb3zi.us-east-1.es.amazonaws.com"
        case .preProdConversation: return"https://vpc-familheey-dev-3e2zt46rpahzxreqeenbobb3zi.us-east-1.es.amazonaws.com"
        }
    }
    var commonURl: String {
        
        switch NetworkManager.environment {
            
        //        case .production: return "https://prod-commonbackend.familheey.com/api/v1/familheey/"
        case .production: return "https://commonbackend.familheey.com/api/v1/familheey/"
            
        case .preProduction: return "https://preprod-commonbackend.familheey.com/api/v1/familheey/"
            
        case .qa: return "https://5eeeea60b893.ngrok.io/api/v1/familheey/"
            
            
        case .development: return "https://dev-commonbackend.familheey.com/api/v1/familheey/"
            
        case .staging: return "https://staging-commonbackend.familheey.com/api/v1/familheey/"
            
        case .conversation: return "https://socket-4000.familheey.com/api/v1/familheey/"
            
        case .preProdConversation: return "https://preprod-socket-4000.familheey.com/api/v1/familheey/"
        }
    }
    
    var baseURL: URL {
        
        switch self {
        case .elasticSearchPosts:
            
            guard let url = URL(string: elasticSearchBaseURl) else { fatalError("elastic search URL could not be configured.")}
            return url
        default:
            guard let url = URL(string: commonURl) else { fatalError("commonURL could not be configured.")}
            return url
        }
    }
    
    var path: String {
        
        switch  self {
            
            
            case .familyJoinFromNoti:
            return "admin_request_action"
        case .get_notification_settings:
            return "get_notification_settings"
        case .addUserHistory:
            return "addUserHistory"
        case .registerToken:
            return "registerToken"
        case .onBoardCheck:
            return "onBoardCheck"
            
        case .updateFamilyWithHistory:
            return "update_family"
            
        case .createEvent:
            return "create_events"
        case .createEventUpdate:
            return "update_events"
        case .EventList:
            //return "get_events"
            return "explore_events"
        case .FetchAllCategory:
            return "fetch_category"
        case .AgendaListing:
            return "list_agenda"
            
        case .create_agenda:
            return "create_agenda"
            
        case .FamilyLinedList:
            return "list_linked_family"
        case .ActionUnLink:
            return "action_linked_family"
            
        case .InviteEvents:
            return "event_invitations"
            
        case .eventURL_availabilitychecking:
            return "check_link"
            
        case .familyURL_availabilitychecking:
            return "family_link_exist"
        case .BlockedUser:
            return "blocked_users"
            
        case .CreatedMeEvents:
            return "created_by_me"
        case .GetAllEventsByGroup:
            return "group_events"
        case .GetAllFoldersGroup:
            return "listGroupFolders"
        case .GetRequestContributorsList:
            return "needs/contribution_list"
            
        case .GetAllFoldersEvents:
            return "listEventFolders"
            
        case .LeaveGroup:
            return "leave_family"
            
        case .createFolder:
            return "createFolder"
            
        case .guest_count:
            return "get_guests_count"
            
        case .update_agenda:
            return "update_agenda"
        case .getEventDetails:
            return "get_event_byId"
            
        case .user_group_members:
            return "user_group_members"
            
        case .deleteFolder:
            return "removeFolder"
            
        case .respondToRSVP:
            return "respondToEvents"
            
        case .addEventContact:
            return "addEventContact"
        case .editEventContact:
            return "editEventContact"
        case .deleteEventContact:
            return "deleteEventContact"
        case .event_deleteorcancel:
            return "event_deleteorcancel"
            
        case .event_send_mail:
            return "event_shareor_invite"
            
        case .create_reminder:
            return "create_reminder"
        case .update_reminder:
            return "update_reminder"
            
        case .getGuestAttendingDetails:
            return "get_going_guest_details"
            
        case .getGuestNotAttendingDetails:
            return "getRSVP_guest_details"
        case .getGuestAllInvitation:
            return "event_invited_byUser"
            
        case .getMutualConnectionsList:
            return "get_family_mutual_list"
            
        case .getConnections:
            return "get_connections"
            
        case .addSignUps:
            return "add_event_signupitems"
        case .ListSignUps:
            return "get_eventitems"
            
            
        case .UpdateSignUps:
            return "update_event_signupitems"
            
        case .deleteSignUps:
            return "delete_event_signupitems"
            
        case .fetch_calender:
            return "fetch_calender"
        case .group_events:
            return "group_events"
            
        case .make_pic_cover:
            return "make_pic_cover"
        case .delete_file:
            return "delete_file"
        case .updateFolder:
            return "updateFolder"
            
        case .add_eventitems_contribution:
            return "add_eventitems_contribution"
            
        case .update_eventitems_contribution:
            return "update_eventitems_contribution"
        case .item_contributor_list:
            return "item_contributor_list"
            
        case .receive_feedback:
            return "receive_feedback"
            
        case .viewContents:
            return "viewContents"
            
        case .getMutualConnections:
            return "getMutualConnections"
        case .getEventShareList:
            return "event_share_list"
            
        case .getUserMutualGroup:
            return "user_mutual_group"
        case .update_file_name:
            return "update_file_name"
        case .globalSearch:
            return "globalsearch"
        case .elasticSearchPosts(let type, _):
            //            return "/\(type)/\(type)/\(id)"
            return "/\(type)/_search"
        case .listUserFolders:
            return "listUserFolders"
            
        case .viewedUsers:
            return "posts/list_post_views"
        case .sharedUsers:
            return "posts/getSharedUserListByUserid"
        case .sharedInGroups:
            return "posts/getSharedUserList"
            
        case .post_share:
            return "posts/post_share"
        case .deletePost:
            return "posts/deletePost"
        case .removePost:
            return "posts/remove_post"
        case .add_view_count:
            return "posts/add_view_count"
        case .post_by_user:
            return "posts/get_my_post"
        case .post_by_id:
            return "posts/getByID"
        case .CreatePost:
            return "posts/create"
        case .UpdatePost:
            return "posts/update_post"
        case .mute_conversation:
            return "posts/mute_conversation"
        case .getFamilyListForPost:
            return "posts/getFamilyListForPost"
        case .PostComment:
            return "posts/post_comment"
        case .postByFamily:
            return "posts/post_by_family"
        case .approveRejectPost:
            return "posts/approve_reject_post"
        case .pendingApprovals:
            return "posts/pending_approvals"
        case .announcementList:
            return "posts/announcement_list"
        case .getCommonSharedUserList:
            return "posts/getCommonSharedUserList"
        case .postPublicFeed:
            return "posts/public_feed"
        case .getPostByID:
            return "posts/getByID"
        case.updateLastReadMessage:
            return "posts/updateLastReadMessage"
        case .postAnnouncements:
            return "posts/public_feed"
        case .postReadUnread:
            return "posts/unread"
        case .getCommentsByPost:
            return "posts/getCommentsByPost"
        case .pendingRequests:
            return "pending_requests"
        case .ReportSpam:
            return "spam_report"
        case .activatePost:
            return "posts/activatePost"
        case .deactivatePost:
            return "posts/deactivatePost"
        case .get_my_post_aggregate:
            return "posts/get_my_post_aggregate"
        case .deletePendingRequests:
            return "delete_pending_requests"
            
        case .deleteNotifications:
            return "clearNotification"
            
        case .deleteSingleNotifications:
            return "DeleteOneNotification"
            
            case .readAllNotifications:
            return "readNotifications"
            
            
        case .delete_reminder:
            return "delete_reminder"
        case .PhoneContactStatus:
            return "PhoneContactStatus"
            
        case .createNeeds:
            return "needs/create"
            
        case .mob_bulk:
            return "mob_bulk"
        case .delete_comment:
            return "posts/delete_comment"
            
        case .post_need_list:
            return "needs/post_need_list"
            
        case .post_need_detail:
            return "needs/post_need_detail"
            
        case .need_delete:
            return "needs/delete"
            
        case .get_contribute_itemAmount_split:
            return "needs/get_contribute_itemAmount_split"
            
        case .item_delete:
            return "needs/delete_items"
        case .add_newItem:
            return "needs/create_items"
        case .item_edit:
            return "needs/update_items"
            
        case .contribution_create:
            return "needs/contribution_create"
            
        case .contribution_update:
            return "needs/contribution_update"
            
        case .update_request:
            return "needs/update"
            
        case .get_announcement_banner_count:
            return "posts/get_announcement_banner_count"
        case .addUpdate_Announcement_Seen:
            return "posts/addUpdate_Announcement_Seen"
        case .copy_file_between_bucket:
            return "copy_file_between_bucket"
            
        case .topic_list:
            return "topic_list"
            
        case .topic_create:
            return "topic_create"
        case .commonTopicListByUsers:
            return "commonTopicListByUsers"
            
        case .add_users_to_topic:
            return "add_users_to_topic"
            
        case .remove_users_from_topic:
            return "remove_users_from_topic"
        case .update_topic:
            return "update_topic"
        case .topic_users_list:
            return "topic_users_list"
        case .topic_comment:
            return "topic_comment"
        case .topic_detail:
            return "topic_detail"
            
        case .activateTopic:
            return "activateTopic"
        case .deactivateTopic:
            return "deactivateTopic"
        case .accept_user_topic:
            return "accept_user_topic"
        case .reject_user_topic:
            return "reject_user_topic"
        case .get_suggested_group_newUser:
            return "get_suggested_group_newUser"
        case .joinFamilySuggested:
            return "joinFamily"
        case .uploadFile:
            return "uploadFile"
            
        //Membership
        case .add_membership_lookup:
            return "add_membership_lookup"
        case .update_membership_lookup:
            return "update_membership_lookup"
        case .membership_dashboard:
            return "membership_dashboard"
        case .list_membership_lookup:
            return "list_membership_lookup"
        case .user_membership_update:
            return "user_membership_update"
        case  .membership_user_list:
            return "viewMembers"
        case .get_membershiptype_ById:
            return "getMembershiptypeById"
        case .Membership_reminder:
            return "Membership_reminder"
        case .get_membership_lookup_periods:
            return "get_membership_lookup_periods"
        case .getFamilies:
            return "viewFamily"
        case .groupMapUpdate:
            return "groupMapUpdate"
            
        //Thank You Posts
        case .publish_post:
            return "publish_post"
        case .get_post_default_image:
            return "get_post_default_image"
            
        case .requestContributionlistByUser:
            return "contributionlistByUser"
        case .requestcontributionStatusUpdation:
            return "contributionStatusUpdation"
            
        //Stripe API payment/stripeGetaccountById
        case .stripe_oauth_link_generation:
            return "payment/stripe_oauth_link_generation"
        case .getUserAdminGroups:
            return "getUserAdminGroups"
        case .stripeGetaccountById:
            return "payment/stripeGetaccountById"
        case .stripeCreatePaymentIntent:
            return "payment/stripeCreatePaymentIntent"
            
        //Get Banner
        case .GetBanners:
            return "GetBanners"
            
        case .editProfile:
            return "edit_profile"
            
        case .paymentHistoryList:
            return "paymentHistoryByUserid"
        case .paymentHistoryListInFamily:
            return "paymentHistoryByGroupid"
            
        case .applyStickyPost:
            return "post/stickyPost"
        case .getStickyPostList:
            return "posts/post_sticky_by_family"
            
        // Elastic Search
        case .getElasticRecords:
            return "elastic/getRecords"
            
        // Feed messages in topic
        case .get_user_commented_post:
            return "get_user_commented_post"
            
        //Get all keys
        case .GetKeys:
            return "GetKeys"
            
        // Open App Link
        case .openAppGetParams:
            return "openAppGetParams"
            
        //Get new token
        case .getAccessToken:
            return "getAccessToken"
            
        }
        
    }
    
    var method: Moya.Method {
        
        switch self {
            
        case .familyJoinFromNoti,.get_notification_settings,.addUserHistory,.registerToken,.onBoardCheck,.updateFamilyWithHistory,.createEvent,.createEventUpdate,.EventList,.FetchAllCategory, .create_agenda, .InviteEvents, .CreatedMeEvents, .AgendaListing, .eventURL_availabilitychecking,.familyURL_availabilitychecking,.guest_count, .update_agenda, .getEventDetails, .user_group_members, .respondToRSVP, .create_reminder, .event_send_mail,.getGuestAttendingDetails,.getGuestNotAttendingDetails,.getGuestAllInvitation,.addSignUps,.UpdateSignUps,.add_eventitems_contribution,.update_eventitems_contribution,.item_contributor_list, .GetAllEventsByGroup, .GetAllFoldersGroup,.GetRequestContributorsList, .viewContents, .deleteSignUps, .ListSignUps, .createFolder, .make_pic_cover,.fetch_calender , .FamilyLinedList, .GetAllFoldersEvents, .ActionUnLink, .receive_feedback, .LeaveGroup, .BlockedUser, .delete_file, .group_events, .updateFolder,.getMutualConnections,.getEventShareList, .deleteFolder, .getMutualConnectionsList,.CreatePost,.UpdatePost, .mute_conversation, .getFamilyListForPost, .addEventContact, .editEventContact, .deleteEventContact,.post_by_user,.post_by_id,.event_deleteorcancel,.deletePost,.removePost, .viewedUsers,.approveRejectPost,.pendingApprovals,.getCommonSharedUserList,.getUserMutualGroup, .sharedUsers,.announcementList,.getPostByID,.sharedInGroups, .post_share, .add_view_count, .PostComment,.postByFamily,.getConnections,.updateLastReadMessage,.postPublicFeed,.getCommentsByPost,.update_file_name,.postReadUnread,.globalSearch,.elasticSearchPosts,.listUserFolders,.pendingRequests,.ReportSpam,.activatePost,.deactivatePost,.deletePendingRequests,.deleteNotifications,.deleteSingleNotifications,.readAllNotifications,.update_reminder,.delete_reminder,.PhoneContactStatus,.mob_bulk,.delete_comment,.post_need_list,.post_need_detail,.createNeeds,.need_delete,.item_delete,.add_newItem,.contribution_create,.item_edit,.contribution_update,.update_request,.get_announcement_banner_count,.addUpdate_Announcement_Seen,.copy_file_between_bucket,.topic_list,.topic_create,.add_users_to_topic,.remove_users_from_topic,.update_topic,.topic_users_list,.commonTopicListByUsers,.topic_comment,.topic_detail,.activateTopic,.deactivateTopic,.accept_user_topic,.reject_user_topic,.get_suggested_group_newUser,.joinFamilySuggested,.uploadFile,.add_membership_lookup,.update_membership_lookup,.membership_dashboard,.list_membership_lookup,.user_membership_update,.membership_user_list,.get_membershiptype_ById,.Membership_reminder,.get_membership_lookup_periods,.get_post_default_image,.publish_post,.requestContributionlistByUser,.requestcontributionStatusUpdation,.getFamilies,.stripe_oauth_link_generation,.getUserAdminGroups,.stripeGetaccountById,.stripeCreatePaymentIntent,.get_contribute_itemAmount_split,.groupMapUpdate,.GetBanners,.editProfile,.paymentHistoryList,.paymentHistoryListInFamily,.applyStickyPost,.getStickyPostList,.getElasticRecords,.get_user_commented_post,.GetKeys,.openAppGetParams,.getAccessToken,.get_my_post_aggregate:
            
            
            return .post
        default:
            return .get
        }
    }
    
    var sampleData: Data {
        
        return Data()
    }
    
    var task: Task {
        
        switch self {
            
            
            
            case.familyJoinFromNoti(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case.get_notification_settings(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case.addUserHistory(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case.registerToken(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .onBoardCheck(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case.updateFamilyWithHistory(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case.createEvent(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case.createEventUpdate(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case.EventList(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case.FetchAllCategory(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case.AgendaListing(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .GetAllFoldersGroup(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .GetRequestContributorsList(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
            
        case .create_agenda(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .InviteEvents(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .CreatedMeEvents(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .GetAllEventsByGroup(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .FamilyLinedList(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .ActionUnLink(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .createFolder(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .GetAllFoldersEvents(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .BlockedUser(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .LeaveGroup(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .eventURL_availabilitychecking(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .familyURL_availabilitychecking(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .guest_count(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .update_agenda(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .user_group_members(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .getEventDetails(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .respondToRSVP(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .addEventContact(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .editEventContact(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .deleteEventContact(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .event_deleteorcancel(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .event_send_mail(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .getGuestAttendingDetails(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .create_reminder(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .update_reminder(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .getGuestNotAttendingDetails(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .getGuestAllInvitation(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
            
        case .deleteFolder(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .getMutualConnections(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .addSignUps(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .ListSignUps(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .deleteSignUps(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .make_pic_cover(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .delete_file(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .updateFolder(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .UpdateSignUps(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .add_eventitems_contribution(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .fetch_calender(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case  .group_events(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .getMutualConnectionsList(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .update_eventitems_contribution(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .item_contributor_list(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .receive_feedback(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .viewContents(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .getConnections(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .getEventShareList(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .update_file_name(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .post_share(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .deletePost(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .post_by_user(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .post_by_id(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .removePost(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .getUserMutualGroup(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .add_view_count(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .viewedUsers(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .sharedUsers(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .sharedInGroups(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .CreatePost(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .UpdatePost(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .mute_conversation(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .getFamilyListForPost(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .approveRejectPost(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .pendingApprovals(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .PostComment(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .postByFamily(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .postPublicFeed(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .announcementList(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .getCommonSharedUserList(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .updateLastReadMessage(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .getPostByID(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .getCommentsByPost(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .postReadUnread(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .globalSearch(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .elasticSearchPosts(_,let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .listUserFolders(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .pendingRequests(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .ReportSpam(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .get_my_post_aggregate(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .activatePost(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .deactivatePost(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .deletePendingRequests(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .deleteNotifications(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .deleteSingleNotifications(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            case .readAllNotifications(let parameter):
                       return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
            
        case .delete_reminder(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .PhoneContactStatus(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .mob_bulk(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .createNeeds(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .delete_comment(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .post_need_list(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .post_need_detail(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .need_delete(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .item_delete(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .get_contribute_itemAmount_split(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .add_newItem(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .item_edit(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .contribution_update(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .contribution_create(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .update_request(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .get_announcement_banner_count(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .addUpdate_Announcement_Seen(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .copy_file_between_bucket(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .update_topic(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .topic_list(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .topic_create(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .add_users_to_topic(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .remove_users_from_topic(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .topic_users_list(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .topic_comment(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .topic_detail(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .activateTopic(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .deactivateTopic(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .accept_user_topic(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .reject_user_topic(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .commonTopicListByUsers(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .get_suggested_group_newUser(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .joinFamilySuggested(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .uploadFile(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        //Membership
        case .add_membership_lookup(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .update_membership_lookup(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .membership_dashboard(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .list_membership_lookup(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .user_membership_update(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .membership_user_list(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .get_membershiptype_ById(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .Membership_reminder(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .get_membership_lookup_periods(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .getFamilies(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .groupMapUpdate(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        //Thank You Posts
        case .get_post_default_image(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .publish_post(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case.requestContributionlistByUser(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case.requestcontributionStatusUpdation(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        //Stripe API Integration
        case .stripe_oauth_link_generation(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .getUserAdminGroups(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .stripeGetaccountById(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .stripeCreatePaymentIntent(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        // Banner
        case .GetBanners(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .editProfile(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .paymentHistoryList(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .paymentHistoryListInFamily(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        case .applyStickyPost(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        case .getStickyPostList(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
            //Elastic Search
            
        case .getElasticRecords(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        //Feed Messages in topic
        case .get_user_commented_post(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        //Get all Keys
        case .GetKeys(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
            
        //Open App Link
        case .openAppGetParams(let parameter):
            return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        
        //Get new token
        case .getAccessToken(let parameter):
                return .requestParameters(parameters: parameter, encoding: JSONEncoding.default)
        
            
            
        default:
            return .requestPlain
            
        }
    }
    
    var headers: [String : String]? {
        let token = UserDefaults.standard.string(forKey: "app_token") ?? ""
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
        let tempDic = ["app_name":"familheey","app_version":version,"os":"iOS"]
        let tempJSON = JSON(arrayLiteral: tempDic)
        print(tempJSON.stringValue)
        switch self {
            
        case .elasticSearchPosts :
            let httpheader = [
                "Accept": "application/json, text/plain, */*",
                "Content-Type" :"application/json ; charset=utf-8"
            ]
            return httpheader
        default :
            if( token != ""){
                var httpheader = [String:String]()
                if let id =  UserDefaults.standard.value(forKey: "userId") as? String{
                    httpheader = [
                        "Accept": "application/json, text/plain, */*",
                        "Content-Type" :"application/json ; charset=utf-8",
                        "Authorization" : "Bearer " + token,// customise Parameter and Value
                        "logined_user_id":id,
                        "app_info":tempJSON.stringValue
                    ]
                }
                else{
                    httpheader = [
                        "Accept": "application/json, text/plain, */*",
                        "Content-Type" :"application/json ; charset=utf-8",
                        "Authorization" : "Bearer " + token,  // customise Parameter and Value
                        "app_info":tempJSON.stringValue
                    ]
                }
                
                print("heaaader  \(httpheader)")
                return httpheader
            }
            else{
                let httpheader = [
                    "Accept": "application/json, text/plain, */*",
                    "Content-Type" :"application/json ; charset=utf-8",
                    "app_info":tempJSON.stringValue
                ]
                print("heaaader  \(httpheader)")

                return httpheader
            }
        }
    }
}


 /* extension FamilyheeyApi{
    init(handleRefreshToken: Bool) {
        if handleRefreshToken {
//            self.init(handleRefreshToken: FamilyheeyApi.endpointResolver())
            self.init(handleRefreshToken: MoyaProvider.endpoint())
        } else {
            self.init(handleRefreshToken: false)
        }
    }

    
  static func endpointResolver() -> MoyaProvider<FamilyheeyApi>.RequestClosure {
        return { (endpoint, closure) in
            //Getting the original request
            let request = try! endpoint.urlRequest()

            //assume you have saved the existing token somewhere
            let token = UserDefaults.standard.value(forKey: "app_token")
            if ((token) != nil) {
                // Token is valid, so just resume the original request
                closure(.success(request))
                return
            }

            //Do a request to refresh the authtoken based on refreshToken
            authenticationProvider.request(.refreshToken(params)) { result in
                switch result {
                case .success(let response):
                    let token = response.mapJSON()["token"]
                    let newRefreshToken = response.mapJSON()["refreshToken"]
                    //overwrite your old token with the new token
                    //overwrite your old refreshToken with the new refresh token

                    closure(.success(request)) // This line will "resume" the actual request, and then you can use AccessTokenPlugin to set the Authentication header
                case .failure(let error):
                    closure(.failure(error)) //something went terrible wrong! Request will not be performed
                }
            }
    }
    }
}*/
