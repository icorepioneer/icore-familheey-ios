//
//  ApiManager.swift
//  GokulMoyaSample
//
//  Created by Innovation Incubator on 26/03/19.
//  Copyright © 2019 Innovation Incubator. All rights reserved.
//

import Foundation
import Moya

enum APIEnvironment {
    case qa
    case production
    case development
    case staging
    case conversation
    case preProduction
    case preProdConversation
}


struct NetworkManager {
    static var environment: APIEnvironment = .production

}
