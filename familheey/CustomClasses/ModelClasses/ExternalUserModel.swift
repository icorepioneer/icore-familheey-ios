//
//  ExternalUserModel.swift
//  familheey
//
//  Created by Giri on 24/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import Foundation
import Tailor

struct ExternalUserModel:SafeMappable {

    var User : UserDetails?
    var count : countModel?
    var group : groupModel?
    var status_code:Int = 000
    
    init(_ map: [String : Any]) throws {
        User = map.relation("profile")
        count <- map.relation("count")
        group <- map.relation("groups")
        status_code <- map.property("status_code")
    }
}

    struct UserDetails : SafeMappable {
        
        var id:Int = 000
        var fullname:String = ""
        var email:String = ""
        var phone:String = ""
        var propic:String = ""
        var coverPic:String = ""
        var location:String = ""
        var origin:String = ""
        var dob:String = ""
        var gender:String = ""
        var type:String = ""
        var about:String = ""
        var createdAt:String = ""
        var updatedAt:String = ""
        var isActive:String = ""
        var isVerified:String = ""
        var originalPic:String = ""
        var isNotification:Bool = true
        var isSearchable:Bool = true
        var work:String = ""

        
        init(_ map: [String : Any]) throws {
            
            id <- map.property("id")
            fullname <- map.property("full_name")
            email <- map.property("email")
            phone <- map.property("phone")
            propic <- map.property("propic")
            location <- map.property("location")
            origin <- map.property("origin")
            dob <- map.property("dob")
            gender <- map.property("gender")
            type <- map.property("type")
            about <- map.property("about")
            createdAt <- map.property("createdAt")
            updatedAt <- map.property("updatedAt")
            isActive <- map.property("is_active")
            isVerified <- map.property("is_verified")
            coverPic <- map.property("cover_pic")
            originalPic <- map.property("user_original_image")
            isNotification <- map.property("notification")
            isSearchable <- map.property("searchable")
            work <- map.property("work")

            
            
        }
    }

struct countModel: SafeMappable{
    var connections:Int = 000
    var familyCount:Int = 000
    var mutualConnections: Int? = 000
    var mutalFamilies: Int? = 000
    
    init(_ map: [String : Any]) throws {
        connections <- map.property("connections")
        familyCount <- map.property("familyCount")
        mutualConnections <- map.property("mutual_connections")
        mutalFamilies <- map.property("mutual_families")
    }
}

struct groupModel : SafeMappable{
    var groupName:String = ""
    
    init(_ map: [String : Any]) throws {
        groupName <- map.property("group_name")
    }
}
