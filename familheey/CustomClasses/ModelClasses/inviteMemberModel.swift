//
//  inviteMemberModel.swift
//  familheey
//
//  Created by familheey on 25/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import Foundation
import Tailor

struct inviteMemberResult:SafeMappable {
    var fromId:Int = 000
    var groupId:Int = 000
    var statuscode:Int = 000
    var type:String = ""
    var status:String = ""
    var id:Int = 000
    var user_id:Int = 000
    var statusStr:String = ""
    
    init(_ map: [String : Any]) throws {
        fromId <- map.property("from_id")
        groupId <- map.property("group_id")
        statuscode <- map.property("status_code")
        type <- map.property("type")
        status <- map.property("status")
        id <- map.property("id")
        user_id <- map.property("user_id")
        statusStr <- map.property("status")
    }
    
    
}
