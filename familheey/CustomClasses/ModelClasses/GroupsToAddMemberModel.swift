//
//  GroupsToAddMemberModel.swift
//  familheey
//
//  Created by Giri on 25/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import Foundation
import Tailor

struct GroupsToAddMembersList:SafeMappable {
    var result:[GroupDetail]?
    
    init(_ map: [String : Any]) throws {
        result <- map.relations("data")
    }
}

struct GroupDetail:SafeMappable{
    
    var id:Int = 000
    var user_id:Int = 000
    var group_id:Int = 000
    var following: String = ""
    var is_blocked:String = ""
    var type:String = ""
    var created_at:String = ""
    var updated_at:String = ""
    var group_name:String = ""
    var group_type:String = ""
    var logo:String = ""
    var cover_pic:String = ""
    var base_region:String = ""
    var created_by:String? = ""
    var group_level:String = ""
    var intro:String = ""
    var is_active:String = ""
    var searchable:String = ""
    var visibility:String = ""
    var member_joining:Int = 000
    var invitation_status:Int = 000
    var member_approval:Int = 000
    var post_create:Int = 000
    var post_approval:Int = 000
    var post_visibilty:Int = 000
    var link_approval:Int = 000
    var group_category:String = ""
    var link_family:Int = 000
    var is_linkable:String = ""
    var JOINED:Bool = false
    var request_status:String = ""
    
    var member_count:String = ""
    var knowncount:String = ""

    

    init(_ map: [String : Any]) throws {
        id <- map.property("id")
        user_id <- map.property("user_id")
        group_id <- map.property("group_id")
        following <- map.property("following")
        is_blocked <- map.property("is_blocked")
        type <- map.property("type")
        created_at <- map.property("created_at")
        updated_at <- map.property("updated_at")
        group_name <- map.property("group_name")
        group_type <- map.property("group_type")
        logo <- map.property("logo")
        cover_pic <- map.property("cover_pic")
        base_region <- map.property("base_region")
        created_by <- map.property("created_by")
        group_level <- map.property("group_level")
        intro <- map.property("intro")
        is_active <- map.property("is_active")
        searchable <- map.property("searchable")
        visibility <- map.property("visibility")
        member_joining <- map.property("member_joining")
        invitation_status <- map.property("invitation_status")
        member_approval <- map.property("member_approval")
        post_create <- map.property("post_create")
        post_approval <- map.property("post_approval")
        post_visibilty <- map.property("post_visibilty")
        link_approval <- map.property("link_approval")
        group_category <- map.property("group_category")
        link_family <- map.property("link_family")
        is_linkable <- map.property("is_linkable")
        JOINED <- map.property("JOINED")
        request_status <- map.property("status")
        
        member_count <- map.property("member_count")
        knowncount <- map.property("new_events_count")

    }
}
