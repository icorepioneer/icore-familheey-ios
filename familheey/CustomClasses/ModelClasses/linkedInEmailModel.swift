//
//  linkedInEmailModel.swift
//  familheey
//
//  Created by familheey on 13/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import Foundation

struct linkedinEmail : Codable {
    let handle : String?
    let handle1 : Handle?
    
    enum CodingKeys: String, CodingKey {
        
        case handle = "handle"
        case handle1 = "handle~"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        handle = try values.decodeIfPresent(String.self, forKey: .handle)
        handle1 = try values.decodeIfPresent(Handle.self, forKey: .handle1)
    }
    
}

struct Handle : Codable {
    let emailAddress : String?
    
    enum CodingKeys: String, CodingKey {
        
        case emailAddress = "emailAddress"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        emailAddress = try values.decodeIfPresent(String.self, forKey: .emailAddress)
    }
    
}

