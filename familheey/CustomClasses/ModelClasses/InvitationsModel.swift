//
//  Invitations.swift
//  familheey
//
//  Created by Giri on 26/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import Foundation

import Tailor

struct InvitationsModel:SafeMappable {
    var result:[invitations]?
    
    init(_ map: [String : Any]) throws {
        result <- map.relations("data")
    }
}

struct invitations:SafeMappable{
    
    var req_id:Int = 000
    var created_at: String = ""
    var group_name:String = ""
    var full_name:String = ""
    var location:String = ""
    var group_id:Int = 000
    var logo:String = ""
    var from_id:Int = 000
    
    init(_ map: [String : Any]) throws {
        req_id <- map.property("req_id")
        created_at <- map.property("created_at")
        group_name <- map.property("group_name")
        full_name <- map.property("full_name")
        location <- map.property("base_region")
        group_id <- map.property("group_id")
        logo <- map.property("propic")
        from_id <- map.property("from_id")
    }
    
}
