//
//  joinFamilyResponseModel.swift
//  familheey
//
//  Created by familheey on 24/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import Foundation
import Tailor

struct JoinResult: SafeMappable {
    
    var joinData : JoinResponse?
    var status_code:Int = 000
    var error:Int = 000
    var message:String = ""

    init(_ map: [String : Any]) throws {
        joinData <- map.relation("data")
        status_code <- map.property("status_code")
        error <- map.property("error")
        message <- map.property("message")

    }
}

struct JoinResponse: SafeMappable {
    var status:String = ""
    var userId:String = ""
    var groupId:String = ""
    var statusMsg:String = ""
    var type:String = ""
    
    init(_ map: [String : Any]) throws {
        status <- map.property("status")
        userId <- map.property("user_id")
        groupId <- map.property("group_id")
        statusMsg <- map.property("status")
        type <- map.property("type")
    }
    
    
}
