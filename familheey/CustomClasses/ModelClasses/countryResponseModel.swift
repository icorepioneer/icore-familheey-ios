//
//  countryResponseModel.swift
//  familheey
//
//  Created by familheey on 16/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import Foundation
import Tailor

struct countryResponseModel : SafeMappable{
    
    var countryList:[countryDataModel]?
    
    init(_ map: [String : Any]) throws {
        countryList = map.relations("countries")
    }
    
}

struct countryDataModel : SafeMappable  {
    
    var code:String = ""
    var name:String = ""
    
    init(_ map: [String : Any]) throws {
        code<-map.property("code")
        name<-map.property("name")
    }
}

