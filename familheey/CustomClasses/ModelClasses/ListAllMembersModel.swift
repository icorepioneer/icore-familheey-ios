//
//  ListAllMembersModel.swift
//  familheey
//
//  Created by familheey on 25/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import Foundation
import Tailor

struct listAllResult:SafeMappable {
    var page:Int = 000
    var pagesize:Int = 000
    var statuscode:Int = 000
    var result:[listMemberResult]?
    
    init(_ map: [String : Any]) throws {
        page <- map.property("page")
        pagesize <- map.property("pagezise")
        statuscode <- map.property("status_code")
        
        result <- map.relations("rows")
    }
    
    
}

struct listMemberResult:SafeMappable {
    var about:String = ""
    var dob:String = ""
    var email:String = ""
    var gender:String = ""
    var fullname:String = ""
    var location:String = ""
    var origin:String = ""
    var phone:String = ""
    var photo:String = ""
    var type:String = ""
    var id:Int = 000
    var exist:Int = 000
    var flag:Int = 000
    var isActive:Int = 000
    var isVerified:Int = 000
    var status:Int = 000
    
    
    init(_ map: [String : Any]) throws {
        about <- map.property("about")
        dob <- map.property("dob")
        email <- map.property("email")
        gender <- map.property("gender")
        fullname <- map.property("full_name")
        location <- map.property("location")
        origin <- map.property("origin")
        phone <- map.property("phone")
        photo <- map.property("propic")
        type <- map.property("type")
        id <- map.property("id")
        exist <- map.property("exists")
        flag <- map.property("FLAG")
        isActive <- map.property("is_active")
        isVerified <- map.property("is_verified")
        status <- map.property("invitation_status")
    }
    
    
}
