//
//  RelationsModel.swift
//  familheey
//
//  Created by Giri on 26/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import Foundation
import Tailor

struct RelationsModel:SafeMappable {
    var result:[Relations]?
    
    init(_ map: [String : Any]) throws {
        result <- map.relations("data")
    }
}

struct Relations:SafeMappable{
    
    var id:Int = 000
    var relationship: String = ""
    var opposite_relation_male:String = ""
    var opposite_relation_female:String = ""
    var createdAt:String = ""
    var updatedAt:String = ""

    init(_ map: [String : Any]) throws {
        id <- map.property("id")
        relationship <- map.property("relationship")
        opposite_relation_male <- map.property("opposite_relation_male")
        opposite_relation_female <- map.property("opposite_relation_female")
        createdAt <- map.property("createdAt")
        updatedAt <- map.property("updatedAt")
    }
}
