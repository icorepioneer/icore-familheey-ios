//
//  userModel.swift
//  familheey
//
//  Created by familheey on 12/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import Foundation
import Tailor

struct userModel:SafeMappable {
    var signupMsg:String = ""
    var status:Int = 000
    var about:String = ""
    //var createdAt:String = ""
    var dob:String = ""
    var email:String = ""
    var gender:String = ""
    var fullname:String = ""
    var location:String = ""
    var origin:String = ""
    var otp:String = ""
    var otpTime:String = ""
    var phone:String = ""
    var photo:String = ""
     var coverPic:String = ""
    var type:String = ""
    //var updatedAt:String = ""
    var id:Int = 000
    var isActive:String = ""
    var isVerified:String = ""
    var reLogin:Int = 000
    var accessToken:String = ""
    var refreshToken:String = ""
    
    init(_ map: [String : Any]) throws {
        signupMsg <- map.property("message")
        about <- map.property("about")
       // createdAt <- map.property("createdAt")
        dob <- map.property("dob")
        email <- map.property("email")
        gender <- map.property("gender")
        fullname <- map.property("full_name")
        location <- map.property("location")
        origin <- map.property("origin")
        otp <- map.property("otp")
        otpTime <- map.property("otpTime")
        phone <- map.property("phone")
        photo <- map.property("propic")
        type <- map.property("type")
        //updatedAt <- map.property("updatedAt")
        id <- map.property("id")
        isActive <- map.property("is_active")
        isVerified <- map.property("is_verified")
        status <- map.property("status_code")
        reLogin <- map.property("reLogin")
        coverPic <- map.property("cover_pic")
        accessToken <- map.property("accessToken")
        refreshToken <- map.property("refreshToken")
    }
}




