//
//  SuccessResponseModel.swift
//  familheey
//
//  Created by Giri on 26/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import Foundation
import Tailor

struct RelationUpdateResponseModel:SafeMappable {
    
    var result:Success?
    var statusCode:Int = 000

    init(_ map: [String : Any]) throws {
        result <- map.relation("data")
        statusCode <- map.property("status_code")
    }
}

struct Success:SafeMappable{
    var message: String = ""
    var tableId : Int = 000
    
    init(_ map: [String : Any]) throws {
        message <- map.property("message")
        tableId <- map.property("id")
    }
}
