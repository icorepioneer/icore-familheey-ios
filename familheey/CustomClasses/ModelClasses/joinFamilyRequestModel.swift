//
//  joinFamilyRequestModel.swift
//  familheey
//
//  Created by familheey on 24/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import Foundation
import Tailor

struct groupReqestResult:SafeMappable {
    var result:[groupRequest]?
    var status_code:Int = 000
    var resultLink:[groupRequestLinked]?
    
    init(_ map: [String : Any]) throws {
        result <- map.relations("request")
        status_code <- map.property("status_code")
        resultLink <- map.relations("linkRequest")
    }
}

struct groupRequest:SafeMappable{
    var fromId:Int = 000
    var groupId:Int = 000
    var type:String = ""
    var status:String = ""
    var id:Int = 000
    var user_id:Int = 000
    var responded_by: String = ""
    var fullname : String = ""
    var logo :String = ""
    var groupName :String = ""
    var propic :String = ""

    
    init(_ map: [String : Any]) throws {
        fromId <- map.property("from_id")
        groupId <- map.property("group_id")
        type <- map.property("type")
        status <- map.property("status")
        id <- map.property("id")
        user_id <- map.property("user_id")
        responded_by <- map.property("responded_by")
        fullname <- map.property("full_name")
        logo <- map.property("logo")
        groupName <- map.property("group_name")
        propic <- map.property("propic")

    }
    
}

struct groupRequestLinked:SafeMappable{
    var fromId:Int = 000
    var groupId:Int = 000
    var type:String = ""
    var status:String = ""
    var id:Int = 000
    var user_id:Int = 000
    var responded_by: String = ""
    var fullname : String = ""
    var logo :String = ""
    var groupName :String = ""
    
    init(_ map: [String : Any]) throws {
        fromId <- map.property("from_id")
        groupId <- map.property("group_id")
        type <- map.property("type")
        status <- map.property("status")
        id <- map.property("id")
        user_id <- map.property("user_id")
        responded_by <- map.property("responded_by")
        fullname <- map.property("full_name")
        logo <- map.property("logo")
        groupName <- map.property("group_name")
    }
    
}
