//
//  SearchResponseModel.swift
//  familheey
//
//  Created by Giri on 24/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import Foundation
import Tailor

struct SearchList: SafeMappable {
    let searchResult : [SearchResultUser]?
    let searchResult2 : [SearchResultGroup]?
    let searchResult3 : [SearchResultsEvents]?
    var status_code:Int = 000
    
    init(_ map: [String : Any]) throws {
        searchResult = map.relations("users")
        searchResult2 = map.relations("groups")
        searchResult3 = map.relations("events")
        status_code <- map.property("status_code")
    }
    
}

struct SearchResultUser : SafeMappable {
    var userid : Int? = 000
    var full_name : String? = ""
    var origin : String? = ""
    var gender : String? = ""
    var propic : String? = ""
    var familycount : String? = ""
    var mutualfamilycount : String? = ""
    var faCount:Int = 000
    var type:String = ""
    var exist:Int = 000
    var status:Int = 000
    
    init(_ map: [String : Any]) throws {
        userid<-map.property("id")
        full_name<-map.property("full_name")
        origin<-map.property("location")
        gender<-map.property("gender")
        propic<-map.property("propic")
        familycount<-map.property("familycount")
        mutualfamilycount<-map.property("mutualfamilycount")
        faCount <- map.property("familycount")
        type <- map.property("type")
        exist <- map.property("exists")
        status <- map.property("invitation_status")
    }
    
    
}
struct SearchResultGroup : SafeMappable {
    var group_name : String? = ""
    var group_id : Int? = 000
    var group_type : String? = ""
    var group_category : String? = ""
    var visibility : String? = ""
    var member_approval : Int = 000
    var member_joining : Int = 000
    var is_linkable : Int = 000
    var base_region : String? = ""
    var type : String? = ""
    var status : String? = ""
    var membercount : String? = ""
    var knowncount : String? = ""
//    var is_joined : Int = 000
    var is_joined : String = ""
    var logo :String = ""
    var isRemoved : Int = 000
    var createdBy :String = ""
    var reqType :String = ""
    var reqId :Int = 000
    var reqFromId :Int = 000
    var post_count: String? = ""
    
    init(_ map: [String : Any]) throws {
        group_name<-map.property("group_name")
        group_id<-map.property("group_id")
        group_type<-map.property("group_type")
        group_category<-map.property("group_category")
        visibility<-map.property("visibility")
        member_approval<-map.property("member_approval")
        member_joining<-map.property("member_joining")
        is_linkable<-map.property("is_linkable")
        base_region<-map.property("base_region")
        type<-map.property("type")
        status<-map.property("status")
        membercount<-map.property("membercount")
        knowncount<-map.property("knowncount")
        is_joined<-map.property("is_joined")
        logo <- map.property("logo")
        isRemoved <- map.property("is_removed")
        createdBy <- map.property("created_by_name")
        reqType <- map.property("type")
        reqId <- map.property("req_id")
        reqFromId <- map.property("from_id")
        post_count <- map.property("post_count")
    }
}

struct SearchResultsEvents : SafeMappable{
    var created:String = ""
    var eventName:String = ""
    var location:String = ""
    var fromDate:Int = 000
    var eventType:String = ""
    var toDate:Int = 000
    var hostName:String = ""
    var id:Int = 000
    var isShare:String = ""
    var propic:String = ""
    var createdAt:Int = 000
    var event_imag:String = ""
    var event_host:String = ""
    var fullName:String = ""
    var firstPerson:String = ""
    var interstedCount:String = "0"
    var goingCount:String = "0"
    var meeting_link:String = ""
    var meeting_logo:String = ""
    var meeting_dial:String = ""
    var meeting_pin:String = ""
    
    init(_ map: [String : Any]) throws {
        created <- map.property("created_user")
        eventName <- map.property("event_name")
        location <- map.property("location")
        fromDate <- map.property("from_date")
        toDate <- map.property("to_date")
        eventType <- map.property("event_type")
        hostName <- map.property("event_host")
        isShare <- map.property("is_sharable")
        id <- map.property("id")
        propic <- map.property("propic")
        createdAt <- map.property("created_at_")
        event_imag <- map.property("event_image")
        fullName <- map.property("full_name")
        firstPerson <- map.property("first_person_going")
        interstedCount <- map.property("interested_count")
        goingCount <- map.property("going_count")
        meeting_link <- map.property("meeting_link")
        meeting_logo <- map.property("meeting_logo")
        meeting_dial <- map.property("meeting_dial_number")
        meeting_pin <- map.property("meeting_pin")
    }
}

/*struct SearchResultsPosts : SafeMappable{
    var categoryId:Int = 000
    var commonShareCount:Int = 000
    var conversationCount:Int = 000
    var newConversationCount:Int = 000
    var isEnableConversation:Int = 000
    var cratedAt:String = ""
    var createdBy:Int = 000
    var createdName:String = ""
    var familyLogo:String = ""
    var fileType:String = ""
    var from_id:Int = 000

    
//        "created_by" = 28;
//        "created_user_name" = Nissanth;
//        "family_logo" = "<null>";
//        "file_type" = "<null>";
//        "from_id" = 244;
//        "group_name" = "<null>";
//        id = 262;
//        "is_active" = 1;
//        "is_approved" = 1;
//        "is_shareable" = 1;
//        "last_conv_date" = "2019-12-31T08:17:25.538Z";
//        muted = "<null>";
//        "orgin_id" = 244;
//        "parent_post_created_user_name" = Nissanth;
//        "parent_post_grp_name" = "Innovation Incubator ";
//        "post_attachment" =             (
//                            {
//                filename = "post-1576264389879.png";
//                height = 1920;
//                type = "image/png";
//                width = 1080;
//            }
//        );
//        "post_id" = 262;
//        "post_info" =             {
//        };
//        "post_ref_id" = SH11EBslfu1576264402285;
//        "privacy_type" = public;
//        propic = "propic-1576505001945.jpg";
//        "shared_user_count" = 0;
//        "shared_user_id" = 28;
//        "shared_user_names" = "<null>";
//        "snap_description" = "test post for innobation";
//        title = "";
//        "to_group_id" = "<null>";
//        "to_user_id" = "<null>";
//        type = post;
//        updatedAt = "2019-12-13T21:05:46.942Z";
//        "views_count" = 12;
    init(_ map: [String : Any]) throws {
    }
}*/


