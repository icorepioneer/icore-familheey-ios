//
//  FamilyListingModel.swift
//  familheey
//
//  Created by familheey on 19/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import Foundation
import Tailor
import SwiftyJSON


struct familyListResponse:SafeMappable {
    
    var familyList:[familyModel]?
    var statusCode:Int = 000
    var Memcount : FamCountModel?
    var pending_count:Int = 000
    
    init(_ map: [String : Any]) throws {
        familyList <- map.relations("data")
        statusCode <- map.property("status_code")
        Memcount <- map.relation("count")
        pending_count <- map.property("pending_count")
    }
}

struct familyModel:SafeMappable{
    var faName:String = ""
    var faCategory:String = ""
    var faRegion:String = ""
    var faAdmin: String = ""
    var faCoverPic:String = ""
    var faLogo:String = ""
    var faId:Int = 000
    var faIntro:String = ""
    var linkFamily:Int = 000
    var linkFamilyApproval:Int = 000
    var memberApproval:Int = 000
    var memberJoining:Int = 000
    var postApproval:Int = 000
    var postVisibility:Int = 000
    var searchable:Int = 000
    var postCreate:Int = 000
    var faType:String = ""
    var islinkable:Int = 000
    var isJoined:Int = 000
    var req_status:String = ""
    var isLinked:String = ""
    var isAdmin:String = ""
    var memberCount:String? = ""
    var knownCount:String? = ""
    var isActive:Int = 000
    var isBlocked:Int = 000
    var createdByName:String = ""
    var joinStatus:String = ""
    var originalCover:String = ""
    var announcementCreate:Int = 000
    var announcementVisibility:Int = 000
    var announcementApproval:Int = 000
    var isFamilyActive: Bool = false
    var isMembershipActive: Bool = false
    var invitationStatus: Int = 000
    var knownMemberCount:String = ""
    
    var history_text:String = ""
    var history_images:[[String:String]] = []

    var f_link:String = ""
    var f_text:String = ""
    
    var membershipName:String = ""
    var membershipFrom:String = ""
    var membershipTo:String = ""
    var membershipId: Int = 000
    var membershipFee:Int = 0000
    var membershipDuration:String = ""
    var membership_period_type_id:Int = 00
    var request_visibility:Int = 00
    var membership_payment_status:String = ""
    var membership_period_type:String = ""
    var group_map_id:Int = 000
    var membership_payment_notes:String  = ""
    var membership_customer_notes:String = ""
    var membership_total_payed_amount:Int = 0000
    
    var stripe_account_id:String = ""
    var stripe_oauth_ref_key:String = ""
    
    init(_ map: [String : Any]) throws {
        print(map)
        faName <- map.property("group_name")
        request_visibility <- map.property("request_visibility")

        faCategory <- map.property("group_category") //groupCategory
        faAdmin <- map.property("created_by")
        faRegion <- map.property("base_region")
        faCoverPic <- map.property("cover_pic")
        faLogo <- map.property("logo")
        faId <- map.property("id")
        faIntro <- map.property("intro")
        linkFamily <- map.property("link_family")
        linkFamilyApproval <- map.property("link_approval")
        memberApproval <- map.property("member_approval")
        memberJoining <- map.property("member_joining")
        postApproval <- map.property("post_approval")
        postVisibility <- map.property("post_visibilty")
        postCreate <- map.property("post_create")
        searchable <- map.property("searchable")
        faType <- map.property("group_type")
        islinkable <- map.property("is_linkable")
        isJoined <- map.property("is_joined")
        req_status <- map.property("req_status")
        isLinked <- map.property("is_linked")
        isAdmin <- map.property("user_status")
        memberCount <- map.property("member_count")
        isActive <- map.property("following")
        isBlocked <- map.property("is_blocked")
        createdByName <- map.property("created_by_name")
        knownCount <- map.property("post_count")
        joinStatus <- map.property("joined_status")
        originalCover <- map.property("group_original_image")
        announcementCreate <- map.property("announcement_create")
        announcementVisibility <- map.property("announcement_visibility")
        announcementApproval <- map.property("announcement_approval")
        isFamilyActive <- map.property("is_active")
        history_text <- map.property("history_text")
        history_images <- map.property("history_images")
        invitationStatus <- map.property("invitation_status")
        knownMemberCount <- map.property("knowncount")
        isMembershipActive <- map.property("is_membership")
        
        membershipName <- map.property("membership_type")
        membershipFrom <- map.property("membership_from")
        membershipTo <- map.property("membership_to")
        membershipId <- map.property("membership_id")
        membershipFee <- map.property("membership_fees")
        membershipDuration <- map.property("membership_duration")
        membership_period_type_id <- map.property("membership_period_type_id")
        membership_payment_status <- map.property("membership_payment_status")
        membership_period_type <- map.property("membership_period_type")
        group_map_id <- map.property("group_map_id")
        membership_payment_notes <- map.property("membership_payment_notes")
        membership_customer_notes <- map.property("membership_payment_notes")
        membership_total_payed_amount <- map.property("membership_total_payed_amount")
        
        f_link <- map.property("f_link")
        f_text <- map.property("f_text")
        
        stripe_account_id <- map.property("stripe_account_id")
        stripe_oauth_ref_key <- map.property("stripe_oauth_ref_key")
    }
}

struct FamCountModel:SafeMappable{
    var connections:Int? = 000
    var memberCount:Int? = 000
    var eventCount:Int? = 000
    
    init(_ map: [String : Any]) throws {
        memberCount <- map.property("familyMembers")
        connections <- map.property("knownConnections")
        eventCount <- map.property("post_count")
    }
}
