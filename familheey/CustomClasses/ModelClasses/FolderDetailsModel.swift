//
//  FolderDetailsModel.swift
//  familheey
//
//  Created by familheey on 30/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import Foundation
import Tailor

struct FolderListResult:SafeMappable{
    var page:Int = 000
    var offset:Int = 000
    var status_code:Int = 000
    var isMember:Int = 000
    var result:[FolderDetails]?
    
    init(_ map: [String : Any]) throws {
        status_code <- map.property("status_code")
        page <- map.property("page")
        offset <- map.property("pageSize")
        isMember <- map.property("member")
        result <- map.relations("rows")
    }
}

struct FolderDetails:SafeMappable{
    var createdBy:Int = 000
    var folderName:String = ""
    var groupId:Int = 000
    var folderId:Int = 000
    var isSharable:Int = 000
    var isActive:Int = 000
    var type:String = ""
    var subFolder:Int = 000
    
    init(_ map: [String : Any]) throws {
        createdBy <- map.property("created_by")
        folderName <- map.property("folder_name")
        groupId <- map.property("group_id")
        folderId <- map.property("id")
        isSharable <- map.property("is_sharable")
        isActive <- map.property("is_active")
        type <- map.property("type")
        subFolder <- map.property("subfolder_of")
    }
}
