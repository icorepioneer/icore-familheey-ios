//
//  ViewMemberModel.swift
//  familheey
//
//  Created by familheey on 25/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import Foundation
import Tailor

struct viewFamilyMembersResult: SafeMappable {
    
    var joinData : [viewMemberDetailsResult]?
    var status_code:Int = 000
    var adminCount:Int = 000
    
    init(_ map: [String : Any]) throws {
        joinData <- map.relations("data")
        status_code <- map.property("status_code")
        adminCount <- map.property("adminsCount")
    }
}

struct viewMemberDetailsResult:SafeMappable {
    var fullname:String = ""
    var userId:Int = 000
    var userType:String = ""
    var isAdmin:String = ""
    var gender:String = ""
    var crntUserId:Int = 000
    var email:String = ""
    var relationShip:String = ""
    var is_blocked:Bool = false
    var is_removed:Bool = false
    var tableId:Int = 000
    var id:Int = 000
    var propic :String = ""
    var memberSince :String = ""
    
    init(_ map: [String : Any]) throws {
        fullname <- map.property("full_name")
        userId <- map.property("user_id")
        userType <- map.property("user_type")
        isAdmin <- map.property("is_primar_admin")
        gender <- map.property("gender")
        crntUserId <- map.property("crnt_user_id")
        email <- map.property("email")
        relationShip <- map.property("relation_ship")
        is_blocked <- map.property("is_blocked")
        is_removed <- map.property("is_removed")
        tableId <- map.property("relation_id")
        id <- map.property("id")
        propic <- map.property("propic")
        memberSince <- map.property("member_since")
    }
}

