//
//  StatusModel.swift
//  familheey
//
//  Created by Giri on 27/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import Foundation
import Tailor

struct userStatusList: SafeMappable {
    
    var result : [userStatus]?
    var status_code:Int = 000
    
    init(_ map: [String : Any]) throws {
        result <- map.relations("data")
        status_code <- map.property("status_code")
    }
}

struct userStatus: SafeMappable {
    var statusID:Int = 000
    var username:String = ""
    var type:String = ""
    var url:String = ""

    init(_ map: [String : Any]) throws {
        statusID <- map.property("statusID")
        username <- map.property("username")
        type <- map.property("type")
        url <- map.property("url")
    }
    
}
