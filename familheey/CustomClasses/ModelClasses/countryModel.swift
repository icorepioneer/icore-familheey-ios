//
//  countryModel.swift
//  familheey
//
//  Created by familheey on 03/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import Foundation

struct CountryList: Codable {
    let countries : [Countries]?
    
    enum CodingKeys: String, CodingKey {
        
        case countries = "countries"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        countries = try values.decodeIfPresent([Countries].self, forKey: .countries)
    }
    
}

struct Countries : Codable {
    let code : String?
    let name : String?
    
    enum CodingKeys: String, CodingKey {
        
        case code = "code"
        case name = "name"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(String.self, forKey: .code)
        name = try values.decodeIfPresent(String.self, forKey: .name)
    }
    
}
