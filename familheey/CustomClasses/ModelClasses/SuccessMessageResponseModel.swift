//
//  SuccessMessageResponseModel.swift
//  familheey
//
//  Created by familheey on 26/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import Foundation
import Tailor

struct requestSuccessModel:SafeMappable {
    var status_code:Int = 000
    var msg:String = ""
    var follow:Int = 000
    
    init(_ map: [String : Any]) throws {
        status_code <- map.property("status_code")
        msg <- map.property("message")
        follow <- map.property("following")
    }
}
