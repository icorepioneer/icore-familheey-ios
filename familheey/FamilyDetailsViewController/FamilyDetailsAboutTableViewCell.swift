//
//  FamilyDetailsAboutTableViewCell.swift
//  familheey
//
//  Created by Giri on 20/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class FamilyDetailsAboutTableViewCell: UITableViewCell {

    
    @IBOutlet weak var btnHistoryImage: UIButton!
    @IBOutlet weak var lblCellHeading: UILabel!
    @IBOutlet weak var textviewAbout: UITextView!
    @IBOutlet weak var editTextVw2: UIView!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var texttoLeftSpacing: NSLayoutConstraint!
    @IBOutlet weak var widthOfImgHistory: NSLayoutConstraint!
    @IBOutlet weak var imgViewOfHistory: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
