//
//  FamilyDetailsTitlesCollectionViewCell.swift
//  familheey
//
//  Created by Giri on 20/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class FamilyDetailsTitlesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
}
