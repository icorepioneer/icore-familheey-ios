//
//  FamilySettingsViewController.swift
//  familheey
//
//  Created by familheey on 29/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

import Moya
import Alamofire
import Kingfisher
import SwiftyJSON

class FamilySettingsTableViewCell : UITableViewCell{
    @IBOutlet weak var imgPic: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
}

class FamilyAccountTableViewCell : UITableViewCell{
    @IBOutlet weak var grpAcntNumber: UIView!
    @IBOutlet weak var lblAcntNum: UILabel!
    @IBOutlet weak var lblAcntStatus: UILabel!
    @IBOutlet weak var btnNoAcnt: UIButton!
    
}

protocol updateAfterFamilySettingsEdit:class {
    func updateFamilyDetailsFromSettings()
}

class FamilySettingsViewController: UIViewController, EditFamilyDetailsDelegate,UITextFieldDelegate {
    var callBackdelegate:familySettingUpdateDelegate!

    var urlEditEnable = false
    var clickCancel = false
    var isUpdateSuccess = false
    
    var shareURLString  =  ""
    var urlTextString  =  ""
    weak var delegate: updateAfterFamilySettingsEdit?
    
    @IBOutlet weak var tblListView: UITableView!
    
    private var networkProvider                       = MoyaProvider<FamilyheeyApi>()
    
    var titleArr = [String]()
    var isAdimn = ""
    var famArr : [familyModel]?
    var groupId = ""
    var grpAcnt = ""
    var acntStatus = ""
    var isGotoStripeAcnt = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

    }
    
    override func viewWillAppear(_ animated: Bool) {


        if self.famArr![0].isAdmin.lowercased() == "admin"{
            self.titleArr = ["Basic settings","Advanced settings","Leave family","Delete family"]
        }
            
        else{
            self.titleArr = ["Leave Family"]
        }
        if self.famArr![0].isAdmin.lowercased() == "admin"{
            if famArr![0].isActive == 1{
                self.titleArr.insert("Unfollow Family", at: 2)
            }
            else{
                self.titleArr.insert("Follow Family", at: 2)
            }
        }
        else{
            if famArr![0].isActive == 1{
                self.titleArr.insert("Unfollow Family", at: 1)
            }
            else{
                self.titleArr.insert("Follow Family", at: 1)
            }
        }
        self.shareURLString  =  famArr![0].f_link
        self.urlTextString = famArr![0].f_text
        
        
        grpAcnt = self.famArr![0].stripe_account_id as String
        
        if self.famArr![0].isAdmin.lowercased() == "admin"{
            if !grpAcnt.isEmpty{
                getStripeAccountByID()
            }
            if isGotoStripeAcnt{
                getFamilyDetails(groupId: groupId)
            }
        }
        
        self.tblListView.estimatedRowHeight = 130
        self.tblListView.rowHeight = UITableView.automaticDimension
        
        tblListView.delegate = self
        tblListView.dataSource = self
        tblListView.reloadData()
    }
    
    //MARK:- WEB API
    func groupFollow(url:String){
        APIServiceManager.callServer.familyFollowStatus(url: url, userId: UserDefaults.standard.value(forKey: "userId") as! String, gropId: groupId, success: { (responseMdl) in
            
            guard let follow = responseMdl as? requestSuccessModel else{
                return
            }
            ActivityIndicatorView.hiding()
            if follow.status_code == 200{
                // self.isActive = follow.follow
                if self.famArr![0].isAdmin.lowercased() == "admin"{
                    self.titleArr = ["Basic Settings","Advance Settings","Leave Family","Delete Family"]
                }
                else{
                    self.titleArr = ["Leave Family"]
                }
                if self.famArr![0].isAdmin.lowercased() == "admin"{
                    if follow.follow == 1{
                        self.titleArr.insert("Unfollow Family", at: 2)
                    }
                    else{
                        self.titleArr.insert("Follow Family", at: 2)
                    }
                }
                else{
                    if follow.follow == 1{
                        self.titleArr.insert("Unfollow Family", at: 1)
                    }
                    else{
                        self.titleArr.insert("Follow Family", at: 1)
                    }
                }
                
                self.tblListView.reloadData()
            }
            
        }) { (error) in
            
        }
    }
    
    func LeaveFamily(){
        let parameter = ["user_id":UserDefaults.standard.value(forKey: "userId") as! String, "group_id":groupId] as [String : Any]
        ActivityIndicatorView.show("Loading....")
        
        networkProvider.request(.LeaveGroup(parameter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    ActivityIndicatorView.hiding()
                    let jsonData =  JSON(response.data)
                    
                    print("Json data : \(jsonData)")
                    
                    if response.statusCode == 200{
                        print(jsonData)
//                        self.displayAlert(alertStr: "You have left this family", title: "")
                        self.displayAlertChoice(alertStr: "You have left this family", title: "") { (result) in
                            if appDel.isFamilyFromSearch{
                                appDel.isFamilyFromSearch = false
                                self.navigationController?.popToViewController(ofClass: SearchTabViewController.self)
                            }
                            else{
                                self.navigationController?.popToViewController(ofClass: FamiliesTabViewController.self)

                            }
                        }
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.LeaveFamily()
                        }
                    }
                }catch let err {
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    func getFamilyDetails(groupId:String){
        APIServiceManager.callServer.getFamilyDetails(url: EndPoint.getFamily, groupId: groupId, userId:UserDefaults.standard.value(forKey: "userId") as! String, success: { (responseMdl) in
            guard let familyMdl = responseMdl as? familyListResponse else{
                return
            }
            ActivityIndicatorView.hiding()
            if familyMdl.statusCode == 200{
                self.famArr = familyMdl.familyList
                self.shareURLString  =  self.famArr![0].f_link
                self.urlTextString = self.famArr![0].f_text
                if self.isGotoStripeAcnt{
                    self.isGotoStripeAcnt = false
                    if self.famArr![0].stripe_account_id.isEmpty{
                    }
                    else{
                        self.getStripeAccountByID()
                    }
                }
                
                self.tblListView.reloadData()
                //                self.tblListView.reloadRows(at: [IndexPath.init(row: 0, section: 2)], with: .none)
            }
        }) { (error) in
            
        }
    }
    
    func deleteFamily(){
        let parameter = ["is_active":"false","is_removed":"true", "id":groupId, "user_id":UserDefaults.standard.value(forKey: "userId") as! String,"action":"delete_family"] as [String : Any]
        
        ActivityIndicatorView.show("Loading....")
        
        networkProvider.request(.updateFamilyWithHistory(parameter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    ActivityIndicatorView.hiding()
                    let jsonData =  JSON(response.data)
                    
                    print("Json data : \(jsonData)")
                    
                    if response.statusCode == 200{
                        print(jsonData)
                        self.navigationController?.popToViewController(ofClass: FamiliesTabViewController.self)
                        //                        self.navigationController?.popToRootViewController(animated: true)
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.deleteFamily()
                        }
                    }
                }catch let err {
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    func getStripeAccountByID(){
        
        let parameter = ["group_id":self.groupId] as [String : Any]
        print(parameter)
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.stripeGetaccountById(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                do {
                    let jsonData = JSON(response.data as Any)
                    print("!!!!@ --- \(jsonData)")
                    if response.statusCode == 200{
                        if jsonData.count > 0{
                            if let payouts = jsonData["payouts_enabled"].bool, let charges = jsonData["charges_enabled"].bool, payouts && charges{
                                self.acntStatus = "Active"
                            }
                            else{
                                self.acntStatus = "Pending"
                            }
                            self.tblListView.reloadSections(IndexSet(integer : 2), with: .none)
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getStripeAccountByID()
                        }
                    }
                    
                } catch let err {
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
    //MARK:- Custom Delegate
    func EditFamilyDetails() {
        getFamilyDetails(groupId: groupId)
    }
    
    //MARK:- Button actions
    
    @IBAction func onClickBack(_ sender: Any) {
        self.delegate?.updateFamilyDetailsFromSettings()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickCreateAcnt(_ sender: Any) {
        
        
        let parameter = ["group_id":groupId] as [String : Any]
        print(parameter)
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.stripe_oauth_link_generation(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                do {
                    let jsonData = JSON(response.data as Any)
                    print("!!!!@ --- \(jsonData)")
                    if response.statusCode == 200{
                        if let urlStr = jsonData["link"].string, !urlStr.isEmpty{
                            self.isGotoStripeAcnt = true
                            let url = URL(string: urlStr)
                            UIApplication.shared.open(url!)
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            let btn = UIButton()
                            self.onClickCreateAcnt(btn)
                        }
                    }
                } catch let err {
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


extension FamilySettingsViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.famArr![0].isAdmin.lowercased() == "admin"{
            return 3
        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
  
      /*  if indexPath.section == 0
        {
            return 40
        }
        else if indexPath.section == 2{
            return UITableView.automaticDimension
        }
        else
        {
            return 90
        }*/
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0
        {
            return titleArr.count
        }else
        {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FamilySettingsTableViewCell", for: indexPath) as! FamilySettingsTableViewCell
            
            tableView.tableFooterView = UIView()
            cell.selectionStyle = .none
            
            cell.lblName.text = titleArr[indexPath.row]
            
            return cell
            
        }
        else if indexPath.section == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FamilyAccountTableViewCell", for: indexPath) as! FamilyAccountTableViewCell
            
            if grpAcnt.isEmpty{
                cell.grpAcntNumber.isHidden = true
                cell.btnNoAcnt.isHidden = false
            }
            else{
                cell.btnNoAcnt.isHidden = true
                cell.grpAcntNumber.isHidden = false
                cell.lblAcntNum.text = grpAcnt
                cell.lblAcntStatus.text = acntStatus
            }
            
            return cell
        }
        else
        {
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "FamilyUrlEditTableViewCell", for: indexPath) as! FamilyUrlEditTableViewCell
            //            cell.txtFamUrl.tag = 2
            //            cell.txtFamUrl.delegate = self
            
            //            btnShare: UIButton!
            //            @IBOutlet weak var imgShare: UIImageView!
            //            @IBOutlet weak var lblUrl: UILabel!
            //            @IBOutlet weak var btnEdit: UIButton!
            //            @IBOutlet weak var imgEdit: U
            
            cell.lblUrl.text = self.shareURLString
            //            if !urlEditEnable
            //            {
            //            cell.txtFamUrl.text = self.shareURLString
            //            cell.txtFamUrl.isUserInteractionEnabled = false
            //            cell.btnEdit.tag = indexPath.row
            //                cell.btnSave.isHidden = true
            //
            if famArr![0].isAdmin.lowercased() == "admin"{
                cell.btnEdit.isHidden = false
                cell.imgEdit.isHidden = false
                
                cell.btnShare.isHidden = false
                cell.imgShare.isHidden = false
                
            }
            else{
                cell.btnEdit.isHidden = true
                cell.imgEdit.isHidden = true
                cell.btnShare.isHidden = false
                cell.imgShare.isHidden = false
                
            }
            //            }
            //            else
            //            {
            //                cell.txtFamUrl.text = self.urlTextString
            //                cell.txtFamUrl.isUserInteractionEnabled = true
            //                self.urlEditEnable = false
            //                cell.btnSave.isHidden = false
            //
            //                cell.btnShare.isHidden = true
            //                cell.imgShare.isHidden = true
            //
            //            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            if famArr![0].isAdmin.lowercased() != "admin"{
                if indexPath.row == 0{
                    self.LeaveFamily()
                }
                else{
                    if self.titleArr[indexPath.row].lowercased() == "unfollow family"{
                        self.groupFollow(url: EndPoint.unfollow)
                    }
                    else{
                        self.groupFollow(url: EndPoint.follow)
                    }
                }
            }
            else{
                if indexPath.row == 2{
                    if self.titleArr[indexPath.row].lowercased() == "unfollow family"{
                        self.groupFollow(url: EndPoint.unfollow)
                    }
                    else{
                        self.groupFollow(url: EndPoint.follow)
                    }
                }
                else if indexPath.row == 3{
                    let alert = UIAlertController(title: "Familheey", message: "Do you want to leave this family", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "YES", style: .destructive, handler: { (acion) in
                        self.LeaveFamily()
                    }))
                    
                    alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }
                else if indexPath.row == 0{
                    let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "FamilyEditViewController") as! FamilyEditViewController
                    vc.delegate = self
                    vc.famArr = self.famArr
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else if indexPath.row == 1{
                    let storyboard = UIStoryboard.init(name: "CreateFamily", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "addFamilyFive") as! AddFamilyScreenFiveViewController
                    vc.isFromGroup = "1"
                    vc.callBackdelegate = self.callBackdelegate
                    vc.famArr = self.famArr
                    vc.groupType = self.famArr![0].faType.firstUppercased
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else if indexPath.row == 4{
                    let alert = UIAlertController(title: "Familheey", message: "Do you want to delete this family", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "YES", style: .destructive, handler: { (acion) in
                        self.deleteFamily()
                    }))
                    
                    alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }
            }
            
            
        }
    }
    
    
    @IBAction func onClickEditUrl(_ sender: UIButton) {
//        self.clickCancel = false
        self.alertWithTF()
        //            self.urlEditEnable = true
        //            tblListView.beginUpdates()
        //                   tblListView.reloadRows(at: [IndexPath.init(row: 0, section: 1)], with: .none)
        //                   tblListView.endUpdates()
        
        
    }
    
    @IBAction func onClickUrlShare(_ sender: Any) {
        
        Helpers.showActivityViewcontroller(url:shareURLString, VC: self)
        
    }
    
    @IBAction func OnClick_SaveUrl(_ sender: Any) {

    }
    
    func apiForFamilyURLavailability(text:String) {

        let parameter = ["f_text":text] as [String : Any]
        print(parameter)
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.familyURL_availabilitychecking(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    print(jsonData["status"])
                    print(response.statusCode)
                    if response.statusCode == 200
                    {
                       
                        if jsonData["count"].intValue == 0
                        {
                            self.urlEditEnable = true
//                            self.tblListView.beginUpdates()
//                            self.tblListView.reloadRows(at: [IndexPath.init(row: 0, section: 1)], with: .none)
//                            self.tblListView.endUpdates()
                            
                            
                            self.apiForFamilyUrlUpdate(urlText: text)

                        }
                        else
                        {
                            Helpers.showAlertDialog(message: "URL is already being used. Please enter valid URL ", target: self)

                        }
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.apiForFamilyURLavailability(text: text)
                            
                        }
                    }
                    else
                    {
                        //                        self.imgOfUrlAvailability.isHidden = true
                        
                        Helpers.showAlertDialog(message: "Error in URL availability checking", target: self)
                        //                        self.txtEventPage.text = ""
                    }
                    
                    
                    
                } catch let err {
                    
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
    
    func apiForFamilyUrlUpdate(urlText:String) {
        
        var parameter = [String:Any]()
        
        parameter = ["id":groupId,"f_text":urlText,"user_id":UserDefaults.standard.value(forKey: "userId") as! String
            ] as [String : Any]
        
        print(parameter)
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.updateFamilyWithHistory(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    print(jsonData["status"])
                    print(response.statusCode)
                    if response.statusCode == 200
                    {
                        //
                        let AlertCus = CustomAlertVC.show(in: self, title: "Success", message: "Successfully Updated Family", image:UIImage.init(named: "Green_tick")!)
                        AlertCus.addAction(CPAlertAction.init(title: "OK", type: .normal, handler: {
                            
                            self.getFamilyDetails(groupId: self.groupId)
                            
                            //                                let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                            //                                let vc = storyboard.instantiateViewController(withIdentifier: "mainTabView") as! HomeViewTabController
                            //                                self.navigationController?.pushViewController(vc, animated: true)
                            
                            //                                self.delegate?.updateAboutHistory(data:String,firstImg:UIImage)
                            //                                                           self.navigationController?.popViewController(animated: true)
                            //                                        let cell = self.tblListView.cellForRow(at: IndexPath.init(row: 0, section: 2))as! FamilyUrlEditTableViewCell
                            //                                        cell.lblUrl.text = jsonData["data"][0]["f_link"].stringValue
                            //                                        self.shareURLString = jsonData["data"][0]["f_link"].stringValue
                            //                                        self.tblListView.reloadRows(at: [IndexPath.init(row: 0, section: 2)], with: .none)
                            
                        }))
                        
                        
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.apiForFamilyUrlUpdate(urlText: urlText)
                            
                        }
                    }
                    else
                    {
                        
                        print(jsonData["code"])
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                    
                    
                } catch let err {
                    
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
//    func textFieldDidEndEditing(_ textField: UITextField) {
//                   if textField.tag == 2
//                   {
//
//            if !self.clickCancel
//
//                       {
//                         //  self.apiForEventURLavailability(text:textField.text!)
//                        self.apiForFamilyURLavailability(text:textField.text!)
//
//                       }
//              }
//          }
    func alertWithTF() {
        let result = self.shareURLString.split(separator: "/")
        let editedText = self.shareURLString.replacingOccurrences(of: result[result.count-1], with: "")
        print(editedText)
        let urlText = self.shareURLString.components(separatedBy: "/").last
        //Step : 1
        let alert = UIAlertController(title: "Change Url", message: editedText, preferredStyle: UIAlertController.Style.alert )
        //Step : 2
        let save = UIAlertAction(title: "Save", style: .default) { (alertAction) in
            self.clickCancel = false
            let textField = alert.textFields![0] as UITextField
            alert.dismiss(animated: true, completion: nil)

            if textField.text!.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                //Read TextFields text data

//                if self.urlEditEnable
//                {
//
//                self.apiForFamilyUrlUpdate(urlText:textField.text!)
//                }
//                else
//                {
//                    Helpers.showAlertDialog(message: "URL is already being used. Please enter valid URL ", target: self)
//
//                }
                if textField.text! == urlText{
                    print("same")
                }
                else{
                  self.apiForFamilyURLavailability(text:textField.text!.trimmingCharacters(in: .whitespacesAndNewlines))
                }
                
                
            } else {
                Helpers.showAlertDialog(message: "Please enter valid URL", target: self)
                
            }
            
            
        }
        
        //Step : 3
        //For first TF
        alert.addTextField { (textField) in
            textField.placeholder = "Enter URL text"
            textField.tag = 2
            textField.delegate = self
        }
        
        
        //Step : 4
        alert.addAction(save)
        //Cancel action
        let cancel = UIAlertAction(title: "Cancel", style: .destructive) { (alertAction) in
            self.clickCancel = true
            alert.dismiss(animated: true, completion: nil)
            
        }
        alert.addAction(cancel)
        
        
        self.present(alert, animated:true, completion: nil)
        
    }

    //MARK:- textfield delegates 
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (string == " ") {
            return false
        }
        return true
    }
//
//    func updateFamilySetting(famArr : [familyModel])
//    {
//      self.famArr = famArr
//        print(self.famArr)
//    }
//
    
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        if textField.tag == 2
//        {
//            if !self.clickCancel
//
//            {
//                self.apiForEventURLavailability(text:textField.text!)
//            }
//
//        }
//    }
}
