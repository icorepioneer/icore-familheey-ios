//
//  FamilyEditViewController.swift
//  familheey
//
//  Created by familheey on 10/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import CropViewController
import GooglePlaces

protocol EditFamilyDetailsDelegate:class {
    func EditFamilyDetails()
}

class FamilyEditViewController: UIViewController, CropViewControllerDelegate {
    @IBOutlet weak var tblSettings: UITableView!
    
    var famArr : [familyModel]?
    var famName = ""
    var famCategory = ""
    var famRegion = ""
    var famCover = ""
    var famLogo = ""
    var famType = ""
    var famSearchable = ""
    var famLinkable = ""
    weak var delegate:EditFamilyDetailsDelegate?
    var imagePicker = UIImagePickerController()
    var btnTag: Int = Int()
    var imgCover = UIImage()
    var imgLogo = UIImage()
    var imgCoverData = NSData()
    var imgLogoData = NSData()
    var originalPicData = NSData()
    var originalCoverImg = UIImageView()
    var selectedLat : Double!
    var selectedLong :Double!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        famName = famArr![0].faName
        famCategory = famArr![0].faCategory
        famRegion = famArr![0].faRegion
        famType = famArr![0].faType
        if famArr![0].islinkable == 1{
            famLinkable = "true"
        }
        else{
            famLinkable = "false"
        }
        
        if famArr![0].searchable == 1{
            famSearchable = "true"
        }
        else{
            famSearchable = "false"
        }
        
       // tblSettings.register(UITableViewCell.self, forCellReuseIdentifier: "CellId")
        tblSettings.delegate = self
        tblSettings.dataSource = self

        addRightNavigationButton()
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if appDel.selectedBaseName.count > 0{
            
            famRegion = appDel.selectedBaseName
            
            var model = self.famArr![0]
            model.faRegion = famRegion
            
            self.famArr![0] = model
            self.tblSettings.reloadData()
//            self.tableView.beginUpdates()
//            self.tableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .none)
//            self.tableView.endUpdates()
        }
      
    }
    

    //MARK:- Custom Methods
    func addRightNavigationButton(){
        let rightButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(onClickrightButtonAction))
        self.navigationItem.rightBarButtonItem = rightButtonItem
    }
    
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else{
            // self.displayAlert(alertStr: "You don't have camera", title: "")
        }
    }
    
    func openGallary(){
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func selectedImage(img:UIImage){
        // let cropView = CropViewController(image: img)
        let cropView = CropViewController(croppingStyle: .default, image: img)
        cropView.aspectRatioLockEnabled = true
        cropView.aspectRatioLockDimensionSwapEnabled = true
        cropView.aspectRatioPickerButtonHidden = true
        if btnTag == 1{
            self.originalPicData = img.jpegData(compressionQuality: 1)! as NSData
            cropView.aspectRatioPreset = TOCropViewControllerAspectRatioPreset.preset4x3
        }
        else{
            cropView.aspectRatioPreset = TOCropViewControllerAspectRatioPreset.presetSquare
        }
        cropView.delegate = self
        self.navigationController?.pushViewController(cropView, animated: true)
    }
    
    //MARK:- button Actions
    @objc func onClickrightButtonAction(sender: UIBarButtonItem){
       
    }
    
    @IBAction func onClickDone(_ sender:UIButton){
        let id = famArr![0].faId
        if famName.count > 0{
           var params = [String:String]()
            if self.selectedLat != nil && self.selectedLong != nil{
                if famType.lowercased() == "private"{
                     params = [
                        "id":"\(id)",
                        "searchable":famSearchable,
                        "group_type":famType,
                        "member_joining":"1",
                        "group_name":famName,
                        "group_category":famCategory,
                        "base_region":famRegion,
                        "is_linkable":famLinkable,
                        "user_id":UserDefaults.standard.value(forKey: "userId") as! String,
                        "lat":"\(String(describing: self.selectedLat!))",
                        "long":"\(String(describing: self.selectedLong!))"
                    ] as [String:String]
                }
                else{
                     params = [
                        "id":"\(id)",
                        "searchable":famSearchable,
                        "group_type":famType,
                        "member_joining":"3",
                        "group_name":famName,
                        "group_category":famCategory,
                        "base_region":famRegion,
                        "is_linkable":famLinkable,
                        "user_id":UserDefaults.standard.value(forKey: "userId") as! String,
                        "lat":"\(String(describing: self.selectedLat!))",
                        "long":"\(String(describing: self.selectedLong!))"
                    ] as [String:String]
                }
            }
            else{
                if famType.lowercased() == "private"{
                     params = [
                        "id":"\(id)",
                        "searchable":famSearchable,
                        "group_type":famType,
                        "member_joining":"1",
                        "group_name":famName,
                        "group_category":famCategory,
                        "base_region":famRegion,
                        "is_linkable":famLinkable,
                        "user_id":UserDefaults.standard.value(forKey: "userId") as! String
                    ]
                }
                else{
                     params = [
                        "id":"\(id)",
                        "searchable":famSearchable,
                        "group_type":famType,
                        "member_joining":"3",
                        "group_name":famName,
                        "group_category":famCategory,
                        "base_region":famRegion,
                        "is_linkable":famLinkable,
                        "user_id":UserDefaults.standard.value(forKey: "userId") as! String
                    ]
                }

            }
            
            
            print(params)
            APIServiceManager.callServer.editFamilyDetails(url: EndPoint.updateFamily, params: params , coverPic: imgCoverData, logo: imgLogoData, originalImg: originalPicData, contentType: "Image", success: { (responseModel) in
                
                guard let responseMdl = responseModel as? familyListResponse else{
                    return
                }
                ActivityIndicatorView.hiding()
                
                if responseMdl.statusCode == 200{
//                    self.displayAlert(alertStr: "Edited successfully", title: "")
                    self.displayAlertChoice(alertStr: "Edited successfully", title: "") { (result) in
                        self.delegate?.EditFamilyDetails()
                        self.navigationController?.popViewController(animated: true)
                    }
                }
                
            }) { (error) in
                self.displayAlert(alertStr: error!.description, title: "")
            }
        }
    }
    
    @IBAction func onClickBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickImageTouch(_ sender:UIButton){
        if sender.tag == 2{
            
            guard #available(iOS 13.0, *) else {
                // Code for earlier iOS versions
                let story = UIStoryboard.init(name: "Main", bundle: nil)
                let SearchPlacesViewController = story.instantiateViewController(withIdentifier: "SearchPlacesViewController") as! SearchPlacesViewController
                SearchPlacesViewController.selectedField = "base"
                self.present(SearchPlacesViewController, animated: true, completion: nil)
                
                return
            }
            
            let acController = GMSAutocompleteViewController()
            
            acController.delegate = self
            present(acController, animated: true, completion: nil)
            
            /* if UIDevice.current.systemVersion.contains("13"){
             
             let acController = GMSAutocompleteViewController()
             
             acController.delegate = self
             present(acController, animated: true, completion: nil)
             }else{
             
             let story = UIStoryboard.init(name: "Main", bundle: nil)
             let SearchPlacesViewController = story.instantiateViewController(withIdentifier: "SearchPlacesViewController") as! SearchPlacesViewController
             SearchPlacesViewController.selectedField = "base"
             self.present(SearchPlacesViewController, animated: true, completion: nil)
             
             }*/
            
        }
        else{
            let arr = ["Regular","Company","Individual firm", "Non Profit Organisation", "Institute", "Religious/Spiritual", "Community", "Association", "Team", "Project", "Branch or division", "Others"]
            self.showActionSheet(titleArr: arr as NSArray, title: "Choose option") { (index) in
                if index == 100{
                }
                else{
                    self.famCategory = arr[index]
                    var model = self.famArr![0]
                    model.faCategory = self.famCategory
                    
                    self.famArr![0] = model
                    self.tblSettings.reloadData()
                }
            }
        }
    }
    
    @IBAction func onClickType(_ sender:UIButton){
        if sender.tag == 1{
            famType = "public"
        }
        else{
            famType = "private"
        }
        var model = self.famArr![0]
        model.faType = famType
        
        self.famArr![0] = model
        self.tblSettings.reloadData()
    }
    
    @IBAction func onClickSearchable(_ sender:UIButton){
        var tempInt = Int()
        print(sender.tag)
        if sender.tag == 6 || sender.tag == 7{
            if sender.tag == 6{
                famSearchable = "true"
                tempInt = 1
            }
            else if sender.tag == 7{
                famSearchable = "false"
                tempInt = 0
            }
            var model = self.famArr![0]
            model.searchable = tempInt
            
            self.famArr![0] = model
            
        }
        else{
            if sender.tag == 9{
                famLinkable = "true"
                tempInt = 1
            }
            else if sender.tag == 10{
                famLinkable = "false"
                tempInt = 0
            }
            var model = self.famArr![0]
            model.islinkable = tempInt
            
            self.famArr![0] = model
        }
        self.tblSettings.reloadData()
    }
    
    @IBAction func onClickImagePick(_ sender: UIButton) {
        //let button = sender as! UIButton
        btnTag = sender.tag
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
   
  
    
    //MARK:- CropView SDK Delegate
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        // 'image' is the newly cropped version of the original image
        
        if btnTag == 1{
            imgCover = image
        }
        else{
            imgLogo = image
        }
        tblSettings.reloadData()
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension FamilyEditViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! EditTypeTwoTableViewCell
            cell.imgLogo.cornerRadius = 10
            cell.imgCover.cornerRadius = 10
            cell.imgCover.clipsToBounds = true
            cell.imgLogo.clipsToBounds = true
            
            if imgCover.size.width != 0{
                cell.imgCover.image = imgCover
                imgCoverData = cell.imgCover.image?.pngData() as! NSData
            }
            else{
                if famArr![0].faCoverPic.count != 0{
                    let url = "\(Helpers.imageURl)"+"\(BaseUrl.groupCover)"+famArr![0].faCoverPic.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                    let imgUrl = URL(string: url)
                    
                    let tempOriginal = "\(Helpers.imageURl)"+"\(BaseUrl.group_origianl)"+famArr![0].originalCover.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                    
                    let imgOriginalUrl = URL(string: tempOriginal)
                    self.originalCoverImg.kf.indicatorType = .activity

                    self.originalCoverImg.kf.setImage(with: imgOriginalUrl, placeholder: nil, options: nil, progressBlock: nil) { (image, error, cache, url) in
                        
                        if error == nil{
                            self.originalPicData = self.originalCoverImg.image?.pngData() as! NSData
                        }
                    }
                    cell.imgCover.kf.indicatorType = .activity

                    cell.imgCover.kf.setImage(with: imgUrl, placeholder: nil, options: nil, progressBlock: nil) { (image, error, cache, url) in
                        
                        if error == nil{
                            self.imgCoverData = cell.imgCover.image?.pngData() as! NSData
                        }
                    }
                }
                else{
                    cell.imgCover.image = #imageLiteral(resourceName: "bgTopOnly")
                }
            }
            
            if imgLogo.size.width != 0{
                cell.imgLogo.image = imgLogo
                imgLogoData = cell.imgLogo.image?.pngData() as! NSData
            }
            else{
                if famArr![0].faLogo.count != 0{
                    let url = "\(Helpers.imaginaryImageBaseUrl)"+"width=150&height=150&url="+"\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+famArr![0].faLogo.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                    let imgUrl = URL(string: url)
                    cell.imgLogo.kf.indicatorType = .activity

                    cell.imgLogo.kf.setImage(with: imgUrl, placeholder: nil, options: nil, progressBlock: nil) { (image, error, cache, url) in
                        
                        if error == nil{
                            self.imgLogoData = cell.imgLogo.image?.pngData() as! NSData
                        }
                    }
                }
                else{
                    cell.imgLogo.image = #imageLiteral(resourceName: "Family Logo")
                }
            }
            cell.btnLogo.tag = 2
            cell.btnCover.tag = 1
            cell.btnCover.addTarget(self, action: #selector(onClickImagePick(_:)), for: .touchUpInside)
            cell.btnLogo.addTarget(self, action: #selector(onClickImagePick(_:)), for: .touchUpInside)
            
            
            
            return cell
        }
        else if indexPath.row == 4 || indexPath.row == 5 || indexPath.row == 6{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellid", for: indexPath) as! EditTypeThreeTableViewCell
            
            if indexPath.row == 4{
                cell.lblHead.text = "Family Type"
                cell.lblValue1.text = "Public"
                cell.lblValue2.text = "Private"
                
                if famArr![0].faType.lowercased() == "public"{
                    cell.imgSelct1.image = UIImage(named: "imgRadio_green")
                    cell.imgSelct2.image = UIImage(named: "")
                }
                else{
                    cell.imgSelct2.image = UIImage(named: "imgRadio_green")
                    cell.imgSelct1.image = UIImage(named: "")
                }
                cell.btnValue2.tag = 2
                cell.btnValue1.tag = 1
                
                cell.btnValue1.addTarget(self, action: #selector(onClickType(_:)), for: .touchUpInside)
                cell.btnValue2.addTarget(self, action: #selector(onClickType(_:)), for: .touchUpInside)
            }
            else if indexPath.row == 5{
                cell.lblHead.text = "is Searchable"
                cell.lblValue1.text = "Yes"
                cell.lblValue2.text = "No"
                
                if famArr![0].searchable == 1{
                    cell.imgSelct1.image = UIImage(named: "imgRadio_green")
                    cell.imgSelct2.image = UIImage(named: "")
                }
                else{
                    cell.imgSelct2.image = UIImage(named: "imgRadio_green")
                    cell.imgSelct1.image = UIImage(named: "")
                }
                cell.btnValue2.tag = 2 + indexPath.row
                cell.btnValue1.tag = 1 + indexPath.row
                cell.btnValue1.addTarget(self, action: #selector(onClickSearchable(_:)), for: .touchUpInside)
                cell.btnValue2.addTarget(self, action: #selector(onClickSearchable(_:)), for: .touchUpInside)
            }
            else if indexPath.row == 6{
                cell.lblHead.text = "is Linkable"
                cell.lblValue1.text = "Yes"
                cell.lblValue2.text = "No"
                
                if famArr![0].islinkable == 1{
                    cell.imgSelct1.image = UIImage(named: "imgRadio_green")
                    cell.imgSelct2.image = UIImage(named: "")
                }
                else{
                    cell.imgSelct2.image = UIImage(named: "imgRadio_green")
                    cell.imgSelct1.image = UIImage(named: "")
                }
                cell.btnValue2.tag = 4 + indexPath.row
                cell.btnValue1.tag = 3 + indexPath.row
//                cell.btnValue1.addTarget(self, action: #selector(onClickLinkable(_:)), for: .touchUpInside)
//                cell.btnValue2.addTarget(self, action: #selector(onClickLinkable(_:)), for: .touchUpInside)
                cell.btnValue1.addTarget(self, action: #selector(onClickSearchable(_:)), for: .touchUpInside)
                cell.btnValue2.addTarget(self, action: #selector(onClickSearchable(_:)), for: .touchUpInside)            }
            
            
            return cell
        }
        else if indexPath.row == 7{
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditTypeFourTableViewCell", for: indexPath) as! EditTypeFourTableViewCell
            
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellId", for: indexPath) as! EditTypeOneTableViewCell
            cell.btnAction.tag = indexPath.row
            
            if indexPath.row == 0{
                cell.lblHead.text = "Family Name"
                cell.txtValue.text = self.famArr![0].faName
                cell.btnAction.isHidden = true
                cell.txtValue.delegate = self
                cell.txtValue.isEnabled = true
            }
            else if indexPath.row == 1{
                cell.lblHead.text = "Family Category"
                cell.txtValue.text = self.famArr![0].faCategory
                cell.btnAction.isHidden = false
                cell.txtValue.isEnabled = false
            }
            else{
                cell.lblHead.text = "Family Region"
                cell.txtValue.text = famArr![0].faRegion
                cell.btnAction.isHidden = false
                cell.txtValue.isEnabled = false
            }
            
            cell.btnAction.addTarget(self, action: #selector(onClickImageTouch(_:)), for: .touchUpInside)
            
            return cell
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 3{
            return 200
        }
        else{
            return 110
        }
       
    }
}

extension FamilyEditViewController : UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("......")
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        print("\(textField.text!)")
        if textField.text!.count > 0{
            self.famName = (textField.text)!
            var model = self.famArr![0]
            model.faName = self.famName
            self.famArr![0] = model
        }
        else{
            self.displayAlert(alertStr: "Name can't be empty", title: "")
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        //print(textField.text as! String)
        return true
    }
}

extension  FamilyEditViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker .dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let choosenImage = info[.originalImage] as! UIImage
        selectedImage(img: choosenImage)
        picker.dismiss(animated: true, completion: nil)
        
    }
}

extension FamilyEditViewController: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        // Get the place name from 'GMSAutocompleteViewController'
        // Then display the name in textField
        
//        famRegion = place.name!
        famRegion = "\(place.formattedAddress!)"
        
        var model = self.famArr![0]
        model.faRegion = famRegion
        
        self.famArr![0] = model
        self.tblSettings.reloadData()
        
        self.selectedLat = place.coordinate.latitude
        self.selectedLong = place.coordinate.longitude
        
        print(place.coordinate.latitude)
        print(place.coordinate.longitude)
        
        dismiss(animated: true, completion: nil)
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // Handle the error
        print("Error: ", error.localizedDescription)
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        // Dismiss when the user canceled the action
        dismiss(animated: true, completion: nil)
    }
}




//@IBAction func onClickLinkable(_ sender:UIButton){
//    var tempInt = Int()
//    print(sender.tag)
//    if sender.tag == 1{
//        famLinkable = "true"
//        tempInt = 1
//    }
//    else{
//        famLinkable = "false"
//        tempInt = 0
//    }
//    var model = self.famArr![0]
//    model.islinkable = tempInt
//    
//    self.famArr![0] = model
//    self.tblSettings.reloadData()
//}
