//
//  FamilyEditHistoryViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 27/11/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Alamofire
import Moya
import SwiftyJSON
import YPImagePicker
import AVKit
protocol EditAboutGroupHistoryDelegate:class {
    func updateAboutHistory(history_text:String,history_images:[JSON])

    
}

class FamilyEditHistoryViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate{
    
    @IBOutlet weak var viewInside: UIView!
    @IBOutlet weak var heightOfInsideView: NSLayoutConstraint!
    
    @IBOutlet weak var scrollView: UIScrollView!
    weak var delegate:EditAboutGroupHistoryDelegate?
      var groupId = ""
    @IBOutlet weak var lblNoFiles: UILabel!
    var imageUploadStatus = false
     var vedioUploadStatus = false
     
     var imgDataArr = [Data]()
     var videoDataArr = [Data]()
    @IBOutlet weak var collViewOfAttachments: UICollectionView!
    
    //    let selectedImageV = UIImageView()
        private var networkProvider = MoyaProvider<FamilyheeyApi>()
    @IBOutlet weak var txtDescription: UITextView!
    
    var history_text = ""
       var history_images:[JSON] = []
    var selectedItemsexisted = Array<[String:Any]>()
    var previousSelectedItemsexisted = Array<[String:Any]>()

//    var selectedItems = [YPMediaItem]()
         var selectedAttachments = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
              scrollView.delegate = self
              self.collViewOfAttachments.delegate = self
              self.collViewOfAttachments.dataSource = self
        self.txtDescription.text = self.history_text
        print(history_images)

        if history_images.count != 0
        {
            for itemsRespone in history_images
            {
                var dict = [String:Any]()
                dict["existing"] = true
                var dict2 = [String:String]()
                                  dict2["filename"] = itemsRespone["filename"].stringValue
                                  dict2["type"] = itemsRespone["type"].stringValue
                                  print(dict)
                dict["value"] = dict2
                self.selectedAttachments.add(dict2)
                self.selectedItemsexisted.append(dict)
                
            }
         
            
            print(selectedAttachments)
            print(selectedItemsexisted)

            
            self.collViewOfAttachments.isHidden = false
                    self.lblNoFiles.isHidden = true
            self.collViewOfAttachments.reloadData()
        }
        else
        {
            self.selectedAttachments = []
                          self.selectedItemsexisted = []
          self.collViewOfAttachments.isHidden = true
         self.lblNoFiles.isHidden = false
            
        }

        
        
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
   
        
//        self.viewInside.frame = CGRect(x: 0, y: 0, width: self.scrollView.frame.width, height: self.viewInside.frame.height + self.txtDescription.frame.height)
//              self.viewInside.updateConstraintsIfNeeded()
        self.scrollView.translatesAutoresizingMaskIntoConstraints = true
        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.width, height: 700 + self.txtDescription.frame.height);
        self.heightOfInsideView.constant = 700 + self.txtDescription.frame.height
        self.viewInside.updateConstraints()
    self.viewInside.layoutIfNeeded()

    }
     @IBAction func btnBack_Onclick(_ sender: Any) {
            
            self.navigationController?.popViewController(animated: true)
        }
        @IBAction func btnAttachFile_OnClick(_ sender: Any) {
            
            var config = YPImagePickerConfiguration()
            config.library.onlySquare = false
            config.onlySquareImagesFromCamera = true
            config.targetImageSize = .original
            config.usesFrontCamera = true
            config.showsPhotoFilters = true
            config.shouldSaveNewPicturesToAlbum = true
            config.video.compression = AVAssetExportPresetHighestQuality
            
            config.library.maxNumberOfItems = 25
            config.library.minNumberOfItems = 1
            config.library.defaultMultipleSelection = true
            config.library.isSquareByDefault = false


            config.screens = [.library, .photo]

            config.startOnScreen = .library
            config.video.recordingTimeLimit = 600
            config.video.libraryTimeLimit = 3600
            config.showsVideoTrimmer = false
            config.wordings.libraryTitle = "Gallery"
            config.hidesStatusBar = false
            
            // Build a picker with your configuration
            let picker = YPImagePicker(configuration: config)

           
            
            /* Multiple media implementation */
            picker.didFinishPicking { [unowned picker] items, cancelled in
                
                if cancelled {
                    print("Picker was canceled")
                    picker.dismiss(animated: true, completion: nil)
                    return
                }
    //            print(items)
                _ = items.map { print("🧀 \($0)") }
                
    //            self.selectedItems = items
//                self.selectedItems.append(contentsOf: items)
                
                
                
                
                self.imgDataArr = []
               self.videoDataArr = []

                for item in items {
                    switch item {
                    case .photo(let photo):
                        print(photo)
                        
                        let img2 = photo.image.resized(withPercentage: 0.75)
                        
                        let imgData = (img2?.pngData())!
                        
                        self.imgDataArr.append(imgData)
                        
                    case .video(let video):
                        print(video)
                        let videoUrl = video.url
                         do {
                         let videoData = try NSData(contentsOf: videoUrl, options: NSData.ReadingOptions())
                            self.videoDataArr.append(videoData as Data)

                            
                        
                    } catch {
                        print(error)
                    }
                    }
                    
                }
                
                
                print("image ar : \(self.imgDataArr)")
                if self.imgDataArr.count != 0
                {
                    self.imageUploadStatus = false

                    self.uploadImageMultipleImages(arr: self.imgDataArr)

                }
                if self.videoDataArr.count != 0
                {
                    self.vedioUploadStatus = false

                    self.uploadVideoMultiple(arr: self.videoDataArr)
                    
                }

                self.previousSelectedItemsexisted = self.selectedItemsexisted

                
                for item in items
                                    
                {
                                              var dict = [String:Any]()
                                              dict["existing"] = false
                                              dict["value"] = item
                                              self.selectedItemsexisted.append(dict)
                                              
                                          }
                                          
                                       print(self.selectedItemsexisted)
                                       print(self.selectedItemsexisted.count)
                
                
                
                if self.selectedItemsexisted.count != 0
                              {
                              self.collViewOfAttachments.isHidden = false
                              self.lblNoFiles.isHidden = true
                                  self.collViewOfAttachments.reloadData()

                              }
                              else
                              {
                                  self.collViewOfAttachments.isHidden = true
                                            self.lblNoFiles.isHidden = false
                                  
                              }
                
                
//                if self.history_images.count != 0
//                       {
//                           for item in items
//                           {
//                               var dict = [String:Any]()
//                               dict["existing"] = false
//                               dict["value"] = item
//                               self.selectedItemsexisted.append(dict)
//
//                           }
//
//                        print(self.selectedItemsexisted)
//                        print(self.selectedItemsexisted.count)
//
//
//                           self.collViewOfAttachments.isHidden = false
//                                   self.lblNoFiles.isHidden = true
//                           self.collViewOfAttachments.reloadData()
//                       }
//                else
//                {
//                if self.selectedItems.count != 0
//                {
//                self.collViewOfAttachments.isHidden = false
//                self.lblNoFiles.isHidden = true
//                    self.collViewOfAttachments.reloadData()
//
//                }
//                else
//                {
//                    self.collViewOfAttachments.isHidden = true
//                              self.lblNoFiles.isHidden = false
//
//                }
//                }
                picker.dismiss(animated: true, completion: nil)
                

            }
            present(picker, animated: true, completion: nil)
            
         
            
            
        }
        
        //MARK: - Multiple Image Uploading
        
        func uploadImageMultipleImages(arr : [Data]){
            
            let param = ["name" :"history_images"]
            
            
          
          
    //        ActivityIndicatorView.show("Uploading...")
            Helpers.requestWithMultipleImages(fileParamName: "file", urlAppendString: "upload_file_tos3", imageData: arr, parameters: param, mimeType: "image/png") { (response) in
                print("Response : \(response)")
                self.imageUploadStatus = true
                
                if response != JSON.null
                {
                self.collViewOfAttachments.reloadData()
                let arrayOfData = response["data"].arrayValue
                
                for currentdata in arrayOfData{
                    var dict = [String:String]()
                    dict["filename"] = currentdata["filename"].stringValue
                    dict["type"] = currentdata["type"].stringValue
                    print(dict)
                    
                    self.selectedAttachments.add(dict)
                    }
                    

//                    if self.history_images.count != 0
//                                                        {
//
//                                                           self.selectedAttachments.add(dict)
//
////                                             self.history_images.append(dict as! JSONDictionary)
//
//                                   }
//                    else
//                    {
//
//                    self.selectedAttachments.add(dict)
//                    }
                    
                }
                
                else
                {
                    Helpers.showAlertDialog(message: "Error in image uploading ,Please try again", target: self)
                    self.selectedItemsexisted = self.previousSelectedItemsexisted
                    self.collViewOfAttachments.reloadData()

                    

                }
              
                
                print(self.selectedAttachments)
                
                
               
    //            ActivityIndicatorView.hiding()

                
                
            }
        }
        
        func uploadVideoMultiple(arr : [Data]){
            
            let param = ["name" :"history_images"]
            
            
            //        ActivityIndicatorView.show("Uploading...")
            Helpers.requestWithMultipleImages(fileParamName: "file", urlAppendString: "upload_file_tos3", imageData: arr, parameters: param, mimeType: "video/quicktime/m4v") { (response) in
                print("Response : \(response)")
                self.vedioUploadStatus = true
                self.collViewOfAttachments.reloadData()
                
                let arrayOfData = response["data"]as! [JSON]
                
                for currentdata in arrayOfData{
                    var dict = [String:String]()
                    dict["filename"] = currentdata["filename"].stringValue
                    dict["type"] = currentdata["type"].stringValue
                    print(dict)
                    self.selectedAttachments.add(dict)

                    
//                          if self.history_images.count != 0
//                                                                            {
//
//                                                                               self.selectedAttachments.add(dict)
//
//                    //                                             self.history_images.append(dict as! JSONDictionary)
//
//                                                       }
//                    else
//                          {
//
//                    self.selectedAttachments.add(dict)
//                    }
//
                }
                
                
                
                print(self.selectedAttachments)
                //            ActivityIndicatorView.hiding()
                
                
                
            }
        }
      
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

     @IBAction func btnPost_OnClick(_ sender: Any) {
            
            if DatavalidationForSubmit()
            {
                self.apiForFamilyUpdate()

            }
            
        }
        
        
        func apiForFamilyUpdate() {

            
            var parameter = [String:Any]()
      
            parameter = ["id":groupId,"history_text":self.txtDescription.text!,
                         "history_images":self.selectedAttachments, "user_id":UserDefaults.standard.value(forKey: "userId") as! String
                            ] as [String : Any]
    
            print(parameter)
            ActivityIndicatorView.show("Loading....")
            networkProvider.request(.updateFamilyWithHistory(parameter: parameter), completion: {(result) in
                
                switch result {
                    
                case .success(let response):
                    
                    do {
                        
                        let jsonData = JSON(response.data as Any)
                        print(jsonData)
                        print(jsonData["status"])
                        print(response.statusCode)
                        if response.statusCode == 200
                        {
                            //
                            let AlertCus = CustomAlertVC.show(in: self, title: "Success", message: "Successfully Updated Family", image:UIImage.init(named: "Green_tick")!)
                            AlertCus.addAction(CPAlertAction.init(title: "OK", type: .normal, handler: {
                                //                                let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                                //                                let vc = storyboard.instantiateViewController(withIdentifier: "mainTabView") as! HomeViewTabController
                                //                                self.navigationController?.pushViewController(vc, animated: true)
                                
                                //                                func updateAboutHistory(data: String, firstImg: UIImage) {
                                
                                let history_text:String = jsonData["data"][0]["history_text"].stringValue
                                var history_images:[JSON] = []
                                let jsonvalue = jsonData["data"][0]["history_images"].arrayValue
                                
                                print(jsonvalue)
                                history_images = jsonvalue
                                
                                
                                
                                self.delegate?.updateAboutHistory(history_text:history_text,history_images:history_images)
                                self.navigationController?.popViewController(animated: true)
                                
                            }))
                            
                            
                            
                        }
                        else if response.statusCode == 401 {
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                self.apiForFamilyUpdate()
                                
                            }
                        }
                        else
                        {
                            
                            print(jsonData["code"])
                            Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                        }
                        
                        
                    } catch let err {
                        
                        Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                    }
                    
                case .failure(let error):
                    
                    Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                }
                ActivityIndicatorView.hiding()
            })
        }
        
        //MARK:- VALIDATION
        func DatavalidationForSubmit()->Bool{
            
         
            if self.imgDataArr.count != 0  && !imageUploadStatus
            {
                
                               Helpers.showAlertDialog(message: "Uploading images Please Wait a moment.", target: self)
                               return false

       
                
            }
            else if self.videoDataArr.count != 0 && !vedioUploadStatus{
            
                Helpers.showAlertDialog(message: "Uploading video Please Wait a moment.", target: self)

                return false

            }
            else {
                
                return true
                
            }
        }
        /*
        // MARK: - Navigation

        // In a storyboard-based application, you will often want to do a little preparation before navigation
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            // Get the new view controller using segue.destination.
            // Pass the selected object to the new view controller.
        }
        */
   
        
        
        
        
        //MARK:- collectionView
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//            if history_images.count != 0
//                   {
//                    return self.selectedItemsexisted.count
//
//            }else
//            {
//                return self.selectedItems.count
//
//            }
            
            return self.selectedItemsexisted.count

            
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
                 let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PostImageCollectionViewCell
            
            
            
                            let item = self.selectedItemsexisted[indexPath.row]
                            if item["existing"]as! Bool == true
                            {
                                let dict = item["value"] as! [String:String]
                                let history_pic = dict["filename"]!

                                                        if history_pic != ""
                                                        {
                                                            let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.history_images)"+history_pic
                                                            let imgUrl = URL(string: temp)
                                                            cell.imgOfPostItems.kf.indicatorType  = .activity
                                                            cell.imgOfPlayVideo.isHidden = true
                                                            cell.viewOfVideoBg.isHidden = true


                                        
                                                            cell.imgOfPostItems.kf.setImage(with: imgUrl, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
                                                            cell.imgOfProgress.image = #imageLiteral(resourceName: "completed_progress.png")
                                                            cell.activityIndicator.isHidden = true
                              
                                                        }
                            }
            else
                            {
                                  let currentItem = item["value"] as! YPMediaItem
                                    //         for item in self.selectedItems{
                                                                switch currentItem {
                                                                case .photo(let photo):
                                //                                    self.selectedImageV.image = photo.image
                                                                    cell.imgOfPostItems.image = photo.image
                                                                    cell.imgOfPlayVideo.isHidden = true
                                                                    cell.viewOfVideoBg.isHidden = true

                                                                    if !imageUploadStatus
                                                                    {
                                                                        cell.imgOfProgress.image = #imageLiteral(resourceName: "ongoing_progress.png")
                                                                        cell.activityIndicator.isHidden = false
                                                                        
                                                                    }
                                                                    else
                                                                    {
                                                                        cell.imgOfProgress.image = #imageLiteral(resourceName: "completed_progress.png")
                                                                        cell.activityIndicator.isHidden = true

                                                                    }
                                    //                                self.uploadImage(img: photo.image)
                                                
                                                                case .video(let video):
                                //                                    self.selectedImageV.image = video.thumbnail
                                                                    cell.imgOfPostItems.image = video.thumbnail
                                                                    cell.imgOfPlayVideo.isHidden = false
                                                                    cell.viewOfVideoBg.isHidden = false

                                                                    if !vedioUploadStatus
                                                                    {
                                                                        cell.imgOfProgress.image = #imageLiteral(resourceName: "ongoing_progress.png")
                                                                        cell.activityIndicator.isHidden = false


                                                                    }
                                                                    else
                                                                    {
                                                                        cell.imgOfProgress.image = #imageLiteral(resourceName: "completed_progress.png")
                                                                        cell.activityIndicator.isHidden = true

                                                                    }
                                                                    cell.btnClose.tag = indexPath.row
                                    //                                cell.btnClose.addTarget(self, action: #selector(btnCloseImage_OnClik(sender:)), for: .touchUpInside)
                                                                    
                                                
                                                //                    let assetURL = video.url
                                                //                    let playerVC = AVPlayerViewController()
                                                //                    let player = AVPlayer(playerItem: AVPlayerItem(url:assetURL))
                                                //                    playerVC.player = player
                                                ////
                                                //                    picker.dismiss(animated: true, completion: { [weak self] in
                                                //                        self?.present(playerVC, animated: true, completion: nil)
                                                //                        print("😀 \(String(describing: self?.resolutionForLocalVideo(url: assetURL)!))")
                                                //                    })
                                    //                            }
                                                            }
                            }

            
            
            
            
            
            
            
            
//            if history_images.count != 0
//            {
//                let item = self.selectedItemsexisted[indexPath.row]
//                if item["existing"]as! Bool == true
//                {
//                    let dict = item["value"] as! [String:String]
//                    let history_pic = dict["filename"]!
//
//                                            if history_pic != ""
//                                            {
//                                                let temp = "\(Helpers.imageURl)"+"\(BaseUrl.history_images)"+history_pic
//                                                let imgUrl = URL(string: temp)
//                                                cell.imgOfPostItems.kf.indicatorType  = .activity
//                                                cell.imgOfPlayVideo.isHidden = true
//                                                cell.viewOfVideoBg.isHidden = true
//
//
//
//                                                cell.imgOfPostItems.kf.setImage(with: imgUrl, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
//                                                cell.imgOfProgress.image = #imageLiteral(resourceName: "completed_progress.png")
//                                                cell.activityIndicator.isHidden = true
//
//                                            }
//                }
//else
//                {
//                      let currentItem = item["value"] as! YPMediaItem
//                        //         for item in self.selectedItems{
//                                                    switch currentItem {
//                                                    case .photo(let photo):
//                    //                                    self.selectedImageV.image = photo.image
//                                                        cell.imgOfPostItems.image = photo.image
//                                                        cell.imgOfPlayVideo.isHidden = true
//                                                        cell.viewOfVideoBg.isHidden = true
//
//                                                        if !imageUploadStatus
//                                                        {
//                                                            cell.imgOfProgress.image = #imageLiteral(resourceName: "ongoing_progress.png")
//                                                            cell.activityIndicator.isHidden = false
//
//                                                        }
//                                                        else
//                                                        {
//                                                            cell.imgOfProgress.image = #imageLiteral(resourceName: "completed_progress.png")
//                                                            cell.activityIndicator.isHidden = true
//
//                                                        }
//                        //                                self.uploadImage(img: photo.image)
//
//                                                    case .video(let video):
//                    //                                    self.selectedImageV.image = video.thumbnail
//                                                        cell.imgOfPostItems.image = video.thumbnail
//                                                        cell.imgOfPlayVideo.isHidden = false
//                                                        cell.viewOfVideoBg.isHidden = false
//
//                                                        if !vedioUploadStatus
//                                                        {
//                                                            cell.imgOfProgress.image = #imageLiteral(resourceName: "ongoing_progress.png")
//                                                            cell.activityIndicator.isHidden = false
//
//
//                                                        }
//                                                        else
//                                                        {
//                                                            cell.imgOfProgress.image = #imageLiteral(resourceName: "completed_progress.png")
//                                                            cell.activityIndicator.isHidden = true
//
//                                                        }
//                                                        cell.btnClose.tag = indexPath.row
//                        //                                cell.btnClose.addTarget(self, action: #selector(btnCloseImage_OnClik(sender:)), for: .touchUpInside)
//
//
//                                    //                    let assetURL = video.url
//                                    //                    let playerVC = AVPlayerViewController()
//                                    //                    let player = AVPlayer(playerItem: AVPlayerItem(url:assetURL))
//                                    //                    playerVC.player = player
//                                    ////
//                                    //                    picker.dismiss(animated: true, completion: { [weak self] in
//                                    //                        self?.present(playerVC, animated: true, completion: nil)
//                                    //                        print("😀 \(String(describing: self?.resolutionForLocalVideo(url: assetURL)!))")
//                                    //                    })
//                        //                            }
//                                                }
//                }
//            }
//            else
//            {
//                  let item = self.selectedItems[indexPath.row]
//                    //         for item in self.selectedItems{
//                                                switch item {
//                                                case .photo(let photo):
//                //                                    self.selectedImageV.image = photo.image
//                                                    cell.imgOfPostItems.image = photo.image
//                                                    cell.imgOfPlayVideo.isHidden = true
//                                                    if !imageUploadStatus
//                                                    {
//                                                        cell.imgOfProgress.image = #imageLiteral(resourceName: "ongoing_progress.png")
//                                                        cell.activityIndicator.isHidden = false
//
//                                                    }
//                                                    else
//                                                    {
//                                                        cell.imgOfProgress.image = #imageLiteral(resourceName: "completed_progress.png")
//                                                        cell.activityIndicator.isHidden = true
//
//                                                    }
//                    //                                self.uploadImage(img: photo.image)
//
//                                                case .video(let video):
//                //                                    self.selectedImageV.image = video.thumbnail
//                                                    cell.imgOfPostItems.image = video.thumbnail
//                                                    cell.imgOfPlayVideo.isHidden = false
//                                                    if !vedioUploadStatus
//                                                    {
//                                                        cell.imgOfProgress.image = #imageLiteral(resourceName: "ongoing_progress.png")
//                                                        cell.activityIndicator.isHidden = false
//
//
//                                                    }
//                                                    else
//                                                    {
//                                                        cell.imgOfProgress.image = #imageLiteral(resourceName: "completed_progress.png")
//                                                        cell.activityIndicator.isHidden = true
//
//                                                    }
//                                                    cell.btnClose.tag = indexPath.row
//                    //                                cell.btnClose.addTarget(self, action: #selector(btnCloseImage_OnClik(sender:)), for: .touchUpInside)
//
//
//                                //                    let assetURL = video.url
//                                //                    let playerVC = AVPlayerViewController()
//                                //                    let player = AVPlayer(playerItem: AVPlayerItem(url:assetURL))
//                                //                    playerVC.player = player
//                                ////
//                                //                    picker.dismiss(animated: true, completion: { [weak self] in
//                                //                        self?.present(playerVC, animated: true, completion: nil)
//                                //                        print("😀 \(String(describing: self?.resolutionForLocalVideo(url: assetURL)!))")
//                                //                    })
//                    //                            }
//                                            }
//            }
       
           
            
            return cell
        }
        
        
        @IBAction func btnCloseImage_Onclick(_ sender: UIButton) {
            
            if self.imgDataArr.count != 0  && !imageUploadStatus
                               {
                                   
                                                  Helpers.showAlertDialog(message: "Uploading images Please Wait a moment.", target: self)

                          
                                   
                               }
                               else if self.videoDataArr.count != 0 && !vedioUploadStatus{
                               
                                   Helpers.showAlertDialog(message: "Uploading video Please Wait a moment.", target: self)


                               }
                         else
                         {
            self.selectedItemsexisted.remove(at: sender.tag)
            self.selectedAttachments.removeObject(at: sender.tag)
            if self.selectedItemsexisted.count != 0
                                        {
                                        self.collViewOfAttachments.isHidden = false
                                        self.lblNoFiles.isHidden = true
                                            self.collViewOfAttachments.reloadData()

                                        }
                                        else
                                        {
                                            self.collViewOfAttachments.isHidden = true
                                                      self.lblNoFiles.isHidden = false
            }
            }
                 
//           if history_images.count != 0
//                    {
//                        self.selectedItemsexisted.remove(at: sender.tag)
//                        self.selectedAttachments.removeObject(at: sender.tag)
//                        if self.selectedItemsexisted.count != 0
//                                                    {
//                                                    self.collViewOfAttachments.isHidden = false
//                                                    self.lblNoFiles.isHidden = true
//                                                        self.collViewOfAttachments.reloadData()
//
//                                                    }
//                                                    else
//                                                    {
//                                                        self.collViewOfAttachments.isHidden = true
//                                                                  self.lblNoFiles.isHidden = false
//                        }
//
//
//            }
//                        else {
//                self.selectedItems.remove(at: sender.tag)
//                    self.selectedAttachments.removeObject(at: sender.tag)
//                    if self.selectedItems.count != 0
//                              {
//                              self.collViewOfAttachments.isHidden = false
//                              self.lblNoFiles.isHidden = true
//                                  self.collViewOfAttachments.reloadData()
//
//                              }
//                              else
//                              {
//                                  self.collViewOfAttachments.isHidden = true
//                                            self.lblNoFiles.isHidden = false
//
//                              }
//                        }

                
               
        }
        
//        @objc func btnCloseImage_OnClik(sender:UIButton)
//        {
//
//    //        if self.imgDataArr.count != 0 || self.videoDataArr.count != 0
//    //        {
//    //         if !imageUploadStatus
//    //        }
//
//            self.selectedItems.remove(at: sender.tag)
//            self.selectedAttachments.removeObject(at: sender.tag)
//            if self.selectedItems.count != 0
//                      {
//                      self.collViewOfAttachments.isHidden = false
//                      self.lblNoFiles.isHidden = true
//                          self.collViewOfAttachments.reloadData()
//
//                      }
//                      else
//                      {
//                          self.collViewOfAttachments.isHidden = true
//                                    self.lblNoFiles.isHidden = false
//
//                      }
//    //        self.collViewOfAttachments.reloadData()
//            // print("current Agenda: \(currentAgenda)")
//
//
//        }
    //    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    //        let item = self.selectedItems[indexPath.row]
    //
    //
    //
    //    }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            
    //        let numberOfCellInRow                   = 3
    //        let padding : Int                       = 2
    //        let collectionCellWidth : CGFloat       = (self.collViewOfAttachments.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
    //        return CGSize(width: collectionCellWidth, height: collectionCellWidth + 20)
            
            return CGSize.init(width: 90, height:  90)
        }
        

    }

    // Support methods
    extension FamilyEditHistoryViewController {
        /* Gives a resolution for the video by URL */
        func resolutionForLocalVideo(url: URL) -> CGSize? {
            guard let track = AVURLAsset(url: url).tracks(withMediaType: AVMediaType.video).first else { return nil }
            let size = track.naturalSize.applying(track.preferredTransform)
            return CGSize(width: abs(size.width), height: abs(size.height))
        }
    }
