//
//  FamilyUrlTableViewCell.swift
//  familheey
//
//  Created by familheey on 12/12/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class FamilyUrlTableViewCell: UITableViewCell {
    @IBOutlet weak var EditVw: UIView!
    @IBOutlet weak var txtFamUrl: UITextField!
    @IBOutlet weak var imgShare: UIImageView!
    @IBOutlet weak var btnShare: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
