//
//  ChangePicturesViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 31/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Kingfisher
import SwiftyJSON
import Moya
import CropViewController
import Photos

protocol changeProPicDelegate: class {
    func changeProPic()
}

class ChangePicturesViewController: UIViewController, CropViewControllerDelegate, UIGestureRecognizerDelegate {
    var pastevents:Bool = false
    var arrayOfOptions = [String]()
    
    private var networkProvider              = MoyaProvider<FamilyheeyApi>()
    @IBOutlet weak var imgVw                 : UIImageView!
    @IBOutlet var changeButt                 : UIButton!
    
    @IBOutlet weak var viewOfChangeOption: UIView!
    
    weak var delegate                        : changeProPicDelegate?
    
    var imgDetails                           = JSON()
    
    var isFrom                               = ""
    var isCoverOrProfilePic                  = ""
    var ids                                  = ""
    
    var imageUrl                             = ""
    var inFor                                = ""
    var isOwn                                = ""
    var isAdmin                              = ""
    var groupId                              = ""
    var eventId                              = ""
    var selectedImg                          = UIImage()
    var pinchGesture = UIPinchGestureRecognizer()
    var originalCover:UIImage!
    var orignalPicData = NSData()
    var originalData = NSData()
    
    @IBOutlet weak var scrollView : UIScrollView!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var btnUploadImage: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        changeButt.underline()
        
        print("img url : \(imageUrl)")
        
        //        self.imgVw.image = #imageLiteral(resourceName: "imgNoImage_landscape")
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            
            if self.isFrom.lowercased() == "group"{
                
                if self.isCoverOrProfilePic == "logo"{
                    
                    self.imgVw.kf.indicatorType = .activity
                    self.imgVw.kf.setImage(with: URL(string: self.imageUrl), placeholder: #imageLiteral(resourceName: "imgNoImage_landscape") , options: nil, progressBlock: nil, completionHandler: nil)
                    self.arrayOfOptions = ["Change Image"]
                    if self.imageUrl != ""{
                        let url = URL(string:self.imageUrl)
                        if let data = try? Data(contentsOf:url!)
                        {
                            let image: UIImage = UIImage(data: data)!
                            self.originalCover = image
                            self.imgVw.isHidden = false
                            if self.isAdmin.lowercased() == "admin"{
                                self.viewOfChangeOption.isHidden = false
                                self.btnUploadImage.isHidden = true
                            }
                            else{
                                self.viewOfChangeOption.isHidden = true
                                self.btnUploadImage.isHidden = true
                            }
                            
                        }
                        else{
                            self.imgVw.isHidden = true
                            if self.isAdmin.lowercased() == "admin"{
                                self.viewOfChangeOption.isHidden = true
                                self.btnUploadImage.isHidden = false
                            }
                            else{
                                self.viewOfChangeOption.isHidden = true
                                self.btnUploadImage.isHidden = true
                            }
                        }
                        
                    }
                    else
                    {
                        self.originalCover = #imageLiteral(resourceName: "Family Logo")
                        self.imgVw.isHidden = true
                        if self.isAdmin.lowercased() == "admin"{
                            self.viewOfChangeOption.isHidden = true
                            self.btnUploadImage.isHidden = false
                        }
                        else{
                            self.viewOfChangeOption.isHidden = true
                            self.btnUploadImage.isHidden = true
                        }
                    }
                }else{
                    self.imgVw.kf.indicatorType = .activity
                    self.imgVw.kf.setImage(with: URL(string: self.imageUrl), placeholder:#imageLiteral(resourceName: "imgNoImage_landscape"), options: nil, progressBlock: nil, completionHandler: nil)
                    self.arrayOfOptions = ["Change Image","Change Crop Position"]
                    
                    if self.imageUrl != ""{
                        let url = URL(string:self.imageUrl)
                        if let data = try? Data(contentsOf:url!)
                        {
                            let image: UIImage = UIImage(data: data)!
                            self.originalCover = image
                            self.imgVw.isHidden = false
                            if self.isAdmin.lowercased() == "admin"{
                                self.viewOfChangeOption.isHidden = false
                                self.btnUploadImage.isHidden = true
                            }
                            else{
                                self.viewOfChangeOption.isHidden = true
                                self.btnUploadImage.isHidden = true
                            }
                        }
                        else{
                            self.imgVw.isHidden = true
                            if self.isAdmin.lowercased() == "admin"{
                                self.viewOfChangeOption.isHidden = true
                                self.btnUploadImage.isHidden = false
                            }
                            else{
                                self.viewOfChangeOption.isHidden = true
                                self.btnUploadImage.isHidden = true
                            }
                        }
                    }
                    else
                    {
                        self.originalCover = #imageLiteral(resourceName: "default_event_image.jpg")
                        self.imgVw.isHidden = true
                        if self.isAdmin.lowercased() == "admin"{
                            self.viewOfChangeOption.isHidden = true
                            self.btnUploadImage.isHidden = false
                        }
                        else{
                            self.viewOfChangeOption.isHidden = true
                            self.btnUploadImage.isHidden = true
                        }
                    }
                }
                
                
            }
            else if self.isFrom.lowercased() == "events"{
                self.imgVw.kf.indicatorType = .activity
                self.imgVw.kf.setImage(with: URL(string: self.imageUrl), placeholder: #imageLiteral(resourceName: "default_event_image.jpg"), options: nil, progressBlock: nil, completionHandler: nil)
                self.arrayOfOptions = ["Change Image","Change Crop Position"]
                if self.imageUrl != ""
                {
                    let url = URL(string:self.imageUrl)
                    if let data = try? Data(contentsOf:url!)
                    {
                        let image: UIImage = UIImage(data: data)!
                        self.originalCover = image
                        self.imgVw.isHidden = false
                        if self.isAdmin.lowercased() == "admin"{
                            self.viewOfChangeOption.isHidden = false
                            self.btnUploadImage.isHidden = true
                        }
                        else{
                            self.viewOfChangeOption.isHidden = true
                            self.btnUploadImage.isHidden = true
                        }
                    }
                    else{
                        self.imgVw.isHidden = true
                        if self.isAdmin.lowercased() == "admin"{
                            self.viewOfChangeOption.isHidden = true
                            self.btnUploadImage.isHidden = false
                        }
                        else{
                            self.viewOfChangeOption.isHidden = true
                            self.btnUploadImage.isHidden = true
                        }
                    }
                }
                else
                {
                    self.originalCover = #imageLiteral(resourceName: "default_event_image.jpg")
                    self.imgVw.isHidden = true
                    if self.isAdmin.lowercased() == "admin"{
                        self.viewOfChangeOption.isHidden = true
                        self.btnUploadImage.isHidden = false
                    }
                    else{
                        self.viewOfChangeOption.isHidden = true
                        self.btnUploadImage.isHidden = true
                    }
                }
                
            }
            else{
                
                if self.isCoverOrProfilePic.lowercased() == "profilepic"{
                    self.imgVw.kf.indicatorType = .activity
                    self.imgVw.kf.setImage(with: URL(string: self.imageUrl), placeholder:  #imageLiteral(resourceName: "default_event_image.jpg"), options: nil, progressBlock: nil, completionHandler: nil)
                    self.arrayOfOptions = ["Change Image"]
                    if self.imageUrl != ""
                    {
                        let url = URL(string:self.imageUrl)
                        if let data = try? Data(contentsOf:url!)
                        {
                            let image: UIImage = UIImage(data: data)!
                            self.originalCover = image
                            
                            self.imgVw.isHidden = false
                            if self.isAdmin.lowercased() == "admin"{
                                self.viewOfChangeOption.isHidden = false
                                self.btnUploadImage.isHidden = true
                            }
                            else{
                                self.viewOfChangeOption.isHidden = true
                                self.btnUploadImage.isHidden = true
                            }
                        }
                        else{
                            self.imgVw.isHidden = true
                            if self.isAdmin.lowercased() == "admin"{
                                self.viewOfChangeOption.isHidden = true
                                self.btnUploadImage.isHidden = false
                            }
                            else{
                                self.viewOfChangeOption.isHidden = true
                                self.btnUploadImage.isHidden = true
                            }
                        }
                    }
                    else
                    {
                        self.originalCover = #imageLiteral(resourceName: "Male Colored")
                        self.imgVw.isHidden = true
                        if self.isAdmin.lowercased() == "admin"{
                            self.viewOfChangeOption.isHidden = true
                            self.btnUploadImage.isHidden = false
                        }
                        else{
                            self.viewOfChangeOption.isHidden = true
                            self.btnUploadImage.isHidden = true
                        }
                    }
                    
                }else{
                    self.imgVw.kf.indicatorType = .activity
                    self.imgVw.kf.setImage(with: URL(string: self.imageUrl), placeholder: #imageLiteral(resourceName: "imgNoImage_landscape"), options: nil, progressBlock: nil, completionHandler: nil)
                    self.arrayOfOptions = ["Change Image","Change Image Preview"]
                    if self.imageUrl != ""
                    {
                        let url = URL(string:self.imageUrl)
                        if let data = try? Data(contentsOf:url!)
                        {
                            let image: UIImage = UIImage(data: data)!
                            self.originalCover = image
                            
                            self.imgVw.isHidden = false
                            if self.isAdmin.lowercased() == "admin"{
                                self.viewOfChangeOption.isHidden = false
                                self.btnUploadImage.isHidden = true
                            }
                            else{
                                self.viewOfChangeOption.isHidden = true
                                self.btnUploadImage.isHidden = true
                            }
                        }
                        else{
                            self.imgVw.isHidden = true
                            if self.isAdmin.lowercased() == "admin"{
                                self.viewOfChangeOption.isHidden = true
                                self.btnUploadImage.isHidden = false
                            }
                            else{
                                self.viewOfChangeOption.isHidden = true
                                self.btnUploadImage.isHidden = true
                            }
                        }
                    }
                    else
                    {
                        self.originalCover = #imageLiteral(resourceName: "default_event_image.jpg")
                        self.imgVw.isHidden = true
                        if self.isAdmin.lowercased() == "admin"{
                            self.viewOfChangeOption.isHidden = true
                            self.btnUploadImage.isHidden = false
                        }
                        else{
                            self.viewOfChangeOption.isHidden = true
                            self.btnUploadImage.isHidden = true
                        }
                    }
                }
                
            }
            
            
//        })
        
        
        if isFrom.lowercased() == "user"{
            if isAdmin.lowercased() == "admin"{
                //                changeButt.isHidden = false
                viewOfChangeOption.isHidden = false
            }
            else{
                //                changeButt.isHidden = true
                viewOfChangeOption.isHidden = true
            }
        }
        else if isFrom.lowercased() == "events"{
            if isAdmin.lowercased() == "admin"{
                
                if !self.pastevents{
                    viewOfChangeOption.isHidden = false
                }
                else{
                    viewOfChangeOption.isHidden = true
                    
                }
                //                changeButt.isHidden = false
            }
            else{
                //                changeButt.isHidden = true
                viewOfChangeOption.isHidden = true
            }
        }
        else {
            if isAdmin.lowercased() == "admin"{
                //                changeButt.isHidden = false
                viewOfChangeOption.isHidden = false
            }
            else{
                //                changeButt.isHidden = true
                viewOfChangeOption.isHidden = true
            }
        }
        let minScale = scrollView.frame.size.width / imgVw.frame.size.width;
        scrollView.minimumZoomScale = minScale
        scrollView.maximumZoomScale = 3.0
        scrollView.contentSize = imgVw.frame.size
        scrollView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        //        self.tabBarController?.tabBar.isHidden = false
    }
    
    //    @IBAction func optionClicked(_ sender: Any) {
    //
    //        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
    //        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
    //            self.openCamera()
    //        }))
    //
    //        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
    //            self.openGallary()
    //        }))
    //
    //
    //        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
    //        self.present(alert, animated: true, completion: nil)
    //    }
    
    
    @IBAction func btnChangeDefaultImage(_ sender: Any) {
        
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func optionClicked(_ sender: Any) {
        
        showActionSheet(titleArr: arrayOfOptions as NSArray, title: "Select options") { (index) in
            if index == 0{
                let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
                alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
                    self.openCamera()
                }))
                
                alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                    self.openGallary()
                }))
                
                
                alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)            }
            else if index == 1{
                self.selectedImage(img: self.originalCover)
                
            }
            else{
                
            }
        }
    }
    
    //MARK:- Custom Methods
    
    var imagePicker   = UIImagePickerController()
    
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else{
            // self.displayAlert(alertStr: "You don't have camera", title: "")
        }
    }
    
    func openGallary(){
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func selectedImage(img:UIImage){
        let cropView = CropViewController(croppingStyle: .default, image: img)
        cropView.aspectRatioLockEnabled = false
        cropView.aspectRatioLockDimensionSwapEnabled = false
        cropView.aspectRatioPickerButtonHidden = true
        if isFrom.lowercased() == "events"{
            cropView.aspectRatioPreset = TOCropViewControllerAspectRatioPreset.preset4x3
            self.originalCover = img
            originalData = img.jpegData(compressionQuality: 1.0)! as NSData
        }
        else if isFrom.lowercased() == "group"{
            if isCoverOrProfilePic == "logo"{
                cropView.aspectRatioPreset = TOCropViewControllerAspectRatioPreset.presetSquare
            }
            else{
                cropView.aspectRatioPreset = TOCropViewControllerAspectRatioPreset.preset4x3
                self.orignalPicData = img.jpegData(compressionQuality: 1.0)! as NSData
            }
        }
        else{
            if isCoverOrProfilePic.lowercased() == "profile"{
                cropView.aspectRatioPreset = TOCropViewControllerAspectRatioPreset.presetSquare
            }
            else{
                cropView.aspectRatioPreset = TOCropViewControllerAspectRatioPreset.preset4x3
                self.orignalPicData = img.jpegData(compressionQuality: 1.0)! as NSData
            }
        }
        cropView.delegate = self
        self.navigationController?.pushViewController(cropView, animated: true)
    }
    
    @objc private func pinchHandler(gesture: UIPinchGestureRecognizer) {
        if let view = gesture.view {
            
            switch gesture.state {
            case .changed:
                let pinchCenter = CGPoint(x: gesture.location(in: view).x - view.bounds.midX,
                                          y: gesture.location(in: view).y - view.bounds.midY)
                let transform = view.transform.translatedBy(x: pinchCenter.x, y: pinchCenter.y)
                    .scaledBy(x: gesture.scale, y: gesture.scale)
                    .translatedBy(x: -pinchCenter.x, y: -pinchCenter.y)
                view.transform = transform
                gesture.scale = 1
                
            default:
                return
            }
            
            
        }
    }
    
    //MARK:- CropView SDK Delegate
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        
        imgVw.image = image
        selectedImg = image
        if isFrom.lowercased() == "events"{
            self.uploadEventCover(img: selectedImg)
        }
        else{
            self.uploadPics(img: selectedImg)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- Upload Image
    func uploadPics(img:UIImage){
        var params = [String:String]()
        let imgData = img.jpegData(compressionQuality: 0.8)
        
        var url = ""
        if self.isFrom.lowercased() == "user"{
            url = EndPoint.profileEdit
            params = ["id":UserDefaults.standard.value(forKey: "userId") as! String]
        }
        else{
            url = EndPoint.updateFamily
            params = ["id":self.groupId,"user_id":UserDefaults.standard.value(forKey: "userId") as! String]
        }
        
        APIServiceManager.callServer.uploadCoverImages(url: url, params: params ,imgData: imgData! as NSData, originalPicData: orignalPicData, contantType: "Image", inFor: self.inFor, isFrom: self.isFrom, success: { (response) in
            // print(response)
            if self.isFrom.lowercased() == "user"{
                print(response!)
                guard let loginMdl = response as? userModel else{
                    return
                }
                if loginMdl.status == 200{
                    self.delegate?.changeProPic()
                    self.navigationController?.popViewController(animated: true)
                }
                else{
                    self.displayAlert(alertStr: "Something went wrong!", title: "")
                }
            }
            else{
                guard let loginMdl = response as? familyListResponse else{
                    return
                }
                if loginMdl.statusCode == 200{
                    self.delegate?.changeProPic()
                    self.navigationController?.popViewController(animated: true)
                }
                else{
                    self.displayAlert(alertStr: "Something went wrong!", title: "")
                }
            }
            
            ActivityIndicatorView.hiding()
            
        }) { (error) in
            ActivityIndicatorView.hiding()
            
        }
    }
    
    func uploadEventCover(img:UIImage){
        let imgData = img.jpegData(compressionQuality: 0.8)
        
        ActivityIndicatorView.show("Loading....")
        
        let parameter = ["user_id" : UserDefaults.standard.value(forKey: "userId") as! String, "id" : self.eventId]
        
        print(parameter)
        
        /* if originalCover != nil{
         //                let img2 = self.originalCover.resized(withPercentage: 0.5)
         // originalData = self.originalCover.jpegData(compressionQuality: 0.5)! as NSData
         //            originalData = img2?.pngData() as! NSData
         }
         else{
         originalData = NSData()
         
         }*/
        
        Helpers.requestWith(fileParamName: "event_image", originalCover: "event_original_image",urlAppendString: "update_events", imageData:imgData, originalImg: originalData as Data, parameters: parameter, mimeType: "image/png") { (response) in
            
            if response["data"]  != nil {
                let responseData = response["data"].dictionaryValue
                print(responseData)
                print(responseData["event_image"]?.stringValue as Any)
                self.navigationController?.popViewController(animated: true)
                
            }
            else{
                Helpers.showAlertDialog(message: "Error in image upload,Please try again", target: self)
            }
            
            //self.getEventDetailsById(eventID: self.eventId)
            
            
            ActivityIndicatorView.hiding()
        }
    }
    
    //MARK: - Make Cover Pic
    
    func makeCoverPic(){
        
        let parameter = ["album_id": imgDetails["folder_id"].stringValue, "cover_pic" : imgDetails["url"].stringValue]
        
        print("Param : \(parameter)")
        ActivityIndicatorView.show("Loading....")
        
        networkProvider.request(.make_pic_cover(parameter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    
                    if response.statusCode == 200{
                        
                        print("Json data : \(jsonData)")
                        
                        var array : [UIViewController] = (self.navigationController?.viewControllers)!
                        array.remove(at: array.count - 1)
                        array.remove(at: array.count - 1)
                        self.navigationController?.viewControllers = array
                        
                        self.backClicked(self)
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.makeCoverPic()
                            
                        }
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    
    @IBAction func backClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


extension  ChangePicturesViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker .dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let choosenImage = info[.originalImage] as! UIImage
        selectedImage(img: choosenImage)
        
        if let asset = info[UIImagePickerController.InfoKey.phAsset] as? PHAsset {
            if let fileName = (asset.value(forKey: "filename")) as? String {
                //Do your stuff here
                print(fileName)
            }
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
}

extension ChangePicturesViewController: UIScrollViewDelegate{
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imgVw
    }
    
}
