//
//  AboutFamilyCollectionViewCell.swift
//  familheey
//
//  Created by familheey on 04/02/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit

class AboutFamilyCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgUnderline: UIImageView!
    
}
