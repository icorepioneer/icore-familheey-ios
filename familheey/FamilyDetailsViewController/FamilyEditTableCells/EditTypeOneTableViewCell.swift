//
//  EditTypeOneTableViewCell.swift
//  familheey
//
//  Created by familheey on 10/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class EditTypeOneTableViewCell: UITableViewCell {
    @IBOutlet weak var txtValue: UITextField!
    @IBOutlet weak var btnAction: UIButton!
    
    @IBOutlet weak var lblHead: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
