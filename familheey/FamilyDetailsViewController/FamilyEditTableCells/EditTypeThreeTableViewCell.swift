//
//  EditTypeThreeTableViewCell.swift
//  familheey
//
//  Created by familheey on 10/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class EditTypeThreeTableViewCell: UITableViewCell {
    @IBOutlet weak var lblHead: UILabel!
    @IBOutlet weak var frontView: UIView!
    @IBOutlet weak var lblValue1: UILabel!
    @IBOutlet weak var btnValue1: UIButton!
    @IBOutlet weak var lblValue2: UILabel!
    @IBOutlet weak var btnValue2: UIButton!
    @IBOutlet weak var imgSelct1: UIImageView!
    @IBOutlet weak var imgSelct2: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
