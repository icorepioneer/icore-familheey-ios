//
//  EditTypeTwoTableViewCell.swift
//  familheey
//
//  Created by familheey on 10/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class EditTypeTwoTableViewCell: UITableViewCell {
    @IBOutlet weak var imgLogo: UIImageView!
    
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var btnCover: UIButton!
    @IBOutlet weak var btnLogo: UIButton!
    
    @IBOutlet weak var lblHead: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
