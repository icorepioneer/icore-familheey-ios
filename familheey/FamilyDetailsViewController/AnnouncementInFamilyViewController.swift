//
//  AnnouncementInFamilyViewController.swift
//  familheey
//
//  Created by familheey on 29/11/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON
import Kingfisher
import SafariServices

class AnnouncementInFamilyViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var viewOfNoData: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearchReset: UIButton!
    @IBOutlet weak var tblListView: UITableView!
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var lblHead: UILabel!
    @IBOutlet weak var aboutClctnVew: UICollectionView!
    @IBOutlet weak var createAnnouncementbutton: UIButton!
    
    var refreshControl = UIRefreshControl()
    var ArrayAnnouncements = [JSON]()
    var offset = 0
    var limit = 20
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    var postoptionsTittle = [String]()
    var groupId = ""
    var link_type = 0
    var isAdmin = ""
    var islinkable:Int = Int()
    var facate = ""
    var arr = [String]()
    var famSettings = 0
    var announcementSet = 0
    var isMembershipActive = false
    var postId                          = 0
    var announcementId                  = 0
    
    var aboutArr = ["FEED","ANNOUNCEMENT","REQUEST","EVENTS","ALBUMS","ABOUT US","DOCUMENTS","LINKED FAMILIES","MEMBERS"]
    var ActiveTab = 1
    var famArr : [familyModel]?
    var memberJoining = 0
    var invitationStatus = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(announcementId)
        print(postId)
        
        if isAdmin.lowercased() == "not-member"{
            aboutArr = ["FEED","ANNOUNCEMENT","ABOUT US"]
            self.createAnnouncementbutton.isHidden = true
        }
        else{
            if isAdmin.lowercased() == "admin"{
                if isMembershipActive{
                    aboutArr = ["FEED","ANNOUNCEMENT","REQUEST","EVENTS","ALBUMS","ABOUT US","DOCUMENTS","LINKED FAMILIES","MEMBERSHIP","MEMBERS"]
                }
            }
        }
        
        aboutClctnVew.delegate = self
        aboutClctnVew.dataSource = self
        
        refreshControl.attributedTitle = NSAttributedString(string: "Please wait...")
        refreshControl.addTarget(self, action: #selector(refreshPost), for: .valueChanged)
        tblListView.addSubview(refreshControl)
        
        Helpers.setleftView(textfield: txtSearch, customWidth: 30)
        txtSearch.delegate = self
        
        self.lblHead.text = appDel.groupNamePublic
        let temp = appDel.groupImageUrlPublic.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let imgUrl = URL(string: temp)
        if imgUrl != nil{
            self.imgCover.kf.indicatorType = .activity
            
            let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=475&height=400&url="+imgUrl!.relativeString
            let urlImg = URL(string: newUrlStr)
            
            self.imgCover.kf.setImage(with: urlImg, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        else{
            self.imgCover.image = #imageLiteral(resourceName: "family_default_cover.png")
        }
        
        //        tblListView.dataSource = self
        //        tblListView.delegate = self
    }
    override func viewDidLayoutSubviews(){
        super.viewDidLayoutSubviews()
        aboutClctnVew.reloadData()
        
        let indexPath = IndexPath(item: ActiveTab, section: 0)
        aboutClctnVew.scrollToItem(at: indexPath, at:.centeredHorizontally, animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        getAnnouncements()
    }
    
    //MARK:- Custom Actions
    
    @objc func refreshPost(refreshControl: UIRefreshControl) {
        refreshControl.endRefreshing()
        ArrayAnnouncements = [JSON]()
        getAnnouncements()
    }
    
    func getAnnouncements(){
        ActivityIndicatorView.show("Please wait....")
        let param = ["user_id" : UserDefaults.standard.value(forKey: "userId") as! String,"offset":"\(offset)","limit":"\(limit)","type":"announcement","group_id":self.groupId,"query":self.txtSearch.text!]
        
        networkProvider.request(.postByFamily(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        print("view contents : \(jsonData)")
                        
                        if let response = jsonData["data"].array{
                            self.ArrayAnnouncements.removeAll()
                            self.ArrayAnnouncements.append(contentsOf: response)
                            if self.ArrayAnnouncements.isEmpty {
                                self.lblNoData.isHidden = false
                                self.viewOfNoData.isHidden = false
                                if self.txtSearch.text == ""
                                {
                                    self.lblNoData.text = "No announcements till now"
                                }
                                else
                                {
                                    self.lblNoData.text = "No results to show"
                                }
                                self.tblListView.isHidden = true
                            } else {
                                self.lblNoData.isHidden = true
                                self.viewOfNoData.isHidden = true
                                self.tblListView.isHidden = false
                                self.tblListView.dataSource = self
                                self.tblListView.delegate = self
                                self.tblListView.reloadData()
                            }
                            
                        } else {
                            
                            self.tblListView.isHidden = true
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getAnnouncements()
                        }
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    func deletePostForCreator(tag:Int){
        let alert = UIAlertController(title: "Familheey", message: "Do you really want to delete this post? Deleting this post will delete it from everywhere you posted it to and you will lose all associated conversations", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "DELETE", style: .destructive, handler: { _ in
            let post = self.ArrayAnnouncements[tag]
            
            
            let param = ["id" : post["post_id"].stringValue,"user_id":UserDefaults.standard.value(forKey: "userId") as! String]
            
            ActivityIndicatorView.show("Please wait....")
            self.networkProvider.request(.deletePost(parameter: param)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                        print("view contents : \(jsonData)")
                        if response.statusCode == 200{
                            print("view contents : \(jsonData)")
                            //                                   self.getPosts()
                            self.getAnnouncements()
                            
                        }
                        else if response.statusCode == 401 {
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                self.deletePostForCreator(tag: tag)
                            }
                        }
                        
                        ActivityIndicatorView.hiding()
                        
                    }catch let err {
                        ActivityIndicatorView.hiding()
                        // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                    }
                case .failure( let error):
                    ActivityIndicatorView.hiding()
                    //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                    break
                }
            }
            
        }))
        alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func removePostFromTimeline(tag:Int){
        let post = ArrayAnnouncements[tag]
        
        let param = ["post_id" : post["post_id"].stringValue, "user_id": UserDefaults.standard.value(forKey: "userId") as! String]
        ActivityIndicatorView.show("Please wait....")
        networkProvider.request(.removePost(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        print("view contents : \(jsonData)")
                        //                    self.getAnnouncements()
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.removePostFromTimeline(tag: tag)
                        }
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //  Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    func muteConversation(tag:Int){
        let post = ArrayAnnouncements[tag]
        let param = ["post_id" : post["post_id"].stringValue, "user_id": UserDefaults.standard.value(forKey: "userId") as! String]
        
        ActivityIndicatorView.show("Please wait....")
        networkProvider.request(.mute_conversation(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        print("view contents : \(jsonData)")
                        let temp = jsonData["data"].dictionaryValue
                        if temp["is_active"]!.boolValue{
                            var temp = post
                            temp["muted"] = true
                            self.ArrayAnnouncements[tag] = temp
                        }
                        else{
                            var temp = post
                            temp["muted"] = false
                            self.ArrayAnnouncements[tag] = temp
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.muteConversation(tag: tag)
                        }
                    }
                    ActivityIndicatorView.hiding()
                }catch let err {
                    ActivityIndicatorView.hiding()
                    //  Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    //MARK:- Button Actions
    @IBAction func onClickBack(_ sender: Any) {
        //        self.navigationController?.popViewController(animated: true)
        self.navigationController?.popToViewController(ofClass: FamilyDetailsTableViewController.self)
    }
    
    @IBAction func onClickSearchReset(_ sender: Any) {
        txtSearch.text = ""
        //         = ""
        getAnnouncements()
        btnSearchReset.isHidden = true
        self.txtSearch.endEditing(true)
    }
    
    @IBAction func onClickShareAnnouncement(_ sender: UIButton) {
        let post = ArrayAnnouncements[sender.tag]
        //        let post = ArrayPosts[sender.tag]
        print(post)
        let mainStoryBoard = UIStoryboard(name: "third", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "PostShareViewController") as! PostShareViewController
        vc.fromAnnouncement = true
        vc.postId = post["post_id"].stringValue
        self.navigationController?.pushViewController(vc, animated:true)
    }
    
    @IBAction func onClickConversation(_ sender: UIButton) {
        let post = ArrayAnnouncements[sender.tag]
        let mainStoryBoard = UIStoryboard(name: "Conversations", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "ConversationsViewController") as! ConversationsViewController
        vc.post = post
        vc.conversationT = .announcement
        self.navigationController?.pushViewController(vc, animated:true)
    }
    
    @IBAction func onClickViews(_ sender: UIButton) {
        let post = ArrayAnnouncements[sender.tag]
        let mainStoryBoard = UIStoryboard(name: "fourth", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "UserFamilyListViewController") as! UserFamilyListViewController
        vc.postId = post["post_id"].stringValue
        vc.isFrompost = true
        vc.isFrom = "postview"
        self.navigationController?.pushViewController(vc, animated:true)
    }
    
    @IBAction func onClickDocs(_ sender: Any) {
    }
    
    @IBAction func onClickRightMenu(_ sender: UIButton) {
        let post = ArrayAnnouncements[sender.tag]
        if isAdmin.lowercased() == "not-member"{
            postoptionsTittle =  ["Report"]
            showActionSheet(titleArr: postoptionsTittle as NSArray, title: "Select options") { (index) in
                if index == 0{
                    let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "ReportPostViewController") as! ReportPostViewController
                    
                    vc.postDetails =  self.ArrayAnnouncements[sender.tag]
                    vc.isFrom = "announcement"
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else if index == 100{
                    
                }
            }
        }
        else{
            if post["created_by"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")" {
                postoptionsTittle =  ["Edit","Delete",]
            }
            else{
                if isAdmin.lowercased() == "admin"{
                    postoptionsTittle =  ["Hide","Delete","Report"]
                }
                else{
                    postoptionsTittle =  ["Hide","Report"]
                }
            }
            
            if post["muted"].stringValue == "true"{
                postoptionsTittle.insert("Unmute", at: 0)
            }
            else{
                postoptionsTittle.insert("Mute", at: 0)
            }
            
            showActionSheet(titleArr: postoptionsTittle as NSArray, title: "Select options") { (index) in
                if index == 2{
                    if post["created_by"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")"{
                        self.deletePostForCreator(tag: sender.tag)
                    }
                    else if self.isAdmin.lowercased() == "admin"{
                        self.deletePostForCreator(tag: sender.tag)
                    }
                    else{
                        let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "ReportPostViewController") as! ReportPostViewController
                        
                        vc.postDetails =  self.ArrayAnnouncements[sender.tag]
                        vc.isFrom = "announcement"
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                else if index == 1{
                    if post["created_by"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")"{
                        
                        let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "CreateAnnouncmentViewController") as! CreateAnnouncmentViewController
                        
                        vc.fromEdit = true
                        vc.editPostDetails = post
                        self.navigationController?.pushViewController(vc, animated: true)
                        //                self.deletePostForCreator(tag: sender.tag)
                    }else{
                        self.removePostFromTimeline(tag: sender.tag)
                    }
                }
                else if index == 100{
                }
                else if index == 3{
                    let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "ReportPostViewController") as! ReportPostViewController
                    
                    vc.postDetails =  self.ArrayAnnouncements[sender.tag]
                    vc.isFrom = "announcement"
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else{
                    self.muteConversation(tag: sender.tag)
                }
            }
        }
    }
    
    
    @IBAction func onClickFeeds(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
        let addMember = storyboard.instantiateViewController(withIdentifier: "PostedInGroupsViewController") as! PostedInGroupsViewController
        addMember.groupId = self.groupId
        addMember.isAdmin = self.isAdmin
        addMember.link_type = self.link_type
        addMember.islinkable = self.islinkable
        addMember.facate = self.facate
        addMember.famSettings = famSettings
        addMember.announcementSet = announcementSet
        addMember.postId                    = self.postId
        addMember.announcementId            = self.announcementId
        addMember.famArr = famArr
        addMember.memberJoining = self.memberJoining
        addMember.invitationStatus = self.invitationStatus
        addMember.isMembershipActive = self.isMembershipActive
        self.navigationController?.pushViewController(addMember, animated: false)
    }
    
    @IBAction func onClickAnnouncement(_ sender: Any) {
        getAnnouncements()
    }
    @IBAction func onClickEvents(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "third", bundle: nil)
        let addMember = storyboard.instantiateViewController(withIdentifier: "EventListingViewController") as! EventListingViewController
        addMember.groupID = self.groupId
        addMember.isAdmin = self.isAdmin
        addMember.link_type = self.link_type
        addMember.islinkable = self.islinkable
        addMember.facte = self.facate
        addMember.famSettings = famSettings
        addMember.announcementSet = announcementSet
        addMember.postId                    = self.postId
        addMember.announcementId            = self.announcementId
        addMember.famArr = famArr
        addMember.memberJoining = self.memberJoining
        addMember.invitationStatus = self.invitationStatus
        addMember.isMembershipActive = self.isMembershipActive
        self.navigationController?.pushViewController(addMember, animated: false)
    }
    @IBAction func onClickAboutus(_ sender: Any) { // Change to Member
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let addMember = storyboard.instantiateViewController(withIdentifier: "AddMembersNewViewController") as! AddMembersNewViewController
        addMember.groupId = self.groupId
        addMember.isAdmin = self.isAdmin
        addMember.link_type = self.link_type
        addMember.islinkable = self.islinkable
        addMember.faCate = self.facate
        addMember.famSettings = famSettings
        addMember.announcementSet = announcementSet
        addMember.postId                    = self.postId
        addMember.announcementId            = self.announcementId
        addMember.famArr = famArr
        addMember.memberJoiningStatus = self.memberJoining
        addMember.invitationStatus = self.invitationStatus
        addMember.isMembershipActive = self.isMembershipActive
        self.navigationController?.pushViewController(addMember, animated: false)
    }
    @IBAction func onClickSharedBy(_ sender: UIButton) {
        let post = ArrayAnnouncements[sender.tag]
        let mainStoryBoard = UIStoryboard(name: "fourth", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "UserFamilyListViewController") as! UserFamilyListViewController
        if post["orgin_id"].stringValue.count > 0{
            vc.postId = post["orgin_id"].stringValue
        }
        else{
            vc.postId = post["post_id"].stringValue
        }
        vc.isFrompost = true
        vc.isFrom = "groupshare"
        vc.familyID = post["to_group_id"].stringValue
        self.navigationController?.pushViewController(vc, animated:true)
    }
    @IBAction func onClickMore(_ sender: Any) {
        arr = ["About us","Linked Families","Albums","Documents","Calender"]
        
        self.showActionSheet(titleArr: self.arr as! NSArray, title: "Choose option") { (index) in
            if index == 3{
                let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "DocumentListingViewController") as! DocumentListingViewController
                addMember.groupID = self.groupId
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.isFromdocument = true
                addMember.facate = self.facate
                //            addMember.faCate = self.familyArr![0].faCategory
                //            addMember.memberJoining = self.familyArr![0].memberApproval
                self.navigationController?.pushViewController(addMember, animated: false)
                //            setActiveButton(index: 2)
            }
            else if index == 100{
            }
            else if index == 2{
                let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "AlbumListingViewController") as! AlbumListingViewController
                addMember.groupID = self.groupId
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.facate = self.facate
                //        addMember.faCate = self.familyArr![0].faCategory
                //        addMember.memberJoining = self.familyArr![0].memberApprova
                self.navigationController?.pushViewController(addMember, animated: false)
            }
            else if index == 0{
                let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
                intro.groupId = self.groupId
                intro.isAboutUsClicked = true
                self.navigationController?.pushViewController(intro, animated: false)
            }
            else if index == 4{
                
                let stryboard = UIStoryboard.init(name: "third", bundle: nil)
                
                let vc = stryboard.instantiateViewController(withIdentifier: "CalendarEventsViewController") as! CalendarEventsViewController
                vc.isFromExplore = ""
                vc.isFromGroup = true
                vc.groupID = self.groupId
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
            else{
                let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "LinkedFamiliesListViewController") as! LinkedFamiliesListViewController
                addMember.groupID = self.groupId
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.facate = self.facate
                //            addMember.faCate = self.familyArr![0].faCategory
                //            addMember.memberJoining = self.familyArr![0].memberApproval
                self.navigationController?.pushViewController(addMember, animated: false)
            }
        }
    }
    
    //MARK:- Go To Create Announcement
    
    @IBAction func goToCreateAnnouncment(_ sender: Any) {
        
        if announcementSet == 17{
            if self.isAdmin.lowercased() != "admin"{
                self.displayAlert(alertStr: "You don't have permission", title: "")
                return
            }
        }
        
        
        
        let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CreateAnnouncmentViewController") as! CreateAnnouncmentViewController
        
        
        let selectedUserIDArr                     = NSMutableArray()
        
        
        
        let selectedGroups = NSMutableDictionary()
        
        selectedGroups.setValue( "\(groupId)", forKey: "id")
        selectedGroups.setValue( "\(announcementId)", forKey: "announcement_create")
        selectedUserIDArr.add(selectedGroups)
        
        vc.selectedUserIDArr = selectedUserIDArr
        vc.fromGroup = true
        //                           vc.fromEdit = true
        //                           vc.editPostDetails = post
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    //MARK:- collectionView Delegates
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return aboutArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AboutFamilyCollectionViewCell", for: indexPath as IndexPath) as! AboutFamilyCollectionViewCell
        cell.lblName.text = aboutArr[indexPath.item]
        if indexPath.row == ActiveTab{
            cell.lblName.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            cell.lblName.font = .boldSystemFont(ofSize: 15)
            cell.imgUnderline.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            
        }
        else{
            cell.lblName.textColor = .black
            cell.lblName.font = .systemFont(ofSize: 14)
            cell.imgUnderline.backgroundColor = .white
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0{ // Feeds
            onClickFeeds(self)
        }
        else if indexPath.row == 1{ // Announcement
            getAnnouncements()
        }
        else if indexPath.row == 2{ // Request
            
            if isAdmin.lowercased() == "not-member"{ // About us
                let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
                intro.groupId = self.groupId
                intro.isAboutUsClicked = true
                self.navigationController?.pushViewController(intro, animated: false)
            }
            else{
                let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "RequestInFamilyViewController") as! RequestInFamilyViewController
                addMember.groupID = self.groupId
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.facate = self.facate
                addMember.famSettings = famSettings
                addMember.announcementSet = announcementSet
                addMember.postId                    = self.postId
                addMember.announcementId            = self.announcementId
                addMember.famArr = famArr
                addMember.memberJoining = self.memberJoining
                addMember.invitationStatus = self.invitationStatus
                addMember.isMembershipActive = self.isMembershipActive
                self.navigationController?.pushViewController(addMember, animated: false)
            }
        }
        else if indexPath.row == 3{ //Events
            onClickEvents(self)
        }
        else if indexPath.row == 5{ // About us
            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
            intro.groupId = self.groupId
            intro.isAboutUsClicked = true
            self.navigationController?.pushViewController(intro, animated: false)
        }
        else if indexPath.row == 8{ // Members
            if isMembershipActive{
                if isAdmin.lowercased() == "admin"{
                    let storyboard = UIStoryboard.init(name: "MembershipMngmnt", bundle: nil)
                    let addMember = storyboard.instantiateViewController(withIdentifier: "MemebrshipDashboardViewController") as! MemebrshipDashboardViewController
                    addMember.groupID = self.groupId
                    addMember.isAdmin = self.isAdmin
                    addMember.link_type = self.link_type
                    addMember.islinkable = self.islinkable
                    //                        addMember.isFromdocument = true
                    addMember.facate = self.facate
                    addMember.famSettings = famSettings
                    addMember.announcementSet = announcementSet
                    addMember.postId                    = self.postId
                    addMember.announcementId            = self.announcementId
                    addMember.isMembershipActive = self.isMembershipActive
                    addMember.famArr = famArr
                    addMember.memberJoining = self.memberJoining
                    addMember.invitationStatus = self.invitationStatus
                    addMember.isMembershipActive = self.isMembershipActive
                    self.navigationController?.pushViewController(addMember, animated: true)
                }
                else{
                    onClickAboutus(self)
                }
            }
            else{
                onClickAboutus(self)
            }
        }
        else if indexPath.row == 9{
            onClickAboutus(self)
        }
        else if indexPath.row == 4{ // Albums
            let storyboard = UIStoryboard.init(name: "third", bundle: nil)
            let addMember = storyboard.instantiateViewController(withIdentifier: "AlbumListingViewController") as! AlbumListingViewController
            addMember.groupID = self.groupId
            addMember.isAdmin = self.isAdmin
            addMember.link_type = self.link_type
            addMember.islinkable = self.islinkable
            addMember.facate = self.facate
            addMember.famSettings = famSettings
            addMember.announcementSet = announcementSet
            addMember.postId                    = self.postId
            addMember.announcementId            = self.announcementId
            addMember.famArr = famArr
            addMember.memberJoining = self.memberJoining
            addMember.invitationStatus = self.invitationStatus
            addMember.isMembershipActive = self.isMembershipActive
            self.navigationController?.pushViewController(addMember, animated: false)
        }
        else if indexPath.row == 6{ // Documents
            let storyboard = UIStoryboard.init(name: "third", bundle: nil)
            let addMember = storyboard.instantiateViewController(withIdentifier: "DocumentListingViewController") as! DocumentListingViewController
            addMember.groupID = self.groupId
            addMember.isAdmin = self.isAdmin
            addMember.link_type = self.link_type
            addMember.islinkable = self.islinkable
            addMember.isFromdocument = true
            addMember.facate = self.facate
            addMember.famSettings = famSettings
            addMember.announcementSet = announcementSet
            addMember.postId                    = self.postId
            addMember.announcementId            = self.announcementId
            addMember.famArr = famArr
            addMember.memberJoining = self.memberJoining
            addMember.invitationStatus = self.invitationStatus
            addMember.isMembershipActive = self.isMembershipActive
            self.navigationController?.pushViewController(addMember, animated: false)
        }
        else{ // Linked families
            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            let addMember = storyboard.instantiateViewController(withIdentifier: "LinkedFamiliesListViewController") as! LinkedFamiliesListViewController
            addMember.groupID = self.groupId
            addMember.isAdmin = self.isAdmin
            addMember.link_type = self.link_type
            addMember.islinkable = self.islinkable
            addMember.facate = self.facate
            addMember.famSettings = famSettings
            addMember.announcementSet = announcementSet
            addMember.postId                    = self.postId
            addMember.announcementId            = self.announcementId
            addMember.famArr = famArr
            addMember.memberJoining = self.memberJoining
            addMember.invitationStatus = self.invitationStatus
            addMember.isMembershipActive = self.isMembershipActive
            self.navigationController?.pushViewController(addMember, animated: false)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = aboutArr[indexPath.row].size(withAttributes: nil)
        return CGSize(width: w.width, height: 60)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension AnnouncementInFamilyViewController : UITableViewDataSource,UITableViewDelegate{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return ArrayAnnouncements.count + 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if ArrayAnnouncements.count == indexPath.row{
            
            return UITableViewCell()
        }
        
        let announcement = ArrayAnnouncements[indexPath.row]
        
        
        if let sharedusers = announcement["shared_user_names"].string , !sharedusers.isEmpty{
            
            let cell  = tableView.dequeueReusableCell(withIdentifier: "AnnouncementsSharedTableViewCell", for: indexPath) as! AnnouncementsSharedTableViewCell
            
            cell.btnSharedBy.tag = indexPath.row
            
            let array = sharedusers.components(separatedBy: ",")
            if array.count != 0{
                if array.count == 1{
                    cell.lblsharedUserName.text = "\(array[0]) shared an announcement"
                }
                else if array.count == 2{
                    cell.lblsharedUserName.text = "\(array[0]) and 1 other shared an announcement"
                }
                else {
                    cell.lblsharedUserName.text = "\(array[0]) and \(array.count-1) others shared an announcement"
                }
            }
            
            cell.lblShared.text = "Shared in"
            cell.lblShare_width.constant = 65.0
            cell.lblPostedIn.text = announcement["parent_post_created_user_name"].string ?? ""
            cell.lblUserName.text = announcement["parent_post_grp_name"].string ?? ""
            
            cell.lblSharedPostedIn.text = announcement["group_name"].string ?? ""
            
            //            if indexPath.section == 0{
            cell.buttonDocs.tag = indexPath.row
            cell.buttonViews.tag = indexPath.row
            cell.buttonComments.tag = indexPath.row
            cell.buttonShareAnnouncement.tag = indexPath.row
            cell.buttonSideMenu.tag = indexPath.row
            //            }
            
            
            cell.lblSharedDate.text = HomeTabViewController.dateFormatterCommon(dateStr: announcement["createdAt"].stringValue)
            
            cell.lblnumberofComments.text = announcement["conversation_count"].string ?? ""
            cell.lblnumberofView.text = announcement["views_count"].string ?? ""
            
            
            var count = 0
            
            if announcement["read_status"].boolValue{
                cell.lblAnnouncement.font = UIFont.systemFont(ofSize: 15.0)
                cell.lblAnnouncement.text = announcement["snap_description"].string ?? ""
            }
            else{
                cell.lblAnnouncement.font = UIFont.boldSystemFont(ofSize: 15.0)
                cell.lblAnnouncement.text = announcement["snap_description"].string ?? ""
            }
            if let attachments = announcement["post_attachment"].array{
                
                for items in attachments{
                    if items["type"].stringValue.contains("image") {
                        count = count + 1
                    }
                }
                let proPic = announcement["parent_family_logo"].stringValue
                
                
                if proPic.count != 0{
                    let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+proPic
                    let imgUrl = URL(string: temp)
                    cell.imageviewUserProfilepic.kf.indicatorType = .activity
                    cell.imageviewUserProfilepic.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                }
                else{
                    cell.imageviewUserProfilepic.image = #imageLiteral(resourceName: "Family Logo")
                }
            }
            if announcement["is_shareable"].boolValue {
                cell.viewShare.isHidden = false
            }
            else{
                cell.viewShare.isHidden = true
                
            }
            if announcement["conversation_enabled"].boolValue{
                cell.viewComments.isHidden = false
            }
            else{
                cell.viewComments.isHidden = true
            }
            cell.numberofDocs.text = "\(count)"
            
            if "\(UserDefaults.standard.value(forKey: "userId") ?? "0")" == announcement["created_by"].stringValue {
                
                cell.viewViews.isHidden = false
                
            }
            else{
                cell.viewViews.isHidden = true
                
            }
            cell.lblAnnouncement.handleHashtagTap { hashtag in
                print("Success. You just tapped the \(hashtag) hashtag")
                self.txtSearch.text  = hashtag
                self.txtSearch.becomeFirstResponder()
            }
            cell.lblAnnouncement.handleMentionTap { (mension) in
                print("Success. You just tapped the \(mension) mension")
                self.txtSearch.text  = mension
                self.txtSearch.becomeFirstResponder()
            }
            cell.lblAnnouncement.handleURLTap { (url) in
                print(url)
                //                UIApplication.shared.open(Requrl)
                
                if !(["http", "https"].contains(url.scheme?.lowercased())) {
                    var strUrl = url.absoluteString
                    strUrl = "http://"+strUrl
                    let Turl = URL(string: strUrl)
                    
                    let vc = SFSafariViewController(url: Turl!)
                    self.present(vc, animated: true, completion: nil)
                }
                else{
                    let vc = SFSafariViewController(url: url)
                    self.present(vc, animated: true, completion: nil)
                }
                
                
                
            }
            return cell
        }
            
        else
        {
            
            
            let cell  = tableView.dequeueReusableCell(withIdentifier: "AnnouncementsTableViewCell", for: indexPath) as! AnnouncementsTableViewCell
            
            cell.buttonDocs.tag = indexPath.row
            cell.buttonViews.tag = indexPath.row
            cell.buttonComments.tag = indexPath.row
            cell.buttonShareAnnouncement.tag = indexPath.row
            cell.buttonSideMenu.tag = indexPath.row
            
            //        cell.lblDate.text = announcement["createdAt"].string ?? ""
            cell.lblDate.text = HomeTabViewController.dateFormatterCommon(dateStr: announcement["createdAt"].stringValue)
            cell.lblPostedIn.text = announcement["group_name"].string ?? ""
            cell.lblUserName.text = announcement["created_user_name"].string ?? ""
            cell.lblAnnouncement.text = announcement["snap_description"].string ?? ""
            
            cell.lblnumberofComments.text = announcement["conversation_count"].string ?? ""
            
            cell.lblnumberofView.text = announcement["views_count"].string ?? ""
            
            
            var count = 0
            
            if let attachments = announcement["post_attachment"].array{
                
                for items in attachments{
                    if items["type"].stringValue.contains("image") {
                        count = count + 1
                    }
                }
                let proPic = announcement["propic"].stringValue
                
                
                if proPic.count != 0{
                    let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                    let imgUrl = URL(string: temp)
                    cell.imageviewUserProfilepic.kf.indicatorType = .activity
                    
                    cell.imageviewUserProfilepic.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                }
                else{
                    cell.imageviewUserProfilepic.image = #imageLiteral(resourceName: "Male Colored")
                }
                
                
            }
            
            cell.numberofDocs.text = "\(count)"
            
            if "\(UserDefaults.standard.value(forKey: "userId") ?? "0")" == announcement["created_by"].stringValue {
                
                cell.viewViews.isHidden = false
                
            }
            else{
                cell.viewViews.isHidden = true
                
            }
            if announcement["is_shareable"].boolValue {
                cell.viewShare.isHidden = false
            }
            else{
                cell.viewShare.isHidden = true
                
            }
            
            if announcement["conversation_enabled"].boolValue{
                cell.viewComments.isHidden = false
            }
            else{
                cell.viewComments.isHidden = true
            }
            /* cell.lblAnnouncement.handleHashtagTap { hashtag in
             print("Success. You just tapped the \(hashtag) hashtag")
             self.txtSearch.text  = "#"+hashtag
             //            self.searchview.isHidden = false
             //            self.searchConstraintheight.constant = 36
             self.txtSearch.becomeFirstResponder()
             
             }
             cell.lblAnnouncement.handleMentionTap { (mension) in
             print("Success. You just tapped the \(mension) mension")
             self.txtSearch.text  = mension
             //            self.searchview.isHidden = false
             //            self.searchConstraintheight.constant = 36
             self.txtSearch.becomeFirstResponder()
             
             }
             cell.lblAnnouncement.handleURLTap { (url) in
             print(url)
             //                    UIApplication.shared.open(url)
             let vc = SFSafariViewController(url: url)
             self.present(vc, animated: true, completion: nil)
             }*/
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if ArrayAnnouncements.count == indexPath.row{
            return
        }
        else
        {
            tableView.deselectRow(at: indexPath, animated: false)
            print(ArrayAnnouncements)
            let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementDetailViewController") as! AnnouncementDetailViewController
            vc.ArrayAnnouncements = ArrayAnnouncements
            vc.selectedindex = indexPath
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }
}
extension AnnouncementInFamilyViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == txtSearch
        {
            // selectWebAPI()
            if textField.text!.count > 0{
                btnSearchReset.isHidden = false
            }
            else{
                btnSearchReset.isHidden = true
            }
            textField.endEditing(true)
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        self.view.endEditing(true)
        if textField == txtSearch
        {
            if textField.text!.count > 0{
                btnSearchReset.isHidden = false
            }
            else{
                btnSearchReset.isHidden = true
            }
            getAnnouncements()
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
            // selectWebAPI()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
            // selectWebAPI()
        }
        return true
    }
}
