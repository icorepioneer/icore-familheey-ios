//
//  FamilyDetailsTableViewController.swift
//  familheey
//
//  Created by Giri on 20/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import SwiftyJSON
import Moya


class FamilyDetailsTableViewController: UITableViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,EditAboutGroupDelegate, EditFamilyDetailsDelegate ,EditAboutGroupHistoryDelegate,UITextFieldDelegate,changeProPicDelegate,updateAfterFamilySettingsEdit,familySettingUpdateDelegate, membershipPaymentDelegate{
    
    let loadingDefaultView = UIView()
    
    var fromButtonAction = false
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    
    
    func updateAbout(data: String,workedit:Bool) {
        if !workedit
        {
            aboutString = data
            tableView.beginUpdates()
            tableView.reloadRows(at: [IndexPath.init(row: 0, section: 0)], with: .none)
            tableView.endUpdates()
            
        }
        
    }
    
    //   func updateAboutHistory(data: String, firstImg: UIImage) {
    //        aboutString = data
    //        tableView.beginUpdates()
    //        tableView.reloadRows(at: [IndexPath.init(row: 0, section: 0)], with: .none)
    //        tableView.endUpdates()
    //    }
    
    
    func updateAboutHistory(history_text:String,history_images:[JSON])
    {
        
        self.history_text = history_text
        self.history_images = history_images
        tableView.beginUpdates()
        tableView.reloadRows(at: [IndexPath.init(row: 0, section: 1)], with: .none)
        tableView.endUpdates()
        
    }
    var urlEditEnable = false
    @IBOutlet weak var top: UIView!
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var imageviewProfilePic: UIImageView!
    @IBOutlet weak var collectionViewTitles: UICollectionView!
    @IBOutlet weak var lblGroupTitle: UILabel!
    @IBOutlet weak var lblGroupDetails: UILabel!
    @IBOutlet weak var aboutView: UIView!
    
    //forActiveState
    @IBOutlet weak var viewtimelineActive: UIView!
    @IBOutlet weak var viewAlbumActive: UIView!
    @IBOutlet weak var viewEventActive: UIView!
    @IBOutlet weak var viewAboutActive: UIView!
    @IBOutlet weak var imgEditCam: UIImageView!
    @IBOutlet weak var imgEditCoverCam: UIImageView!
    
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var imgSettings: UIImageView!
    @IBOutlet weak var lblJoin: UILabel!
    
    @IBOutlet weak var btnSettings: UIButton!
    
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var labelAbout: UILabel!
    @IBOutlet weak var labelEvent: UILabel!
    @IBOutlet weak var labelAlbum: UILabel!
    @IBOutlet weak var labelTimeLine: UILabel!
    @IBOutlet weak var aboutCollectnVew: UICollectionView!
    
    @IBOutlet weak var viewMembership: UIView!
    @IBOutlet weak var lblMembershipData: UILabel!
    @IBOutlet weak var noMembrView: UIView!
    @IBOutlet weak var imgMembrshipStatus: UIImageView!
    
    
    var btnFloat = UIButton(type: .custom)
    var ActiveTab = 5
    var groupId:String = ""
    var isFromCreate:Bool = false
    var isFromNotification:Bool = false
    var isFromFamilyList = false
    var isAboutUsClicked = false
    var isFromSearchTab = false
    var isFromDelegate = false
    var isMemberNotification = false
    var isFromBanner = false
    
    var isAdmin = ""
    var isActive = 0
    var linkType = 0
    var arr = [String]()
    var familyArr : [familyModel]?
    var memberCount = 0
    var eventCount = 0
    var knowCount = 0
    var faCate = ""
    var famSettings = 6
    var announcementSettings = 18
    var postVisibility = 0
    
    var aboutString  =  ""
    var shareURLString  =  ""
    var urlTextString  =  ""
    
    
    var roleOFUser   = ""
    
    var history_text = ""
    var history_images:[JSON] = []
    var accountArr = JSON()
    var isAccountValid = false
    
    var titleArray = ["members","posts"]
    var aboutArr = ["FEED","ANNOUNCEMENT","REQUEST","EVENTS","ALBUMS","ABOUT US","DOCUMENTS","LINKED FAMILIES","MEMBERS"]
    @IBOutlet weak var btnChangeCover: UIButton!
    
    var profileImageUrl = ""
    var coverPicUrl     = ""
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadingDefaultView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height:  self.view.frame.size.height)
        self.loadingDefaultView.backgroundColor = .white
       
        print(groupId)
//        appDel.fromFamilyDetailBack = false

        getFamilyDetails(groupId: groupId)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        btnFloat.removeFromSuperview()
        
        //Show the navigation bar
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.navigationController?.navigationBar.isHidden = true
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        getFamilyDetails(groupId: groupId)

        self.tabBarController?.tabBar.isHidden = true
        
        //  setFloatingButton()
        // Hide the navigation bar
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        //        setActiveButton(index: 0)
        
        
//        noMembrView.isHidden = false
        collectionViewTitles.delegate = self
        collectionViewTitles.dataSource = self
        self.aboutCollectnVew.dataSource = self
        self.aboutCollectnVew.delegate = self
        self.aboutCollectnVew.reloadData()
        
        tableView.tableFooterView = UIView.init()
        
        self.aboutView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        self.noMembrView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    override func viewDidLayoutSubviews(){
        super.viewDidLayoutSubviews()
        let indexPath = IndexPath(item: ActiveTab, section: 0)
        aboutCollectnVew.scrollToItem(at: indexPath, at:.centeredHorizontally, animated: true)
    }
    
    //MARK:- Web API
    func getFamilyDetails(groupId:String){
        print(groupId)
        print(UserDefaults.standard.value(forKey: "userId") as! String)
        if !isFromDelegate{
            self.loadingDefaultView.showDefaultAnimation(target:self)
        }
        else{
            self.isFromDelegate = false
        }
    
        if !Connectivity.isConnectedToInternet(){
            self.displayAlertChoice(alertStr: "No Internet! Please Check your connectivity", title: "") { (result) in
                self.getFamilyDetails(groupId: self.groupId)
            }
        }
        ActivityIndicatorView.show("Please wait!")

        APIServiceManager.callServer.getFamilyDetails(url: EndPoint.getFamily, groupId: groupId, userId:UserDefaults.standard.value(forKey: "userId") as! String, success: { (responseMdl) in
            print(responseMdl!)
            guard let familyMdl = responseMdl as? familyListResponse else{
                return
            }
            ActivityIndicatorView.hiding()
            self.familyArr = familyMdl.familyList
            print(familyMdl.familyList as Any)
            if familyMdl.statusCode == 200
            {
                self.loadingDefaultView.removeFromSuperview()
                
                
                if familyMdl.familyList!.count > 0{
                    if !familyMdl.familyList![0].isFamilyActive{
                        self.displayAlertChoice(alertStr: "Oops! This content is no longer available.", title: "", completion: { (result) in
                            self.navigationController?.popViewController(animated: true)
                        })
                        return
                    }
                    self.lblGroupTitle.text = familyMdl.familyList![0].faName
                    
                    appDel.groupNamePublic = self.lblGroupTitle.text!
                    
                    self.lblGroupDetails.text = "\(familyMdl.familyList![0].faCategory), by \(familyMdl.familyList![0].createdByName)"
                    self.lblLocation.text = familyMdl.familyList![0].faRegion
                    self.aboutString = familyMdl.familyList![0].faIntro
                    self.shareURLString = familyMdl.familyList![0].f_link
                    self.urlTextString = familyMdl.familyList![0].f_text
                    
                    
                    self.isActive = familyMdl.familyList![0].isActive
                    self.isAdmin = familyMdl.familyList![0].isAdmin
                    self.linkType = familyMdl.familyList![0].linkFamily
                    self.updateAbout(data: self.aboutString, workedit: false)
                    self.famSettings = familyMdl.familyList![0].postCreate
                    self.announcementSettings = familyMdl.familyList![0].announcementCreate
                    self.postVisibility = familyMdl.familyList![0].postVisibility
                    
                    self.history_text = familyMdl.familyList![0].history_text
                    
                    let jsonvalue = JSON(arrayLiteral: familyMdl.familyList![0].history_images)
                    print(jsonvalue)
                    self.history_images = jsonvalue[0].arrayValue
                    
                    if familyMdl.familyList![0].faLogo.count != 0{
                        let url = "\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+familyMdl.familyList![0].faLogo.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                        let imgUrl = URL(string: url)
                        self.imageviewProfilePic.kf.indicatorType = .activity
                        self.profileImageUrl = familyMdl.familyList![0].faLogo
                        let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+imgUrl!.relativeString
                        let urlImg = URL(string: newUrlStr)
                        
                        self.imageviewProfilePic.kf.setImage(with: urlImg, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
                        
                    }else{
                        self.imageviewProfilePic.image = #imageLiteral(resourceName: "Family Logo")
                    }
                    
                    if familyMdl.familyList![0].faCoverPic.count != 0{
                        let url = "\(Helpers.imageURl)"+"\(BaseUrl.groupCover)"+familyMdl.familyList![0].faCoverPic.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                        
                        appDel.groupImageUrlPublic = url
                        let imgUrl = URL(string: url)
                        
                        if familyMdl.familyList![0].originalCover.count != 0{
                            let Originalurl = "\(Helpers.imageURl)"+"\(BaseUrl.group_origianl)"+familyMdl.familyList![0].originalCover
                            self.coverPicUrl = Originalurl
                        }
                        else{
                            self.coverPicUrl = url
                        }
                        
                        self.imgCover.kf.indicatorType = .activity
                        
                        let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=1600&height=1200&url="+imgUrl!.relativeString
                        let urlImg = URL(string: newUrlStr)
                        
                        self.imgCover.kf.setImage(with: urlImg, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
                    }else{
                        appDel.groupImageUrlPublic = ""
                        self.imgCover.image = #imageLiteral(resourceName: "family_default_cover.png")
                        
                    }
                    
                    print(familyMdl.familyList![0].isAdmin)
                    if self.familyArr![0].faCategory.lowercased() == "regular"{
                        //                        addMember.faCate = "regular"
                        self.faCate = "regular"
                    }
                    if familyMdl.familyList![0].isMembershipActive{
                        
                        let temp = familyMdl.familyList![0].membershipTo
                        if temp.isEmpty{
                            self.viewMembership.isHidden = true
                        }
                        else{
                            self.viewMembership.isHidden = false
                            if familyMdl.familyList![0].membership_period_type_id == 4{
                                self.lblMembershipData.text = "\(familyMdl.familyList![0].membershipName) Member, till Life time)"
                            }
                            else{
                                self.lblMembershipData.text = "\(familyMdl.familyList![0].membershipName) Member, till \(Helpers.convertTimeStampWithoutTime(dateAsString: familyMdl.familyList![0].membershipTo))"
                            }
                            
                            let paymentStatus =  familyMdl.familyList![0].membership_payment_status
                            if paymentStatus.lowercased() == "completed" || paymentStatus.lowercased() == "success" {
                                self.imgMembrshipStatus.isHidden = true
                            }
                            else{
                                self.imgMembrshipStatus.isHidden = false
                            }
                        }
                    }
                    else{
                        self.viewMembership.isHidden = true
                    }
                    
                    if familyMdl.familyList![0].isAdmin.lowercased() == "admin"{
                        self.btnEdit.isEnabled = true
                        self.noMembrView.isHidden = true
                        self.btnChangeCover.isEnabled = true
                        self.btnSettings.isEnabled = true
                        self.lblJoin.isHidden = true
                        self.imgEditCam.isHidden = false
                        self.imgEditCoverCam.isHidden = false
                        self.roleOFUser = "admin"
                        if familyMdl.familyList![0].isMembershipActive{
                           self.aboutArr = ["FEED","ANNOUNCEMENT","REQUEST","EVENTS","ALBUMS","ABOUT US","DOCUMENTS","LINKED FAMILIES","MEMBERSHIP","MEMBERS"]
                        }
                        else{
                            self.aboutArr = ["FEED","ANNOUNCEMENT","REQUEST","EVENTS","ALBUMS","ABOUT US","DOCUMENTS","LINKED FAMILIES","MEMBERS"]
                        }
                       
                        
                        self.collectionViewTitles.delegate = self
                        self.collectionViewTitles.dataSource = self
                        
                        self.aboutCollectnVew.reloadData()
                        
                        if self.ActiveTab == 5{
                            self.tableView.reloadData()
                        }
                        let stripeAcnt = familyMdl.familyList![0].stripe_account_id
                        if stripeAcnt.isEmpty{
                            
                        }
                        else{
                            self.getStripeAccountByID()
                        }
                        if !self.isAboutUsClicked{
                            if self.isFromCreate{
                                let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                                let addMember = storyboard.instantiateViewController(withIdentifier: "AddMembersNewViewController") as! AddMembersNewViewController
                                addMember.groupId = self.groupId
                                addMember.memberJoining = self.familyArr![0].memberApproval
                                addMember.isAdmin = self.isAdmin
                                addMember.link_type = self.linkType
                                addMember.famSettings = self.famSettings
                                addMember.announcementSet = self.announcementSettings
                                addMember.postId                    = self.familyArr![0].postCreate
                                addMember.announcementId            = self.familyArr![0].announcementCreate
                                addMember.isMembershipActive = self.familyArr![0].isMembershipActive
                                
                                if self.familyArr![0].faCategory.lowercased() == "regular"{
                                    addMember.faCate = "regular"
                                    self.faCate = "regular"
                                }
                                addMember.islinkable = self.familyArr![0].islinkable
                                self.navigationController?.pushViewController(addMember, animated: true)
                            }
                            else{
                                if self.isMemberNotification{
                                    self.onClickMembershipAction(self)
                                }
                                else{
                                    self.onClickActionTimeLine(self)
                                }
                            }
                        }
                    }
                    else if familyMdl.familyList![0].isAdmin.lowercased() == "not-member"{
                        
                        self.titleArray = ["members","posts","known members"]
                        self.collectionViewTitles.delegate = self
                        self.collectionViewTitles.dataSource = self
                        
                        self.btnEdit.isEnabled = true
                        self.btnChangeCover.isEnabled = true
                        self.imgSettings.isHidden = true
                        self.lblJoin.isHidden = false
                        self.roleOFUser = ""
                        self.ActiveTab = 2
        print(familyMdl.familyList![0])
                        if familyMdl.familyList![0].memberJoining == 1{
                            self.lblJoin.text = "Private"
                            self.btnSettings.isEnabled = false
                        }
                        else{
                            if familyMdl.familyList![0].invitationStatus == 1{
                                self.lblJoin.text = "Pending"
                                self.btnSettings.isEnabled = true
                            }
                            else{
                                self.lblJoin.text = "Join"
                                self.btnSettings.isEnabled = true
                            }
                        }
                        
                        if familyMdl.familyList![0].faType.lowercased() == "public"{
                            self.aboutArr = ["FEED","ANNOUNCEMENT","ABOUT US"]
                            self.noMembrView.isHidden = true
                             self.aboutCollectnVew.reloadData()
                            
                            if !self.isAboutUsClicked{
                                self.onClickActionTimeLine(self)
                            }
                            else{
                              
                            }
                        }
                        else{
                            self.noMembrView.isHidden = false
                        }
                        
                    }
                    else{
                        self.btnEdit.isEnabled = true
                        self.btnChangeCover.isEnabled = true
                        self.lblJoin.isHidden = true
                        self.noMembrView.isHidden = true
                        self.roleOFUser = "member"
                        //self.btnSettings.isEnabled = false
                        let stripeAcnt = familyMdl.familyList![0].stripe_account_id
                        if stripeAcnt.isEmpty{
                            
                        }
                        else{
                            self.getStripeAccountByID()
                        }
                        if !self.isAboutUsClicked{
                            print(self.isMemberNotification)
                            if self.isMemberNotification{
                                self.onClickMembershipAction(self)
                            }
                            else{
//                                if !appDel.fromFamilyDetailBack
//                                {
                                self.onClickActionTimeLine(self)
//                                }
//                                else
//                                {
//                                    self.lblJoin.isHidden = true
//                                                        self.imgSettings.isHidden = false
//
//                                }
                            }
                        }
                    }
                 
                    if self.familyArr![0].faCategory.lowercased() == "regular"{
                        self.faCate = "regular"
                    }
                    
                    self.memberCount = familyMdl.Memcount!.memberCount!
                    self.eventCount = familyMdl.Memcount!.eventCount!
                    self.knowCount = familyMdl.Memcount!.connections!
                    self.collectionViewTitles.reloadData()
                    if self.fromButtonAction
                    {
                        self.fromButtonAction = false
                     let stryboard = UIStoryboard.init(name: "MembershipMngmnt", bundle: nil)
                            let vc = stryboard.instantiateViewController(withIdentifier: "membershipPaymentViewController") as! membershipPaymentViewController
                            vc.modalPresentationStyle = .fullScreen
                        vc.familyArr = self.familyArr
                    //        vc.isStripeAccountValidate = isAccountValid

                            vc.delegate = self
                        
                            let navigationController = UINavigationController(rootViewController: vc)
                        self.present(navigationController, animated: true, completion: nil)
                    }
                }
                else{
                    self.displayAlertChoice(alertStr: "Oops! This content is no longer available.", title: "", completion: { (result) in
                        self.loadingDefaultView.removeFromSuperview()
                        
                        self.navigationController?.popViewController(animated: true)
                    })
                }
            }
            else{
                self.displayAlertChoice(alertStr: "Oops! This content is no longer available.", title: "", completion: { (result) in
                    self.loadingDefaultView.removeFromSuperview()
                    
                    self.navigationController?.popViewController(animated: true)
                })
            }
            
        }) { (error) in
            ActivityIndicatorView.hiding()
            self.displayAlertChoice(alertStr: "Oops! Something went wrong please try again later", title: "") { (result) in
                self.getFamilyDetails(groupId: self.groupId)
                self.loadingDefaultView.removeFromSuperview()
            }
            

        }
    }
    
    func groupFollow(url:String){
        APIServiceManager.callServer.familyFollowStatus(url: url, userId: UserDefaults.standard.value(forKey: "userId") as! String, gropId: groupId, success: { (responseMdl) in
            
            guard let follow = responseMdl as? requestSuccessModel else{
                return
            }
            ActivityIndicatorView.hiding()
            if follow.status_code == 200{
                self.isActive = follow.follow
            }
            
        }) { (error) in
            
        }
    }
    
    func stripeConnectAccount(){
        let parameter = ["group_id":self.groupId] as [String : Any]
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.stripe_oauth_link_generation(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                do {
                    let jsonData = JSON(response.data as Any)
                    print("!!!!@ --- \(jsonData)")
                    if response.statusCode == 200{
                        if let urlStr = jsonData["link"].string, !urlStr.isEmpty{
                            let url = URL(string: urlStr)
                            UIApplication.shared.open(url!)
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.stripeConnectAccount()
                        }
                    }
                } catch let err {
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
    func getStripeAccountByID(){
            // let gId = self.selectedIds[0] as! String
            
            let parameter = ["group_id":groupId] as [String : Any]
            print(parameter)
            ActivityIndicatorView.show("Loading....")
            networkProvider.request(.stripeGetaccountById(parameter: parameter), completion: {(result) in
                
                switch result {
                    
                case .success(let response):
                    do {
                        let jsonData = JSON(response.data as Any)
                        print("!!!!@ --- \(jsonData)")
                        if response.statusCode == 200{
                            self.accountArr = jsonData
                            if jsonData.count > 0{
                                if let payouts = jsonData["payouts_enabled"].bool, let charges = jsonData["charges_enabled"].bool, payouts && charges{
                                    self.isAccountValid = true
                                }
                                else{
                                   self.isAccountValid = false
                                }
    //                            self.tblItemList.reloadData()
                            }
                        }
                        else if response.statusCode == 401 {
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                self.getStripeAccountByID()
                            }
                        }
                        
                    } catch let err {
                        Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                    }
                    
                case .failure(let error):
                    Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                }
                ActivityIndicatorView.hiding()
            })
        }

    
    //MARK:- Button Actions
    
    @IBAction func onClickSettings(_ sender: Any) {
          if familyArr![0].isAdmin.lowercased() == "not-member"{
              ActivityIndicatorView.show("Please wait!")
              APIServiceManager.callServer.joinFamilyRequest(url: EndPoint.joinFamily, userId:  UserDefaults.standard.value(forKey: "userId") as! String, groupId: groupId, success: { (response) in
                  ActivityIndicatorView.hiding()
                  guard let result = response as? JoinResult else{
                      return
                  }
                  print(result)
                  if result.joinData?.status.lowercased() == "accepeted" || result.joinData?.status.lowercased() == "joined"{
                      self.lblJoin.isHidden = true
                      self.imgSettings.isHidden = false
                      self.getFamilyDetails(groupId: self.groupId)
                  }
                  else{
                      self.lblJoin.text = "Pending"
                  }
                  
              }) { (error) in
                  ActivityIndicatorView.hiding()
                  
              }
          }
          else{
              let storyboard = UIStoryboard.init(name: "second", bundle: nil)
              let addMember = storyboard.instantiateViewController(withIdentifier: "FamilySettingsViewController") as! FamilySettingsViewController
              addMember.famArr = self.familyArr
              addMember.callBackdelegate = self
              addMember.groupId = self.groupId
              addMember.delegate = self
             
              // addMember.isFromGroup = "1"
              self.navigationController?.pushViewController(addMember, animated: true)
          }
      }
    
    func updateFamilySetting(famArr : [familyModel]){
//        self.familyArr = famArr
//        print(self.familyArr)
        getFamilyDetails(groupId: groupId)
    }
    //
    @IBAction func onClickAddMembers(_ sender: Any) {
        
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let addMember = storyboard.instantiateViewController(withIdentifier: "AddMembersNewViewController") as! AddMembersNewViewController
        addMember.groupId = self.groupId
        if self.familyArr![0].faCategory.lowercased() == "regular"{
            addMember.faCate = "regular"
            faCate = "regular"
        }
        addMember.familyArr = self.familyArr
        addMember.memberJoining = self.familyArr![0].memberApproval
        addMember.isAdmin = self.isAdmin
        addMember.link_type = self.linkType
        addMember.islinkable = self.familyArr![0].islinkable
        self.navigationController?.pushViewController(addMember, animated: false)
        
    }
    @IBAction func onClickActionAboutUs(_ sender: Any) {
        //        setActiveButton(index: 0)
    }
    
    
    @IBAction func onClickActionEvent(_ sender: Any) {  //Call Events viewController
        let storyboard = UIStoryboard.init(name: "third", bundle: nil)
        let addMember = storyboard.instantiateViewController(withIdentifier: "EventListingViewController") as! EventListingViewController
        addMember.groupID = self.groupId
        addMember.isAdmin = self.isAdmin
        addMember.link_type = self.linkType
        addMember.islinkable = self.familyArr![0].islinkable
        
        addMember.facte = self.faCate
        
        self.navigationController?.pushViewController(addMember, animated: false)
    }
    
    
    @IBAction func onClickActionAlbum(_ sender: Any) {  //Call Announcement view controller
        
        let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
        let addMember = storyboard.instantiateViewController(withIdentifier: "AnnouncementInFamilyViewController") as! AnnouncementInFamilyViewController
        addMember.groupId = self.groupId
        addMember.isAdmin = self.isAdmin
        addMember.link_type = self.linkType
        addMember.islinkable = self.familyArr![0].islinkable
        addMember.facate = self.faCate
        addMember.postId                      = self.familyArr![0].postCreate
        addMember.announcementId              = self.familyArr![0].announcementCreate
        self.navigationController?.pushViewController(addMember, animated: false)
    }
    
    @IBAction func onClickActionTimeLine(_ sender: Any) { //Call Post listing View Controller
        
        let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
        let addMember = storyboard.instantiateViewController(withIdentifier: "PostedInGroupsViewController") as! PostedInGroupsViewController
        addMember.groupId = self.groupId
        addMember.isAdmin = self.isAdmin
        addMember.link_type = self.linkType
        addMember.islinkable = self.familyArr![0].islinkable
        addMember.facate = self.faCate
        addMember.famSettings = famSettings
        addMember.famArr = self.familyArr
        addMember.memberJoining = self.familyArr![0].memberJoining
        addMember.invitationStatus = self.familyArr![0].invitationStatus
        addMember.announcementSet = self.announcementSettings
        addMember.postId                    = self.familyArr![0].postCreate
        addMember.announcementId            = self.familyArr![0].announcementCreate
        addMember.isMembershipActive = self.familyArr![0].isMembershipActive
        
        self.navigationController?.pushViewController(addMember, animated: false)
    }
    
    @IBAction func onClickEditAction(_ sender: UIButton) {
        if sender.tag == 0{
            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            let addMember = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsEditAboutViewController") as! FamilyDetailsEditAboutViewController
            addMember.delegate = self
            addMember.aboutString = aboutString
            addMember.groupId = self.groupId
            self.navigationController?.pushViewController(addMember, animated: true)
        }
        else{
            let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
            let addMember = storyboard.instantiateViewController(withIdentifier: "FamilyEditHistoryViewController") as! FamilyEditHistoryViewController
            addMember.delegate = self
            addMember.history_text = self.history_text
            addMember.history_images = self.history_images
            addMember.groupId = self.groupId
            self.navigationController?.pushViewController(addMember, animated: true)
        }
    }
    
    @IBAction func imageAction(_ sender: UIButton) {
        if self.history_images.count != 0{
            let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsPreviewViewController") as! AnnouncementsPreviewViewController
            vc.attachments = self.history_images
            vc.isFromFamilyHistory = true
            self.navigationController?.addFadeAnimation()
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
    }
    
    @IBAction func onClickBackButton(_ sender: Any) {
        if isFromCreate{
            appDel.groupNamePublic = ""
            self.navigationController?.popToViewController(ofClass: FamiliesTabViewController.self)
            //             self.navigationController?.popToRootViewController(animated: true)
            self.tabBarController?.selectedIndex = 2
        }
        else if isFromNotification{
            self.navigationController?.popToViewController(ofClass: FamiliesTabViewController.self)
        }
//        else if isFromFamilyList && appDel.isFromLinkedFam{
//            self.navigationController?.popToViewController(ofClass: LinkedFamiliesListViewController.self)
//        }
        else if isFromFamilyList{
            self.navigationController?.popViewController(animated: true)
        }
        else if isFromSearchTab{
            self.navigationController?.popToViewController(ofClass: SearchTabViewController.self)
        }
        else if isFromBanner{
            self.navigationController?.popToViewController(ofClass: HomeTabViewController.self)
        }
        else{
            appDel.groupNamePublic = ""
            self.navigationController?.popToViewController(ofClass: FamiliesTabViewController.self)
            //             self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    @IBAction func onClickEditDetailsAction(_ sender: Any) {
        
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: "ChangePicturesViewController") as! ChangePicturesViewController
        // vc.isFrom = "Family"
        vc.delegate = self
        vc.isCoverOrProfilePic = "logo"
        vc.ids           = "\(self.familyArr![0].faId)"
        let url = "\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+familyArr![0].faLogo
        print(url)
        vc.imageUrl      = url
        vc.isFrom        = "group"
        vc.inFor         = "logo"
        vc.isAdmin = self.isAdmin
        vc.groupId = self.groupId
        vc.isAdmin = self.roleOFUser
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func EditFamilyDetails() {
        getFamilyDetails(groupId: groupId)
    }
    
    @IBAction func onClickMoreAction(_ sender: Any) {
        arr = ["Members","Linked Families","Albums","Documents"]
        
        self.showActionSheet(titleArr: self.arr as! NSArray, title: "Choose option") { (index) in
            if index == 3{
                let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "DocumentListingViewController") as! DocumentListingViewController
                addMember.groupID = self.groupId
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.linkType
                addMember.islinkable = self.familyArr![0].islinkable
                addMember.isFromdocument = true
                addMember.facate = self.faCate
                
                self.navigationController?.pushViewController(addMember, animated: false)
                //setActiveButton(index: 2)
            }
            else if index == 100{
            }
            else if index == 2{
                
                let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "AlbumListingViewController") as! AlbumListingViewController
                addMember.groupID = self.groupId
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.linkType
                addMember.islinkable = self.familyArr![0].islinkable
                addMember.facate = self.faCate
                
                self.navigationController?.pushViewController(addMember, animated: false)
            }
            else if index == 0{
                
                let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "AddMembersNewViewController") as! AddMembersNewViewController
                addMember.groupId = self.groupId
                addMember.memberJoining = self.familyArr![0].memberApproval
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.linkType
                
                if self.familyArr![0].faCategory.lowercased() == "regular"{
                    addMember.faCate = "regular"
                    self.faCate = "regular"
                }
                addMember.islinkable = self.familyArr![0].islinkable
                self.navigationController?.pushViewController(addMember, animated: false)
            }
            else{
                let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "LinkedFamiliesListViewController") as! LinkedFamiliesListViewController
                
                addMember.groupID = self.groupId
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.linkType
                addMember.islinkable = self.familyArr![0].islinkable
                addMember.facate = self.faCate
                self.navigationController?.pushViewController(addMember, animated: false)
            }
        }
    }
    
    @IBAction func onClickEditUrl(_ sender: UIButton) {
        self.urlEditEnable = true
        tableView.beginUpdates()
        tableView.reloadRows(at: [IndexPath.init(row: 0, section: 2)], with: .none)
        tableView.endUpdates()
    }
    
    @IBAction func onClickUrlShare(_ sender: Any) {
        Helpers.showActivityViewcontroller(url:shareURLString, VC: self)
    }
    
    @IBAction func OnClick_SaveUrl(_ sender: Any) {
    }
    
    @IBAction func onClickMembershipAction(_ sender: Any) {
        let stryboard = UIStoryboard.init(name: "MembershipMngmnt", bundle: nil)
        let vc = stryboard.instantiateViewController(withIdentifier: "membershipPaymentViewController") as! membershipPaymentViewController
        vc.modalPresentationStyle = .fullScreen
        vc.familyArr = familyArr
//        vc.isStripeAccountValidate = isAccountValid

        vc.delegate = self
    
        let navigationController = UINavigationController(rootViewController: vc)
        present(navigationController, animated: true, completion: nil)
    }
    
    
    //MARK: - onClickChangeCoverPic
    @IBAction func onClickChangeCoverPic(_ sender: Any) {
        
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: "ChangePicturesViewController") as! ChangePicturesViewController
        vc.delegate = self
        vc.isCoverOrProfilePic = "CoverPic"
        vc.ids           = "\(self.familyArr![0].faId)"
        
        vc.imageUrl      = coverPicUrl
        vc.isFrom        = "group"
        vc.inFor         = "cover"
        vc.isAdmin = isAdmin
        vc.groupId = self.groupId
        vc.isAdmin = self.roleOFUser
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- custom delegate
    func changeProPic() {
        isAboutUsClicked = true
        getFamilyDetails(groupId: groupId)
    }
    
    func updateFamilyDetailsFromSettings() {
        getFamilyDetails(groupId: groupId)
        
    }
    
    func updateMembershipPayment(){
        isAboutUsClicked = true
        isFromDelegate = true
        getFamilyDetails(groupId: groupId)
    }
    
    //MARK:- Custom Actions
    func setActiveButton(index:Int){
        switch index {
        case 0:  //POST ---- Change events to post
            
            labelTimeLine.textColor = UIColor.black
            labelAlbum.textColor = UIColor.black
            labelEvent.textColor = UIColor.black
            labelAbout.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            
            viewtimelineActive.isHidden = true
            viewAlbumActive.isHidden = true
            viewEventActive.isHidden = true
            viewAboutActive.isHidden = false
            ActiveTab = 0
            
            break
        case 1: //Announcement ------------ Change members to  announcement
            
            labelTimeLine.textColor = UIColor.black
            labelAlbum.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            labelEvent.textColor = UIColor.black
            labelAbout.textColor = UIColor.black
            
            viewtimelineActive.isHidden = true
            viewAlbumActive.isHidden = false
            viewEventActive.isHidden = true
            viewAboutActive.isHidden = true
            ActiveTab = 1
            
            break
        case 2: //Albums
            
            labelTimeLine.textColor = UIColor.black
            labelAlbum.textColor = UIColor.black
            labelEvent.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            labelAbout.textColor = UIColor.black
            
            viewtimelineActive.isHidden = true
            viewAlbumActive.isHidden = true
            viewEventActive.isHidden = false
            viewAboutActive.isHidden = true
            ActiveTab = 2
            
            break
        case 3: //About Us
            
            labelTimeLine.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            labelAlbum.textColor = UIColor.black
            labelEvent.textColor = UIColor.black
            labelAbout.textColor = UIColor.black
            viewtimelineActive.isHidden = false
            viewAlbumActive.isHidden = true
            viewEventActive.isHidden = true
            viewAboutActive.isHidden = true
            ActiveTab = 5
            
            break
        default:
            break
        }
        tableView.reloadData()
    }
    
    func setFloatingButton(){
        btnFloat.frame = CGRect(x: UIScreen.main.bounds.size.width - 75, y: UIScreen.main.bounds.size.height - 150, width: 50, height: 50)
        btnFloat.setTitle(": :", for: .normal)
        btnFloat.setTitleColor(UIColor.black, for: .normal)
        btnFloat.backgroundColor = UIColor.groupTableViewBackground
        btnFloat.clipsToBounds = true
        btnFloat.layer.cornerRadius = 25
        //        btnFloat.addTarget(self,action: #selector(onclickFloatingAction), for: UIControl.Event.touchUpInside)
        if let window = UIApplication.shared.keyWindow {
            window.addSubview(btnFloat)
        }
        
    }
    
    
    
    
    //MARK:- collectionView Delegates
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 2{
            return aboutArr.count
        }
        else{
            return titleArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == 2{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AboutFamilyCollectionViewCell", for: indexPath as IndexPath) as! AboutFamilyCollectionViewCell
            cell.lblName.text = aboutArr[indexPath.item]
            if indexPath.row == ActiveTab{
                cell.lblName.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                cell.lblName.font = .boldSystemFont(ofSize: 15)
                cell.imgUnderline.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                
            }
            else{
                cell.lblName.textColor = .black
                cell.lblName.font = .systemFont(ofSize: 14)
                cell.imgUnderline.backgroundColor = .white
            }
            return cell
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FamilyDetailsTitlesCollectionViewCell", for: indexPath as IndexPath) as! FamilyDetailsTitlesCollectionViewCell
            cell.lblTitle.text = titleArray[indexPath.item]
            if indexPath.row == 0{
                cell.lblCount.text = "\(memberCount)"
            }
            else if indexPath.row == 1{
                cell.lblCount.text = "\(eventCount)"
            }
            else {
                cell.lblCount.text = "\(knowCount)"
            }
            
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 2{
            
            if self.familyArr![0].isAdmin.lowercased() == "not-member"{
                if indexPath.row == 0{
                    let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
                    let addMember = storyboard.instantiateViewController(withIdentifier: "PostedInGroupsViewController") as! PostedInGroupsViewController
                    addMember.groupId = self.groupId
                    addMember.isAdmin = self.isAdmin
                    addMember.link_type = self.linkType
                    addMember.islinkable = self.familyArr![0].islinkable
                    addMember.facate = self.faCate
                    addMember.famSettings = famSettings
                    addMember.famArr = self.familyArr
                    addMember.memberJoining = self.familyArr![0].memberJoining
                    addMember.invitationStatus = self.familyArr![0].invitationStatus
                    addMember.announcementSet = self.announcementSettings
                    addMember.postId                    = self.familyArr![0].postCreate
                    addMember.announcementId            = self.familyArr![0].announcementCreate
                    addMember.isMembershipActive = self.familyArr![0].isMembershipActive
                    addMember.memberJoining = self.familyArr![0].memberJoining
                    addMember.invitationStatus = self.familyArr![0].invitationStatus
                    self.navigationController?.pushViewController(addMember, animated: true)
                }
                else if indexPath.row == 1{
                    let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
                    let addMember = storyboard.instantiateViewController(withIdentifier: "AnnouncementInFamilyViewController") as! AnnouncementInFamilyViewController
                    addMember.groupId = self.groupId
                    addMember.isAdmin = self.isAdmin
                    addMember.link_type = self.linkType
                    addMember.islinkable = self.familyArr![0].islinkable
                    addMember.facate = self.faCate
                    addMember.famSettings = famSettings
                    addMember.famArr = self.familyArr
                    addMember.memberJoining = self.familyArr![0].memberJoining
                    addMember.invitationStatus = self.familyArr![0].invitationStatus
                    addMember.memberJoining = self.familyArr![0].memberJoining
                    addMember.invitationStatus = self.familyArr![0].invitationStatus
                    addMember.announcementSet = self.announcementSettings
                    addMember.postId                      = self.familyArr![0].postCreate
                    addMember.announcementId              = self.familyArr![0].announcementCreate
                    self.navigationController?.pushViewController(addMember, animated: true)
                }
                else{
                    setActiveButton(index: 2)
                }
            }
            else{
                if indexPath.row == 0{
                    let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
                    let addMember = storyboard.instantiateViewController(withIdentifier: "PostedInGroupsViewController") as! PostedInGroupsViewController
                    addMember.groupId = self.groupId
                    addMember.isAdmin = self.isAdmin
                    addMember.link_type = self.linkType
                    addMember.islinkable = self.familyArr![0].islinkable
                    addMember.facate = self.faCate
                    addMember.famSettings = famSettings
                    addMember.famArr = self.familyArr
                    addMember.memberJoining = self.familyArr![0].memberJoining
                    addMember.invitationStatus = self.familyArr![0].invitationStatus
                    addMember.memberJoining = self.familyArr![0].memberJoining
                    addMember.invitationStatus = self.familyArr![0].invitationStatus
                    addMember.announcementSet = self.announcementSettings
                    addMember.postId                    = self.familyArr![0].postCreate
                    addMember.announcementId            = self.familyArr![0].announcementCreate
                    addMember.isMembershipActive = self.familyArr![0].isMembershipActive
                    
                    self.navigationController?.pushViewController(addMember, animated: true)
                }
                else if indexPath.row == 1{
                    let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
                    let addMember = storyboard.instantiateViewController(withIdentifier: "AnnouncementInFamilyViewController") as! AnnouncementInFamilyViewController
                    addMember.groupId = self.groupId
                    addMember.isAdmin = self.isAdmin
                    addMember.link_type = self.linkType
                    addMember.islinkable = self.familyArr![0].islinkable
                    addMember.facate = self.faCate
                    addMember.famSettings = famSettings
                    addMember.famArr = self.familyArr
                    addMember.memberJoining = self.familyArr![0].memberJoining
                    addMember.invitationStatus = self.familyArr![0].invitationStatus
                    addMember.memberJoining = self.familyArr![0].memberJoining
                    addMember.invitationStatus = self.familyArr![0].invitationStatus
                    addMember.announcementSet = self.announcementSettings
                    addMember.postId                      = self.familyArr![0].postCreate
                    addMember.announcementId              = self.familyArr![0].announcementCreate
                    addMember.isMembershipActive = self.familyArr![0].isMembershipActive
                    self.navigationController?.pushViewController(addMember, animated: true)
                }
                else if indexPath.row == 2{
                    let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
                    let addMember = storyboard.instantiateViewController(withIdentifier: "RequestInFamilyViewController") as! RequestInFamilyViewController
                    addMember.groupID = self.groupId
                    addMember.isAdmin = self.isAdmin
                    addMember.link_type = self.linkType
                    addMember.islinkable = self.familyArr![0].islinkable
                    addMember.facate = self.faCate
                    addMember.famSettings = famSettings
                    addMember.famArr = self.familyArr
                    addMember.memberJoining = self.familyArr![0].memberJoining
                    addMember.invitationStatus = self.familyArr![0].invitationStatus
                    addMember.announcementSet = self.announcementSettings
                    addMember.postId                      = self.familyArr![0].postCreate
                    addMember.announcementId              = self.familyArr![0].announcementCreate
                    addMember.isMembershipActive = self.familyArr![0].isMembershipActive
                    self.navigationController?.pushViewController(addMember, animated: true)
                }
                else if indexPath.row == 3{ // Events
                    let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                    let addMember = storyboard.instantiateViewController(withIdentifier: "EventListingViewController") as! EventListingViewController
                    addMember.groupID = self.groupId
                    addMember.isAdmin = self.isAdmin
                    addMember.link_type = self.linkType
                    addMember.islinkable = self.familyArr![0].islinkable
                    addMember.famSettings = famSettings
                    addMember.facte = self.faCate
                    addMember.announcementSet = self.announcementSettings
                    addMember.famArr = self.familyArr
                    addMember.memberJoining = self.familyArr![0].memberJoining
                    addMember.invitationStatus = self.familyArr![0].invitationStatus
                    addMember.postId                    = self.familyArr![0].postCreate
                    addMember.announcementId            = self.familyArr![0].announcementCreate
                    addMember.isMembershipActive = self.familyArr![0].isMembershipActive
                    
                    self.navigationController?.pushViewController(addMember, animated: true)
                }
                else if indexPath.row == 5{
                    setActiveButton(index: 5)
                }
                else if indexPath.row == 8{
                    if familyArr![0].isMembershipActive{
                        if self.roleOFUser.lowercased() == "admin"{
                            let storyboard = UIStoryboard.init(name: "MembershipMngmnt", bundle: nil)
                            let addMember = storyboard.instantiateViewController(withIdentifier: "MemebrshipDashboardViewController") as! MemebrshipDashboardViewController
                            addMember.groupID = self.groupId
                            addMember.isAdmin = self.isAdmin
                            addMember.link_type = self.linkType
                            addMember.islinkable = self.familyArr![0].islinkable
                            //                        addMember.isFromdocument = true
                            addMember.facate = self.faCate
                            addMember.famSettings = famSettings
                            addMember.famArr = self.familyArr
                            addMember.memberJoining = self.familyArr![0].memberJoining
                            addMember.invitationStatus = self.familyArr![0].invitationStatus
                            addMember.announcementSet = self.announcementSettings
                            addMember.postId                    = self.familyArr![0].postCreate
                            addMember.announcementId            = self.familyArr![0].announcementCreate
                            addMember.isMembershipActive = self.familyArr![0].isMembershipActive
                            
                            self.navigationController?.pushViewController(addMember, animated: true)
                        }
                        else{
                            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                            let addMember = storyboard.instantiateViewController(withIdentifier: "AddMembersNewViewController") as! AddMembersNewViewController
                            addMember.groupId = self.groupId
                            addMember.memberJoining = self.familyArr![0].memberApproval
                            addMember.isAdmin = self.isAdmin
                            addMember.link_type = self.linkType
                            addMember.famSettings = famSettings
                            addMember.announcementSet = self.announcementSettings
                            addMember.famArr = self.familyArr
                            addMember.postId                    = self.familyArr![0].postCreate
                            addMember.announcementId            = self.familyArr![0].announcementCreate
                            addMember.isMembershipActive = self.familyArr![0].isMembershipActive
                            addMember.memberJoiningStatus = self.familyArr![0].memberJoining
                            addMember.invitationStatus = self.familyArr![0].invitationStatus
                            if self.familyArr![0].faCategory.lowercased() == "regular"{
                                addMember.faCate = "regular"
                                self.faCate = "regular"
                            }
                            addMember.islinkable = self.familyArr![0].islinkable
                            self.navigationController?.pushViewController(addMember, animated: true)
                        }
                    }
                    else{
                        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                        let addMember = storyboard.instantiateViewController(withIdentifier: "AddMembersNewViewController") as! AddMembersNewViewController
                        addMember.groupId = self.groupId
                        addMember.memberJoining = self.familyArr![0].memberApproval
                        addMember.isAdmin = self.isAdmin
                        addMember.link_type = self.linkType
                        addMember.famSettings = famSettings
                        addMember.famArr = self.familyArr
                        addMember.announcementSet = self.announcementSettings
                        addMember.postId                    = self.familyArr![0].postCreate
                        addMember.announcementId            = self.familyArr![0].announcementCreate
                        addMember.isMembershipActive = self.familyArr![0].isMembershipActive
                        addMember.memberJoiningStatus = self.familyArr![0].memberJoining
                        addMember.invitationStatus = self.familyArr![0].invitationStatus

                        
                        if self.familyArr![0].faCategory.lowercased() == "regular"{
                            addMember.faCate = "regular"
                            self.faCate = "regular"
                        }
                        addMember.islinkable = self.familyArr![0].islinkable
                        self.navigationController?.pushViewController(addMember, animated: true)
                    }
                }
                else if indexPath.row == 9{ // TO Member tab if is MembershipActive is true
                    let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                    let addMember = storyboard.instantiateViewController(withIdentifier: "AddMembersNewViewController") as! AddMembersNewViewController
                    addMember.groupId = self.groupId
                    addMember.memberJoining = self.familyArr![0].memberApproval
                    addMember.isAdmin = self.isAdmin
                    addMember.link_type = self.linkType
                    addMember.famSettings = famSettings
                    addMember.announcementSet = self.announcementSettings
                    addMember.famArr = self.familyArr
                    addMember.postId                    = self.familyArr![0].postCreate
                    addMember.announcementId            = self.familyArr![0].announcementCreate
                    addMember.isMembershipActive = self.familyArr![0].isMembershipActive
                    addMember.memberJoiningStatus = self.familyArr![0].memberJoining
                    addMember.invitationStatus = self.familyArr![0].invitationStatus

                    
                    if self.familyArr![0].faCategory.lowercased() == "regular"{
                        addMember.faCate = "regular"
                        self.faCate = "regular"
                    }
                    addMember.islinkable = self.familyArr![0].islinkable
                    self.navigationController?.pushViewController(addMember, animated: true)
                }
                else if indexPath.row == 4{
                    let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                    let addMember = storyboard.instantiateViewController(withIdentifier: "AlbumListingViewController") as! AlbumListingViewController
                    addMember.groupID = self.groupId
                    addMember.isAdmin = self.isAdmin
                    addMember.link_type = self.linkType
                    addMember.islinkable = self.familyArr![0].islinkable
                    addMember.facate = self.faCate
                    addMember.famSettings = famSettings
                    addMember.famArr = self.familyArr
                    addMember.memberJoining = self.familyArr![0].memberJoining
                    addMember.invitationStatus = self.familyArr![0].invitationStatus
                    addMember.announcementSet = self.announcementSettings
                    addMember.postId                    = self.familyArr![0].postCreate
                    addMember.announcementId            = self.familyArr![0].announcementCreate
                    addMember.isMembershipActive = self.familyArr![0].isMembershipActive
                    
                    self.navigationController?.pushViewController(addMember, animated: true)
                }
                else if indexPath.row == 6{
                    let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                    let addMember = storyboard.instantiateViewController(withIdentifier: "DocumentListingViewController") as! DocumentListingViewController
                    addMember.groupID = self.groupId
                    addMember.isAdmin = self.isAdmin
                    addMember.link_type = self.linkType
                    addMember.islinkable = self.familyArr![0].islinkable
                    addMember.isFromdocument = true
                    addMember.facate = self.faCate
                    addMember.famSettings = famSettings
                    addMember.famArr = self.familyArr
                    addMember.memberJoining = self.familyArr![0].memberJoining
                    addMember.invitationStatus = self.familyArr![0].invitationStatus
                    addMember.announcementSet = self.announcementSettings
                    addMember.postId                    = self.familyArr![0].postCreate
                    addMember.announcementId            = self.familyArr![0].announcementCreate
                    addMember.isMembershipActive = self.familyArr![0].isMembershipActive
                    
                    self.navigationController?.pushViewController(addMember, animated: true)
                    
                }
                else{
                    let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                    let addMember = storyboard.instantiateViewController(withIdentifier: "LinkedFamiliesListViewController") as! LinkedFamiliesListViewController
                    
                    addMember.groupID = self.groupId
                    addMember.isAdmin = self.isAdmin
                    addMember.link_type = self.linkType
                    addMember.islinkable = self.familyArr![0].islinkable
                    addMember.facate = self.faCate
                    addMember.famSettings = famSettings
                    addMember.famArr = self.familyArr
                    addMember.memberJoining = self.familyArr![0].memberJoining
                    addMember.invitationStatus = self.familyArr![0].invitationStatus
                    addMember.announcementSet = self.announcementSettings
                    addMember.postId                    = self.familyArr![0].postCreate
                    addMember.announcementId            = self.familyArr![0].announcementCreate
                    addMember.isMembershipActive = self.familyArr![0].isMembershipActive
                    
                    self.navigationController?.pushViewController(addMember, animated: true)
                }
                
            }
        }
        else{
            if familyArr![0].isAdmin.lowercased() == "not-member"{
                if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2{
                    if indexPath.row == 0 {
                        Helpers.showAlertDialog(message: "You need to be a member to view this family!", target: self)
                    }
                    else if indexPath.row == 2{
                        if knowCount > 0{
                            let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "UserFamilyListViewController") as! UserFamilyListViewController
                            vc.familyID = self.groupId
                            vc.fromFamilies = true
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                    else if indexPath.row == 1{
                        if self.familyArr![0].faType.lowercased() == "public"{
                            if eventCount > 0{
                                let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
                                let addMember = storyboard.instantiateViewController(withIdentifier: "PostedInGroupsViewController") as! PostedInGroupsViewController
                                addMember.groupId = self.groupId
                                addMember.isAdmin = self.isAdmin
                                addMember.link_type = self.linkType
                                addMember.islinkable = self.familyArr![0].islinkable
                                addMember.facate = self.faCate
                                addMember.famSettings = famSettings
                                addMember.famArr = self.familyArr
                                addMember.memberJoining = self.familyArr![0].memberJoining
                                addMember.invitationStatus = self.familyArr![0].invitationStatus
                                addMember.announcementSet = self.announcementSettings
                                addMember.postId                    = self.familyArr![0].postCreate
                                addMember.announcementId            = self.familyArr![0].announcementCreate
                                addMember.isMembershipActive = self.familyArr![0].isMembershipActive
                                self.navigationController?.pushViewController(addMember, animated: false)
//                                Helpers.showAlertDialog(message: "You need to be a member to view this family!", target: self)
                            }
                        }
                        else{
                            Helpers.showAlertDialog(message: "You need to be a member to view this family!", target: self)
                        }
                    }
                    else{
                        Helpers.showAlertDialog(message: "You need to be a member to view this family!", target: self)
                    }
                }
                else{
                    let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "UserFamilyListViewController") as! UserFamilyListViewController
                    vc.familyID = self.groupId
                    vc.fromFamilies = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else{
                if indexPath.row == 0{
                    let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                    let addMember = storyboard.instantiateViewController(withIdentifier: "AddMembersNewViewController") as! AddMembersNewViewController
                    addMember.groupId = self.groupId
                    if self.familyArr![0].faCategory.lowercased() == "regular"{
                        addMember.faCate = "regular"
                        faCate = "regular"
                    }
                    addMember.familyArr = self.familyArr
                    addMember.groupId = self.groupId
                    addMember.memberJoining = self.familyArr![0].memberApproval
                    addMember.isAdmin = self.isAdmin
                    addMember.link_type = self.linkType
                    addMember.famSettings = self.famSettings
                    addMember.announcementSet = self.announcementSettings
                    addMember.famArr = self.familyArr
                    addMember.memberJoiningStatus = self.familyArr![0].memberJoining
                    addMember.invitationStatus = self.familyArr![0].invitationStatus
                    addMember.postId                    = self.familyArr![0].postCreate
                    addMember.announcementId            = self.familyArr![0].announcementCreate
                    addMember.isMembershipActive = self.familyArr![0].isMembershipActive
                    self.navigationController?.pushViewController(addMember, animated: false)
                }
                else if indexPath.row == 1{
                    if eventCount > 0{
                        let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
                        let addMember = storyboard.instantiateViewController(withIdentifier: "PostedInGroupsViewController") as! PostedInGroupsViewController
                        addMember.groupId = self.groupId
                        addMember.isAdmin = self.isAdmin
                        addMember.link_type = self.linkType
                        addMember.islinkable = self.familyArr![0].islinkable
                        addMember.facate = self.faCate
                        addMember.famSettings = famSettings
                        addMember.famArr = self.familyArr
                        addMember.memberJoining = self.familyArr![0].memberJoining
                        addMember.invitationStatus = self.familyArr![0].invitationStatus
                        addMember.announcementSet = self.announcementSettings
                        addMember.postId                    = self.familyArr![0].postCreate
                        addMember.announcementId            = self.familyArr![0].announcementCreate
                        addMember.isMembershipActive = self.familyArr![0].isMembershipActive
                        self.navigationController?.pushViewController(addMember, animated: false)
                    }
                }
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 2{
            let w = aboutArr[indexPath.row].size(withAttributes: nil)
            return CGSize(width: w.width, height: 60)
        }
        else{
            let numberOfCellInRow : Int = self.titleArray.count
            let padding : Int = 0
            let collectionCellWidth : CGFloat = (self.collectionViewTitles.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
            return CGSize(width: collectionCellWidth , height: 50)
        }
    }
    
    //MARK:- tableview Delegates
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        switch ActiveTab {
        case 5:
            return 2
        case 2:
            return 2
        default:
            return 0
        }
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch ActiveTab {
        case 5:
            return 1
        case 2:
            return 1
        default:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.tableView.separatorStyle = .none
        self.tableView.allowsSelection = false
        if indexPath.section == 0{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "FamilyDetailsAboutTableViewCell", for: indexPath) as! FamilyDetailsAboutTableViewCell
            
            cell.textviewAbout.text = aboutString
            cell.widthOfImgHistory.constant = 0
            cell.texttoLeftSpacing.constant = 0
            cell.imgViewOfHistory.isHidden = true
            cell.btnHistoryImage.isHidden = true
            
            let padding = cell.textviewAbout.textContainer.lineFragmentPadding
            cell.textviewAbout.textContainerInset =  UIEdgeInsets(top: 0, left: -padding, bottom: 0, right: -padding)
            
            
            cell.lblCellHeading.text = "Introduction"
            cell.btnEdit.tag = 0
            if self.roleOFUser == "admin"{
                cell.editTextVw2.isHidden = false
            }
            else{
                cell.editTextVw2.isHidden = true
            }
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FamilyDetailsAboutTableViewCell", for: indexPath) as! FamilyDetailsAboutTableViewCell
            cell.lblCellHeading.text = "History"
            cell.btnEdit.tag = 1
            cell.textviewAbout.text = self.history_text
            
            let padding = cell.textviewAbout.textContainer.lineFragmentPadding
            cell.textviewAbout.textContainerInset =  UIEdgeInsets(top: 0, left: -padding, bottom: 0, right: -padding)
            
            
            if self.history_images.count != 0
            {
                cell.widthOfImgHistory.constant = 50
                
                cell.imgViewOfHistory.isHidden = false
                cell.btnHistoryImage.isHidden = false
                print(self.history_images)
                let dict = self.history_images[0].dictionaryValue
                print(dict)
                
                let history_pic = self.history_images[0]["filename"].stringValue
                if history_pic != ""{
                    let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+"\(Helpers.imageURl)"+"\(BaseUrl.history_images)"+history_pic
                    let imgUrl = URL(string: temp)
                    cell.imgViewOfHistory.kf.indicatorType  = .activity
                    cell.imgViewOfHistory.kf.setImage(with: imgUrl, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
                }
            }
            else{
                cell.widthOfImgHistory.constant = 0
                // cell.texttoLeftSpacing.constant = 0
                cell.imgViewOfHistory.isHidden = true
            }
            if self.roleOFUser == "admin"{
                cell.editTextVw2.isHidden = false
            }
            else{
                cell.editTextVw2.isHidden = true
            }
            return cell
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return UITableView.automaticDimension
        }
        else{
            if self.history_images.count != 0{
                //                return 150
                
                return UITableView.automaticDimension
                
            }
            else{
                return UITableView.automaticDimension
            }
        }
    }
    
    func applyLeftImage(_ image: UIImage,textview:UITextView) {
        let icn : UIImage = image
        let imageView = UIImageView(image: icn)
        imageView.frame = CGRect(x: 0, y: 2.0, width: icn.size.width + 20, height: icn.size.height)
        imageView.contentMode = UIView.ContentMode.center
        //Where self = UItextView
        textview.addSubview(imageView)
        textview.textContainerInset = UIEdgeInsets(top: 2.0, left: icn.size.width + 10.0 , bottom: 2.0, right: 2.0)
    }
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 2{
            self.apiForFamilyURLavailability(text:textField.text!)
        }
    }
    
    func apiForFamilyURLavailability(text:String) {
        let parameter = ["f_text":text] as [String : Any]
        print(parameter)
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.familyURL_availabilitychecking(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    print(jsonData["status"])
                    print(response.statusCode)
                    if response.statusCode == 200{
                        //                        self.imgOfUrlAvailability.isHidden = false
                        //                        self.imgOfUrlAvailability.image = #imageLiteral(resourceName: "Green_tick.png")
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.apiForFamilyURLavailability(text: text)
                        }
                    }
                    else{
                        //                        self.imgOfUrlAvailability.isHidden = true
                        
                        Helpers.showAlertDialog(message: "url already exist", target: self)
                        //                        self.txtEventPage.text = ""
                    }
                    
                    
                    
                } catch let err {
                    
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
    func apiForFamilyUrlUpdate() {
        
        let cell = tableView.cellForRow(at: IndexPath.init(row: 0, section: 2)) as! FamilyUrlTableViewCell
        var parameter = [String:Any]()
        
        parameter = ["id":groupId,"f_text":cell.txtFamUrl.text!,"user_id":UserDefaults.standard.value(forKey: "userId") as! String
            ] as [String : Any]
        
        print(parameter)
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.updateFamilyWithHistory(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    print(jsonData["status"])
                    print(response.statusCode)
                    if response.statusCode == 200
                    {
                        //
                        let AlertCus = CustomAlertVC.show(in: self, title: "Success", message: "Successfully Updated Family", image:UIImage.init(named: "Green_tick")!)
                        AlertCus.addAction(CPAlertAction.init(title: "OK", type: .normal, handler: {
                            self.navigationController?.popViewController(animated: true)
                            
                        }))
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.apiForFamilyUrlUpdate()
                        }
                    }
                    else{
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                } catch let err {
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
}
