//
//  stickyPostCell.swift
//  familheey
//
//  Created by Innovation Incubator on 12/08/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit

class stickyPostCell: UICollectionViewCell {
    @IBOutlet weak var viewOfBG: UIView!
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var lblDescription: UILabel!

    @IBOutlet weak var imgPlayIcon: UIImageView!
    @IBOutlet weak var heightOfImg: NSLayoutConstraint!
    @IBOutlet weak var widthOfImg: NSLayoutConstraint!
    
    @IBOutlet weak var btnUnsticky: UIButton!
}
