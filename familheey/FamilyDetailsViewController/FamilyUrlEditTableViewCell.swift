//
//  FamilyUrlEditTableViewCell.swift
//  familheey
//
//  Created by Innovation Incubator on 13/12/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class FamilyUrlEditTableViewCell: UITableViewCell {

    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var imgShare: UIImageView!
    @IBOutlet weak var lblUrl: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var imgEdit: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
