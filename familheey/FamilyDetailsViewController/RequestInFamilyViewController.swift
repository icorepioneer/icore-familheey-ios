//
//  RequestInFamilyViewController.swift
//  familheey
//
//  Created by familheey on 03/04/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON
import Kingfisher

class RequestInFamilyViewController: UIViewController {
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var aboutClctnView: UICollectionView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearchReset: UIButton!
    @IBOutlet weak var viewOfNoData: UIView!
    @IBOutlet weak var lblNoData: UILabel!
    
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var createRequestbutton: UIButton!
    
    
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    var collectionViewLayout = UICollectionViewFlowLayout()
    
    var aboutArr = ["FEED","ANNOUNCEMENT","REQUEST","EVENTS","ALBUMS","ABOUT US","DOCUMENTS","LINKED FAMILIES","MEMBERS"]
    var groupID = ""
    var postoptionsTittle = [""]
    var link_type = 0
    var isAdmin = ""
    var facate = ""
    var islinkable = 0
    var famSettings = 0
    var announcementSet = 0
    var postId                          = 0
    var announcementId                  = 0
    var ActiveTab = 2
    var ArrayPosts = [JSON]()
    var offset = 0
    var limit = 1000
    var stopPagination = false
    var isNewDataLoading = true
    var isMembershipActive = false
    var famArr : [familyModel]?
    var memberJoining = 0
    var invitationStatus = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblList.isHidden = true

        if isAdmin.lowercased() == "admin"{
            if isMembershipActive{
                aboutArr = ["FEED","ANNOUNCEMENT","REQUEST","EVENTS","ALBUMS","ABOUT US","DOCUMENTS","LINKED FAMILIES","MEMBERSHIP","MEMBERS"]
            }
        }
        aboutClctnView.delegate = self
        aboutClctnView.dataSource = self
        aboutClctnView.reloadData()
        collectionViewLayout.scrollDirection = .horizontal
        collectionViewLayout.minimumLineSpacing = 0
        self.aboutClctnView.setCollectionViewLayout(collectionViewLayout, animated: true)
        
        self.lblName.text = appDel.groupNamePublic
        
        let imgUrl = URL(string: appDel.groupImageUrlPublic)
        if imgUrl != nil{
            self.imgCover.kf.indicatorType = .activity
            
            let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=400&height=300&url="+imgUrl!.relativeString
            let urlImg = URL(string: newUrlStr)
            
            self.imgCover.kf.setImage(with: urlImg, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        else{
            self.imgCover.image = #imageLiteral(resourceName: "family_default_cover.png")
        }
        txtSearch.delegate = self
        
        self.tblList.tableFooterView = UIView(frame: .zero)
    

    }
    
    override func viewDidLayoutSubviews(){
        super.viewDidLayoutSubviews()
        let indexPath = IndexPath(item: ActiveTab, section: 0)
        aboutClctnView.scrollToItem(at: indexPath, at:.centeredHorizontally, animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        getrequests()
    }
    
    //MARK:- get all req
    
    func getrequests(){
        
        //        lblnoData.text = "Requests will show up here"
        
        if offset == 0{
            ArrayPosts.removeAll()
            ActivityIndicatorView.show("Please wait....")
        }else{
            tblList.showLoading()
        }
        self.stopPagination = false
        
        
        let param = ["user_id" : UserDefaults.standard.value(forKey: "userId") as! String,"offset":"\(offset)","limit":"\(limit)","type":"post","txt":txtSearch.text!,"group_id":groupID]
        
        networkProvider.request(.post_need_list(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    
                    if self.offset == 0{
                        ActivityIndicatorView.hiding()
                    }
                    else{
                        self.tblList.hideLoading()
                    }
                    self.isNewDataLoading = false
                    let jsonData =  JSON(response.data)
                    if response.statusCode == 200{
                        
                        print("view contents : \(jsonData)")
                        
                        if let response = jsonData["data"].array{
                            
                            print("Json data reponse contents : \(response.count) Offset = \(self.offset) Limit = \(self.limit)")
                            
                            if response.count == 0{
                                self.stopPagination = true
                            }
                            self.ArrayPosts.append(contentsOf: response)
                            
                            if self.offset != 0{
                                if response.count > 0 {
                                    self.insertRowsAtTableView(responseCount: response.count)
                                }
                            }
                            if self.ArrayPosts.count > 0 {
                                self.tblList.isHidden = false
                                self.viewOfNoData.isHidden = true
                                
                                
                                if self.offset == 0{
                                    self.tblList.delegate = self
                                    self.tblList.dataSource = self
                                    self.tblList.reloadData()
                                }
                                if self.offset == 0{
                                    let topIndex = IndexPath(row: 0, section: 0)
                                    self.tblList.scrollToRow(at: topIndex, at: .top, animated: false)
                                }
                                
                            }
                            else{
                                self.tblList.isHidden = true
                                self.viewOfNoData.isHidden = false
                                
                                if self.txtSearch.text!.count > 0{
                                    self.lblNoData.text = "No results to show"
                                }
                                else{
                                    self.lblNoData.text = "No request till now"

                                }
                            }
                            
                            
                        }
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getrequests()
                        }
                    }
                    
                }catch _ {
                    self.tblList.isHidden = true
                    self.viewOfNoData.isHidden = false
                    self.lblNoData.text = "No request till now"

                    if self.offset == 0{
                        ActivityIndicatorView.hiding()
                    }
                    else{
                        self.tblList.hideLoading()
                    }
                }
            case .failure( _):
                self.tblList.isHidden = true
                self.viewOfNoData.isHidden = false
                self.lblNoData.text = "No request till now"

                if self.offset == 0{
                    ActivityIndicatorView.hiding()
                }
                else{
                    self.tblList.hideLoading()
                }
                break
            }
        }
    }
    
    //MARK:- Button actions
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popToViewController(ofClass: FamilyDetailsTableViewController.self)
    }
    @IBAction func onClickSearchReset(_ sender: Any) {
        txtSearch.text = ""
        self.offset = 0
        self.getrequests()
        
        btnSearchReset.isHidden = true
        self.txtSearch.endEditing(true)
    }
    
    
    //MARK:- Go To Create Request

    @IBAction func goToCreateRequest(_ sender: Any) {
        let selectedIds = NSMutableArray()
        selectedIds.add("\(groupID)")
        let storyBoard = UIStoryboard.init(name: "PostRequest", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "CreateRequestViewController") as! CreateRequestViewController
        vc.selectedIds = selectedIds
        vc.isAdmin = self.isAdmin
        vc.fromGroup = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func insertRowsAtTableView(responseCount: Int){
        
        var indexPaths = [IndexPath]()
        
        for i in 0 ..< responseCount{
            let indx = ArrayPosts.count - responseCount
            let indexPath = indx + i
            indexPaths.append(IndexPath(row: indexPath, section: 0))
        }
        
        print(indexPaths)
        self.tblList.beginUpdates()
        self.tblList.insertRows(at: indexPaths, with: .none)
        self.tblList.endUpdates()
    }
    
}
extension RequestInFamilyViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ArrayPosts.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        guard ArrayPosts.count != 0 else {
            return UITableViewCell.init()
        }
        if ArrayPosts.count == indexPath.row{
            tblList.tableFooterView = nil
            return UITableViewCell()
        }
            
        else{
            let need = ArrayPosts[indexPath.row]
            
            let cellid = "NeedsRequestListTableViewCell"
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! NeedsRequestListTableViewCell
            
            cell.buttonMenu.tag = indexPath.row
            cell.buttonViewMore.tag = indexPath.row
            cell.delegate = self
            cell.selectionStyle = .none
            cell.butttongotoDetails.tag = indexPath.row
            if let items = need["items"].array,items.count > 0{
                if let item = items[0].dictionary{
                    cell.lblNeedOneDesc.text = item["request_item_description"]?.string ?? ""
                    cell.lblNeedOnetitle.text =  item["request_item_title"]?.string ?? ""
                    cell.lblNeedOneQuantity.text =  "\(RequestDetailsViewController.subtact(total:item["item_quantity"]?.int ?? 0, contributed: item["received_amount"]?.int ?? 0)) of \(item["item_quantity"]?.int ?? 0)"
                    
                    if let total = item["received_amount"]?.int, let quantity = item["item_quantity"]?.int{
                        if total >= quantity{
                            cell.lblNeedOneStatus.text = ""
                            cell.lblNeedOneQuantity.text = "Completed"
                            cell.lblNeedOneQuantity.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                        }
                        else{
                            cell.lblNeedOneStatus.text = "Still needed"
                            cell.lblNeedOneQuantity.textColor = .black
                        }
                    }
                    else{
                        cell.lblNeedOneStatus.text = "Still needed"
                        cell.lblNeedOneQuantity.textColor = .black
                    }
                    
                    //                cell.lblNeedOneStatus.text = "Still needed"
                    
                    cell.needOneView.isHidden = false
                    cell.needThreeView.isHidden = true
                    cell.needTwoView.isHidden = true
                }
                if items.count > 1{
                    if let item = items[1].dictionary{
                        cell.lblNeedTwoDesc.text = item["request_item_description"]?.string ?? ""
                        cell.lblNeedTwotitle.text =  item["request_item_title"]?.string ?? ""
                        cell.lblNeedTwoQuantity.text =  "\(RequestDetailsViewController.subtact(total:item["item_quantity"]?.int ?? 0, contributed: item["received_amount"]?.int ?? 0)) of \(item["item_quantity"]?.int ?? 0)"
                        if let total = item["received_amount"]?.int, let quantity = item["item_quantity"]?.int{
                            if total >= quantity{
                                cell.lblNeedTwoStatus.text = ""
                                cell.lblNeedTwoQuantity.text = "Completed"
                                cell.lblNeedTwoQuantity.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                            }
                            else{
                                cell.lblNeedTwoStatus.text = "Still needed"
                                cell.lblNeedTwoQuantity.textColor = .black
                            }
                        }
                        else{
                            
                            cell.lblNeedTwoStatus.text = "Still needed"
                            cell.lblNeedTwoQuantity.textColor = .black
                            //                        cell.lblNeedTwoStatus.text = "Still needed"
                        }
                        
                        
                        cell.needThreeView.isHidden = true
                        cell.needTwoView.isHidden = false
                        cell.needOneView.isHidden = false
                        
                    }
                }
                if items.count > 2{
                    if let item = items[2].dictionary{
                        cell.lblNeedThreeDesc.text = item["request_item_description"]?.string ?? ""
                        cell.lblNeedThreetitle.text =  item["request_item_title"]?.string ?? ""
                        cell.lblNeedThreeQuantity.text =  "\(RequestDetailsViewController.subtact(total:item["item_quantity"]?.int ?? 0, contributed: item["received_amount"]?.int ?? 0)) of \(item["item_quantity"]?.int ?? 0)"
                        if let total = item["received_amount"]?.int, let quantity = item["item_quantity"]?.int{
                            if total >= quantity{
                                cell.lblNeedThreeStatus.text = ""
                                cell.lblNeedThreeQuantity.text = "Completed"
                                cell.lblNeedThreeQuantity.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                            }
                            else{
                                cell.lblNeedThreeStatus.text = "Still needed"
                                cell.lblNeedThreeQuantity.textColor = .black
                            }
                        }
                        else{
                            cell.lblNeedThreeStatus.text = "Still needed"
                            cell.lblNeedThreeQuantity.textColor = .black
                        }
                        
                        //                    cell.lblNeedThreeStatus.text = "Still needed"
                        
                        cell.needThreeView.isHidden = false
                        cell.needTwoView.isHidden = false
                        cell.needOneView.isHidden = false
                        
                    }
                }
            }
            else{
                cell.needOneView.isHidden = true
                cell.needThreeView.isHidden = true
                cell.needTwoView.isHidden = true
            }
            
            if let groups = need["to_groups"].array, groups.count > 0{
                if groups.count == 1{
                    cell.lblpostedInFamilyName.text = "Posted in \(groups[0]["group_name"].string ?? "Unknown")"
                }
                else if groups.count == 2{
                    cell.lblpostedInFamilyName.text = "Posted in \(groups[0]["group_name"].string ?? "Unknown") and 1 other family"
                }
                else{
                    cell.lblpostedInFamilyName.text = "Posted in \(groups[0]["group_name"].string ?? "Unknown") and \(groups.count - 1) other families"
                }
            }
            else{
                cell.lblpostedInFamilyName.text = "Posted in Public"
            }
            cell.lblUserName.text = need["full_name"].string ?? "Unknown"
            cell.lblNeedLocation.text = need["request_location"].string ?? "Unknown"
            cell.lblDate.text = HomeTabViewController.dateFormatterCommon(dateStr: need["created_at"].stringValue)
            if let enddate = need["end_date"].string, !enddate.isEmpty{
                if enddate == "0"{
                    cell.lblNeedDate.text = EventsTabViewController.convertTimeStampToDate(timestamp: need["start_date"].stringValue)
                }
                else{
                    cell.lblNeedDate.text = EventsTabViewController.convertTimeStampToDate(timestamp: need["end_date"].stringValue)
                }
            }
            else{
                cell.lblNeedDate.text = EventsTabViewController.convertTimeStampToDate(timestamp: need["start_date"].stringValue)
            }
            cell.lblNumberofSupporters.text = need["supporters"].stringValue
            let supportCount = Int(need["supporters"].stringValue)!
            if supportCount > 1{
                cell.lblHeadingSupporter.text = "Supporters"
            }
            else{
                cell.lblHeadingSupporter.text = "Supporter"
            }
            
            let proPic = need["propic"].stringValue
            
            DispatchQueue.main.async {
                
                if proPic.count != 0{
                    let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic
                    let imgUrl = URL(string: temp)
                    cell.ImageViewuserProfile.kf.indicatorType = .activity
                    
                    cell.ImageViewuserProfile.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                }
                else{
                    cell.ImageViewuserProfile.image = #imageLiteral(resourceName: "Male Colored")
                }
            }
            cell.ImageViewuserProfile.isTappable(id: need["user_id"].intValue)
            
            if need["user_id"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")" {
                
                cell.viewOfThreeDot.isHidden = false
                
            }
            else{
                cell.viewOfThreeDot.isHidden = true
            }
            
            return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        // pagination
        
        if tableView.isLast(for: indexPath, arrayCount: ArrayPosts.count) {
            
            if !isNewDataLoading && !self.stopPagination{
                isNewDataLoading = true
                if ArrayPosts.count != 0{
                    offset = ArrayPosts.count + 1
                }
                self.getrequests()
            }
        }
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
extension RequestInFamilyViewController: NeedsRequestListTableViewCellDelegate{
    func viewMoreAction(index: Int) {
        
        let need = ArrayPosts[index]
        let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "RequestDetailsViewController") as! RequestDetailsViewController
        vc.requestId = need["post_request_id"].stringValue
        //        vc.requestDic = need
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func rightMenuAction(index: Int) {
        let need = ArrayPosts[index]
        
        postoptionsTittle.removeAll()
        
        
        if need["user_id"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")" {
            
            postoptionsTittle =  ["Edit","Delete"]
            
        }
        else{
            postoptionsTittle =  ["Report"]
            
        }
        
        showActionSheet(titleArr: postoptionsTittle as NSArray, title: "Select options") { (indx) in
            
            if indx == 1{
                if need["user_id"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")"{
                    
                    self.deleteRequestCreater(index: index)
                    
                }
                else
                {
                    //                                        let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
                    //                                        let vc = storyboard.instantiateViewController(withIdentifier: "ReportPostViewController") as! ReportPostViewController
                    //
                    //                                        vc.requestDetails =  need
                    //                                        vc.isFrom = "request_needs"
                    //                                        self.navigationController?.pushViewController(vc, animated: true)
                    
                    //Report
                    
                    
                }
            }
            else if indx == 0{
                if need["user_id"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")"{
                    
                    //Edit
                    let stryboard = UIStoryboard.init(name: "PostRequest", bundle: nil)
                    let vc = stryboard.instantiateViewController(withIdentifier: "EditRequestViewController") as! EditRequestViewController
                    vc.requestDic = need
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }else{
                    let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "ReportPostViewController") as! ReportPostViewController
                    
                    vc.requestDetails =  need
                    vc.isFrom = "request_needs"
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else if indx == 100{
            }
            
        }
    }
    
    func deleteRequestCreater(index:Int){
        let alert = UIAlertController(title: "Familheey", message: "Do you really want to delete this request? Deleting this request will delete it from everywhere you posted it to", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "DELETE", style: .destructive, handler: { _ in
            let need = self.ArrayPosts[index]
            
            
            let param = ["post_request_id" : need["post_request_id"].stringValue,"user_id":UserDefaults.standard.value(forKey: "userId") as! String]
            
            ActivityIndicatorView.show("Please wait....")
            self.networkProvider.request(.need_delete(parameter: param)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                        print("view contents : \(jsonData)")
                        if response.statusCode == 200{
                            print("view contents : \(jsonData)")
                            self.offset = 0
                            
                            self.getrequests()
                        }
                        else if response.statusCode == 401 {
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                self.deleteRequestCreater(index: index)
                            }
                        }
                        
                        ActivityIndicatorView.hiding()
                        
                    }catch _ {
                        ActivityIndicatorView.hiding()
                    }
                case .failure( _):
                    ActivityIndicatorView.hiding()
                    break
                }
            }
            
        }))
        alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
}
extension RequestInFamilyViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return aboutArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AboutFamilyCollectionViewCell", for: indexPath as IndexPath) as! AboutFamilyCollectionViewCell
        cell.lblName.text = aboutArr[indexPath.item]
        if indexPath.row == ActiveTab{
            cell.lblName.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            cell.lblName.font = .boldSystemFont(ofSize: 15)
            cell.imgUnderline.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            
        }
        else{
            cell.lblName.textColor = .black
            cell.lblName.font = .systemFont(ofSize: 14)
            cell.imgUnderline.backgroundColor = .white
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0{ // Feeds
            let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
            let addMember = storyboard.instantiateViewController(withIdentifier: "PostedInGroupsViewController") as! PostedInGroupsViewController
            addMember.groupId = self.groupID
            addMember.isAdmin = self.isAdmin
            addMember.link_type = self.link_type
            addMember.islinkable = self.islinkable
            addMember.facate = self.facate
            addMember.famSettings = famSettings
            addMember.announcementSet = announcementSet
            addMember.postId                    = self.postId
            addMember.announcementId            = self.announcementId
            addMember.famArr = famArr
            addMember.memberJoining = self.memberJoining
            addMember.invitationStatus = self.invitationStatus
            addMember.isMembershipActive = self.isMembershipActive
            self.navigationController?.pushViewController(addMember, animated: false)
        }
        else if indexPath.row == 1{ // Announcement
            let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
            let addMember = storyboard.instantiateViewController(withIdentifier: "AnnouncementInFamilyViewController") as! AnnouncementInFamilyViewController
            addMember.groupId = self.groupID
            addMember.isAdmin = self.isAdmin
            addMember.link_type = self.link_type
            addMember.islinkable = self.islinkable
            addMember.facate = self.facate
            addMember.famSettings = famSettings
            addMember.announcementSet = announcementSet
            addMember.postId                    = self.postId
            addMember.announcementId            = self.announcementId
            addMember.famArr = famArr
            addMember.memberJoining = self.memberJoining
            addMember.invitationStatus = self.invitationStatus
            addMember.isMembershipActive = self.isMembershipActive
            self.navigationController?.pushViewController(addMember, animated: false)
        }
        else if indexPath.row == 2{ // Request
            
        }
        else if indexPath.row == 3{ // Events
            let storyboard = UIStoryboard.init(name: "third", bundle: nil)
            let addMember = storyboard.instantiateViewController(withIdentifier: "EventListingViewController") as! EventListingViewController
            addMember.groupID = self.groupID
            addMember.isAdmin = self.isAdmin
            addMember.link_type = self.link_type
            addMember.islinkable = self.islinkable
            addMember.facte = self.facate
            addMember.famSettings = famSettings
            addMember.announcementSet = announcementSet
            addMember.postId                    = self.postId
            addMember.announcementId            = self.announcementId
            addMember.famArr = famArr
            addMember.memberJoining = self.memberJoining
            addMember.invitationStatus = self.invitationStatus
            addMember.isMembershipActive = self.isMembershipActive
            self.navigationController?.pushViewController(addMember, animated: false)
        }
        else if indexPath.row == 5{ // About us
            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
            intro.groupId = self.groupID
            intro.isAboutUsClicked = true
            self.navigationController?.pushViewController(intro, animated: false)
        }
        else if indexPath.row == 8{ // Members
            if isMembershipActive{
                if isAdmin.lowercased() == "admin"{
                    let storyboard = UIStoryboard.init(name: "MembershipMngmnt", bundle: nil)
                    let addMember = storyboard.instantiateViewController(withIdentifier: "MemebrshipDashboardViewController") as! MemebrshipDashboardViewController
                    addMember.groupID = self.groupID
                    addMember.isAdmin = self.isAdmin
                    addMember.link_type = self.link_type
                    addMember.islinkable = self.islinkable
                    //                        addMember.isFromdocument = true
                    addMember.facate = self.facate
                    addMember.famSettings = famSettings
                    addMember.announcementSet = announcementSet
                    addMember.postId                    = self.postId
                    addMember.announcementId            = self.announcementId
                    addMember.isMembershipActive = self.isMembershipActive
                    addMember.famArr = famArr
                    addMember.memberJoining = self.memberJoining
                    addMember.invitationStatus = self.invitationStatus
                    self.navigationController?.pushViewController(addMember, animated: true)
                }
                else{
                    let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                    let addMember = storyboard.instantiateViewController(withIdentifier: "AddMembersNewViewController") as! AddMembersNewViewController
                    addMember.groupId = self.groupID
                    addMember.isAdmin = self.isAdmin
                    addMember.link_type = self.link_type
                    addMember.islinkable = self.islinkable
                    addMember.faCate = self.facate
                    addMember.famSettings = famSettings
                    addMember.announcementSet = announcementSet
                    addMember.postId                    = self.postId
                    addMember.announcementId            = self.announcementId
                    addMember.isMembershipActive = self.isMembershipActive
                    addMember.famArr = famArr
                    addMember.memberJoiningStatus = self.memberJoining
                    addMember.invitationStatus = self.invitationStatus
                    self.navigationController?.pushViewController(addMember, animated: false)
                }
            }
            else{
                let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "AddMembersNewViewController") as! AddMembersNewViewController
                addMember.groupId = self.groupID
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.faCate = self.facate
                addMember.famSettings = famSettings
                addMember.announcementSet = announcementSet
                addMember.postId                    = self.postId
                addMember.announcementId            = self.announcementId
                addMember.isMembershipActive = self.isMembershipActive
                addMember.famArr = famArr
                addMember.memberJoiningStatus = self.memberJoining
                addMember.invitationStatus = self.invitationStatus
                self.navigationController?.pushViewController(addMember, animated: false)
            }
        }
        else if indexPath.row == 9{
            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            let addMember = storyboard.instantiateViewController(withIdentifier: "AddMembersNewViewController") as! AddMembersNewViewController
            addMember.groupId = self.groupID
            addMember.isAdmin = self.isAdmin
            addMember.link_type = self.link_type
            addMember.islinkable = self.islinkable
            addMember.faCate = self.facate
            addMember.famSettings = famSettings
            addMember.announcementSet = announcementSet
            addMember.postId                    = self.postId
            addMember.announcementId            = self.announcementId
            addMember.isMembershipActive = self.isMembershipActive
            addMember.famArr = famArr
            addMember.memberJoiningStatus = self.memberJoining
            addMember.invitationStatus = self.invitationStatus
            self.navigationController?.pushViewController(addMember, animated: false)
        }
        else if indexPath.row == 4{ // Albums
            let storyboard = UIStoryboard.init(name: "third", bundle: nil)
            let addMember = storyboard.instantiateViewController(withIdentifier: "AlbumListingViewController") as! AlbumListingViewController
            addMember.groupID = groupID
            addMember.isAdmin = self.isAdmin
            addMember.link_type = self.link_type
            addMember.islinkable = self.islinkable
            addMember.facate = self.facate
            addMember.famSettings = famSettings
            addMember.announcementSet = announcementSet
            addMember.postId                    = self.postId
            addMember.announcementId            = self.announcementId
            addMember.famArr = famArr
            addMember.memberJoining = self.memberJoining
            addMember.invitationStatus = self.invitationStatus
            addMember.isMembershipActive = self.isMembershipActive
            self.navigationController?.pushViewController(addMember, animated: false)
        }
        else if indexPath.row == 6{ // Documents
            let storyboard = UIStoryboard.init(name: "third", bundle: nil)
            let addMember = storyboard.instantiateViewController(withIdentifier: "DocumentListingViewController") as! DocumentListingViewController
            addMember.groupID = self.groupID
            addMember.isAdmin = self.isAdmin
            addMember.link_type = self.link_type
            addMember.islinkable = self.islinkable
            addMember.isFromdocument = true
            addMember.facate = self.facate
            addMember.famSettings = famSettings
            addMember.announcementSet = announcementSet
            addMember.postId                    = self.postId
            addMember.announcementId            = self.announcementId
            addMember.famArr = famArr
            addMember.memberJoining = self.memberJoining
            addMember.invitationStatus = self.invitationStatus
            addMember.isMembershipActive = self.isMembershipActive
            self.navigationController?.pushViewController(addMember, animated: false)
        }
        else{ // Linked families
            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            let addMember = storyboard.instantiateViewController(withIdentifier: "LinkedFamiliesListViewController") as! LinkedFamiliesListViewController
            addMember.groupID = self.groupID
            addMember.isAdmin = self.isAdmin
            addMember.link_type = self.link_type
            addMember.islinkable = self.islinkable
            addMember.facate = self.facate
            addMember.famSettings = famSettings
            addMember.announcementSet = announcementSet
            addMember.postId                    = self.postId
            addMember.announcementId            = self.announcementId
            addMember.famArr = famArr
            addMember.memberJoining = self.memberJoining
            addMember.invitationStatus = self.invitationStatus
            addMember.isMembershipActive = self.isMembershipActive
            self.navigationController?.pushViewController(addMember, animated: false)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var w = aboutArr[indexPath.row].size(withAttributes: nil)
        w.width = w.width + 32
        return CGSize(width: w.width, height: 60)
    }
    
}

extension RequestInFamilyViewController: UITextFieldDelegate{
    //MARK:- textfield delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        textField.resignFirstResponder()
        if textField == txtSearch
        {
            // selectWebAPI()
            if textField.text!.count > 0{
                btnSearchReset.isHidden = false
            }
            else{
                btnSearchReset.isHidden = true
            }
            textField.endEditing(true)
        }
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        self.view.endEditing(true)
        if textField == txtSearch
        {
            if textField.text!.count > 0{
                btnSearchReset.isHidden = false
                if textField.text!.trimmingCharacters(in: .whitespaces).isEmpty {
                    txtSearch.text = ""
                    btnSearchReset.isHidden = true
                    
                }
            }
            else{
                btnSearchReset.isHidden = true
            }
            self.offset = 0
            getrequests()
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
            // selectWebAPI()
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
        }
        return true
    }
}
