//
//  LinkedFamiliesListViewController.swift
//  familheey
//
//  Created by ANIL K on 28/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Moya
import Alamofire
import Kingfisher
import SwiftyJSON

class LinkedFamiliesListViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var bgNofamily: UIView!
    @IBOutlet weak var tblListView: UITableView!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var bgNoBlockedusers: UIView!
    @IBOutlet weak var linkedRequestView: UIView!
    @IBOutlet weak var linkedFamilyView: UIView!
    
    private var networkProvider                        = MoyaProvider<FamilyheeyApi>()
    
    @IBOutlet weak var imgShadow: UIImageView!
    @IBOutlet weak var lblHead: UILabel!
    @IBOutlet weak var aboutView: UIView!
    @IBOutlet weak var aboutClctnVew: UICollectionView!
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var aboutVew_height: NSLayoutConstraint!
    
    @IBOutlet weak var noRequstVew: UIView!
    var dataArr = [JSON]()
    
    var groupID = ""
    
    var link_type = 0
    var isAdmin = ""
    var facate = ""
    var islinkable = 0
    var famSettings = 0
    var announcementSet = 0
    var postId                          = 0
    var announcementId                  = 0
    var isFromRequest = false
    var isMembershipActive = false
    
    var isFromBlocked = false
    var selectedIndex = 0
    var requestArr = [groupRequestLinked]()
    var aboutArr = ["FEED","ANNOUNCEMENT","REQUEST","EVENTS","ALBUMS","ABOUT US","DOCUMENTS","LINKED FAMILIES","MEMBERS"]
    var ActiveTab = 7
    
    var famArr : [familyModel]?
    var memberJoining = 0
    var invitationStatus = 0
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearchReset: UIButton!
    
    @IBOutlet weak var requestView_height: NSLayoutConstraint!
    @IBOutlet weak var linkRequestView: UIView!
    @IBOutlet weak var imgPeople: UIImageView!
    @IBOutlet weak var btnPeople: UIButton!
    @IBOutlet weak var imgOther: UIImageView!
    @IBOutlet weak var btnOther: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        Helpers.setleftView(textfield: txtSearch, customWidth: 30)
        if isAdmin.lowercased() == "admin"{
            if isMembershipActive{
               aboutArr = ["FEED","ANNOUNCEMENT","REQUEST","EVENTS","ALBUMS","ABOUT US","DOCUMENTS","LINKED FAMILIES","MEMBERSHIP","MEMBERS"]
            }
        }
        
        aboutClctnVew.delegate = self
        aboutClctnVew.dataSource = self
        aboutClctnVew.reloadData()

        let headerNib = UINib.init(nibName: "FamilyListingView", bundle: Bundle.main)
        tblListView.register(headerNib, forHeaderFooterViewReuseIdentifier: "FamilyListingView")
        
        if isFromBlocked{
            getAllBlockedUsers()
            lblHead.text = "Blocked Members"
            bgNofamily.isHidden = true
            bgNoBlockedusers.isHidden = false
            btnAdd.isHidden = true
            requestView_height.constant = 0
            linkRequestView.isHidden = true
            imgCover.isHidden = true
            aboutVew_height.constant = 0
            //            imgShadow.isHidden = true
            //            lblHead.textColor = .black
        }
        else{
            //            lblHead.text = "Linked Families"
            //             lblHead.textColor = .white
            aboutVew_height.constant = 46
//            getAllLinkedFamilies()
            bgNoBlockedusers.isHidden = true
            bgNofamily.isHidden = false
            btnAdd.isHidden = false
            
            requestView_height.constant = 40
            linkRequestView.isHidden = false
            
            if famArr![0].linkFamilyApproval == 16{
                if famArr![0].isAdmin.lowercased() == "admin"{
                    self.linkedRequestView.isHidden = false
                }
                else{
                    self.linkedRequestView.isHidden = true
                }
            }
            else{
                self.linkedRequestView.isHidden = false
            }
            
            let button = UIButton()
            if isFromRequest{
               button.tag = 102
            }
            else{
               button.tag = 101
            }
            
            self.onClickTabAction(button)
            
            self.lblHead.text = appDel.groupNamePublic
            let temp = appDel.groupImageUrlPublic.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let imgUrl = URL(string: temp)
            if imgUrl != nil{
                self.imgCover.kf.indicatorType = .activity
                let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=475&height=400&url="+imgUrl!.relativeString
                let urlImg = URL(string: newUrlStr)
                
                self.imgCover.kf.setImage(with: urlImg, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
            }
            else{
                self.imgCover.image = #imageLiteral(resourceName: "family_default_cover.png")
            }
            
        }
        txtSearch.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if appDel.isFromLinkedFam{
            self.groupID = appDel.currentGrpID
            appDel.groupImageUrlPublic = appDel.crntgroupImageUrlPublic
            appDel.groupNamePublic = appDel.crntGrpName
            appDel.isFromLinkedFam = false
        }
    }
    
    override func viewDidLayoutSubviews(){
        super.viewDidLayoutSubviews()
        let indexPath = IndexPath(item: ActiveTab, section: 0)
        aboutClctnVew.scrollToItem(at: indexPath, at:.centeredHorizontally, animated: true)
    }
    
    //MARK:- Web api
    func getGroupRequests(grpId:String){
        APIServiceManager.callServer.getAllGroupRequest(url: EndPoint.groupRequestList, groupId: grpId, success: { (responseMdl) in
            guard let result = responseMdl as? groupReqestResult else{
                return
            }
            ActivityIndicatorView.hiding()
            if result.status_code == 200{
                self.requestArr = result.resultLink!
                if self.requestArr.count > 0{
                    self.tblListView.isHidden = false
                    self.tblListView.delegate = self
                    self.tblListView.dataSource = self
                    self.tblListView.reloadData()
                    self.noRequstVew.isHidden = true
                    self.btnAdd.isHidden = true

                }
                else{
                    self.tblListView.isHidden = true
                    self.noRequstVew.isHidden = false
                    self.bgNofamily.isHidden = true
                    self.bgNoBlockedusers.isHidden = true
                    self.btnAdd.isHidden = true

                }
            }
            
        }) { (error) in
            ActivityIndicatorView.hiding()
        }
    }
    
    func getAllLinkedFamilies(){
        let parameter = ["group_id":self.groupID,"query":txtSearch.text!] as [String : Any]
        ActivityIndicatorView.show("Loading.....")
        networkProvider.request(.FamilyLinedList(parameter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    
                    if response.statusCode == 200{
                        print("jsonData : \(jsonData)")
                        self.dataArr = jsonData["data"].arrayValue
                        
                        if self.dataArr.count > 0{
                            self.tblListView.delegate = self
                            self.tblListView.dataSource = self
                            self.tblListView.isHidden = false
                            self.btnAdd.isHidden = false
                            self.tblListView.reloadData()
                        }
                        else{
                            self.tblListView.isHidden = true
                            self.btnAdd.isHidden = true
                            self.noRequstVew.isHidden = true
                            self.bgNofamily.isHidden = false
                            self.bgNoBlockedusers.isHidden = true
                        }
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getAllLinkedFamilies()
                        }
                    }
                    else{
                        
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                
                
                
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    func getAllBlockedUsers(){
        let parameter = ["group_id":self.groupID, "type":"groups", "user_id":UserDefaults.standard.value(forKey: "userId") as! String,"query":txtSearch.text!] as [String : Any]
        ActivityIndicatorView.show("Loading.....")
        networkProvider.request(.BlockedUser(parameter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    
                    if response.statusCode == 200{
                        print("jsonData : \(jsonData)")
                        self.dataArr = jsonData["data"].arrayValue
                        
                        if self.dataArr.count > 0{
                            self.tblListView.delegate = self
                            self.tblListView.dataSource = self
                            self.tblListView.isHidden = false
                            // self.btnAdd.isHidden = false
                            self.tblListView.reloadData()
                        }
                        else{
                            self.tblListView.isHidden = true
                            self.btnAdd.isHidden = true
                            self.noRequstVew.isHidden = true
                            self.bgNofamily.isHidden = true
                            self.bgNoBlockedusers.isHidden = false
                        }
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getAllBlockedUsers()
                        }
                    }
                    else{
                        
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                
                
                
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    func unblockUsers(index:Int){
        if self.isAdmin.lowercased() == "admin"{
            let data = dataArr[index]
            let params = ["id":data["groupmap_id"].stringValue,"is_blocked":NSNumber(value: false)] as [String : Any]
            
            APIServiceManager.callServer.updateGroupMaps(url: EndPoint.update_groupmaps, params: params, success: { (response) in
                ActivityIndicatorView.hiding()
                guard let result = response as? requestSuccessModel else{
                    return
                }
                ActivityIndicatorView.hiding()
                if result.status_code == 200{
                    self.displayAlert(alertStr: "User unblocked successfully", title: "")
                    self.getAllBlockedUsers()
                }
            }, failure: { (error) in
                ActivityIndicatorView.hiding()
            })
        }
    }
    
    //MARK:- Custom medthods
    @IBAction func onClickTabAction(_ sender: UIButton) {
        if sender.tag == 102{
            selectedIndex = 1
            btnOther.setTitleColor(UIColor(named: "greenBackgrounf"), for: .normal)
            btnPeople.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            
            imgOther.backgroundColor  = UIColor(named: "greenBackgrounf")
            imgPeople.backgroundColor  = UIColor(named: "lightGrayTextColor")
            
            self.getGroupRequests(grpId: self.groupID)
            //               btomView.isHidden = false
            //               btmView_height.constant = 89
            //
            //               self.tblList.delegate = self
            //               self.tblList.dataSource = self
            //               self.tblList.reloadData()
            
        }
        else{
            selectedIndex = 0
            btnOther.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            btnPeople.setTitleColor(UIColor(named: "greenBackgrounf"), for: .normal)
            
            imgOther.backgroundColor  = UIColor(named: "lightGrayTextColor")
            imgPeople.backgroundColor  = UIColor(named: "greenBackgrounf")
            
            self.getAllLinkedFamilies()
            
            //               btomView.isHidden = true
            //               btmView_height.constant = 0
            //               getAllMembers(groupId: self.groupId)
        }
    }
    
    func requestAccept(tag:Int){
        let tempDic = requestArr[tag]
        
        APIServiceManager.callServer.groupRequestResponse(url: EndPoint.groupRequestResp, tableId: "\(tempDic.id)", userId: UserDefaults.standard.value(forKey: "userId") as! String, groupId: "\(tempDic.groupId)", type: tempDic.type, status: "accepted", success: { (responseMdl) in
            
            guard let result = responseMdl as? requestSuccessModel else{
                return
            }
            
            if result.status_code == 200{
                self.getGroupRequests(grpId: self.groupID)
            }
            else{
                ActivityIndicatorView.hiding()
            }
            
        }) { (error) in
            ActivityIndicatorView.hiding()
        }
    }
    
    func requestReject(tag:Int){
        let tempDic = requestArr[tag]
        APIServiceManager.callServer.groupRequestResponse(url: EndPoint.groupRequestResp, tableId: "\(tempDic.id)", userId: UserDefaults.standard.value(forKey: "userId") as! String, groupId: "\(tempDic.groupId)", type: tempDic.type, status: "rejected", success: { (responseMdl) in
            
            guard let result = responseMdl as? requestSuccessModel else{
                return
            }
            
            if result.status_code == 200{
                self.getGroupRequests(grpId: self.groupID)
            }
            else{
                ActivityIndicatorView.hiding()
            }
            
        }) { (error) in
            ActivityIndicatorView.hiding()
        }
    }
    
    //MARK:- Button actions
    @IBAction func onClickBack(_ sender: Any) {
        if isFromBlocked{
                    self.navigationController?.popViewController(animated: true)
        }
        else{
            self.navigationController?.popToViewController(ofClass: FamilyDetailsTableViewController.self)

        }
    }
    
    @IBAction func onClickAddLinkAction(_ sender: Any) {
        if self.link_type == 13{
            if self.isAdmin.lowercased() == "admin"{
                if self.islinkable == 1{
                    let story = UIStoryboard.init(name: "CreateFamily", bundle: nil)
                    let linked =  story.instantiateViewController(withIdentifier: "addFamilySix") as! AddFamilyScreenSixViewController
                    linked.groupId = self.groupID
                    linked.isFromLink = true
                    linked.isFromLinked = true
                    appDel.isFromLinkedFam = true
                    appDel.currentGrpID = self.groupID
                    appDel.crntGrpName = appDel.groupNamePublic
                    appDel.crntgroupImageUrlPublic = appDel.groupImageUrlPublic
                    
                    self.navigationController?.pushViewController(linked, animated: true)
                }
                else{
                    self.displayAlert(alertStr: "The group is currently not linkable. You can change that in group settings", title: "")
                }
            }
            else{
                self.displayAlert(alertStr: "Only admins can initiate link request.", title: "")
            }
        }
        else{
            if self.islinkable == 1{
                let story = UIStoryboard.init(name: "CreateFamily", bundle: nil)
                let linked =  story.instantiateViewController(withIdentifier: "addFamilySix") as! AddFamilyScreenSixViewController
                linked.groupId = self.groupID
                linked.isFromLink = true
                linked.isFromLinked = true
                appDel.isFromLinkedFam = true
                appDel.currentGrpID = self.groupID
                appDel.crntGrpName = appDel.groupNamePublic
                appDel.crntgroupImageUrlPublic = appDel.groupImageUrlPublic
                
                self.navigationController?.pushViewController(linked, animated: true)
            }
            else{
                self.displayAlert(alertStr: "The group is currently not linkable. You can change that in group settings", title: "")
            }
        }
    }
    
    @IBAction func onClickUnlinkAction(_ sender: UIButton){
        if isFromBlocked{
            self.unblockUsers(index: sender.tag)
        }
        else{
            if self.isAdmin.lowercased() == "admin"{
                let alert = UIAlertController(title: "Familheey", message: "Do you really want to unlink the family?", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "UNLINK", style: .destructive, handler: { _ in
                    let data = self.dataArr[sender.tag]
                    print(data)
                    var status = ""
                    
                    if data["status"].stringValue.lowercased() == "accepted"{
                        status = "unlinked"
                    }
                    else{
                        status = "accepted"
                    }
                    
                    let parameter = ["id":data["id"].stringValue, "status": status] as [String : Any]
                    
                    ActivityIndicatorView.show("Loading.....")
                    self.networkProvider.request(.ActionUnLink(parameter: parameter)) { (result) in
                        switch result{
                            
                        case .success( let response):
                            do{
                                let jsonData =  JSON(response.data)
                                
                                if response.statusCode == 200{
                                    print("jsonData : \(jsonData)")
                                    self.getAllLinkedFamilies()
                                }
                                else if response.statusCode == 401 {
                                    Helpers.getAccessToken { (accessToken) in
                                        setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                        let btn = UIButton()
                                        btn.tag = sender.tag
                                        self.onClickUnlinkAction(btn)
                                    }
                                }
                                else{
                                    ActivityIndicatorView.hiding()
                                }
                            }catch let err {
                                
                                ActivityIndicatorView.hiding()
                                Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                            }
                        case .failure( let error):
                            ActivityIndicatorView.hiding()
                            Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                            break
                        }
                    }
                }))
                alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: nil))
                
                self.present(alert, animated: true, completion: nil)
            }
            else{
                
            }
        }
    }
    
    @IBAction func onClickViewFamily(_ sender: UIButton){
        var grpId = ""
        let temp = dataArr[sender.tag]
        
        grpId = temp["to_group"].stringValue
        if self.groupID == grpId{
            grpId = temp["from_group"].stringValue
        }
        
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
        intro.groupId = grpId
        intro.isFromFamilyList = true
        appDel.isFromLinkedFam = true
        appDel.currentGrpID = self.groupID
        appDel.crntGrpName = appDel.groupNamePublic
        appDel.crntgroupImageUrlPublic = appDel.groupImageUrlPublic
        
        self.navigationController?.pushViewController(intro, animated: true)
    }
    
    @IBAction func onClickSerchReset(_ sender: Any) {
        txtSearch.text  = ""
        if isFromBlocked{
            getAllBlockedUsers()
        }
        else{
            getAllLinkedFamilies()
        }
        self.btnSearchReset.isHidden = true
        self.txtSearch.endEditing(true)
    }
    
    @IBAction func onClickBtnAccept(_ sender: UIButton) {
        // let tempDic = requestArr[sender.tag]
        
        //        if memberJoining == 4{
        //            if isPrimaryAdmin.lowercased() == "admin"{
        //                requestAccept(tag: sender.tag)
        //            }
        //            else{
        //                self.displayAlert(alertStr: "No access", title: "")
        //            }
        //        }
        //        else{
        
        //        }
        
        if famArr![0].linkFamilyApproval == 16{
            if famArr![0].isAdmin.lowercased() == "admin"{
                requestAccept(tag: sender.tag)
            }
            else{
                self.displayAlert(alertStr: "No access. Only Admin can accept the request.", title: "")
            }
        }
        else{
            requestAccept(tag: sender.tag)
        }
        
        
    }
    @IBAction func onClickBtnReject(_ sender: UIButton) {
//        let tempDic = requestArr[sender.tag]
        //        if memberJoining == 4{
        //            if isPrimaryAdmin.lowercased() == "admin"{
        //                requestReject(tag: sender.tag)
        //            }
        //            else{
        //                self.displayAlert(alertStr: "No access", title: "")
        //            }
        //        }
        //        else{
        
        requestReject(tag: sender.tag)
        //        }
        if famArr![0].linkFamilyApproval == 16{
            if famArr![0].isAdmin.lowercased() == "admin"{
                requestReject(tag: sender.tag)
            }
            else{
                self.displayAlert(alertStr: "No access. Only Admin can reject the request.", title: "")
            }
        }
        else{
            requestReject(tag: sender.tag)
        }
        
    }
    
    @IBAction func onClickAddFamily(_ sender: Any) {
        if self.link_type == 13{
            if self.isAdmin.lowercased() == "admin"{
                if self.islinkable == 1{
                    let story = UIStoryboard.init(name: "CreateFamily", bundle: nil)
                    let linked =  story.instantiateViewController(withIdentifier: "addFamilyOne") as! AddFamilyScreenOneViewController
                    linked.isFromLinked = true
                    linked.fromId = self.groupID
                    appDel.isFromLinkedFam = true
                    appDel.currentGrpID = self.groupID
                    appDel.crntGrpName = appDel.groupNamePublic
                    appDel.crntgroupImageUrlPublic = appDel.groupImageUrlPublic
                    self.navigationController?.pushViewController(linked, animated: true)
                }
                else{
                    self.displayAlert(alertStr: "The group is currently not linkable. You can change that in group settings", title: "")
                }
            }
            else{
                self.displayAlert(alertStr: "The group is currently not linkable. You can change that in group settings", title: "")
            }
        }
        else{
            if self.islinkable == 1{
                let story = UIStoryboard.init(name: "CreateFamily", bundle: nil)
                let linked =  story.instantiateViewController(withIdentifier: "addFamilyOne") as! AddFamilyScreenOneViewController
                linked.isFromLinked = true
                linked.fromId = self.groupID
                appDel.isFromLinkedFam = true
                appDel.currentGrpID = self.groupID
                appDel.crntGrpName = appDel.groupNamePublic
                appDel.crntgroupImageUrlPublic = appDel.groupImageUrlPublic
                self.navigationController?.pushViewController(linked, animated: true)
            }
            else{
                self.displayAlert(alertStr: "The group is currently not linkable. You can change that in group settings", title: "")
            }
        }
    }
    
    //MARK:- collectionView Delegates
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return aboutArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AboutFamilyCollectionViewCell", for: indexPath as IndexPath) as! AboutFamilyCollectionViewCell
        cell.lblName.text = aboutArr[indexPath.item]
           if indexPath.row == ActiveTab{
                 cell.lblName.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                 cell.lblName.font = .boldSystemFont(ofSize: 15)
                 cell.imgUnderline.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)

             }
             else{
                 cell.lblName.textColor = .black
                 cell.lblName.font = .systemFont(ofSize: 14)
                 cell.imgUnderline.backgroundColor = .white
             }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0{ // Feeds
            let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
            let addMember = storyboard.instantiateViewController(withIdentifier: "PostedInGroupsViewController") as! PostedInGroupsViewController
            addMember.groupId = self.groupID
            addMember.isAdmin = self.isAdmin
            addMember.link_type = self.link_type
            addMember.islinkable = self.islinkable
            addMember.facate = self.facate
            addMember.famSettings = famSettings
            addMember.announcementSet = announcementSet
            addMember.postId                    = self.postId
            addMember.announcementId            = self.announcementId
            addMember.isMembershipActive = self.isMembershipActive
            addMember.famArr = famArr
            addMember.memberJoining = self.memberJoining
            addMember.invitationStatus = self.invitationStatus
            self.navigationController?.pushViewController(addMember, animated: false)
        }
        else if indexPath.row == 1{ // Announcement
            let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
            let addMember = storyboard.instantiateViewController(withIdentifier: "AnnouncementInFamilyViewController") as! AnnouncementInFamilyViewController
            addMember.groupId = self.groupID
            addMember.isAdmin = self.isAdmin
            addMember.link_type = self.link_type
            addMember.islinkable = self.islinkable
            addMember.facate = self.facate
            addMember.famSettings = famSettings
            addMember.announcementSet = announcementSet
            addMember.postId                    = self.postId
            addMember.announcementId            = self.announcementId
            addMember.isMembershipActive = self.isMembershipActive
            addMember.famArr = famArr
            addMember.memberJoining = self.memberJoining
            addMember.invitationStatus = self.invitationStatus
            self.navigationController?.pushViewController(addMember, animated: false)
        }
        else if indexPath.row == 2{ // Request
            let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
            let addMember = storyboard.instantiateViewController(withIdentifier: "RequestInFamilyViewController") as! RequestInFamilyViewController
            addMember.groupID = self.groupID
            addMember.isAdmin = self.isAdmin
            addMember.link_type = self.link_type
            addMember.islinkable = self.islinkable
            addMember.facate = self.facate
            addMember.famSettings = famSettings
            addMember.announcementSet = announcementSet
            addMember.postId                    = self.postId
            addMember.announcementId            = self.announcementId
            addMember.isMembershipActive = self.isMembershipActive
            addMember.famArr = famArr
            addMember.memberJoining = self.memberJoining
            addMember.invitationStatus = self.invitationStatus
            self.navigationController?.pushViewController(addMember, animated: false)
        }
        else if indexPath.row == 3{ // Events
            let storyboard = UIStoryboard.init(name: "third", bundle: nil)
            let addMember = storyboard.instantiateViewController(withIdentifier: "EventListingViewController") as! EventListingViewController
            addMember.groupID = self.groupID
            addMember.isAdmin = self.isAdmin
            addMember.link_type = self.link_type
            addMember.islinkable = self.islinkable
            addMember.facte = self.facate
            addMember.famSettings = famSettings
            addMember.announcementSet = announcementSet
            addMember.postId                    = self.postId
            addMember.announcementId            = self.announcementId
            addMember.isMembershipActive = self.isMembershipActive
            addMember.famArr = famArr
            addMember.memberJoining = self.memberJoining
            addMember.invitationStatus = self.invitationStatus
            self.navigationController?.pushViewController(addMember, animated: false)
        }
        else if indexPath.row == 5{ // About us
            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
            intro.groupId = self.groupID
            intro.isAboutUsClicked = true
            self.navigationController?.pushViewController(intro, animated: false)
        }
        else if indexPath.row == 8{ // Members
            if isMembershipActive{
                if isAdmin.lowercased() == "admin"{
                    let storyboard = UIStoryboard.init(name: "MembershipMngmnt", bundle: nil)
                    let addMember = storyboard.instantiateViewController(withIdentifier: "MemebrshipDashboardViewController") as! MemebrshipDashboardViewController
                    addMember.groupID = self.groupID
                    addMember.isAdmin = self.isAdmin
                    addMember.link_type = self.link_type
                    addMember.islinkable = self.islinkable
                    //                        addMember.isFromdocument = true
                    addMember.facate = self.facate
                    addMember.famSettings = famSettings
                    addMember.announcementSet = announcementSet
                    addMember.postId                    = self.postId
                    addMember.announcementId            = self.announcementId
                    addMember.isMembershipActive = self.isMembershipActive
                    addMember.famArr = famArr
                    addMember.memberJoining = self.memberJoining
                    addMember.invitationStatus = self.invitationStatus
                    self.navigationController?.pushViewController(addMember, animated: true)
                }
                else{
                    let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                    let addMember = storyboard.instantiateViewController(withIdentifier: "AddMembersNewViewController") as! AddMembersNewViewController
                    addMember.groupId = self.groupID
                    addMember.isAdmin = self.isAdmin
                    addMember.link_type = self.link_type
                    addMember.islinkable = self.islinkable
                    addMember.faCate = self.facate
                    addMember.famSettings = famSettings
                    addMember.announcementSet = announcementSet
                    addMember.postId                    = self.postId
                    addMember.announcementId            = self.announcementId
                    addMember.isMembershipActive = self.isMembershipActive
                    addMember.famArr = famArr
                    addMember.memberJoiningStatus = self.memberJoining
                    addMember.invitationStatus = self.invitationStatus
                    self.navigationController?.pushViewController(addMember, animated: false)
                }
            }
            else{
                let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "AddMembersNewViewController") as! AddMembersNewViewController
                addMember.groupId = self.groupID
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.faCate = self.facate
                addMember.famSettings = famSettings
                addMember.announcementSet = announcementSet
                addMember.postId                    = self.postId
                addMember.announcementId            = self.announcementId
                addMember.isMembershipActive = self.isMembershipActive
                addMember.famArr = famArr
                addMember.memberJoiningStatus = self.memberJoining
                addMember.invitationStatus = self.invitationStatus
                self.navigationController?.pushViewController(addMember, animated: false)
            }

        }
        else if indexPath.row == 9{
            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            let addMember = storyboard.instantiateViewController(withIdentifier: "AddMembersNewViewController") as! AddMembersNewViewController
            addMember.groupId = self.groupID
            addMember.isAdmin = self.isAdmin
            addMember.link_type = self.link_type
            addMember.islinkable = self.islinkable
            addMember.faCate = self.facate
            addMember.famSettings = famSettings
            addMember.announcementSet = announcementSet
            addMember.postId                    = self.postId
            addMember.announcementId            = self.announcementId
            addMember.isMembershipActive = self.isMembershipActive
            addMember.famArr = famArr
            addMember.memberJoiningStatus = self.memberJoining
            addMember.invitationStatus = self.invitationStatus
            self.navigationController?.pushViewController(addMember, animated: false)
        }
        else if indexPath.row == 4{ // Albums
            let storyboard = UIStoryboard.init(name: "third", bundle: nil)
            let addMember = storyboard.instantiateViewController(withIdentifier: "AlbumListingViewController") as! AlbumListingViewController
            addMember.groupID = groupID
            addMember.isAdmin = self.isAdmin
            addMember.link_type = self.link_type
            addMember.islinkable = self.islinkable
            addMember.facate = self.facate
            addMember.famSettings = famSettings
            addMember.announcementSet = announcementSet
            addMember.postId                    = self.postId
            addMember.announcementId            = self.announcementId
            addMember.isMembershipActive = self.isMembershipActive
            addMember.famArr = famArr
            addMember.memberJoining = self.memberJoining
            addMember.invitationStatus = self.invitationStatus
            self.navigationController?.pushViewController(addMember, animated: false)
        }
        else if indexPath.row == 6{ // Documents
            let storyboard = UIStoryboard.init(name: "third", bundle: nil)
            let addMember = storyboard.instantiateViewController(withIdentifier: "DocumentListingViewController") as! DocumentListingViewController
            addMember.groupID = self.groupID
            addMember.isAdmin = self.isAdmin
            addMember.link_type = self.link_type
            addMember.islinkable = self.islinkable
            addMember.isFromdocument = true
            addMember.facate = self.facate
            addMember.famSettings = famSettings
            addMember.announcementSet = announcementSet
            addMember.postId                    = self.postId
            addMember.announcementId            = self.announcementId
            addMember.isMembershipActive = self.isMembershipActive
            addMember.famArr = famArr
            addMember.memberJoining = self.memberJoining
            addMember.invitationStatus = self.invitationStatus
            self.navigationController?.pushViewController(addMember, animated: false)
        }
        else{ // Linked families
            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            let addMember = storyboard.instantiateViewController(withIdentifier: "LinkedFamiliesListViewController") as! LinkedFamiliesListViewController
            addMember.groupID = self.groupID
            addMember.isAdmin = self.isAdmin
            addMember.link_type = self.link_type
            addMember.islinkable = self.islinkable
            addMember.facate = self.facate
            addMember.famSettings = famSettings
            addMember.announcementSet = announcementSet
            addMember.postId                    = self.postId
            addMember.announcementId            = self.announcementId
            addMember.isMembershipActive = self.isMembershipActive
            addMember.famArr = famArr
            addMember.memberJoining = self.memberJoining
            addMember.invitationStatus = self.invitationStatus
            self.navigationController?.pushViewController(addMember, animated: false)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = aboutArr[indexPath.row].size(withAttributes: nil)
        return CGSize(width: w.width, height: 60)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension LinkedFamiliesListViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        if selectedIndex == 1{
            return 1
        }
        else{
            return dataArr.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // return FamilyArr.familyList!.count
        if selectedIndex == 1{
            return self.requestArr.count
        }
        else{
            return 0
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if selectedIndex == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddMembers_RequestsTableViewCell", for: indexPath) as! AddMembers_RequestsTableViewCell
            cell.lblName.text = requestArr[indexPath.row].fullname
            
            if requestArr[indexPath.row].type.lowercased() == "request"{
                cell.lblLocation.text = "wants to join this family"
            }
            else{
               // cell.lblLocation.text = "wants to link his family \(requestArr[indexPath.row].groupName) to this group"
                
                cell.lblLocation.text = "has requested to link \"\(requestArr[indexPath.row].groupName)\" family with this family"
            }
            if requestArr[indexPath.row].logo.count != 0{
                let temp = "\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+requestArr[indexPath.row].logo.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                let imgUrl = URL(string: temp)
                //                cell.imageViewAvatar.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
                let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+imgUrl!.relativeString
                let url = URL(string: newUrlStr)
                cell.imageViewAvatar.kf.indicatorType = .activity

                cell.imageViewAvatar.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
            }else{
                cell.imageViewAvatar.image = #imageLiteral(resourceName: "Family Logo")
            }
            cell.btnAccept.tag = indexPath.row
            cell.btnReject.tag = indexPath.row
            
            return cell
        }
        else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if selectedIndex == 0{
            let header = self.tblListView.dequeueReusableHeaderFooterView(withIdentifier: "FamilyListingView") as! FamilyListingView
            let data = dataArr[section]
        
            
            header.btnAction.tag = section
            header.btnView.tag = section
            
            //            header.btnView.isHidden = true
            //  header.lblCreated_height.constant = 0
            //            header.btnAction_btm.constant = 20
            
            if isFromBlocked{
                header.byTitle.isHidden = true
                header.lblRegion.isHidden = true
                header.imgLocation.isHidden = true
                header.lblCreatedBy.isHidden = true
                header.lblMembersCount.isHidden = true
                header.lblKnownHead.isHidden = true
                header.lblKnown.isHidden = true
                header.lblMemberHead.isHidden = true
                
                //                header.buttontoRoghtConstraint.priority = UILayoutPriority(rawValue: 900)
                header.lblType.isHidden = true
                header.lblTitle.text = data["full_name"].string ?? "Unknown"
                if data["propic"].stringValue.count != 0{
                    let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+data["propic"].stringValue.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                    let imgurl = URL(string: temp)
                    header.imgLogo.kf.indicatorType = .activity

                    header.imgLogo.kf.setImage(with: imgurl, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
                }
                else{
                    header.imgLogo.image = UIImage(named: "Male Colored")
                }
                header.btnAction.setTitle("Unblock", for: .normal)
                header.btnAction.isHidden = false
                header.btnAction.addTarget(self, action: #selector(onClickUnlinkAction(_:)), for: .touchUpInside)
                //                header.btnView.addTarget(self, action: #selector(onClickViewFamily(_:)), for: .touchUpInside)
            }
            else{
                header.byTitle.isHidden = false
                header.lblRegion.isHidden = false
                header.imgLocation.isHidden = false
                header.lblCreatedBy.isHidden = false
                header.lblKnownHead.isHidden = true
                header.lblKnown.isHidden = true
                
                header.byTitle.text = "By"
                header.lblCreatedBy.text =  data["requested_by_name"].stringValue
                header.lblTitle.text = data["group_name"].stringValue
                header.lblRegion.text = data["base_region"].stringValue
                header.lblType.text = data["group_category"].stringValue
                
                let memberCount = data["member_count"].intValue
                if memberCount > 0{
                    header.lblMembersCount.isHidden = false
                    header.lblMemberHead.isHidden = false
                    header.lblMembersCount.text = data["member_count"].stringValue
                }
                else{
                    header.lblMembersCount.isHidden = true
                    header.lblMemberHead.isHidden = true
                }
                
                if data["logo"].stringValue.count != 0{
                    let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=150&height=150&url="+"\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+data["logo"].stringValue.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                    let imgurl = URL(string: temp)
                    header.imgLogo.kf.indicatorType = .activity

                    header.imgLogo.kf.setImage(with: imgurl, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
                }
                else{
                    header.imgLogo.image = #imageLiteral(resourceName: "imgNoImage")
                }
                
                if data["status"].stringValue.lowercased() == "accepted"{
                    header.btnAction.setTitle("UnLink", for: .normal)
                }
                else{
                    header.btnAction.setTitle("Link", for: .normal)
                }
                header.btnAction.isHidden = false
                header.btnAction.addTarget(self, action: #selector(onClickUnlinkAction(_:)), for: .touchUpInside)
                header.btnView.addTarget(self, action: #selector(onClickViewFamily(_:)), for: .touchUpInside)
            }
            
            // header.lblType.text = FamilyArr.familyList![section].faType
            //  header.lblCreatedBy.text = "\(FamilyArr.familyList![section].faAdmin)"
            // header.lblMembersCount.text = "\(FamilyArr.familyList![section].memberCount)"
            // header.lblMembersCount.text = FamilyArr.familyList![section].memberCount
            // header.lblKnown.text = FamilyArr.familyList![section].knownCount
            
            // header.btnView.addTarget(self, action: #selector(onClickViewFamily(_:)), for: .touchUpInside)
            
            return header
        }
        else{
            return UIView()
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedIndex == 1{
            let data = requestArr[indexPath.row]
            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
            intro.groupId = "\(requestArr[indexPath.row].groupId)"
            intro.isFromFamilyList = true
            appDel.isFromLinkedFam = true
            appDel.currentGrpID = self.groupID
            appDel.crntGrpName = appDel.groupNamePublic
            appDel.crntgroupImageUrlPublic = appDel.groupImageUrlPublic
            
            self.navigationController?.pushViewController(intro, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectedIndex == 1{
            return 110
        }
        else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if isFromBlocked{
            return 110
        }
        else{
            if selectedIndex == 0{
                return 150
            }
            else{
                return 0
            }
        }
        /*if selectedIndex == 0{
         return 150
         }
         else{
         return 150
         }*/
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
}

extension LinkedFamiliesListViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        textField.resignFirstResponder()
        if textField == txtSearch
        {
            // selectWebAPI()
            if textField.text!.count > 0{
                btnSearchReset.isHidden = false
            }
            else{
                btnSearchReset.isHidden = true
            }
            textField.endEditing(true)
        }
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        self.view.endEditing(true)
        if textField == txtSearch
        {
            if textField.text!.count > 0{
                btnSearchReset.isHidden = false
            }
            else{
                btnSearchReset.isHidden = true
            }
            // getFamilyList()
        }
        if isFromBlocked{
            getAllBlockedUsers()
        }
        else{
            getAllLinkedFamilies()
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
            // selectWebAPI()
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
        }
        return true
    }
}
