//
//  membershipPaymentViewController.swift
//  familheey
//
//  Created by familheey on 24/07/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON

protocol membershipPaymentDelegate:class {
    func updateMembershipPayment()
}

class membershipPaymentViewController: UIViewController {
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var lblStartFrom: UILabel!
    @IBOutlet weak var lblValidTill: UILabel!
    @IBOutlet weak var lblFees: UILabel!
    @IBOutlet weak var viewOfPaynow: UIView!
    @IBOutlet weak var viewOfPayComplete: UIView!
    @IBOutlet weak var txtNote: UITextView!
    @IBOutlet weak var viewOfOfflineDetails: UIControl!
    @IBOutlet weak var txtOfflineNote: UITextView!
    @IBOutlet weak var lblPaidAmount: UILabel!
    @IBOutlet weak var txtAmount: UITextField!
    
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    weak var delegate:membershipPaymentDelegate?
    
    var familyArr : [familyModel]?
    var accountArr = JSON()
    var isStripeAccountValidate = false
    var paymentMethod = ""
    var paymentType = ""
    var paymentNote = ""
    var amountPaid = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblType.text = ": \(familyArr![0].membershipName)"
        lblDuration.text = ": \(familyArr![0].membership_period_type)"
        lblStartFrom.text = ": \(Helpers.convertTimeStampWithoutTime(dateAsString: familyArr![0].membershipFrom))"
        lblValidTill.text = ": \(Helpers.convertTimeStampWithoutTime(dateAsString: familyArr![0].membershipTo))"
        lblFees.text = ": \(familyArr![0].membershipFee)"
        txtNote.text = familyArr![0].membership_payment_notes
        lblPaidAmount.text = ": \(familyArr![0].membership_total_payed_amount)"
        let dueAmount = familyArr![0].membershipFee - familyArr![0].membership_total_payed_amount
        txtAmount.text = "\(dueAmount)"
        
        if familyArr![0].membership_payment_status.lowercased() == "completed" || familyArr![0].membership_payment_status.lowercased() == "success"{
            viewOfPaynow.isHidden = true
            viewOfPayComplete.isHidden = false
        }
        else{
            viewOfPaynow.isHidden = false
            viewOfPayComplete.isHidden = true
        }
        let stripeAcnt = familyArr![0].stripe_account_id
        if stripeAcnt.isEmpty{
            
        }
        else{
            self.getStripeAccountByID()
        }
    }
    
    //MARK:- button Actions
    
    @IBAction func onTouchOutterView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickPayNow(_ sender: Any) {
        
        if txtAmount.text!.trimmingCharacters(in: .whitespacesAndNewlines).count != 0 && txtAmount!.text != "0" {
            let amount = Int(txtAmount.text!)
            if amount! > familyArr![0].membershipFee{
                self.displayAlertChoice(alertStr: "Enterd amount is greater than membership fee.", title: "") { (result) in
                    return
                }
            }
            else{
                amountPaid = txtAmount.text!
                let alert = UIAlertController(title: "Choose an Option", message: nil, preferredStyle: .actionSheet)
                
                if self.isStripeAccountValidate{
                    alert.addAction(UIAlertAction(title: "Online", style: .default, handler: { _ in
                        self.paymentType = "ONLINE"
                        self.onlinePaymentIntent()
                    }))
                }

                alert.addAction(UIAlertAction(title: "Cash", style: .default, handler: { _ in
                    self.paymentMethod = "cash"
                    self.paymentType = "OFFLINE"
                    self.offlinePaymentNote()
                }))
                alert.addAction(UIAlertAction(title: "Cheque", style: .default, handler: { _ in
                    self.paymentMethod = "cheque"
                    self.paymentType = "OFFLINE"
                    self.offlinePaymentNote()
                }))
                alert.addAction(UIAlertAction(title: "Others", style: .default, handler: { _ in
                    self.paymentMethod = "others"
                    self.paymentType = "OFFLINE"
                    self.offlinePaymentNote()
                }))
                
                alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
                /*If you want work actionsheet on ipad
                 then you have to use popoverPresentationController to present the actionsheet,
                 otherwise app will crash on iPad */
                
                self.present(alert, animated: true, completion: nil)
            }
        }
        else{
            displayAlert(alertStr: "Please enter a valid amount", title: "")
        }

    }
    
    @IBAction func onClckCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickOfflineDetailsSave(_ sender: Any) {
        self.paymentNote = txtOfflineNote.text
        createOfflinePayment()
    }
    
    @IBAction func onClickOfflineDetailsCancel(_ sender: Any) {
        viewOfOfflineDetails.isHidden = true
    }
    
    @IBAction func onTouchOfflineOutter(_ sender: Any) {
        viewOfOfflineDetails.isHidden = true
    }
    
    //MARK :- Custom methods
    func offlinePaymentNote(){
        txtOfflineNote.text = ""
        viewOfOfflineDetails.isHidden = false
    }
    
    // MARK:- API Methods
    func getStripeAccountByID(){
        // let gId = self.selectedIds[0] as! String
        
        let parameter = ["group_id":"\(familyArr![0].faId)"] as [String : Any]
        print(parameter)
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.stripeGetaccountById(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                do {
                    let jsonData = JSON(response.data as Any)
                    print("!!!!@ StripeAccount--- \(jsonData)")
                    
                    if response.statusCode == 200{
                        self.accountArr = jsonData
                        if jsonData.count > 0{
                            if let payouts = jsonData["payouts_enabled"].bool, let charges = jsonData["charges_enabled"].bool, payouts && charges{
                                self.isStripeAccountValidate = true
                            }
                            else{
                                self.isStripeAccountValidate = false
                            }
                            //                            self.tblItemList.reloadData()
                        }
                        ActivityIndicatorView.hiding()
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getStripeAccountByID()
                        }
                    }
                } catch let err {
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
    func createOfflinePayment(){
        self.view.endEditing(true)
        ActivityIndicatorView.show("Please wait....")
        let param = ["id":"\(familyArr![0].group_map_id)", "membership_customer_notes":paymentNote,"membership_payment_method":paymentMethod,"membership_payment_type":paymentType,"membership_payed_amount":amountPaid] as [String : Any]
        print(param)
        networkProvider.request(.groupMapUpdate(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        self.delegate?.updateMembershipPayment()
                        self.dismiss(animated: true, completion: nil)
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.createOfflinePayment()
                        }
                    }
                    else{
                        self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                    
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
        
    }
    
    func onlinePaymentIntent(){
        let temp = Int(amountPaid)
        let amount = temp! * 100
        ActivityIndicatorView.show("Please wait....")
        let param = ["amount":amount, "group_id":"\(familyArr![0].faId)","user_id":UserDefaults.standard.value(forKey: "userId") as! String,"to_type":"membership","to_subtype":"item","to_type_id":"\(familyArr![0].group_map_id)","to_subtype_id":"","os":"ios","contributionId":"\(familyArr![0].group_map_id)"] as [String:Any]
        print(param)
        
        networkProvider.request(.stripeCreatePaymentIntent(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        
                        if let urlStr = jsonData["weburl"].string, !urlStr.isEmpty{
                            appDel.isRequestUpdated = true
                            appDel.paymentStart = true
                            
                            let url = URL(string: urlStr)
                            UIApplication.shared.open(url!)
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.onlinePaymentIntent()
                        }
                    }
                    else{
                        self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                    
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
