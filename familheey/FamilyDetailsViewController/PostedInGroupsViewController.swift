//
//  PostedInGroupsViewController.swift
//  familheey
//
//  Created by familheey on 28/11/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON
import Kingfisher
import SafariServices


//PostsInUserProfileViewController

class PostedInGroupsViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,familySettingUpdateDelegate ,postUpdateDelegate ,updateAfterFamilySettingsEdit {
    
    @IBAction func onClickProfileAction(_ sender: UIButton) {
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "UserProfileTableViewController") as! UserProfileTableViewController
        
//        let userId = UserDefaults.standard.value(forKey: "userId") as! String
        print(sender.tag)
        vc.userID = "\(sender.tag)"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBOutlet weak var viewOfStickyPost: UIView!
    
    @IBOutlet weak var collVwOfStickyPost: UICollectionView!
    @IBOutlet weak var heightOfStickyPost: NSLayoutConstraint!
    var ShareOptionArray = [String]()
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    var ArrayPosts = [JSON]()
    var ArrayStickyPosts = [JSON]()
    var documentInteractionController: UIDocumentInteractionController!
    var searchTxt = ""
    var fromRefresh = false
    var groupId = ""
    let maxNumberOfLines = 2
    var refreshControl = UIRefreshControl()
    var famSettings = 0
    var announcementSet = 0
    
    var initiallimit = 20
    var offset = 0
    var limit = 0
    var lastFetchedIndex = 0;
    var postoptionsTittle = [String]()
    var isMembershipActive = false
    var link_type = 0
    var isAdmin = ""
    var islinkable:Int = Int()
    var facate = ""
    var arr = [String]()
    var aboutArr = ["FEED","ANNOUNCEMENT","REQUEST","EVENTS","ALBUMS","ABOUT US","DOCUMENTS","LINKED FAMILIES","MEMBERS"]
    var ActiveTab = 0
    var postId                          = 0
    var announcementId                  = 0
    var isNewDataLoading = true
    var stopPagination = false
    var famArr : [familyModel]?
    var memberJoining = 0
    var invitationStatus = 0
    
    @IBOutlet weak var lblHead: UILabel!
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearchRest: UIButton!
    @IBOutlet weak var tblListView: UITableView!
    @IBOutlet weak var aboutClctnVew: UICollectionView!
    @IBOutlet weak var createPostbutton: UIButton!
    @IBOutlet weak var imgSettings: UIImageView!
    @IBOutlet weak var lblJoin: UILabel!
    @IBOutlet weak var btnSettings: UIButton!
    
    
    var collectionViewLayout = UICollectionViewFlowLayout()
    var collectionViewLayoutForStickPost = UICollectionViewFlowLayout()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isAdmin.lowercased() == "not-member"{
            aboutArr = ["FEED","ANNOUNCEMENT","ABOUT US"]
            self.createPostbutton.isHidden = true
            imgSettings.isHidden = true
            lblJoin.isHidden = false
            
            if memberJoining == 1{
                self.lblJoin.text = "Private"
                self.btnSettings.isEnabled = false
            }
            else{
                if invitationStatus == 1{
                    self.lblJoin.text = "Pending"
                    self.btnSettings.isEnabled = true
                }
                else{
                    self.lblJoin.text = "Join"
                    self.btnSettings.isEnabled = true
                }
            }
        }
        else{
            imgSettings.isHidden = false
            lblJoin.isHidden = true
            
            if isAdmin.lowercased() == "admin"{
                if isMembershipActive{
                    aboutArr = ["FEED","ANNOUNCEMENT","REQUEST","EVENTS","ALBUMS","ABOUT US","DOCUMENTS","LINKED FAMILIES","MEMBERSHIP","MEMBERS"]
                }
            }
        }
        
        aboutClctnVew.delegate = self
        aboutClctnVew.dataSource = self
        aboutClctnVew.reloadData()
        collectionViewLayout.scrollDirection = .horizontal
        collectionViewLayout.minimumLineSpacing = 0
        self.aboutClctnVew.setCollectionViewLayout(collectionViewLayout, animated: true)
        
        collectionViewLayoutForStickPost.scrollDirection = .horizontal
        collectionViewLayoutForStickPost.minimumLineSpacing = 0
        
        
        self.collVwOfStickyPost.setCollectionViewLayout(collectionViewLayoutForStickPost, animated: true)
        
        limit = initiallimit
        lastFetchedIndex = initiallimit
        //        viewAnnouncement.isHidden = false
        txtSearch.delegate = self
        
        Helpers.setleftView(textfield: txtSearch, customWidth: 30)
        ArrayPosts.removeAll()
        
        refreshControl.attributedTitle = NSAttributedString(string: "Please wait...")
        refreshControl.addTarget(self, action: #selector(refreshPost), for: .valueChanged)
        tblListView.addSubview(refreshControl)
        
        self.lblHead.text = appDel.groupNamePublic
        let temp = appDel.groupImageUrlPublic.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let imgUrl = URL(string: temp)
        if imgUrl != nil{
            self.imgCover.kf.indicatorType = .activity
            
            let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=400&height=300&url="+imgUrl!.relativeString
            let urlImg = URL(string: newUrlStr)
            
            self.imgCover.kf.setImage(with: urlImg, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        else{
            self.imgCover.image = #imageLiteral(resourceName: "family_default_cover.png")
        }
        //        getPosts()
        print(self.famArr!)
    }
    override func viewDidLayoutSubviews(){
        super.viewDidLayoutSubviews()
        aboutClctnVew.reloadData()
        //        let indexPath = IndexPath(item: ActiveTab, section: 0)
        //        aboutClctnVew.scrollToItem(at: indexPath, at:.centeredHorizontally, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.viewOfStickyPost.isHidden  = true
        self.heightOfStickyPost.constant = 0
        self.collVwOfStickyPost.isHidden = true
        offset = 0
        getPosts()
        getStikyPost()
        
    }
    //MARK:- Custom Methods
    @objc func refreshPost(refreshControl: UIRefreshControl) {
        refreshControl.endRefreshing()
        fromRefresh = true
        getPosts()
    }
    func insertRowsAtTableView(responseCount: Int){
        
        var indexPaths = [IndexPath]()
        
        for i in 0 ..< responseCount{
            let indx = ArrayPosts.count - responseCount
            let indexPath = indx + i
            indexPaths.append(IndexPath(row: indexPath, section: 0))
        }
        
        print(indexPaths)
        self.tblListView.beginUpdates()
        self.tblListView.insertRows(at: indexPaths, with: .none)
        self.tblListView.endUpdates()
    }
    func getPosts(){
        if offset == 0{
            ArrayPosts.removeAll()
        }
        self.stopPagination = false
        
        if offset == 0{
            ActivityIndicatorView.show("Please wait....")
        }
        else{
            tblListView.showLoading()
        }
        let param = ["user_id" : UserDefaults.standard.value(forKey: "userId") as! String,"offset":"\(offset)","limit":"\(limit)","type":"post", "query":self.txtSearch.text!,"group_id":self.groupId]
        print(param)
        networkProvider.request(.postByFamily(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    self.isNewDataLoading = false
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        if self.offset == 0{
                            ActivityIndicatorView.hiding()
                        }else{
                            self.tblListView.hideLoading()
                        }
                        print("view contents : \(jsonData)")
                        //      "orgin_id" : 2426,
                        
                        if let response = jsonData["data"].array{
                            
                            if response.count == 0{
                                self.stopPagination = true
                            }
                            self.ArrayPosts.append(contentsOf: response)
                            
                            if self.offset != 0{
                                if response.count > 0 {
                                    self.insertRowsAtTableView(responseCount: response.count)
                                }
                            }
                            
                            if self.ArrayPosts.isEmpty {
                                
                                self.tblListView.isHidden = true
                            } else {
                                
                                self.tblListView.isHidden = false
                                self.tblListView.delegate = self
                                self.tblListView.dataSource = self
                                self.tblListView.reloadData()
                            }
                        } else  {
                            
                            self.tblListView.isHidden = true
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getPosts()
                            
                        }
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    func getStikyPost(){
        
        ActivityIndicatorView.show("Please wait....")
        
        let param = ["user_id" : UserDefaults.standard.value(forKey: "userId") as! String,"group_id":self.groupId]
        print(param)
        networkProvider.request(.getStickyPostList(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        
                        print("view contents : \(jsonData)")
                        //      "orgin_id" : 2426,
                        
                        if let response = jsonData.array{
                            self.ArrayStickyPosts = response
                            if self.ArrayStickyPosts.count == 0{
                                self.viewOfStickyPost.isHidden  = true
                                self.heightOfStickyPost.constant = 0
                                self.collVwOfStickyPost.isHidden = true
                            }
                            else
                            {
                                self.viewOfStickyPost.isHidden  = false
                                self.heightOfStickyPost.constant = 75
                                self.collVwOfStickyPost.isHidden = false
                                self.collVwOfStickyPost.dataSource = self
                                self.collVwOfStickyPost.delegate = self
                                self.collVwOfStickyPost.reloadData()
                                //                                self.tblListView.dataSource = self
                                //                                self.tblListView.reloadData()
                                
                            }

                        } else  {
                            
                            self.tblListView.isHidden = true
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getStikyPost()
                            
                        }
                    }
                    else if response.statusCode == 500{
                        self.viewOfStickyPost.isHidden  = true
                        self.heightOfStickyPost.constant = 0
                        self.collVwOfStickyPost.isHidden = true
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    func getFamilyDetails(groupId:String){
        APIServiceManager.callServer.getFamilyDetails(url: EndPoint.getFamily, groupId: groupId, userId:UserDefaults.standard.value(forKey: "userId") as! String, success: { (responseMdl) in
            guard let familyMdl = responseMdl as? familyListResponse else{
                return
            }
            ActivityIndicatorView.hiding()
            if familyMdl.statusCode == 200{
                self.famArr = familyMdl.familyList
                print(self.famArr!)
                self.isAdmin = self.famArr![0].isAdmin
                self.link_type = self.famArr![0].linkFamily
                self.islinkable = self.famArr![0].islinkable
                if self.famArr![0].faCategory.lowercased() == "regular"{
                    self.facate = "regular"
                }
                self.famSettings  = self.famArr![0].postCreate
                self.memberJoining = self.famArr![0].memberJoining
                self.invitationStatus = self.famArr![0].invitationStatus
                self.announcementSet = self.famArr![0].announcementCreate
                self.postId = self.famArr![0].postCreate
                self.announcementId = self.famArr![0].announcementCreate
                self.isMembershipActive = self.famArr![0].isMembershipActive
            }
        }) { (error) in
            
        }
    }
    
    // Open App Link
    func openAppLink(url:String){
        
        let param = ["url" : url]
        
        networkProvider.request(.openAppGetParams(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("@@##!!! REsult\(jsonData)")
                    if response.statusCode == 200{
                        let dataArr = jsonData["data"]
                        let subType = dataArr["sub_type"].stringValue
                        let type =  dataArr["type"].stringValue
                        
                        if subType == notificationSubTypes.publicfeed || subType == notificationSubTypes.familyfeed && type == notificationTypes.home{
                            let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
                            vc.postId = "\(dataArr["type_id"].intValue)"
                            self.navigationController?.pushViewController(vc, animated: false)
                            
                        }
                        else if type == notificationTypes.family{
                            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                            let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
                            intro.groupId = "\(dataArr["type_id"].intValue)"
                            self.navigationController?.pushViewController(intro, animated: false)
                        }
                        else{
                            let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
                            vc.enableEventEdit = false
                            vc.isFromNotification = "true"
                            vc.eventId = "\(dataArr["type_id"].intValue)"
                            self.navigationController?.pushViewController(vc, animated: false)
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.openAppLink(url: url)
                            
                        }
                    }
                    else{
                        
                    }
                    
                }catch _ {
                    
                    
                }
            case .failure( _):
                break
            }
        }
    }
    
    //MARK:- custom
    func deletePostForCreator(tag:Int){
        let alert = UIAlertController(title: "Familheey", message: "Do you really want to delete this post? Deleting this post will delete it from everywhere you posted it to and you will lose all associated conversations", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "DELETE", style: .destructive, handler: { _ in
            let post = self.ArrayPosts[tag]
            
            
            let param = ["id" : post["post_id"].stringValue,"user_id":UserDefaults.standard.value(forKey: "userId")as! String]
            
            ActivityIndicatorView.show("Please wait....")
            self.networkProvider.request(.deletePost(parameter: param)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                        print("view contents : \(jsonData)")
                        if response.statusCode == 200{
                            print("view contents : \(jsonData)")
                            self.offset = 0
                            self.getPosts()
                        }
                        else if response.statusCode == 401 {
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                self.deletePostForCreator(tag: tag)
                            }
                        }
                        ActivityIndicatorView.hiding()
                        
                    }catch let err {
                        ActivityIndicatorView.hiding()
                        // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                    }
                case .failure( let error):
                    ActivityIndicatorView.hiding()
                    //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                    break
                }
            }
            
        }))
        alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func applyForStickyPost(tag:Int,type:String){
        var param = [String:Any]()
        if type == "stick"
        {
            let post = self.ArrayPosts[tag]
            print(post)
            param = ["post_id" : post["post_id"].stringValue,"group_id":self.groupId,"type":type]
        }
        else{
            let post = self.ArrayStickyPosts[tag]
            param = ["post_id" : post["post_id"].stringValue,"group_id":self.groupId,"type":type]
        }
        print(param)
        ActivityIndicatorView.show("Please wait....")
        self.networkProvider.request(.applyStickyPost(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        print("view contents : \(jsonData)")
                        //                        self.getPosts()
                        self.getStikyPost()
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.applyForStickyPost(tag: tag, type: type)
                        }
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
        
        
    }
    
    func removePostFromTimeline(tag:Int){
        let post = ArrayPosts[tag]
        
        let param = ["post_id" : post["post_id"].stringValue, "user_id": UserDefaults.standard.value(forKey: "userId") as! String]
        
        ActivityIndicatorView.show("Please wait....")
        networkProvider.request(.removePost(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        print("view contents : \(jsonData)")
                        self.offset = 0
                        self.getPosts()
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.removePostFromTimeline(tag: tag)
                        }
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    func viewCurrentPost(index:Int){
        
        if  ArrayPosts.count >= index  {
            
            let post = ArrayPosts[index]
            
            let param = ["post_id" : post["post_id"].stringValue, "user_id": UserDefaults.standard.value(forKey: "userId") as! String]
            
            //               ActivityIndicatorView.show("Please wait....")
            networkProvider.request(.add_view_count(parameter: param)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                        print("view contents : \(jsonData)")
                        if response.statusCode == 200{
                            print("view contents : \(jsonData)")
                            //                               self.getPosts()
                        }
                        else if response.statusCode == 401 {
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                self.viewCurrentPost(index: index)
                            }
                        }
                        
                        //                           ActivityIndicatorView.hiding()
                        
                    }catch let err {
                        //                           ActivityIndicatorView.hiding()
                        // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                    }
                case .failure( let error):
                    //                       ActivityIndicatorView.hiding()
                    //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                    break
                }
            }
        }
    }
    
    //MARK: UIButton Actions
    @IBAction func onClickBack(_ sender: Any) {
        //        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        //               let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
        //               intro.groupId = self.groupId
        //               self.navigationController?.pushViewController(intro, animated: false)
//        appDel.fromFamilyDetailBack = true
        self.navigationController?.popToViewController(ofClass: FamilyDetailsTableViewController.self)
    }
    @IBAction func onClickSearchRest(_ sender: Any) {
        txtSearch.text = ""
        searchTxt = ""
        self.offset = 0
        self.getPosts()
        
        btnSearchRest.isHidden = true
        self.txtSearch.endEditing(true)
    }
    
    @IBAction func onClickRightMenu(_ sender: UIButton) {
        let post = ArrayPosts[sender.tag]
        print(sender.tag)
        if let postType = post["publish_type"].string, postType.lowercased() == "request" || postType.lowercased() == "albums" || postType.lowercased() == "documents" {
            if isAdmin.lowercased() == "not-member"{
                postoptionsTittle =  ["Report"]
                showActionSheet(titleArr: postoptionsTittle as NSArray, title: "Select options") { (index) in
                    if index == 0{
                        let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "ReportPostViewController") as! ReportPostViewController
                        
                        vc.postDetails =  self.ArrayPosts[sender.tag]
                        vc.isFrom = "post"
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    else if index == 100{
                        
                    }
                }
            }
            else{
                if post["created_by"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")" {
                    if isAdmin.lowercased() == "admin"{
                        
                        postoptionsTittle =  ["Stick this post"]
                    }
                }
                else{
                    if isAdmin.lowercased() == "admin"{
                        postoptionsTittle =  ["Report","Stick this post"]
                    }
                    else{
                        postoptionsTittle =  ["Report"]
                    }
                    
                }
                showActionSheet(titleArr: postoptionsTittle as NSArray, title: "Select options") { (index) in
                    
                    if index == 3{
                    }
                    else if index == 0{
                        if post["created_by"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")"{
                            
                            if self.isAdmin.lowercased() == "admin"{
                                let alert = UIAlertController(title: "Familheey", message: "Do you want to stick this post?)", preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "OK", style: .destructive, handler: { _ in
                                    self.applyForStickyPost(tag: sender.tag, type: "stick")
                                    
                                }))
                                alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: nil))
                                
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                        }else{
                            //                            self.removePostFromTimeline(tag: sender.tag)
                            let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "ReportPostViewController") as! ReportPostViewController
                            
                            vc.postDetails =  self.ArrayPosts[sender.tag]
                            vc.isFrom = "post"
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                    else if index == 1{
                        let alert = UIAlertController(title: "Familheey", message: "Do you want to stick this post ?", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .destructive, handler: { _ in
                            self.applyForStickyPost(tag: sender.tag, type: "stick")
                            
                        }))
                        alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: nil))
                        
                        self.present(alert, animated: true, completion: nil)
                    }
                    else if index == 100{
                    }
                    else{
                        
                    }
                }
            }
        }
        else{
            if isAdmin.lowercased() == "not-member"{
                postoptionsTittle =  ["Report"]
                showActionSheet(titleArr: postoptionsTittle as NSArray, title: "Select options") { (index) in
                    if index == 0{
                        let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "ReportPostViewController") as! ReportPostViewController
                        
                        vc.postDetails =  self.ArrayPosts[sender.tag]
                        vc.isFrom = "post"
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    else if index == 100{
                        
                    }
                }
            }
            else{
                if post["created_by"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")" {
                    if let sharedusers = post["shared_user_name"].string , !sharedusers.isEmpty{
                        
                        if self.isAdmin.lowercased() == "admin"{
                            postoptionsTittle =  ["Delete","Stick this post"]
                        }
                        else{
                            postoptionsTittle =  ["Delete"]
                        }
                        
                        
                    }
                    else{
                        if self.isAdmin.lowercased() == "admin"{
                            postoptionsTittle =  ["Edit","Delete","Stick this post"]
                        }
                        else{
                            postoptionsTittle =  ["Edit","Delete"]
                        }
                        
                    }
                    
                }
                else{
                    if isAdmin.lowercased() == "admin"{
                        postoptionsTittle =  ["Delete","Report","Stick this post"]
                    }
                    else{
                        postoptionsTittle =  ["Report"]
                    }
                }
                showActionSheet(titleArr: postoptionsTittle as NSArray, title: "Select options") { (index) in
                    
                    if index == 1{
                        if post["created_by"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")"{
                            if let sharedusers = post["shared_user_name"].string , !sharedusers.isEmpty{
                                if self.isAdmin.lowercased() == "admin"{
                                    let alert = UIAlertController(title: "Familheey", message: "Do you want to stick this post ?", preferredStyle: .alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: .destructive, handler: { _ in
                                        self.applyForStickyPost(tag: sender.tag, type: "stick")
                                        
                                    }))
                                    alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: nil))
                                    
                                    self.present(alert, animated: true, completion: nil)
                                }
                            }
                            else{
                                self.deletePostForCreator(tag: sender.tag)
                            }
                        }
                        else{
                            let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "ReportPostViewController") as! ReportPostViewController
                            
                            vc.postDetails =  self.ArrayPosts[sender.tag]
                            vc.isFrom = "post"
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                    else if index == 100{
                    }
                    else if index == 0{
                        if post["created_by"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")"{
                            if let sharedusers = post["shared_user_name"].string , !sharedusers.isEmpty{
                                self.deletePostForCreator(tag: sender.tag)
                            }
                            else{
                                let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "CreatePostViewController") as! CreatePostViewController
                                
                                vc.fromEdit = true
                                vc.editPostDetails = post
                                vc.callBackDelegate = self
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                        else{
                            if self.isAdmin.lowercased() == "admin"{
                                self.deletePostForCreator(tag: sender.tag)
                            }
                            else{
                                let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "ReportPostViewController") as! ReportPostViewController
                                
                                vc.postDetails =  self.ArrayPosts[sender.tag]
                                vc.isFrom = "post"
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                    }
                    else if index == 2{
                        print("sticky post")
                        
                        let alert = UIAlertController(title: "Familheey", message: "Do you want to stick this post ?", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .destructive, handler: { _ in
                            self.applyForStickyPost(tag: sender.tag, type: "stick")
                            
                        }))
                        alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: nil))
                        
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    else{
                    }
                }
            }
        }
        
    }
    @IBAction func onClickReadmoreAction(_ sender: UIButton) {
        
        let cell = tblListView.cellForRow(at: IndexPath.init(row: sender.tag, section: 0)) as! PostImageTableViewCell
        if cell.labelPost.calculateMaxLines() > maxNumberOfLines &&  cell.labelPost.numberOfLines == maxNumberOfLines{
            
            tblListView.beginUpdates()
            cell.labelPost.numberOfLines = 0
            cell.readmoreButton.setTitle("view less", for: .normal)
            tblListView.endUpdates()
            
            
        }
        else if cell.labelPost.calculateMaxLines() > maxNumberOfLines &&  cell.labelPost.numberOfLines == 0 {
            UIView.performWithoutAnimation {
                tblListView.beginUpdates()
                cell.labelPost.numberOfLines = maxNumberOfLines
                cell.readmoreButton.setTitle("view more", for: .normal)
                tblListView.endUpdates()
            }
        }
        
    }
    
    @IBAction func onClickReadmoreShareAction(_ sender: UIButton) {
        
        let cell = tblListView.cellForRow(at: IndexPath.init(row: sender.tag, section: 0)) as! SharedPostTableViewCell
        if cell.labelPost.calculateMaxLines() > maxNumberOfLines &&  cell.labelPost.numberOfLines == maxNumberOfLines{
            
            tblListView.beginUpdates()
            cell.labelPost.numberOfLines = 0
            cell.readmoreButton.setTitle("view less", for: .normal)
            tblListView.endUpdates()
            
            
        }
        else if cell.labelPost.calculateMaxLines() > maxNumberOfLines &&  cell.labelPost.numberOfLines == 0 {
            UIView.performWithoutAnimation {
                tblListView.beginUpdates()
                cell.labelPost.numberOfLines = maxNumberOfLines
                cell.readmoreButton.setTitle("view more", for: .normal)
                tblListView.endUpdates()
            }
        }
        
    }
    
    
    @IBAction func onClickConversation(_ sender: UIButton) {
        let post = ArrayPosts[sender.tag]
        let mainStoryBoard = UIStoryboard(name: "Conversations", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "ConversationsViewController") as! ConversationsViewController
        vc.post = post
        vc.conversationT = .post
        self.navigationController?.pushViewController(vc, animated:true)
    }
    @IBAction func onClickSharedUser(_ sender: UIButton) {
        let post = ArrayPosts[sender.tag]
        let mainStoryBoard = UIStoryboard(name: "fourth", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "UserFamilyListViewController") as! UserFamilyListViewController
        if post["orgin_id"].stringValue.count > 0{
            vc.postId = post["orgin_id"].stringValue
        }
        else{
            vc.postId = post["post_id"].stringValue
        }
        vc.isFrompost = true
        vc.isFrom = "usershare"
        self.navigationController?.pushViewController(vc, animated:true)
    }
    @IBAction func onClickViews(_ sender: UIButton) {
        let post = ArrayPosts[sender.tag]
        let mainStoryBoard = UIStoryboard(name: "fourth", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "UserFamilyListViewController") as! UserFamilyListViewController
        vc.postId = post["post_id"].stringValue
        vc.isFrompost = true
        vc.isFrom = "postview"
        self.navigationController?.pushViewController(vc, animated:true)
    }
    
    @IBAction func onClickPostShareAction(_ sender: UIButton) {
        //        let post = ArrayPosts[sender.tag]
        //
        //        let mainStoryBoard = UIStoryboard(name: "third", bundle: nil)
        //        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "PostShareViewController") as! PostShareViewController
        //        vc.postId = post["post_id"].stringValue
        //        self.navigationController?.pushViewController(vc, animated:true)
        //
        
        // New changes share with social media
        self.ShareOptionArray = ["Share in Familheey","Share in Social Media"]
        showActionSheet(titleArr: ShareOptionArray as NSArray, title: "Select options") { (index) in
            if index == 0{
                let post = self.ArrayPosts[sender.tag]
                print(post)
                //                 DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                let mainStoryBoard = UIStoryboard(name: "third", bundle: nil)
                let vc = mainStoryBoard.instantiateViewController(withIdentifier: "PostShareViewController") as! PostShareViewController
                vc.postId = post["post_id"].stringValue
                self.navigationController?.pushViewController(vc, animated: false)
                //                }
                //                 self.navigationController?.pushViewController(vc, animated: false)
            }
            else if index == 1{
                let post = self.ArrayPosts[sender.tag]
                let urlString =
                    Helpers.showActivityViewcontroller(url: "\(BaseUrl.PostUrlPrd)\(post["post_id"].stringValue)", VC: self)
            }
            else{
                
            }
        }
    }
    
    
    @IBAction func onClickFeeds(_ sender: Any) {
        getPosts()
    }
    @IBAction func onClickAnnouncement(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
        let addMember = storyboard.instantiateViewController(withIdentifier: "AnnouncementInFamilyViewController") as! AnnouncementInFamilyViewController
        addMember.groupId = self.groupId
        addMember.isAdmin = self.isAdmin
        addMember.link_type = self.link_type
        addMember.islinkable = self.islinkable
        addMember.facate = self.facate
        addMember.famSettings = famSettings
        addMember.announcementSet = announcementSet
        addMember.postId                    = self.postId
        addMember.announcementId            = self.announcementId
        addMember.famArr = famArr
        addMember.memberJoining = self.memberJoining
        addMember.invitationStatus = self.invitationStatus
        addMember.isMembershipActive = self.isMembershipActive
        self.navigationController?.pushViewController(addMember, animated: false)
    }
    @IBAction func onClickEvents(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "third", bundle: nil)
        let addMember = storyboard.instantiateViewController(withIdentifier: "EventListingViewController") as! EventListingViewController
        addMember.groupID = self.groupId
        addMember.isAdmin = self.isAdmin
        addMember.link_type = self.link_type
        addMember.islinkable = self.islinkable
        addMember.facte = self.facate
        addMember.famSettings = famSettings
        addMember.announcementSet = announcementSet
        addMember.postId                    = self.postId
        addMember.announcementId            = self.announcementId
        addMember.famArr = famArr
        addMember.memberJoining = self.memberJoining
        addMember.invitationStatus = self.invitationStatus
        addMember.isMembershipActive = self.isMembershipActive
        //        addMember.faCate = self.familyArr![0].faCategory
        //        addMember.memberJoining = self.familyArr![0].memberApproval
        self.navigationController?.pushViewController(addMember, animated: false)
    }
    @IBAction func onClickAboutus(_ sender: Any) { // Changed to memebrs
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let addMember = storyboard.instantiateViewController(withIdentifier: "AddMembersNewViewController") as! AddMembersNewViewController
        addMember.groupId = self.groupId
        addMember.isAdmin = self.isAdmin
        addMember.link_type = self.link_type
        addMember.islinkable = self.islinkable
        addMember.faCate = self.facate
        addMember.famSettings = famSettings
        addMember.announcementSet = announcementSet
        addMember.postId                    = self.postId
        addMember.announcementId            = self.announcementId
        addMember.famArr = famArr
        addMember.memberJoiningStatus = self.memberJoining
        addMember.invitationStatus = self.invitationStatus
        addMember.isMembershipActive = self.isMembershipActive
        self.navigationController?.pushViewController(addMember, animated: false)
    }
    
    @IBAction func onClickOpenRequest(_ sender: UIButton) {
        let need = ArrayPosts[sender.tag]
        if let postType = need["publish_type"].string, postType.lowercased() == "request"{
            let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "RequestDetailsViewController") as! RequestDetailsViewController
            vc.requestId = need["publish_id"].stringValue
            //        vc.requestDic = need
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            let storyboard = UIStoryboard.init(name: "third", bundle: nil)
            /* let addMember = storyboard.instantiateViewController(withIdentifier: "AlbumListingViewController") as! AlbumListingViewController
             addMember.groupID = need["to_group_id"].stringValue
             addMember.isFromPost = true
             addMember.isFromdocument = false
             addMember.albumId = need["publish_id"].stringValue
             
             
             self.navigationController?.pushViewController(addMember, animated: false)*/
            let AlbumDetailsViewController = storyboard.instantiateViewController(withIdentifier: "AlbumDetailsViewController") as! AlbumDetailsViewController
            //            AlbumDetailsViewController.albumDetailsDict = items
            AlbumDetailsViewController.isFrom = "groups"
            AlbumDetailsViewController.isFromPost = true
            AlbumDetailsViewController.groupId = need["to_group_id"].stringValue
            if let postType = need["publish_type"].string, postType.lowercased() == "albums"{
                AlbumDetailsViewController.folder_type = "albums"
            }
            else{
                AlbumDetailsViewController.folder_type = "documents"
                AlbumDetailsViewController.isFromDocs = true
            }
            
            AlbumDetailsViewController.createdBy = need["created_by"].stringValue
            AlbumDetailsViewController.albumId = need["publish_id"].stringValue
            self.navigationController?.pushViewController(AlbumDetailsViewController, animated: true)
        }
        
    }
    @IBAction func onClickThanksReadMore(_ sender: UIButton) {
        let cell = tblListView.cellForRow(at: IndexPath.init(row: sender.tag, section: 0)) as! PostThanksTableViewCell
        
        if cell.labelPost.calculateMaxLines() > maxNumberOfLines &&  cell.labelPost.numberOfLines == maxNumberOfLines{
            
            tblListView.beginUpdates()
            cell.labelPost.numberOfLines = 0
            cell.readmoreButton.setTitle("view less", for: .normal)
            tblListView.endUpdates()
            
            
        }
        else if cell.labelPost.calculateMaxLines() > maxNumberOfLines &&  cell.labelPost.numberOfLines == 0 {
            UIView.performWithoutAnimation {
                tblListView.beginUpdates()
                cell.labelPost.numberOfLines = maxNumberOfLines
                cell.readmoreButton.setTitle("view more", for: .normal)
                tblListView.endUpdates()
            }
        }
    }
    
    @IBAction func onClickSettings(_ sender: Any) {
        if isAdmin.lowercased() == "not-member"{
            ActivityIndicatorView.show("Please wait!")
            APIServiceManager.callServer.joinFamilyRequest(url: EndPoint.joinFamily, userId:  UserDefaults.standard.value(forKey: "userId") as! String, groupId: groupId, success: { (response) in
                ActivityIndicatorView.hiding()
                guard let result = response as? JoinResult else{
                    return
                }
                print(result)
                if result.joinData?.status.lowercased() == "accepeted" || result.joinData?.status.lowercased() == "joined"{
                    self.lblJoin.isHidden = true
                    self.imgSettings.isHidden = false
                    self.isAdmin = "member"
                    
                    self.famArr![0].isAdmin = "member"
                    print(self.famArr![0])
                    self.viewDidLoad()
                    //                    self.getFamilyDetails(groupId: self.groupId)
                    
                }
                else{
                    self.lblJoin.text = "Pending"
                }
                
            }) { (error) in
                ActivityIndicatorView.hiding()
                
            }
        }
        else{
            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            let addMember = storyboard.instantiateViewController(withIdentifier: "FamilySettingsViewController") as! FamilySettingsViewController
            addMember.famArr = self.famArr
            addMember.callBackdelegate = self
            addMember.groupId = self.groupId
            addMember.delegate = self
            
            // addMember.isFromGroup = "1"
            self.navigationController?.pushViewController(addMember, animated: true)
        }
    }
    
    
    @IBAction func onClickMore(_ sender: Any) {
        arr = ["About us","Linked Families","Albums","Documents"]
        
        self.showActionSheet(titleArr: self.arr as! NSArray, title: "Choose option") { (index) in
            if index == 3{
                let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "DocumentListingViewController") as! DocumentListingViewController
                addMember.groupID = self.groupId
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.isFromdocument = true
                addMember.facate = self.facate
                self.navigationController?.pushViewController(addMember, animated: false)
                //            setActiveButton(index: 2)
            }
            else if index == 100{
            }
            else if index == 2{
                let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "AlbumListingViewController") as! AlbumListingViewController
                addMember.groupID = self.groupId
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.facate = self.facate
                self.navigationController?.pushViewController(addMember, animated: false)
            }
            else if index == 0{
                let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
                intro.groupId = self.groupId
                intro.isAboutUsClicked = true
                self.navigationController?.pushViewController(intro, animated: false)
            }
                
            else{
                let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "LinkedFamiliesListViewController") as! LinkedFamiliesListViewController
                addMember.groupID = self.groupId
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.facate = self.facate
                self.navigationController?.pushViewController(addMember, animated: false)
            }
        }
    }
    //MARK:- collectionView Delegates
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 1
        {
            
            return ArrayStickyPosts.count
            
        }
        else
        {
            return aboutArr.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 1
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "stickyPostCell", for: indexPath as IndexPath) as! stickyPostCell
            let stickyPost = ArrayStickyPosts[indexPath.row]
            print(stickyPost)
            
            cell.lblDescription.text = stickyPost["snap_description"].string ?? ""
            cell.lblHeading.text = "By \(stickyPost["created_user_name"].string ?? "")"
            
            if isAdmin.lowercased() == "admin"{
                cell.btnUnsticky.isHidden = false
            }
            else
            {
                cell.btnUnsticky.isHidden = true
                
            }
            cell.btnUnsticky.tag = indexPath.row
            //            heightOfImg: NSLayoutConstraint!
            //            @IBOutlet weak var widthOfImg
            //            class : UICollectionViewCell {
            //                @IBOutlet weak var viewOfBG: UIView!
            //                @IBOutlet weak var imgCover: UIImageView!
            //                @IBOutlet weak var lblHeading: UILabel!
            //                @IBOutlet weak var lblDescription: UILabel!
            //
            //
            //            }
            
            if let Attachments = stickyPost["post_attachment"].array, Attachments.count != 0{
                print("updating slide........")
                print(indexPath.row)
                cell.imgCover.isHidden = false
                cell.imgPlayIcon.isHidden = true
                cell.imgCover.layer.cornerRadius = 10
                cell.imgCover.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
                cell.heightOfImg.constant = 65
                cell.widthOfImg.constant = 110
                let attachment = Attachments[0]
                if let type = attachment["type"].string ,type.contains("image"){
                    
                    cell.imgPlayIcon.isHidden = true
                    
                    cell.imgCover.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.9411764706, blue: 0.9450980392, alpha: 1)
                    
                    
                    let  temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+"\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+attachment["filename"].stringValue
                    
                    var imgUrl = URL(string: temp)
                    
                    if type.lowercased().contains("gif"){
                        imgUrl = URL(string: temp )
                    }
                    //                    var imgurl = URL(string:BaseUrl.imaginaryURLForDetail2X + temp )
                    //                    if type.lowercased().contains("gif"){
                    //                        imgurl = URL(string: temp )
                    //                    }
                    DispatchQueue.main.async {
                        
                        cell.imgCover.kf.indicatorType = .activity
                        
                        cell.imgCover.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                        //                        cell.imgCover.kf.indicatorType = .image(imageData: self.appDelegate.gifData)
                        //                        cell.imgCover.kf.setImage(with: imgurl, placeholder:nil, options: [.processor(
                        //                            ResizingImageProcessor(referenceSize: CGSize(width: cell.imageViewPost.frame.width, height: cell.imageViewPost.frame.height), mode: .aspectFit)),
                        //                                                                                                .scaleFactor(UIScreen.main.scale),
                        //                                                                                                .transition(.fade(1)),
                        //                                                                                                .cacheOriginalImage
                        //                        ])
                    }
                    
                    
                    //            cell.imageViewPost.kf.setImage(with: imgurl, placeholder: #imageLiteral(resourceName: "placeholder.png") , options: nil, progressBlock: nil, completionHandler: nil)
                    
                }
                else if let type = attachment["type"].string ,type.contains("video"){
                    cell.imgPlayIcon.isHidden = false
                    cell.imgCover.backgroundColor = .black
                    
                    //                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostCollectionViewCell", for: indexPath) as! PostCollectionViewCell
                    //                    cell.playIcon.isHidden = false
                    //                    var temp = ""
                    //                    //"\(Helpers.imageURl)"+"default_video.jpg"
                    //
                    var temp = ""
                    
                    if let thumb = attachment["video_thumb"].string, !thumb.isEmpty {
                        temp = "\(Helpers.imageURl)"+thumb
                        let imgurl = URL(string:temp )
                        
                        cell.imgPlayIcon.isHidden = false
                        
                        
                        DispatchQueue.main.async {
                            //                            cell.imgCover.kf.indicatorType = .image(imageData: self.appDelegate.gifData)
                            //                            cell.imageViewPost.kf.setImage(with: imgurl, placeholder:nil, options: [.processor(
                            //                                ResizingImageProcessor(referenceSize: CGSize(width: cell.imageViewPost.frame.width, height: cell.imageViewPost.frame.height), mode: .aspectFit)),
                            //                                                                                                    .scaleFactor(UIScreen.main.scale),
                            //                                                                                                    .transition(.fade(1)),
                            //                                                                                                    .cacheOriginalImage
                            //                            ])
                            
                            
                            
                            cell.imgCover.kf.indicatorType = .activity
                            
                            cell.imgCover.kf.setImage(with: imgurl, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
                        }
                    }
                    //                let imgurl = URL(string: temp )
                    //                print(temp)
                    
                    
                    
                    /*
                     let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostCollectionViewCellVideo", for: indexPath) as! PostCollectionViewCellVideo
                     
                     let temp = "\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+attachment["filename"].stringValue
                     let url = URL(string:temp)
                     DispatchQueue.main.async {
                     
                     cell.player = AVPlayer(url: url!)
                     
                     cell.avpController.player = cell.player
                     
                     cell.avpController.view.frame.size.height = cell.videoLayer.frame.size.height
                     
                     cell.avpController.view.frame.size.width = cell.videoLayer.frame.size.width
                     
                     cell.videoLayer.addSubview(cell.avpController.view)
                     }
                     return cell */
                    
                }
                else{
                    
                    //                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostCollectionViewCellVideo", for: indexPath) as! PostCollectionViewCellVideo
                    //
                    //                cell.labaleDocName.text = attachment["original_name"].string ?? attachment["filename"].stringValue
                    //
                    cell.imgPlayIcon.isHidden = true
                    cell.imgCover.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.9411764706, blue: 0.9450980392, alpha: 1)
                    
                    if attachment["original_name"].stringValue.lowercased().contains("pdf"){
                        cell.imgCover.image = #imageLiteral(resourceName: "PdfImage")
                    }
                    else{
                        cell.imgCover.image = #imageLiteral(resourceName: "docsIcon")
                    }
                }
                //                cell.updateSliderThankYou(data: Attachment, Ptype: postType)
            }
            else{
                print("No Slide........")
                print(indexPath.row)
                
                cell.imgCover.isHidden = true
                cell.imgPlayIcon.isHidden = true
                
                cell.heightOfImg.constant = 0
                cell.widthOfImg.constant = 0
                
                
                
            }
            return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AboutFamilyCollectionViewCell", for: indexPath as IndexPath) as! AboutFamilyCollectionViewCell
            cell.lblName.text = aboutArr[indexPath.item]
            if indexPath.row == ActiveTab{
                cell.lblName.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                cell.lblName.font = .boldSystemFont(ofSize: 15)
                cell.imgUnderline.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                
            }
            else{
                cell.lblName.textColor = .black
                cell.lblName.font = .systemFont(ofSize: 14)
                cell.imgUnderline.backgroundColor = .white
            }
            return cell
        }
    }
    @IBAction func btnUnsticky_OnClick(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Familheey", message: "Do you want to unstick this post ?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .destructive, handler: { _ in
            self.applyForStickyPost(tag:sender.tag,type:"unstick")
            
        }))
        alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 1
        {
            
            let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
            
            let vc = storyboard.instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
            //                vc.isFromSticky = true
            vc.postId = self.ArrayStickyPosts[indexPath.row]["post_id"].stringValue
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else
        {
            if indexPath.row == 0{ // Feeds
                getPosts()
            }
            else if indexPath.row == 1{ // Announcement
                onClickAnnouncement(self)
            }
            else if indexPath.row == 2{ // Request
                if isAdmin.lowercased() == "not-member"{ // About us
                    let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                    let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
                    intro.groupId = self.groupId
                    intro.isAboutUsClicked = true
                    //            addMember.famSettings = famSettings
                    self.navigationController?.pushViewController(intro, animated: false)
                }
                else{
                    let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
                    let addMember = storyboard.instantiateViewController(withIdentifier: "RequestInFamilyViewController") as! RequestInFamilyViewController
                    addMember.groupID = self.groupId
                    addMember.isAdmin = self.isAdmin
                    addMember.link_type = self.link_type
                    addMember.islinkable = self.islinkable
                    addMember.facate = self.facate
                    addMember.famSettings = famSettings
                    addMember.announcementSet = announcementSet
                    addMember.postId                    = self.postId
                    addMember.announcementId            = self.announcementId
                    addMember.famArr = famArr
                    addMember.memberJoining = self.memberJoining
                    addMember.invitationStatus = self.invitationStatus
                    addMember.isMembershipActive = self.isMembershipActive
                    self.navigationController?.pushViewController(addMember, animated: false)
                }
            }
            else if indexPath.row == 3{ // Events
                onClickEvents(self)
            }
            else if indexPath.row == 5{ // About us
                let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
                intro.groupId = self.groupId
                intro.isAboutUsClicked = true
                //            addMember.famSettings = famSettings
                self.navigationController?.pushViewController(intro, animated: false)
            }
            else if indexPath.row == 8{ // Members
                if isMembershipActive{
                    if isAdmin.lowercased() == "admin"{
                        let storyboard = UIStoryboard.init(name: "MembershipMngmnt", bundle: nil)
                        let addMember = storyboard.instantiateViewController(withIdentifier: "MemebrshipDashboardViewController") as! MemebrshipDashboardViewController
                        addMember.groupID = self.groupId
                        addMember.isAdmin = self.isAdmin
                        addMember.link_type = self.link_type
                        addMember.islinkable = self.islinkable
                        //                        addMember.isFromdocument = true
                        addMember.facate = self.facate
                        addMember.famSettings = famSettings
                        addMember.announcementSet = announcementSet
                        addMember.postId                    = self.postId
                        addMember.announcementId            = self.announcementId
                        addMember.isMembershipActive = self.isMembershipActive
                        addMember.famArr = famArr
                        addMember.memberJoining = self.memberJoining
                        addMember.invitationStatus = self.invitationStatus
                        addMember.isMembershipActive = self.isMembershipActive
                        self.navigationController?.pushViewController(addMember, animated: true)
                    }
                    else{
                        onClickAboutus(self)
                    }
                }
                else{
                    onClickAboutus(self)
                }
            }
            else if indexPath.row == 9{
                onClickAboutus(self)
            }
            else if indexPath.row == 4{ // Albums
                let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "AlbumListingViewController") as! AlbumListingViewController
                addMember.groupID = self.groupId
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.facate = self.facate
                addMember.famSettings = famSettings
                addMember.announcementSet = announcementSet
                addMember.postId                    = self.postId
                addMember.announcementId            = self.announcementId
                addMember.famArr = famArr
                addMember.memberJoining = self.memberJoining
                addMember.invitationStatus = self.invitationStatus
                addMember.isMembershipActive = self.isMembershipActive
                self.navigationController?.pushViewController(addMember, animated: false)
            }
            else if indexPath.row == 6{ // Documents
                let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "DocumentListingViewController") as! DocumentListingViewController
                addMember.groupID = self.groupId
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.isFromdocument = true
                addMember.facate = self.facate
                addMember.famSettings = famSettings
                addMember.announcementSet = announcementSet
                addMember.postId                    = self.postId
                addMember.announcementId            = self.announcementId
                addMember.famArr = famArr
                addMember.memberJoining = self.memberJoining
                addMember.invitationStatus = self.invitationStatus
                addMember.isMembershipActive = self.isMembershipActive
                self.navigationController?.pushViewController(addMember, animated: false)
            }
            else{ // Linked families
                let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "LinkedFamiliesListViewController") as! LinkedFamiliesListViewController
                addMember.groupID = self.groupId
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.facate = self.facate
                addMember.famSettings = famSettings
                addMember.announcementSet = announcementSet
                addMember.postId                    = self.postId
                addMember.announcementId            = self.announcementId
                addMember.famArr = famArr
                addMember.memberJoining = self.memberJoining
                addMember.invitationStatus = self.invitationStatus
                addMember.isMembershipActive = self.isMembershipActive
                self.navigationController?.pushViewController(addMember, animated: false)
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 1
        {
            let numberOfCellInRow  = 1
            let padding : Int      = 40
            let collectionCellWidth : CGFloat = (self.collVwOfStickyPost.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
            print(collectionCellWidth)
            return CGSize(width: collectionCellWidth, height: 75)
        }
        else
        {
            var w = aboutArr[indexPath.row].size(withAttributes: nil)
            w.width = w.width + 32
            return CGSize(width: w.width, height: 60)
        }
        
        
    }
    
    //MARK:-  Custom Delegate
    func updateFamilyDetailsFromSettings() {
        getFamilyDetails(groupId: groupId)
    }
    
    func updateFamilySetting(famArr : [familyModel]){
        getFamilyDetails(groupId: groupId)
    }
    
    func postupdateSuccess(index:Int , SuccessData:JSON){
    }
    
    //MARK:- Go To Create Post
    
    @IBAction func goToCreatePost(_ sender: Any) {
        
        if famSettings == 7{
            if self.isAdmin.lowercased() != "admin"{
                self.displayAlert(alertStr: "You don't have permission", title: "")
                return
            }
        }
        
        
        let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CreatePostViewController") as! CreatePostViewController
        
        let selectedUserIDArr                     = NSMutableArray()
        
        
        
        let selectedGroups = NSMutableDictionary()
        
        selectedGroups.setValue( "\(groupId)", forKey: "id")
        selectedGroups.setValue( "\(postId)", forKey: "post_create")
        
        selectedUserIDArr.add(selectedGroups)
        
        vc.selectedUserIDArr = selectedUserIDArr
        vc.fromGroup = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension PostedInGroupsViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == txtSearch
        {
            // selectWebAPI()
            if textField.text!.count > 0{
                btnSearchRest.isHidden = false
            }
            else{
                btnSearchRest.isHidden = true
            }
            textField.endEditing(true)
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        self.view.endEditing(true)
        if textField == txtSearch
        {
            if textField.text!.count > 0{
                btnSearchRest.isHidden = false
            }
            else{
                btnSearchRest.isHidden = true
            }
            self.offset = 0
            self.getPosts()
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text!.count > 0 {
            btnSearchRest.isHidden = false
        }
        else{
            btnSearchRest.isHidden = true
            // selectWebAPI()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text!.count > 0 {
            btnSearchRest.isHidden = false
        }
        else{
            btnSearchRest.isHidden = true
            // selectWebAPI()
        }
        return true
    }
}

extension PostedInGroupsViewController : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ArrayPosts.count+1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard ArrayPosts.count != 0 else {
            return UITableViewCell.init()
        }
        
        if ArrayPosts.count == indexPath.row{
            return UITableViewCell()
        }
        else
        {
            let post = ArrayPosts[indexPath.row]
            
            if indexPath.row == 0{
                //            self.viewCurrentPost(index: indexPath.row)
            }
            if let postType = post["publish_type"].string, postType.lowercased() == "request" || postType.lowercased() == "albums" || postType.lowercased() == "documents" {
                
                let cellid = "PostThanksTableViewCell"
                
                let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! PostThanksTableViewCell
                
                
                cell.readmoreButton.tag = indexPath.row
                cell.buttonShare.tag = indexPath.row
                cell.buttonRightMenu.tag = indexPath.row
                cell.commentsButton.tag = indexPath.row
                cell.viewsButton.tag = indexPath.row
                cell.sharedUsersButton.tag = indexPath.row
                cell.buttonComments.tag = indexPath.row
                cell.buttonComments.tag = indexPath.row
                cell.btnFamilyClick.tag = indexPath.row
                cell.btnOpenRequest.tag = indexPath.row
                
                //User Details
                cell.labelUserName.text = post["created_user_name"].string ?? "Unknown"
                cell.labelPostedIn.text = post["group_name"].string ?? "Public"
                if cell.labelPostedIn.text == "Public"
                {
                    cell.btnFamilyClick.isUserInteractionEnabled = false
                }
                else
                {
                    cell.btnFamilyClick.isUserInteractionEnabled = true
                    
                }
                //            cell.labelDate.text = post["createdAt"].string ?? ""
                cell.labelDate.text = HomeTabViewController.dateFormatterCommon(dateStr: post["createdAt"].stringValue)
                
                let proPic = post["propic"].string ?? ""
                
                DispatchQueue.main.async {
                    
                    if proPic.count != 0{
                        let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                        let imgUrl = URL(string: temp)
                        cell.imageViewProfilePic.kf.indicatorType = .activity
                        
                        cell.imageViewProfilePic.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                    }
                    else{
                        cell.imageViewProfilePic.image = #imageLiteral(resourceName: "Male Colored")
                    }
                }
                print(post["created_by"].intValue)
            //    cell.btnProfile.tag =  post["created_by"].intValue
                cell.imageViewProfilePic.isTappable(id: post["created_by"].intValue)
                //Post
                cell.labelPost.text = post["snap_description"].string ?? ""
                
                //Details
                cell.lableNumberOfConversations.text = post["conversation_count"].stringValue
                cell.lblNumberOfShares.text = post["shared_user_count"].stringValue
                cell.labelNumberOfViews.text = post["views_count"].stringValue
                
                if let Attachment = post["post_attachment"].array, Attachment.count != 0{
                    print("updating slide........")
                    print(indexPath.row)
                    cell.updateSliderThankYou(data: Attachment, Ptype: postType)
                }
                else{
                    print("No Slide........")
                    print(indexPath.row)
                    
                    cell.viewPostBg.isHidden = false
                    cell.viewPageControllBG.isHidden = false
                    if let urlArray = post["valid_urls"].array, urlArray.count != 0{
                        if urlArray.count == 1{
                            if let urlMetadata = post["url_metadata"].dictionary, urlMetadata.count != 0{
                                if let result = urlMetadata["urlMetadataResult"], result.count != 0{
                                    cell.viewPostBg.isHidden = false
                                    cell.viewPageControllBG.isHidden = true
                                    
                                    cell.updateSliderLink(data: [result])
                                }
                                else{
                                    print(indexPath.row)
                                    cell.viewPostBg.isHidden = true
                                    cell.viewPageControllBG.isHidden = true
                                }
                            }
                            
                            
                        }
                        else{
                            print(indexPath.row)
                            cell.viewPostBg.isHidden = true
                            cell.viewPageControllBG.isHidden = true
                        }
                    }
                    else{
                        print(indexPath.row)
                        cell.viewPostBg.isHidden = true
                        cell.viewPageControllBG.isHidden = true
                    }
                }
                cell.viewShared.isHidden = true
                if let type = post["publish_type"].string, type.lowercased() == "request"{
                    cell.lblItemNames.isHidden = false
                    if let mentionedItems = post["publish_mention_items"].array, mentionedItems.count != 0{
                        //                    for index in mentionedUsers{
                        //                        let name = index["user_name"].stringValue
                        //                        let temArr = name.components(separatedBy: " ")
                        //                        if temArr.count > 0{
                        //                            let formattedName = "@\(temArr[0])"
                        //                            stringArr.append(formattedName)
                        //                        }
                        //                    }
                        cell.lblItemNames.text = mentionedItems[0]["item_name"].stringValue
                    }
                    else{
                        cell.lblItemNames.text = ""
                    }
                    cell.btnOpenRequest.setTitle("Open Request", for: .normal)
                    cell.viewViews.isHidden = true
                }
                else{
                    cell.lblItemNames.text = ""
                    cell.lblItemNames.isHidden = true
                    cell.viewViews.isHidden = false
                    
                    cell.delegate = self
                    if let type = post["publish_type"].string, type.lowercased() == "albums"{
                        cell.btnOpenRequest.setTitle("Open Album", for: .normal)
                    }
                    else{
                        cell.btnOpenRequest.setTitle("Open Folder", for: .normal)
                    }
                }
                
                if cell.labelPost.calculateMaxLines() > maxNumberOfLines{
                    cell.labelPost.numberOfLines = maxNumberOfLines
                    cell.readMoreView.isHidden = false
                    
                    //            tableView.layoutIfNeeded()
                }
                else{
                    cell.labelPost.numberOfLines = 0
                    cell.readMoreView.isHidden = true
                    //            tableView.layoutIfNeeded()
                }
                //                cell.delegate = self
                
                
                // String(describing: UserDefaults.standard.value(forKey: "userId"))
                let user = UserDefaults.standard.value(forKey: "userId") as! String
                if  user  == post["created_by"].stringValue{
                    
                    //                cell.viewViews.isHidden = false
                    //                cell.viewShared.isHidden = false
                    if post["conversation_enabled"].bool ?? false{
                        cell.viewComments.isHidden = false
                    }
                    else{
                        cell.viewComments.isHidden = true
                    }
                }
                else{
                    
                    //                cell.viewViews.isHidden = true
                    //                cell.viewShared.isHidden = true
                    if post["conversation_enabled"].bool ?? false{
                        cell.viewComments.isHidden = false
                    }
                    else{
                        cell.viewComments.isHidden = true
                    }
                }
                
                if post["conversation_count_new"].intValue > 0{
                    cell.lblNewConvrstn.isHidden = false
                }
                else{
                    cell.lblNewConvrstn.isHidden = true
                }
                
                
                if post["is_shareable"].bool ?? false {
                    
                    cell.shareView.isHidden = false
                    
                }
                else{
                    
                    cell.shareView.isHidden = true
                    
                }
//                var stringArr = [String]()
//                if let mentionedUsers = post["publish_mention_users"].array, mentionedUsers.count != 0{
//                    for index in mentionedUsers{
//                        let name = index["user_name"].stringValue
//                        let temArr = name.components(separatedBy: " ")
//                        if temArr.count > 0{
//                            let formattedName = "@\(temArr[0])"
//                            stringArr.append(formattedName)
//                        }
//                    }
//                    cell.lblMentionedNames.text = stringArr.joined(separator: ", ")
//                }
//                else{
//                    cell.lblMentionedNames.text = ""
//                }
                
                cell.btnMentionedNames.tag = indexPath.row

                 if let mentionedUsers = post["publish_mention_users"].array, mentionedUsers.count != 0{

                                   if mentionedUsers.count == 1
                                   {
                cell.lblMentionedNames.text = "@ \(mentionedUsers[0]["user_name"].stringValue.components(separatedBy: " ")[0])"
                                                       cell.lblMentionedNames.isHidden = false
                                    cell.btnMentionedNames.isHidden = false
                                    
                                                       cell.collVwOfMentionedNames.isHidden = true

                                                       cell.heightOfMentionedView.constant = 30
                                  
                                    
                                    }
                                    else
                                   {
                                    cell.lblMentionedNames.isHidden = true
                                    cell.btnMentionedNames.isHidden = true

                                                                          cell.collVwOfMentionedNames.isHidden = false
                                    cell.loadMentionedNames(data:mentionedUsers)

                                    }
                                    

                                }
                                else{
                                    cell.lblMentionedNames.text = ""
                                    cell.lblMentionedNames.isHidden = true
                                    cell.collVwOfMentionedNames.isHidden = true
                                    cell.btnMentionedNames.isHidden = true

                                    cell.heightOfMentionedView.constant = 0

                                }
                cell.labelPost.handleHashtagTap { hashtag in
                    print("Success. You just tapped the \(hashtag) hashtag")
                    self.txtSearch.text  = "#"+hashtag
                    //                self.searchview.isHidden = false
                    //                self.searchConstraintheight.constant = 36
                    self.txtSearch.becomeFirstResponder()
                    
                }
                cell.labelPost.handleMentionTap { (mension) in
                    print("Success. You just tapped the \(mension) mension")
                    
                    
                }
                cell.labelPost.handleURLTap { (url) in
                    print(url)
                    if url.absoluteString.contains("familheey"){
                        self.openAppLink(url: url.absoluteString)
                    }
                    else{
                        if !(["http", "https"].contains(url.scheme?.lowercased())) {
                            var strUrl = url.absoluteString
                            strUrl = "http://"+strUrl
                            let Turl = URL(string: strUrl)
                            
                            let vc = SFSafariViewController(url: Turl!)
                            self.present(vc, animated: true, completion: nil)
                        }
                        else{
                            let vc = SFSafariViewController(url: url)
                            self.present(vc, animated: true, completion: nil)
                        }
                    }
                }
                
                return cell
                
            }
            else{
                if let sharedusers = post["shared_user_names"].string , !sharedusers.isEmpty{
                    
                    
                    let cellid = "SharedPostTableViewCell"
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! SharedPostTableViewCell
                    
                    let post = ArrayPosts[indexPath.row]
                    cell.buttonSharedUsers.tag = indexPath.row

                    /* cell.readmoreButton.tag = indexPath.row
                     cell.buttonShare.tag = indexPath.row
                     cell.buttonSharedUsers.tag = indexPath.row
                     //            cell.buttonRightMenu.tag = indexPath.row
                     cell.commentsButton.tag = indexPath.row
                     cell.buttonComments.tag = indexPath.row
                     //            cell.viewsButton.tag = indexPath.row
                     //            cell.sharedUsersButton.tag = indexPath.row*/
                    
                    cell.readmoreButton.tag = indexPath.row
                    cell.buttonShare.tag = indexPath.row
                    cell.btnRightMrnu.tag = indexPath.row
                    //            cell.commentsButton.tag = indexPath.row
                    cell.viewsButtons.tag = indexPath.row
                    cell.sharedUserButton.tag = indexPath.row
                    cell.buttonComments.tag = indexPath.row
                    
                    
                    //User Details
                    
                    let array = sharedusers.components(separatedBy: ",")
                    if array.count != 0{
                        if array.count == 1{
                            cell.labelUserName.text = "\(array[0]) shared a post"
                        }
                        else if array.count == 2{
                            cell.labelUserName.text = "\(array[0]) and 1 other shared a post"
                        }
                        else {
                            cell.labelUserName.text = "\(array[0]) and \(array.count-1) others shared a post"
                        }
                        //post["created_user_name"].string ?? ""
                        
                    }
                    cell.labelPostedIn.text = post["group_name"].string ?? ""
                    cell.labelPostedInInner.text = post["parent_post_grp_name"].string ?? ""
                    cell.labelUserNameInner.text = post["parent_post_created_user_name"].string ?? ""
                    
                    //            cell.labelDate.text = post["createdAt"].string ?? ""
                    cell.labelDate.text = HomeTabViewController.dateFormatterCommon(dateStr: post["createdAt"].stringValue)
                    
                    let proPic = post["parent_post_created_user_propic"].stringValue
                    
                    
                    if proPic.count != 0{
                        let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                        let imgUrl = URL(string: temp)
                        cell.imageviewProfilePicInner.kf.indicatorType = .activity
                        
                        cell.imageviewProfilePicInner.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                    }
                    else{
                        cell.imageviewProfilePicInner.image = #imageLiteral(resourceName: "Male Colored")
                    }
                    
                    //Post
                    cell.labelPost.text = post["snap_description"].string ?? ""
                    
                    //Details
                    cell.lableNumberOfConversations.text = post["conversation_count"].stringValue
                    cell.lblNumberOfShares.text = post["shared_user_count"].stringValue
                    cell.lblNumberOfViews.text = post["views_count"].stringValue
                    
                    if let Attachment = post["post_attachment"].array, Attachment.count != 0{
                        cell.updateSlider(data: Attachment)
                    }
                    else{
                        print("No Slide........")
                        print(indexPath.row)
                        
                        if let urlArray = post["valid_urls"].array, urlArray.count != 0{
                            if urlArray.count == 1{
                                if let urlMetadata = post["url_metadata"].dictionary, urlMetadata.count != 0{
                                    if let result = urlMetadata["urlMetadataResult"], result.count != 0{
                                        cell.viewPostBg.isHidden = false
                                        cell.viewPageControllBG.isHidden = true
                                        
                                        cell.updateSliderLink(data: [result])
                                    }
                                    else{
                                        print(indexPath.row)
                                        cell.viewPostBg.isHidden = true
                                        cell.viewPageControllBG.isHidden = true
                                    }
                                }
                            }
                            else{
                                print(indexPath.row)
                                cell.viewPostBg.isHidden = true
                                cell.viewPageControllBG.isHidden = true
                            }
                        }
                        else{
                            print(indexPath.row)
                            cell.viewPostBg.isHidden = true
                            cell.viewPageControllBG.isHidden = true
                        }
                    }
                    
                    
                    if cell.labelPost.calculateMaxLines() > maxNumberOfLines{
                        cell.labelPost.numberOfLines = maxNumberOfLines
                        cell.readMoreView.isHidden = false
                        
                        //            tableView.layoutIfNeeded()
                    }
                    else{
                        cell.labelPost.numberOfLines = 0
                        cell.readMoreView.isHidden = true
                        //            tableView.layoutIfNeeded()
                    }
                    cell.delegate = self
                    
                    
                    let user = UserDefaults.standard.value(forKey: "userId") as! String
                    if  user  == post["created_by"].stringValue{
                        cell.viewViews.isHidden = false
                        cell.viewShared.isHidden = false
                        if post["conversation_enabled"].bool ?? false{
                            cell.ViewComments.isHidden = false
                        }
                        else{
                            cell.ViewComments.isHidden = true
                        }
                    }
                    else{
                        cell.viewViews.isHidden = true
                        cell.viewShared.isHidden = true
                        if post["conversation_enabled"].bool ?? false{
                            cell.ViewComments.isHidden = false
                        }
                        else{
                            cell.ViewComments.isHidden = true
                        }
                    }
                    
                    if post["conversation_count_new"].intValue > 0{
                        cell.lblNewConverstn.isHidden = false
                    }
                    else{
                        cell.lblNewConverstn.isHidden = true
                    }
                    
                    if post["is_shareable"].bool ?? false {
                        
                        cell.shareView.isHidden = false
                        
                    }
                    else{
                        
                        cell.shareView.isHidden = true
                        
                    }
                    cell.labelPost.handleHashtagTap { hashtag in
                        print("Success. You just tapped the \(hashtag) hashtag")
                        self.txtSearch.text  = "#"+hashtag
                        //                self.searchview.isHidden = false
                        //                self.searchConstraintheight.constant = 36
                        self.txtSearch.becomeFirstResponder()
                        
                    }
                    cell.labelPost.handleMentionTap { (mension) in
                        print("Success. You just tapped the \(mension) mension")
                        self.txtSearch.text  = mension
                        //                self.searchview.isHidden = false
                        //                self.searchConstraintheight.constant = 36
                        self.txtSearch.becomeFirstResponder()
                        
                    }
                    cell.labelPost.handleURLTap { (url) in
                        print(url)
                        if url.absoluteString.contains("familheey"){
                            self.openAppLink(url: url.absoluteString)
                        }
                        else{
                            if !(["http", "https"].contains(url.scheme?.lowercased())) {
                                var strUrl = url.absoluteString
                                strUrl = "http://"+strUrl
                                let Turl = URL(string: strUrl)
                                
                                let vc = SFSafariViewController(url: Turl!)
                                self.present(vc, animated: true, completion: nil)
                            }
                            else{
                                let vc = SFSafariViewController(url: url)
                                self.present(vc, animated: true, completion: nil)
                            }
                        }
                    }
                    
                    //pagination
                    //                  if indexPath.row == self.ArrayPosts.count - 1 {
                    //
                    //                        offset = ArrayPosts.count + 1
                    //                        if selectedIndex == 0{
                    //                            getPosts()
                    //                        }
                    //                        else{
                    //                            getpublicfeed()
                    //                        }
                    //
                    //                    }
                    return cell
                }
                else{
                    let cellid = "PostImageTableViewCell"
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! PostImageTableViewCell
                    
                    
                    cell.readmoreButton.tag = indexPath.row
                    cell.buttonShare.tag = indexPath.row
                    cell.buttonRightMenu.tag = indexPath.row
                    cell.commentsButton.tag = indexPath.row
                    cell.viewsButton.tag = indexPath.row
                    cell.sharedUsersButton.tag = indexPath.row
                    cell.buttonComments.tag = indexPath.row
                    
                    //User Details
                    cell.labelUserName.text = post["created_user_name"].string ?? ""
                    cell.labelPostedIn.text = post["group_name"].string ?? ""
                    //            cell.labelDate.text = post["createdAt"].string ?? ""
                    cell.labelDate.text = HomeTabViewController.dateFormatterCommon(dateStr: post["createdAt"].stringValue)
                    
                    let proPic = post["propic"].string ?? ""
                    
                    
                    if proPic.count != 0{
                        let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                        let imgUrl = URL(string: temp)
                        cell.imageViewProfilePic.kf.indicatorType = .activity
                        
                        cell.imageViewProfilePic.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                    }
                    else{
                        cell.imageViewProfilePic.image = #imageLiteral(resourceName: "Male Colored")
                    }
                    print(post["created_by"].intValue)
                                   cell.btnProfile.tag =  post["created_by"].intValue
                    //Post
                    cell.labelPost.text = post["snap_description"].string ?? ""
                    
                    //Details
                    cell.lableNumberOfConversations.text = post["conversation_count"].stringValue
                    cell.lblNumberOfShares.text = post["shared_user_count"].stringValue
                    cell.labelNumberOfViews.text = post["views_count"].stringValue
                    
                    if let Attachment = post["post_attachment"].array, Attachment.count != 0{
                        cell.updateSlider(data: Attachment)
                    }
                    else{
                        print("No Slide........")
                        print(indexPath.row)
                        
                        if let urlArray = post["valid_urls"].array, urlArray.count != 0{
                            if urlArray.count == 1{
                                if let urlMetadata = post["url_metadata"].dictionary, urlMetadata.count != 0{
                                    if let result = urlMetadata["urlMetadataResult"], result.count != 0{
                                        cell.viewPostBg.isHidden = false
                                        cell.viewPageControllBG.isHidden = true
                                        
                                        cell.updateSliderLink(data: [result])
                                    }
                                    else{
                                        print(indexPath.row)
                                        cell.viewPostBg.isHidden = true
                                        cell.viewPageControllBG.isHidden = true
                                    }
                                }
                            }
                            else{
                                print(indexPath.row)
                                cell.viewPostBg.isHidden = true
                                cell.viewPageControllBG.isHidden = true
                            }
                        }
                        else{
                            print(indexPath.row)
                            cell.viewPostBg.isHidden = true
                            cell.viewPageControllBG.isHidden = true
                        }
                    }
                    
                    if cell.labelPost.calculateMaxLines() > maxNumberOfLines{
                        cell.labelPost.numberOfLines = maxNumberOfLines
                        cell.readMoreView.isHidden = false
                        
                        //            tableView.layoutIfNeeded()
                    }
                    else{
                        cell.labelPost.numberOfLines = 0
                        cell.readMoreView.isHidden = true
                        //            tableView.layoutIfNeeded()
                    }
                    cell.delegate = self
                    
                    
                    // String(describing: UserDefaults.standard.value(forKey: "userId"))
                    let user = UserDefaults.standard.value(forKey: "userId") as! String
                    if  user  == post["created_by"].stringValue{
                        
                        cell.viewViews.isHidden = false
                        cell.viewShared.isHidden = false
                        if post["conversation_enabled"].bool ?? false{
                            cell.viewComments.isHidden = false
                        }
                        else{
                            cell.viewComments.isHidden = true
                        }
                    }
                    else{
                        
                        cell.viewViews.isHidden = true
                        cell.viewShared.isHidden = true
                        if post["conversation_enabled"].bool ?? false{
                            cell.viewComments.isHidden = false
                        }
                        else{
                            cell.viewComments.isHidden = true
                        }
                    }
                    
                    if post["conversation_count_new"].intValue > 0{
                        cell.lblNewConvrstn.isHidden = false
                    }
                    else{
                        cell.lblNewConvrstn.isHidden = true
                    }
                    
                    
                    if post["is_shareable"].bool ?? false {
                        
                        cell.shareView.isHidden = false
                        
                    }
                    else{
                        
                        cell.shareView.isHidden = true
                        
                    }
                    
                    cell.labelPost.handleHashtagTap { hashtag in
                        print("Success. You just tapped the \(hashtag) hashtag")
                        self.txtSearch.text  = "#"+hashtag
                        //                self.searchview.isHidden = false
                        //                self.searchConstraintheight.constant = 36
                        self.txtSearch.becomeFirstResponder()
                        
                    }
                    cell.labelPost.handleMentionTap { (mension) in
                        print("Success. You just tapped the \(mension) mension")
                        self.txtSearch.text  = mension
                        //                self.searchview.isHidden = false
                        //                self.searchConstraintheight.constant = 36
                        self.txtSearch.becomeFirstResponder()
                        
                    }
                    cell.labelPost.handleURLTap { (url) in
                        print(url)
                        if url.absoluteString.contains("familheey"){
                            self.openAppLink(url: url.absoluteString)
                        }
                        else{
                            if !(["http", "https"].contains(url.scheme?.lowercased())) {
                                var strUrl = url.absoluteString
                                strUrl = "http://"+strUrl
                                let Turl = URL(string: strUrl)
                                
                                let vc = SFSafariViewController(url: Turl!)
                                self.present(vc, animated: true, completion: nil)
                            }
                            else{
                                let vc = SFSafariViewController(url: url)
                                self.present(vc, animated: true, completion: nil)
                            }
                        }
                    }
                    //pagination
                    //
                    //            if indexPath.row == self.ArrayPosts.count - 1 {
                    //
                    //                offset = ArrayPosts.count + 1
                    //                if selectedIndex == 0{
                    //                    getPosts()
                    //                }
                    //                else{
                    //                    getpublicfeed()
                    //                }
                    //
                    //
                    //            }
                    
                    
                    return cell
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        // pagination
        
        if tableView.isLast(for: indexPath, arrayCount: ArrayPosts.count) {
            
            if !isNewDataLoading && !self.stopPagination{
                isNewDataLoading = true
                if ArrayPosts.count != 0{
                    offset = ArrayPosts.count + 1
                }
                print("---------------------------------------------------\(offset)")
                
                getPosts()
            }
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if fromRefresh{
            fromRefresh = false
        }
        else{
            var currentCellOffset = scrollView.contentOffset
            currentCellOffset.x += (self.tblListView.bounds.width);
            
            let indepath = self.tblListView.indexPathsForVisibleRows?.first
            if indepath != nil{
                self.viewCurrentPost(index: (indepath?.row)!)
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func getnumberOfLines(label:UILabel) -> Int{
        var   lineCount:NSInteger = 0
        let  textSize:CGSize = CGSize.init(width: label.frame.size.width, height: CGFloat(MAXFLOAT))
        let   rHeight:Int = lroundf(Float(label.sizeThatFits(textSize).height))
        let   charSize:Int = lroundf(Float(label.font!.lineHeight))
        lineCount = rHeight/charSize
        NSLog("No of lines: %i",lineCount)
        return lineCount
    }
    
}
extension PostedInGroupsViewController : PostImageTableViewCellDelegate, UIDocumentInteractionControllerDelegate{
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    func share(url: URL) {
        print(url)
        documentInteractionController = UIDocumentInteractionController(url: url)
        //        documentInteractionController.uti = url.typeIdentifier ?? "public.data, public.content"
        //        documentInteractionController.name = url.localizedName ?? url.lastPathComponent
        documentInteractionController.delegate = self
        documentInteractionController.presentPreview(animated: true)
    }
    
    func gotoPreview(data: [JSON], index: Int){
        
        if let type = data[index]["type"].string ,( type.contains("text") || type.contains("pdf") || type.contains("doc") || type.contains("docx") || type.contains("xls") || type.contains("xlsx") || type.contains("msword") || type.contains("txt")) {
            var temp:String!
            temp = "\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+data[index]["filename"].stringValue
            
            let url = URL(string: temp)!
             ActivityIndicatorView.show("Loading....")
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else { return }
                let tmpURL = FileManager.default.temporaryDirectory
                    .appendingPathComponent(response?.suggestedFilename ?? "fileName.png")
                do {
                    try data.write(to: tmpURL)
                } catch {
                    print(error)
                    ActivityIndicatorView.hiding()
                }
                DispatchQueue.main.async {
                    /// STOP YOUR ACTIVITY INDICATOR HERE
                    ActivityIndicatorView.hiding()
                    print(tmpURL)
                    self.share(url: tmpURL)
                }
            }.resume()
            
            /*let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
             
             let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementDocPreviewViewController") as! AnnouncementDocPreviewViewController
             vc.UrlToLoad = temp
             self.present(vc, animated: true, completion: nil)*/
        }
        else if let type = data[index]["url"].string {
            if type.isEmpty{
                
            }
            else{
                print(type.contains("familheey"))
                if type.contains("familheey"){
                    self.openAppLink(url: type)
                }
                else{
                    //                    UIApplication.shared.open(url)
                    let url = URL(string: type)!
                    let vc = SFSafariViewController(url: url)
                    self.present(vc, animated: true, completion: nil)
                }
            }
            
        }
        else{
            let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsPreviewViewController") as! AnnouncementsPreviewViewController
            vc.attachments = data
            vc.isFromPost = true
            vc.selectedIndex = index
            self.navigationController?.addFadeAnimation()
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    //    {
    //
    //        let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
    //        let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsPreviewViewController") as! AnnouncementsPreviewViewController
    //        vc.attachments = data
    //        vc.isFromPost = true
    //        vc.selectedIndex = index
    //        self.navigationController?.addFadeAnimation()
    //        self.navigationController?.pushViewController(vc, animated: false)
    //    }
    
    func gotoSinglePreview(data: String) {
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: "ChangePicturesViewController") as! ChangePicturesViewController
        vc.isCoverOrProfilePic = "CoverPic"
        vc.ids           = UserDefaults.standard.value(forKey: "userId") as! String
        vc.isFrom = "post"
        vc.inFor = "cover"
        vc.isAdmin = ""
        
        vc.imageUrl = data
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}
extension PostedInGroupsViewController: PostThanksableViewCellDelegate{
    func gotoThanksPreview(data: [JSON], index: Int) {
        
        if let type = data[index]["type"].string ,( type.contains("text") || type.contains("pdf") || type.contains("doc") || type.contains("docx") || type.contains("xls") || type.contains("xlsx") || type.contains("msword") || type.contains("txt")) {
            var temp:String!
            temp = "\(Helpers.imageURl)"+"\(BaseUrl.document_image)"+data[index]["filename"].stringValue
            let url = URL(string: temp)!
             ActivityIndicatorView.show("Loading....")
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else { return }
                let tmpURL = FileManager.default.temporaryDirectory
                    .appendingPathComponent(response?.suggestedFilename ?? "fileName.png")
                do {
                    try data.write(to: tmpURL)
                } catch {
                    print(error)
                    ActivityIndicatorView.hiding()
                }
                DispatchQueue.main.async {
                    /// STOP YOUR ACTIVITY INDICATOR HERE
                    ActivityIndicatorView.hiding()
                    print(tmpURL)
                    self.share(url: tmpURL)
                }
            }.resume()
            /*let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
             
             let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementDocPreviewViewController") as! AnnouncementDocPreviewViewController
             vc.UrlToLoad = temp
             self.present(vc, animated: true, completion: nil)*/
        }
        else if let type = data[index]["url"].string {
            if type.isEmpty{
                
            }
            else{
                print(type.contains("familheey"))
                if type.contains("familheey"){
                    self.openAppLink(url: type)
                }
                else{
                    //                    UIApplication.shared.open(url)
                    let url = URL(string: type)!
                    let vc = SFSafariViewController(url: url)
                    self.present(vc, animated: true, completion: nil)
                }
            }
            
        }
        else{
            let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsPreviewViewController") as! AnnouncementsPreviewViewController
            vc.attachments = data
            vc.isFromPost = true
            vc.typeAlbum = true
            vc.selectedIndex = index
            self.navigationController?.addFadeAnimation()
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
    }
    
    func gotoThanksSinglePreview(data: String) {
        /*let storyboard = UIStoryboard.init(name: "second", bundle: nil)
         
         let vc = storyboard.instantiateViewController(withIdentifier: "ChangePicturesViewController") as! ChangePicturesViewController
         vc.isCoverOrProfilePic = "CoverPic"
         vc.ids           = UserDefaults.standard.value(forKey: "userId") as! String
         vc.isFrom = "post"
         vc.inFor = "cover"
         vc.isAdmin = ""
         vc.imageUrl = data
         self.imgClicked = true
         self.navigationController?.pushViewController(vc, animated: true)*/
        
    }
      func selectMentionedName(data: [JSON], index: Int) {
           
           let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                  let vc = storyboard.instantiateViewController(withIdentifier: "UserProfileTableViewController") as! UserProfileTableViewController
           let userId = data[index]["user_id"].stringValue
                  vc.userID = userId
                  self.navigationController?.pushViewController(vc, animated: true)
           
       }
       @IBAction func btnMentinedNameClicked(_ sender: UIButton)
           {
               let post = ArrayPosts[sender.tag]
           
               
               let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                           let vc = storyboard.instantiateViewController(withIdentifier: "UserProfileTableViewController") as! UserProfileTableViewController
                    let userId = post["publish_mention_users"][0]["user_id"].stringValue
                           vc.userID = userId
                           self.navigationController?.pushViewController(vc, animated: true)
           }
    
}

extension PostedInGroupsViewController : SharedPostTableViewCellDelegate{
    
    func gotoSharedPreview(data: [JSON], index: Int){
        
        if let type = data[index]["type"].string ,( type.contains("text") || type.contains("pdf") || type.contains("doc") || type.contains("docx") || type.contains("xls") || type.contains("xlsx") || type.contains("msword") || type.contains("txt")) {
            var temp:String!
            temp = "\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+data[index]["filename"].stringValue
            let url = URL(string: temp)!
             ActivityIndicatorView.show("Loading....")
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else { return }
                let tmpURL = FileManager.default.temporaryDirectory
                    .appendingPathComponent(response?.suggestedFilename ?? "fileName.png")
                do {
                    try data.write(to: tmpURL)
                } catch {
                    print(error)
                    ActivityIndicatorView.hiding()
                }
                DispatchQueue.main.async {
                    /// STOP YOUR ACTIVITY INDICATOR HERE
                    ActivityIndicatorView.hiding()
                    print(tmpURL)
                    self.share(url: tmpURL)
                }
            }.resume()
            
            /*let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
             
             let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementDocPreviewViewController") as! AnnouncementDocPreviewViewController
             vc.UrlToLoad = temp
             self.present(vc, animated: true, completion: nil)*/
        }
        else if let type = data[index]["url"].string {
            if type.isEmpty{
                
            }
            else{
                print(type.contains("familheey"))
                if type.contains("familheey"){
                    self.openAppLink(url: type)
                }
                else{
                    //                    UIApplication.shared.open(url)
                    let url = URL(string: type)!
                    let vc = SFSafariViewController(url: url)
                    self.present(vc, animated: true, completion: nil)
                }
            }
            
        }
        else{
            let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsPreviewViewController") as! AnnouncementsPreviewViewController
            vc.attachments = data
            vc.isFromPost = true
            vc.selectedIndex = index
            self.navigationController?.addFadeAnimation()
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    //    {
    //
    //        let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
    //        let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsPreviewViewController") as! AnnouncementsPreviewViewController
    //        vc.attachments = data
    //        vc.isFromPost = true
    //        vc.selectedIndex = index
    //        self.navigationController?.addFadeAnimation()
    //        self.navigationController?.pushViewController(vc, animated: false)
    //    }
    
    func gotoSharedSinglePreview(data: String){
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: "ChangePicturesViewController") as! ChangePicturesViewController
        vc.isCoverOrProfilePic = "CoverPic"
        vc.ids           = UserDefaults.standard.value(forKey: "userId") as! String
        vc.isFrom = "post"
        vc.inFor = "cover"
        vc.isAdmin = ""
        
        vc.imageUrl = data
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}
