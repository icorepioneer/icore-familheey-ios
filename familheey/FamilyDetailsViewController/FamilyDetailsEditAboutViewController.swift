//
//  FamilyDetailsEditAboutViewController.swift
//  familheey
//
//  Created by Giri on 20/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
protocol EditAboutGroupDelegate:class {
    func updateAbout(data:String,workedit:Bool)
}
class FamilyDetailsEditAboutViewController: UIViewController {
    @IBOutlet weak var textviewEditIntro: UITextView!
    var aboutString = ""
    var workString = ""

    weak var delegate:EditAboutGroupDelegate?
    var groupId = ""
    var isFrom = ""
    var workedit = ""
    
    @IBOutlet weak var lblNavHeading: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        addMember.isFrom = "user"
//              addMember.workedit = "work"

        if self.workedit == "work"
        {
            self.textviewEditIntro.text  = workString
            self.lblNavHeading.text = "Edit Work"
        }
        else
        {
            textviewEditIntro.text  = aboutString
            self.lblNavHeading.text = "Edit Introduction"


        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.isHidden = true
    }
    override func viewDidAppear(_ animated: Bool) {                textviewEditIntro.becomeFirstResponder()

    }
    override func viewWillDisappear(_ animated: Bool) {
//        self.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK:- Button Actions
    @IBAction func onClickDoneAction(_ sender: Any) {
        
        if isFrom.lowercased() == "user"{
            if self.workedit != "work"
            {
            if textviewEditIntro.text != "Enter here"{
                APIServiceManager.callServer.updateUserItro(url: EndPoint.profileEdit, userId: groupId, intro: textviewEditIntro.text!,workedit:false, success: { (responseModel) in
                
                    guard let userMdl = responseModel as? ExternalUserModel else{
                        return
                    }
                    
                    let about = userMdl.User?.about ?? self.textviewEditIntro.text
                    self.delegate?.updateAbout(data: about ?? "",workedit:false)
                    self.navigationController?.popViewController(animated: true)
                    
                    ActivityIndicatorView.hiding()
                    
                }) { (error) in
                    self.displayAlert(alertStr: "Something Wrong, please try again later", title: "Error")
                }
            }
        }
            else
            {
                if textviewEditIntro.text != "Enter here"{
                    APIServiceManager.callServer.updateUserItro(url: EndPoint.profileEdit, userId: groupId, intro: textviewEditIntro.text!,workedit:true, success: { (responseModel) in
                    
                        guard let userMdl = responseModel as? ExternalUserModel else{
                            return
                        }
                        
                        let work = userMdl.User?.work ?? self.textviewEditIntro.text
                        self.delegate?.updateAbout(data: work ?? "",workedit:true)
                        self.navigationController?.popViewController(animated: true)
                        
                        ActivityIndicatorView.hiding()
                        
                    }) { (error) in
                        self.displayAlert(alertStr: "Something Wrong, please try again later", title: "Error")
                    }
                }
            }
        }
        else{
            if textviewEditIntro.text != "Enter here"{
                APIServiceManager.callServer.updateFamilyIntro(url: EndPoint.updateFamily, faId: groupId, intro: textviewEditIntro.text, success: { (resposnseMdl) in
                    
                    guard let familyMdl = resposnseMdl as? familyListResponse else{
                        return
                    }
                    ActivityIndicatorView.hiding()
                    if familyMdl.statusCode == 200{
                        self.delegate?.updateAbout(data: self.textviewEditIntro.text,workedit:false)

                        
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                }) { (error) in
                    
                }
            }
        }
    }
    @IBAction func onClockBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Textview Delegates
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Enter here"
            textView.textColor = UIColor.lightGray
        }
    }
    
}
