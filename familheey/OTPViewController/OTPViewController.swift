//
//  OTPViewController.swift
//  familheey
//
//  Created by familheey on 03/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON
import CoreLocation
import MessageUI


class OTPViewController: BaseClassViewController,OTPControlDelegate,CLLocationManagerDelegate,MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var lblTimeRemaining: UILabel!
    @IBOutlet weak var otpUxView                 : OTPControl!
    @IBOutlet weak var btnSend                   : UIButton!
    @IBOutlet weak var uxView_Height             : NSLayoutConstraint!
    @IBOutlet weak var otpInstructionLbl         : UILabel!
    @IBOutlet weak var btnResend: UIButton!
    
    var phone:String                             = String()
    var otpString:String                         = String()
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    var getLocationInFirstTime = false
    
    let locationManager = CLLocationManager()
    var currentLocation : String!
    var countTimer:Timer!
    
    var counter = 59
    
    override func onScreenLoad() {
        otpUxView.delegate = self
        
        otpUxView.show()
        
        if self.view.frame.height < 600{
            uxView_Height.constant = 30
        }
        
        //        let first4 = phone.substring(to:phone.index(phone.startIndex, offsetBy: 7))
        
        //        otpInstructionLbl.text = "Enter the OTP you received to \(first4)-xxxx"
        otpInstructionLbl.text = "Enter the OTP you received on \(phone)"
        self.btnResend.setTitleColor(.gray, for: .normal)
        self.countTimer = Timer.scheduledTimer(timeInterval: 1 ,
                                               target: self,
                                               selector: #selector(self.changeTitle),
                                               userInfo: nil,
                                               repeats: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.countTimer.invalidate()
    }
    
    //MARK:- OTPView Delegates
    func OTPTyped(_ optString: String?){
        if otpString.count == 6{
            print(otpString)
            OTPEntered(otp: otpString)
            onClickSend(self)
        }
    }
    
    func OTPControlDelegateBeginEditing(inputField: UITextField){
    }
    
    func OTPEntered(otp: String){
        if otpUxView.isValidOtp(){
            print(otp)
            otpString = otp
            btnSend.isHidden = false
            //  self.btnSend.setImage(UIImage(named: "tickImg"), for: .normal)
        }
        else{
            print(otp)
            btnSend.isHidden = true
        }
    }
    
    //MARK:- Location and custom function
    
    @objc func changeTitle(){
        if counter != 0{
            self.lblTimeRemaining.text = "\(counter)"
            counter -= 1
            self.btnResend.isEnabled = false
        }
        else{
            self.btnResend.setTitleColor(#colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1), for: .normal)
            self.lblTimeRemaining.text = "\(counter)"
            countTimer.invalidate()
            self.btnResend.isEnabled = true
        }
    }
    
    func callCurrentLocationForUserHistoryUpdate(){
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access location")
                self.callCurrentLocation()
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access location")
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
                
            }
        }
        else{
            self.callCurrentLocation()
        }
        
    }
    func callCurrentLocation()
    {
        let currentLocale = NSLocale.current as NSLocale
        let countryCode = currentLocale.object(forKey: .countryCode) as! String//get the set country name, code of your iphone
        let countryName = currentLocale.displayName(forKey: NSLocale.Key.countryCode, value: countryCode)
        print("from current location")
        print(countryName)
        
        self.currentLocation = "\(countryName)"
        self.getUserUpdateDetails()
    }
    
    //MARK: - location delegate methods
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        
        guard let location: CLLocation = manager.location else { return }
        fetchCityAndCountry(from: location) { city, country, error in
            guard let city = city, let country = country, error == nil else { return }
            print(city + ", " + country)
            if !self.getLocationInFirstTime
            {
                self.currentLocation = "\(city) , \(country)"
                self.locationManager.stopUpdatingLocation()
                self.getLocationInFirstTime  = true
                self.getUserUpdateDetails()
            }
            
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }
    
    
    func fetchCityAndCountry(from location: CLLocation, completion: @escaping (_ city: String?, _ country:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            completion(placemarks?.first?.locality,
                       placemarks?.first?.country,
                       error)
            
        }
    }
    
    
    
    func getUserUpdateDetails() {
        
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        print(appVersion!)
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        print(deviceID)
        
        let parameter = ["user_id":UserDefaults.standard.value(forKey: "userId") as! String,"login_type":"iOS","login_device":UIDevice.current.name,"app_version":appVersion!,"deviceID":deviceID,"login_location":self.currentLocation!]
        print(parameter)
        
        networkProvider.request(.addUserHistory(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    if response.statusCode == 200{
                        do {
                            // make sure this JSON is in the format we expect
                            if let json = try JSONSerialization.jsonObject(with: response.data, options: []) as? [String: Any] {
                                // try to read out a string array
                                print(json)
                                
                                appDel.isUserHistoryAdded = true
                                //
                                
                            }
                        } catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                    else if response.statusCode == 401{
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getUserUpdateDetails()
                        }
                    }
                    else{
                        
                    }
                } catch let err {
                    
                    //                             Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let _):
                break
                
                //                         Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
        })
    }
    
    //MARK:- Button Actions
    
    @IBAction func onClickSend(_ sender: Any) {
        
        //        let loginView = self.storyboard?.instantiateViewController(withIdentifier: "profileEditOne") as! ProfileEditOneViewController
        //        loginView.name  = "A J"
        //        loginView.email = "aj@gm.com"
        //        loginView.type  = "type"
        //        self.navigationController?.pushViewController(loginView, animated: true)
        //
        //        return
        
        APIServiceManager.callServer.otpVerification(url: EndPoint.otpVerify, phone: phone, otp: otpString, success: { (responseMdl) in
            
            ActivityIndicatorView.hiding()
            
            guard let loginMdl = responseMdl as? userModel else{
                return
            }
            if loginMdl.status == 200{
                self.apiForRegisterTocken()
                
                self.countTimer.invalidate()
                
                if loginMdl.reLogin == 1 {
                    print(loginMdl)
                    let temp = String(loginMdl.id)
                    UserDefaults.standard.set(loginMdl.fullname, forKey: "user_fullname")
                    setUserDefaultValues.setUserDefaultValue(userDefaultValue: temp, userDefaultKey: "userId")
                    setUserDefaultValues.setUserDefaultValue(userDefaultValue: loginMdl.phone, userDefaultKey: "userPhone")
                    setUserDefaultValues.setUserDefaultValue(userDefaultValue: "true", userDefaultKey: "isLogin")
                    setUserDefaultValues.setUserDefaultValue(userDefaultValue: loginMdl.accessToken, userDefaultKey: "app_token")
                    setUserDefaultValues.setUserDefaultValue(userDefaultValue: loginMdl.refreshToken, userDefaultKey: "refresh_token")
                    setUserDefaultValues.setUserDefaultValue(userDefaultValue: "1", userDefaultKey: "isLogedOut")
                    setUserDefaultValues.setUserDefaultValue(userDefaultValue: "0", userDefaultKey: "isFirst_Time")
                    
                    //this code for send user history to backend
                    self.getProfile()
                    self.getAllKeys()
                    
                    if !appDel.isUserHistoryAdded{
                        self.callCurrentLocationForUserHistoryUpdate()
                    }
                    
                    //                    let tabBar = self.storyboard?.instantiateViewController(withIdentifier: "mainTabView") as! HomeViewTabController
                    //                                      //  loginView.regType = regType
                    //                                      self.navigationController?.pushViewController(tabBar, animated: true)
                    
                    //above code token registration
                    
                    //                    let tabBar = self.storyboard?.instantiateViewController(withIdentifier: "mainTabView") as! HomeViewTabController
                    //                                                                                 //  loginView.regType = regType
                    //                    self.navigationController?.pushViewController(tabBar, animated: true)
                    //
                    //                    appDel.setupSocketForMessaging()
                    
                    
                    
                    
                }
                else{
                    print(responseMdl!)
                    let mainStoryBoard = UIStoryboard(name: "OnboardUser", bundle: nil)
                    //                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "ProfileEditNewViewController") as! ProfileEditNewViewController
                    let loginView = mainStoryBoard.instantiateViewController(withIdentifier: "ProfileEditNewViewController") as! ProfileEditNewViewController
                    
                    setUserDefaultValues.setUserDefaultValue(userDefaultValue: "true", userDefaultKey: "isLogin")
                    setUserDefaultValues.setUserDefaultValue(userDefaultValue: "1", userDefaultKey: "isLogedOut")
                    let temp = String(loginMdl.id)
                    setUserDefaultValues.setUserDefaultValue(userDefaultValue: temp, userDefaultKey: "userId")
                    setUserDefaultValues.setUserDefaultValue(userDefaultValue: loginMdl.accessToken, userDefaultKey: "app_token")
                    setUserDefaultValues.setUserDefaultValue(userDefaultValue: loginMdl.refreshToken, userDefaultKey: "refresh_token")
                    setUserDefaultValues.setUserDefaultValue(userDefaultValue: "1", userDefaultKey: "isFirst_Time")
                    setUserDefaultValues.setUserDefaultValue(userDefaultValue: loginMdl.phone, userDefaultKey: "userPhone")
                    
                    self.getAllKeys()
                    
                    
                    //this code for send user history to backend
                    
                    if !appDel.isUserHistoryAdded
                    {
                        self.callCurrentLocationForUserHistoryUpdate()
                    }
                    
                    
                    
                    //  loginView.regType = regType
                    appDel.setupSocketForMessaging()
                    self.navigationController?.pushViewController(loginView, animated: true)
                }
            }else{
                self.btnSend.setImage(UIImage(named: "red-cross"), for: .normal)
                
                
                let alert = UIAlertController(title: NSLocalizedString("", comment: ""), message: "Invalid OTP.\n Please try again", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: { _ -> Void in
                    self.btnSend.setImage(nil, for: .normal)
                    self.otpUxView.clearPin()
                    self.otpString = ""
                    
                    
                }))
                
                self.present(alert, animated: true, completion: nil)
                //                Helpers.showAlertDialog(message: "Invalid otp code.\n Please try again", target: self)
            }
        }) { (error) in
            self.otpUxView.clearPin()
            self.otpString = ""
            Helpers.showAlertDialog(message: "Invalid OTP.\n Please try again", target: self)
        }
    }
    private func createEmailUrl(to: String, subject: String, body: String) -> URL? {
        let subjectEncoded = subject.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        let bodyEncoded = body.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        
        let gmailUrl = URL(string: "googlegmail://co?to=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let outlookUrl = URL(string: "ms-outlook://compose?to=\(to)&subject=\(subjectEncoded)")
        let yahooMail = URL(string: "ymail://mail/compose?to=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let sparkUrl = URL(string: "readdle-spark://compose?recipient=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let defaultUrl = URL(string: "mailto:\(to)?subject=\(subjectEncoded)&body=\(bodyEncoded)")
        
        if let gmailUrl = gmailUrl, UIApplication.shared.canOpenURL(gmailUrl) {
            return gmailUrl
        } else if let outlookUrl = outlookUrl, UIApplication.shared.canOpenURL(outlookUrl) {
            return outlookUrl
        } else if let yahooMail = yahooMail, UIApplication.shared.canOpenURL(yahooMail) {
            return yahooMail
        } else if let sparkUrl = sparkUrl, UIApplication.shared.canOpenURL(sparkUrl) {
            return sparkUrl
        }
        
        return defaultUrl
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    func getAllKeys(){
        let param = [
            "user_id" : UserDefaults.standard.value(forKey: "userId") as! String
            ] as [String : Any]
        
        networkProvider.request(.GetKeys(parameter: param), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                do{
                    if response.statusCode == 200{
                        let jsonData =  JSON(response.data)
                        print(jsonData["s3_base_url"].stringValue)
                        if let s3url  = jsonData["s3_base_url"].string, !s3url.isEmpty{
                            appDel.s3BaseUrl = s3url
                        }
                        else{
                            
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getAllKeys()
                        }
                    }
                    else{
                        
                    }
                }catch _ {
                    
                }
            case .failure(let _):
                break
                
            }
        })
        
    }
    
    func getProfile(){
        DispatchQueue.main.async {
            ActivityIndicatorView.show("Loading....")
        }
        
        
        if let id =  UserDefaults.standard.value(forKey: "userId") as? String{
            
            let params = ["user_id":id] as [String : Any]
            self.networkProvider.request(.onBoardCheck(parameter: params)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                        print("view contents : \(jsonData)")
                        appDel.notification_auto_delete = jsonData["notification_auto_delete"].boolValue

                        if response.statusCode == 200{
                            
                            /* if jsonData["ios_force"].boolValue{
                             let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
                             if jsonData["ios_version"].stringValue == appVersion{
                             
                             }
                             else{
                             let domain = Bundle.main.bundleIdentifier!
                             UserDefaults.standard.removePersistentDomain(forName: domain)
                             UserDefaults.standard.synchronize()
                             
                             let alert = UIAlertController(title: "New version available", message: "Please update the application", preferredStyle: .alert)
                             
                             alert.addAction(UIAlertAction(title: "Update", style: .destructive, handler: { (acion) in
                             //                                    UIApplication.shared.openURL(NSURL(string: "https://apps.apple.com/us/app/familheey/id1485617876?ls=1")! as URL)
                             UIApplication.shared.open(URL(string: "https://apps.apple.com/us/app/familheey/id1485617876?ls=1")!, options: [:], completionHandler: nil)
                             
                             }))
                             
                             //                                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                             //                                    exit(0)
                             //                                }))
                             self.present(alert, animated: false, completion: nil)
                             }
                             
                             }*/
                            //                            else{
                            //                                let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
                            //                                let temp = Int(appVersion!)
                            //                                if jsonData["ios_version"].int! > temp!{
                            
                            
                            //                                    let alert = UIAlertController(title: "New version available", message: "Please update the application", preferredStyle: .alert)
                            //
                            //                                    alert.addAction(UIAlertAction(title: "Update", style: .destructive, handler: { (acion) in
                            //                                        UIApplication.shared.openURL(NSURL(string: "https://apps.apple.com/us/app/familheey/id1485617876?ls=1") as! URL)
                            //
                            //                                    }))
                            //
                            //                                    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                            //
                            //                                    }))
                            //
                            //                                    self.window?.rootViewController?.present(alert, animated: false, completion: nil)
                            //                                }
                            //                                else{
                            //                                    let tempArr = jsonData["subscriptions"].arrayValue
                            //                                    if tempArr.count > 0{
                            //                                        self.subscriptionArr = [tempArr[0].dictionaryValue]
                            //                                    }
                            //                                    print(self.subscriptionArr)
                            ActivityIndicatorView.hiding()
                            if jsonData["user_is_blocked"].boolValue
                            {
                                let alert = UIAlertController(title: "", message: "Sorry, you account have been suspended. Please email us on contact@familheey.com to reactivate your account", preferredStyle: .alert)
                                
                                alert.addAction(UIAlertAction(title: "Compose Mail", style: .destructive, handler: { (acion) in
                                    
                                    
                                    
                                    // Modify following variables with your text / recipient
                                    let recipientEmail = "contact@familheey.com"
                                    let subject = "Reactivate Account - \(id) "
                                    let body = "Please reactivate Account , User id - \(id)"
                                    
                                    // Show default mail composer
                                    if MFMailComposeViewController.canSendMail() {
                                        let mail = MFMailComposeViewController()
                                        mail.mailComposeDelegate = self
                                        mail.setToRecipients([recipientEmail])
                                        mail.setSubject(subject)
                                        mail.setMessageBody(body, isHTML: false)
                                        
                                        self.present(mail, animated: true)
                                        
                                        // Show third party email composer if default Mail app is not present
                                    } else if let emailUrl = self.createEmailUrl(to: recipientEmail, subject: subject, body: body) {
                                        UIApplication.shared.open(emailUrl)
                                    }
                                    
                                    
                                }))
                                
                                alert.addAction(UIAlertAction(title: "Login", style: .destructive, handler: { (acion) in
                                    
                                    
                                    //                                                              let domain = Bundle.main.bundleIdentifier!
                                    //                                                              UserDefaults.standard.removePersistentDomain(forName: domain)
                                    //                                                              UserDefaults.standard.synchronize()
                                    
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let loginView = mainStoryBoard.instantiateViewController(withIdentifier: "FirstViewController") as! FirstViewController
                                    
                                    let navView = UINavigationController(rootViewController: loginView)
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = navView
                                    
                                    
                                    
                                    
                                    
                                }))
                                self.present(alert, animated: false, completion: nil)
                                return
                            }
                            if jsonData["full_name"].stringValue.isEmpty || jsonData["email"].stringValue.isEmpty || jsonData["location"].stringValue.isEmpty || jsonData["origin"].stringValue.isEmpty {
                                let mainStoryBoard = UIStoryboard(name: "OnboardUser", bundle: nil)
                                let loginView = mainStoryBoard.instantiateViewController(withIdentifier: "ProfileEditNewViewController") as! ProfileEditNewViewController
                                appDel.setupSocketForMessaging()
                                let navView = UINavigationController(rootViewController: loginView)
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = navView
                            }
                            else{
                                if jsonData["family_count"].intValue == 0{
                                    appDel.noFamily = true
                                }
                                else{
                                    appDel.noFamily = false
                                }
                                let isGuideShow = UserDefaults.standard.value(forKey: "showGuide") as? String ?? ""
                                if isGuideShow == "1"{
                                    if jsonData["family_count"].intValue == 0{
                                        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                        setUserDefaultValues.setUserDefaultValue(userDefaultValue: "0", userDefaultKey: "isFirst_Time")
                                        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "HomeNavigation") as! HomeNavigation
                                        appDel.noFamily = true
                                        appDel.postcreatedInPublic = false
                                        
                                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                        appDelegate.window?.rootViewController = redViewController
                                    }
                                    else{
                                        setUserDefaultValues.setUserDefaultValue(userDefaultValue: "0", userDefaultKey: "isFirst_Time")
                                        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "HomeNavigation") as! HomeNavigation
                                        appDel.noFamily = false
                                        appDel.postcreatedInPublic = false
                                        
                                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                        appDelegate.window?.rootViewController = redViewController
                                    }
                                }
                                else{
                                    setUserDefaultValues.setUserDefaultValue(userDefaultValue: "0", userDefaultKey: "isFirst_Time")
                                    let mainStoryBoard = UIStoryboard(name: "Subscription", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "WalkthorughViewController") as! WalkthorughViewController
                                    appDel.noFamily = false
                                    appDel.postcreatedInPublic = false
                                    
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                            
                            //                                }
                            //                            }
                        }
                        else if response.statusCode == 401 {
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                self.getProfile()
                            }
                        }
                            
                        else {
                            let alert = UIAlertController(title: "", message: "Opps! Something went wrong", preferredStyle: .alert)
                            
                            alert.addAction(UIAlertAction(title: "Retry", style: .destructive, handler: { (acion) in
                                
                                self.getProfile()
                                
                            }))
                            
                            //                                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                            //                                    exit(0)
                            //                                }))
                            
                            self.present(alert, animated: false, completion: nil)
                        }
                        
                    }catch let err {
                        ActivityIndicatorView.hiding()
                        // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                    }
                case .failure( let error):
                    ActivityIndicatorView.hiding()
                    //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                    break
                }
            }
        }
        
    }
    
    
    func apiForRegisterTocken() {
        
        //        "ticket_type":dataFromPrevious["ticket_type"]!,
        
        var parameter = [String:Any]()
        parameter = ["user_id":UserDefaults.standard.value(forKey: "userId") as! String,"device_token":appDel.userToken,"device_type":"ios","device_id":UIDevice.current.identifierForVendor!.uuidString] as [String : Any]
        
        print(parameter)
        
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.registerToken(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                do {
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    print(jsonData["status"])
                    print(response.statusCode)
                    if response.statusCode == 200{
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.apiForRegisterTocken()
                        }
                    }

                    else{
                        print(jsonData["code"])
                        Helpers.showAlertDialog(message: "Error Token Registration", target: self)
                    }
                } catch let err {
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    @IBAction func onClickResend(_ sender: Any) {
        
        self.otpUxView.clearPin()
        
        APIServiceManager.callServer.resendOTP(url: EndPoint.resend, phone: phone, success: { (responseMdl) in
            ActivityIndicatorView.hiding()
            guard let loginMdl = responseMdl as? userModel else{
                return
            }
            if loginMdl.status == 200{
                self.counter = 59
                self.countTimer = Timer.scheduledTimer(timeInterval: 1 ,
                                                       target: self,
                                                       selector: #selector(self.changeTitle),
                                                       userInfo: nil,
                                                       repeats: true)
                self.displayAlert(alertStr: "OTP has been resent successfully", title: "")
                
            }
        }) { (error) in
            
        }
    }
    
    @IBAction func backClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
