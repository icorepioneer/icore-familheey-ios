//
//  MembershipDashboardCollectionViewCell.swift
//  familheey
//
//  Created by familheey on 20/05/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit

class MembershipDashboardCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblMemberNo: UILabel!
    @IBOutlet weak var lblMembrshipType: UILabel!
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        lblMemberNo.text = ""
        lblMembrshipType.text = ""
    }
}
