//
//  MembershipFullUserListViewController.swift
//  familheey
//
//  Created by familheey on 22/05/20.
//  Copyright © 2020 familheey. All rights reserved.



import UIKit
import Alamofire
import Kingfisher
import SwiftyJSON
import Moya

class MembershipFullUserListViewController: UIViewController {
    @IBOutlet weak var tblListView: UITableView!
    
    var groupId = ""
    var typeTitle = ""
    var memberArr = [viewMemberDetailsResult]()
    
    private var networkProvider  = MoyaProvider<FamilyheeyApi>()
    var dataArr = [JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getMemberListAPI()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Web API
    func getMemberListAPI(){
            let parameter = ["group_id":self.groupId, "crnt_user_id":UserDefaults.standard.value(forKey: "userId") as! String] as [String : Any]
            ActivityIndicatorView.show("Loading.....")
            networkProvider.request(.membership_user_list(parameter: parameter)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                        
                        if response.statusCode == 200{
                            print("jsonData : \(jsonData)")
                            self.dataArr = jsonData["data"].arrayValue

                            if self.dataArr.count > 0{
                                self.tblListView.delegate = self
                                self.tblListView.dataSource = self
                                self.tblListView.reloadData()
                      
                            }
                            else{ // Add no member view
                            
    //                            self.tblMemberList.delegate = self
    //                            self.tblMemberList.dataSource = self
    //                            self.tblMemberList.reloadData()
                            }
                            
                        }
                        else if response.statusCode == 401{
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                self.getMemberListAPI()
                            }
                        }
                        else{
                            
                        }
                        
                        ActivityIndicatorView.hiding()
                        
                    }catch let err {
                        ActivityIndicatorView.hiding()
                        Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                    }
                case .failure( let error):
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                    break
                }
            }
        }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickStartTopic(_ sender: UIButton) {
        var userArr = [String]()
         let user = dataArr[sender.tag]
        let uId = user["user_id"].stringValue
        
        userArr.append(uId)
        let storyboard = UIStoryboard.init(name: "Topic", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CreateTopicViewController") as! CreateTopicViewController
        vc.isFromCreate = false
        vc.isFromMembership  = true
        vc.toUsers = userArr
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}

extension MembershipFullUserListViewController: UITableViewDelegate, UITableViewDataSource{
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return dataArr.count
     }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MembershipUserListTableViewCell", for: indexPath) as! MembershipUserListTableViewCell
        cell.selectionStyle = .none
        
        let user = dataArr[indexPath.row]
        cell.btnEdit.tag = indexPath.row
        cell.btnReminder.tag = indexPath.row
        cell.btnStartTopic.tag = indexPath.row
        
        cell.btnReminder.isHidden = true
        cell.lblValidity.isHidden = true
        cell.btnEdit.isUserInteractionEnabled = false
        
        cell.lblName.text = user["full_name"].stringValue
        cell.lblType.text = user["membership_type"].stringValue
        
        //        let temStr = "\(EventsTabViewController.convertTimeStampToDate(timestamp: user["membership_paid_on"].stringValue))"
        //        let tempArr = temStr.components(separatedBy: " ")
        //        cell.lblValidity.text = "\(tempArr[0]) \(tempArr[1]) \(tempArr[2])"
        
        
//        if user["is_expaired"].boolValue{
//           cell.btnReminder.isHidden = false
//            cell.lblValidity.text = "Validity expired"
//            cell.lblValidity.textColor = .red
//        }
//        else{
        cell.btnEdit.setTitleColor(.white, for: .normal)
        cell.btnEdit.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)

            if user["membership_payment_status"].stringValue.lowercased() == "completed" || user["membership_payment_status"].stringValue.lowercased() == "success"{
                cell.imgPending.backgroundColor =  #colorLiteral(red: 0.3843137255, green: 0.9803921569, blue: 0.1803921569, alpha: 1)
                cell.lblDueAmount.isHidden = true
            }
            else{
                cell.lblDueAmount.isHidden = false
                cell.imgPending.backgroundColor =  #colorLiteral(red: 0.9725490196, green: 0.9294117647, blue: 0.06666666667, alpha: 1)
            }
            
            let totalFee = user["membership_fees"].intValue
            let paidAmount = user["membership_total_payed_amount"].intValue
            
            let due = totalFee - paidAmount
            cell.lblDueAmount.text = "Due Amount : \(due)"
            
//            if let durtn = user["membership_period_type_id"].int, durtn == 4{
//                cell.lblValidity.text = "Valid till: Life time"
//            }
//            else{
//                cell.lblValidity.text = "Valid till: \(Helpers.convertTimeStampWithoutTime(dateAsString: user["membership_to"].stringValue))"
//            }
//        }
        

        
        
        if user["propic"].stringValue.count != 0{
            let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+user["propic"].stringValue.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let imgUrl = URL(string: temp)
            //                cell.imageViewAvatar.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
            let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+imgUrl!.relativeString
            let url = URL(string: newUrlStr)
            DispatchQueue.main.async {
                cell.imgProfile.kf.indicatorType = .activity
                cell.imgProfile.kf.setImage(with: url, placeholder:#imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
            }
        }
        else{
            DispatchQueue.main.async {
                cell.imgProfile.image = #imageLiteral(resourceName: "Male Colored")
            }
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user = dataArr[indexPath.row]
        let story = UIStoryboard.init(name: "MembershipMngmnt", bundle: nil)
        let vc =  story.instantiateViewController(withIdentifier: "UserMembershipEditViewController") as! UserMembershipEditViewController
        vc.groupID = self.groupId
        vc.userID = user["user_id"].stringValue
        vc.userName = user["full_name"].stringValue
        vc.typeTitle = typeTitle
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func convertDateToDisplayDate(dateFromResponse:String)->String{
        if dateFromResponse != ""
        {
            let dateAsString               = dateFromResponse
            let dateFormatter              = DateFormatter()
            dateFormatter.dateFormat       = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            
            if let datee =  dateFormatter.date(from: dateAsString){
                dateFormatter.dateFormat       = "MMM dd yyyy"
                let DateFormatted = dateFormatter.string(from: datee)
                return DateFormatted
            }
            return ""
        }else
        {
            return ""
        }
    }

}
