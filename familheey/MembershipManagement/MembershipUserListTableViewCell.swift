//
//  MembershipUserListTableViewCell.swift
//  familheey
//
//  Created by familheey on 21/05/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit

class MembershipUserListTableViewCell: UITableViewCell {
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnStartTopic: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblValidity: UILabel!
    @IBOutlet weak var lblDueAmount: UILabel!
    
    @IBOutlet weak var imgPending: UIImageView!
    @IBOutlet weak var btnReminder: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var rightView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
