//
//  MemebrshipDashboardViewController.swift
//  familheey
//
//  Created by familheey on 19/05/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher
import SwiftyJSON
import Moya

class MemebrshipDashboardViewController: UIViewController {
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var lblFamName: UILabel!
    @IBOutlet weak var aboutClctnVew: UICollectionView!
    @IBOutlet weak var lblAllMembr: UILabel!
    @IBOutlet weak var lblActiveMembr: UILabel!
    @IBOutlet weak var lblInactiveMembr: UILabel!
    @IBOutlet weak var btnAllMembers: UIButton!
    @IBOutlet weak var btnActiveMembers: UIButton!
    @IBOutlet weak var btnInactiveMembers: UIButton!
    
    @IBOutlet weak var viewInactiveMember: UIView!
    @IBOutlet weak var viewActiveMember: UIView!
    @IBOutlet weak var viewAllMembers: UIView!
    
    @IBOutlet weak var btnFloating: UIButton!
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var btnNoDataAdd: UIButton!
    
    @IBOutlet weak var cltnTypeList: UICollectionView!
    @IBOutlet weak var lblCurrency: UILabel!
    @IBOutlet weak var lblDueAmnt: UILabel!
    
    
    private var networkProvider  = MoyaProvider<FamilyheeyApi>()
    var dataArr = [JSON]()
    
    var groupID = ""
    var link_type = 0
    var isAdmin = ""
    var facate = ""
    var islinkable = 0
    var famSettings = 0
    var announcementSet = 0
    var postId                          = 0
    var announcementId                  = 0
    var selectedIndex = 0
    var ActiveTab = 6
    var filter = ""
    var isMembershipActive = false
    var famArr : [familyModel]?
    var memberJoining = 0
    var invitationStatus = 0
    var defaultID = 0

    var aboutArr = ["FEED","ANNOUNCEMENT","REQUEST","EVENTS","ALBUMS","ABOUT US","DOCUMENTS","LINKED FAMILIES","MEMBERS"]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isAdmin.lowercased() == "admin"{
            ActiveTab = 8
            aboutArr = ["FEED","ANNOUNCEMENT","REQUEST","EVENTS","ALBUMS","ABOUT US","DOCUMENTS","LINKED FAMILIES","MEMBERSHIP","MEMBERS"]
            aboutClctnVew.delegate = self
            aboutClctnVew.dataSource = self
            aboutClctnVew.reloadData()
            
            cltnTypeList.delegate = self
            cltnTypeList.dataSource = self
//            cltnTypeList.reloadData()
        }
        self.lblFamName.text = appDel.groupNamePublic
        let temp = appDel.groupImageUrlPublic.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        let imgUrl = URL(string: temp)
        if imgUrl != nil{
            self.imgCover.kf.indicatorType = .activity
            
            let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=400&height=300&url="+imgUrl!.relativeString
            let urlImg = URL(string: newUrlStr)
            
            self.imgCover.kf.setImage(with: urlImg, placeholder: #imageLiteral(resourceName: "Family Logo"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        else{
            self.imgCover.image = #imageLiteral(resourceName: "family_default_cover.png")
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        filter = ""
        let btn = UIButton()
        btn.tag = 1
        onClickChangeFilter(btn.self)
        getMembershipTypeList()
    }
    
    override func viewDidLayoutSubviews(){
        super.viewDidLayoutSubviews()
        let indexPath = IndexPath(item: ActiveTab, section: 0)
        aboutClctnVew.scrollToItem(at: indexPath, at:.centeredHorizontally, animated: true)
    }
    
    
    //MARK:- WEB API
    func getMembershipTypeList(){
        let parameter = ["group_id":self.groupID, "user_id":UserDefaults.standard.value(forKey: "userId") as! String,"filter":filter] as [String : Any]
        self.dataArr.removeAll()
        self.cltnTypeList.isHidden = true
        ActivityIndicatorView.show("Loading.....")
        networkProvider.request(.membership_dashboard(parameter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                   
                    if response.statusCode == 200{
                        print("jsonData : \(jsonData)")
                        self.dataArr = jsonData["membership_data"].arrayValue
                        let tempArr = jsonData["membership_counts"].arrayValue
                        self.defaultID = tempArr[0]["default_membership_id"].intValue
                        
                        self.lblAllMembr.text = "All(\(tempArr[0]["all_count"].stringValue))"
                        if let activeCount = tempArr[0]["active_count"].string{
                            if activeCount != "0"{
                                self.lblActiveMembr.text =  "Current(\(tempArr[0]["active_count"].stringValue))"
                                self.btnActiveMembers.isEnabled = true
                            }
                            else{
                                self.lblActiveMembr.text =  "Current(\(tempArr[0]["active_count"].stringValue))"
                                self.btnActiveMembers.isEnabled = false
                                
                            }
                        }
                        if let inactiveCount = tempArr[0]["expaired_count"].string{
                            if inactiveCount != "0"{
                                self.lblInactiveMembr.text =  "Expired(\(tempArr[0]["expaired_count"].stringValue))"
                                self.btnInactiveMembers.isEnabled = true
                            }
                            else{
                                self.lblInactiveMembr.text =  "Expired(\(tempArr[0]["expaired_count"].stringValue))"
                                self.btnInactiveMembers.isEnabled = false
                                
                            }
                        }
                        if let due = tempArr[0]["total_due_amount"].string, due.isEmpty{
                            self.lblDueAmnt.text = "No dues"
                        }
                        else{
                            self.lblDueAmnt.text = tempArr[0]["total_due_amount"].stringValue
                        }
                        
                        if self.dataArr.count > 0{
                            self.noDataView.isHidden = true
                             self.cltnTypeList.isHidden = false
                            self.cltnTypeList.reloadData()
                        }
                        else{ //Add no data view
                            self.noDataView.isHidden = false
                            if self.filter.isEmpty{
                                self.lblMsg.text = "No data found. You can add a membership type here"
                                self.btnFloating.isHidden = true
                            }
                            else{
                               self.lblMsg.text = "No result found"
                                self.btnNoDataAdd.isHidden = true
                            }
                            
                             self.cltnTypeList.reloadData()
                        }
                        
                    }
                    else if response.statusCode == 401{
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getMembershipTypeList()
                        }
                    }
                    else{
                        self.displayAlertChoice(alertStr: "Oops! Something went wrong. Please try again", title: "") { (result) in
                            self.getMembershipTypeList()
                        }
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }

    
    //MARK:- Button Actions
    @IBAction func onClickBack(_ sender: Any) {
         self.navigationController?.popToViewController(ofClass: FamilyDetailsTableViewController.self)
    }
    @IBAction func onClickAddType(_ sender: Any) {
        let story = UIStoryboard.init(name: "MembershipMngmnt", bundle: nil)
        let addType =  story.instantiateViewController(withIdentifier: "AddMembershipTypeViewController") as! AddMembershipTypeViewController
        addType.groupID = self.groupID
  
        self.navigationController?.pushViewController(addType, animated: true)
    }
    
    @IBAction func onClickChangeFilter(_ sender: UIButton) {
        if sender.tag == 1{
            viewAllMembers.backgroundColor = #colorLiteral(red: 0.4941176471, green: 0.3411764706, blue: 0.7607843137, alpha: 1)
            lblAllMembr.textColor = .white
            viewActiveMember.backgroundColor = #colorLiteral(red: 0.7803921569, green: 0.7803921569, blue: 0.8, alpha: 1)
            lblActiveMembr.textColor = .black
        viewInactiveMember.backgroundColor = #colorLiteral(red: 0.7803921569, green: 0.7803921569, blue: 0.8, alpha: 1)
            lblInactiveMembr.textColor = .black
            
            filter = ""
        }
        else if sender.tag == 2{
            viewActiveMember.backgroundColor = #colorLiteral(red: 0.4941176471, green: 0.3411764706, blue: 0.7607843137, alpha: 1)
            lblActiveMembr.textColor = .white
            
            viewInactiveMember.backgroundColor = #colorLiteral(red: 0.7803921569, green: 0.7803921569, blue: 0.8, alpha: 1)
            lblInactiveMembr.textColor = .black
            viewAllMembers.backgroundColor = #colorLiteral(red: 0.7803921569, green: 0.7803921569, blue: 0.8, alpha: 1)
            lblAllMembr.textColor = .black
            
            filter = "current"
        }
        else{
            viewInactiveMember.backgroundColor = #colorLiteral(red: 0.4941176471, green: 0.3411764706, blue: 0.7607843137, alpha: 1)
            lblInactiveMembr.textColor = .white
            
            viewActiveMember.backgroundColor = #colorLiteral(red: 0.7803921569, green: 0.7803921569, blue: 0.8, alpha: 1)
            lblActiveMembr.textColor = .black
            viewAllMembers.backgroundColor = #colorLiteral(red: 0.7803921569, green: 0.7803921569, blue: 0.8, alpha: 1)
            lblAllMembr.textColor = .black
            
            filter = "overdue"
        }
        getMembershipTypeList()
    }
    
}

extension MemebrshipDashboardViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 2{
            return aboutArr.count
        }
        else{
            return dataArr.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 2{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AboutFamilyCollectionViewCell", for: indexPath as IndexPath) as! AboutFamilyCollectionViewCell
            cell.lblName.text = aboutArr[indexPath.item]
            if indexPath.row == ActiveTab{
                cell.lblName.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                cell.lblName.font = .boldSystemFont(ofSize: 15)
                cell.imgUnderline.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                
            }
            else{
                cell.lblName.textColor = .black
                cell.lblName.font = .systemFont(ofSize: 14)
                cell.imgUnderline.backgroundColor = .white
            }
            return cell
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MembershipDashboardCollectionViewCell", for: indexPath as IndexPath) as! MembershipDashboardCollectionViewCell

            let type = dataArr[indexPath.row]
            cell.lblMemberNo.text = type["member_count"].stringValue
            cell.lblMembrshipType.text = type["membership_name"].stringValue
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 2{
            if indexPath.row == 0{ // Feeds
                let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "PostedInGroupsViewController") as! PostedInGroupsViewController
                addMember.groupId = self.groupID
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.facate = self.facate
                addMember.famSettings = famSettings
                addMember.announcementSet = announcementSet
                addMember.postId                    = self.postId
                addMember.announcementId            = self.announcementId
                addMember.isMembershipActive = self.isMembershipActive
                addMember.famArr = famArr
                addMember.memberJoining = self.memberJoining
                addMember.invitationStatus = self.invitationStatus
                self.navigationController?.pushViewController(addMember, animated: false)
            }
            else if indexPath.row == 1{ // Announcement
                let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "AnnouncementInFamilyViewController") as! AnnouncementInFamilyViewController
                addMember.groupId = self.groupID
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.facate = self.facate
                addMember.famSettings = famSettings
                addMember.announcementSet = announcementSet
                addMember.postId                    = self.postId
                addMember.announcementId            = self.announcementId
                addMember.isMembershipActive = self.isMembershipActive
                addMember.famArr = famArr
                addMember.memberJoining = self.memberJoining
                addMember.invitationStatus = self.invitationStatus
                self.navigationController?.pushViewController(addMember, animated: false)
            }
            else if indexPath.row == 2{ // Request
                let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "RequestInFamilyViewController") as! RequestInFamilyViewController
                addMember.groupID = self.groupID
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.facate = self.facate
                addMember.famSettings = famSettings
                addMember.announcementSet = announcementSet
                addMember.postId                    = self.postId
                addMember.announcementId            = self.announcementId
                addMember.isMembershipActive = self.isMembershipActive
                addMember.famArr = famArr
                addMember.memberJoining = self.memberJoining
                addMember.invitationStatus = self.invitationStatus
                self.navigationController?.pushViewController(addMember, animated: false)
            }
            else if indexPath.row == 3{ // Events
                let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "EventListingViewController") as! EventListingViewController
                addMember.groupID = self.groupID
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.facte = self.facate
                addMember.famSettings = famSettings
                addMember.announcementSet = announcementSet
                addMember.postId                    = self.postId
                addMember.announcementId            = self.announcementId
                addMember.isMembershipActive = self.isMembershipActive
                addMember.famArr = famArr
                addMember.memberJoining = self.memberJoining
                addMember.invitationStatus = self.invitationStatus
                self.navigationController?.pushViewController(addMember, animated: false)
            }
            else if indexPath.row == 5{ // About us
                let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
                intro.groupId = self.groupID
                intro.isAboutUsClicked = true
                self.navigationController?.pushViewController(intro, animated: false)
            }
            else if indexPath.row == 8{ // Members
                if isAdmin.lowercased() == "admin"{
                    let storyboard = UIStoryboard.init(name: "MembershipMngmnt", bundle: nil)
                    let addMember = storyboard.instantiateViewController(withIdentifier: "MemebrshipDashboardViewController") as! MemebrshipDashboardViewController
                    addMember.groupID = self.groupID
                    addMember.isAdmin = self.isAdmin
                    addMember.link_type = self.link_type
                    addMember.islinkable = self.islinkable
                    //                        addMember.isFromdocument = true
                    addMember.facate = self.facate
                    addMember.famSettings = famSettings
                    addMember.announcementSet = announcementSet
                    addMember.postId                    = self.postId
                    addMember.announcementId            = self.announcementId
                    addMember.isMembershipActive = self.isMembershipActive
                    addMember.famArr = famArr
                    addMember.memberJoining = self.memberJoining
                    addMember.invitationStatus = self.invitationStatus
                    self.navigationController?.pushViewController(addMember, animated: true)
                }
                else{
                    let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                    let addMember = storyboard.instantiateViewController(withIdentifier: "AddMembersNewViewController") as! AddMembersNewViewController
                    addMember.groupId = self.groupID
                    addMember.isAdmin = self.isAdmin
                    addMember.link_type = self.link_type
                    addMember.islinkable = self.islinkable
                    addMember.faCate = self.facate
                    addMember.famSettings = famSettings
                    addMember.announcementSet = announcementSet
                    addMember.postId                    = self.postId
                    addMember.announcementId            = self.announcementId
                    addMember.isMembershipActive = self.isMembershipActive
                    addMember.famArr = famArr
                    addMember.memberJoiningStatus = self.memberJoining
                    addMember.invitationStatus = self.invitationStatus
                    self.navigationController?.pushViewController(addMember, animated: false)
                }

            }
            else if indexPath.row == 9{
                let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "AddMembersNewViewController") as! AddMembersNewViewController
                addMember.groupId = self.groupID
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.faCate = self.facate
                addMember.famSettings = famSettings
                addMember.announcementSet = announcementSet
                addMember.postId                    = self.postId
                addMember.announcementId            = self.announcementId
                addMember.isMembershipActive = self.isMembershipActive
                addMember.famArr = famArr
                addMember.memberJoiningStatus = self.memberJoining
                addMember.invitationStatus = self.invitationStatus
                self.navigationController?.pushViewController(addMember, animated: false)
            }
            else if indexPath.row == 4{ // Albums
                let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "AlbumListingViewController") as! AlbumListingViewController
                addMember.groupID = groupID
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.facate = self.facate
                addMember.famSettings = famSettings
                addMember.announcementSet = announcementSet
                addMember.postId                    = self.postId
                addMember.announcementId            = self.announcementId
                addMember.isMembershipActive = self.isMembershipActive
                addMember.famArr = famArr
                addMember.memberJoining = self.memberJoining
                addMember.invitationStatus = self.invitationStatus
                self.navigationController?.pushViewController(addMember, animated: false)
            }
            else if indexPath.row == 6{ // Documents
                let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "DocumentListingViewController") as! DocumentListingViewController
                addMember.groupID = self.groupID
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.isFromdocument = true
                addMember.facate = self.facate
                addMember.famSettings = famSettings
                addMember.announcementSet = announcementSet
                addMember.postId                    = self.postId
                addMember.announcementId            = self.announcementId
                addMember.isMembershipActive = self.isMembershipActive
                addMember.famArr = famArr
                addMember.memberJoining = self.memberJoining
                addMember.invitationStatus = self.invitationStatus
                self.navigationController?.pushViewController(addMember, animated: false)
            }
            else{ // Linked families
                let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "LinkedFamiliesListViewController") as! LinkedFamiliesListViewController
                addMember.groupID = self.groupID
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.facate = self.facate
                addMember.famSettings = famSettings
                addMember.announcementSet = announcementSet
                addMember.postId                    = self.postId
                addMember.announcementId            = self.announcementId
                addMember.isMembershipActive = self.isMembershipActive
                addMember.famArr = famArr
                addMember.memberJoining = self.memberJoining
                addMember.invitationStatus = self.invitationStatus
                self.navigationController?.pushViewController(addMember, animated: false)
            }
        }
        else{
            let type = dataArr[indexPath.row]
           
            let story = UIStoryboard.init(name: "MembershipMngmnt", bundle: nil)
            let vc =  story.instantiateViewController(withIdentifier: "MemberTypeUserListViewController") as! MemberTypeUserListViewController
            vc.groupID = self.groupID
            vc.Typetitle = type["membership_name"].stringValue
            vc.typeId = type["membershipid"].intValue
            vc.typeItem = type
            vc.defaultID =  self.defaultID
            self.navigationController?.pushViewController(vc, animated: true)
        }
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 2{
            let w = aboutArr[indexPath.row].size(withAttributes: nil)
            return CGSize(width: w.width, height: 48)
        }
        else{
            let numberOfCellInRow  = 2
            let padding : Int      = 10
            let collectionCellWidth : CGFloat = (self.cltnTypeList.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
            return CGSize(width: collectionCellWidth, height: 120)
        }
    }
}
