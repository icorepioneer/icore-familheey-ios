//
//  UserMembershipEditViewController.swift
//  familheey
//
//  Created by familheey on 19/05/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit
import iOSDropDown
import Moya
import SwiftyJSON
import Kingfisher

class UserMembershipEditViewController: UIViewController {
    @IBOutlet weak var lblTitke: UILabel!
    @IBOutlet weak var txtMembershipType: UITextField!
    @IBOutlet weak var txtDuration: UITextField!
    @IBOutlet weak var txtPaymentStatus: UITextField!
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var txtPaidOn: UITextField!
    @IBOutlet weak var txtNoteVew: UITextView!
    @IBOutlet weak var txtMembersNote: UITextView!
    
    @IBOutlet weak var lblDueAmount: UILabel!
    @IBOutlet weak var txtMembershipFrom: UITextField!
    @IBOutlet weak var txtMembershipTo: UITextField!
    
    @IBOutlet weak var tblMembrType: UITableView!
    @IBOutlet weak var tblMembrType_height: NSLayoutConstraint!
    @IBOutlet weak var tblDuration: UITableView!
    @IBOutlet weak var tblDuration_height: NSLayoutConstraint!
    @IBOutlet weak var tblPaymentStatus: UITableView!
    @IBOutlet weak var tblPaymentstatus_height: NSLayoutConstraint!
    
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    var theme: SambagTheme = .light
    
    var durationArr = [JSON]()
    var statusArr = ["Completed","Partial","Pending"]
    var groupID = ""
    var userID = ""
    var userName = ""
    var membershipID = ""
    var membershipPrdType = ""
    var membershipTypeId = 0
    var numberofDays = 0
    var typeArr = [JSON]()
    var details = JSON()
    var userData = JSON()
    var typeTitle = ""
    var isFromEdit = false
    
    var selectedStartDate:String = ""
    var selectedStartDateFormatted:String = ""
    var startDateTimestamp:Int! = 0
    var startDateTimeStampString:String = ""
    
    var selectedFromDate:String = ""
    var selectedFromDateFormatted:String = ""
    var fromDateTimestamp:Int! = 0
    var fromDateTimeStampString:String = ""
    
   var dateTag:Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AutofillPaymentStartDate()
        
        self.tblMembrType.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.tblDuration.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.tblPaymentStatus.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        tblDuration.delegate = self
        tblDuration.dataSource = self
        tblPaymentStatus.delegate = self
        tblPaymentStatus.dataSource = self
        tblPaymentStatus.reloadData()
        
        lblTitke.text = userName
        if details.count > 0{
            setUserDetails()
        }
        getMembershipTypes()
    }
    
    //MARK:- Set UI
    func setUserDetails(){
        for item in typeArr{
            if item["membership_name"].stringValue.lowercased() == self.typeTitle.lowercased(){
                details = item
                break
            }
        }
        
        if isFromEdit{
            txtMembershipType.text = userData["membership_type"].stringValue
            
            if let amount = userData["membership_fees"].int, amount > 0{
                txtAmount.text = "\(amount)"
                
                if let paidAmount = userData["membership_total_payed_amount"].int, paidAmount > 0{
                    let due = amount - paidAmount
                    lblDueAmount.text = "Due Amount :\(due)"
                }
                else{
                    lblDueAmount.text = "Due Amount : 0"
                }
            }
            else{
                txtAmount.text = "0"
                lblDueAmount.text = "Due Amount : 0"
            }
            

            txtNoteVew.text = userData["membership_payment_notes"].string ?? ""
            txtMembersNote.text = userData["membership_customer_notes"].string ?? ""
            membershipID = userData["membership_id"].stringValue
            
            if let status = userData["membership_payment_status"].string, !status.isEmpty{
                txtPaymentStatus.text = status
            }
            else{
                txtPaymentStatus.text = "Select"
            }
            
            if let num = userData["membership_duration"].int, num > 0{
                self.numberofDays = num
            }
            
            if let typeId = userData["membership_period_type_id"].int, typeId > 0{
                self.membershipTypeId = typeId
            }
            
            if let  paidOn = userData["membership_paid_on"].int, paidOn != 0{
                startDateTimeStampString = "\(paidOn)"
                txtPaidOn.text = Helpers.convertTimeStampWithoutTime(dateAsString: "\(paidOn)")
            }
            else{
                //                AutofillStartDatewithToday()
            }
            if let  fromDate = userData["membership_from"].string, !fromDate.isEmpty{
                fromDateTimeStampString = "\(fromDate)"
                txtMembershipFrom.text = Helpers.convertTimeStampWithoutTime(dateAsString: "\(fromDate)")
            }
            else{
                AutofillStartDatewithToday()
            }
            
            if let  toDate = userData["membership_to"].string, !toDate.isEmpty{
                startDateTimeStampString = "\(toDate)"
                if membershipTypeId == 4{
                    txtMembershipTo.text = "Life Time"
                }
                else{
                    txtMembershipTo.text = Helpers.convertTimeStampWithoutTime(dateAsString: "\(toDate)")
                }
            }
            else{
                //                AutofillStartDatewithToday()
            }
            
            if let duratn = userData["membership_period_type"].string, !duratn.isEmpty{
                self.membershipPrdType = duratn
                txtDuration.text = duratn
            }
            else{
                txtDuration.text = "Select"
            }
                        
           /* if details["membership_period"].stringValue == "7"{
                durationArr = ["1 Week","2 Week","3 Week","4 Week"]
            }
            else if details["membership_period"].stringValue == "30"{
                durationArr = ["1 Month","2 Month","3 Month","4 Month","5 Month","6 Month"]
            }
            else if details["membership_period"].stringValue == "365"{
                durationArr = ["1 Year","2 Year","3 Year","4 Year","5 Year","6 Year"]
            }
            else{
                durationArr = ["Life time"]
            }*/
            self.durationArr = self.details["types"].arrayValue
            
            /*if membershipPeriod != 0{
                if numberofDays == 3650{
                    txtDuration.text = "Life time"
                }
                else if details["membership_period"].stringValue == "7"{
                    txtDuration.text = "\(numberofDays / membershipPeriod!) Week"
                }
                else if details["membership_period"].stringValue == "30"{
                    txtDuration.text = "\(numberofDays / membershipPeriod!) Month"
                }
                else if details["membership_period"].stringValue == "365"{
                    txtDuration.text = "\(numberofDays / membershipPeriod!) Year"
                }
                else{
                    txtDuration.text = "Select"
                }
            }
            else{
                txtDuration.text = "Select"
                
            }*/
        }
        else{
            if details.count > 0{
                txtMembershipType.text = details["membership_name"].stringValue
                self.membershipID = details["id"].stringValue
                
                if let num = details["membership_period"].int, num > 0{
                    self.numberofDays = num
                }
                if let typeId = details["membership_period_type_id"].int, typeId > 0{
                    self.membershipTypeId = typeId
                }
                if let  fromDate = details["membership_from"].string, !fromDate.isEmpty{
                    
                }
                else{
                    AutofillStartDatewithToday()
                }
                lblDueAmount.text = "Due Amount : 0"
               /* if details["membership_period"].stringValue == "7"{
                    durationArr = ["1 Week","2 Week","3 Week","4 Week"]
                }
                else if details["membership_period"].stringValue == "30"{
                    durationArr = ["1 Month","2 Month","3 Month","4 Month","5 Month","6 Month"]
                }
                else if details["membership_period"].stringValue == "365"{
                    durationArr = ["1 Year","2 Year","3 Year","4 Year","5 Year","6 Year"]
                }
                else{
                    durationArr = ["Life time"]
                }*/
                self.durationArr = self.details["types"].arrayValue
                
                txtDuration.text = "Select"
                txtPaymentStatus.text = "Select"
//                AutofillStartDatewithToday()
            }
        }
    }
    
    func setDatetoTextfield(result:String){
       
    }
    
    //MARK:- Web Api's
    func getMembershipTypes(){
        let parameter = ["group_id":self.groupID, "user_id":UserDefaults.standard.value(forKey: "userId") as! String] as [String : Any]
        ActivityIndicatorView.show("Loading.....")
        networkProvider.request(.list_membership_lookup(parameter: parameter)){ (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    
                    if response.statusCode == 200{
                        print("jsonData : \(jsonData)")
                        self.typeArr = jsonData.arrayValue
                        if self.typeArr.count > 0{
                            self.setUserDetails()
                            
                            self.tblMembrType.delegate = self
                            self.tblMembrType.dataSource = self
                            self.tblMembrType.reloadData()
                        }
                        else{
                            self.tblMembrType.delegate = self
                            self.tblMembrType.dataSource = self
                            self.tblMembrType.reloadData()
                        }
              
                    }
                    else if response.statusCode == 401{
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getMembershipTypes()
                        }
                    }
                    else{
                       ActivityIndicatorView.hiding()
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    func addMembershipDetails(){
        var parameter = [String:Any]()
        if isFromEdit{
            parameter = ["user_id":userID,"group_id":self.groupID,"membership_id":membershipID,"membership_fees":txtAmount.text!,"membership_payment_status":txtPaymentStatus.text!,"membership_paid_on":startDateTimeStampString,"membership_duration":numberofDays,"membership_payment_notes":txtNoteVew.text ?? "","membership_period_type":membershipPrdType,"membership_from":fromDateTimeStampString,"edited_user_id":UserDefaults.standard.value(forKey: "userId") as! String] as [String : Any]
            
            if let mbrshipTo =  userData["membership_to"].string, !mbrshipTo.isEmpty{
                parameter["membership_to"] = mbrshipTo
            }

        }
        else{
            parameter = ["user_id":userID,"group_id":self.groupID,"membership_id":membershipID,"membership_fees":txtAmount.text!,"membership_payment_status":txtPaymentStatus.text!,"membership_paid_on":startDateTimestamp!,"membership_duration":numberofDays,"membership_payment_notes":txtNoteVew.text ?? "","membership_period_type":membershipPrdType,"membership_from":fromDateTimeStampString,"edited_user_id":UserDefaults.standard.value(forKey: "userId") as! String] as [String : Any]
        }
        
        print(parameter)
        
        ActivityIndicatorView.show("Loading.....")
        networkProvider.request(.user_membership_update(parameter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    if response.statusCode == 200{
                        print("jsonData : \(jsonData)")
                        self.displayAlertChoice(alertStr: "Membership details added successfully", title: "") { (result) in
                            self.navigationController?.popToViewController(ofClass: MemberTypeUserListViewController.self)
                        }
                        
                    }
                    else if response.statusCode == 401{
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.addMembershipDetails()
                        }
                    }
                    else{
                        ActivityIndicatorView.hiding()
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    //MARK:- Button actions
    @IBAction func onCickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickMembershipTypeSelect(_ sender: Any) {
        if tblMembrType.isHidden{
            tblMembrType.isHidden = false
            if tblMembrType.contentSize.height < 250{
                tblMembrType_height.constant = tblMembrType.contentSize.height
            }
            else{
                tblMembrType_height.constant = 250
            }
            
        }
        else{
            tblMembrType.isHidden = true
            tblMembrType_height.constant = 0
        }
    }
    
    @IBAction func onClickDurationSelect(_ sender: Any) {
        if txtMembershipType.text!.isEmpty{
            self.displayAlert(alertStr: "Please select membership type", title: "")
        }
        else{
            if tblDuration.isHidden{
                tblDuration.isHidden = false
                tblDuration.reloadData()
                
                if tblDuration.contentSize.height < 250{
                    tblDuration_height.constant = tblDuration.contentSize.height
                }
                else{
                   tblDuration_height.constant = 250
                }
                
                
                
            }
            else{
                tblDuration.isHidden = true
                tblDuration_height.constant = 0
            }
        }
    }
    @IBAction func onClickPaymentStatus(_ sender: Any) {
        if tblPaymentStatus.isHidden{
            tblPaymentStatus.isHidden = false
            tblPaymentstatus_height.constant = 150
        }
        else{
            tblPaymentStatus.isHidden = true
            tblPaymentstatus_height.constant = 0
        }
    }
    @IBAction func onClickPaidOnSelect(_ sender: Any) {
        dateTag = 1
        let vc = SambagDatePickerViewController()
        vc.theme = theme
        vc.delegate = self
        
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func onClickSave(_ sender: Any) {
        if txtMembershipType.text!.isEmpty{
            self.displayAlert(alertStr: "Please select membership type", title: "")
            return
        }
        else  if txtDuration.text?.lowercased() == "select"{
            self.displayAlert(alertStr: "Please select membership duration", title: "")
            return
        }
        else if txtPaymentStatus.text?.lowercased() == "select"{
            self.displayAlert(alertStr: "Please select payment status", title: "")
            return
        }
        else if txtAmount.text!.isEmpty{
            self.displayAlert(alertStr: "Please add total fees paid", title: "")
            return
        }
//        else if txtPaidOn.text!.isEmpty{
//            self.displayAlert(alertStr: "Please select payment date", title: "")
//            return
//        }
        else if txtMembershipFrom.text!.isEmpty{
            self.displayAlert(alertStr: "Please select membership from date", title: "")
            return
        }
        else{
            if !isFromEdit{
                
            }
            print(startDateTimestamp)
            addMembershipDetails()
            
        }
    }
    
    @IBAction func onClickFromDateSelect(_ sender: Any) {
        dateTag = 0
        let vc = SambagDatePickerViewController()
        vc.theme = theme
        vc.delegate = self
        
        present(vc, animated: true, completion: nil)
    }
    
    
    //MARK:- Custom date function
    func AutofillStartDatewithToday()
    {
        let currentDateTime = Date()
        print(currentDateTime)
        
        //For display
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd yyyy"
        formatter.timeZone = TimeZone.current
        let localDatef = formatter.string(from: currentDateTime)
        txtMembershipFrom.text = "\(localDatef)"
        
        // converting to time stamp
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "dd/MM/yyyy   hh:mm a"
        formatter2.timeZone = NSTimeZone.local
        let localDatef2 = formatter2.string(from: currentDateTime)
        selectedStartDate = "\(localDatef2)"
        print(selectedStartDate)
        startDateTimestamp = self.convertTimetoTimestamp(date: self.selectedStartDate)
        
        if startDateTimestamp != 0{
            startDateTimeStampString = "\(startDateTimestamp!)"
            fromDateTimeStampString =  startDateTimeStampString
        }
        print(startDateTimeStampString)
        
        let formatter3 = DateFormatter()
        formatter3.dateFormat = "dd/MM/yyyy"
        let localDatef3 = formatter3.string(from: currentDateTime)
        self.calculateToDateUsingDuration(dateString: localDatef3)
    }
    
    func AutofillPaymentStartDate(){
        let currentDateTime = Date()
        print(currentDateTime)
        
        // converting to time stamp
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "dd/MM/yyyy   hh:mm a"
        formatter2.timeZone = NSTimeZone.local
        let localDatef2 = formatter2.string(from: currentDateTime)
        selectedStartDate = "\(localDatef2)"
        print(selectedStartDate)
        startDateTimestamp = self.convertTimetoTimestamp(date: self.selectedStartDate)
        
        if startDateTimestamp != 0{
            startDateTimeStampString = "\(startDateTimestamp!)"
        }
        print(startDateTimeStampString)
    }
}

extension UserMembershipEditViewController: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
         if textField == txtMembershipType{
             txtMembershipType.resignFirstResponder();
             // Additional code here
         }
    }
}

extension UserMembershipEditViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 1{
           return self.typeArr.count
        }
        else if tableView.tag == 2{
            return durationArr.count
        }
        else{
            return statusArr.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as UITableViewCell
        
        if tableView.tag == 1{
            cell.textLabel?.text = typeArr[indexPath.row]["membership_name"].stringValue
        }
        else if tableView.tag == 2{
            let type = durationArr[indexPath.row]
            
            cell.textLabel?.text = type["type_name"].stringValue
        }
        else{
            cell.textLabel?.text = statusArr[indexPath.row]
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.tag == 1{
            let type = typeArr[indexPath.row]
            self.txtMembershipType.text = type["membership_name"].stringValue
            self.membershipID = type["id"].stringValue
            self.details = type
            
            if let num = type["membership_period_type_id"].int, num > 0{
                self.membershipTypeId = num
            }
            
            self.durationArr = type["types"].arrayValue
            self.tblMembrType.isHidden = true
            self.txtDuration.text = "Select"
        }
        else if tableView.tag == 2{
            let type = durationArr[indexPath.row]
            
            self.txtDuration.text = type["type_name"].stringValue
            
            self.membershipPrdType = type["type_name"].stringValue
            
            if type["type_name"].stringValue.lowercased() == "life time"{
                txtAmount.text = details["membership_fees"].stringValue
            }
            else{
                let multiple = indexPath.row + 1
                numberofDays = type["type_value"].int ?? 0
                let fee = details["membership_fees"].int!
                let total = fee * multiple
                txtAmount.text = "\(total)"
            }
            self.tblDuration.isHidden = true
            if !isFromEdit{
                if txtMembershipFrom.text!.isEmpty{
                    self.AutofillStartDatewithToday()
                }
                else{
                    convertThenCallCalculate(dateString: txtMembershipFrom.text!)
                }
               
            }
            else{
                if txtMembershipFrom.text!.isEmpty{
                    self.AutofillStartDatewithToday()
                }
                else{
                    self.AutofillStartDatewithToday()
//                    convertThenCallCalculate(dateString: txtMembershipFrom.text!)
                }
            }
        }
        else{
            self.txtPaymentStatus.text = statusArr[indexPath.row]
            tblPaymentStatus.isHidden = true
        }
    }
    
}

extension UserMembershipEditViewController: SambagDatePickerViewControllerDelegate{
    func didSelectDate(result:String,dateTag:Int){
        
         if dateTag == 0{
            selectedFromDate = "\(result)   00:00 a"
            txtMembershipFrom.text = Helpers.sambagTimeDisplaywithoutTime2(dateAsString: "\(result)")
            fromDateTimestamp = self.convertTimetoTimestamp(date: self.selectedFromDate)
            if fromDateTimestamp != 0{
                fromDateTimeStampString = "\(fromDateTimestamp!)"
            }
            self.calculateToDateUsingDuration(dateString: "\(result)")
            self.selectedFromDateFormatted = self.convertDateFormat(date: selectedFromDate)
        }
        else{
            
            selectedStartDate = "\(result)   00:00 a"
            print(selectedStartDate)
            
            txtPaidOn.text = Helpers.sambagTimeDisplaywithoutTime2(dateAsString: "\(result)")
            startDateTimestamp = self.convertTimetoTimestamp(date: self.selectedStartDate)
            if startDateTimestamp != 0{
                startDateTimeStampString = "\(startDateTimestamp!)"
            }
            self.selectedStartDateFormatted = self.convertDateFormat(date: selectedStartDate)
        }
        
    }
    
    func sambagDatePickerDidSet(_ viewController: SambagDatePickerViewController, result: SambagDatePickerResult) {
        if dateTag == 0{
            selectedFromDate = "\(result)   00:00 a"
            txtMembershipFrom.text = Helpers.sambagTimeDisplaywithoutTime2(dateAsString: "\(result)")
            fromDateTimestamp = self.convertTimetoTimestamp(date: self.selectedFromDate)
            if fromDateTimestamp != 0{
                fromDateTimeStampString = "\(fromDateTimestamp!)"
            }
            self.calculateToDateUsingDuration(dateString: "\(result)")
            self.selectedFromDateFormatted = self.convertDateFormat(date: selectedFromDate)
        }
        else{
            selectedStartDate = "\(result)   00:00 a"
            txtPaidOn.text = Helpers.sambagTimeDisplaywithoutTime2(dateAsString: "\(result)")
            startDateTimestamp = self.convertTimetoTimestamp(date: self.selectedStartDate)
            if startDateTimestamp != 0{
                startDateTimeStampString = "\(startDateTimestamp!)"
            }
            self.selectedStartDateFormatted = self.convertDateFormat(date: selectedStartDate)
        }
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func sambagDatePickerDidCancel(_ viewController: SambagDatePickerViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    //MARK: Custom Date Functions
    func convertDateFormat(date:String)->String{
        do{
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy hh:mm a"
            //        let convertedDate = formatter.date(from: "16-09-2017 2:00 PM")
            if  let convertedDate = try formatter.date(from: date){
                //                formatter.timeZone = TimeZone(identifier: "UTC")
                formatter.timeZone = NSTimeZone.local
                let date = formatter.string(from: convertedDate)
                print(date)
                let formatter2 = DateFormatter()
                formatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
                let convertedDate2 = formatter.date(from: date)
                formatter2.timeZone = NSTimeZone.local
                let date2 = formatter2.string(from: convertedDate2!)
                print(date2)
                
                return date2
            }
            else{
                return ""
            }
            
        }catch{
            return ""
        }
    }
    
    func convertThenCallCalculate(dateString:String){
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd yyyy"
        
        let convertedDate = formatter.date(from: dateString)
        
        formatter.dateFormat = "dd/MM/yyyy"
        let dateFormatted = formatter.string(from: convertedDate!)
        calculateToDateUsingDuration(dateString: dateFormatted)
    }
    
    func calculateToDateUsingDuration(dateString:String){
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        formatter.timeZone = NSTimeZone.local
        formatter.locale = .current
        let convertedDate = formatter.date(from: dateString)

        let toDate = convertedDate?.addDay(n: numberofDays)
        print(toDate)
        print(convertedDate)
        
        formatter.dateFormat = "MMM dd yyyy"
        if membershipTypeId == 4{
           txtMembershipTo.text = "Life Time"
        }
        else{
            txtMembershipTo.text = formatter.string(from: toDate!)
        }
        
    }
    
    func convertTimetoTimestamp(date:String)->Int{
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"
        //        let convertedDate = formatter.date(from: "16-09-2017 2:00 PM")
        let convertedDate = formatter.date(from: date)
        
        //        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.timeZone = NSTimeZone.local
        let date = formatter.string(from: convertedDate!)
        print(date)
        let convertedDate2 = formatter.date(from: date)
        
        let timestamp = convertedDate2!.timeIntervalSince1970
        print(timestamp)
        
        return Int(timestamp)
        
    }
}
