//
//  MemberTypeUserListViewController.swift
//  familheey
//
//  Created by familheey on 21/05/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher
import SwiftyJSON
import Moya

class MemberTypeUserListViewController: UIViewController {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblMemberList: UITableView!
    @IBOutlet weak var btnFloating: UIButton!
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var lblNoDataMsg: UILabel!
    @IBOutlet weak var btnNoDataAdd: UIButton!
    
    @IBOutlet weak var editView: UIView!
    
    
    private var networkProvider  = MoyaProvider<FamilyheeyApi>()
    var dataArr = [JSON]()
    
    var Typetitle = ""
    var typeId = 0
    var groupID = ""
    var typeItem = JSON()
    var defaultID = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblTitle.text = Typetitle
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if typeId == defaultID {
            editView.isHidden = true
        }
        else{
            editView.isHidden = false
        }
        
        getMembershipList()
    }
    
    
    //MARK:- WEB API's
    func getMembershipList(){
        let parameter = ["group_id":self.groupID, "crnt_user_id":UserDefaults.standard.value(forKey: "userId") as! String,"membership_id":typeItem["membershipid"].stringValue] as [String : Any]
        ActivityIndicatorView.show("Loading.....")
        networkProvider.request(.membership_user_list(parameter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    
                    if response.statusCode == 200{
                        print("jsonData : \(jsonData)")
                        self.dataArr = jsonData["data"].arrayValue

                        if self.dataArr.count > 0{
                            self.tblMemberList.delegate = self
                            self.tblMemberList.dataSource = self
                            self.tblMemberList.reloadData()
                            self.noDataView.isHidden = true
                            self.tblMemberList.isHidden = false
                            self.btnFloating.isHidden = false
                        }
                        else{ // Add no member view
                            self.noDataView.isHidden = false
                            self.tblMemberList.isHidden = true
                            self.lblNoDataMsg.text = "No members added"
                            self.btnFloating.isHidden = true
//                            self.tblMemberList.delegate = self
//                            self.tblMemberList.dataSource = self
//                            self.tblMemberList.reloadData()
                        }
                        
                    }
                    else if response.statusCode == 401{
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getMembershipList()
                        }
                    }
                    else{
                        
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    //MARK:- Button actions
    @IBAction func onClickBack(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickStartTopic(_ sender: UIButton) {
        var userArr = [String]()
         let user = dataArr[sender.tag]
        let uId = user["user_id"].stringValue
        
        userArr.append(uId)
        let storyboard = UIStoryboard.init(name: "Topic", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CreateTopicViewController") as! CreateTopicViewController
        vc.isFromCreate = false
        vc.isFromMembership  = true
        vc.toUsers = userArr
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickEditType(_ sender: Any) {
        let story = UIStoryboard.init(name: "MembershipMngmnt", bundle: nil)
        let vc =  story.instantiateViewController(withIdentifier: "AddMembershipTypeViewController") as! AddMembershipTypeViewController
        vc.groupID = self.groupID
        vc.isfromEdit = true
        vc.typeItem = typeItem
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickSendReminder(_ sender: UIButton) {
        let user = dataArr[sender.tag]
        
        let parameter = ["group_id":self.groupID, "loggedin_user_id":UserDefaults.standard.value(forKey: "userId") as! String,"to_userid":user["user_id"].stringValue] as [String : Any]
        ActivityIndicatorView.show("Loading.....")
        networkProvider.request(.Membership_reminder(parameter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    
                    if response.statusCode == 200{
                        print(jsonData)
                        self.displayAlert(alertStr: "Reminder notification send", title: "")
                        
                    }
                    else if response.statusCode == 401{
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            let btn = UIButton()
                            btn.tag = sender.tag
                            self.onClickBack(btn)
                        }
                    }
                    else{
                        
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    @IBAction func onClickEditUser(_ sender: UIButton) {
        let user = dataArr[sender.tag]
        
        let story = UIStoryboard.init(name: "MembershipMngmnt", bundle: nil)
        let vc =  story.instantiateViewController(withIdentifier: "UserMembershipEditViewController") as! UserMembershipEditViewController
        vc.groupID = self.groupID
        vc.userID = user["user_id"].stringValue
        vc.userName = user["full_name"].stringValue
        vc.userData = user
        vc.isFromEdit = true
        vc.typeTitle = self.Typetitle
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickAddMembershipUser(_ sender: Any) {
        let story = UIStoryboard.init(name: "MembershipMngmnt", bundle: nil)
        let vc =  story.instantiateViewController(withIdentifier: "MembershipFullUserListViewController") as! MembershipFullUserListViewController
        vc.groupId = self.groupID
        vc.typeTitle = self.Typetitle
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension MemberTypeUserListViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MembershipUserListTableViewCell", for: indexPath) as! MembershipUserListTableViewCell
        cell.selectionStyle = .none
        
        let user = dataArr[indexPath.row]
        cell.btnEdit.tag = indexPath.row
        cell.btnReminder.tag = indexPath.row
        cell.btnStartTopic.tag = indexPath.row
        
        cell.lblName.text = user["full_name"].stringValue
        cell.lblType.text = user["membership_type"].stringValue
        
        
        if user["is_expaired"].boolValue{
            cell.btnReminder.isHidden = false
            cell.lblValidity.text = "Validity expired"
            cell.lblValidity.textColor = .red
        }
        else{
            if user["membership_payment_status"].stringValue.lowercased() == "completed" || user["membership_payment_status"].stringValue.lowercased() == "success"{
                cell.btnReminder.isHidden = true
                cell.imgPending.backgroundColor =  #colorLiteral(red: 0.3843137255, green: 0.9803921569, blue: 0.1803921569, alpha: 1)
                cell.lblDueAmount.isHidden = true
            }
            else{
                cell.btnReminder.isHidden = false
                cell.lblDueAmount.isHidden = false
                cell.imgPending.backgroundColor =  #colorLiteral(red: 0.9725490196, green: 0.9294117647, blue: 0.06666666667, alpha: 1)
            }
            
            let totalFee = user["membership_fees"].intValue
            let paidAmount = user["membership_total_payed_amount"].intValue
            
            let due = totalFee - paidAmount
            cell.lblDueAmount.text = "Due Amount : \(due)"
            
            if let durtn = user["membership_period_type_id"].int, durtn == 4{
                cell.lblValidity.text = "Valid till: Life time"
            }
            else{
                cell.lblValidity.text = "Valid till: \(Helpers.convertTimeStampWithoutTime(dateAsString: user["membership_to"].stringValue))"
            }
        }
        
        
        
//        let temStr = "\(EventsTabViewController.convertTimeStampToDate(timestamp: user["membership_paid_on"].stringValue))"
//        let tempArr = temStr.components(separatedBy: " ")
//        cell.lblValidity.text = "\(tempArr[0]) \(tempArr[1]) \(tempArr[2])"
       
       
        if user["propic"].stringValue.count != 0{
            let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+user["propic"].stringValue.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let imgUrl = URL(string: temp)
            //                cell.imageViewAvatar.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
            let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+imgUrl!.relativeString
            let url = URL(string: newUrlStr)
            DispatchQueue.main.async {
                cell.imgProfile.kf.indicatorType = .activity
                cell.imgProfile.kf.setImage(with: url, placeholder:#imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
            }
        }
        else{
            DispatchQueue.main.async {
                cell.imgProfile.image = #imageLiteral(resourceName: "Male Colored")
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user = dataArr[indexPath.row]
        
        let story = UIStoryboard.init(name: "MembershipMngmnt", bundle: nil)
        let vc =  story.instantiateViewController(withIdentifier: "UserMembershipEditViewController") as! UserMembershipEditViewController
        vc.groupID = self.groupID
        vc.userID = user["user_id"].stringValue
        vc.userName = user["full_name"].stringValue
        vc.userData = user
        vc.isFromEdit = true
        vc.typeTitle = self.Typetitle
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
