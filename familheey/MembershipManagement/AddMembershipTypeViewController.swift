//
//  AddMembershipTypeViewController.swift
//  familheey
//
//  Created by familheey on 18/05/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit
import Moya
import Alamofire
import Kingfisher
import SwiftyJSON

class AddMembershipTypeViewController: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtType: UITextField!
    @IBOutlet weak var txtPeriodType: UITextField!
    @IBOutlet weak var tblPeriodTypeList: UITableView!
    @IBOutlet weak var txtFees: UITextField!
    @IBOutlet weak var swtActive: UISwitch!
    @IBOutlet weak var btnSave: UIButton!
    
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    var typeArr = ["Weekly","Monthly","Yearly","Life time"]
    
    var isActive = true
    var isfromEdit = false
    var groupID = ""
    var numofDays = 0
    var membershipID = ""
    var typeItem = JSON()
    var dataArr = JSON()
    var periodTypeArr = [JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        
        if isfromEdit{
            getMembershipPeriodType()
            
            lblTitle.text = "Edit Membership Type"
            btnSave.setTitle("Update", for: .normal)
        }
        else{
             getMembershipPeriodType()
            lblTitle.text = "Add Membership Type"
            btnSave.setTitle("Save", for: .normal)
        }
        tblPeriodTypeList.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tblPeriodTypeList.delegate = self
        tblPeriodTypeList.dataSource = self
        tblPeriodTypeList.reloadData()
    }
    
    //MARK:- WEB API's
    func saveMembershipType(){
        let parameter = ["group_id":self.groupID,"membership_name":txtType.text!,"membership_period":numofDays,"membership_period_type_id":membershipID,"membership_currency":"USD","membership_fees":txtFees.text!,"created_by":UserDefaults.standard.value(forKey: "userId") as! String,"is_active":isActive] as [String : Any]
        
        ActivityIndicatorView.show("Loading.....")
        networkProvider.request(.add_membership_lookup(parameter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    if response.statusCode == 200{
                        print("jsonData : \(jsonData)")
                        self.displayAlertChoice(alertStr: "Membership type added successfully", title: "") { (result) in
                            self.navigationController?.popViewController(animated: true)
                        }
                        
                    }
                    else if response.statusCode == 401{
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.saveMembershipType()
                        }
                    }
                    else{
                        ActivityIndicatorView.hiding()
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    func editMembershipType(){
        let parameter = ["group_id":self.groupID,"membership_name":txtType.text!,"membership_period":numofDays,"membership_period_type_id":membershipID,"membership_currency":"USD","membership_fees":txtFees.text!,"created_by":UserDefaults.standard.value(forKey: "userId") as! String,"is_active":isActive,"id":dataArr["id"].stringValue] as [String : Any]
        
        ActivityIndicatorView.show("Loading.....")
        networkProvider.request(.update_membership_lookup(parameter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    if response.statusCode == 200{
                        print("jsonData : \(jsonData)")
                        self.displayAlertChoice(alertStr: "Membership type edited successfully", title: "") { (result) in
                            self.navigationController?.popToViewController(ofClass: MemebrshipDashboardViewController.self)
                        }
                        
                    }
                    else if response.statusCode == 401{
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.editMembershipType()
                        }
                    }
                    else{
                        ActivityIndicatorView.hiding()
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    func getMembershipPeriodType(){
        let parameter = [String:Any]()
        ActivityIndicatorView.show("Loading.....")
        networkProvider.request(.get_membership_lookup_periods(parameter: parameter)){ (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    
                    if response.statusCode == 200{
                        print("jsonData : \(jsonData)")
                        
                        self.periodTypeArr = jsonData.arrayValue
                        
                        if self.periodTypeArr.count > 0{
                            self.tblPeriodTypeList.delegate = self
                            self.tblPeriodTypeList.dataSource = self
                            self.tblPeriodTypeList.reloadData()
                            if self.isfromEdit{
                                self.getMembershipType()
                            }
                        }
                        else{
                            self.tblPeriodTypeList.delegate = self
                            self.tblPeriodTypeList.dataSource = self
                            self.tblPeriodTypeList.reloadData()
                        }
                        
                    }
                    else if response.statusCode == 401{
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getMembershipPeriodType()
                        }
                    }
                    else{
                        self.displayAlertChoice(alertStr: "Oops something went wrong. Please try again", title: "") { (result) in
                            self.getMembershipPeriodType()
                        }
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    func getMembershipType(){
        let parameter = ["user_id":UserDefaults.standard.value(forKey: "userId") as! String,"membership_id":typeItem["membershipid"].stringValue] as [String : Any]
        ActivityIndicatorView.show("Loading.....")
        networkProvider.request(.get_membershiptype_ById(parameter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    
                    if response.statusCode == 200{
                        print("jsonData : \(jsonData)")
                        self.dataArr = jsonData["doc"]
                        
                        if self.dataArr.count > 0{
                            self.txtType.text = self.dataArr["membership_name"].stringValue
                            self.txtFees.text = self.dataArr["membership_fees"].stringValue
                            self.numofDays = self.dataArr["membership_period"].int ?? 0
                            self.membershipID = self.dataArr["membership_period_type_id"].stringValue
                            
                            if self.periodTypeArr.count > 0{
                                for index in self.periodTypeArr{
                                    if index["id"].stringValue == self.membershipID{
                                        self.txtPeriodType.text = index["membership_lookup_period_type"].stringValue
                                    }
                                }
                            }
                            
                           /* if let periodType = self.dataArr["membership_period_type"].string, !periodType.isEmpty{
                                self.txtPeriodType.text = periodType
                                self.numofDays = self.dataArr["membership_period"].int!
                            }
                            else{
                                if self.numofDays == self.dataArr["membership_period"].int!, self.numofDays > 0{
                                    if self.numofDays == 7{
                                        self.txtPeriodType.text = "Weekly"
                                    }
                                    else if self.numofDays == 30{
                                        self.txtPeriodType.text = "Monthly"
                                    }
                                    else if self.numofDays == 365{
                                        self.txtPeriodType.text = "Yearly"
                                    }
                                    else{
                                        self.txtPeriodType.text = "Life time"
                                    }
                                }
                            }*/
                            
                            if self.dataArr["is_active"].boolValue{
                                self.swtActive.setOn(true, animated: true)
                                self.isActive = true
                            }
                            else{
                                self.swtActive.setOn(false, animated: true)
                                self.isActive = false
                            }
                        }
                        else{
                            self.displayAlert(alertStr: "Something went wrong", title: "")
                        }
                        
                    }
                    else if response.statusCode == 401{
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getMembershipType()
                        }
                    }
                    else{
                        self.displayAlertChoice(alertStr: "Something went wrong", title: "") { (rslt) in
                            self.getMembershipType()
                        }
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    //MARK:- Button actions
    
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickSwitchAction(_ sender: UISwitch) {
        if sender.isOn{
            isActive = true
        }
        else{
            isActive = false
        }
    }
    @IBAction func onClickSave(_ sender: Any) {
        if txtType.text!.isEmpty{
            self.displayAlert(alertStr: "Pleae enter a name for membership type.", title: "")
            return
        }
        else if txtPeriodType.text!.isEmpty{
            self.displayAlert(alertStr: "Pleae select membership  duration", title: "")
            return
        }
        else if txtFees.text!.isEmpty{
            self.displayAlert(alertStr: "Pleae enter membership fees", title: "")
            return
        }
        else{
            if isfromEdit{
                editMembershipType()
            }
            else{
                saveMembershipType()
            }
        }
        
    }
    @IBAction func onClickPeriodTypeSelection(_ sender: Any) {
        if tblPeriodTypeList.isHidden{
            tblPeriodTypeList.isHidden = false
        }
        else{
            tblPeriodTypeList.isHidden = true
        }
    }
    
    
}

extension AddMembershipTypeViewController : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return periodTypeArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let type = periodTypeArr[indexPath.row]
        
        cell.textLabel?.text = type["membership_lookup_period_type"].stringValue
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let type = periodTypeArr[indexPath.row]
        self.numofDays = type["membership_lookup_period"].int ?? 0
        self.membershipID = type["id"].stringValue
        print(membershipID)
        self.txtPeriodType.text = type["membership_lookup_period_type"].stringValue
        self.tblPeriodTypeList.isHidden = true
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
}
