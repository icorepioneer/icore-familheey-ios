//
//  SearchPlacesViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 04/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SearchPlacesViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
    
    

    @IBOutlet weak var searchField            : UITextField!
    @IBOutlet weak var tbl                    : UITableView!
    var addressArr                            = NSArray()
    var selectedField                         = String()
    
    @IBOutlet weak var nameLbl: UILabel!
    
    var addressJSON                           = JSON()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        let leftView1                           = UILabel(frame: CGRect(x: 5, y: 0, width: 15, height: 26))
        leftView1.backgroundColor               = .clear
        searchField.leftView                    = leftView1
        searchField.leftViewMode                = .always
        searchField.contentVerticalAlignment    = .center
        searchField.delegate                    = self
        
        if let _ = UserDefaults.standard.value(forKey: "userName") as? String{
            
            nameLbl.text                        = "Hi, \(UserDefaults.standard.value(forKey: "userName") as! String)"
        }else{
            
            nameLbl.text                        = "Hi,"
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden    = true
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            
            self.searchField.becomeFirstResponder()
        })
    }
    
    //MARK: - Text field delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            
            self.checkTxtField()
        })
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func checkTxtField(){
        
        if !searchField.text!.isEmpty{
            
            if searchField.text!.count > 3{
                callGoogleApi(str: searchField.text!)
            }else{
                addressArr = NSArray()
                tbl.reloadData()
            }
        }
    }
    
    func callGoogleApi(str : String){
       
        //https://maps.googleapis.com/maps/api/geocode/json?address=\(str)&sensor=false&key=\(Consts.googlePlacesApiKey)
        //https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(str)&key=\(Consts.googlePlacesApiKey)

        //https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=\(str)&inputtype=textquery&fields=photos,formatted_address,name,rating,opening_hours,geometry&key=\(Consts.googlePlacesApiKey)
        
        
        Alamofire.request("https://maps.googleapis.com/maps/api/geocode/json?address=\(str.replacingOccurrences(of: " ", with: ""))&sensor=false&key=\(Consts.googlePlacesApiKey)", method: .get, encoding: JSONEncoding.default, headers: nil)
            .responseJSON { response in
                
                switch response.result {
                case .success(let json):
                    DispatchQueue.main.async {
                  //      print("Google result : \(response.result.value!)")
                        
                        if let dict = response.result.value as? NSDictionary{
                            
                            if let status = dict["status"] as? String{
                                if status == "OK"{
                                    if let results = dict["results"] as? NSArray{
                                        print(results)
                                        if let address_components = results.value(forKey: "address_components") as? NSArray{
                                            self.addressArr = NSArray()
                                            
                                            self.addressArr = address_components
                                            
                                            self.addressJSON = JSON(self.addressArr)
                                            
                                            print("addressJSON : \(self.addressJSON)")
                                        }
                                    }
                                }
                            }
                        }
                        
                        self.tbl.reloadData()
                    }
                case .failure(let error):
                    print(error)
                }
        }
        
    }
    
    //MARK: - Table View
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if addressArr.count == 0{
            tbl.isHidden = true
        }else{
            tbl.isHidden = false
        }
        
        return addressArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        
        if let lbl = cell?.viewWithTag(1) as? UILabel{
            
            if let arr = addressArr[indexPath.row] as? NSArray{
                
                for i in arr{
                    
                    if let dict = i as? NSDictionary{
                        
                        if let typs = dict["types"] as? NSArray{
                            if typs.contains("locality"){
                                lbl.text = "\(dict["long_name"]!)"
                            }
                        }
                    }
                }
            }
         }
        
        if let lbl2 = cell?.viewWithTag(2) as? UILabel{
            
            if let arr = addressArr[indexPath.row] as? NSArray{
                
                for i in arr{
                    
                    if let dict = i as? NSDictionary{
                        
                        if let typs = dict["types"] as? NSArray{
                            if typs.contains("administrative_area_level_1"){
                                lbl2.text = "\(dict["long_name"]!)"
                            }
                            
                            if typs.contains("country"){
                                lbl2.text = "\(lbl2.text!), \(dict["long_name"]!)"
                            }
                        }
                    }
                }
            }
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var str = String()
        
        if let arr = addressArr[indexPath.row] as? NSArray{
            
            for i in arr{
                
                if let dict = i as? NSDictionary{
                    
                    if let typs = dict["types"] as? NSArray{
                        
                        if typs.contains("locality"){
                            
                            if str == ""{
                                
                                str = "\(dict["long_name"]!)"
                            }else{
                                
                                str = "\(dict["long_name"]!), \(str)"
                            }
                        }
                                                  
                        if typs.contains("administrative_area_level_1"){
                                                                                                         
                            if str == ""{
                                                     
                                str = "\(dict["long_name"]!)"
                            }else{
                            
                                str = "\(str), \(dict["long_name"]!)"
                            }
                        }
                        
                        if typs.contains("country"){
                                                    
                            if str == ""{
                                                        
                                str = "\(dict["long_name"]!)"
                                
                            }else{
                                                        
                                str = "\(str), \(dict["long_name"]!)"
                                
                            }
                        }
                    }
                    
                    print("Str : \(str)")
                 
                }
            }
                        
            searchField.text = str
            tbl.isHidden = true
            backClicked(self)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 95
    }
    
    
    @IBAction func backClicked(_ sender: Any) {
        
        if selectedField == "living"{

            appDel.selectedPlaceName1 = searchField.text!
        }
        else if selectedField == "base"{

            appDel.selectedBaseName = searchField.text!
        }
        else if selectedField == "eventCreation"{

            appDel.selectedEventVenue = searchField.text!
        }

        else{

            appDel.selectedPlaceName2 = searchField.text!
        }
        
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
      // self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
