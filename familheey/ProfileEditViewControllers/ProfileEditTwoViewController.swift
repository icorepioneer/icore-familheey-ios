//
//  ProfileEditTwoViewController.swift
//  familheey
//
//  Created by familheey on 06/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import GooglePlaces
import CoreLocation

class ProfileEditTwoViewController: BaseClassViewController,DatePickerPopupDelegate,CLLocationManagerDelegate {
    @IBOutlet weak var lblDob_Top: NSLayoutConstraint!
    @IBOutlet weak var txtDob: UITextField!
    @IBOutlet weak var txtLivingin: UITextField!
    @IBOutlet weak var txtOrigin: UITextField!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var livingInButt            : UIButton!
    @IBOutlet weak var originButt              : UIButton!
    @IBOutlet weak var backgroundVw            : UIView!
    var checkWhichButt                         = String()
    var dob                                    = String()
    let locationManager = CLLocationManager()
    var currentLocation = ""
    @IBOutlet weak var dobButt                 : UIButton!
    
    @IBOutlet weak var nameLbl                 : UILabel!
    
    
    override func onScreenLoad() {
        txtDob.delegate = self
        txtOrigin.delegate = self
        txtLivingin.delegate = self
        btnNext.backgroundColor          = #colorLiteral(red: 0.4941176471, green: 0.3411764706, blue: 0.7607843137, alpha: 1)
        if self.view.frame.height < 600{
            lblDob_Top.constant = 55
        }
//        locationManager.requestLocation()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if checkWhichButt == "living"{
                   
            livingInButt.setTitle(appDel.selectedPlaceName1, for: .normal)
            originButt.setTitle(appDel.selectedPlaceName2, for: .normal)
//appDel.selectedPlaceName2 = appDel.selectedPlaceName1
            
               }else{
            
            originButt.setTitle(appDel.selectedPlaceName2, for: .normal)
        }
    }
    
    override func viewWillLayoutSubviews() {
        
        backgroundVw.roundCorners(corners: [.topLeft, .topRight], radius: 25)
        backgroundVw.clipsToBounds = true
    }

    //MARK:- Custom Methods
    func loadPickerView(){
        let pickerPop = DatePickerPopup.getInstance
        pickerPop.delegate = self
        
        //note :- also need to change date min and max value in profile change DOB screen
        
        let cmaxal: Calendar = Calendar.current
        pickerPop.setMaximumDate(date: cmaxal.date(byAdding: .year, value: 0, to: Date())!)
        
        let cal: Calendar = Calendar.current
        pickerPop.setMinimumDate(date: cal.date(byAdding: .year, value: -150, to: Date())!)
        
        pickerPop.showInView(self.view)
    }
    
    //MARK:- DatePicker View Delegate
    func datePickerPopupDidSelectDate(_ dateString: String!) {
      
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let result = formatter.date(from: dateString)
        
        let formatter2 = DateFormatter()
        formatter2.dateStyle = .long
        formatter2.dateFormat = "MMM dd yyyy"
        if result != nil{
            let val = formatter2.string(from: result!)
            dobButt.setTitle(val, for: .normal)
            dob = val
        }
        
        if dob.count > 0 && appDel.selectedPlaceName1.count > 0 && appDel.selectedPlaceName2.count > 0{
            btnNext.backgroundColor          = #colorLiteral(red: 0.4941176471, green: 0.3411764706, blue: 0.7607843137, alpha: 1)
        }
    }
    
    func datePickerPopupDidCancel() {
        
    }
    
    //MARK:- Button Actions
    
    @IBAction func livingInClicked(_ sender: Any) {
        
        checkWhichButt = "living"
        
        guard #available(iOS 13.0, *) else {
            // Code for earlier iOS versions
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let SearchPlacesViewController = storyboard.instantiateViewController(withIdentifier: "SearchPlacesViewController") as! SearchPlacesViewController
            SearchPlacesViewController.selectedField = checkWhichButt
            self.present(SearchPlacesViewController, animated: true, completion: nil)
            
            return
        }
        
        let acController = GMSAutocompleteViewController()
        
        acController.delegate = self
        present(acController, animated: true, completion: nil)
        
        /* if UIDevice.current.systemVersion.contains("13"){
         
         let acController = GMSAutocompleteViewController()
         acController.delegate = self
         present(acController, animated: true, completion: nil)
         }else{
         
         let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
         let SearchPlacesViewController = storyboard.instantiateViewController(withIdentifier: "SearchPlacesViewController") as! SearchPlacesViewController
         SearchPlacesViewController.selectedField = checkWhichButt
         self.present(SearchPlacesViewController, animated: true, completion: nil)
         
         
         }*/
        
    }
    
    @IBAction func originClicked(_ sender: Any) {
        
        checkWhichButt = "origin"
        guard #available(iOS 13.0, *) else {
            // Code for earlier iOS versions
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let SearchPlacesViewController = storyboard.instantiateViewController(withIdentifier: "SearchPlacesViewController") as! SearchPlacesViewController
            
            SearchPlacesViewController.selectedField = checkWhichButt
            
            self.present(SearchPlacesViewController, animated: true, completion: nil)
            
            return
        }
        
        let acController = GMSAutocompleteViewController()
        
        acController.delegate = self
        present(acController, animated: true, completion: nil)
        
        /*if UIDevice.current.systemVersion.contains("13"){
         
         let acController = GMSAutocompleteViewController()
         
         acController.delegate = self
         present(acController, animated: true, completion: nil)
         }else{
         
         let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
         let SearchPlacesViewController = storyboard.instantiateViewController(withIdentifier: "SearchPlacesViewController") as! SearchPlacesViewController
         
         SearchPlacesViewController.selectedField = checkWhichButt
         
         self.present(SearchPlacesViewController, animated: true, completion: nil)
         
         }*/
        
    }
    
    
    @IBAction func onClickDob(_ sender: Any) {
        loadPickerView()
    }
    @IBAction func onClickNext(_ sender: Any) {
        
//        let loginView = self.storyboard?.instantiateViewController(withIdentifier: "profileEditThree") as! ProfileEditThreeViewController
//        //  loginView.regType = regType
//        self.navigationController?.pushViewController(loginView, animated: true)
//
//        return
        
        if dob.count > 0{
            setUserDefaultValues.setUserDefaultValue(userDefaultValue: dob, userDefaultKey: "userDob")
        }
        else{
            setUserDefaultValues.setUserDefaultValue(userDefaultValue: "", userDefaultKey: "userDob")
        }
        if appDel.selectedPlaceName1.count > 0{
            setUserDefaultValues.setUserDefaultValue(userDefaultValue: appDel.selectedPlaceName1, userDefaultKey: "living")
        }
        else{
            if self.currentLocation.isEmpty{
                let currentLocale = NSLocale.current as NSLocale
                let countryCode = currentLocale.object(forKey: .countryCode) as! String//get the set country name, code of your iphone
                let countryName = currentLocale.displayName(forKey: NSLocale.Key.countryCode, value: countryCode)
                self.currentLocation = countryName!
                setUserDefaultValues.setUserDefaultValue(userDefaultValue: currentLocation, userDefaultKey: "living")
            }
            else{
                setUserDefaultValues.setUserDefaultValue(userDefaultValue: self.currentLocation, userDefaultKey: "living")
            }
        }
        if  appDel.selectedPlaceName2.count > 0 {
            setUserDefaultValues.setUserDefaultValue(userDefaultValue: appDel.selectedPlaceName2, userDefaultKey: "origin")
        }
        else{
            if self.currentLocation.isEmpty{
                let currentLocale = NSLocale.current as NSLocale
                let countryCode = currentLocale.object(forKey: .countryCode) as! String//get the set country name, code of your iphone
                let countryName = currentLocale.displayName(forKey: NSLocale.Key.countryCode, value: countryCode)
                self.currentLocation = countryName!
                setUserDefaultValues.setUserDefaultValue(userDefaultValue: currentLocation, userDefaultKey: "origin")
            }
            else{
                setUserDefaultValues.setUserDefaultValue(userDefaultValue: self.currentLocation, userDefaultKey: "origin")
            }
        }
        
        let loginView = self.storyboard?.instantiateViewController(withIdentifier: "profileEditThree") as! ProfileEditThreeViewController
        //  loginView.regType = regType
        self.navigationController?.pushViewController(loginView, animated: true)
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        
        appDel.selectedPlaceName1 = ""
        appDel.selectedPlaceName2 = ""
        
        self.navigationController?.popViewController(animated: true)
    }
    
      //MARK: - location delegate methods
        func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    //        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
    //        print("locations = \(locValue.latitude) \(locValue.longitude)")
            
            
            guard let location: CLLocation = manager.location else { return }
            fetchCityAndCountry(from: location) { city, country, error in
                guard let city = city, let country = country, error == nil else { return }
                print(city + ", " + country)
                self.currentLocation = "\(city) , \(country)"
                self.locationManager.stopUpdatingLocation()
//               self.getUserUpdateDetails()

            }
        }
        func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
            print("Error \(error)")
        }
        
        func fetchCityAndCountry(from location: CLLocation, completion: @escaping (_ city: String?, _ country:  String?, _ error: Error?) -> ()) {
            CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
                completion(placemarks?.first?.locality,
                           placemarks?.first?.country,
                           error)

            }
        }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ProfileEditTwoViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        let nextResponder = textField.superview?.viewWithTag(nextTag)
        if nextResponder != nil{
          
        }
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
 
    }
}
extension ProfileEditTwoViewController: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        // Get the place name from 'GMSAutocompleteViewController'
        // Then display the name in textField
        
//        txtVenue.text = "\(place.formattedAddress!)"

        if checkWhichButt == "living"{
            
            livingInButt.setTitle("\(place.formattedAddress!)", for: .normal)
            appDel.selectedPlaceName1 = "\(place.formattedAddress!)"
            originButt.setTitle("\(place.formattedAddress!)", for: .normal)
            appDel.selectedPlaceName2 = appDel.selectedPlaceName1
        }else{
            originButt.setTitle("\(place.formattedAddress!)", for: .normal)
            appDel.selectedPlaceName2 = "\(place.formattedAddress!)"
        }
        print(place.coordinate.latitude)
        print(place.coordinate.longitude)
        
        if dob.count > 0 && appDel.selectedPlaceName1.count > 0 && appDel.selectedPlaceName2.count > 0{
            btnNext.backgroundColor          = #colorLiteral(red: 0.4941176471, green: 0.3411764706, blue: 0.7607843137, alpha: 1)
        }
        
        dismiss(animated: true, completion: nil)
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // Handle the error
        print("Error: ", error.localizedDescription)
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        // Dismiss when the user canceled the action
        dismiss(animated: true, completion: nil)
    }
}
