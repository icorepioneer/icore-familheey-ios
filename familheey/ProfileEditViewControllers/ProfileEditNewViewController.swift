//
//  ProfileEditNewViewController.swift
//  familheey
//
//  Created by familheey on 30/04/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit
import Alamofire
import Moya
import SwiftyJSON
import CropViewController
import CoreLocation
import GooglePlaces


class ProfileEditNewViewController: UIViewController {
    
    @IBOutlet weak var imgPropic: UIImageView!
    @IBOutlet weak var editVew: UIView!
    @IBOutlet weak var lblHead1: UILabel!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var lblHead2: UILabel!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtLivingIn: UITextField!
    @IBOutlet weak var txtOrigin: UITextField!
    @IBOutlet weak var btnContinue: UIButton!
    
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    var imagePicker = UIImagePickerController()
    let locationManager = CLLocationManager()

    var propic:UIImage = UIImage()
    var checkWhichButt = String()
    var currentLocation = ""
    var currentOrigin = ""
    var selectedLat : Double!
    var selectedLong :Double!
    var originLat : Double!
    var originLong : Double!


    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        if CLLocationManager.locationServicesEnabled() {
            
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access location")
               // self.callCurrentLocation()
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access location")
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
                
            }
        }
        else{
          //  self.callCurrentLocation()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        if checkWhichButt == "living"{
            txtLivingIn.text = appDel.selectedPlaceName1
            txtOrigin.text = appDel.selectedPlaceName1
        }
        else{
            txtOrigin.text = appDel.selectedPlaceName2
        }
    }
    
    //MARK:- Custom Methods
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else{
            // self.displayAlert(alertStr: "You don't have camera", title: "")
        }
    }
    
    func openGallary(){
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    func selectedImage(img:UIImage){
        let cropView = CropViewController(image: img)
        cropView.delegate = self
        self.navigationController?.pushViewController(cropView, animated: true)
    }
    
    func updateUserProfile(){
        var imageData = NSData()
        if propic.size.width != 0{
            imageData = propic.jpegData(compressionQuality: 0.75)! as NSData
        }
        else{
            propic = UIImage(named: "Male Colored")!
            imageData = propic.jpegData(compressionQuality: 0.75)! as NSData
        }
        var param = [String:String]()
        if self.selectedLong != nil && self.selectedLat != nil && self.originLong != nil && self.originLat != nil{
            param = [
                "id":UserDefaults.standard.value(forKeyPath: "userId") as! String,
                "full_name":txtName.text!,
                "email":txtEmail.text!,
                "location":self.currentLocation,
                "origin":self.currentOrigin,
                "is_active":"true",
                "lat":"\(String(describing: self.selectedLat!))",
                "long":"\(String(describing: self.selectedLong!))",
                "origin_lat":"\(String(describing: self.originLat!))",
                "origin_long":"\(String(describing: self.originLong!))"
            ]
        }
        else{
            param = [
                "id":UserDefaults.standard.value(forKeyPath: "userId") as! String,
                "full_name":txtName.text!,
                "email":txtEmail.text!,
                "location":self.currentLocation,
                "origin":self.currentOrigin,
                "is_active":"true"
            ]
        }
        print(param)
        if !Connectivity.isConnectedToInternet(){
            self.displayAlertChoice(alertStr: "No Internet! Please Check your connectivity", title: "") { (result) in
                ActivityIndicatorView.hiding()
                return
            }
        }
        
        APIServiceManager.callServer.updateUserProfile(url: EndPoint.profileEdit, param: param,  pic: imageData, cntntType: "Image", success: { (responseMdl) in
            ActivityIndicatorView.hiding()
            
            guard let loginMdl = responseMdl as? userModel else{
                return
            }
            
            if loginMdl.status == 200{
                UserDefaults.standard.set(loginMdl.photo, forKey: "userProfile")
                print(responseMdl!)
                setUserDefaultValues.setUserDefaultValue(userDefaultValue: "1", userDefaultKey: "isFirst_Time")
                let mainStoryBoard = UIStoryboard(name: "Subscription", bundle: nil)
                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "WalkthorughViewController") as! WalkthorughViewController


                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = redViewController
            }
            
        }) { (error) in
            
        }
    }
    
    //MARK:- Button Actions
    @IBAction func onClickContinue(_ sender: Any) {
        if !txtName.text!.isValidUserName{
            self.displayAlert(alertStr: "Please enter a valid name, First letter of the name must be an alphabet also The name must contain at least two alphabets", title: "")
            return
        }
        else if txtEmail.text!.isEmpty{
            self.displayAlert(alertStr: "Email can't be empty", title: "")
            return
        }
        else{
            if !Helpers.validateEmail(enteredEmail: txtEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines)){
                self.displayAlert(alertStr: "Please Enter a valid email", title: "")
                return
            }
            let tempStr = txtEmail.text!.prefix(1)
            if !String(tempStr).isAlphanumeric{
                self.displayAlert(alertStr: "Please start email with a alphanumeric characters", title: "")
                return
            }
            if txtLivingIn.text!.isEmpty{
                if self.currentLocation.isEmpty{
                    let currentLocale = NSLocale.current as NSLocale
                    let countryCode = currentLocale.object(forKey: .countryCode) as! String//get the set country name, code of your iphone
                    let countryName = currentLocale.displayName(forKey: NSLocale.Key.countryCode, value: countryCode)
                    self.currentLocation = countryName!
                    self.currentOrigin = countryName!
                }
                else{
                }
            }
            else{
                self.currentLocation = txtLivingIn.text!
                self.currentOrigin = txtLivingIn.text!
            }
            
            if txtOrigin.text!.isEmpty{
                if self.currentLocation.isEmpty{
                    let currentLocale = NSLocale.current as NSLocale
                    let countryCode = currentLocale.object(forKey: .countryCode) as! String//get the set country name, code of your iphone
                    let countryName = currentLocale.displayName(forKey: NSLocale.Key.countryCode, value: countryCode)
                    self.currentOrigin = countryName!
                }
                else{
                }
            }
            else{
                self.currentOrigin = txtOrigin.text!
            }
            self.updateUserProfile()
        }
    }
    
    @IBAction func onClickUpload(_ sender: Any) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func onClickLivingIn(_ sender: Any) {
        
        checkWhichButt = "living"
        
        guard #available(iOS 13.0, *) else {
            // Code for earlier iOS versions
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let SearchPlacesViewController = storyboard.instantiateViewController(withIdentifier: "SearchPlacesViewController") as! SearchPlacesViewController
            SearchPlacesViewController.selectedField = checkWhichButt
            self.present(SearchPlacesViewController, animated: true, completion: nil)
            
            return
        }
        
        let acController = GMSAutocompleteViewController()
        
        acController.delegate = self
        present(acController, animated: true, completion: nil)
        
        /*if UIDevice.current.systemVersion.contains("13"){
            let acController = GMSAutocompleteViewController()
            acController.delegate = self
            present(acController, animated: true, completion: nil)
        }else{
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let SearchPlacesViewController = storyboard.instantiateViewController(withIdentifier: "SearchPlacesViewController") as! SearchPlacesViewController
            SearchPlacesViewController.selectedField = checkWhichButt
            self.present(SearchPlacesViewController, animated: true, completion: nil)
        }*/
    }
    
    @IBAction func onClickOrigin(_ sender: Any) {
        checkWhichButt = "origin"
        
        guard #available(iOS 13.0, *) else {
            // Code for earlier iOS versions
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let SearchPlacesViewController = storyboard.instantiateViewController(withIdentifier: "SearchPlacesViewController") as! SearchPlacesViewController
            SearchPlacesViewController.selectedField = checkWhichButt
            self.present(SearchPlacesViewController, animated: true, completion: nil)
            
            return
        }
        
        let acController = GMSAutocompleteViewController()
        
        acController.delegate = self
        present(acController, animated: true, completion: nil)
        
       /* if UIDevice.current.systemVersion.contains("13"){
            let acController = GMSAutocompleteViewController()
            acController.delegate = self
            present(acController, animated: true, completion: nil)
        }else{
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let SearchPlacesViewController = storyboard.instantiateViewController(withIdentifier: "SearchPlacesViewController") as! SearchPlacesViewController
            SearchPlacesViewController.selectedField = checkWhichButt
            self.present(SearchPlacesViewController, animated: true, completion: nil)
        }*/
    }
}
extension  ProfileEditNewViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate,CropViewControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker .dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let choosenImage = info[.originalImage] as! UIImage
        selectedImage(img: choosenImage)
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    //MARK:- CropView SDK Delegate
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        // 'image' is the newly cropped version of the original image
        imgPropic.image = image
        propic = image
        self.navigationController?.popViewController(animated: true)
    }
}
extension ProfileEditNewViewController: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location: CLLocation = manager.location else { return }
        fetchCityAndCountry(from: location) { city, country, error in
            guard let city = city, let country = country, error == nil else { return }
            print(city + ", " + country)
            self.currentLocation = "\(city) , \(country)"
            self.currentOrigin = "\(city) , \(country)"
            
            self.selectedLong = location.coordinate.longitude
            self.selectedLat = location.coordinate.latitude
            self.originLong = self.selectedLong
            self.originLat = self.selectedLat
            
            self.locationManager.stopUpdatingLocation()
            
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }
    
    func fetchCityAndCountry(from location: CLLocation, completion: @escaping (_ city: String?, _ country:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            completion(placemarks?.first?.locality,
                       placemarks?.first?.country,
                       error)
            
        }
    }
}
extension ProfileEditNewViewController: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        // Get the place name from 'GMSAutocompleteViewController'
        // Then display the name in textField
        
        if checkWhichButt == "living"{
            txtLivingIn.text = place.formattedAddress ?? ""
            txtOrigin.text = place.formattedAddress ?? ""
            self.selectedLong = place.coordinate.longitude
            self.selectedLat = place.coordinate.latitude
            self.originLong = self.selectedLong
            self.originLat = self.selectedLat
            
            appDel.selectedPlaceName1 = "\(place.formattedAddress!)"
            appDel.selectedPlaceName2 = appDel.selectedPlaceName1
        }else{
            txtOrigin.text = place.formattedAddress ?? ""
            self.originLong = place.coordinate.longitude
            self.originLat = place.coordinate.latitude
            appDel.selectedPlaceName2 = "\(place.formattedAddress!)"
        }
        print(place.coordinate.latitude)
        print(place.coordinate.longitude)
        
        dismiss(animated: true, completion: nil)
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // Handle the error
        print("Error: ", error.localizedDescription)
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        // Dismiss when the user canceled the action
        dismiss(animated: true, completion: nil)
    }
}

