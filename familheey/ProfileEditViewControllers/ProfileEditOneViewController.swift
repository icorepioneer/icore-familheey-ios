//
//  ProfileEditOneViewController.swift
//  familheey
//
//  Created by familheey on 06/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class ProfileEditOneViewController: BaseClassViewController, UITextFieldDelegate{
    @IBOutlet weak var lblName_Top            : NSLayoutConstraint!
    @IBOutlet weak var btnNext_Bottom         : NSLayoutConstraint!
    @IBOutlet weak var txtName                : UITextField!
    @IBOutlet weak var txtEmail               : UITextField!
    @IBOutlet weak var btnNext                : UIButton!
    @IBOutlet weak var btnMale                : UIButton!
    @IBOutlet weak var btnFemale              : UIButton!
    
    @IBOutlet weak var backgroundVw           : UIView!
    
    @IBOutlet weak var btnNotSpecified: UIButton!
    
    var name:String = String()
    var email:String = String()
    var profilePic:String = String()
    var type:String = String()
    var gender                    = ""
    
    @IBOutlet weak var maleCheckmark               : UIImageView!
    @IBOutlet weak var femaleCheckmark             : UIImageView!
    @IBOutlet weak var notSayCheckmark             : UIImageView!
    
    
    override func onScreenLoad() {
        btnFemale.layer.cornerRadius          = 10
        btnMale.layer.cornerRadius            = 10
        btnNotSpecified.layer.cornerRadius    = 10
        
        btnMale.layer.borderColor             = UIColor.lightGray.cgColor
        btnFemale.layer.borderColor           = UIColor.lightGray.cgColor
        btnNotSpecified.layer.borderColor     = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
        
        
        if self.view.frame.height < 600{
            lblName_Top.constant = 55
            btnNext_Bottom.constant = 20
        }
        
        if type == "normal"{
        }
        else{
          //  txtName.text = name
            //txtEmail.text = email
        }
        
        btnNext.cornerRadius                = 10
        btnNext.clipsToBounds               = true
        btnNext.backgroundColor             = .lightGray
        btnNext.isEnabled                   = false
        
        txtName.layer.borderWidth           = 1
        txtName.layer.cornerRadius          = 5
        txtName.layer.borderColor           = UIColor.lightGray.cgColor
        txtName.delegate                    = self
        
        txtEmail.layer.borderWidth          = 1
        txtEmail.layer.cornerRadius         = 5
        txtEmail.layer.borderColor          = UIColor.lightGray.cgColor
        txtEmail.delegate                   = self
    
//        let leftView1                       = UILabel(frame: CGRect(x: 5, y: 0, width: 10, height: 26))
//        leftView1.backgroundColor           = .clear
//        txtName.leftView                    = leftView1
//        txtName.leftViewMode                = .always
//        txtName.contentVerticalAlignment    = .center
//
//        let leftView2                       = UILabel(frame: CGRect(x: 5, y: 0, width: 10, height: 26))
//        leftView2.backgroundColor           = .clear
//        txtEmail.leftView                   = leftView2
//        txtEmail.leftViewMode               = .always
//        txtEmail.contentVerticalAlignment   = .center
        
        maleCheckmark.isHidden              = true
        femaleCheckmark.isHidden            = true
        notSayCheckmark.isHidden            = false
        
    }
    
 
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            
            self.checkTxtField()
        })
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func checkTxtField(){
        
        if txtName.text!.trimmingCharacters(in: .whitespacesAndNewlines).count != 0 && txtEmail.text?.count != 0 && txtEmail.validateEmail(enteredEmail: txtEmail.text!){
            
            btnNext.backgroundColor          = #colorLiteral(red: 0.4941176471, green: 0.3411764706, blue: 0.7607843137, alpha: 1)
            btnNext.isEnabled                = true
        }else{
            
            btnNext.backgroundColor          = .lightGray
            btnNext.isEnabled                = false
        }
        
    }
    
    override func viewWillLayoutSubviews() {
        
        backgroundVw.roundCorners(corners: [.topLeft, .topRight], radius: 25)
        backgroundVw.clipsToBounds = true
    }
    
    //MARK:- Button Actions
    @IBAction func onClickNext(_ sender: Any) {

        if !txtName.text!.isValidUserName{
            self.displayAlert(alertStr: "Please enter a valid name, First letter of the name must be an alphabet also The name must contain at least two alphabets", title: "")
            return

        }
        if !Helpers.validateEmail(enteredEmail: txtEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines)){
            self.displayAlert(alertStr: "Please Enter a valid email", title: "")
            return
        }
        let tempStr = txtEmail.text!.prefix(1)
        if !String(tempStr).isAlphanumeric{
            self.displayAlert(alertStr: "Please start email with a alphanumeric characters", title: "")
            return
        }
        
        
        if txtEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty != true && txtName.text!.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty != true{
            setUserDefaultValues.setUserDefaultValue(userDefaultValue: txtEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines), userDefaultKey: "email")
            setUserDefaultValues.setUserDefaultValue(userDefaultValue: gender, userDefaultKey: "gender")
            setUserDefaultValues.setUserDefaultValue(userDefaultValue: txtName.text!.trimmingCharacters(in: .whitespacesAndNewlines), userDefaultKey: "userName")
            
            let loginView = self.storyboard?.instantiateViewController(withIdentifier: "profileEditTwo") as! ProfileEditTwoViewController
            //  loginView.regType = regType
            self.navigationController?.pushViewController(loginView, animated: true)
        }
        else{
            self.displayAlert(alertStr:"All fields are mandatory",title:"")
        }
    }
    
    @IBAction func onClickGender(_ sender: Any) {
        
        if (sender as AnyObject).tag == 1{
            btnMale.setImage(#imageLiteral(resourceName: "maleImgSelected"), for: .normal)
            btnFemale.setImage(#imageLiteral(resourceName: "femaleImg"), for: .normal)
            btnNotSpecified.setImage(#imageLiteral(resourceName: "notSay"), for: .normal)
            
            gender = "male"
            
            btnMale.layer.borderColor             = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            btnFemale.layer.borderColor           = UIColor.lightGray.cgColor
            btnNotSpecified.layer.borderColor     = UIColor.lightGray.cgColor
            
            maleCheckmark.isHidden                = false
            femaleCheckmark.isHidden              = true
            notSayCheckmark.isHidden              = true
        }
        else if (sender as AnyObject).tag == 2{
            
            btnMale.setImage(#imageLiteral(resourceName: "maleImg"), for: .normal)
            btnFemale.setImage(#imageLiteral(resourceName: "femaleImgSelected"), for: .normal)
            btnNotSpecified.setImage(#imageLiteral(resourceName: "notSay"), for: .normal)
            
            gender = "female"
            
            btnMale.layer.borderColor             = UIColor.lightGray.cgColor
            btnFemale.layer.borderColor           = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            btnNotSpecified.layer.borderColor     = UIColor.lightGray.cgColor
            
            maleCheckmark.isHidden                = true
            femaleCheckmark.isHidden              = false
            notSayCheckmark.isHidden              = true
        }else{
            
            btnMale.setImage(#imageLiteral(resourceName: "maleImg"), for: .normal)
            btnFemale.setImage(#imageLiteral(resourceName: "femaleImg"), for: .normal)
            
            btnNotSpecified.setImage(#imageLiteral(resourceName: "notSaySelected"), for: .normal)
            
            gender = ""
            
            btnMale.layer.borderColor             = UIColor.lightGray.cgColor
            btnFemale.layer.borderColor           = UIColor.lightGray.cgColor
            btnNotSpecified.layer.borderColor     = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            
            maleCheckmark.isHidden                = true
            femaleCheckmark.isHidden              = true
            notSayCheckmark.isHidden              = false
        }

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension String{
    var isValidUserName : Bool {
        if self.prefix(1).range(of: "[^a-zA-Z]", options: .regularExpression) != nil{
            return false
        }
        let Regex = "(?=.*[a-zA-Z].*[a-zA-Z])^.*"
        return NSPredicate(format: "SELF MATCHES %@", Regex).evaluate(with: self)
    }
}
