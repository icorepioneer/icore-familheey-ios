//
//  ProfileEditThreeViewController.swift
//  familheey
//
//  Created by familheey on 06/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import CropViewController

class ProfileEditThreeViewController: BaseClassViewController,CropViewControllerDelegate {
    @IBOutlet weak var imgProfilePic: UIImageView!
    @IBOutlet weak var btnProfile: UIButton!
    
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var lblAlmostText: UILabel!
    @IBOutlet weak var lblFooter: UILabel!
    
    var imagePicker = UIImagePickerController()
    var propic:UIImage = UIImage()
    
    @IBOutlet weak var recommendLbl: UILabel!
    
    @IBOutlet weak var backgroundVw            : UIView!
    @IBOutlet weak var nameLbl                 : UILabel!
    
    
    override func onScreenLoad() {
        lblAlmostText.text = "Name" //"Almost there, \(String(describing: UserDefaults.standard.value(forKey: "userName")!))!"
        
       // imgProfilePic.image = propic

//        imgProfilePic.layer.cornerRadius = imgProfilePic.frame.width / 2
//        imgProfilePic.clipsToBounds = true
        
//        btnSkip.underline()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // propic = UIImage(named: "Male Colored")!
       nameLbl.text = "Hi, \(UserDefaults.standard.value(forKey: "userName") as! String)"
    }
    
    override func viewWillLayoutSubviews() {
        
        backgroundVw.roundCorners(corners: [.topLeft, .topRight], radius: 25)
        backgroundVw.clipsToBounds = true
    }

    //MARK:- Custom Methods
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else{
            // self.displayAlert(alertStr: "You don't have camera", title: "")
        }
    }
    
    func openGallary(){
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    func selectedImage(img:UIImage){
        let cropView = CropViewController(image: img)
       // let cropView = CropViewController(croppingStyle: .circular, image: img)
        cropView.delegate = self
        self.navigationController?.pushViewController(cropView, animated: true)
    }
    
    //MARK:- Web API for profile Edit
    func profileEdit(){
        var imageData = NSData()

        if propic.size.width != 0{
            imageData = propic.jpegData(compressionQuality: 0.75)! as NSData
        }
        else{
            propic = UIImage(named: "Male Colored")!
            imageData = propic.jpegData(compressionQuality: 0.75)! as NSData
        }
        var param = [String:String]()
        let dob = UserDefaults.standard.value(forKeyPath: "userDob") as! String
        if dob.isEmpty{
            param = [
                "id":UserDefaults.standard.value(forKeyPath: "userId") as! String,
                "full_name":UserDefaults.standard.value(forKeyPath: "userName") as! String,
                "email":UserDefaults.standard.value(forKeyPath: "email") as! String,
                "gender":UserDefaults.standard.value(forKeyPath: "gender") as! String,
                "location":UserDefaults.standard.value(forKeyPath: "living") as! String,
                "origin":UserDefaults.standard.value(forKeyPath: "origin") as! String,
                "is_active":"true"
            ]
        }
        else{
            param = [
                "id":UserDefaults.standard.value(forKeyPath: "userId") as! String,
                "full_name":UserDefaults.standard.value(forKeyPath: "userName") as! String,
                "email":UserDefaults.standard.value(forKeyPath: "email") as! String,
                "gender":UserDefaults.standard.value(forKeyPath: "gender") as! String,
                "location":UserDefaults.standard.value(forKeyPath: "living") as! String,
                "origin":UserDefaults.standard.value(forKeyPath: "origin") as! String,
                "dob":dob,
                "is_active":"true"
            ]
        }
         
        
        APIServiceManager.callServer.updateUserProfile(url: EndPoint.profileEdit, param: param,  pic: imageData, cntntType: "Image", success: { (responseMdl) in
            ActivityIndicatorView.hiding()
            
            guard let loginMdl = responseMdl as? userModel else{
                return
            }
            
            if loginMdl.status == 200{
                UserDefaults.standard.set(loginMdl.photo, forKey: "userProfile")
                print(responseMdl!)
                setUserDefaultValues.setUserDefaultValue(userDefaultValue: "1", userDefaultKey: "isFirst_Time")
                let mainStoryBoard = UIStoryboard(name: "Subscription", bundle: nil)
                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "WalkthorughViewController") as! WalkthorughViewController


                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = redViewController
            }
            
        }) { (error) in
            
        }
    }
    
    //MARK:- CropView SDK Delegate
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        // 'image' is the newly cropped version of the original image
        imgProfilePic.image = image
        propic = image
        self.btnNext.setTitle("Next", for: .normal)
        self.navigationController?.popViewController(animated: true)
        
        lblFooter.text = "All set! Press next to complete"
        btnSkip.isHidden = true
    }
    
    //MARK:- Button Action
    @IBAction func onClickNext(_ sender: UIButton) {
        if sender.titleLabel?.text?.lowercased() == "upload"{
            onClickProfilePic(self)
        }
        else{
             profileEdit()
        }
//        if self.imgProfilePic.image == nil{
//            onClickProfilePic(self)
//        }
//        else{
//             profileEdit()
//        }
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickSkip(_ sender: Any) {
        
       // self.performSegue(withIdentifier: "1", sender: self)
        
        profileEdit()
    }
    
    @IBAction func onClickProfilePic(_ sender: Any) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        
        self.present(alert, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension  ProfileEditThreeViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker .dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let choosenImage = info[.originalImage] as! UIImage
//        let asset = info[UIImagePickerController.InfoKey.phAsset]
//        let imageUrl = info[UIImagePickerController.InfoKey.imageURL] as? URL
//        let imageName         = imageUrl?.lastPathComponent
//        let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
//        let photoURL          = NSURL(fileURLWithPath: documentDirectory)
//        let localPath         = photoURL.appendingPathComponent(imageName!)
//        let path:String = localPath!.path
 //       print(localPath!)
//        imgView.image = choosenImage
        selectedImage(img: choosenImage)
        
        //self.recommendLbl.text = "Cool! good pic. Please press next to complete"
       // self.btnNext.setTitle("Next", for: .normal)
    
        picker.dismiss(animated: true, completion: nil)
        
    }
}
