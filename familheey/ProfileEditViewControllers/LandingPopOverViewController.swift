//
//  LandingPopOverViewController.swift
//  familheey
//
//  Created by familheey on 04/05/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON
import Kingfisher

class LandingPopOverTableViewCell: UITableViewCell{
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblFaName: UILabel!
    @IBOutlet weak var lblMembers: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var btnJoin: UIButton!
    
}

protocol suggestedViewDismissDelegate: class {
    func getButtonClicked(index:Int)
}

class LandingPopOverViewController: UIViewController {
    @IBOutlet weak var tblListView: UITableView!
    
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    weak var delegate: suggestedViewDismissDelegate?
    var famArray = [JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblListView.delegate = self
        tblListView.dataSource = self
        tblListView.reloadData()
        
        getSuggestedFamily()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    //MARK:- Custom Methods
    func getSuggestedFamily(){
        ActivityIndicatorView.show("Please wait....")
        let param = ["user_id" : UserDefaults.standard.value(forKey: "userId") as! String] as [String : Any]
        
        networkProvider.request(.get_suggested_group_newUser(parameter: param), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    ActivityIndicatorView.hiding()
                    let jsonData = JSON(response.data)
                    
                    if response.statusCode == 200{
                        self.famArray = jsonData["data"].arrayValue
                        print(self.famArray)
                        self.tblListView.reloadData()
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getSuggestedFamily()
                        }
                    }
                        
                    else{
                        self.tblListView.isHidden = true
                    }
                } catch let err {
                    
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    

   //MARK:- Button Actions
    @IBAction func onClickCreateFamily(_ sender: Any) {
        self.delegate?.getButtonClicked(index: 1)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onClickSkip(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onClickViewall(_ sender: Any) {
        self.delegate?.getButtonClicked(index: 0)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onClickJoinFamily(_ sender: UIButton) {
        let family = famArray[sender.tag]
        
        ActivityIndicatorView.show("Please wait!")
        let param = ["user_id" : UserDefaults.standard.value(forKey: "userId") as! String, "group_id":family["id"].stringValue] as [String : Any]
        networkProvider.request(.joinFamilySuggested(parameter: param), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    ActivityIndicatorView.hiding()
                    let jsonData = JSON(response.data)
                    
                    if response.statusCode == 200{
                        print(jsonData)
                        self.getSuggestedFamily()
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            let btn = UIButton()
                            btn.tag = sender.tag
                            self.onClickJoinFamily(btn)
                        }
                    }

                    else{
                    }
                } catch let err {
                    
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
        
       /* APIServiceManager.callServer.joinFamilyRequest(url: EndPoint.joinFamily, userId:  UserDefaults.standard.value(forKey: "userId") as! String, groupId: family["id"].stringValue, success: { (response) in
            ActivityIndicatorView.hiding()
           
            
            guard let joinResultMdl = response as? JoinResult else{
                return
            }
            
            print(joinResultMdl)
            self.displayAlert(alertStr: "Request successfull", title: "")
            self.getSuggestedFamily()
            
            
        }) { (error) in
            ActivityIndicatorView.hiding()
            
        }*/
    }
}

extension LandingPopOverViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return famArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellid = "LandingPopOverTableViewCell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! LandingPopOverTableViewCell
        
        cell.btnJoin.tag = indexPath.row
        
        let family = famArray[indexPath.row]
        
        cell.lblFaName.text = family["group_name"].string ?? "Unknown"
        cell.lblMembers.text = "\(family["membercount"].stringValue) members"
        cell.lblLocation.text = family["base_region"].string ?? "Unknown"
        
        if !family["logo"].stringValue.isEmpty{
            let temp = "\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+family["logo"].stringValue.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let imgurl = URL(string: temp)
            let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=80&height=80&url="+imgurl!.relativeString
            let url = URL(string: newUrlStr)
            cell.imgLogo.kf.indicatorType = .activity

            cell.imgLogo.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "Family Logo"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        else{
            cell.imgLogo.image = #imageLiteral(resourceName: "Family Logo")
        }
        
        if family["is_joined"].stringValue == "1" {
            cell.btnJoin.setTitle("Member", for: .normal)
            cell.btnJoin.isEnabled = false
        }
        else{
            if family["status"].stringValue.lowercased() == "pending"{
                cell.btnJoin.setTitle("Pending", for: .normal)
                cell.btnJoin.isEnabled = false
            }
            else if family["status"].stringValue.lowercased() == "rejected"{
                cell.btnJoin.setTitle("Join Family", for: .normal)
                cell.btnJoin.isEnabled = true
            }
            else {
                cell.btnJoin.setTitle("Join Family", for: .normal)
                cell.btnJoin.isEnabled = true
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
