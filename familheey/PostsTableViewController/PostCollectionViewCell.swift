//
//  PostCollectionViewCell.swift
//  familheey
//
//  Created by Giri on 21/11/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import AVKit
import ActiveLabel

//Image

class PostCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageViewPost: UIImageView!
    @IBOutlet weak var playIcon: UIImageView!
    
     override func prepareForReuse() {
         super.prepareForReuse()
//         imageViewPost.kf.cancelDownloadTask()
         imageViewPost.image = nil
     }
    
}
//Video

class PostCollectionViewCellVideo : UICollectionViewCell{
    
    @IBOutlet weak var videoLayer: UIView!
    @IBOutlet weak var imageViewDoc: UIImageView!
    @IBOutlet weak var labaleDocName: UILabel!
    @IBOutlet weak var buttonView: UIButton!

    var player: AVPlayer!
    var avpController = AVPlayerViewController()
    
    
    
}

//Link
class PostCollectionViewCellLink : UICollectionViewCell{
    @IBOutlet weak var imgPreview: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    
}
class saythanksMentionedNameCell : UICollectionViewCell{
    @IBOutlet weak var lblMentionedNames: ActiveLabel!

    
}
