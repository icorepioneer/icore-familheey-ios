//
//  PostThanksTableViewCell.swift
//  familheey
//
//  Created by familheey on 03/06/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit
import AVKit
import SwiftyJSON
import SafariServices
import ActiveLabel
import Kingfisher

 protocol PostThanksableViewCellDelegate: class {
    func gotoThanksPreview(data:[JSON],index:Int)
    func gotoThanksSinglePreview(data:String)
    func selectMentionedName(data:[JSON],index:Int)
}

class PostThanksTableViewCell: UITableViewCell {
    var arrayOfMentionedName = [JSON]()
     @IBOutlet weak var btnFamilyClick: UIButton!
     
    @IBOutlet weak var heightOfMentionedView: NSLayoutConstraint!
    //User Details
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var imageViewProfilePic: UIImageView!
     @IBOutlet weak var labelUserName: UILabel!
     @IBOutlet weak var labelPostedIn: UILabel!
     @IBOutlet weak var labelDate: UILabel!
     @IBOutlet weak var lblPostedHead: UILabel!
     
     //Posted Details
     @IBOutlet weak var pageController: UIPageControl!
     @IBOutlet weak var viewPostBg: UIView!
     @IBOutlet weak var viewPageControllBG: UIView!
     @IBOutlet weak var rightMenuVew: UIView!
     
     @IBOutlet weak var labelPost: ActiveLabel!
     @IBOutlet weak var lableNumberOfConversations: UILabel!
     @IBOutlet weak var labelNumberOfViews: UILabel!
     @IBOutlet weak var pageControl: UIPageControl!
     @IBOutlet weak var lblNumberOfShares: UILabel!
     
     @IBOutlet weak var collectionViewPost: UICollectionView!
    @IBOutlet weak var collVwOfMentionedNames: UICollectionView!
    
     @IBOutlet weak var buttonComments: UIButton!
     @IBOutlet weak var lblNewConvrstn: UILabel!
     @IBOutlet weak var readMoreView: UIView!
     @IBOutlet weak var collectionviewBGHeight: NSLayoutConstraint!
     
     var Cellheight = CGFloat.init(0.0)
     
     @IBOutlet weak var shareView: UIView!
     @IBOutlet weak var readmoreButton: UIButton!
     
     @IBOutlet weak var buttonRightMenu: UIButton!
     var attachments = [JSON]()
     @IBOutlet weak var buttonShare: UIButton!
     @IBOutlet weak var commentsButton: UILabel!
     
     @IBOutlet weak var viewsButton: UIButton!
     @IBOutlet weak var sharedUsersButton: UIButton!
     weak var delegate: PostThanksableViewCellDelegate?
     
     @IBOutlet weak var viewComments: UIView!
     @IBOutlet weak var viewShared: UIView!
     @IBOutlet weak var viewViews: UIView!
     var collectionViewLayout = UICollectionViewFlowLayout()
    var collectionViewLayout2 = UICollectionViewFlowLayout()

     var indexOfCellBeforeDragging = 0
     var postType = ""
    
    //Thanks post
    @IBOutlet weak var lblMentionedNames: ActiveLabel!
    @IBOutlet weak var lblItemNames: UILabel!
    @IBOutlet weak var btnOpenRequest: UIButton!
    
    @IBOutlet weak var btnMentionedNames: UIButton!
    //MARK:- Review post extra items
     
     @IBOutlet weak var btnAcceptReview: UIButton!
     @IBOutlet weak var btnRejectReview: UIButton!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    override func awakeFromNib() {
        super.awakeFromNib()
        
        Cellheight = collectionViewPost.bounds.width*(3/4)
        viewPostBg.isHidden = true
        viewPageControllBG.isHidden = true
        collectionViewPost.delegate = self
        collectionViewPost.dataSource = self
        collectionViewLayout.scrollDirection = .horizontal
        collectionViewLayout.minimumLineSpacing = 0
        self.collectionViewPost.setCollectionViewLayout(collectionViewLayout, animated: true)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func updateSliderThankYou(data:[JSON],Ptype:String){
        postType = Ptype
        if data.count == 0 {
            
            self.viewPostBg.isHidden = true
            self.viewPageControllBG.isHidden = true
            
        }
        else{
            
            if data.count != 0{
                self.attachments = data
                let attachment = data[0]
                if let type = attachment["type"].string ,type.contains("image"){
                    if let W = attachment["width"].string,let H = attachment["height"].string{
                        
                        let Cheight = self.getDownscaledSize(W: W, H: H)
                        self.Cellheight = Cheight
                        
                    }
                    else{
                        self.Cellheight = self.collectionViewPost.bounds.width*(3/4)
                    }
                }
                else{
                    self.Cellheight = self.collectionViewPost.bounds.width*(3/4)
                }
                self.collectionViewPost.reloadData()
                self.collectionviewBGHeight.constant = self.Cellheight
                self.setNeedsLayout()
                self.updatePageCntrl(data: data)
                
                if data.count == 1 {
                    self.viewPageControllBG.isHidden = false
                }
                else{
                    self.viewPageControllBG.isHidden = false
                }
                self.viewPostBg.isHidden = false
                
                
            }
            else{
                self.viewPostBg.isHidden = true
                self.viewPageControllBG.isHidden = true
            }
            
        }
    }
    
    func loadMentionedNames(data:[JSON])
    {
        self.arrayOfMentionedName =  data
        collVwOfMentionedNames.delegate = self
        collVwOfMentionedNames.dataSource = self
//                                            self.heightOfMentionedView.constant = CGFloat(25 * 4)
//        collVwOfMentionedNames.backgroundColor = .red
//
                            if arrayOfMentionedName.count <= 4
                            {
                                self.heightOfMentionedView.constant = 30

                            }
                            else
                            {
                                if arrayOfMentionedName.count % 4 != 0
                                {
                                    self.heightOfMentionedView.constant = CGFloat(30 * (Int(arrayOfMentionedName.count/4)+1))
//                                     self.heightOfMentionedView.constant = 200
                                }
                                else
                                {
                                    self.heightOfMentionedView.constant = CGFloat(30 * Int(arrayOfMentionedName.count/4))
                                }

                            }
//        collectionViewLayout2.scrollDirection = .vertical
//               collectionViewLayout2.minimumLineSpacing = 0
//               self.collVwOfMentionedNames.setCollectionViewLayout(collectionViewLayout2, animated: true)
        self.collVwOfMentionedNames.reloadData()
    }
    
    func updateSliderLink(data:[JSON]){
        print(data)
        if data.count == 0 {
            
            self.viewPostBg.isHidden = false
            self.viewPageControllBG.isHidden = true
            self.Cellheight = 105
            self.collectionViewPost.reloadData()
            self.collectionviewBGHeight.constant = self.Cellheight
            self.setNeedsLayout()
        }
        else{
            self.attachments = data
            self.viewPageControllBG.isHidden = true
            
            self.Cellheight = 105
            self.collectionViewPost.reloadData()
            self.collectionviewBGHeight.constant = self.Cellheight
            self.setNeedsLayout()
            self.viewPostBg.isHidden = false
        }
    }
    
    func getDownscaledSize(W:String,H:String) -> CGFloat{
        guard let width = NumberFormatter().number(from: W) else { return 0}
        guard let height = NumberFormatter().number(from: H) else { return 0}
        let ratio = UIScreen.main.bounds.width/CGFloat(truncating: width)
        return CGFloat(truncating: height)*ratio
        
        
    }
    func updatePageCntrl(data:[JSON]){
        
        pageController.currentPage = 0
        pageController.numberOfPages = data.count
        
    }
    func stopPlaying(){
        DispatchQueue.main.async {
            
            if let cell = self.collectionViewPost.visibleCells.first as? PostCollectionViewCellVideo {
                if (cell.player != nil){
                    cell.player.pause()
                }
            }
            
        }
    }
}

extension PostThanksTableViewCell : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 2
        {
           return self.arrayOfMentionedName.count
//            return 10
        }
        else
        {
        return attachments.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == 2
               {
                let mentionedName = arrayOfMentionedName[indexPath.item]
                 let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "saythanksMentionedNameCell", for: indexPath) as! saythanksMentionedNameCell
                cell.lblMentionedNames.text = "@ \(mentionedName["user_name"].stringValue.components(separatedBy: " ")[0])"
                
//                cell.lblMentionedNames.text = "@@@@ anil"

            return cell
        }
        else
        {
        let attachment = attachments[indexPath.item]
        
        if let type = attachment["url"].string, !type.isEmpty{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostCollectionViewCellLink", for: indexPath) as! PostCollectionViewCellLink
            
            cell.lblTitle.text = attachment["url"].stringValue
            
            if let desc = attachment["title"].string, !desc.isEmpty{
                cell.lblDesc.text = desc
            }
            else{
                cell.lblDesc.text = attachment["description"].stringValue
            }
            
            let temp = attachment["image"].stringValue.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            var imgurl = URL(string:temp )
            if type.lowercased().contains("gif"){
                imgurl = URL(string: temp )
            }
            DispatchQueue.main.async {
                cell.imgPreview.kf.indicatorType = .image(imageData: self.appDelegate.gifData)
                cell.imgPreview.kf.setImage(with: imgurl, placeholder:#imageLiteral(resourceName: "url_imgSetDefault"), options: [.processor(
                    ResizingImageProcessor(referenceSize: CGSize(width: cell.imgPreview.frame.width, height: cell.imgPreview.frame.height), mode: .aspectFit)),
                                                                                                                                  .scaleFactor(UIScreen.main.scale),
                                                                                                                                  .transition(.fade(1)),
                                                                                                                                  .cacheOriginalImage
                ])
            }
            
            return cell
        }
        else{
            if let type = attachment["type"].string ,type.contains("image"){
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostCollectionViewCell", for: indexPath) as! PostCollectionViewCell
                cell.playIcon.isHidden = true
                var  temp  = ""
                if postType.lowercased() == "albums"{
                    temp = "\(Helpers.imageURl)"+"\(BaseUrl.document_image)"+attachment["filename"].stringValue.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                }
                else{
                    temp = "\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+attachment["filename"].stringValue.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                }
                 
                var imgurl = URL(string:BaseUrl.imaginaryURLForDetail2X + temp )
                if type.lowercased().contains("gif"){
                    imgurl = URL(string: temp )
                }
                DispatchQueue.main.async {
                    cell.imageViewPost.kf.indicatorType = .image(imageData: self.appDelegate.gifData)
                    cell.imageViewPost.kf.setImage(with: imgurl, placeholder:nil, options: [.processor(
                        ResizingImageProcessor(referenceSize: CGSize(width: cell.imageViewPost.frame.width, height: cell.imageViewPost.frame.height), mode: .aspectFit)),
                                                                                            .scaleFactor(UIScreen.main.scale),
                                                                                            .transition(.fade(1)),
                                                                                            .cacheOriginalImage
                    ])
                }
                
                
                //            cell.imageViewPost.kf.setImage(with: imgurl, placeholder: #imageLiteral(resourceName: "placeholder.png") , options: nil, progressBlock: nil, completionHandler: nil)
                
                return cell
            }
            else if let type = attachment["type"].string ,type.contains("video"){
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostCollectionViewCell", for: indexPath) as! PostCollectionViewCell
                cell.playIcon.isHidden = false
                var temp = ""
                //"\(Helpers.imageURl)"+"default_video.jpg"
                
                if let thumb = attachment["video_thumb"].string, !thumb.isEmpty {
                    temp = "\(Helpers.imageURl)"+thumb
                    let imgurl = URL(string:BaseUrl.imaginaryURLForDetail2X + temp )
                    
                    cell.playIcon.isHidden = false
                    
                    
                    DispatchQueue.main.async {
                        cell.imageViewPost.kf.indicatorType = .image(imageData: self.appDelegate.gifData)
                        cell.imageViewPost.kf.setImage(with: imgurl, placeholder:nil, options: [.processor(
                            ResizingImageProcessor(referenceSize: CGSize(width: cell.imageViewPost.frame.width, height: cell.imageViewPost.frame.height), mode: .aspectFit)),
                                                                                                .scaleFactor(UIScreen.main.scale),
                                                                                                .transition(.fade(1)),
                                                                                                .cacheOriginalImage
                        ])
                    }
                }
                //                let imgurl = URL(string: temp )
                //                print(temp)
                
                
                return cell
                
                /*
                 let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostCollectionViewCellVideo", for: indexPath) as! PostCollectionViewCellVideo
                 
                 let temp = "\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+attachment["filename"].stringValue
                 let url = URL(string:temp)
                 DispatchQueue.main.async {
                 
                 cell.player = AVPlayer(url: url!)
                 
                 cell.avpController.player = cell.player
                 
                 cell.avpController.view.frame.size.height = cell.videoLayer.frame.size.height
                 
                 cell.avpController.view.frame.size.width = cell.videoLayer.frame.size.width
                 
                 cell.videoLayer.addSubview(cell.avpController.view)
                 }
                 return cell */
                
            }
            else{
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostCollectionViewCellVideo", for: indexPath) as! PostCollectionViewCellVideo
                
                cell.labaleDocName.text = attachment["original_name"].string ?? attachment["filename"].stringValue
                
                if attachment["original_name"].stringValue.lowercased().contains("pdf"){
                    cell.imageViewDoc.image = #imageLiteral(resourceName: "PdfImage")
                }
                else if attachment["original_name"].stringValue.lowercased().contains("doc"){
                                 cell.imageViewDoc.image = #imageLiteral(resourceName: "DocumentImage")
                }
                else if attachment["original_name"].stringValue.lowercased().contains("docx"){
                                     cell.imageViewDoc.image = #imageLiteral(resourceName: "DocumentImage")
                    }
                else{
                    cell.imageViewDoc.image = #imageLiteral(resourceName: "docsIcon")
                }
                //                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostCollectionViewCell", for: indexPath) as! PostCollectionViewCell
                
                //                let temp = "\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+attachment["filename"].stringValue
                //                let imgurl = URL(string: temp)
                //                print(temp)
                //                DispatchQueue.main.async {
                //                     let thumbnailSize = CGSize(width: cell.imageViewPost.bounds.width, height: cell.imageViewPost.bounds.height)
                //                                let thumbnail = SharedPostTableViewCell.generatePdfThumbnail(of: thumbnailSize, for: imgurl!, atPage: 0)
                //                    //            cell.imageViewPost.kf.indicatorType = .activity
                //                                cell.imageViewPost.image = thumbnail ?? #imageLiteral(resourceName: "ic_MSword_big@3x.png")
                //                }
                //
                //                DispatchQueue.main.async {
                //                    cell.playIcon.isHidden = true
                //                cell.imageViewPost.image = #imageLiteral(resourceName: "DocumentTumb")
                //                }
                return cell
            }
        }
        
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 2
                      {
                        let numberOfCellInRow  = 4 //titleArr.count
                              let padding : Int      = 0
                              let collectionCellWidth : CGFloat = (collectionView.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
//                        let height = self.heightOfMentionedView.constant
                        if self.arrayOfMentionedName.count == 1
                        {
                            return CGSize(width: 100, height: 30)

                        }
                        else
                        {
                              return CGSize(width: collectionCellWidth, height: 30)
                        }
        }
        else
        {
        return CGSize.init(width: collectionView.bounds.width, height: Cellheight);
        }
        
        
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView.tag != 2
        {
        pageController.currentPage = indexPath.item
        }
    }
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if let cell = cell as?PostCollectionViewCellVideo{
            
            if (cell.player != nil){
                cell.player.pause()
                
            }
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //        if attachments.count == 1{
        //            let attachment = attachments[indexPath.item]
        //
        //        if let type = attachment["type"].string ,type.contains("text"){
        //            let temp = "\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+attachment["filename"].stringValue
        //            let url = URL.init(string: temp)
        //            let safariViewController = SFSafariViewController(url:url ?? URL.init(string: "")!)
        //            safariViewController.delegate = self
        //
        //        }
        //            else{
        
        //            }
        if collectionView.tag == 2
        {
            print("name selected ........")
            delegate?.selectMentionedName(data: arrayOfMentionedName, index: indexPath.row)

        }
        else
        {
        if attachments.count != 0{
            if let cell = collectionView.cellForItem(at: indexPath)as? PostCollectionViewCellVideo {
                if (cell.player != nil){
                    cell.player.pause()
                }
            }
            delegate?.gotoThanksPreview(data: attachments, index: indexPath.row)
        }
        }
        
    }
    //    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
    //        targetContentOffset.pointee = scrollView.contentOffset
    //        let pageWidth:Float = Float(self.collectionViewPost.bounds.width)
    //        let minSpace:Float = 0.0
    //        var cellToSwipe:Double = Double(Float((scrollView.contentOffset.x))/Float((pageWidth+minSpace))) + Double(0.5)
    //        if cellToSwipe < 0 {
    //            cellToSwipe = 0
    //        } else if cellToSwipe >= Double(self.attachments.count) {
    //            cellToSwipe = Double(self.attachments.count) - Double(1)
    //        }
    //        let indexPath:IndexPath = IndexPath(row: Int(cellToSwipe), section:0)
    //        pageController.currentPage = indexPath.item
    //        self.collectionViewPost.scrollToItem(at:indexPath, at: UICollectionView.ScrollPosition.left, animated: true)
    //
    //
    //    }
    //    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    //        var currentCellOffset = scrollView.contentOffset
    //        currentCellOffset.x += (self.collectionViewPost.bounds.width/2);
    //        let indepath = self.collectionViewPost.indexPathForItem(at: currentCellOffset) ?? IndexPath.init(row: 0, section: 0)
    //        self.collectionViewPost.scrollToItem(at: indepath, at: .centeredHorizontally, animated: true)
    //        pageController.currentPage = indepath.item
    //
    //    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        let pageWidth = self.collectionViewPost.bounds.width
        let proportionalOffset = collectionViewPost.contentOffset.x / pageWidth
        indexOfCellBeforeDragging = Int(round(proportionalOffset))
        
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if(scrollView.panGestureRecognizer.translation(in: scrollView.superview).x < 0) && indexOfCellBeforeDragging >= self.attachments.count - 1{ //right swipe
            self.collectionViewPost.scrollToItem(at:IndexPath.init(row: 0, section: 0), at: UICollectionView.ScrollPosition.left, animated: true)
        }
    }
    
}
