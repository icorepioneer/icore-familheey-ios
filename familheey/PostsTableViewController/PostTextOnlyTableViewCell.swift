//
//  PostTextOnlyTableViewCell.swift
//  familheey
//
//  Created by familheey on 19/11/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class PostTextOnlyTableViewCell: UITableViewCell {
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblFamName: UILabel!
    @IBOutlet weak var lblCreatedAt: UILabel!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var ConvrstnView: UIView!
    @IBOutlet weak var postsUserView: UIView!
    @IBOutlet weak var btmView: UIView!
    @IBOutlet weak var btnReadMore: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.topView.clipsToBounds = true
        self.topView.layer.cornerRadius = 15
        
        self.topView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
