//
//  ReviewPostsViewController.swift
//  familheey
//
//  Created by Giri on 04/12/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON
import Kingfisher
import Firebase

class ReviewPostsViewController: UIViewController {
    
    @IBOutlet weak var segmentedController: UISegmentedControl!
    @IBOutlet weak var tableViewList: UITableView!
    var ArrayPosts = [JSON]()
    var ArrayAnnouncements = [JSON]()
    
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    var initiallimit = 20
    var offset = 0
    var limit = 20
    var lastFetchedIndex = 0;
    let maxNumberOfLines = 2
    var groupId = ""
    var imgClicked = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        segmentedController.selectedSegmentIndex = 0
        
        getPosts()
    }
    
    //MARK:- Button Actions
    
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    @IBAction func postReadmoreAction(_ sender: UIButton) {
        
        let cell = tableViewList.cellForRow(at: IndexPath.init(row: sender.tag, section: 0)) as! PostImageTableViewCell
        if cell.labelPost.calculateMaxLines() > maxNumberOfLines &&  cell.labelPost.numberOfLines == maxNumberOfLines{
            
            tableViewList.beginUpdates()
            cell.labelPost.numberOfLines = 0
            cell.readmoreButton.setTitle("view less", for: .normal)
            tableViewList.endUpdates()
            
            
        }
        else if cell.labelPost.calculateMaxLines() > maxNumberOfLines &&  cell.labelPost.numberOfLines == 0 {
            UIView.performWithoutAnimation {
                tableViewList.beginUpdates()
                cell.labelPost.numberOfLines = maxNumberOfLines
                cell.readmoreButton.setTitle("view more", for: .normal)
                tableViewList.endUpdates()
            }
        }
        
    }
    @IBAction func postAcceptAction(_ sender: UIButton) {
        let post = ArrayPosts[sender.tag]
        
        callApproveRejectPost(approve: true, type: "post", postId:post["post_id"].stringValue, index: sender.tag)
        
    }
    @IBAction func postRejectAction(_ sender: UIButton) {
        let post = ArrayPosts[sender.tag]
        
        callApproveRejectPost(approve: false, type: "post", postId:post["post_id"].stringValue, index: sender.tag)
        
    }
    
    @IBAction func announcementAcceptAction(_ sender: UIButton) {
        let announcement = ArrayAnnouncements[sender.tag]
        callApproveRejectPost(approve: true, type: "announcement", postId:announcement["post_id"].stringValue, index: sender.tag)
        
    }
    @IBAction func AnnouncementRejectAction(_ sender: UIButton) {
        let announcement = ArrayAnnouncements[sender.tag]
        callApproveRejectPost(approve: false, type: "announcement", postId:announcement["post_id"].stringValue, index: sender.tag)
        
    }
    
    @IBAction func sharedPostViewMoreAction(_ sender: UIButton) {
        
        let cell = tableViewList.cellForRow(at: IndexPath.init(row: sender.tag, section: 0)) as! SharedPostTableViewCell
        if cell.labelPost.calculateMaxLines() > maxNumberOfLines &&  cell.labelPost.numberOfLines == maxNumberOfLines{
            
            tableViewList.beginUpdates()
            cell.labelPost.numberOfLines = 0
            cell.readmoreButton.setTitle("view less", for: .normal)
            tableViewList.endUpdates()
            
            
        }
        else if cell.labelPost.calculateMaxLines() > maxNumberOfLines &&  cell.labelPost.numberOfLines == 0 {
            UIView.performWithoutAnimation {
                tableViewList.beginUpdates()
                cell.labelPost.numberOfLines = maxNumberOfLines
                cell.readmoreButton.setTitle("view more", for: .normal)
                tableViewList.endUpdates()
            }
        }
        
    }
    @IBAction func sharedPostAcceptAction(_ sender: UIButton) {
        let post = ArrayPosts[sender.tag]
        callApproveRejectPost(approve: true, type: "post", postId:post["post_id"].stringValue, index: sender.tag)
        
        
    }
    @IBAction func sharedPostRejectAction(_ sender: UIButton) {
        let post = ArrayPosts[sender.tag]
        callApproveRejectPost(approve: false, type: "post", postId:post["post_id"].stringValue, index: sender.tag)
        
    }
    @IBAction func sharedAnnouncementAcceptAction(_ sender: UIButton) {
        let announcement = ArrayAnnouncements[sender.tag]
        callApproveRejectPost(approve: true, type: "announcement", postId:announcement["post_id"].stringValue, index: sender.tag)
        
    }
    @IBAction func sharedAnnouncementRejectAction(_ sender: UIButton) {
        let announcement = ArrayAnnouncements[sender.tag]
        callApproveRejectPost(approve: false, type: "announcement", postId:announcement["post_id"].stringValue, index: sender.tag)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    //MARK:- Do Accept Reject
    
    func callApproveRejectPost(approve:Bool,type:String,postId:String,index:Int){
        
        ActivityIndicatorView.show("Please wait....")
        
        let param = ["user_id" : UserDefaults.standard.value(forKey: "userId") as! String,"post_id":"\(postId)","is_approved":approve ? "1":"2" ,"type":type]
        
        networkProvider.request(.approveRejectPost(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        
                        if let response = jsonData["message"].string,response.lowercased() == "success"{
                            
                            self.tableViewList.beginUpdates()
                            self.tableViewList.deleteRows(at: [ IndexPath(row: index, section: 0) ], with: .fade)
                            if type == "post"{
                                self.ArrayPosts.remove(at: index)
                            }
                            else{
                                self.ArrayAnnouncements.remove(at: index)
                            }
                            self.tableViewList.endUpdates()
                            
                        }
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.callApproveRejectPost(approve: approve, type: type, postId: postId, index: index)
                        }
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
//                     Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
//                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    
    //MARK:- Segment Control
    @IBAction func sengmentedControllChangedValue(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex
        {
        case 0://Posts selection
            groupId = "1009"
            getPosts()
            break
            
        case 1://Announcements
            groupId = "979"
            getAnnouncements()
            break
            
        default:
            break;
        }
        
    }
    
    //MARK:- Get Announcements
    
    func getAnnouncements(){
        ActivityIndicatorView.show("Please wait....")
        let param = ["user_id" : UserDefaults.standard.value(forKey: "userId") as! String,"offset":"\(offset)","limit":"\(limit)","type":"announcement","group_id":groupId]
        
        networkProvider.request(.pendingApprovals(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        print("view contents : \(jsonData)")
                        
                        if let response = jsonData["data"].array{
                            self.ArrayAnnouncements.removeAll()
                            self.ArrayAnnouncements.append(contentsOf: response)
                            self.tableViewList.dataSource = self
                            self.tableViewList.delegate = self
                            self.tableViewList.reloadData()
                            
                        }
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getAnnouncements()
                        }
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    //MARK:- Get posts
    
    func getPosts(){
        ArrayPosts.removeAll()
        
        ActivityIndicatorView.show("Please wait....")
        let param = ["user_id" : UserDefaults.standard.value(forKey: "userId") as! String,"offset":"\(offset)","limit":"\(limit)","type":"post","group_id":groupId]
        
        networkProvider.request(.pendingApprovals(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        print("view contents : \(jsonData)")
                        
                        if let response = jsonData["data"].array{
                            
                            self.ArrayPosts = response
                            if self.ArrayPosts.count > 0 {
                                self.tableViewList.isHidden = false
                                self.tableViewList.delegate = self
                                self.tableViewList.dataSource = self
                                self.tableViewList.reloadData()
                                let topIndex = IndexPath(row: 0, section: 0)
                                self.tableViewList.scrollToRow(at: topIndex, at: .top, animated: false)
                            }
                            else{
                                self.tableViewList.isHidden = true
                            }
                            
                        }
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getPosts()
                        }
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
        
        
        
    }
    
}
//MARK:- Tableview Data source and Delegates

extension ReviewPostsViewController : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch segmentedController.selectedSegmentIndex
        {
        case 0://Posts selection
            return ArrayPosts.count
            
        case 1://Announcements
            return ArrayAnnouncements.count
            
            
        default:
            return 0
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch segmentedController.selectedSegmentIndex
        {
        case 0://Posts selection
            
            guard ArrayPosts.count != 0 else {
                return UITableViewCell.init()
            }
            
            let post = ArrayPosts[indexPath.row]
            
            if let sharedusers = post["shared_user_names"].string , !sharedusers.isEmpty{
                
                
                let cellid = "SharedPostTableViewCell"
                
                let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! SharedPostTableViewCell
                
                let post = ArrayPosts[indexPath.row]
                
                cell.readmoreButton.tag = indexPath.row
                cell.buttonShare.tag = indexPath.row
                cell.buttonSharedUsers.tag = indexPath.row
                //            cell.buttonRightMenu.tag = indexPath.row
                //            cell.commentsButton.tag = indexPath.row
                cell.buttonComments.tag = indexPath.row
                cell.viewsButtons.tag = indexPath.row
                cell.sharedUserButton.tag = indexPath.row
                cell.btnAccept.tag = indexPath.row
                cell.btnReject.tag = indexPath.row
                
                
                //User Details
                
                let array = sharedusers.components(separatedBy: ",")
                if array.count != 0{
                    if array.count == 1{
                        cell.labelUserName.text = "\(array[0]) shared a post"
                    }
                    else if array.count == 2{
                        cell.labelUserName.text = "\(array[0]) and 1 other shared a post"
                    }
                    else {
                        cell.labelUserName.text = "\(array[0]) and \(array.count-1) others shared a post"
                    }
                    //post["created_user_name"].string ?? ""
                    
                }
                if post["to_user_id"].stringValue == UserDefaults.standard.value(forKey: "userId") as! String{
                    cell.lblShared.text = "Shared with"
                    cell.lblShare_width.constant = 78.0
                    cell.labelPostedIn.text = " you"
                }
                else{
                    cell.lblShared.text = "Shared in"
                    cell.lblShare_width.constant = 65.0
                    cell.labelPostedIn.text = post["group_name"].string ?? ""
                }
                
                cell.labelUserNameInner.text = post["parent_post_created_user_name"].string ?? ""
                if post["parent_post_grp_name"].stringValue.count > 0 {
                    cell.labelPostedInInner.text = post["parent_post_grp_name"].string ?? ""
                }
                else{
                    cell.labelPostedInInner.text = post["privacy_type"].string ?? ""
                }
                
                
                //            cell.labelDate.text = post["createdAt"].string ?? ""
                cell.labelDate.text = HomeTabViewController.dateFormatterCommon(dateStr: post["createdAt"].stringValue)
                
                let proPic = post["propic"].stringValue
                
                
                if proPic.count != 0{
                    let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic
                    let imgUrl = URL(string: temp)
                    cell.imageviewProfilePicInner.kf.indicatorType = .activity
                    cell.imageviewProfilePicInner.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                }
                else{
                    cell.imageviewProfilePicInner.image = #imageLiteral(resourceName: "Male Colored")
                }
                
                //Post
                cell.labelPost.text = post["snap_description"].string ?? ""
                
                //Details
                cell.lableNumberOfConversations.text = post["conversation_count"].stringValue
                cell.lblNumberOfShares.text = post["shared_user_count"].stringValue
                cell.lblNumberOfViews.text = post["views_count"].stringValue
                cell.delegate = self
                
                if let Attachment = post["post_attachment"].array, Attachment.count != 0{
                    
                    cell.updateSlider(data: Attachment)
                }
                else{
                    
                    cell.viewPostBg.isHidden = true
                    cell.viewPageControllBG.isHidden = true
                }
                
                
                
                if cell.labelPost.calculateMaxLines() > maxNumberOfLines{
                    cell.labelPost.numberOfLines = maxNumberOfLines
                    cell.readMoreView.isHidden = false
                    
                    //            tableView.layoutIfNeeded()
                }
                else{
                    cell.labelPost.numberOfLines = 0
                    //                cell.readMoreView.isHidden = true
                    cell.readmoreButton.isHidden = true
                    cell.readMore_height.constant = 5
                    //            tableView.layoutIfNeeded()
                }
                
                
                let user = UserDefaults.standard.value(forKey: "userId") as! String
                if  user  == post["created_by"].stringValue{
                    cell.viewViews.isHidden = false
                    cell.viewShared.isHidden = false
                    if post["conversation_enabled"].bool ?? false{
                        cell.ViewComments.isHidden = false
                    }
                    else{
                        cell.ViewComments.isHidden = true
                    }
                }
                else{
                    cell.viewViews.isHidden = true
                    cell.viewShared.isHidden = true
                    if post["conversation_enabled"].bool ?? false{
                        cell.ViewComments.isHidden = false
                    }
                    else{
                        cell.ViewComments.isHidden = true
                    }
                }
                
                if post["conversation_count_new"].intValue > 0{
                    cell.lblNewConverstn.isHidden = false
                }
                else{
                    cell.lblNewConverstn.isHidden = true
                }
                
                if post["is_shareable"].bool ?? false {
                    
                    cell.shareView.isHidden = false
                    
                }
                else{
                    
                    cell.shareView.isHidden = true
                    
                }
                
                //pagination
                //                  if indexPath.row == self.ArrayPosts.count - 1 {
                //
                //                        offset = ArrayPosts.count + 1
                //                        if selectedIndex == 0{
                //                            getPosts()
                //                        }
                //                        else{
                //                            getpublicfeed()
                //                        }
                //
                //                    }
                return cell
            }
                
                
            else{
                let cellid = "PostImageTableViewCell"
                
                let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! PostImageTableViewCell
                
                
                cell.readmoreButton.tag = indexPath.row
                cell.buttonShare.tag = indexPath.row
                cell.buttonRightMenu.tag = indexPath.row
                cell.commentsButton.tag = indexPath.row
                cell.viewsButton.tag = indexPath.row
                cell.sharedUsersButton.tag = indexPath.row
                cell.buttonComments.tag = indexPath.row
                cell.btnRejectReview.tag = indexPath.row
                cell.btnAcceptReview.tag = indexPath.row
                //User Details
                cell.labelUserName.text = post["created_user_name"].string ?? ""
                cell.labelPostedIn.text = post["group_name"].string ?? "Public"
                //            cell.labelDate.text = post["createdAt"].string ?? ""
                cell.labelDate.text = HomeTabViewController.dateFormatterCommon(dateStr: post["createdAt"].stringValue)
                
                let proPic = post["propic"].string ?? ""
                
                
                if proPic.count != 0{
                    let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic
                    let imgUrl = URL(string: temp)
                    
                    cell.imageViewProfilePic.kf.indicatorType = .activity
                    cell.imageViewProfilePic.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                }
                else{
                    cell.imageViewProfilePic.image = #imageLiteral(resourceName: "Male Colored")
                }
                
                //Post
                cell.labelPost.text = post["snap_description"].string ?? ""
                
                //Details
                cell.lableNumberOfConversations.text = post["conversation_count"].stringValue
                cell.lblNumberOfShares.text = post["shared_user_count"].stringValue
                cell.labelNumberOfViews.text = post["views_count"].stringValue
                cell.delegate = self

                if let Attachment = post["post_attachment"].array, Attachment.count != 0{
                    print("updating slide........")
                    print(indexPath.row)
                    cell.updateSlider(data: Attachment)
                }
                else{
                    print("No Slide........")
                    print(indexPath.row)
                    cell.viewPostBg.isHidden = true
                    cell.viewPageControllBG.isHidden = true
                }
                
                
                
                
                if cell.labelPost.calculateMaxLines() > maxNumberOfLines{
                    cell.labelPost.numberOfLines = maxNumberOfLines
                    cell.readMoreView.isHidden = false
                    
                    //            tableView.layoutIfNeeded()
                }
                else{
                    cell.labelPost.numberOfLines = 0
                    cell.readMoreView.isHidden = true
                    //            tableView.layoutIfNeeded()
                }
                
                // String(describing: UserDefaults.standard.value(forKey: "userId"))
                let user = UserDefaults.standard.value(forKey: "userId") as! String
                if  user  == post["created_by"].stringValue{
                    
                    cell.viewViews.isHidden = false
                    cell.viewShared.isHidden = false
                    if post["conversation_enabled"].bool ?? false{
                        cell.viewComments.isHidden = false
                    }
                    else{
                        cell.viewComments.isHidden = true
                    }
                }
                else{
                    
                    cell.viewViews.isHidden = true
                    cell.viewShared.isHidden = true
                    if post["conversation_enabled"].bool ?? false{
                        cell.viewComments.isHidden = false
                    }
                    else{
                        cell.viewComments.isHidden = true
                    }
                }
                
                if post["conversation_count_new"].intValue > 0{
                    cell.lblNewConvrstn.isHidden = false
                }
                else{
                    cell.lblNewConvrstn.isHidden = true
                }
                
                
                if post["is_shareable"].bool ?? false {
                    
                    cell.shareView.isHidden = false
                    
                }
                else{
                    
                    cell.shareView.isHidden = true
                    
                }
                
                //pagination
                //
                //            if indexPath.row == self.ArrayPosts.count - 1 {
                //
                //                offset = ArrayPosts.count + 1
                //                if selectedIndex == 0{
                //                    getPosts()
                //                }
                //                else{
                //                    getpublicfeed()
                //                }
                //
                //
                //            }
                
                
                return cell
            }
            
        case 1://Announcements
            
            if ArrayAnnouncements.count == 0{
                return UITableViewCell.init()
            }
            
            let announcement = ArrayAnnouncements[indexPath.row]
            
            if let sharedusers = announcement["shared_user_names"].string , !sharedusers.isEmpty{
                
                let cell  = tableView.dequeueReusableCell(withIdentifier: "AnnouncementsSharedTableViewCell", for: indexPath) as! AnnouncementsSharedTableViewCell
                
                
                let array = sharedusers.components(separatedBy: ",")
                if array.count != 0{
                    if array.count == 1{
                        cell.lblsharedUserName.text = "\(array[0]) shared a post"
                    }
                    else if array.count == 2{
                        cell.lblsharedUserName.text = "\(array[0]) and 1 other shared a post"
                    }
                    else {
                        cell.lblsharedUserName.text = "\(array[0]) and \(array.count-1) others shared a post"
                    }
                }
                
                if announcement["to_user_id"].stringValue == UserDefaults.standard.value(forKey: "userId") as! String{
                    cell.lblShared.text = "Shared with"
                    cell.lblShare_width.constant = 78.0
                    cell.lblPostedIn.text = " you"
                }
                else{
                    cell.lblShared.text = "Shared in"
                    cell.lblShare_width.constant = 65.0
                    cell.lblPostedIn.text = announcement["group_name"].string ?? ""
                }
                
                
                cell.lblUserName.text = announcement["parent_post_created_user_name"].string ?? ""
                if announcement["parent_post_grp_name"].stringValue.count > 0 {
                    cell.lblPostedIn.text = announcement["parent_post_grp_name"].string ?? ""
                }
                else{
                    cell.lblPostedIn.text = announcement["privacy_type"].string ?? ""
                }
                
                
                cell.lblSharedPostedIn.text = announcement["group_name"].string ?? ""
                
                
                cell.buttonDocs.tag = indexPath.row
                cell.buttonViews.tag = indexPath.row
                cell.buttonComments.tag = indexPath.row
                cell.buttonShareAnnouncement.tag = indexPath.row
                cell.buttonSideMenu.tag = indexPath.row
                cell.btnRejectReview.tag = indexPath.row
                cell.btnAcceptReview.tag = indexPath.row
                cell.lblSharedDate.text = HomeTabViewController.dateFormatterCommon(dateStr: announcement["createdAt"].stringValue)
                cell.lblAnnouncement.text = announcement["snap_description"].string ?? ""
                cell.lblnumberofComments.text = announcement["conversation_count"].string ?? ""
                cell.lblnumberofView.text = announcement["views_count"].string ?? ""
                
                
                var count = 0
                
                if let attachments = announcement["post_attachment"].array{
                    
                    for items in attachments{
                        if items["type"].stringValue.contains("image"){
                            count = count + 1
                        }
                    }
                    let proPic = announcement["propic"].stringValue
                    
                    
                    if proPic.count != 0{
                        let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic
                        let imgUrl = URL(string: temp)
                        cell.imageviewUserProfilepic.kf.indicatorType = .activity
                        cell.imageviewUserProfilepic.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                    }
                    else{
                        cell.imageviewUserProfilepic.image = #imageLiteral(resourceName: "Male Colored")
                    }
                }
                if announcement["is_shareable"].boolValue {
                    cell.viewShare.isHidden = false
                }
                else{
                    cell.viewShare.isHidden = true
                    
                }
                if announcement["conversation_enabled"].boolValue{
                    cell.viewComments.isHidden = false
                }
                else{
                    cell.viewComments.isHidden = true
                }
                cell.numberofDocs.text = "\(count)"
                
                if "\(UserDefaults.standard.value(forKey: "userId") ?? "0")" == announcement["created_by"].stringValue {
                    
                    cell.viewViews.isHidden = false
                    
                }
                else{
                    cell.viewViews.isHidden = true
                    
                }
                return cell
            }
            else{
                
                let cell  = tableView.dequeueReusableCell(withIdentifier: "AnnouncementsTableViewCell", for: indexPath) as! AnnouncementsTableViewCell
                
                cell.buttonDocs.tag = indexPath.row
                cell.buttonViews.tag = indexPath.row
                cell.buttonComments.tag = indexPath.row
                cell.buttonShareAnnouncement.tag = indexPath.row
                cell.buttonSideMenu.tag = indexPath.row
                cell.btnAcceptReview.tag = indexPath.row
                cell.btnRejectReview.tag = indexPath.row
                
                //        cell.lblDate.text = announcement["createdAt"].string ?? ""
                cell.lblDate.text = HomeTabViewController.dateFormatterCommon(dateStr: announcement["createdAt"].stringValue)
                
                cell.lblPostedIn.text = announcement["group_name"].string ?? ""
                cell.lblUserName.text = announcement["created_user_name"].string ?? ""
                cell.lblAnnouncement.text = announcement["snap_description"].string ?? ""
                
                cell.lblnumberofComments.text = announcement["conversation_count"].string ?? ""
                
                cell.lblnumberofView.text = announcement["views_count"].string ?? ""
                
                
                var count = 0
                
                if let attachments = announcement["post_attachment"].array{
                    
                    for items in attachments{
                        if items["type"].stringValue.contains("image") {
                            count = count + 1
                        }
                    }
                    let proPic = announcement["propic"].stringValue
                    
                    
                    if proPic.count != 0{
                        let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic
                        let imgUrl = URL(string: temp)
                        cell.imageviewUserProfilepic.kf.indicatorType = .activity
                        cell.imageviewUserProfilepic.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                    }
                    else{
                        cell.imageviewUserProfilepic.image = #imageLiteral(resourceName: "Male Colored")
                    }
                }
                if announcement["is_shareable"].boolValue {
                    cell.viewShare.isHidden = false
                }
                else{
                    cell.viewShare.isHidden = true
                    
                }
                if announcement["conversation_enabled"].boolValue{
                    cell.viewComments.isHidden = false
                }
                else{
                    cell.viewComments.isHidden = true
                }
                cell.numberofDocs.text = "\(count)"
                
                if "\(UserDefaults.standard.value(forKey: "userId") ?? "0")" == announcement["created_by"].stringValue {
                    
                    cell.viewViews.isHidden = false
                    
                }
                else{
                    cell.viewViews.isHidden = true
                    
                }
                return cell
                
            }
        default:
            
            return UITableViewCell.init()
            
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        switch segmentedController.selectedSegmentIndex
        {
        case 1://Announcement selection
            
            tableView.deselectRow(at: indexPath, animated: false)
            
            let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementDetailViewController") as! AnnouncementDetailViewController
            vc.ArrayAnnouncements = ArrayAnnouncements
            vc.selectedindex = indexPath
            self.navigationController?.pushViewController(vc, animated: true)
            
            break
        default:
            break
        }
        
    }
    
    //    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    //        if fromRefresh{
    //            fromRefresh = false
    //        }
    //        else{
    //            var currentCellOffset = scrollView.contentOffset
    //            currentCellOffset.x += (self.tblListView.bounds.width);
    //
    //            let indepath = self.tblListView.indexPathsForVisibleRows?.first
    //            if indepath != nil{
    //                self.viewCurrentPost(index: (indepath?.row)!)
    //            }
    //        }
    //    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func getnumberOfLines(label:UILabel) -> Int{
        var   lineCount:NSInteger = 0
        let  textSize:CGSize = CGSize.init(width: label.frame.size.width, height: CGFloat(MAXFLOAT))
        let   rHeight:Int = lroundf(Float(label.sizeThatFits(textSize).height))
        let   charSize:Int = lroundf(Float(label.font!.lineHeight))
        lineCount = rHeight/charSize
        NSLog("No of lines: %i",lineCount)
        return lineCount
    }
}
extension ReviewPostsViewController : PostImageTableViewCellDelegate{
    func gotoPreview(data: [JSON],index:Int) {
        
        let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsPreviewViewController") as! AnnouncementsPreviewViewController
        vc.attachments = data
        vc.isFromPost = true
        vc.selectedIndex = index
        self.navigationController?.addFadeAnimation()
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func gotoSinglePreview(data: String) {
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: "ChangePicturesViewController") as! ChangePicturesViewController
        vc.isCoverOrProfilePic = "CoverPic"
        vc.ids           = UserDefaults.standard.value(forKey: "userId") as! String
        vc.isFrom = "post"
        vc.inFor = "cover"
        vc.isAdmin = ""
        vc.imageUrl = data
        self.imgClicked = true
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}
extension ReviewPostsViewController : SharedPostTableViewCellDelegate{
    
    func gotoSharedPreview(data: [JSON], index: Int)
    {
        
        let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsPreviewViewController") as! AnnouncementsPreviewViewController
        vc.attachments = data
        vc.isFromPost = true
        vc.selectedIndex = index
        self.imgClicked = true
    self.navigationController?.addFadeAnimation()
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func gotoSharedSinglePreview(data: String)
    {
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: "ChangePicturesViewController") as! ChangePicturesViewController
        vc.isCoverOrProfilePic = "CoverPic"
        vc.ids           = UserDefaults.standard.value(forKey: "userId") as! String
        vc.isFrom = "post"
        vc.inFor = "cover"
        vc.isAdmin = ""
        self.imgClicked = true
        vc.imageUrl = data
        self.navigationController?.pushViewController(vc, animated: true)
        
    }

    
}
