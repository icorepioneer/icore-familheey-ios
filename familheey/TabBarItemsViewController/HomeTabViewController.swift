//
//  HomeTabViewViewController.swift
//  familheey
//
//  Created by familheey on 16/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON
import Kingfisher
import Firebase
import SideMenu

import CoreLocation
import SafariServices



class HomeTabViewController: UIViewController,UIGestureRecognizerDelegate,CLLocationManagerDelegate, suggestedViewDismissDelegate,postUpdateDelegate {
    
    @IBOutlet weak var bannerView: UIView!
    @IBOutlet weak var bannerImageView:UIImageView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblHead: UILabel!
    
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var imgSearch: UIImageView!
    @IBOutlet weak var btnResetSearch: UIButton!
    
    @IBOutlet weak var btnMyFamilheey: UIButton!
    @IBOutlet weak var imgMyFamilheey: UIImageView!
    @IBOutlet weak var btnPublicFeed: UIButton!
    @IBOutlet weak var imgPublicFee: UIImageView!
    
    @IBOutlet weak var viewAnnouncement: UIView!
    @IBOutlet weak var tblListView: UITableView!
    
    @IBOutlet weak var lblNoti_Count: UILabel!
    @IBOutlet weak var viewOfNoti_Count: UIView!
    @IBOutlet weak var imgOfNoti_Bell: UIImageView!
    @IBOutlet weak var noPostView: UIView!
    @IBOutlet weak var readMore_height: NSLayoutConstraint!
    @IBOutlet weak var lblnoData: UILabel!
    @IBOutlet weak var searchConstraintheight: NSLayoutConstraint!
    @IBOutlet weak var searchview: UIView!
    @IBOutlet weak var notificationHeight: NSLayoutConstraint!
    
    @IBOutlet weak var notificationView: UIView!
    @IBOutlet weak var noPostSearch: UIView!
    @IBOutlet weak var imgRequests: UIImageView!
    @IBOutlet weak var buttonRequests: UIButton!
    @IBOutlet weak var imgViewNoData: UIImageView!
    
    var ShareOptionArray = [String]()
    var previousPageXOffset: CGFloat = 0.0
    let headerHeight: CGFloat = 56
    
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var documentInteractionController: UIDocumentInteractionController!
    
    var ArrayPosts = [JSON]()
    var bannerArray = JSON()
    var selectedIndex = Int()
    var searchTxt = ""
    var fromRefresh = false
    
    let maxNumberOfLines = 2
    var refreshControl = UIRefreshControl()
    var initiallimit = 30
    var offset = 0
    var limit = 0
    var lastFetchedIndex = 0;
    var arrayOfNotificationList = [[String:Any]]()
    var notiUnreadCount:Int = 0
    var imgClicked = false
    var stopPagination = false
    var isNewDataLoading = true
    var announcementCountExists = false
    // var searchTxt = ""
    var redirectType = ""
    
    var bannerImageAvailable = false
    
    
    var postoptionsTittle = ["Mute conversation","Edit post","Delete post","Report"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        limit = initiallimit
        lastFetchedIndex = initiallimit
        viewAnnouncement.isHidden = true
        announcementCountExists = false
        txtSearch.delegate = self
        
        let right: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action:#selector(self.respondToSwipeGesture))
        right.cancelsTouchesInView = false
        right.direction = .right
        right.delegate = self
        let left: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action:#selector(self.respondToSwipeGesture))
        left.cancelsTouchesInView = false
        left.direction = .left
        left.delegate = self
        view.addGestureRecognizer(left)
        view.addGestureRecognizer(right)
        
        Helpers.setleftView(textfield: txtSearch, customWidth: 30)
        
        let button = UIButton()
        if appDel.noFamily{
            button.tag = 101
            selectedIndex = 1
        }
        else{
            button.tag = 100
            selectedIndex = 0
        }
        updateTabUi()
        
        tblListView.register(UINib(nibName: "PostTextOnlyTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        //        tblListView.rowHeight = UITableView.automaticDimension
        //        tblListView.estimatedRowHeight = 210
        self.tblListView.delegate = self
        self.tblListView.dataSource = self
        ArrayPosts.removeAll()
        
        refreshControl.attributedTitle = NSAttributedString(string: "Please wait...")
        refreshControl.addTarget(self, action: #selector(refreshPost), for: .valueChanged)
        tblListView.addSubview(refreshControl)
        let userName = UserDefaults.standard.value(forKey: "user_fullname") as? String ?? ""
        //        if userName == ""{
        //            self.getUserDetails()
        //        }
        
        appDelegate.isConversationUpdated = true
        searchview.isHidden = true
        searchConstraintheight.constant = 0
        
        if appDel.isFromNotification{
            
            //            self.lblNoti_Count.isHidden = true
            self.viewOfNoti_Count.isHidden = true
            //        appDel.firstTimeNot = true
            let storyboard = UIStoryboard.init(name: "third", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            vc.arrayOfNotificationList = self.arrayOfNotificationList
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if appDel.isFromShare {
            
            let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CreatePostViewController") as! CreatePostViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        self.tabBarController?.tabBar.isTranslucent = false
        bannerImageAvailable = true
        bannerView.isHidden = true
        getBannerForDisplaying()
        setupRightView()
        NotificationCenter.default.addObserver(self, selector: #selector(childNodeAdded(notification:)), name: Notification.Name("ChildAdded"), object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //        ArrayPosts.removeAll()
        self.navigationController?.navigationBar.isHidden = true
        
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.isTranslucent = false
        
        self.viewOfNoti_Count.isHidden = true
        self.NotificationArrayListing()
        
        self.getAnnouncementBannerCount()
        if appDelegate.isConversationUpdated{
            if appDel.iscreation == isFromCreation.publicfeed{
                selectedIndex = 1
            }
            else if appDel.iscreation == isFromCreation.request{
                selectedIndex = 2
            }
            else{
                selectedIndex = 1
                if appDel.noFamily{
                    DispatchQueue.main.asyncAfter(deadline: .now()+3) {
                        let stryboard = UIStoryboard.init(name: "OnboardUser", bundle: nil)
                        let vc = stryboard.instantiateViewController(withIdentifier: "LandingPopOverViewController") as! LandingPopOverViewController
                        vc.modalPresentationStyle = .fullScreen
                        vc.delegate = self
                        let navigationController = UINavigationController(rootViewController: vc)
                        self.present(navigationController, animated: true, completion: nil)
                    }
                }
                else{
                    selectedIndex = 0
                }
                
            }
            updateTabUi()
            appDel.iscreation = isFromCreation.myfamily
        }
        else{
            
        }
        
        if imgClicked{
            
        }
        else{
            if selectedIndex == 0 && appDelegate.isConversationUpdated{
                offset = 0
                getPosts()
            }
            else if appDelegate.isConversationUpdated && selectedIndex == 1{
                offset = 0
                getpublicfeed()
            }
            else if selectedIndex == 2 && appDelegate.isConversationUpdated  {
                offset = 0
                getrequests()
            }
        }
        
        
        if UserDefaults.standard.value(forKey: "userProfile") != nil{
            let pic = UserDefaults.standard.value(forKey: "userProfile") as! String
            let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+pic.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let imgUrl = URL(string: temp)
            let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+imgUrl!.relativeString
            let url = URL(string: newUrlStr)
            imgProfile.kf.indicatorType = .activity
            
            imgProfile.kf.setImage(with: url, placeholder: UIImage(named: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        else{
            //            imgProfile.image = UIImage(named: "Male Colored")
            self.getUserDetails()
            
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isTranslucent = true
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if let swipeGesture = gestureRecognizer as? UISwipeGestureRecognizer {
            //            switch swipeGesture.direction {
            //            case UISwipeGestureRecognizer.Direction.right:
            //                print("Swiped right")
            //
            //                if selectedIndex == 1{
            //                    btnMyFamilheey.sendActions(for: .touchUpInside)
            //                }
            //
            //            case UISwipeGestureRecognizer.Direction.left:
            //
            //                if selectedIndex == 0{
            //                    btnPublicFeed.sendActions(for: .touchUpInside)
            //                }
            //                print("Swiped left")
            //            default:
            //                break
            //            }
        }
        return true
    }
    
    //MARK:- setup right swipe to conversation
    func setupRightView(){
        let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "topicListingViewController") as! topicListingViewController
        
        let rightMenuNavigationController = SideMenuNavigationController(rootViewController: vc)
        SideMenuManager.default.leftMenuNavigationController = rightMenuNavigationController
        SideMenuManager.default.addPanGestureToPresent(toView: self.view)
        SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: self.view)
        rightMenuNavigationController.statusBarEndAlpha = 0
        rightMenuNavigationController.menuWidth = self.view.frame.width
        rightMenuNavigationController.dismissOnPush = false
        rightMenuNavigationController.dismissOnPresent = false
        rightMenuNavigationController.pushStyle = .subMenu
    }
    
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer){
        
    }
    @objc func refreshPost(refreshControl: UIRefreshControl) {
        refreshControl.endRefreshing()
        fromRefresh = true
        offset = 0
        if selectedIndex == 0{
            getPosts()
        }
        else if selectedIndex == 1{
            getpublicfeed()
        }
        else{
            getrequests()
        }
    }
    
    
    //MARK:- Update Announcement seen
    func updateAnnouncementSeen(){
        let param = ["user_id" : UserDefaults.standard.value(forKey: "userId") as! String]
        
        networkProvider.request(.addUpdate_Announcement_Seen(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    
                    if response.statusCode == 200{
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.updateAnnouncementSeen()
                        }
                    }
                }catch _ {
                }
            case .failure( _):
                break
            }
        }
    }
    
    //MARK:- Update Announcement count
    func getAnnouncementBannerCount(){
        let param = ["user_id" : UserDefaults.standard.value(forKey: "userId") as! String]
        
        networkProvider.request(.get_announcement_banner_count(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    
                    if response.statusCode == 200{
                        let jsonArray = jsonData.arrayValue
                        
                        if jsonArray.count != 0  {
                            self.viewAnnouncement.isHidden = false
                            self.announcementCountExists = true
                        }
                        else{
                            self.viewAnnouncement.isHidden = true
                            self.announcementCountExists = false
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getAnnouncementBannerCount()
                        }
                    }
                }catch _ {
                    
                    
                }
            case .failure( _):
                //                ActivityIndicatorView.hiding()
                break
            }
        }
    }
    
    //MARK:- Get all posts
    func getPosts(){
        appDelegate.isConversationUpdated = false
        lblnoData.text = "Posts and updates from your families will show up here."
        
        imgViewNoData.isHidden = true
        self.noPostSearch.isHidden = true
        self.noPostView.isHidden = true
        
        if offset == 0{
            ArrayPosts.removeAll()
            ActivityIndicatorView.show("Please wait....")
        }else{
            tblListView.showLoading()
        }
        
        self.stopPagination = false
        
        let param = ["user_id" : UserDefaults.standard.value(forKey: "userId") as! String,"offset":"\(offset)","limit":"\(limit)","type":"post", "query":self.txtSearch.text!]
        
        networkProvider.request(.postByFamily(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    self.isNewDataLoading = false
                    let jsonData =  JSON(response.data)
                    
                    if response.statusCode == 200{
                        if self.offset == 0{
                            ActivityIndicatorView.hiding()
                        }else{
                            self.tblListView.hideLoading()
                        }
                        
                        if let response = jsonData["data"].array{
                            
                            print("Json data reponse contents : \(response.count) Offset = \(self.offset) Limit = \(self.limit)")
                            if response.count == 0{
                                self.stopPagination = true
                            }
                            self.ArrayPosts.append(contentsOf: response)
                            
                            if self.offset != 0{
                                if response.count > 0 {
                                    self.insertRowsAtTableView(responseCount: response.count)
                                }
                            }
                            if self.ArrayPosts.count > 0 {
                                print("Json data : \(self.ArrayPosts)")
                                self.tblListView.isHidden = false
                                
                                if self.offset == 0{
                                    self.tblListView.reloadData()
                                }
                                if self.offset == 0{
                                    let topIndex = IndexPath(row: 0, section: 0)
                                    self.tblListView.scrollToRow(at: topIndex, at: .top, animated: false)
                                }
                                if self.offset <= self.limit{
                                    //                                    let topIndex = IndexPath(row: 0, section: 0)
                                    //                                    self.tblListView.scrollToRow(at: topIndex, at: .top, animated: false)
                                    if self.txtSearch.text!.isEmpty{
                                        self.closeSearch()
                                        
                                    }
                                    else{
                                        self.openSearch()
                                    }
                                    
                                }
                                
                                //                                if self.offset == 0{
                                //                                    let range = NSMakeRange(0, self.tblListView.numberOfSections)
                                //                                    let sections = NSIndexSet(indexesIn: range)
                                //                                    self.tblListView.reloadSections(sections as IndexSet, with: .right)
                                //                                }
                                //                                else{
                                //
                                //                                }
                                
                                //                                let topIndex = IndexPath(row: 0, section: 0)
                                //                                self.tblListView.scrollToRow(at: topIndex, at: .top, animated: false)
                            }
                            else{
                                self.tblListView.isHidden = true
                                if self.txtSearch.text!.count > 0{
                                    self.noPostSearch.isHidden = false
                                }
                                else{
                                    self.noPostView.isHidden = false
                                }
                            }
                            
                            //                            if response.count < self.limit {
                            //                            }else{
                            //                                self.offset =  self.ArrayPosts.count + 1
                            //                                self.getPosts()
                            //                            }
                        }
                        
                        
                        //                        if let newAnnouncemnt = jsonData["announcement_count"].array{
                        //                            let temp = newAnnouncemnt
                        //                            let value = temp[0]
                        //                            if value["count"].intValue > 0 {
                        //                                self.viewAnnouncement.isHidden = false
                        //                                self.announcementCountExists = true
                        //                            }
                        //                            else{
                        //                                self.viewAnnouncement.isHidden = true
                        //                                self.announcementCountExists = false
                        //                            }
                        //                        }
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getPosts()
                        }
                    }
                        
                    else{
                        if self.offset == 0{
                            ActivityIndicatorView.hiding()
                        }
                        else{
                            self.tblListView.hideLoading()
                            
                        }
                    }
                    
                }catch _ {
                    if self.offset == 0{
                        ActivityIndicatorView.hiding()
                    }
                    else{
                        self.tblListView.hideLoading()
                        
                    }
                }
            case .failure( _):
                if self.offset == 0{
                    ActivityIndicatorView.hiding()
                }
                else{
                    self.tblListView.hideLoading()
                }                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
        
        
        
    }
    
    //    func prefetchingMyPost(offset : Int){
    //        appDelegate.isConversationUpdated = false
    //        lblnoData.text = "Posts and updates from your families will show up here."
    //        if offset == 0{
    //            ArrayPosts.removeAll()
    //        }
    //
    //        let param = ["user_id" : UserDefaults.standard.value(forKey: "userId") as! String,"offset":"\(offset)","limit":"\(limit)","type":"post", "query":self.txtSearch.text!]
    //
    //        networkProvider.request(.postByFamily(parameter: param)) { (result) in
    //            switch result{
    //            case .success( let response):
    //                do{
    //                    self.isNewDataLoading = false
    //                    let jsonData =  JSON(response.data)
    //                    print("view contents : \(jsonData)")
    //                    if response.statusCode == 200{
    //                        if let response = jsonData["data"].array{
    //                            print("Json data reponse contents : \(response.count) Offset = \(offset) Limit = \(self.limit)")
    //                            if response.count == 0{
    //                                self.stopPagination = true
    //                            }
    //
    //                            self.ArrayPosts.append(contentsOf: response)
    //
    //                            if offset != 0{
    //                                if response.count > 0 {
    //                                self.insertRowsAtTableView(responseCount: response.count)
    //                                }
    //                            }
    //
    //
    //
    //                            if self.ArrayPosts.count > 0 {
    //                                self.tblListView.isHidden = false
    //
    //                                if offset == 0{
    //                                    self.tblListView.delegate = self
    //                                    self.tblListView.dataSource = self
    //                                     self.tblListView.reloadData()
    //                                }
    //
    ////                                if self.offset <= self.limit{ Shibin commented
    ////                                    let topIndex = IndexPath(row: 0, section: 0)
    ////                                    self.tblListView.scrollToRow(at: topIndex, at: .top, animated: false)
    ////                                }
    //                            }
    //                            else{
    //                                self.tblListView.isHidden = true
    //                                if self.txtSearch.text!.count > 0{
    //                                    self.noPostSearch.isHidden = false
    //                                }
    //                                else{
    //                                    self.noPostView.isHidden = false
    //                                }
    //                            }
    //
    //
    //                            if response.count < self.limit {
    //                                print("Responseeee Count = \(response.count)")
    ////                                self.tblListView.reloadData()
    //                            }else{
    //                                self.prefetchingMyPost(offset: self.ArrayPosts.count+1)
    //                                print("Arrrrraaayyy COuunnt \(self.ArrayPosts.count)")
    //                            }
    //
    //
    //                        }
    //                        if let newAnnouncemnt = jsonData["announcement_count"].array{
    //                            let temp = newAnnouncemnt
    //                            let value = temp[0]
    //                            if value["count"].intValue > 0 && !self.searchview.isHidden{
    //                                self.viewAnnouncement.isHidden = false
    //                                self.announcementCountExists = true
    //                            }
    //                            else{
    //                                self.viewAnnouncement.isHidden = true
    //                                self.announcementCountExists = false
    //                            }
    //                        }
    //
    //                    }
    //
    //                    if self.offset == 0{
    //                        ActivityIndicatorView.hiding()
    //                    }
    //                    else{
    //                        self.tblListView.hideLoading()
    //                    }
    //
    //                }catch _ {
    //                    if self.offset == 0{
    //                        ActivityIndicatorView.hiding()
    //                    }
    //                    else{
    //                        self.tblListView.hideLoading()
    //
    //                    }
    //                }
    //            case .failure( _):
    //                if self.offset == 0{
    //                    ActivityIndicatorView.hiding()
    //                }
    //                else{
    //                    self.tblListView.hideLoading()
    //                }                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
    //                break
    //            }
    //        }
    //    }
    func insertRowsAtTableView(responseCount: Int){
        
        var indexPaths = [IndexPath]()
        
        for i in 0 ..< responseCount{
            let indx = ArrayPosts.count - responseCount
            let indexPath = indx + i
            indexPaths.append(IndexPath(row: indexPath, section: 0))
        }
        
        print(indexPaths)
        self.tblListView.beginUpdates()
        self.tblListView.insertRows(at: indexPaths, with: .none)
        self.tblListView.endUpdates()
    }
    func getrequests(){
        
        appDelegate.isConversationUpdated = false
        lblnoData.text = "Help requests from your families will show up here."
        imgViewNoData.isHidden = false
        self.noPostSearch.isHidden = true
        self.noPostView.isHidden = true
        
        if offset == 0{
            ArrayPosts.removeAll()
            ActivityIndicatorView.show("Please wait....")
        }else{
            tblListView.showLoading()
        }
        self.stopPagination = false
        
        
        let param = ["user_id" : UserDefaults.standard.value(forKey: "userId") as! String,"offset":"\(offset)","limit":"\(limit)","type":"post","txt":txtSearch.text!]
        print(param)
        networkProvider.request(.post_need_list(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    
                    if self.offset == 0{
                        ActivityIndicatorView.hiding()
                    }
                    else{
                        self.tblListView.hideLoading()
                    }
                    self.isNewDataLoading = false
                    let jsonData =  JSON(response.data)
                    if response.statusCode == 200{
                        
                        print("view contents : \(jsonData)")
                        
                        if let response = jsonData["data"].array{
                            
                            print("Json data reponse contents : \(response.count) Offset = \(self.offset) Limit = \(self.limit)")
                            
                            if response.count == 0{
                                self.stopPagination = true
                            }
                            self.ArrayPosts.append(contentsOf: response)
                            
                            if self.offset != 0{
                                if response.count > 0 {
                                    self.insertRowsAtTableView(responseCount: response.count)
                                }
                            }
                            if self.ArrayPosts.count > 0 {
                                self.tblListView.isHidden = false
                                
                                if self.offset == 0{
                                    self.tblListView.delegate = self
                                    self.tblListView.dataSource = self
                                    self.tblListView.reloadData()
                                }
                                if self.offset == 0{
                                    let topIndex = IndexPath(row: 0, section: 0)
                                    self.tblListView.scrollToRow(at: topIndex, at: .top, animated: false)
                                }
                                
                            }
                            else{
                                self.tblListView.isHidden = true
                                if self.txtSearch.text!.count > 0{
                                    self.noPostSearch.isHidden = false
                                }
                                else{
                                    self.noPostView.isHidden = false
                                }
                            }
                            
                            
                        }
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getrequests()
                        }
                    }
                    
                }catch _ {
                    if self.offset == 0{
                        ActivityIndicatorView.hiding()
                    }
                    else{
                        self.tblListView.hideLoading()
                    }
                }
            case .failure( _):
                if self.offset == 0{
                    ActivityIndicatorView.hiding()
                }
                else{
                    self.tblListView.hideLoading()
                }
                break
            }
        }
    }
    
    func getpublicfeed(){
        
        appDelegate.isConversationUpdated = false
        lblnoData.text = "Public posts will show up here"
        imgViewNoData.isHidden = true
        self.noPostSearch.isHidden = true
        self.noPostView.isHidden = true
        
        if offset == 0{
            ArrayPosts.removeAll()
            ActivityIndicatorView.show("Please wait....")
        }else{
            tblListView.showLoading()
        }
        self.stopPagination = false
        
        
        let param = ["user_id" : UserDefaults.standard.value(forKey: "userId") as! String,"offset":"\(offset)","limit":"\(limit)","type":"post","query":txtSearch.text!]
        
        networkProvider.request(.postPublicFeed(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    
                    if self.offset == 0{
                        ActivityIndicatorView.hiding()
                    }
                    else{
                        self.tblListView.hideLoading()
                    }
                    self.isNewDataLoading = false
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        
                        print("view contents : \(jsonData)")
                        
                        if let response = jsonData["data"].array{
                            
                            print("Json data reponse contents : \(response.count) Offset = \(self.offset) Limit = \(self.limit)")
                            
                            if response.count == 0{
                                self.stopPagination = true
                            }
                            self.ArrayPosts.append(contentsOf: response)
                            
                            if self.offset != 0{
                                if response.count > 0 {
                                    self.insertRowsAtTableView(responseCount: response.count)
                                }
                            }
                            if self.ArrayPosts.count > 0 {
                                self.tblListView.isHidden = false
                                
                                if self.offset == 0{
                                    self.tblListView.delegate = self
                                    self.tblListView.dataSource = self
                                    self.tblListView.reloadData()
                                }
                                if self.offset == 0{
                                    let topIndex = IndexPath(row: 0, section: 0)
                                    self.tblListView.scrollToRow(at: topIndex, at: .top, animated: false)
                                }
                                //                                if self.offset <= self.limit{
                                //                                    let topIndex = IndexPath(row: 0, section: 0)
                                //                                    self.tblListView.scrollToRow(at: topIndex, at: .top, animated: false)
                                //                                }
                                
                                //                                if self.offset == 0{
                                //                                    let range = NSMakeRange(0, self.tblListView.numberOfSections)
                                //                                    let sections = NSIndexSet(indexesIn: range)
                                //                                    self.tblListView.reloadSections(sections as IndexSet, with: .left)
                                //                                }
                                //                                else{
                                //
                                //                                }
                                
                                //           let topIndex = IndexPath(row: 0, section: 0)
                                //           self.tblListView.scrollToRow(at: topIndex, at: .top, animated: false)
                            }
                            else{
                                self.tblListView.isHidden = true
                                if self.txtSearch.text!.count > 0{
                                    self.noPostSearch.isHidden = false
                                }
                                else{
                                    self.noPostView.isHidden = false
                                }
                            }
                            
                            //                            if response.count < self.limit {
                            //                            }else{
                            //                                self.offset =  self.ArrayPosts.count + 1
                            //                                self.getPosts()
                            //                            }
                            
                        }
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getpublicfeed()
                        }
                    }
                }catch _ {
                    if self.offset == 0{
                        ActivityIndicatorView.hiding()
                    }
                    else{
                        self.tblListView.hideLoading()
                    }
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( _):
                if self.offset == 0{
                    ActivityIndicatorView.hiding()
                }
                else{
                    self.tblListView.hideLoading()
                }
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    func getBannerForDisplaying(){
        
        let param = ["user_id" : UserDefaults.standard.value(forKey: "userId") as! String]
        
        networkProvider.request(.GetBanners(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("Banner view\(jsonData)")
                    if response.statusCode == 200{
                        self.bannerArray = jsonData[0]
                        let tempUrl = self.bannerArray["banner_url"]
                        print("BannerURl \(tempUrl[0])")
                        if let strUrl = self.bannerArray["banner_url"][0].string, !strUrl.isEmpty{
                            let temp = strUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                            let urlImg = URL(string: temp)
                            self.bannerImageView.kf.indicatorType = .activity
                            self.bannerImageView.kf.setImage(with: urlImg, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
                        }
                        else{
                            self.bannerImageView.backgroundColor = #colorLiteral(red: 0.4941176471, green: 0.3411764706, blue: 0.7607843137, alpha: 1)
                        }
                        self.bannerView.isHidden = false
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getBannerForDisplaying()
                        }
                    }
                    
                }catch _ {
                    
                    
                }
            case .failure( _):
                break
            }
        }
    }
    
    // Open App Link
    func openAppLink(url:String){
        
        let param = ["url" : url]
        
        networkProvider.request(.openAppGetParams(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("@@##!!! REsult\(jsonData)")
                    if response.statusCode == 200{
                        let dataArr = jsonData["data"]
                        let subType = dataArr["sub_type"].stringValue
                        let type =  dataArr["type"].stringValue
                        
                        if subType == notificationSubTypes.publicfeed || subType == notificationSubTypes.familyfeed && type == notificationTypes.home{
                            let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
                            vc.postId = "\(dataArr["type_id"].intValue)"
                            self.navigationController?.pushViewController(vc, animated: false)
                            
                        }
                        else if type == notificationTypes.family{
                            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                            let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
                            intro.groupId = "\(dataArr["type_id"].intValue)"
                            self.navigationController?.pushViewController(intro, animated: false)
                        }
                        else{
                            let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
                            vc.enableEventEdit = false
                            vc.isFromNotification = "true"
                            vc.eventId = "\(dataArr["type_id"].intValue)"
                            self.navigationController?.pushViewController(vc, animated: false)
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.openAppLink(url: url)
                        }
                    }
                    else{
                        
                    }
                    
                }catch _ {
                    
                    
                }
            case .failure( _):
                break
            }
        }
    }
    
    //MARK:- Custom Methods
    func deletePostForCreator(tag:Int){
        let alert = UIAlertController(title: "Familheey", message: "Do you really want to delete this post? Deleting this post will delete it from everywhere you posted it to and you will lose all associated conversations", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "DELETE", style: .destructive, handler: { _ in
            let post = self.ArrayPosts[tag]
            
            
            let param = ["id" : post["post_id"].stringValue,"user_id":UserDefaults.standard.value(forKey: "userId") as! String]
            
            ActivityIndicatorView.show("Please wait....")
            self.networkProvider.request(.deletePost(parameter: param)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                        print("view contents : \(jsonData)")
                        if response.statusCode == 200{
                            print("view contents : \(jsonData)")
                            self.offset = 0
                            if self.selectedIndex == 1{
                                self.getpublicfeed()
                            }
                            else if self.selectedIndex == 0{
                                self.getPosts()
                            }
                            else{
                                self.getrequests()
                            }
                            //                            if tag < self.ArrayPosts.count{
                            //                                self.ArrayPosts.remove(at: tag)
                            //                                self.tblListView.deleteRows(at: [IndexPath.init(row: tag, section: 0)], with: .automatic)
                            //                            }
                        }
                        else if response.statusCode == 401 {
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                self.deletePostForCreator(tag: tag)
                            }
                        }
                        
                        ActivityIndicatorView.hiding()
                        
                    }catch _ {
                        ActivityIndicatorView.hiding()
                        // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                    }
                case .failure( _):
                    ActivityIndicatorView.hiding()
                    //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                    break
                }
            }
            
        }))
        alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func removePostFromTimeline(tag:Int){
        let post = ArrayPosts[tag]
        
        let param = ["post_id" : post["post_id"].stringValue, "user_id": UserDefaults.standard.value(forKey: "userId") as! String]
        
        ActivityIndicatorView.show("Please wait....")
        networkProvider.request(.removePost(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        print("view contents : \(jsonData)")
                        //                        if self.selectedIndex == 1{
                        //                            self.getpublicfeed()
                        //                        }
                        //                        else{
                        //                            self.getPosts()
                        //                        }
                        if tag < self.ArrayPosts.count{
                            self.ArrayPosts.remove(at: tag)
                            //                            self.tblListView.deleteRows(at: [IndexPath.init(row: tag, section: 0)], with: .automatic)
                            self.tblListView.reloadData()
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.removePostFromTimeline(tag: tag)
                        }
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch _ {
                    ActivityIndicatorView.hiding()
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( _):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    func muteConversation(tag:Int){
        let post = ArrayPosts[tag]
        let param = ["post_id" : post["post_id"].stringValue, "user_id": UserDefaults.standard.value(forKey: "userId") as! String]
        
        ActivityIndicatorView.show("Please wait....")
        networkProvider.request(.mute_conversation(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        print("view contents : \(jsonData)")
                        let temp = jsonData["data"].dictionaryValue
                        if temp["is_active"]!.boolValue{
                            var temp = post
                            temp["muted"] = true
                            self.ArrayPosts[tag] = temp
                            print("#######NEW ARR - \(self.ArrayPosts[tag])")
                        }
                        else{
                            var temp = post
                            temp["muted"] = false
                            self.ArrayPosts[tag] = temp
                            print("#######NEW ARR - \(self.ArrayPosts[tag])")
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.muteConversation(tag: tag)
                        }
                    }
                    
                    ActivityIndicatorView.hiding()
                }catch _ {
                    ActivityIndicatorView.hiding()
                    //  Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( _):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    @objc func childNodeAdded(notification: Notification) {
        self.NotificationArrayListing()
    }
    
    func viewCurrentPost(index:Int){
        
        if  ArrayPosts.count > index  {
            
            let post = ArrayPosts[index]
            
            let param = ["post_id" : post["post_id"].stringValue, "user_id": UserDefaults.standard.value(forKey: "userId") as! String]
            
            //               ActivityIndicatorView.show("Please wait....")
            networkProvider.request(.add_view_count(parameter: param)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                        print("view contents : \(jsonData)")
                        if response.statusCode == 200{
                            print("view contents : \(jsonData)")
                        }
                        else if response.statusCode == 401 {
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                self.viewCurrentPost(index: index)
                            }
                        }
                        
                        
                        //                           ActivityIndicatorView.hiding()
                        
                    }catch _ {
                        //                           ActivityIndicatorView.hiding()
                        // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                    }
                case .failure( _):
                    //                       ActivityIndicatorView.hiding()
                    //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                    break
                }
            }
        }
    }
    
    func NotificationArrayListing(){
        
        
        let userId = UserDefaults.standard.value(forKey: "userId") as! String
        
        let noti_String :String = "\(Helpers.notificationUploadstring)\(userId)_notification"
        let ref = Database.database().reference(withPath: noti_String).queryLimited(toLast: 1200)
        ref.observeSingleEvent(of: .value, with: { snapshot in
            
            if !snapshot.exists() {
                self.arrayOfNotificationList = []
                appDel.arrayOfNotificationList = []
                appDel.tempNotifi = []
                return
                
            }
            self.arrayOfNotificationList = []
            appDel.arrayOfNotificationList = []
            appDel.tempNotifi = []
            
            let dict:[String:Any] = snapshot.value as! [String:Any]
            for (key, value) in dict {
                var dictionary = [String:Any]()
                dictionary["key"] = key
                dictionary["value"] = value as! [String:Any]
                appDel.arrayOfNotificationList.append(dictionary)
                appDel.tempNotifi.append(value as! [String:Any])
            }
            
            self.arrayOfNotificationList = appDel.arrayOfNotificationList
            
            self.notiUnreadCount = 0
            for data in self.arrayOfNotificationList{
                let dict  = data["value"] as! [String:Any]
                if dict["visible_status"] as! String == "unread"{
                    self.notiUnreadCount = self.notiUnreadCount + 1
                }
            }
            
            if self.notiUnreadCount == 0{
                self.lblNoti_Count.isHidden = true
                self.viewOfNoti_Count.isHidden = true
            }
            else{
                self.lblNoti_Count.isHidden = false
                self.viewOfNoti_Count.isHidden = false
                if self.notiUnreadCount > 99{
                    self.lblNoti_Count.text = "99+"
                }
                else{
                    self.lblNoti_Count.text = "\(self.notiUnreadCount)"
                }
            }
        })
    }
    
    
    func sendFeedBack(){
        let stryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = stryboard.instantiateViewController(withIdentifier: "FeedbackViewController") as! FeedbackViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func viewAnnouncements(){
        let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsViewController") as! AnnouncementsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func viewCalendar(){
        let stryboard = UIStoryboard.init(name: "third", bundle: nil)
        let vc = stryboard.instantiateViewController(withIdentifier: "CalendarEventsViewController") as! CalendarEventsViewController
        vc.isFromExplore = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func viewSubscription(){
        let stryboard = UIStoryboard.init(name: "Subscription", bundle: nil)
        let vc = stryboard.instantiateViewController(withIdentifier: "WalkthorughViewController") as! WalkthorughViewController
        appDel.quickTour = true
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func viewMessages(){
        let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "topicListingViewController") as! topicListingViewController
        
        let rightMenuNavigationController = SideMenuNavigationController(rootViewController: vc)
        SideMenuManager.default.leftMenuNavigationController = rightMenuNavigationController
        SideMenuManager.default.addPanGestureToPresent(toView: self.view)
        SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: self.view)
        rightMenuNavigationController.statusBarEndAlpha = 0
        rightMenuNavigationController.menuWidth = self.view.frame.width
        rightMenuNavigationController.dismissOnPush = false
        rightMenuNavigationController.dismissOnPresent = false
        rightMenuNavigationController.pushStyle = .subMenu
        present(rightMenuNavigationController, animated: true, completion: nil)
    }
    
    func getUserDetails(){
        let crntUser = UserDefaults.standard.value(forKey: "userId") as! String
        
        APIServiceManager.callServer.getUserDetails(url: EndPoint.userDetails, userId: crntUser, profileId: crntUser ,success: { (response) in
            
            guard let loginMdl = response as? ExternalUserModel else{
                return
            }
            
            
            UserDefaults.standard.set(loginMdl.User?.fullname, forKey: "user_fullname")
            
            if loginMdl.User?.propic.count != 0 {
                UserDefaults.standard.set(loginMdl.User?.propic, forKey: "userProfile")
                let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+(loginMdl.User!.propic).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                let imgUrl = URL(string: temp)
                
                let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+imgUrl!.relativeString
                let urlImg = URL(string: newUrlStr)
                self.imgProfile.kf.indicatorType = .activity
                self.imgProfile.kf.setImage(with: urlImg, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
            }
            else{
                self.imgProfile.image = #imageLiteral(resourceName: "Male Colored")
            }
            
            
            
        }) { (error) in
            //            ActivityIndicatorView.hiding()
            
        }
        
    }
    
    func openSearch(){
        notificationView.isHidden = false
        self.notificationHeight.constant = self.headerHeight
        self.searchview.isHidden = false
        self.searchConstraintheight.constant = self.headerHeight
        if self.announcementCountExists{
            self.viewAnnouncement.isHidden = true
        }
        self.btnResetSearch.isHidden = false
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
            self.searchview.alpha = 1.0
            
        }, completion: nil)
    }
    func closeSearch(){
        DispatchQueue.main.async {
            
            UIView.animate(withDuration: 0.5, animations: {
                self.searchview.alpha = 0.0
                
            }) { (true) in
                
                self.searchview.isHidden = true
                self.searchConstraintheight.constant = 0
                self.notificationView.isHidden = false
                if self.announcementCountExists{
                    self.viewAnnouncement.isHidden = false
                }
                self.notificationHeight.constant = self.headerHeight
            }
        }
        
        
    }
    
    
    //MARK:- Custom Delegate
    func getButtonClicked(index: Int) {
        if index == 0{
            //            self.tabBarController?.selectedIndex = 3
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SearchTabViewController") as! SearchTabViewController
            //        vc.arrayOfNotificationList = self.arrayOfNotificationList
            vc.tabIndex = 100
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if index == 1{
            let storyBoard = UIStoryboard.init(name: "CreateFamily", bundle: nil)
            let addFamily = storyBoard.instantiateViewController(withIdentifier: "addFamilyOne") as! AddFamilyScreenOneViewController
            //  loginView.regType = regType
            self.navigationController?.pushViewController(addFamily, animated: true)
        }
    }
    
    //MARK:- UIButton Actions
    
    @IBAction func bannerImageAction( sender:Any){
        if let subtype = bannerArray["link_subtype"].string, !subtype.isEmpty{
            if subtype.lowercased() == "request" && bannerArray["link_type"].stringValue.lowercased() == "home"{
                let btn = UIButton()
                btn.tag = 102
                self.onClickTabSelection(btn)
            }
        }
        else{
            if bannerArray["link_type"].stringValue.lowercased() == "family"{
                if let grpId = bannerArray["link_id"].int, grpId != 0{
                    let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                    let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
                    intro.groupId = "\(grpId)"
                    intro.isFromBanner = true
                    self.navigationController?.pushViewController(intro, animated: true)
                }
            }
        }
    }
    
    @IBAction func searchAction(_ sender: Any) {
        
        openSearch()
        
    }
    
    @IBAction func onClickFamilyName_NormalPost(_ sender: UIButton) {
        print("click normal family")
        
        var grpId = ""
        
        let post = ArrayPosts[sender.tag]
        
        
        
        grpId   = post["to_group_id"].stringValue
        
        //               grpId = "\(FamilyArr.familyList![sender.tag].faId)"
        
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
        intro.groupId = grpId
        self.navigationController?.pushViewController(intro, animated: true)
        
        
    }
    
    
    
    @IBAction func onClickFamilyName_SharedPost(_ sender: UIButton) {
        
        print("click shared family")
        
        var grpId = ""
        
        let post = ArrayPosts[sender.tag]
        grpId   = post["to_group_id"].stringValue
        
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
        intro.groupId = grpId
        self.navigationController?.pushViewController(intro, animated: true)
        
        
    }
    
    @IBAction func onClickReadmoreAction(_ sender: UIButton) {
        
        let cell = tblListView.cellForRow(at: IndexPath.init(row: sender.tag, section: 0)) as! PostImageTableViewCell
        
        if cell.labelPost.calculateMaxLines() > maxNumberOfLines &&  cell.labelPost.numberOfLines == maxNumberOfLines{
            
            tblListView.beginUpdates()
            cell.labelPost.numberOfLines = 0
            cell.readmoreButton.setTitle("view less", for: .normal)
            tblListView.endUpdates()
            
            
        }
        else if cell.labelPost.calculateMaxLines() > maxNumberOfLines &&  cell.labelPost.numberOfLines == 0 {
            UIView.performWithoutAnimation {
                tblListView.beginUpdates()
                cell.labelPost.numberOfLines = maxNumberOfLines
                cell.readmoreButton.setTitle("view more", for: .normal)
                tblListView.endUpdates()
            }
        }
        
    }
    
    @IBAction func onClickReadmoreShareAction(_ sender: UIButton) {
        
        let cell = tblListView.cellForRow(at: IndexPath.init(row: sender.tag, section: 0)) as! SharedPostTableViewCell
        if cell.labelPost.calculateMaxLines() > maxNumberOfLines &&  cell.labelPost.numberOfLines == maxNumberOfLines{
            
            tblListView.beginUpdates()
            cell.labelPost.numberOfLines = 0
            cell.readmoreButton.setTitle("view less", for: .normal)
            tblListView.endUpdates()
            
            
        }
        else if cell.labelPost.calculateMaxLines() > maxNumberOfLines &&  cell.labelPost.numberOfLines == 0 {
            UIView.performWithoutAnimation {
                tblListView.beginUpdates()
                cell.labelPost.numberOfLines = maxNumberOfLines
                cell.readmoreButton.setTitle("view more", for: .normal)
                tblListView.endUpdates()
            }
        }
        
    }
    
    @IBAction func onClickThanksReadMore(_ sender: UIButton) {
        let cell = tblListView.cellForRow(at: IndexPath.init(row: sender.tag, section: 0)) as! PostThanksTableViewCell
        
        if cell.labelPost.calculateMaxLines() > maxNumberOfLines &&  cell.labelPost.numberOfLines == maxNumberOfLines{
            
            tblListView.beginUpdates()
            cell.labelPost.numberOfLines = 0
            cell.readmoreButton.setTitle("view less", for: .normal)
            tblListView.endUpdates()
            
            
        }
        else if cell.labelPost.calculateMaxLines() > maxNumberOfLines &&  cell.labelPost.numberOfLines == 0 {
            UIView.performWithoutAnimation {
                tblListView.beginUpdates()
                cell.labelPost.numberOfLines = maxNumberOfLines
                cell.readmoreButton.setTitle("view more", for: .normal)
                tblListView.endUpdates()
            }
        }
    }
    
    
    @IBAction func onClickSharedUsersShareAction(_ sender: UIButton) {
        
        /* let post = ArrayPosts[sender.tag]
         
         if let sharedusers = post["shared_user_names"].string , !sharedusers.isEmpty{
         
         let array = sharedusers.components(separatedBy: ",")
         
         showActionSheet(titleArr: array as NSArray, title: "Select option", completion: { (index) in
         
         })
         
         }*/
        let post = ArrayPosts[sender.tag]
        let mainStoryBoard = UIStoryboard(name: "fourth", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "UserFamilyListViewController") as! UserFamilyListViewController
        if post["orgin_id"].stringValue.count > 0{
            vc.postId = post["orgin_id"].stringValue
        }
        else{
            vc.postId = post["post_id"].stringValue
        }
        vc.isFrompost = true
        vc.isFrom = "groupshare"
        vc.familyID = post["to_group_id"].stringValue
        self.navigationController?.pushViewController(vc, animated:true)
    }
    
    @IBAction func onClickShareSharedPostAction(_ sender: UIButton) {
        
        /* let post = ArrayPosts[sender.tag]
         
         let mainStoryBoard = UIStoryboard(name: "third", bundle: nil)
         let vc = mainStoryBoard.instantiateViewController(withIdentifier: "PostShareViewController") as! PostShareViewController
         vc.postId = post["post_id"].stringValue
         self.navigationController?.pushViewController(vc, animated:true)*/
        self.ShareOptionArray = ["Share in Familheey","Share in Social Media"]
        showActionSheet(titleArr: ShareOptionArray as NSArray, title: "Select options") { (index) in
            if index == 0{
                let post = self.ArrayPosts[sender.tag]
                print(post)
                //                 DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                let mainStoryBoard = UIStoryboard(name: "third", bundle: nil)
                let vc = mainStoryBoard.instantiateViewController(withIdentifier: "PostShareViewController") as! PostShareViewController
                vc.postId = post["post_id"].stringValue
                self.navigationController?.pushViewController(vc, animated: false)
                //                }
                //                 self.navigationController?.pushViewController(vc, animated: false)
            }
            else if index == 1{
                let post = self.ArrayPosts[sender.tag]
                _ =
                    Helpers.showActivityViewcontroller(url: "\(BaseUrl.PostUrlPrd)\(post["post_id"].stringValue)", VC: self)
            }
            else{
                
            }
        }
    }
    
    @IBAction func onClickCommentsShareAction(_ sender: UIButton) {
        
        _ = ArrayPosts[sender.tag]
        let mainStoryBoard = UIStoryboard(name: "Conversations", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "ConversationsViewController") as! ConversationsViewController
        //               vc.post = post
        self.navigationController?.pushViewController(vc, animated:true)
    }
    
    @IBAction func onClickViewsAction(_ sender: UIButton) {
        let post = ArrayPosts[sender.tag]
        if let count =  post["views_count"].string, Int(count)! > 0{
            let mainStoryBoard = UIStoryboard(name: "fourth", bundle: nil)
            let vc = mainStoryBoard.instantiateViewController(withIdentifier: "UserFamilyListViewController") as! UserFamilyListViewController
            vc.postId = post["post_id"].stringValue
            vc.isFrompost = true
            vc.isFrom = "postview"
            self.navigationController?.pushViewController(vc, animated:true)
        }
        else{
            self.displayAlertChoice(alertStr: "Sorry, nobody viewed your post. Please check after sometime.", title: "") { (result) in
                
            }
        }
        
    }
    
    @IBAction func onClickSharedUsersAction(_ sender: UIButton) {
        let post = ArrayPosts[sender.tag]
        let mainStoryBoard = UIStoryboard(name: "fourth", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "UserFamilyListViewController") as! UserFamilyListViewController
        if post["orgin_id"].stringValue.count > 0{
            vc.postId = post["orgin_id"].stringValue
        }
        else{
            vc.postId = post["post_id"].stringValue
        }
        vc.isFrompost = true
        vc.isFrom = "usershare"
        self.navigationController?.pushViewController(vc, animated:true)
    }
    
    @IBAction func onClickCommentAction(_ sender: UIButton) {
        
        var post = ArrayPosts[sender.tag]
        post["conversation_count_new"] = "0"
        if  let cell = tblListView.cellForRow(at: IndexPath.init(row: sender.tag, section: 0)) as? SharedPostTableViewCell{
            
            tblListView.beginUpdates()
            cell.lblNewConverstn.isHidden = true
            tblListView.endUpdates()
        }
        else if  let cell = tblListView.cellForRow(at: IndexPath.init(row: sender.tag, section: 0)) as? PostImageTableViewCell{
            tblListView.beginUpdates()
            cell.lblNewConvrstn.isHidden = true
            tblListView.endUpdates()
        }
        
        let mainStoryBoard = UIStoryboard(name: "Conversations", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "ConversationsViewController") as! ConversationsViewController
        vc.post = post
        if selectedIndex == 0{
            vc.conversationT = .post
        }
        else if selectedIndex == 1{
            vc.conversationT = .publicpost
        }
        self.navigationController?.pushViewController(vc, animated:true)
    }
    
    @IBAction func onClickProfileAction(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "UserProfileTableViewController") as! UserProfileTableViewController
        
        let userId = UserDefaults.standard.value(forKey: "userId") as! String
        vc.userID = userId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onClickNotification(_ sender: Any) {
        self.lblNoti_Count.isHidden = true
        self.viewOfNoti_Count.isHidden = true
        //        appDel.firstTimeNot = true
        let storyboard = UIStoryboard.init(name: "third", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        vc.arrayOfNotificationList = self.arrayOfNotificationList
        self.navigationController?.pushViewController(vc, animated: true)
        
        //        let storyboard = UIStoryboard.init(name: "Contacts", bundle: nil)
        //        let vc = storyboard.instantiateViewController(withIdentifier: "CollageMakeViewController") as! CollageMakeViewController
        ////        vc.arrayOfNotificationList = self.arrayOfNotificationList
        //        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func onClickFeedback(_ sender: Any) {
        postoptionsTittle.removeAll()
        //        postoptionsTittle =  ["Send feedback","View announcement","Calendar"]
        postoptionsTittle =  ["Calendar","Announcements","Messages","Feedback","Quick tour"]
        
        
        showActionSheet(titleArr: postoptionsTittle as NSArray, title: "Select options") { (index) in
            if index == 0{
                self.viewCalendar()
            }
            else if index == 1{
                self.viewAnnouncements()
            }
            else if index == 2{
                self.viewMessages()
            }
            else if index == 100{
            }
            else if index == 3{
                self.sendFeedBack()
            }
            else if index == 4{
                self.viewSubscription()
            }
            else{
            }
        }
    }
    
    @IBAction func onClickSearchReset(_ sender: Any) {
        txtSearch.text = ""
        searchTxt = ""
        self.offset = 0
        if self.selectedIndex == 1{
            self.getpublicfeed()
        }
        else if selectedIndex == 0{
            self.getPosts()
        }
        else{
            self.getrequests()
        }
        closeSearch()
        //        btnResetSearch.isHidden = true
        
        self.txtSearch.endEditing(true)
    }
    @IBAction func onClickPostMenu(_ sender: UIButton) {
        let post = ArrayPosts[sender.tag]
        
        postoptionsTittle.removeAll()
        
        if let postType = post["publish_type"].string, postType.lowercased() == "request" || postType.lowercased() == "albums" || postType.lowercased() == "documents" {
            if post["created_by"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")" {
                
            }
            else{
                postoptionsTittle =  ["Hide","Report"]
            }
            
            if post["muted"].stringValue == "true"{
                postoptionsTittle.insert("Unmute", at: 0)
            }
            else{
                postoptionsTittle.insert("Mute", at: 0)
            }
            showActionSheet(titleArr: postoptionsTittle as NSArray, title: "Select options") { (index) in
                
                if index == 2{
                    if post["created_by"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")"{
                        //                        self.deletePostForCreator(tag: sender.tag)
                    }
                    else
                    {
                        let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "ReportPostViewController") as! ReportPostViewController
                        
                        vc.postDetails =  self.ArrayPosts[sender.tag]
                        vc.isFrom = "post"
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                    }
                }
                else if index == 1{
                    if post["created_by"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")"{
                        /*if let sharedusers = post["shared_user_name"].string , !sharedusers.isEmpty{
                         self.deletePostForCreator(tag: sender.tag)
                         }
                         else{
                         let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
                         let vc = storyboard.instantiateViewController(withIdentifier: "CreatePostViewController") as! CreatePostViewController
                         
                         vc.fromEdit = true
                         vc.editPostDetails = post
                         self.navigationController?.pushViewController(vc, animated: true)
                         }*/
                    }else{
                        self.removePostFromTimeline(tag: sender.tag)
                    }
                }
                else if index == 0{
                    self.muteConversation(tag: sender.tag)
                }
                else if index == 100{
                }
                    //                  else if index == 2{
                    //                      Helpers.showActivityViewcontroller(url: "http://www.sampleurl.com", VC: self)
                    //                  }
                else{
                    
                }
            }
        }
        else{
            if post["created_by"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")" {
                //                  postoptionsTittle =  ["Edit post","Share social media","Delete post"]
                if let sharedusers = post["shared_user_name"].string , !sharedusers.isEmpty{
                    postoptionsTittle =  ["Delete"]
                }
                else{
                    postoptionsTittle =  ["Edit","Delete"]
                }
            }
            else{
                //                  postoptionsTittle =  ["Remove Post","Share social media","Report"]
                postoptionsTittle =  ["Hide","Report"]
                
            }
            
            if post["muted"].stringValue == "true"{
                postoptionsTittle.insert("Unmute", at: 0)
            }
            else{
                postoptionsTittle.insert("Mute", at: 0)
            }
            
            showActionSheet(titleArr: postoptionsTittle as NSArray, title: "Select options") { (index) in
                
                if index == 2{
                    if post["created_by"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")"{
                        self.deletePostForCreator(tag: sender.tag)
                    }
                    else
                    {
                        let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "ReportPostViewController") as! ReportPostViewController
                        
                        vc.postDetails =  self.ArrayPosts[sender.tag]
                        vc.isFrom = "post"
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                    }
                }
                else if index == 1{
                    if post["created_by"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")"{
                        if let sharedusers = post["shared_user_name"].string , !sharedusers.isEmpty{
                            self.deletePostForCreator(tag: sender.tag)
                        }
                        else{
                            let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "CreatePostViewController") as! CreatePostViewController
                            
                            vc.fromEdit = true
                            vc.editPostDetails = post
                            vc.callBackDelegate = self
                            vc.selectedTag = sender.tag
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }else{
                        self.removePostFromTimeline(tag: sender.tag)
                    }
                }
                else if index == 100{
                }
                    //                  else if index == 2{
                    //                      Helpers.showActivityViewcontroller(url: "http://www.sampleurl.com", VC: self)
                    //                  }
                else{
                    self.muteConversation(tag: sender.tag)
                }
            }
        }
    }
    
    func postupdateSuccess(index:Int , SuccessData:JSON)
    {
        ArrayPosts[index] = SuccessData
        tblListView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
        
    }
    
    @IBAction func onClickShareAction(_ sender: UIButton) {
        self.ShareOptionArray = ["Share in Familheey","Share in Social Media"]
        
        showActionSheet(titleArr: ShareOptionArray as NSArray, title: "Select options") { (index) in
            if index == 0{
                let post = self.ArrayPosts[sender.tag]
                print(post)
                
                let mainStoryBoard = UIStoryboard(name: "third", bundle: nil)
                let vc = mainStoryBoard.instantiateViewController(withIdentifier: "PostShareViewController") as! PostShareViewController
                vc.postId = post["post_id"].stringValue
                self.navigationController?.pushViewController(vc, animated:true)
            }
            else if index == 1{
                let post = self.ArrayPosts[sender.tag]
                _ =
                    Helpers.showActivityViewcontroller(url: "\(BaseUrl.PostUrlPrd)\(post["post_id"].stringValue)", VC: self)
                
                
                //                     Helpers.showActivityViewcontroller(url: "http://www.sampleurl.com", VC: self)
            }
            else{
                
            }
        }
    }
    @IBAction func onClickTabSelection(_ sender: UIButton){
        if sender.tag == 100{
            self.selectedIndex = 0
            offset = 0
            updateTabUi()
            ArrayPosts.removeAll()
            if txtSearch.text! == ""{
                closeSearch()
            }
            getPosts()
            
        }
        else if sender.tag == 102{
            self.selectedIndex = 2
            offset = 0
            ArrayPosts.removeAll()
            if txtSearch.text! == ""{
                closeSearch()
            }
            updateTabUi()
            getrequests()
            
        }
        else{
            self.selectedIndex = 1
            offset = 0
            updateTabUi()
            ArrayPosts.removeAll()
            if txtSearch.text! == ""{
                closeSearch()
            }
            getpublicfeed()
            
        }
    }
    
    @IBAction func onclickOpenAnnouncementAction(_ sender: Any) {
        
        viewAnnouncement.isHidden = true
        let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsViewController") as! AnnouncementsViewController
        vc.isFromHome = true
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    @IBAction func onClickNoFamilySearch(_ sender: Any) {
        self.tabBarController?.selectedIndex = 3
    }
    @IBAction func onClickCloseAnnouncementAction(_ sender: Any) {
        
        viewAnnouncement.isHidden = true
        self.updateAnnouncementSeen()
        //        let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
        //        let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsViewController") as! AnnouncementsViewController
        //        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func onClickOpenRequest(_ sender: UIButton) {
        let need = ArrayPosts[sender.tag]
        if let postType = need["publish_type"].string, postType.lowercased() == "request"{
            let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "RequestDetailsViewController") as! RequestDetailsViewController
            vc.requestId = need["publish_id"].stringValue
            //        vc.requestDic = need
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            let storyboard = UIStoryboard.init(name: "third", bundle: nil)
            /* let addMember = storyboard.instantiateViewController(withIdentifier: "AlbumListingViewController") as! AlbumListingViewController
             addMember.groupID = need["to_group_id"].stringValue
             addMember.isFromPost = true
             addMember.isFromdocument = false
             addMember.albumId = need["publish_id"].stringValue
             
             
             self.navigationController?.pushViewController(addMember, animated: false)*/
            let AlbumDetailsViewController = storyboard.instantiateViewController(withIdentifier: "AlbumDetailsViewController") as! AlbumDetailsViewController
            //            AlbumDetailsViewController.albumDetailsDict = items
            AlbumDetailsViewController.isFrom = "groups"
            AlbumDetailsViewController.isFromPost = true
            AlbumDetailsViewController.groupId = need["to_group_id"].stringValue
            if let postType = need["publish_type"].string, postType.lowercased() == "albums"{
                AlbumDetailsViewController.folder_type = "albums"
            }
            else{
                AlbumDetailsViewController.folder_type = "documents"
                AlbumDetailsViewController.isFromDocs = true
            }
            
            AlbumDetailsViewController.createdBy = need["created_by"].stringValue
            AlbumDetailsViewController.albumId = need["publish_id"].stringValue
            self.navigationController?.pushViewController(AlbumDetailsViewController, animated: true)
        }
        
    }
    
    static func dateFormatterCommon(dateStr:String) -> String{
        
        if !dateStr.isEmpty{
            let dateFormatter              = DateFormatter()
            dateFormatter.dateFormat       = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
            let date = dateFormatter.date(from: dateStr)
            dateFormatter.dateFormat       = "MMM dd yyyy hh:mm a"
            dateFormatter.timeZone = NSTimeZone.local
            if date != nil{
                return dateFormatter.string(from: date!)
            }
            else{
                return ""
            }
            
            //                return date?.timeAgoSinceDate() ?? ""
        }
        else{
            return ""
        }
    }
    
    //update tab ui
    
    func updateTabUi(){
        if selectedIndex == 0{
            btnPublicFeed.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            imgPublicFee.backgroundColor  = UIColor(named: "lightGrayTextColor")
            
            btnMyFamilheey.setTitleColor(UIColor(named: "greenBackgrounf"), for: .normal)
            imgMyFamilheey.backgroundColor  = UIColor(named: "greenBackgrounf")
            
            imgRequests.backgroundColor =  UIColor(named: "lightGrayTextColor")
            buttonRequests.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
        }
        else if selectedIndex == 1{
            btnMyFamilheey.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            imgMyFamilheey.backgroundColor  = UIColor(named: "lightGrayTextColor")
            
            imgPublicFee.backgroundColor  = UIColor(named: "greenBackgrounf")
            btnPublicFeed.setTitleColor(UIColor(named: "greenBackgrounf"), for: .normal)
            
            imgRequests.backgroundColor =  UIColor(named: "lightGrayTextColor")
            buttonRequests.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
        }
        else if selectedIndex == 2{
            imgRequests.backgroundColor =  UIColor(named: "greenBackgrounf")
            buttonRequests.setTitleColor(UIColor(named: "greenBackgrounf"), for: .normal)
            
            btnPublicFeed.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            imgPublicFee.backgroundColor  = UIColor(named: "lightGrayTextColor")
            
            btnMyFamilheey.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            imgMyFamilheey.backgroundColor  = UIColor(named: "lightGrayTextColor")
        }
    }
    
    // MARK: - Location and user history
    
    let locationManager = CLLocationManager()
    var currentLocation : String!
    
    
    //MARK: - location delegate methods
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        
        guard let location: CLLocation = manager.location else { return }
        fetchCityAndCountry(from: location) { city, country, error in
            guard let city = city, let country = country, error == nil else { return }
            //            print(city + ", " + country)
            self.currentLocation = "\(city) , \(country)"
            self.locationManager.stopUpdatingLocation()
            self.getUserUpdateDetails()
            
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        //        print("Error \(error)")
    }
    
    
    func fetchCityAndCountry(from location: CLLocation, completion: @escaping (_ city: String?, _ country:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            completion(placemarks?.first?.locality,
                       placemarks?.first?.country,
                       error)
            
        }
    }
    
    
    
    func getUserUpdateDetails() {
        
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        //        print(appVersion!)
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        //        print(deviceID)
        
        let parameter = ["user_id":UserDefaults.standard.value(forKey: "userId") as! String,"login_type":"iOS","login_device":UIDevice.current.name,"app_version":appVersion!,"deviceID":deviceID,"login_location":self.currentLocation!]
        //        print(parameter)
        
        networkProvider.request(.addUserHistory(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    if response.statusCode == 200{
                        do {
                            // make sure this JSON is in the format we expect
                            if let json = try JSONSerialization.jsonObject(with: response.data, options: []) as? [String: Any] {
                                // try to read out a string array
                                //                            print(json)
                                
                                appDel.isUserHistoryAdded = true
                                //
                                
                            }
                        } catch let error as NSError {
                            //                        print("Failed to load: \(error.localizedDescription)")
                        }
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getUserUpdateDetails()
                        }
                    }
                    
                } catch let err {
                    
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
        })
    }
    
    
}

extension HomeTabViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        //        if textField == txtSearch
        //        {
        //            // selectWebAPI()
        //            if textField.text!.count > 0{
        //                btnResetSearch.isHidden = false
        //            }
        //            else{
        //                btnResetSearch.isHidden = true
        //            }
        textField.endEditing(true)
        //        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        self.view.endEditing(true)
        if textField == txtSearch
        {
            //            if textField.text!.count > 0{
            //                btnResetSearch.isHidden = false
            //                if textField.text!.trimmingCharacters(in: .whitespaces).isEmpty {
            //                                  txtSearch.text = ""
            //                    btnResetSearch.isHidden = true
            //
            //                                    }
            //            }
            //            else{
            //                btnResetSearch.isHidden = true
            //            }
            self.offset = 0
            if self.selectedIndex == 1{
                self.getpublicfeed()
            }
            else if selectedIndex == 0{
                self.getPosts()
            }
            else{
                self.getrequests()
            }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //        if textField.text!.count > 0 {
        //            btnResetSearch.isHidden = false
        //        }
        //        else{
        //            btnResetSearch.isHidden = true
        //            // selectWebAPI()
        //        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //        if textField.text!.count > 0 {
        //            btnResetSearch.isHidden = false
        //        }
        //        else{
        //            btnResetSearch.isHidden = true
        //            // selectWebAPI()
        //        }
        return true
    }
}

extension HomeTabViewController : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    //    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    //          return 20
    //      }
    //
    //      func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    //          let viewTemp = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 20), reuseIdentifier: "cellvew")
    //          let label = UILabel(frame: CGRect(x: 16, y: 0, width: viewTemp.frame.width - 16, height: 20))
    //          if txtSearch.text!.isEmpty{
    //              label.text = "Suggested"
    //          }
    //          else{
    //              label.text = "Search result for \"\(txtSearch.text!)\""
    //          }
    //          label.textColor = .black
    //          viewTemp.addSubview(label)
    //          viewTemp.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.9411764706, blue: 0.9450980392, alpha: 1)
    //          return viewTemp
    //      }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ArrayPosts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        guard ArrayPosts.count != 0 else {
            return UITableViewCell.init()
        }
        
        if self.selectedIndex == 2{
            let need = ArrayPosts[indexPath.row]
            
            let cellid = "NeedsRequestListTableViewCell"
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! NeedsRequestListTableViewCell
            
            cell.buttonMenu.tag = indexPath.row
            cell.buttonViewMore.tag = indexPath.row
            cell.delegate = self
            cell.selectionStyle = .none
            cell.butttongotoDetails.tag = indexPath.row
            
            if need["user_id"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")" {
                
                cell.viewOfThreeDot.isHidden = false
                
            }
            else{
                cell.viewOfThreeDot.isHidden = true
                
            }
            if let items = need["items"].array,items.count > 0{
                if let item = items[0].dictionary{
                    cell.lblNeedOneDesc.text = item["request_item_description"]?.string ?? ""
                    cell.lblNeedOnetitle.text =  item["request_item_title"]?.string ?? ""
                    if let type = need["request_type"].string, type.lowercased() == "fund"{
                        cell.lblNeedOneQuantity.text =  "$\(RequestDetailsViewController.subtact(total:item["item_quantity"]?.int ?? 0, contributed: item["received_amount"]?.int ?? 0)) of $\(item["item_quantity"]?.int ?? 0)"
                    }
                    else{
                        cell.lblNeedOneQuantity.text =  "\(RequestDetailsViewController.subtact(total:item["item_quantity"]?.int ?? 0, contributed: item["received_amount"]?.int ?? 0)) of \(item["item_quantity"]?.int ?? 0)"
                    }
                    
                    
                    if let total = item["received_amount"]?.int, let quantity = item["item_quantity"]?.int{
                        if total >= quantity{
                            cell.lblNeedOneStatus.text = ""
                            cell.lblNeedOneQuantity.text = "Completed"
                            cell.lblNeedOneQuantity.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                        }
                        else{
                            cell.lblNeedOneStatus.text = "Still needed"
                            cell.lblNeedOneQuantity.textColor = .black
                            
                        }
                    }
                    else{
                        cell.lblNeedOneStatus.text = "Still needed"
                        cell.lblNeedOneQuantity.textColor = .black
                        
                    }
                    
                    //                    cell.lblNeedOneStatus.text = "Still needed"
                    
                    cell.needOneView.isHidden = false
                    cell.needThreeView.isHidden = true
                    cell.needTwoView.isHidden = true
                }
                if items.count > 1{
                    if let item = items[1].dictionary{
                        cell.lblNeedTwoDesc.text = item["request_item_description"]?.string ?? ""
                        cell.lblNeedTwotitle.text =  item["request_item_title"]?.string ?? ""
                        if let type = need["request_type"].string, type.lowercased() == "fund"{
                            cell.lblNeedTwoQuantity.text =  "$\(RequestDetailsViewController.subtact(total:item["item_quantity"]?.int ?? 0, contributed: item["received_amount"]?.int ?? 0)) of $\(item["item_quantity"]?.int ?? 0)"
                        }
                        else{
                            cell.lblNeedTwoQuantity.text =  "\(RequestDetailsViewController.subtact(total:item["item_quantity"]?.int ?? 0, contributed: item["received_amount"]?.int ?? 0)) of \(item["item_quantity"]?.int ?? 0)"
                        }
                        
                        if let total = item["received_amount"]?.int, let quantity = item["item_quantity"]?.int{
                            if total >= quantity{
                                cell.lblNeedTwoStatus.text = ""
                                cell.lblNeedTwoQuantity.text = "Completed"
                                cell.lblNeedTwoQuantity.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                            }
                            else{
                                cell.lblNeedTwoStatus.text = "Still needed"
                                cell.lblNeedTwoQuantity.textColor = .black
                                
                            }
                        }
                        else{
                            cell.lblNeedTwoStatus.text = "Still needed"
                            cell.lblNeedTwoQuantity.textColor = .black
                            
                        }
                        
                        //                        cell.lblNeedTwoStatus.text = "Still needed"
                        
                        cell.needThreeView.isHidden = true
                        cell.needTwoView.isHidden = false
                        cell.needOneView.isHidden = false
                        
                    }
                }
                if items.count > 2{
                    if let item = items[2].dictionary{
                        cell.lblNeedThreeDesc.text = item["request_item_description"]?.string ?? ""
                        cell.lblNeedThreetitle.text =  item["request_item_title"]?.string ?? ""
                        if let type = need["request_type"].string, type.lowercased() == "fund"{
                            cell.lblNeedThreeQuantity.text =  "$\(RequestDetailsViewController.subtact(total:item["item_quantity"]?.int ?? 0, contributed: item["received_amount"]?.int ?? 0)) of $\(item["item_quantity"]?.int ?? 0)"
                        }
                        else{
                            cell.lblNeedThreeQuantity.text =  "\(RequestDetailsViewController.subtact(total:item["item_quantity"]?.int ?? 0, contributed: item["received_amount"]?.int ?? 0)) of \(item["item_quantity"]?.int ?? 0)"
                        }
                        
                        if let total = item["received_amount"]?.int, let quantity = item["item_quantity"]?.int{
                            if total >= quantity{
                                cell.lblNeedThreeStatus.text = ""
                                cell.lblNeedThreeQuantity.text = "Completed"
                                cell.lblNeedThreeQuantity.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                            }
                            else{
                                cell.lblNeedThreeStatus.text = "Still needed"
                                cell.lblNeedThreeQuantity.textColor = .black
                                
                            }
                        }
                        else{
                            cell.lblNeedThreeStatus.text = "Still needed"
                            cell.lblNeedThreeQuantity.textColor = .black
                            
                        }
                        
                        //                        cell.lblNeedThreeStatus.text = "Still needed"
                        
                        cell.needThreeView.isHidden = false
                        cell.needTwoView.isHidden = false
                        cell.needOneView.isHidden = false
                        
                    }
                }
            }
            else{
                cell.needOneView.isHidden = true
                cell.needThreeView.isHidden = true
                cell.needTwoView.isHidden = true
            }
            
            if let groups = need["to_groups"].array, groups.count > 0{
                if groups.count == 1{
                    cell.lblpostedInFamilyName.text = "Posted in \(groups[0]["group_name"].string ?? "Unknown")"
                }
                else if groups.count == 2{
                    cell.lblpostedInFamilyName.text = "Posted in \(groups[0]["group_name"].string ?? "Unknown") and 1 other family"
                }
                else{
                    cell.lblpostedInFamilyName.text = "Posted in \(groups[0]["group_name"].string ?? "Unknown") and \(groups.count - 1) other families"
                }
            }
            else{
                cell.lblpostedInFamilyName.text = "Posted in Public"
            }
            cell.lblUserName.text = need["full_name"].string ?? "Unknown"
            cell.lblNeedLocation.text = need["request_location"].string ?? "Unknown"
            cell.lblDate.text = HomeTabViewController.dateFormatterCommon(dateStr: need["created_at"].stringValue)
            //            if let enddate = need["end_date"].string, !enddate.isEmpty{
            //                if enddate == "0"{
            //                    cell.lblNeedDate.text = EventsTabViewController.convertTimeStampToDate(timestamp: need["start_date"].stringValue)
            //                }
            //                else{
            //                    cell.lblNeedDate.text = EventsTabViewController.convertTimeStampToDate(timestamp: need["end_date"].stringValue)
            //                }
            //            }
            //            else{
            //                cell.lblNeedDate.text = EventsTabViewController.convertTimeStampToDate(timestamp: need["start_date"].stringValue)
            //            }
            cell.lblNeedDate.text = EventsTabViewController.convertTimeStampToDate(timestamp: need["start_date"].stringValue)
            
            cell.lblNumberofSupporters.text = need["supporters"].stringValue
            let supportCount = Int(need["supporters"].stringValue)!
            if supportCount > 1{
                cell.lblHeadingSupporter.text = "Supporters"
            }
            else{
                cell.lblHeadingSupporter.text = "Supporter"
            }
            
            let proPic = need["propic"].stringValue
            
            DispatchQueue.main.async {
                
                if proPic.count != 0{
                    let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                    let imgUrl = URL(string: temp)
                    cell.ImageViewuserProfile.kf.indicatorType = .activity
                    
                    cell.ImageViewuserProfile.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                }
                else{
                    cell.ImageViewuserProfile.image = #imageLiteral(resourceName: "Male Colored")
                }
            }
            cell.ImageViewuserProfile.isTappable(id: need["user_id"].intValue)
            
            return cell
        }
        else{
            let post = ArrayPosts[indexPath.row]
            
            if indexPath.row == 0{
                self.viewCurrentPost(index: indexPath.row)
            }
            if let postType = post["publish_type"].string, postType.lowercased() == "request" || postType.lowercased() == "albums" || postType.lowercased() == "documents"{
                
                let cellid = "PostThanksTableViewCell"
                
                let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! PostThanksTableViewCell
                
                
                cell.readmoreButton.tag = indexPath.row
                cell.buttonShare.tag = indexPath.row
                cell.buttonRightMenu.tag = indexPath.row
                cell.commentsButton.tag = indexPath.row
                cell.viewsButton.tag = indexPath.row
                cell.sharedUsersButton.tag = indexPath.row
                cell.buttonComments.tag = indexPath.row
                cell.buttonComments.tag = indexPath.row
                cell.btnFamilyClick.tag = indexPath.row
                cell.btnOpenRequest.tag = indexPath.row
                
                //User Details
                cell.labelUserName.text = post["created_user_name"].string ?? "Unknown"
                cell.labelPostedIn.text = post["group_name"].string ?? "Public"
                if cell.labelPostedIn.text == "Public"
                {
                    cell.btnFamilyClick.isUserInteractionEnabled = false
                }
                else
                {
                    cell.btnFamilyClick.isUserInteractionEnabled = true
                    
                }
                //            cell.labelDate.text = post["createdAt"].string ?? ""
                cell.labelDate.text = HomeTabViewController.dateFormatterCommon(dateStr: post["createdAt"].stringValue)
                
                let proPic = post["propic"].string ?? ""
                print(post["created_user_name"],post["propic"])
                DispatchQueue.main.async {
                    
                    if proPic.count != 0{
                        let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                        let imgUrl = URL(string: temp)
                        cell.imageViewProfilePic.kf.indicatorType = .activity
                        
                        cell.imageViewProfilePic.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                    }
                    else{
                        cell.imageViewProfilePic.image = #imageLiteral(resourceName: "Male Colored")
                    }
                }
                cell.imageViewProfilePic.isTappable(id: post["created_by"].intValue)
                //Post
                cell.labelPost.text = post["snap_description"].string ?? ""
                
                //Details
                cell.lableNumberOfConversations.text = post["conversation_count"].stringValue
                cell.lblNumberOfShares.text = post["shared_user_count"].stringValue
                cell.labelNumberOfViews.text = post["views_count"].stringValue
                
                if let Attachment = post["post_attachment"].array, Attachment.count != 0{
                    print("updating slide........")
                    print(indexPath.row)
                    cell.updateSliderThankYou(data: Attachment, Ptype: postType)
                }
                else{
                    print("No Slide........")
                    print(indexPath.row)
                    
                    cell.viewPostBg.isHidden = false
                    cell.viewPageControllBG.isHidden = false
                    if let urlArray = post["valid_urls"].array, urlArray.count != 0{
                        if urlArray.count == 1{
                            if let urlMetadata = post["url_metadata"].dictionary, urlMetadata.count != 0{
                                if let result = urlMetadata["urlMetadataResult"], result.count != 0{
                                    cell.viewPostBg.isHidden = false
                                    cell.viewPageControllBG.isHidden = true
                                    
                                    cell.updateSliderLink(data: [result])
                                }
                                else{
                                    print(indexPath.row)
                                    cell.viewPostBg.isHidden = true
                                    cell.viewPageControllBG.isHidden = true
                                }
                            }
                            
                            
                        }
                        else{
                            print(indexPath.row)
                            cell.viewPostBg.isHidden = true
                            cell.viewPageControllBG.isHidden = true
                        }
                    }
                    else{
                        print(indexPath.row)
                        cell.viewPostBg.isHidden = true
                        cell.viewPageControllBG.isHidden = true
                    }
                }
                
                
                if let type = post["publish_type"].string, type.lowercased() == "request"{
                    cell.lblItemNames.isHidden = false
                    if let mentionedItems = post["publish_mention_items"].array, mentionedItems.count != 0{
                        //                    for index in mentionedUsers{
                        //                        let name = index["user_name"].stringValue
                        //                        let temArr = name.components(separatedBy: " ")
                        //                        if temArr.count > 0{
                        //                            let formattedName = "@\(temArr[0])"
                        //                            stringArr.append(formattedName)
                        //                        }
                        //                    }
                        cell.lblItemNames.text = mentionedItems[0]["item_name"].stringValue
                    }
                    else{
                        cell.lblItemNames.text = ""
                    }
                    cell.btnOpenRequest.setTitle("Open Request", for: .normal)
                    cell.viewViews.isHidden = true
                }
                else{
                    cell.lblItemNames.text = ""
                    cell.lblItemNames.isHidden = true
                    cell.viewViews.isHidden = false
                    
                    cell.delegate = self
                    if let type = post["publish_type"].string, type.lowercased() == "albums"{
                        cell.btnOpenRequest.setTitle("Open Album", for: .normal)
                    }
                    else{
                        cell.btnOpenRequest.setTitle("Open Folder", for: .normal)
                    }
                    
                }
                
                if cell.labelPost.calculateMaxLines() > maxNumberOfLines{
                    cell.labelPost.numberOfLines = maxNumberOfLines
                    cell.readMoreView.isHidden = false
                    
                    //            tableView.layoutIfNeeded()
                }
                else{
                    cell.labelPost.numberOfLines = 0
                    cell.readMoreView.isHidden = true
                    //            tableView.layoutIfNeeded()
                }
                //                cell.delegate = self
                
                
                // String(describing: UserDefaults.standard.value(forKey: "userId"))
                let user = UserDefaults.standard.value(forKey: "userId") as! String
                if  user  == post["created_by"].stringValue{
                    
                    //                    cell.viewViews.isHidden = false
                    //                    cell.viewShared.isHidden = false
                    if post["conversation_enabled"].bool ?? false{
                        cell.viewComments.isHidden = false
                    }
                    else{
                        cell.viewComments.isHidden = true
                    }
                }
                else{
                    
                    //                    cell.viewViews.isHidden = true
                    //                    cell.viewShared.isHidden = true
                    if post["conversation_enabled"].bool ?? false{
                        cell.viewComments.isHidden = false
                    }
                    else{
                        cell.viewComments.isHidden = true
                    }
                }
                
                if post["conversation_count_new"].intValue > 0{
                    cell.lblNewConvrstn.isHidden = false
                }
                else{
                    cell.lblNewConvrstn.isHidden = true
                }
                
                
                if post["is_shareable"].bool ?? false {
                    
                    cell.shareView.isHidden = false
                    let user = UserDefaults.standard.value(forKey: "userId") as! String
                    if  user  == post["created_by"].stringValue{
                        cell.viewShared.isHidden = false
                    }
                    
                    
                }
                else{
                    cell.viewShared.isHidden = true
                    cell.shareView.isHidden = true
                    
                }
                cell.btnMentionedNames.tag = indexPath.row
                
                //                var stringArr = [String]()
                //                    for index in mentionedUsers{
                //                        let name = index["user_name"].stringValue
                //                        let temArr = name.components(separatedBy: " ")
                //                        if temArr.count > 0{
                //                            let formattedName = "@\(temArr[0])"
                //                            stringArr.append(formattedName)
                //                        }
                //                    }
                //                    cell.lblMentionedNames.text = stringArr.joined(separator: ", ")
                if let mentionedUsers = post["publish_mention_users"].array, mentionedUsers.count != 0{
                    
                    if mentionedUsers.count == 1
                    {
                        cell.lblMentionedNames.text = "@ \(mentionedUsers[0]["user_name"].stringValue.components(separatedBy: " ")[0])"
                        cell.lblMentionedNames.isHidden = false
                        cell.btnMentionedNames.isHidden = false
                        
                        cell.collVwOfMentionedNames.isHidden = true
                        
                        cell.heightOfMentionedView.constant = 30
                        
                        
                    }
                    else
                    {
                        cell.lblMentionedNames.isHidden = true
                        cell.btnMentionedNames.isHidden = true
                        
                        cell.collVwOfMentionedNames.isHidden = false
                        cell.loadMentionedNames(data:mentionedUsers)
                        
                    }
                    
                    
                }
                else{
                    cell.lblMentionedNames.text = ""
                    cell.lblMentionedNames.isHidden = true
                    cell.collVwOfMentionedNames.isHidden = true
                    cell.btnMentionedNames.isHidden = true
                    
                    cell.heightOfMentionedView.constant = 0
                    
                }
                
                cell.labelPost.handleHashtagTap { hashtag in
                    print("Success. You just tapped the \(hashtag) hashtag")
                    self.txtSearch.text  = "#"+hashtag
                    self.openSearch()
                    //                self.searchview.isHidden = false
                    //                self.searchConstraintheight.constant = 36
                    self.txtSearch.becomeFirstResponder()
                    
                }
                cell.labelPost.handleMentionTap { (mension) in
                    print("Success. You just tapped the \(mension) mension")
                    self.txtSearch.text  = mension
                    //                self.searchview.isHidden = false
                    //                self.searchConstraintheight.constant = 36
                    self.openSearch()
                    self.txtSearch.becomeFirstResponder()
                    
                }
                cell.labelPost.handleURLTap { (url) in
                    print(url)
                    //                    UIApplication.shared.open(url)
                    
                    if url.absoluteString.contains("familheey"){
                        self.openAppLink(url: url.absoluteString)
                        //                        Helpers.openAppLinkCommon(url: url.absoluteString, currentVC: self)
                    }
                    else{
                        if !(["http", "https"].contains(url.scheme?.lowercased())) {
                            var strUrl = url.absoluteString
                            strUrl = "http://"+strUrl
                            let Turl = URL(string: strUrl)
                            
                            let vc = SFSafariViewController(url: Turl!)
                            self.present(vc, animated: true, completion: nil)
                        }
                        else{
                            let vc = SFSafariViewController(url: url)
                            self.present(vc, animated: true, completion: nil)
                        }
                    }
                    
                }
                
                return cell
                
            }
            else{
                if let sharedusers = post["shared_user_name"].string , !sharedusers.isEmpty{
                    
                    
                    let cellid = "SharedPostTableViewCell"
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! SharedPostTableViewCell
                    
                    let post = ArrayPosts[indexPath.row]
                    
                    cell.readmoreButton.tag = indexPath.row
                    cell.buttonShare.tag = indexPath.row
                    cell.buttonSharedUsers.tag = indexPath.row
                    cell.btnRightMrnu.tag = indexPath.row
                    //            cell.commentsButton.tag = indexPath.row
                    cell.buttonComments.tag = indexPath.row
                    cell.viewsButtons.tag = indexPath.row
                    cell.sharedUserButton.tag = indexPath.row
                    cell.btnFamilyClick.tag = indexPath.row
                    
                    
                    //User Details
                    
                    if post["common_share_count"].intValue != 0{
                        
                        if post["common_share_count"].intValue == 1{
                            cell.labelUserName.text = "\(sharedusers) shared a post"
                        }
                        else if post["common_share_count"].intValue == 2{
                            cell.labelUserName.text = "\(sharedusers) and 1 other shared a post"
                        }
                        else {
                            cell.labelUserName.text = "\(sharedusers) and \(post["common_share_count"].intValue-1) others shared a post"
                        }
                    }
                    if post["to_user_id"].stringValue == UserDefaults.standard.value(forKey: "userId") as! String{
                        cell.lblShared.text = "Shared with"
                        //                cell.lblShare_width.constant = 78.0
                        cell.labelPostedIn.text = " you"
                    }
                    else{
                        cell.lblShared.text = "Shared in"
                        //                cell.lblShare_width.constant = 65.0
                        cell.labelPostedIn.text = post["group_name"].string ?? ""
                    }
                    
                    cell.labelUserNameInner.text = post["parent_post_created_user_name"].string ?? "Unknown"
                    if post["parent_post_grp_name"].stringValue.count > 0 {
                        cell.labelPostedInInner.text = post["parent_post_grp_name"].string ?? ""
                    }
                    else{
                        cell.labelPostedInInner.text = post["privacy_type"].string ?? ""
                    }
                    
                    
                    //            cell.labelDate.text = post["createdAt"].string ?? ""
                    cell.labelDate.text = HomeTabViewController.dateFormatterCommon(dateStr: post["createdAt"].stringValue)
                    
                    let proPic = post["parent_post_created_user_propic"].stringValue
                    print(post["created_user_name"],post["propic"])
                    DispatchQueue.main.async {
                        
                        if proPic.count != 0{
                            let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                            let imgUrl = URL(string: temp)
                            cell.imageviewProfilePicInner.kf.indicatorType = .activity
                            
                            cell.imageviewProfilePicInner.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                        }
                        else{
                            cell.imageviewProfilePicInner.image = #imageLiteral(resourceName: "Male Colored")
                        }
                    }
                    DispatchQueue.main.async {
                        
                        if proPic.count != 0{
                            let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                            let imgUrl = URL(string: temp)
                            cell.imageviewProfilePicInner.kf.indicatorType = .activity
                            
                            cell.imageviewProfilePicInner.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                        }
                        else{
                            cell.imageviewProfilePicInner.image = #imageLiteral(resourceName: "Male Colored")
                        }
                    }
                    cell.imageviewProfilePicInner.isTappable(id: post["parent_post_created_user_id"].intValue)
                    //Post
                    cell.labelPost.text = post["snap_description"].string ?? ""
                    
                    //Details
                    cell.lableNumberOfConversations.text = post["conversation_count"].stringValue
                    cell.lblNumberOfShares.text = post["shared_user_count"].stringValue
                    cell.lblNumberOfViews.text = post["views_count"].stringValue
                    
                    if let Attachment = post["post_attachment"].array, Attachment.count != 0{
                        
                        cell.updateSlider(data: Attachment)
                    }
                    else{
                        print("No Slide........")
                        print(indexPath.row)
                        
                        if let urlArray = post["valid_urls"].array, urlArray.count != 0{
                            if urlArray.count == 1{
                                if let urlMetadata = post["url_metadata"].dictionary, urlMetadata.count != 0{
                                    if let result = urlMetadata["urlMetadataResult"], result.count != 0{
                                        cell.viewPostBg.isHidden = false
                                        cell.viewPageControllBG.isHidden = true
                                        
                                        cell.updateSliderLink(data: [result])
                                    }
                                    else{
                                        print(indexPath.row)
                                        cell.viewPostBg.isHidden = true
                                        cell.viewPageControllBG.isHidden = true
                                    }
                                }
                            }
                            else{
                                print(indexPath.row)
                                cell.viewPostBg.isHidden = true
                                cell.viewPageControllBG.isHidden = true
                            }
                        }
                        else{
                            print(indexPath.row)
                            cell.viewPostBg.isHidden = true
                            cell.viewPageControllBG.isHidden = true
                        }
                    }
                    
                    if cell.labelPost.calculateMaxLines() > maxNumberOfLines{
                        cell.labelPost.numberOfLines = maxNumberOfLines
                        cell.readMoreView.isHidden = false
                        
                        //            tableView.layoutIfNeeded()
                    }
                    else{
                        cell.labelPost.numberOfLines = 0
                        //                cell.readMoreView.isHidden = true
                        cell.readmoreButton.isHidden = true
                        cell.readMore_height.constant = 5
                        //            tableView.layoutIfNeeded()
                    }
                    cell.delegate = self
                    
                    
                    let user = UserDefaults.standard.value(forKey: "userId") as! String
                    if  user  == post["created_by"].stringValue{
                        cell.viewViews.isHidden = false
                        if post["conversation_enabled"].bool ?? false{
                            cell.ViewComments.isHidden = false
                        }
                        else{
                            cell.ViewComments.isHidden = true
                        }
                    }
                    else{
                        cell.viewViews.isHidden = true
                        if post["conversation_enabled"].bool ?? false{
                            cell.ViewComments.isHidden = false
                        }
                        else{
                            cell.ViewComments.isHidden = true
                        }
                    }
                    //            if  user  == post["created_by"].stringValue{
                    //                cell.rightMenuVew.isHidden = false
                    //            }
                    //            else{
                    //                cell.rightMenuVew.isHidden = true
                    //            }
                    if post["conversation_count_new"].intValue > 0{
                        cell.lblNewConverstn.isHidden = false
                    }
                    else{
                        cell.lblNewConverstn.isHidden = true
                    }
                    
                    if post["is_shareable"].bool ?? false {
                        
                        cell.shareView.isHidden = false
                        let user = UserDefaults.standard.value(forKey: "userId") as! String
                        if  user  == post["created_by"].stringValue{
                            cell.viewShared.isHidden = false
                        }
                    }
                    else{
                        
                        cell.shareView.isHidden = true
                        cell.viewShared.isHidden = true
                        
                    }
                    cell.labelPost.handleHashtagTap { hashtag in
                        print("Success. You just tapped the \(hashtag) hashtag")
                        self.txtSearch.text  = "#"+hashtag
                        //                self.searchview.isHidden = false
                        //                self.searchConstraintheight.constant = 36
                        self.openSearch()
                        self.txtSearch.becomeFirstResponder()
                        
                    }
                    cell.labelPost.handleMentionTap { (mension) in
                        print("Success. You just tapped the \(mension) mension")
                        self.txtSearch.text  = mension
                        self.openSearch()
                        //                self.searchview.isHidden = false
                        //                self.searchConstraintheight.constant = 36
                        self.txtSearch.becomeFirstResponder()
                        
                    }
                    cell.labelPost.handleURLTap { (url) in
                        print(url)
                        //                    UIApplication.shared.open(url)
                        if url.absoluteString.contains("familheey"){
                            self.openAppLink(url: url.absoluteString)
                            //                            Helpers.openAppLinkCommon(url: url.absoluteString, currentVC: self)
                        }
                        else{
                            if !(["http", "https"].contains(url.scheme?.lowercased())) {
                                var strUrl = url.absoluteString
                                strUrl = "http://"+strUrl
                                let Turl = URL(string: strUrl)
                                
                                let vc = SFSafariViewController(url: Turl!)
                                self.present(vc, animated: true, completion: nil)
                            }
                            else{
                                let vc = SFSafariViewController(url: url)
                                self.present(vc, animated: true, completion: nil)
                            }
                        }
                        
                    }
                    
                    //            if tableView.isRowCompletelyVisible(at: indexPath){
                    //
                    //            }
                    //            else{
                    //
                    //            }
                    return cell
                }
                    
                    
                else{
                    let cellid = "PostImageTableViewCell"
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! PostImageTableViewCell
                    
                    
                    cell.readmoreButton.tag = indexPath.row
                    cell.buttonShare.tag = indexPath.row
                    cell.buttonRightMenu.tag = indexPath.row
                    cell.commentsButton.tag = indexPath.row
                    cell.viewsButton.tag = indexPath.row
                    cell.sharedUsersButton.tag = indexPath.row
                    cell.buttonComments.tag = indexPath.row
                    cell.buttonComments.tag = indexPath.row
                    cell.btnFamilyClick.tag = indexPath.row
                    
                    
                    //User Details
                    cell.labelUserName.text = post["created_user_name"].string ?? "Unknown"
                    cell.labelPostedIn.text = post["group_name"].string ?? "Public"
                    if cell.labelPostedIn.text == "Public"
                    {
                        cell.btnFamilyClick.isUserInteractionEnabled = false
                    }
                    else
                    {
                        cell.btnFamilyClick.isUserInteractionEnabled = true
                        
                    }
                    //            cell.labelDate.text = post["createdAt"].string ?? ""
                    cell.labelDate.text = HomeTabViewController.dateFormatterCommon(dateStr: post["createdAt"].stringValue)
                    
                    
                    DispatchQueue.main.async {
                        let proPic = post["propic"].string ?? ""  //Jeena 18-02-2021
                        if proPic.count != 0{
                            let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                            let imgUrl = URL(string: temp)
                            cell.imageViewProfilePic.kf.indicatorType = .activity
                            cell.imageViewProfilePic.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                        }
                        
                        else{
                            /* Jeena 18-02-2021 to solve profile mismatch issuee*/
                            let imgUrl = URL(string: "")
                            cell.imageViewProfilePic.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                            /*** end   */
                            cell.imageViewProfilePic.image = #imageLiteral(resourceName: "Male Colored")
                    }
                   
                    }

                    cell.imageViewProfilePic.isTappable(id: post["created_by"].intValue)
                    //Post
                    cell.labelPost.text = post["snap_description"].string ?? ""
                    
                    //Details
                    cell.lableNumberOfConversations.text = post["conversation_count"].stringValue
                    cell.lblNumberOfShares.text = post["shared_user_count"].stringValue
                    cell.labelNumberOfViews.text = post["views_count"].stringValue
                    
                    if let Attachment = post["post_attachment"].array, Attachment.count != 0{
                        print("updating slide........")
                        print(indexPath.row)
                        cell.updateSlider(data: Attachment)
                    }
                    else{
                        print("No Slide........")
                        print(indexPath.row)
                        
                        cell.viewPostBg.isHidden = false
                        cell.viewPageControllBG.isHidden = true
                        if let urlArray = post["valid_urls"].array, urlArray.count != 0{
                            if urlArray.count == 1{
                                if let urlMetadata = post["url_metadata"].dictionary, urlMetadata.count != 0{
                                    if let result = urlMetadata["urlMetadataResult"], result.count != 0{
                                        cell.viewPostBg.isHidden = false
                                        cell.viewPageControllBG.isHidden = true
                                        
                                        cell.updateSliderLink(data: [result])
                                    }
                                    else{
                                        print(indexPath.row)
                                        cell.viewPostBg.isHidden = true
                                        cell.viewPageControllBG.isHidden = true
                                    }
                                }
                                
                                
                            }
                            else{
                                print(indexPath.row)
                                cell.viewPostBg.isHidden = true
                                cell.viewPageControllBG.isHidden = true
                            }
                        }
                        else{
                            print(indexPath.row)
                            cell.viewPostBg.isHidden = true
                            cell.viewPageControllBG.isHidden = true
                        }
                    }
                    
                    if cell.labelPost.calculateMaxLines() > maxNumberOfLines{
                        cell.labelPost.numberOfLines = maxNumberOfLines
                        cell.readMoreView.isHidden = false
                        
                        //            tableView.layoutIfNeeded()
                    }
                    else{
                        cell.labelPost.numberOfLines = 0
                        cell.readMoreView.isHidden = true
                        //            tableView.layoutIfNeeded()
                    }
                    cell.delegate = self
                    
                    
                    // String(describing: UserDefaults.standard.value(forKey: "userId"))
                    let user = UserDefaults.standard.value(forKey: "userId") as! String
                    if  user  == post["created_by"].stringValue{
                        
                        cell.viewViews.isHidden = false
                        if post["conversation_enabled"].bool ?? false{
                            cell.viewComments.isHidden = false
                        }
                        else{
                            cell.viewComments.isHidden = true
                        }
                    }
                    else{
                        
                        cell.viewViews.isHidden = true
                        if post["conversation_enabled"].bool ?? false{
                            cell.viewComments.isHidden = false
                        }
                        else{
                            cell.viewComments.isHidden = true
                        }
                    }
                    
                    if post["conversation_count_new"].intValue > 0{
                        cell.lblNewConvrstn.isHidden = false
                    }
                    else{
                        cell.lblNewConvrstn.isHidden = true
                    }
                    
                    
                    if post["is_shareable"].bool ?? false {
                        
                        cell.shareView.isHidden = false
                        let user = UserDefaults.standard.value(forKey: "userId") as! String
                        if  user  == post["created_by"].stringValue{
                            cell.viewShared.isHidden = false
                        }
                    }
                    else{
                        
                        cell.shareView.isHidden = true
                        cell.viewShared.isHidden = true
                        
                    }
                    
                    cell.labelPost.handleHashtagTap { hashtag in
                        print("Success. You just tapped the \(hashtag) hashtag")
                        self.txtSearch.text  = "#"+hashtag
                        self.openSearch()
                        //                self.searchview.isHidden = false
                        //                self.searchConstraintheight.constant = 36
                        self.txtSearch.becomeFirstResponder()
                        
                    }
                    cell.labelPost.handleMentionTap { (mension) in
                        print("Success. You just tapped the \(mension) mension")
                        self.txtSearch.text  = mension
                        //                self.searchview.isHidden = false
                        //                self.searchConstraintheight.constant = 36
                        self.openSearch()
                        self.txtSearch.becomeFirstResponder()
                        
                    }
                    cell.labelPost.handleURLTap { (url) in
                        print(url)
                        //                    UIApplication.shared.open(url)
                        if url.absoluteString.contains("familheey"){
                            self.openAppLink(url: url.absoluteString)
                            //                            Helpers.openAppLinkCommon(url: url.absoluteString, currentVC: self)
                        }
                        else{
                            if !(["http", "https"].contains(url.scheme?.lowercased())) {
                                var strUrl = url.absoluteString
                                strUrl = "http://"+strUrl
                                let Turl = URL(string: strUrl)
                                
                                let vc = SFSafariViewController(url: Turl!)
                                self.present(vc, animated: true, completion: nil)
                            }
                            else{
                                let vc = SFSafariViewController(url: url)
                                self.present(vc, animated: true, completion: nil)
                            }
                        }
                        
                    }
                    return cell
                }
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        // pagination
        
        if tableView.isLast(for: indexPath, arrayCount: ArrayPosts.count) {
            
            if !isNewDataLoading && !self.stopPagination{
                isNewDataLoading = true
                if ArrayPosts.count != 0{
                    offset = ArrayPosts.count + 1
                }
                print("---------------------------------------------------\(offset)")
                if selectedIndex == 0{
                    getPosts()
                }
                else if selectedIndex == 1{
                    getpublicfeed()
                }
                else{
                    self.getrequests()
                }
            }
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y
        
        //           if !searchview.isHidden{
        //               self.notificationHeight.constant = 0
        //               self.notificationView.isHidden = true
        //               return
        //           }
        
        if(scrollView.panGestureRecognizer.translation(in: scrollView.superview).y >= 0)
        {
            DispatchQueue.main.async {
                if  !self.notificationView.isHidden /*&& self.notificationHeight.constant == self.headerHeight*/{
                    return
                }
                //                if self.notificationHeight.constant <= self.headerHeight{
                if  self.notificationView.isHidden{
                    self.notificationView.isHidden = false
                    //                    if self.bannerImageAvailable {
                    //                        self.bannerView.isHidden = false
                    //                    }
                    
                }
                //                    if self.notificationHeight.constant + (self.previousPageXOffset  - offset) >= self.headerHeight || self.notificationHeight.constant + (self.previousPageXOffset  - offset) <= 0 {
                //                        self.notificationHeight.constant = self.headerHeight
                //                    }
                //                    else{
                //                        self.notificationHeight.constant = self.notificationHeight.constant + (self.previousPageXOffset  - offset)
                //                    }
                //                }
                //                else if self.notificationHeight.constant > self.headerHeight{
                //                    self.notificationHeight.constant = self.headerHeight
                //                }
                self.previousPageXOffset = offset
                
            }
        }
        else{
            DispatchQueue.main.async {
                if  self.notificationView.isHidden /*&& self.notificationHeight.constant == 0 )*/{
                    return
                }
                //                if self.notificationHeight.constant <= self.headerHeight {
                //                    if self.notificationHeight.constant <= 0{
                //                        self.notificationHeight.constant = 0
                if  !self.notificationView.isHidden{
                    self.notificationView.isHidden = true
                    //                    self.bannerView.isHidden = true
                    
                }
                //                    }
                //                    if self.notificationHeight.constant - (offset - self.previousPageXOffset) >= self.headerHeight || self.notificationHeight.constant - (offset - self.previousPageXOffset) <= 0 {
                //                        self.notificationHeight.constant = 0
                //                        self.notificationView.isHidden = true
                //
                //
                //                    }
                //                    else{
                //                        self.notificationHeight.constant = self.notificationHeight.constant - (offset - self.previousPageXOffset)
                //
                //                    }
                //                }
                //                else if self.notificationHeight.constant > self.headerHeight{
                //                    self.notificationHeight.constant = self.headerHeight
                //                }
                self.previousPageXOffset = offset
                
            }
        }
        guard let indepaths = self.tblListView.indexPathsForVisibleRows else { return  }
        DispatchQueue.main.async {
            
            for index in indepaths{
                
                if let cell = self.tblListView.cellForRow(at: index) as? PostImageTableViewCell{
                    cell.stopPlaying()
                    break
                }
                else if let cell = self.tblListView.cellForRow(at: index) as? SharedPostTableViewCell {
                    cell.stopPlaying()
                    break
                }
            }
        }
        
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if fromRefresh{
            fromRefresh = false
        }
        else{
            var currentCellOffset = scrollView.contentOffset
            currentCellOffset.x += (self.tblListView.bounds.width);
            
            let indepath = self.tblListView.indexPathsForVisibleRows?.first
            if indepath != nil{
                self.viewCurrentPost(index: (indepath?.row)!)
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func getnumberOfLines(label:UILabel) -> Int{
        var   lineCount:NSInteger = 0
        let  textSize:CGSize = CGSize.init(width: label.frame.size.width, height: CGFloat(MAXFLOAT))
        let   rHeight:Int = lroundf(Float(label.sizeThatFits(textSize).height))
        let   charSize:Int = lroundf(Float(label.font!.lineHeight))
        lineCount = rHeight/charSize
        NSLog("No of lines: %i",lineCount)
        return lineCount
    }
    
}
extension HomeTabViewController : PostImageTableViewCellDelegate, UIDocumentInteractionControllerDelegate{
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    func gotoPreview(data: [JSON],index:Int) {
        
        if let type = data[index]["type"].string ,( type.contains("text") || type.contains("pdf") || type.contains("doc") || type.contains("docx") || type.contains("xls") || type.contains("xlsx") || type.contains("msword") || type.contains("txt")) {
            var temp:String!
            temp = "\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+data[index]["filename"].stringValue
            
            let url = URL(string: temp)!
            ActivityIndicatorView.show("Loading....")
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else { return }
                let tmpURL = FileManager.default.temporaryDirectory
                    .appendingPathComponent(response?.suggestedFilename ?? "fileName.png")
                do {
                    try data.write(to: tmpURL)
                } catch {
                    print(error)
                    ActivityIndicatorView.hiding()
                }
                DispatchQueue.main.async {
                    /// STOP YOUR ACTIVITY INDICATOR HERE
                    ActivityIndicatorView.hiding()
                    print(tmpURL)
                    self.share(url: tmpURL)
                }
            }.resume()
            
            /*let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
             
             let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementDocPreviewViewController") as! AnnouncementDocPreviewViewController
             vc.UrlToLoad = temp
             self.present(vc, animated: true, completion: nil)*/
        }
        else if let type = data[index]["url"].string {
            if type.isEmpty{
                
            }
            else{
                print(type.contains("familheey"))
                if type.contains("familheey"){
                    self.openAppLink(url: type)
                    //                    Helpers.openAppLinkCommon(url: type, currentVC: self)
                }
                else{
                    //                    UIApplication.shared.open(url)
                    let url = URL(string: type)!
                    let vc = SFSafariViewController(url: url)
                    self.present(vc, animated: true, completion: nil)
                }
            }
            
        }
        else{
            let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsPreviewViewController") as! AnnouncementsPreviewViewController
            vc.attachments = data
            vc.isFromPost = true
            vc.selectedIndex = index
            self.navigationController?.addFadeAnimation()
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    func share(url: URL) {
        print(url)
        documentInteractionController = UIDocumentInteractionController(url: url)
        //        documentInteractionController.uti = url.typeIdentifier ?? "public.data, public.content"
        //        documentInteractionController.name = url.localizedName ?? url.lastPathComponent
        documentInteractionController.delegate = self
        documentInteractionController.presentPreview(animated: true)
    }
    
    func gotoSinglePreview(data: String) {
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: "ChangePicturesViewController") as! ChangePicturesViewController
        vc.isCoverOrProfilePic = "CoverPic"
        vc.ids           = UserDefaults.standard.value(forKey: "userId") as! String
        vc.isFrom = "post"
        vc.inFor = "cover"
        vc.isAdmin = ""
        vc.imageUrl = data
        self.imgClicked = true
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}
extension HomeTabViewController : SharedPostTableViewCellDelegate{
    
    func gotoSharedPreview(data: [JSON], index: Int)
    {
        
        if let type = data[index]["type"].string ,( type.contains("text") || type.contains("pdf") || type.contains("doc") || type.contains("docx") || type.contains("xls") || type.contains("xlsx") || type.contains("msword") || type.contains("txt")) {
            var temp:String!
            temp = "\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+data[index]["filename"].stringValue
            let url = URL(string: temp)!
            ActivityIndicatorView.show("Loading....")
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else { return }
                let tmpURL = FileManager.default.temporaryDirectory
                    .appendingPathComponent(response?.suggestedFilename ?? "fileName.png")
                do {
                    try data.write(to: tmpURL)
                } catch {
                    print(error)
                    ActivityIndicatorView.hiding()
                }
                DispatchQueue.main.async {
                    /// STOP YOUR ACTIVITY INDICATOR HERE
                    ActivityIndicatorView.hiding()
                    print(tmpURL)
                    self.share(url: tmpURL)
                }
            }.resume()
            
            /*let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
             
             let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementDocPreviewViewController") as! AnnouncementDocPreviewViewController
             vc.UrlToLoad = temp
             self.present(vc, animated: true, completion: nil)*/
        }
        else if let type = data[index]["url"].string {
            if type.isEmpty{
                
            }
            else{
                print(type.contains("familheey"))
                if type.contains("familheey"){
                    self.openAppLink(url: type)
                    //                    Helpers.openAppLinkCommon(url: type, currentVC: self)
                }
                else{
                    //                    UIApplication.shared.open(url)
                    let url = URL(string: type)!
                    let vc = SFSafariViewController(url: url)
                    self.present(vc, animated: true, completion: nil)
                }
            }
            
        }
        else{
            let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsPreviewViewController") as! AnnouncementsPreviewViewController
            vc.attachments = data
            vc.isFromPost = true
            vc.selectedIndex = index
            self.navigationController?.addFadeAnimation()
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    func gotoSharedSinglePreview(data: String)
    {
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: "ChangePicturesViewController") as! ChangePicturesViewController
        vc.isCoverOrProfilePic = "CoverPic"
        vc.ids           = UserDefaults.standard.value(forKey: "userId") as! String
        vc.isFrom = "post"
        vc.inFor = "cover"
        vc.isAdmin = ""
        self.imgClicked = true
        vc.imageUrl = data
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}

extension HomeTabViewController: PostThanksableViewCellDelegate{
    func gotoThanksPreview(data: [JSON], index: Int) {
        
        if let type = data[index]["type"].string ,( type.contains("text") || type.contains("pdf") || type.contains("doc") || type.contains("docx") || type.contains("xls") || type.contains("xlsx") || type.contains("msword") || type.contains("txt")) {
            var temp:String!
            
            temp = "\(Helpers.imageURl)"+"\(BaseUrl.document_image)"+data[index]["filename"].stringValue
            let url = URL(string: temp)!
            ActivityIndicatorView.show("Loading....")
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else { return }
                let tmpURL = FileManager.default.temporaryDirectory
                    .appendingPathComponent(response?.suggestedFilename ?? "fileName.png")
                do {
                    try data.write(to: tmpURL)
                } catch {
                    print(error)
                    ActivityIndicatorView.hiding()
                }
                DispatchQueue.main.async {
                    /// STOP YOUR ACTIVITY INDICATOR HERE
                    ActivityIndicatorView.hiding()
                    print(tmpURL)
                    self.share(url: tmpURL)
                }
            }.resume()
            /*let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
             
             let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementDocPreviewViewController") as! AnnouncementDocPreviewViewController
             vc.UrlToLoad = temp
             self.present(vc, animated: true, completion: nil)*/
        }
        else if let type = data[index]["url"].string {
            if type.isEmpty{
                
            }
            else{
                print(type.contains("familheey"))
                if type.contains("familheey"){
                    self.openAppLink(url: type)
                    //                    Helpers.openAppLinkCommon(url: type, currentVC: self)
                }
                else{
                    //                    UIApplication.shared.open(url)
                    let url = URL(string: type)!
                    let vc = SFSafariViewController(url: url)
                    self.present(vc, animated: true, completion: nil)
                }
            }
            
        }
        else{
            let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsPreviewViewController") as! AnnouncementsPreviewViewController
            vc.attachments = data
            vc.isFromPost = true
            vc.typeAlbum = true
            vc.selectedIndex = index
            self.navigationController?.addFadeAnimation()
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
    }
    
    func gotoThanksSinglePreview(data: String) {
        /*let storyboard = UIStoryboard.init(name: "second", bundle: nil)
         
         let vc = storyboard.instantiateViewController(withIdentifier: "ChangePicturesViewController") as! ChangePicturesViewController
         vc.isCoverOrProfilePic = "CoverPic"
         vc.ids           = UserDefaults.standard.value(forKey: "userId") as! String
         vc.isFrom = "post"
         vc.inFor = "cover"
         vc.isAdmin = ""
         vc.imageUrl = data
         self.imgClicked = true
         self.navigationController?.pushViewController(vc, animated: true)*/
        
    }
    func selectMentionedName(data: [JSON], index: Int) {
        
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "UserProfileTableViewController") as! UserProfileTableViewController
        let userId = data[index]["user_id"].stringValue
        vc.userID = userId
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func btnMentinedNameClicked(_ sender: UIButton)
    {
        let post = ArrayPosts[sender.tag]
        
        
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "UserProfileTableViewController") as! UserProfileTableViewController
        let userId = post["publish_mention_users"][0]["user_id"].stringValue
        vc.userID = userId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension HomeTabViewController: NeedsRequestListTableViewCellDelegate{
    func viewMoreAction(index: Int) {
        if ArrayPosts.count < index{
            return
        }
        
        let need = ArrayPosts[index]
        //        if let postType = need["publish_type"].string, postType.lowercased() == "request"{
        let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "RequestDetailsViewController") as! RequestDetailsViewController
        vc.requestId = need["post_request_id"].stringValue
        //        vc.requestDic = need
        self.navigationController?.pushViewController(vc, animated: true)
        //        }
        //        else{
        //
        //        }
    }
    
    func rightMenuAction(index: Int) {
        if ArrayPosts.count < index{
            return
        }
        
        let need = ArrayPosts[index]
        
        postoptionsTittle.removeAll()
        
        
        if need["user_id"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")" {
            
            postoptionsTittle =  ["Edit","Delete"]
            
        }
        else{
            postoptionsTittle =  ["Report"]
            
        }
        
        showActionSheet(titleArr: postoptionsTittle as NSArray, title: "Select options") { (indx) in
            
            if indx == 1{
                if need["user_id"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")"{
                    
                    self.deleteRequestCreater(index: index)
                    
                }
                else
                {
                    //                                        let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
                    //                                        let vc = storyboard.instantiateViewController(withIdentifier: "ReportPostViewController") as! ReportPostViewController
                    //
                    //                                        vc.requestDetails =  need
                    //                                        vc.isFrom = "request_needs"
                    //                                        self.navigationController?.pushViewController(vc, animated: true)
                    
                    //Report
                    
                    
                }
            }
            else if indx == 0{
                if need["user_id"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")"{
                    
                    //Edit
                    
                    let stryboard = UIStoryboard.init(name: "PostRequest", bundle: nil)
                    let vc = stryboard.instantiateViewController(withIdentifier: "EditRequestViewController") as! EditRequestViewController
                    vc.requestDic = need
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }else{
                    let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "ReportPostViewController") as! ReportPostViewController
                    
                    vc.requestDetails =  need
                    vc.isFrom = "request_needs"
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else if indx == 100{
            }
            
        }
    }
    
    func deleteRequestCreater(index:Int){
        let alert = UIAlertController(title: "Familheey", message: "Do you really want to delete this request? Deleting this request will delete it from everywhere you posted it to", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "DELETE", style: .destructive, handler: { _ in
            let need = self.ArrayPosts[index]
            
            
            let param = ["post_request_id" : need["post_request_id"].stringValue,"user_id":UserDefaults.standard.value(forKey: "userId") as! String]
            
            ActivityIndicatorView.show("Please wait....")
            self.networkProvider.request(.need_delete(parameter: param)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                        print("view contents : \(jsonData)")
                        if response.statusCode == 200{
                            print("view contents : \(jsonData)")
                            self.offset = 0
                            if self.selectedIndex == 1{
                                self.getpublicfeed()
                            }
                            else if self.selectedIndex == 0{
                                self.getPosts()
                            }
                            else{
                                self.getrequests()
                            }
                        }
                        else if response.statusCode == 401 {
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                self.deleteRequestCreater(index: index)
                            }
                        }
                        
                        ActivityIndicatorView.hiding()
                        
                    }catch _ {
                        ActivityIndicatorView.hiding()
                    }
                case .failure( _):
                    ActivityIndicatorView.hiding()
                    break
                }
            }
            
        }))
        alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension UILabel {
    func calculateMaxLines() -> Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let text = (self.text ?? "") as NSString
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font!], context: nil)
        let linesRoundedUp = Int(ceil(textSize.height/charSize))
        return linesRoundedUp
    }
}
//
//extension HomeTabViewController : TappableImageViewDelegate{
//    func profileImageAction(id: Int) {
//        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "UserProfileTableViewController") as! UserProfileTableViewController
//        vc.userID = "\(id)"
//        self.navigationController?.pushViewController(vc, animated: true)
//    }
//}
//
extension UITableView {
    
    public var boundsWithoutInset: CGRect {
        var boundsWithoutInset = bounds
        boundsWithoutInset.origin.y += contentInset.top
        boundsWithoutInset.size.height -= contentInset.top + contentInset.bottom
        return boundsWithoutInset
    }
    
    public func isRowCompletelyVisible(at indexPath: IndexPath) -> Bool {
        let rect = rectForRow(at: indexPath)
        return boundsWithoutInset.contains(rect)
    }
}

