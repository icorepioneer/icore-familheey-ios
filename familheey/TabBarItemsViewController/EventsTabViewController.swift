//
//  EventsTabViewController.swift
//  familheey
//
//  Created by familheey on 16/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON
import Kingfisher
import Firebase
import iOSDropDown
import SideMenu
import CoreLocation


class EventsTabViewController: UIViewController,UITextFieldDelegate,CLLocationManagerDelegate {
    
    @IBOutlet weak var lblSearchResult: UILabel!
    
    @IBOutlet weak var heightForSearchText: NSLayoutConstraint!
    
    
    @IBOutlet weak var viewOfSearchResult: UIView!
    let locationManager = CLLocationManager()
    var currentLocation : String!
    var arrayOfEventListingType = ["All Events","Today","Tomorrow","Custom"]
    var selectedEventTypeList:String = ""
    var theme: SambagTheme = .light
    var fromCustomTimeStamp:String! = ""
    var toCustomTimeStamp:String! = ""
    var reduceRowHeight = false
    var offset = 0
    
    @IBOutlet weak var txtSearch: UITextField!
    
    @IBOutlet weak var lblEndDateHeading: UILabel!
    @IBOutlet weak var lblStartDateHeading: UILabel!
    @IBOutlet weak var txtStartDate: UITextField!
    @IBOutlet weak var txtEndDate: UITextField!
    var selectedStartDate:String = ""
    var selectedEndDate:String = ""
    var timetag:Int!
    var dateTag:Int!
    var selectedStartDateFormatted:String = ""
    var selectedEndDateFormatted:String = ""
    
    @IBOutlet weak var heightOfTimeSelection: NSLayoutConstraint!
    @IBOutlet weak var viewOfDateSelection: UIView!
    
    @IBOutlet weak var dropDownEventListing: DropDown!
    var ref: DatabaseReference!
    var arrayOfNotificationList = [[String:Any]]()
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    var arrayOfEventList = [String:JSON]()
    var notiUnreadCount:Int = 0
    @IBOutlet weak var lblNoti_Count: UILabel!
    @IBOutlet weak var viewOfNoti_Count: UIView!
    @IBOutlet weak var imgOfNoti_Bell: UIImageView!
    var sharedEventsArr = [JSON]()
    var nearbyEventsArr = [JSON]()
    var publicEventsArr = [JSON]()
    var inviteEventsArr = [JSON]()
    var rsvpYesEventsArr = [JSON]()
    var pastEventsArr = [JSON]()
    @IBOutlet weak var notificationViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var btnSearchReset: UIButton!
    var rsvpMaybeEventsArr = [JSON]()
    var createdEventsArr = [JSON]()
    var myEventsArr = [JSON]()
    
    var selectedIndex = Int()
    
    
    var btnFloat = UIButton(type: .custom)
    
    @IBOutlet weak var viewOfEventList: UIView!
    
    @IBOutlet weak var tblViewEventList: UITableView!
    @IBOutlet weak var btnEventSetUp: UIButton!
    var searchTxt = ""
    
    @IBOutlet weak var imgViewEvent: UIImageView!
    
    @IBOutlet weak var btnEventAdd: UIButton!
    @IBOutlet weak var lblNoDataHeading2: UILabel!
    @IBOutlet weak var lblNoDataHeading1: UILabel!
    
    @IBOutlet weak var imgExplore: UIImageView!
    @IBOutlet weak var btnExplore: UIButton!
    @IBOutlet weak var btnInviteEvents: UIButton!
    @IBOutlet weak var imgInvite: UIImageView!
    @IBOutlet weak var btnMyEvents: UIButton!
    @IBOutlet weak var imgMyEvents: UIImageView!
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var searchConstraintheight: NSLayoutConstraint!
    @IBOutlet weak var searchview: UIView!
    var previousPageXOffset: CGFloat = 0.0
    let headerHeight: CGFloat = 56
    @IBOutlet weak var notificationView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        setUserDefaultValues.setUserDefaultValue(userDefaultValue: "accessToken", userDefaultKey: "app_token")
        
        self.lblSearchResult.isHidden = true
        self.heightForSearchText.constant = 0
        let userName = UserDefaults.standard.value(forKey: "user_fullname") as! String
        if userName == ""{
            self.getUserDetails()
        }
        
        Helpers.setleftView(textfield: txtSearch, customWidth: 30)
        self.txtSearch.delegate = self
        self.viewOfDateSelection.isHidden = true
        self.heightOfTimeSelection.constant = 0
        
        self.lblStartDateHeading.attributedText = Helpers.setAttributedPlaceHolder(placeHolderName: "Start Date & Time")
        self.lblEndDateHeading.attributedText = Helpers.setAttributedPlaceHolder(placeHolderName: "End Date & Time")
        Helpers.setleftView(textfield: txtStartDate, customWidth: 15)
        Helpers.setleftView(textfield: txtEndDate, customWidth: 15)
        self.txtStartDate.delegate = self
        self.txtStartDate.delegate = self
        
//        self.lblNoti_Count.isHidden = true
        self.viewOfNoti_Count.isHidden = true
        self.tblViewEventList.dataSource = self
        self.tblViewEventList.delegate = self
        
        tblViewEventList.register(UINib(nibName: "EventListsTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        self.tblViewEventList.tableFooterView = UIView()
        var frame = CGRect.zero
        frame.size.height = .leastNormalMagnitude
        tblViewEventList.tableHeaderView = UIView(frame: frame)
        
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: 20))
        
        let button = UIButton()
        button.tag = 101
        selectedIndex = 1
        
        self.onClickTabSelection(button)
        
        Helpers.setleftView(textfield: dropDownEventListing, customWidth: 15)
        
        dropDownEventListing.text = "All Events"
        self.selectedEventTypeList = "All Events"
        //        self.apiForEventsList(from: "", to: "", eventType: "")
        
        dropDownEventListing.delegate = self
        dropDownEventListing.optionArray = self.arrayOfEventListingType
        dropDownEventListing.didSelect {(selectedText , index ,id) in
            self.selectedEventTypeList = selectedText
            if selectedText == "All Events"{
                self.viewOfDateSelection.isHidden = true
                self.heightOfTimeSelection.constant = 0
                self.fromCustomTimeStamp = ""
                self.toCustomTimeStamp = ""
                self.txtStartDate.text = ""
                self.txtEndDate.text = ""
                
                if self.selectedIndex == 0{
                    self.apiForEventsList(from: "", to: "", eventType: "")
                }
                else if self.selectedIndex == 1{
                    self.apiInviteEvents(from: "", to: "", eventType: "")
                }
                else{
                    self.apiMyEvents(from: "", to: "", eventType: "")
                }
                
            }
            /*else if selectedText == "Online"{
                self.viewOfDateSelection.isHidden = true
                self.heightOfTimeSelection.constant = 0
                self.fromCustomTimeStamp = ""
                self.toCustomTimeStamp = ""
                self.txtStartDate.text = ""
                self.txtEndDate.text = ""
                
                if self.selectedIndex == 0{
                    self.apiForEventsList(from: "", to: "", eventType: "online")
                }
                else if self.selectedIndex == 1{
                    self.apiInviteEvents(from: "", to: "", eventType: "online")
                }
                else{
                    self.apiMyEvents(from: "", to: "", eventType: "online")
                }
            }*/
            else if selectedText == "Today"
            {
                self.fromCustomTimeStamp = ""
                self.toCustomTimeStamp = ""
                self.txtStartDate.text = ""
                self.txtEndDate.text = ""
                self.viewOfDateSelection.isHidden = true
                self.heightOfTimeSelection.constant = 0
                self.todayConvertFunc()
            }
            else if selectedText == "Tomorrow"
            {
                self.fromCustomTimeStamp = ""
                self.toCustomTimeStamp = ""
                self.txtStartDate.text = ""
                self.txtEndDate.text = ""
                self.viewOfDateSelection.isHidden = true
                self.heightOfTimeSelection.constant = 0
                self.tomorrowConvertFunc()
            }
            else if selectedText == "Custom"
            {
                self.viewOfDateSelection.isHidden = false
                self.heightOfTimeSelection.constant = 70
                self.txtStartDate.text = ""
                self.txtEndDate.text = ""
                self.fromCustomTimeStamp = ""
                self.toCustomTimeStamp = ""
            }
            
            
        }
        
        closeSearch()
        setupRightView()
        //        self.tblViewEventList.contentInset = UIEdgeInsets(top: -10, left: 0, bottom: 0, right: 0)
        NotificationCenter.default.addObserver(self, selector: #selector(childNodeAdded(notification:)), name: Notification.Name("ChildAdded"), object: nil)
    }
    
    func setupRightView(){
         let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
           let vc = storyboard.instantiateViewController(withIdentifier: "topicListingViewController") as! topicListingViewController

         let rightMenuNavigationController = SideMenuNavigationController(rootViewController: vc)
             SideMenuManager.default.leftMenuNavigationController = rightMenuNavigationController
             SideMenuManager.default.addPanGestureToPresent(toView: self.view)
             SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: self.view)
             rightMenuNavigationController.statusBarEndAlpha = 0
             rightMenuNavigationController.menuWidth = self.view.frame.width
             rightMenuNavigationController.dismissOnPush = false
             rightMenuNavigationController.dismissOnPresent = false
             rightMenuNavigationController.pushStyle = .subMenu
    }
    
     @objc func childNodeAdded(notification: Notification) {
        self.NotificationArrayListing()
    }
    
    func NotificationArrayListing(){
        let userId = UserDefaults.standard.value(forKey: "userId") as! String
        
        let noti_String :String = "\(Helpers.notificationUploadstring)\(userId)_notification"
        let ref = Database.database().reference(withPath: noti_String).queryLimited(toLast: 1200)
        ref.observeSingleEvent(of: .value, with: { snapshot in
            
            if !snapshot.exists() {
                self.arrayOfNotificationList = []
                appDel.arrayOfNotificationList = []
                appDel.tempNotifi = []
                return
                
            }
            
            self.arrayOfNotificationList = []
            appDel.arrayOfNotificationList = []
            appDel.tempNotifi = []
            let dict:[String:Any] = snapshot.value as! [String:Any]
            
            for (key, value) in dict {
                var dictionary = [String:Any]()
                dictionary["key"] = key
                dictionary["value"] = value as! [String:Any]
                appDel.arrayOfNotificationList.append(dictionary)
                appDel.tempNotifi.append(value as! [String:Any])
            }
            
            //            appDel.arrayOfNotificationList = self.arrayOfNotificationList
            self.arrayOfNotificationList = appDel.arrayOfNotificationList
            
            self.notiUnreadCount = 0
            for data in self.arrayOfNotificationList{
                let dict  = data["value"] as! [String:Any]
                if dict["visible_status"] as! String == "unread"{
                    self.notiUnreadCount = self.notiUnreadCount + 1
                }
            }
            
            if self.notiUnreadCount == 0
            {
                self.lblNoti_Count.isHidden = true
                self.viewOfNoti_Count.isHidden = true
            }
            else{
                self.lblNoti_Count.isHidden = false
                self.viewOfNoti_Count.isHidden = false
                if self.notiUnreadCount > 99{
                    self.lblNoti_Count.text = "99+"
                }
                else{
                    self.lblNoti_Count.text = "\(self.notiUnreadCount)"
                }
            }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.isTranslucent = false
        
        if UserDefaults.standard.value(forKey: "userProfile") != nil{
            let pic = UserDefaults.standard.value(forKey: "userProfile") as! String
            let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+pic.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let imgUrl = URL(string: temp)
            let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+imgUrl!.relativeString
            let url = URL(string: newUrlStr)
            imgProfile.kf.indicatorType = .activity
            
            imgProfile.kf.setImage(with: url, placeholder: UIImage(named: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        else{
            imgProfile.image = UIImage(named: "Male Colored")
        }
        
        if appDel.isFromEventDetails{
            
        }
        else{
            dropDownEventListing.text = "All Events"
            self.selectedEventTypeList = "All Events"
            //            self.apiForEventsList(from: "", to: "", eventType: "")
            dropDownEventListing.delegate = self
            dropDownEventListing.optionArray = self.arrayOfEventListingType
            dropDownEventListing.didSelect {(selectedText , index ,id) in
                self.selectedEventTypeList = selectedText
                
                if selectedText == "All Events"
                {
                    self.viewOfDateSelection.isHidden = true
                    self.heightOfTimeSelection.constant = 0
                    self.fromCustomTimeStamp = ""
                    self.toCustomTimeStamp = ""
                    self.txtStartDate.text = ""
                    self.txtEndDate.text = ""
                    if self.selectedIndex == 0{
                        self.apiForEventsList(from: "", to: "", eventType: "")
                    }
                    else if self.selectedIndex == 1{
                        self.apiInviteEvents(from: "", to: "", eventType: "")
                    }
                    else{
                        self.apiMyEvents(from: "", to: "", eventType: "")
                    }
                    
                }
               /* else if selectedText == "Online"{
                    self.viewOfDateSelection.isHidden = true
                    self.heightOfTimeSelection.constant = 0
                    self.fromCustomTimeStamp = ""
                    self.toCustomTimeStamp = ""
                    self.txtStartDate.text = ""
                    self.txtEndDate.text = ""
                    
                    if self.selectedIndex == 0{
                        self.apiForEventsList(from: "", to: "", eventType: "online")
                    }
                    else if self.selectedIndex == 1{
                        self.apiInviteEvents(from: "", to: "", eventType: "online")
                    }
                    else{
                        self.apiMyEvents(from: "", to: "", eventType: "online")
                    }
                }*/
                else if selectedText == "Today"
                {
                    self.fromCustomTimeStamp = ""
                    self.toCustomTimeStamp = ""
                    self.txtStartDate.text = ""
                    self.txtEndDate.text = ""
                    self.viewOfDateSelection.isHidden = true
                    self.heightOfTimeSelection.constant = 0
                    self.todayConvertFunc()
                }
                else if selectedText == "Tomorrow"
                {
                    self.fromCustomTimeStamp = ""
                    self.toCustomTimeStamp = ""
                    self.txtStartDate.text = ""
                    self.txtEndDate.text = ""
                    self.viewOfDateSelection.isHidden = true
                    self.heightOfTimeSelection.constant = 0
                    self.tomorrowConvertFunc()
                }
                else if selectedText == "Custom"
                {
                    self.viewOfDateSelection.isHidden = false
                    self.heightOfTimeSelection.constant = 70
                    self.txtStartDate.text = ""
                    self.txtEndDate.text = ""
                    self.fromCustomTimeStamp = ""
                    self.toCustomTimeStamp = ""
                }
            }
            
        }
        
        
        if selectedEventTypeList == "All Events"{
            
            if self.selectedIndex == 0{
                self.apiForEventsList(from: "", to: "", eventType: "")
            }
            else if self.selectedIndex == 1{
                self.apiInviteEvents(from: "", to: "", eventType: "")
            }
            else{
                self.apiMyEvents(from: "", to: "", eventType: "")
            }
            //                self.apiForEventsList(from: "", to: "")
            
        }
        else if selectedEventTypeList == "Online"{
            if self.selectedIndex == 0{
                self.apiForEventsList(from: "", to: "", eventType: "online")
            }
            else if self.selectedIndex == 1{
                self.apiInviteEvents(from: "", to: "", eventType: "online")
            }
            else{
                self.apiMyEvents(from: "", to: "", eventType: "online")
            }
        }
        else if selectedEventTypeList == "Today"
        {
            
            self.todayConvertFunc()
        }
        else if selectedEventTypeList == "Tomorrow"
        {
            
            self.tomorrowConvertFunc()
        }
        else if selectedEventTypeList == "Custom"
        {
            
        }
        
        self.NotificationArrayListing()
        
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        appDel.isFromEventDetails = false
        self.tabBarController?.tabBar.isTranslucent = true
    }
    
    
    //MARK:- Floating Buton
    func setFloatingButton(){
        btnFloat.frame = CGRect(x: UIScreen.main.bounds.size.width - 75, y: UIScreen.main.bounds.size.height - 150, width: 50, height: 50)
        btnFloat.setTitle(" + ", for: .normal)
        btnFloat.setTitleColor(UIColor.black, for: .normal)
        btnFloat.backgroundColor = UIColor.groupTableViewBackground
        btnFloat.clipsToBounds = true
        btnFloat.layer.cornerRadius = 25
        btnFloat.addTarget(self,action: #selector(onclickFloatingAction), for: UIControl.Event.touchUpInside)
        self.view.addSubview(btnFloat)
        
    }
    func openSearch(){
        self.notificationView.isHidden = false
        self.notificationViewHeight.constant = self.headerHeight
        self.searchview.isHidden = false
        self.searchConstraintheight.constant = self.headerHeight
        
        self.btnSearchReset.isHidden = false
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
            self.searchview.alpha = 1.0
        }, completion: nil)
    }
    
    func closeSearch(){
        DispatchQueue.main.async {
            
            UIView.animate(withDuration: 0.5, animations: {
                self.searchview.alpha = 0.0
                
            }) { (true) in
                
                self.searchview.isHidden = true
                self.searchConstraintheight.constant = 0
                self.notificationView.isHidden = false
                self.notificationViewHeight.constant = self.headerHeight
                
            }
        }
    }
    
    //MARK:- Button Ations
    @IBAction func searchAction(_ sender: Any) {
        openSearch()
    }
    @IBAction func showSambagDatePickerViewController(_ sender: UIButton) {
        
        if sender.tag == 0
        {
            self.txtEndDate.text = ""
            self.dateTag = 0
            let vc = SambagDatePickerViewController()
            vc.theme = theme
            vc.delegate = self
            present(vc, animated: true, completion: nil)
            
        }
        else
        {
            if self.txtStartDate.text == ""
            {
                Helpers.showAlertDialog(message: "Please select Start Date", target: self)
                
            }
            else
            {
                self.dateTag = 1
                let vc = SambagDatePickerViewController()
                vc.theme = theme
                vc.delegate = self
                present(vc, animated: true, completion: nil)
                
            }
        }
    }
    
    
    @IBAction func btnDoneOnCustomDateSelection_OnClick(_ sender: Any) {
        if txtStartDate.text == "" {
            
            Helpers.showAlertDialog(message: "Start Date is empty", target: self)
            
        }
        else if txtEndDate.text == "" {
            
            Helpers.showAlertDialog(message: "End Date is empty", target: self)
            
            
        }
        else
        {
            
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy hh:mm a"
            let convertedDate = formatter.date(from: self.selectedStartDate)
            self.fromCustomTimeStamp = self.getCurrentTimeStampWOMiliseconds(dateToConvert: convertedDate! as NSDate)
            
            //            print(self.fromCustomTimeStamp!)
            //        var nextDate1 = NSDate()
            //        nextDate1 = now.adding(hour: 1, minutes: 59) as NSDate
            let nextDate1 = formatter.date(from: self.selectedEndDate)
            //            print(nextDate1!)
            self.toCustomTimeStamp = self.getCurrentTimeStampWOMiliseconds(dateToConvert: nextDate1! as NSDate)
            
            //            print(self.toCustomTimeStamp!)
            
            
            
            
            if self.selectedIndex == 0{
                self.apiForEventsList(from: "\(fromCustomTimeStamp!)", to: "\(toCustomTimeStamp!)", eventType: "")
            }
            else if self.selectedIndex == 1
            {
                apiInviteEvents(from: "\(fromCustomTimeStamp!)", to: "\(toCustomTimeStamp!)", eventType: "")
                
            }
            else if self.selectedIndex == 2
            {
                apiMyEvents(from: "\(fromCustomTimeStamp!)", to: "\(toCustomTimeStamp!)", eventType: "")
                
            }
            
        }
    }
    @IBAction func btnNotificationOnClick(_ sender: Any) {
        self.lblNoti_Count.isHidden = true
        self.viewOfNoti_Count.isHidden = true
        //        appDel.firstTimeNot = true
        let storyboard = UIStoryboard.init(name: "third", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        vc.arrayOfNotificationList = self.arrayOfNotificationList
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnAddEvents_Onclick(_ sender: Any) {
        
        let storyboard = UIStoryboard.init(name: "third", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CreateEventStep1ViewController") as! CreateEventStep1ViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func onclickFloatingAction(){
        self.view.sendSubviewToBack(btnFloat)
        let storyboard = UIStoryboard.init(name: "third", bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: "CreateEventStep1ViewController") as! CreateEventStep1ViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func onClickTabSelection(_ sender: UIButton){
        if sender.tag == 102{
            self.selectedIndex = 2
            if self.txtSearch.text!.isEmpty{
                self.closeSearch()
                
            }
            else{
                self.openSearch()
            }
            self.selectWebAPI()
            btnExplore.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            btnMyEvents.setTitleColor(UIColor(named: "greenBackgrounf"), for: .normal)
            btnInviteEvents.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            
            imgExplore.backgroundColor  = UIColor(named: "lightGrayTextColor")
            imgMyEvents.backgroundColor  = UIColor(named: "greenBackgrounf")
            imgInvite.backgroundColor  = UIColor(named: "lightGrayTextColor")
        }
        else if sender.tag == 101{
            self.selectedIndex = 1
            if self.txtSearch.text!.isEmpty{
                self.closeSearch()
                
            }
            else{
                self.openSearch()
            }
            self.selectWebAPI()
            btnExplore.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            btnMyEvents.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            btnInviteEvents.setTitleColor(UIColor(named: "greenBackgrounf"), for: .normal)
            
            imgExplore.backgroundColor  = UIColor(named: "lightGrayTextColor")
            imgMyEvents.backgroundColor  = UIColor(named: "lightGrayTextColor")
            imgInvite.backgroundColor  = UIColor(named: "greenBackgrounf")
        }
        else{
            self.selectedIndex = 0
            if self.txtSearch.text!.isEmpty{
                self.closeSearch()
                
            }
            else{
                self.openSearch()
            }
            self.selectWebAPI()
            btnExplore.setTitleColor(UIColor(named: "greenBackgrounf"), for: .normal)
            btnMyEvents.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            btnInviteEvents.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            
            imgExplore.backgroundColor  = UIColor(named: "greenBackgrounf")
            imgMyEvents.backgroundColor  = UIColor(named: "lightGrayTextColor")
            imgInvite.backgroundColor  = UIColor(named: "lightGrayTextColor")
            
            appDel.calendarFromDate = Int()
            appDel.calendarToDate   = Int()
        }
        
        tblViewEventList.reloadData()
    }
    
    
    @IBAction func onClickProfilePic(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "UserProfileTableViewController") as! UserProfileTableViewController
        
        var userId = UserDefaults.standard.value(forKey: "userId") as! String
        vc.userID = userId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickShareAction(_ sender: subclassedUIButton){
        var data = JSON()
        var isShare = ""
        //        print(sender.indexPath!)
        //        print(sender.indexSection!)
        if selectedIndex == 0{
            if sender.indexSection == 0{
                data = self.sharedEventsArr[sender.indexPath!]
            }
            else if sender.indexSection == 1{
                data = self.nearbyEventsArr[sender.indexPath!]
            }
            else{
                data = self.publicEventsArr[sender.indexPath!]
            }
            if data["public_event"].boolValue{
                isShare = "share"
            }
            else{
                isShare = "invitation"
            }
        }
        else if selectedIndex == 1{
            data = inviteEventsArr[sender.indexPath!]
            if data["public_event"].boolValue{
                isShare = "share"
            }
            else{
                isShare = "invitation"
            }
        }
        else{
            
            
            if sender.indexSection == 0{
                data = self.createdEventsArr[sender.indexPath!]
            }
            else if sender.indexSection == 1{
                data = self.rsvpYesEventsArr[sender.indexPath!]
            }
            else{
                data = self.rsvpMaybeEventsArr[sender.indexPath!]
            }
            if data["is_public"].boolValue{
                isShare = "share"
            }
            else{
                isShare = "invitation"
            }
        }
        
        
        //        print(data)
        
        
        let stryboard = UIStoryboard.init(name: "Main", bundle: nil)
        
        let vc = stryboard.instantiateViewController(withIdentifier: "EventShareViewController") as! EventShareViewController
        
        vc.eventId = "\(data["event_id"])"
        
        vc.typeOfInvitations = isShare
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func onClickShareOverViewAction(_ sender: UIButton){
        
        var data = JSON()
        
        data = self.sharedEventsArr[sender.tag]
        
        //        print(data)
        
        let stryboard = UIStoryboard.init(name: "fourth", bundle: nil)
        
        let vc = stryboard.instantiateViewController(withIdentifier: "UserFamilyListViewController") as! UserFamilyListViewController
        vc.isFamilies = false
        vc.eventID = "\(data["event_id"])"
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    @IBAction func onClickCal(_ sender: Any) {
        
        let stryboard = UIStoryboard.init(name: "third", bundle: nil)
        
        let vc = stryboard.instantiateViewController(withIdentifier: "CalendarEventsViewController") as! CalendarEventsViewController
        vc.isFromExplore = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: - Feed back
    @IBAction func feedBackClicked(_ sender: Any) {
        
        let  postoptionsTittle =  ["Calendar","Announcements","Messages","Feedback","Quick tour"]
        
        showActionSheet(titleArr: postoptionsTittle as NSArray, title: "Select options") { (index) in
            if index == 0{
                self.viewCalendar()
            }
            else if index == 1{
                self.viewAnnouncements()
            }
            else if index == 2{
                self.viewMessages()
            }
            else if index == 100{
            }
            else if index == 4{
                self.viewSubscription()
            }
            else if index == 3{
                self.sendFeedBack()
            }
            else{
            }
        }
    }
    
    func sendFeedBack(){
        let stryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = stryboard.instantiateViewController(withIdentifier: "FeedbackViewController") as! FeedbackViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func viewSubscription(){
        let stryboard = UIStoryboard.init(name: "Subscription", bundle: nil)
        let vc = stryboard.instantiateViewController(withIdentifier: "WalkthorughViewController") as! WalkthorughViewController
        appDel.quickTour = true
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func viewAnnouncements(){
        let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsViewController") as! AnnouncementsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func viewCalendar(){
        let stryboard = UIStoryboard.init(name: "third", bundle: nil)
        let vc = stryboard.instantiateViewController(withIdentifier: "CalendarEventsViewController") as! CalendarEventsViewController
        vc.isFromExplore = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func viewMessages(){
        let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "topicListingViewController") as! topicListingViewController
        
        let rightMenuNavigationController = SideMenuNavigationController(rootViewController: vc)
        SideMenuManager.default.leftMenuNavigationController = rightMenuNavigationController
        SideMenuManager.default.addPanGestureToPresent(toView: self.view)
        SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: self.view)
        rightMenuNavigationController.statusBarEndAlpha = 0
        rightMenuNavigationController.menuWidth = self.view.frame.width
        rightMenuNavigationController.dismissOnPush = false
        rightMenuNavigationController.dismissOnPresent = false
        rightMenuNavigationController.pushStyle = .subMenu
        present(rightMenuNavigationController, animated: true, completion: nil)
    }
    
    @IBAction func onClickSearchReset(_ sender: Any) {
        txtSearch.text = ""
        searchTxt = ""
        selectWebAPI()
        closeSearch()
        //        btnSearchReset.isHidden = true
        self.txtSearch.endEditing(true)
    }
    //MARK:- WEB API
    func apiForEventsList(from : String, to: String ,eventType:String) {
        let parameter = ["user_id":UserDefaults.standard.value(forKey: "userId") as! String, "from_date" : from, "to_date" : to,"query":txtSearch.text!.trimmingCharacters(in: .whitespacesAndNewlines),"filter":eventType,"offset":offset,"limit":5] as [String : Any]
        print(parameter)
        
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.EventList(parameter: parameter), completion: {(result) in
            switch result {
            case .success(let response):
                do {
                    let jsonData = JSON(response.data as Any)
                    
                    if response.statusCode == 200{
                        self.arrayOfEventList = jsonData["data"].dictionaryValue
                        print(self.arrayOfEventList)
                        
                        if self.arrayOfEventList.count != 0
                        {
                            self.imgViewEvent.isHidden = true
                            self.lblNoDataHeading1.isHidden = true
                            self.lblNoDataHeading2.isHidden = true
                            self.btnEventSetUp.isHidden = true
                            
                            
                            self.sharedEventsArr = self.arrayOfEventList["explore"]!["sharedEvents"].arrayValue
                            self.nearbyEventsArr = self.arrayOfEventList["explore"]!["basedOnLocation"].arrayValue
                            self.publicEventsArr = self.arrayOfEventList["explore"]!["publicEvents"].arrayValue
                            
                            
                            //                            print(self.sharedEventsArr)
                            
                            
                            
                            if self.sharedEventsArr.count == 0 && self.nearbyEventsArr.count == 0 && self.publicEventsArr.count == 0{
                                self.lblNoDataHeading1.isHidden = false
                                self.btnEventSetUp.isHidden = true
                                
                                
                                if self.txtSearch.text!.isEmpty{
                                    self.lblSearchResult.isHidden = true
                                    self.heightForSearchText.constant = 0
                                    self.lblNoDataHeading2.isHidden = false
                                    self.imgViewEvent.isHidden = false
                                    
                                    self.lblNoDataHeading1.text = "No event suggestions"
                                    self.lblNoDataHeading2.text = "Please check back later."
                                }
                                else{
                                    self.lblSearchResult.isHidden = false
                                    self.lblNoDataHeading2.isHidden = true
                                    self.imgViewEvent.isHidden = false
                                    
                                    self.lblNoDataHeading1.text = "No results to show"
                                    
                                    self.heightForSearchText.constant = 25
                                    self.lblSearchResult.text = "Search result for \"\(self.txtSearch.text!)\""
                                }
                                self.tblViewEventList.isHidden = true
                                self.viewOfSearchResult.backgroundColor = .white
                                
                                self.btnEventAdd.isHidden = true
                            }
                            else{
                                self.tblViewEventList.isHidden = false
                                self.viewOfSearchResult.backgroundColor = self.tblViewEventList.backgroundColor
                                
                                //                                self.btnEventAdd.isHidden = false
                                if self.txtSearch.text!.isEmpty{
                                    self.lblSearchResult.isHidden = true
                                    self.heightForSearchText.constant = 0
                                }
                                else{
                                    self.lblSearchResult.isHidden = false
                                    self.heightForSearchText.constant = 25
                                    self.lblSearchResult.text = "Search result for \"\(self.txtSearch.text!)\""
                                }
                                
                                self.tblViewEventList.reloadData()
                                //<<<<<<< HEAD
                                //                                //                                self.tblViewEventList.scrollToRow(at:  IndexPath(row: 0, section: 0), at: .top, animated: false)
                                //                                self.closeSearch()
                                
                                //                                self.tblViewEventList.scrollToRow(at:  IndexPath(row: 0, section: 0), at: .top, animated: false)
                                if self.txtSearch.text!.isEmpty{
                                    self.closeSearch()
                                    
                                }
                                else{
                                    self.openSearch()
                                }
                                
                            }
                        }
                        else{
                            //                          self.viewOfEventList.isHidden = false
                            self.lblNoDataHeading1.isHidden = false
                            if self.txtSearch.text!.isEmpty{
                                self.lblSearchResult.isHidden = true
                                self.heightForSearchText.constant = 0
                                self.lblNoDataHeading2.isHidden = false
                                self.imgViewEvent.isHidden = false
                                
                                self.lblNoDataHeading1.text = "You have not received any events yet"
                                self.lblNoDataHeading2.text = "Please check back later."
                            }
                            else{
                                self.lblSearchResult.isHidden = false
                                self.heightForSearchText.constant = 25
                                self.lblSearchResult.text = "Search result for \"\(self.txtSearch.text!)\""
                                self.lblNoDataHeading2.isHidden = true
                                self.imgViewEvent.isHidden = false
                                
                                self.lblNoDataHeading1.text = "No results to show"
                                
                                
                            }
                            
                            
                            
                            self.btnEventSetUp.isHidden = true
                            
                            self.tblViewEventList.isHidden = true
                            self.viewOfSearchResult.backgroundColor = .white
                            
                            self.btnEventAdd.isHidden = true
                        }
                        
                        //                        if self.sharedEventsArr.count == 00 && self.nearbyEventsArr.count == 0 && self.publicEventsArr.count == 0{
                        //
                        //                            self.imgViewEvent.isHidden = false
                        //                            self.lblNoDataHeading1.isHidden = false
                        //                            self.lblNoDataHeading2.isHidden = false
                        //                            self.btnEventSetUp.isHidden = true
                        //
                        //                            self.tblViewEventList.isHidden = true
                        //                        }
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.apiMyEvents(from: from, to: to, eventType: eventType)
                        }
                    }
                    else{
                        //                        print(jsonData["code"])
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                } catch let err {
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
    func apiInviteEvents(from : String, to: String,eventType:String){
        let parameter = ["user_id":UserDefaults.standard.value(forKey: "userId") as! String, "from_date" : from, "to_date" : to,"query":txtSearch.text!.trimmingCharacters(in: .whitespacesAndNewlines),"filter":eventType] as [String : Any]
        ActivityIndicatorView.show("Loading....")
        
        networkProvider.request(.InviteEvents(parameter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data as Any)
                    print(jsonData)
                    ActivityIndicatorView.hiding()
                    if response.statusCode == 200{
                        //                        print(jsonData)
                        self.inviteEventsArr = jsonData["data"].arrayValue
                        print(self.inviteEventsArr)
                        if self.inviteEventsArr.count != 0
                        {
                            
                            self.imgViewEvent.isHidden = true
                            self.lblNoDataHeading1.isHidden = true
                            self.lblNoDataHeading2.isHidden = true
                            self.btnEventSetUp.isHidden = true
                            if self.txtSearch.text!.isEmpty{
                                self.lblSearchResult.isHidden = true
                                self.heightForSearchText.constant = 0
                                
                                
                            }
                            else{
                                self.lblSearchResult.isHidden = false
                                self.heightForSearchText.constant = 25
                                self.lblSearchResult.text = "Search result for \"\(self.txtSearch.text!)\""
                                
                            }
                            
                            
                            self.tblViewEventList.isHidden = false
                            self.viewOfSearchResult.backgroundColor = self.tblViewEventList.backgroundColor
                            
                            self.btnEventAdd.isHidden = true
                            self.tblViewEventList.reloadData()
                            //<<<<<<< HEAD
                            //                            //                            self.tblViewEventList.scrollToRow(at:  IndexPath(row: 0, section: 0), at: .top, animated: false)
                            //                            self.closeSearch()
                            //                            self.tblViewEventList.scrollToRow(at:  IndexPath(row: 0, section: 0), at: .top, animated: false)
                            if self.txtSearch.text!.isEmpty{
                                self.closeSearch()
                                
                            }
                            else{
                                self.openSearch()
                            }
                            
                        }
                        else{
                            
                            
                            
                            
                            if self.txtSearch.text!.isEmpty{
                                self.lblSearchResult.isHidden = true
                                self.heightForSearchText.constant = 0
                                self.lblNoDataHeading1.isHidden = false
                                self.lblNoDataHeading2.isHidden = false
                                self.imgViewEvent.isHidden = false
                                self.lblNoDataHeading1.text = "You have not received any invitations yet"
                                self.lblNoDataHeading2.text = "Invitations will be shown here when you do."
                                
                                
                            }
                            else{
                                self.lblSearchResult.isHidden = false
                                self.heightForSearchText.constant = 25
                                self.lblSearchResult.text = "Search result for \"\(self.txtSearch.text!)\""
                                
                                self.lblNoDataHeading1.isHidden = false
                                self.lblNoDataHeading2.isHidden = true
                                self.imgViewEvent.isHidden = false
                                self.lblNoDataHeading1.text = "No results to show"
                            }
                            self.btnEventSetUp.isHidden = true
                            self.viewOfSearchResult.backgroundColor = .white
                            
                            self.tblViewEventList.isHidden = true
                            self.btnEventAdd.isHidden = true
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.apiInviteEvents(from: from, to: to, eventType: eventType)
                        }
                    }

                    
                }catch let err {
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    func apiMyEvents(from : String, to: String,eventType:String){
        let parameter = ["user_id":UserDefaults.standard.value(forKey: "userId") as! String, "from_date" : from, "to_date" : to,"query":txtSearch.text!.trimmingCharacters(in: .whitespacesAndNewlines),"filter":eventType] as [String : Any]
        print(parameter)
        
        ActivityIndicatorView.show("Loading....")
        
        networkProvider.request(.CreatedMeEvents(parameter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data as Any)
                    ActivityIndicatorView.hiding()
                    if response.statusCode == 200{
                        self.arrayOfEventList = jsonData["data"].dictionaryValue
                        print(self.arrayOfEventList)
                        
                        self.createdEventsArr = self.arrayOfEventList["my_event_list"]!["created_by_me"].arrayValue
                        self.rsvpYesEventsArr = self.arrayOfEventList["my_event_list"]!["rsvp_yes"].arrayValue
                        self.rsvpMaybeEventsArr = self.arrayOfEventList["my_event_list"]!["rsvp_maybe"].arrayValue
                        self.pastEventsArr = self.arrayOfEventList["past_event_list"]!["data"].arrayValue
                        
                        
                        self.myEventsArr = []
                        self.myEventsArr.append(contentsOf: self.createdEventsArr)
                        self.myEventsArr.append(contentsOf: self.rsvpYesEventsArr)
                        self.myEventsArr.append(contentsOf: self.rsvpMaybeEventsArr)
                        self.myEventsArr.append(contentsOf: self.pastEventsArr)
                        
                        //                        print(self.myEventsArr)
                        
                        if self.myEventsArr.count != 0
                        {
                            self.imgViewEvent.isHidden = true
                            self.lblNoDataHeading1.isHidden = true
                            self.lblNoDataHeading2.isHidden = true
                            self.btnEventSetUp.isHidden = true
                            
                            
                            if self.txtSearch.text!.isEmpty{
                                self.lblSearchResult.isHidden = true
                                self.heightForSearchText.constant = 0
                                
                                
                            }
                            else{
                                self.lblSearchResult.isHidden = false
                                self.heightForSearchText.constant = 25
                                self.lblSearchResult.text = "Search result for \"\(self.txtSearch.text!)\""
                                
                            }
                            
                            
                            self.viewOfSearchResult.backgroundColor = self.tblViewEventList.backgroundColor
                            
                            self.tblViewEventList.isHidden = false
                            self.tblViewEventList.reloadData()
                            //<<<<<<< HEAD
                            //                            //                            self.tblViewEventList.scrollToRow(at:  IndexPath(row: 0, section: 0), at: .top, animated: false)
                            //                            self.closeSearch()
                            //                            self.tblViewEventList.scrollToRow(at:  IndexPath(row: 0, section: 0), at: .top, animated: false)
                            if self.txtSearch.text!.isEmpty{
                                self.closeSearch()
                                
                            }
                            else{
                                self.openSearch()
                            }
                            
                        }
                        else{
                            
                            
                            
                            
                            if self.txtSearch.text!.isEmpty{
                                self.lblSearchResult.isHidden = true
                                self.heightForSearchText.constant = 0
                                self.lblNoDataHeading2.isHidden = false
                                self.lblNoDataHeading1.isHidden = false
                                self.btnEventSetUp.isHidden = false
                                self.lblNoDataHeading1.text = "No events yet? let's add a few..."
                                self.lblNoDataHeading2.text = "Simply set your 1st event up!"
                                self.imgViewEvent.isHidden = false
                                
                                
                            }
                            else{
                                self.lblSearchResult.isHidden = false
                                self.heightForSearchText.constant = 25
                                self.lblSearchResult.text = "Search result for \"\(self.txtSearch.text!)\""
                                self.lblNoDataHeading1.isHidden = false
                                self.lblNoDataHeading1.text = "No results to show"
                                self.lblNoDataHeading2.isHidden = true
                                self.btnEventSetUp.isHidden = true
                                self.imgViewEvent.isHidden = false
                                
                            }
                            
                            
                            self.tblViewEventList.isHidden = true
                            self.viewOfSearchResult.backgroundColor = .white
                            
                            self.btnEventAdd.isHidden = true
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.apiMyEvents(from: from, to: to, eventType: eventType)
                        }
                    }

                    
                }catch let err {
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
        
    }
    
    
    //MARK: - Calendar Date conversions
    func getCurrentTimeStampWOMiliseconds(dateToConvert: NSDate) -> String {
        let objDateformat: DateFormatter = DateFormatter()
        objDateformat.dateFormat = "yyyy-MM-dd HH:mm"
        
        let strTime: String = objDateformat.string(from: dateToConvert as Date)
        let objUTCDate: NSDate = objDateformat.date(from: strTime)! as NSDate
        let milliseconds: Int64 = Int64(objUTCDate.timeIntervalSince1970)
        let strTimeStamp: String = "\(milliseconds)"
        return strTimeStamp
    }
    
    
    func todayConvertFunc(){
        let now = Date()
        let nowTimeStamp1 = self.getCurrentTimeStampWOMiliseconds(dateToConvert: now as NSDate)
        
        //        print(nowTimeStamp1)
        var nextDate1 = NSDate()
        nextDate1 = now.adding(hour: 1, minutes: 59) as NSDate
        let nextTimeStamp1 = self.getCurrentTimeStampWOMiliseconds(dateToConvert: nextDate1 as NSDate)
        
        //        print(nextTimeStamp1)
        
        
        if self.selectedIndex == 0{
            self.apiForEventsList(from: "", to: "", eventType: "today")
        }
        else if self.selectedIndex == 1{
            self.apiInviteEvents(from: "", to: "", eventType: "today")
        }
        else{
            self.apiMyEvents(from: "", to: "", eventType: "today")
        }
        
    }
    
    
    func tomorrowConvertFunc(){
        
        var dateVal                       = Date()
        dateVal                           = dateVal.adding(hour: 0, minutes: 0)
        //        print("Date with time : \(dateVal)")
        
        let datVal2                       = dateVal.adding(hour: 24, minutes: 0)
        
        //        print("date val : \(convertTimetoTimestamp(date: dateVal))")
        
        
        
        if self.selectedIndex == 0{
            self.apiForEventsList(from: "", to: "", eventType: "tomorrow")
        }
        else if self.selectedIndex == 1{
            self.apiInviteEvents(from: "", to: "", eventType: "tomorrow")
        }
        else{
            self.apiMyEvents(from: "", to: "", eventType: "tomorrow")
        }
        
    }
    
    //MARK: - Convert Date
    
    func convertTimetoTimestamp(date:Date)->Int
    {
        
        let timestamp = date.timeIntervalSince1970
        
        return Int(timestamp)
        
    }
    
    //MARK:- Custom methods
    static  func convertTimeStampToDate(timestamp:String)->String
    {
        if timestamp != ""
        {
            let date = Date(timeIntervalSince1970: Double(timestamp)!)
            
            let formatter = DateFormatter()
            formatter.dateFormat = "MMM dd yyyy hh:mm a"
            //        formatter.timeZone = TimeZone(identifier: "UTC")
            formatter.timeZone = TimeZone.current
            
            
            let localDatef = formatter.string(from: date)
            //            print(localDatef)
            return localDatef
        }
        else
        {
            return ""
        }
        
    }
    
    static func convertTimeStamp(timestamp:String) -> String {
        let date = Date(timeIntervalSince1970: Double(timestamp) as! TimeInterval)
        //    dateFormatter.dateFormat       = "EEE MMM dd yyy"
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MMM/yyyy hh:mma"
        //    formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.timeZone = TimeZone.current
        
        
        let localDatef = formatter.string(from: date)
        //        print(localDatef)
        return localDatef
    }
    
    func filterDayOnly(timestamp:String) -> String {
        let date = Date(timeIntervalSince1970: Double(timestamp) as! TimeInterval)
        //    dateFormatter.dateFormat       = "EEE MMM dd yyy"
        
        let formatter = DateFormatter()
        formatter.dateFormat = "EEE"
        //        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.timeZone = TimeZone.current
        
        
        let localDatef = formatter.string(from: date)
        //        print(localDatef)
        return localDatef
    }
    func getUserDetails(){
        let crntUser = UserDefaults.standard.value(forKey: "userId") as! String
        ActivityIndicatorView.show("Please wait!")
        APIServiceManager.callServer.getUserDetails(url: EndPoint.userDetails, userId: crntUser, profileId: crntUser ,success: { (response) in
            
            ActivityIndicatorView.hiding()
            guard let loginMdl = response as? ExternalUserModel else{
                return
            }
            
            
            UserDefaults.standard.set(loginMdl.User?.fullname, forKey: "user_fullname")
            
            if loginMdl.User?.propic.count != 0 {
                UserDefaults.standard.set(loginMdl.User?.propic, forKey: "userProfile")
                let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+(loginMdl.User!.propic).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                let imgUrl = URL(string: temp)
                
                let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+imgUrl!.relativeString
                let urlImg = URL(string: newUrlStr)
                self.imgProfile.kf.indicatorType = .activity
                self.imgProfile.kf.setImage(with: urlImg, placeholder: #imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
            }
            else{
                self.imgProfile.image = #imageLiteral(resourceName: "Male Colored")
            }
            
            
            
        }) { (error) in
            ActivityIndicatorView.hiding()
            
        }
        
    }
    
    func selectWebAPI(){
        //        self.view.endEditing(true)
        //        self.searchBar.endEditing(true)
        if selectedIndex == 0{
            if self.selectedEventTypeList == "All Events"
            {
                apiForEventsList(from: "", to: "", eventType: "")
            }
            else if self.selectedEventTypeList == "Today"
            {
                self.todayConvertFunc()
            }
            else if self.selectedEventTypeList == "Tomorrow"
            {
                self.tomorrowConvertFunc()
            }
            else
            {
                self.apiForEventsList(from: "\(fromCustomTimeStamp!)", to: "\(toCustomTimeStamp!)", eventType: "")
                
            }
            
        }
        else if selectedIndex == 1{
            if self.selectedEventTypeList == "All Events"
            {
                apiInviteEvents(from: "", to: "", eventType: "")
            }
            else if self.selectedEventTypeList == "Today"
            {
                self.todayConvertFunc()
            }
            else if self.selectedEventTypeList == "Tomorrow"
            {
                self.tomorrowConvertFunc()
            }
            else
            {
                self.apiInviteEvents(from: "\(fromCustomTimeStamp!)", to: "\(toCustomTimeStamp!)", eventType: "")
                
            }
        }
        else{
            if self.selectedEventTypeList == "All Events"
            {
                apiMyEvents(from: "", to: "", eventType: "")
            }
            else if self.selectedEventTypeList == "Today"
            {
                self.todayConvertFunc()
            }
            else if self.selectedEventTypeList == "Tomorrow"
            {
                self.tomorrowConvertFunc()
            }
            else
            {
                self.apiMyEvents(from: "\(fromCustomTimeStamp!)", to: "\(toCustomTimeStamp!)", eventType: "")
                
            }
        }
    }
    
    //MARK: - Other Butt Clicked
    
    @IBAction func otherVwClicked(sender : UIButton){
        
        
    }
    
    //MARK: - location delegate methods
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        
        guard let location: CLLocation = manager.location else { return }
        fetchCityAndCountry(from: location) { city, country, error in
            guard let city = city, let country = country, error == nil else { return }
            print(city + ", " + country)
            self.currentLocation = "\(city) , \(country)"
            self.locationManager.stopUpdatingLocation()
            self.getUserUpdateDetails()
            
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }
    
    
    func fetchCityAndCountry(from location: CLLocation, completion: @escaping (_ city: String?, _ country:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            completion(placemarks?.first?.locality,
                       placemarks?.first?.country,
                       error)
            
        }
    }
    
    func getUserUpdateDetails() {
        
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        print(appVersion!)
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        print(deviceID)
        
        let parameter = ["user_id":UserDefaults.standard.value(forKey: "userId") as! String,"login_type":"iOS","login_device":UIDevice.current.name,"app_version":appVersion!,"deviceID":deviceID,"login_location":self.currentLocation!]
        print(parameter)
        
        networkProvider.request(.addUserHistory(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    if response.statusCode == 200{
                        
                        do {
                            // make sure this JSON is in the format we expect
                            if let json = try JSONSerialization.jsonObject(with: response.data, options: []) as? [String: Any] {
                                // try to read out a string array
                                print(json)
                                
                                appDel.isUserHistoryAdded = true
                                //
                                
                            }
                        } catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getUserUpdateDetails()
                        }
                    }
                } catch let err {
                    
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
        })
    }
    
    
    
}

extension EventsTabViewController :UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        if selectedIndex == 0{
            return 3
        }
        else if selectedIndex == 1
        {
            return 1
            
        }
        else{
            return 4
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if selectedIndex == 2{
            
            if section == 0{
                if self.createdEventsArr.count != 0{
                    return 40
                }
                else{
                    return 0
                }
            }
            else if section == 1{
                if self.rsvpYesEventsArr.count != 0{
                    return 40
                }
                else{
                    return 0
                }
            }
            else if section == 2{
                if self.rsvpMaybeEventsArr.count != 0{
                    return 40
                }
                else{
                    return 0
                }
            }
            else{
                if self.pastEventsArr.count != 0{
                    return 40
                }
                else{
                    return 0
                }
            }
        }
        else{
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if selectedIndex == 2{
            let headerView = UIView.init(frame: CGRect.init(x: 20, y: 10, width: tableView.frame.width-40, height: 40))
            headerView.backgroundColor = .clear
            let label = UILabel()
            label.frame = CGRect.init(x: 5, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
            label.textColor = .darkGray
            var headerString = String()
            if section == 0
            {
                headerString = "Created by me"
            }
            else if section == 1
            {
                headerString = "Attending"
                
            }
            else if section == 2
            {
                headerString = "Interested"
                
            }
            else
            {
                headerString = "Past Events"
                
            }
            
            var myMutableString = NSMutableAttributedString()
            myMutableString = NSMutableAttributedString(string: headerString, attributes: [NSAttributedString.Key.font:UIFont.boldSystemFont(ofSize: 18)])
            
            
            label.attributedText = myMutableString
            //        label.text = DateFormatted
            
            
            headerView.addSubview(label)
            
            if section == 0
            {
                if self.createdEventsArr.count != 0
                {
                    return headerView
                }
                else
                {
                    return nil
                    
                    
                }
            }
            else if section == 1
            {
                if self.rsvpYesEventsArr.count != 0
                {
                    return headerView
                }
                else
                {
                    return nil
                    
                    
                }
            }
            else if section == 2
            {
                if self.rsvpMaybeEventsArr.count != 0
                {
                    return headerView
                }
                else
                {
                    return nil
                    
                    
                }
            }
                
            else
            {
                if self.pastEventsArr.count != 0
                {
                    return headerView
                }
                else
                {
                    return nil
                    
                    
                }
            }
            
            
        }
        else
        {
            return nil
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedIndex == 0{
            if section == 0{
                return self.sharedEventsArr.count
            }
            else if section == 1{
                return nearbyEventsArr.count
            }
            else{
                return publicEventsArr.count
            }
        }
        else if selectedIndex == 1{
            return inviteEventsArr.count
        }
        else{
            
            if section == 0{
                return self.createdEventsArr.count
            }
            else if section == 1{
                return rsvpYesEventsArr.count
            }
            else if section == 2{
                return rsvpMaybeEventsArr.count
            }
            else
            {
                return pastEventsArr.count
                
            }
        }
    }
    
    
    @IBAction func onClickMore(_ sender: Any) {
        var arr = ["Delete Event","Cancel Event"]
        
        self.showActionSheet(titleArr: arr as! NSArray, title: "Choose option") { (index) in
            if index == 0{
                
                
            }
            else if index == 100{
            }
            else{
                
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellid = "cell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! EventListsTableViewCell
        
        cell.selectionStyle = .none
        cell.shareOtherVw.isHidden = true
        
        cell.shareOtherVw.addTarget(self, action: #selector(onClickShareOverViewAction), for: .touchUpInside)
        
        
        if self.selectedIndex == 0{
            if indexPath.section == 0{
                let data:JSON = self.sharedEventsArr[indexPath.row]
                cell.btnShare.indexPath = indexPath.row
                cell.btnShare.indexSection = indexPath.section
                cell.shareOtherVw.tag = indexPath.row
                
                cell.lblCreatedBy.text = data["full_name"].string ?? "Unknown"
                
                var convertedDate = ""
                if data["created_at_"].stringValue.count > 0{
                    convertedDate =  EventsTabViewController.convertTimeStampToDate(timestamp: data["created_at_"].stringValue)
                }
                print(convertedDate)
                cell.lblCreatedDate.text = "\(convertedDate)"
                cell.lblEventName.text = data["event_name"].stringValue
                
                var fDate = ""
                var tDate = ""
                
                var fdateOnly = [String]()
                var tdateOnly = [String]()
                var fDateArray = [String]()
                var tDateArray = [String]()
                var fYear = [String]()
                var tYear = [String]()
                
                if data["from_date"].stringValue.count > 0{
                    fDate = EventsTabViewController.convertTimeStamp(timestamp: data["from_date"].stringValue)
                    fdateOnly = fDate.components(separatedBy: " ")
                    fDateArray = fDate.components(separatedBy: "/")
                    fYear = fDateArray[2].components(separatedBy: " ")
                }
                if data["to_date"].stringValue.count > 0{
                    tDate = EventsTabViewController.convertTimeStamp(timestamp: data["to_date"].stringValue)
                    tdateOnly = tDate.components(separatedBy: " ")
                    tDateArray = tDate.components(separatedBy: "/")
                    tYear = tDateArray[2].components(separatedBy: " ")
                }
                
                if fdateOnly.count > 0 && tdateOnly.count > 0{
                    
                    if fdateOnly[0] == tdateOnly[0]
                    {
                        cell.lblTime.text = "\(filterDayOnly(timestamp: data["from_date"].stringValue)) , \(fdateOnly[1]) - \(tdateOnly[1])"
                    }
                    else
                    {
                        if fYear[0] == tYear[0]{
                            cell.lblTime.text = "\(filterDayOnly(timestamp: data["from_date"].stringValue)) \(fDateArray[1]) \(fDateArray[0]) - \(filterDayOnly(timestamp: data["to_date"].stringValue)) \(tDateArray[1]) \(tDateArray[0]) , \(tYear[0]) "
                        }
                        else
                        {
                            cell.lblTime.text = "\(filterDayOnly(timestamp: data["from_date"].stringValue)) \(fDateArray[1]) \(fDateArray[0]) \(fYear[0])  - \(filterDayOnly(timestamp: data["to_date"].stringValue)) \(tDateArray[1]) \(tDateArray[0]) \(tYear[0]) "
                        }
                    }
                }
                
                if fDateArray.count > 0{
                    cell.lblMonth.text = fDateArray[1].uppercased()
                    cell.lblDate.text = fDateArray[0]
                }
                
                if data["event_type"].string?.lowercased() == "online"{
                    if data["location"].stringValue.isEmpty{
                        cell.lblLocation.text = data["meeting_link"].stringValue
                        cell.imgLocationorUrl.image = #imageLiteral(resourceName: "pointer_icon")
                    }
                    else{
                        cell.lblLocation.text = "Unknown event"
                        cell.imgLocationorUrl.image = #imageLiteral(resourceName: "ic_location_details_")
                    }
                    if data["meeting_logo"].stringValue != ""{
                        let temp = "\(Helpers.imageURl)"+data["meeting_logo"].stringValue.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                        let imgUrl = URL(string: temp)
                        let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=30&height=30&url="+imgUrl!.relativeString
                        
                        let url = URL(string: newUrlStr)
                        
                        
                        cell.imgLocationorUrl.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
                    }
                    else{
                        cell.imgLocationorUrl.image = #imageLiteral(resourceName: "pointer_icon")
                    }
                }
                else{
                    cell.lblLocation.text = data["location"].stringValue
                    cell.imgLocationorUrl.image = #imageLiteral(resourceName: "ic_location_details_")
                }
                
                //                cell.lblEventCate.text = data["category"].stringValue.uppercased()
                cell.lblEventCate.text = data["category"].stringValue.capitalizingFirstLetter()
                
                
                cell.lblEventType.text = data["event_type"].stringValue
                cell.typeView.isHidden = true
                
                let GoneCount = data["going_count"].intValue
                if GoneCount > 0{
                    cell.lblGoneCount.text = data["going_count"].stringValue
                    cell.lblGoneCount.isHidden = false
                    cell.lblGoingHead.isHidden = false
                    cell.goingView.isHidden = false
                    //                    cell.going_width.constant = 90
                    let firstPerson = data["first_person_going"].string
                    cell.lblKnownCount.isHidden = false
                    if GoneCount > 1{
                        cell.lblKnownCount.text = firstPerson!+" and \(GoneCount-1) others are going"
                    }
                    else if GoneCount == 2{
                        cell.lblKnownCount.text = firstPerson!+" and \(GoneCount-1) other is going"
                    }
                    else{
                        cell.lblKnownCount.text = firstPerson!+" is going"
                    }
                }
                else{
                    cell.lblGoneCount.isHidden = true
                    cell.lblGoingHead.isHidden = true
                    cell.goingView.isHidden = true
                    //                    cell.going_width.constant = 0
                    cell.lblKnownCount.isHidden = true
                }
                
                let IntrstedCount = data["interested_count"].intValue
                if IntrstedCount > 0{
                    cell.lblIntrestedCount.text = data["interested_count"].stringValue
                    cell.lblIntrstdHead.isHidden = false
                    cell.lblIntrestedCount.isHidden = false
                    
                }
                else{
                    cell.lblIntrstdHead.isHidden = true
                    cell.lblIntrestedCount.isHidden = true
                }
                
                if GoneCount > 0 || IntrstedCount > 0{
                    cell.bottomVw.isHidden = false
                    self.reduceRowHeight = false
                    //                    self.tblViewEventList.beginUpdates()
                    //                    self.tblViewEventList.endUpdates()
                }
                else{
                    cell.bottomVw.isHidden = true
                    self.reduceRowHeight = true
                    //                    self.tblViewEventList.beginUpdates()
                    //                    self.tblViewEventList.endUpdates()
                }
                
                if data["public_event"].boolValue{
                    cell.lblIspublic.text = "Public"
                    if data["is_sharable"].boolValue{
                        cell.imgShare.isHidden = false
                        cell.btnShare.isEnabled = true
                    }
                    else{
                        cell.imgShare.isHidden = true
                        cell.btnShare.isEnabled = false
                    }
                }
                else{
                    cell.lblIspublic.text = "Private"
                    
                    if data["is_sharable"].boolValue{
                        cell.imgShare.isHidden = false
                        cell.btnShare.isEnabled = true
                    }
                    else{
                        cell.imgShare.isHidden = true
                        cell.btnShare.isEnabled = false
                    }
                }
                
                let proPic = data["propic"].stringValue
                
                if proPic.count != 0{
                    let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                    let imgUrl = URL(string: temp)
                    
                    let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+imgUrl!.relativeString
                    let url = URL(string: newUrlStr)
                    DispatchQueue.main.async {
                        
                        cell.imgPropic.kf.indicatorType = .activity
                        
                        cell.imgPropic.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "Family Logo"), options: nil, progressBlock: nil, completionHandler: nil)
                    }
                }
                else{
                    DispatchQueue.main.async {
                        
                        cell.imgPropic.image = #imageLiteral(resourceName: "Male Colored")
                    }
                }
                
                let eventImage = data["event_image"].stringValue
                if eventImage.count != 0{
                    let temp = "\(Helpers.imageURl)"+"\(BaseUrl.event_image)"+eventImage.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                    let imgUrl = URL(string: temp)
                    let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=1600&height=1200&url="+imgUrl!.relativeString
                    let url = URL(string: newUrlStr)
                    DispatchQueue.main.async {
                        
                        cell.imgEvent.kf.indicatorType = .activity
                        
                        cell.imgEvent.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "default_event_image.jpg"), options: nil, progressBlock: nil, completionHandler: nil)
                    }
                }//[.forceRefresh]
                else{
                    DispatchQueue.main.async {
                        
                        cell.imgEvent.image = #imageLiteral(resourceName: "default_event_image.jpg")
                    }
                }
                
                cell.shareOtherVw.isHidden = false
                
                
            }
            else if indexPath.section == 1{
                let data:JSON = self.nearbyEventsArr[indexPath.row]
                cell.btnShare.indexPath = indexPath.row
                cell.btnShare.indexSection = indexPath.section
                cell.lblCreatedBy.text = data["full_name"].string ?? "Unknown"
                
                let convertedDate =  EventsTabViewController.convertTimeStampToDate(timestamp: data["created_at_"].stringValue)
                cell.lblCreatedDate.text = "\(convertedDate)"
                
                cell.lblEventName.text = data["event_name"].stringValue
                
                var fDate = ""
                var tDate = ""
                
                var fdateOnly = [String]()
                var tdateOnly = [String]()
                var fDateArray = [String]()
                var tDateArray = [String]()
                var fYear = [String]()
                var tYear = [String]()
                
                if data["from_date"].stringValue.count > 0{
                    fDate = EventsTabViewController.convertTimeStamp(timestamp: data["from_date"].stringValue)
                    fdateOnly = fDate.components(separatedBy: " ")
                    fDateArray = fDate.components(separatedBy: "/")
                    fYear = fDateArray[2].components(separatedBy: " ")
                }
                if data["to_date"].stringValue.count > 0{
                    tDate = EventsTabViewController.convertTimeStamp(timestamp: data["to_date"].stringValue)
                    tdateOnly = tDate.components(separatedBy: " ")
                    tDateArray = tDate.components(separatedBy: "/")
                    tYear = tDateArray[2].components(separatedBy: " ")
                }
                
                if fdateOnly.count > 0 && tdateOnly.count > 0{
                    
                    
                    if fdateOnly[0] == tdateOnly[0]
                    {
                        cell.lblTime.text = "\(filterDayOnly(timestamp: data["from_date"].stringValue)) , \(fdateOnly[1]) - \(tdateOnly[1])"
                        
                    }
                    else
                        
                    {
                        if fYear[0] == tYear[0]
                        {
                            cell.lblTime.text = "\(filterDayOnly(timestamp: data["from_date"].stringValue)) \(fDateArray[1]) \(fDateArray[0]) - \(filterDayOnly(timestamp: data["to_date"].stringValue)) \(tDateArray[1]) \(tDateArray[0]) , \(tYear[0]) "
                            
                        }
                        else
                        {
                            cell.lblTime.text = "\(filterDayOnly(timestamp: data["from_date"].stringValue)) \(fDateArray[1]) \(fDateArray[0]) \(fYear[0])  - \(filterDayOnly(timestamp: data["to_date"].stringValue)) \(tDateArray[1]) \(tDateArray[0]) \(tYear[0]) "
                        }
                    }
                }
                
                if fDateArray.count > 0{
                    cell.lblMonth.text = fDateArray[1].uppercased()
                    cell.lblDate.text = fDateArray[0]
                }
                
                if data["event_type"].string?.lowercased() == "online"{
                    if data["location"].stringValue.isEmpty{
                        cell.lblLocation.text = data["meeting_link"].stringValue
                        cell.imgLocationorUrl.image = #imageLiteral(resourceName: "pointer_icon")
                    }
                    else{
                        cell.lblLocation.text = "Unknown event"
                        cell.imgLocationorUrl.image = #imageLiteral(resourceName: "ic_location_details_")
                    }
                    if data["meeting_logo"].stringValue != ""{
                        let temp = "\(Helpers.imageURl)"+data["meeting_logo"].stringValue.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                        let imgUrl = URL(string: temp)
                        let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=30&height=30&url="+imgUrl!.relativeString
                        
                        let url = URL(string: newUrlStr)
                        
                        
                        cell.imgLocationorUrl.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
                    }
                    else{
                        cell.imgLocationorUrl.image = #imageLiteral(resourceName: "pointer_icon")
                    }
                }
                else{
                    cell.lblLocation.text = data["location"].stringValue
                    cell.imgLocationorUrl.image = #imageLiteral(resourceName: "ic_location_details_")
                }
                
                //                cell.lblEventCate.text = data["category"].stringValue.uppercased()
                cell.lblEventCate.text = data["category"].stringValue.capitalizingFirstLetter()
                
                cell.lblEventType.text = data["event_type"].stringValue
                cell.typeView.isHidden = true
                
                let GoneCount = data["going_count"].intValue
                if GoneCount > 0{
                    cell.lblGoneCount.text = data["going_count"].stringValue
                    cell.lblGoneCount.isHidden = false
                    cell.lblGoingHead.isHidden = false
                    cell.goingView.isHidden = false
                    //                    cell.going_width.constant = 90
                    let firstPerson = data["first_person_going"].string
                    cell.lblKnownCount.isHidden = false
                    if GoneCount > 1{
                        cell.lblKnownCount.text = firstPerson!+" and \(GoneCount-1) others are going"
                    }
                    else if GoneCount == 2{
                        cell.lblKnownCount.text = firstPerson!+" and \(GoneCount-1) other is going"
                    }
                    else{
                        cell.lblKnownCount.text = firstPerson!+" is going"
                    }
                }
                else{
                    cell.lblGoneCount.isHidden = true
                    cell.lblGoingHead.isHidden = true
                    cell.goingView.isHidden = true
                    //                    cell.going_width.constant = 0
                    cell.lblKnownCount.isHidden = true
                }
                
                let IntrstedCount = data["interested_count"].intValue
                if IntrstedCount > 0{
                    cell.lblIntrestedCount.text = data["interested_count"].stringValue
                    cell.lblIntrstdHead.isHidden = false
                    cell.lblIntrestedCount.isHidden = false
                    
                }
                else{
                    cell.lblIntrstdHead.isHidden = true
                    cell.lblIntrestedCount.isHidden = true
                }
                if GoneCount > 0 || IntrstedCount > 0{
                    cell.bottomVw.isHidden = false
                    self.reduceRowHeight = false
                    //                    self.tblViewEventList.beginUpdates()
                    //                    self.tblViewEventList.endUpdates()
                }
                else{
                    cell.bottomVw.isHidden = true
                    self.reduceRowHeight = true
                    //                    self.tblViewEventList.beginUpdates()
                    //                    self.tblViewEventList.endUpdates()
                }
                if data["public_event"].boolValue{
                    cell.lblIspublic.text = "Public"
                    if data["is_sharable"].boolValue{
                        cell.imgShare.isHidden = false
                        cell.btnShare.isEnabled = true
                    }
                    else{
                        cell.imgShare.isHidden = true
                        cell.btnShare.isEnabled = false
                    }
                }
                else{
                    cell.lblIspublic.text = "Private"
                    
                    if data["is_sharable"].boolValue{
                        cell.imgShare.isHidden = false
                        cell.btnShare.isEnabled = true
                    }
                    else{
                        cell.imgShare.isHidden = true
                        cell.btnShare.isEnabled = false
                    }
                }
                let proPic = data["propic"].stringValue
                
                if proPic.count != 0{
                    let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                    let imgUrl = URL(string: temp)
                    
                    let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+imgUrl!.relativeString
                    let url = URL(string: newUrlStr)
                    DispatchQueue.main.async {
                        
                        cell.imgPropic.kf.indicatorType = .activity
                        
                        cell.imgPropic.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
                    }
                }
                else{
                    DispatchQueue.main.async {
                        
                        cell.imgPropic.image = #imageLiteral(resourceName: "Male Colored")
                    }
                }
                
                let eventImage = data["event_image"].stringValue
                if eventImage.count != 0{
                    let temp = "\(Helpers.imageURl)"+"\(BaseUrl.event_image)"+eventImage.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                    let imgUrl = URL(string: temp)
                    let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=1600&height=1200&url="+imgUrl!.relativeString
                    let url = URL(string: newUrlStr)
                    DispatchQueue.main.async {
                        
                        cell.imgEvent.kf.indicatorType = .activity
                        
                        cell.imgEvent.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "default_event_image.jpg"), options: nil, progressBlock: nil, completionHandler: nil)
                    }
                }
                else{
                    DispatchQueue.main.async {
                        
                        cell.imgEvent.image = #imageLiteral(resourceName: "default_event_image.jpg")
                    }
                }
                
                cell.shareOtherVw.isHidden = true
                
            }
            else{
                let data:JSON = self.publicEventsArr[indexPath.row]
                cell.btnShare.indexPath = indexPath.row
                cell.btnShare.indexSection = indexPath.section
                cell.lblCreatedBy.text = data["full_name"].string ?? "Unknown"
                
                let convertedDate =  EventsTabViewController.convertTimeStampToDate(timestamp: data["created_at_"].stringValue)
                cell.lblCreatedDate.text = "\(convertedDate)"
                cell.lblEventName.text = data["event_name"].stringValue
                
                var fDate = ""
                var tDate = ""
                
                var fdateOnly = [String]()
                var tdateOnly = [String]()
                var fDateArray = [String]()
                var tDateArray = [String]()
                var fYear = [String]()
                var tYear = [String]()
                
                if data["from_date"].stringValue.count > 0{
                    fDate = EventsTabViewController.convertTimeStamp(timestamp: data["from_date"].stringValue)
                    
                    fdateOnly = fDate.components(separatedBy: " ")
                    fDateArray = fDate.components(separatedBy: "/")
                    fYear = fDateArray[2].components(separatedBy: " ")
                }
                if data["to_date"].stringValue.count > 0{
                    tDate = EventsTabViewController.convertTimeStamp(timestamp: data["to_date"].stringValue)
                    tdateOnly = tDate.components(separatedBy: " ")
                    tDateArray = tDate.components(separatedBy: "/")
                    tYear = tDateArray[2].components(separatedBy: " ")
                }
                
                if fdateOnly.count > 0 && tdateOnly.count > 0{
                    if fdateOnly[0] == tdateOnly[0]
                    {
                        cell.lblTime.text = "\(filterDayOnly(timestamp: data["from_date"].stringValue)) , \(fdateOnly[1]) - \(tdateOnly[1])"
                        
                    }
                    else
                        
                    {
                        if fYear[0] == tYear[0]
                        {
                            cell.lblTime.text = "\(filterDayOnly(timestamp: data["from_date"].stringValue)) \(fDateArray[1]) \(fDateArray[0]) - \(filterDayOnly(timestamp: data["to_date"].stringValue)) \(tDateArray[1]) \(tDateArray[0]) , \(tYear[0]) "
                            
                        }
                        else
                        {
                            cell.lblTime.text = "\(filterDayOnly(timestamp: data["from_date"].stringValue)) \(fDateArray[1]) \(fDateArray[0]) \(fYear[0])  - \(filterDayOnly(timestamp: data["to_date"].stringValue)) \(tDateArray[1]) \(tDateArray[0]) \(tYear[0]) "
                        }
                    }
                }
                
                if fDateArray.count > 0{
                    cell.lblMonth.text = fDateArray[1].uppercased()
                    cell.lblDate.text = fDateArray[0]
                }
                
                if data["event_type"].string?.lowercased() == "online"{
                    if data["location"].stringValue.isEmpty{
                        cell.lblLocation.text = data["meeting_link"].stringValue
                        cell.imgLocationorUrl.image = #imageLiteral(resourceName: "pointer_icon")
                    }
                    else{
                        cell.lblLocation.text = "Unknown event"
                        cell.imgLocationorUrl.image = #imageLiteral(resourceName: "ic_location_details_")
                    }
                    if data["meeting_logo"].stringValue != ""{
                        let temp = "\(Helpers.imageURl)"+data["meeting_logo"].stringValue.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                        let imgUrl = URL(string: temp)
                        let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=30&height=30&url="+imgUrl!.relativeString
                        
                        let url = URL(string: newUrlStr)
                        
                        
                        cell.imgLocationorUrl.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
                    }
                    else{
                        cell.imgLocationorUrl.image = #imageLiteral(resourceName: "pointer_icon")
                    }
                }
                else{
                    cell.lblLocation.text = data["location"].stringValue
                    cell.imgLocationorUrl.image = #imageLiteral(resourceName: "ic_location_details_")
                }
                
                //                cell.lblEventCate.text = data["category"].stringValue.uppercased()
                cell.lblEventCate.text = data["category"].stringValue.capitalizingFirstLetter()
                
                cell.lblEventType.text = data["event_type"].stringValue
                cell.typeView.isHidden = true
                
                let GoneCount = data["going_count"].intValue
                if GoneCount > 0{
                    cell.lblGoneCount.text = data["going_count"].stringValue
                    cell.lblGoneCount.isHidden = false
                    cell.lblGoingHead.isHidden = false
                    cell.goingView.isHidden = false
                    //                    cell.going_width.constant = 90
                    let firstPerson = data["first_person_going"].stringValue
                    cell.lblKnownCount.isHidden = false
                    if GoneCount > 1{
                        cell.lblKnownCount.text = firstPerson+" and \(GoneCount-1) others are going"
                    }
                    else if GoneCount == 2{
                        cell.lblKnownCount.text = firstPerson+" and \(GoneCount-1) other is going"
                    }
                    else{
                        cell.lblKnownCount.text = firstPerson+" is going"
                    }
                }
                else{
                    cell.lblGoneCount.isHidden = true
                    cell.lblGoingHead.isHidden = true
                    cell.goingView.isHidden = true
                    //                    cell.going_width.constant = 0
                    cell.lblKnownCount.isHidden = true
                }
                
                let IntrstedCount = data["interested_count"].intValue
                if IntrstedCount > 0{
                    cell.lblIntrestedCount.text = data["interested_count"].stringValue
                    cell.lblIntrstdHead.isHidden = false
                    cell.lblIntrestedCount.isHidden = false
                    
                }
                else{
                    cell.lblIntrstdHead.isHidden = true
                    cell.lblIntrestedCount.isHidden = true
                }
                if GoneCount > 0 || IntrstedCount > 0{
                    cell.bottomVw.isHidden = false
                    self.reduceRowHeight = false
                    //                    self.tblViewEventList.beginUpdates()
                    //                    self.tblViewEventList.endUpdates()
                }
                else{
                    cell.bottomVw.isHidden = true
                    self.reduceRowHeight = true
                    //                    self.tblViewEventList.beginUpdates()
                    //                    self.tblViewEventList.endUpdates()
                }
                if data["public_event"].boolValue{
                    cell.lblIspublic.text = "Public"
                    //                    cell.imgShare.isHidden = false
                    //                    cell.btnShare.isEnabled = true
                    if data["is_sharable"].boolValue{
                        cell.imgShare.isHidden = false
                        cell.btnShare.isEnabled = true
                    }
                    else{
                        cell.imgShare.isHidden = true
                        cell.btnShare.isEnabled = false
                    }
                }
                else{
                    cell.lblIspublic.text = "Private"
                    //                    cell.imgShare.isHidden = true
                    //                    cell.btnShare.isEnabled = false
                    if data["is_sharable"].boolValue{
                        cell.imgShare.isHidden = false
                        cell.btnShare.isEnabled = true
                    }
                    else{
                        cell.imgShare.isHidden = true
                        cell.btnShare.isEnabled = false
                    }
                }
                
                let proPic = data["propic"].stringValue
                
                if proPic.count != 0{
                    let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                    let imgUrl = URL(string: temp)
                    let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+imgUrl!.relativeString
                    let url = URL(string: newUrlStr)
                    DispatchQueue.main.async {
                        
                        cell.imgPropic.kf.indicatorType = .activity
                        
                        cell.imgPropic.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
                    }
                }
                else{
                    DispatchQueue.main.async {
                        
                        cell.imgPropic.image = #imageLiteral(resourceName: "Male Colored")
                    }
                }
                
                let eventImage = data["event_image"].stringValue
                if eventImage.count != 0{
                    let temp = "\(Helpers.imageURl)"+"\(BaseUrl.event_image)"+eventImage.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                    let imgUrl = URL(string: temp)
                    
                    let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=1600&height=1200&url="+imgUrl!.relativeString
                    let url = URL(string: newUrlStr)
                    DispatchQueue.main.async {
                        
                        cell.imgEvent.kf.indicatorType = .activity
                        
                        cell.imgEvent.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "default_event_image.jpg"), options: nil, progressBlock: nil, completionHandler: nil)
                    }
                }
                else{
                    cell.imgEvent.image = #imageLiteral(resourceName: "default_event_image.jpg")
                }
                cell.shareOtherVw.isHidden = true
            }
        }
        else if selectedIndex == 1{
            let data = inviteEventsArr[indexPath.row]
            cell.btnShare.indexPath = indexPath.row
            cell.btnShare.indexSection = indexPath.section
            
            cell.lblCreatedBy.text = data["full_name"].string ?? "Unknown"
            let convertedDate =  EventsTabViewController.convertTimeStampToDate(timestamp: data["created_at_"].stringValue)
            cell.lblCreatedDate.text = "\(convertedDate)"
            cell.lblEventName.text = data["event_name"].stringValue
            
            var fDate = ""
            var tDate = ""
            
            var fdateOnly = [String]()
            var tdateOnly = [String]()
            var fDateArray = [String]()
            var tDateArray = [String]()
            var fYear = [String]()
            var tYear = [String]()
            
            if data["from_date"].stringValue.count > 0{
                fDate = EventsTabViewController.convertTimeStamp(timestamp: data["from_date"].stringValue)
                
                fdateOnly = fDate.components(separatedBy: " ")
                fDateArray = fDate.components(separatedBy: "/")
                fYear = fDateArray[2].components(separatedBy: " ")
            }
            if data["to_date"].stringValue.count > 0{
                tDate = EventsTabViewController.convertTimeStamp(timestamp: data["to_date"].stringValue)
                tdateOnly = tDate.components(separatedBy: " ")
                tDateArray = tDate.components(separatedBy: "/")
                tYear = tDateArray[2].components(separatedBy: " ")
            }
            
            if fdateOnly.count > 0 && tdateOnly.count > 0{
                if fdateOnly[0] == tdateOnly[0]
                {
                    cell.lblTime.text = "\(filterDayOnly(timestamp: data["from_date"].stringValue)) , \(fdateOnly[1]) - \(tdateOnly[1])"
                    
                }
                else
                    
                {
                    if fYear[0] == tYear[0]
                    {
                        cell.lblTime.text = "\(filterDayOnly(timestamp: data["from_date"].stringValue)) \(fDateArray[1]) \(fDateArray[0]) - \(filterDayOnly(timestamp: data["to_date"].stringValue)) \(tDateArray[1]) \(tDateArray[0]) , \(tYear[0]) "
                        
                    }
                    else
                    {
                        cell.lblTime.text = "\(filterDayOnly(timestamp: data["from_date"].stringValue)) \(fDateArray[1]) \(fDateArray[0]) \(fYear[0])  - \(filterDayOnly(timestamp: data["to_date"].stringValue)) \(tDateArray[1]) \(tDateArray[0]) \(tYear[0]) "
                    }
                }
            }
            
            if fDateArray.count > 0{
                cell.lblMonth.text = fDateArray[1].uppercased()
                cell.lblDate.text = fDateArray[0]
            }
            
            if data["event_type"].string?.lowercased() == "online"{
                if data["location"].stringValue.isEmpty{
                    cell.lblLocation.text = data["meeting_link"].stringValue
                    cell.imgLocationorUrl.image = #imageLiteral(resourceName: "pointer_icon")
                }
                else{
                    cell.lblLocation.text = "Unknown event"
                    cell.imgLocationorUrl.image = #imageLiteral(resourceName: "ic_location_details_")
                }
                if data["meeting_logo"].stringValue != ""{
                    let temp = "\(Helpers.imageURl)"+data["meeting_logo"].stringValue.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                    let imgUrl = URL(string: temp)
                    let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=30&height=30&url="+imgUrl!.relativeString
                    
                    let url = URL(string: newUrlStr)
                    
                    
                    cell.imgLocationorUrl.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
                }
                else{
                    cell.imgLocationorUrl.image = #imageLiteral(resourceName: "pointer_icon")
                }
            }
            else{
                cell.lblLocation.text = data["location"].stringValue
                cell.imgLocationorUrl.image = #imageLiteral(resourceName: "ic_location_details_")
            }
            
            //            cell.lblEventCate.text = data["category"].stringValue.uppercased()
            cell.lblEventCate.text = data["category"].stringValue.capitalizingFirstLetter()
            
            cell.lblEventType.text = data["event_type"].stringValue
            cell.typeView.isHidden = true
            
            let GoneCount = data["going_count"].intValue
            if GoneCount > 0{
                cell.lblGoneCount.text = data["going_count"].stringValue
                cell.lblGoneCount.isHidden = false
                cell.lblGoingHead.isHidden = false
                cell.goingView.isHidden = false
                //                cell.going_width.constant = 90
                let firstPerson = data["first_person_going"].string
                cell.lblKnownCount.isHidden = false
                if GoneCount > 1{
                    cell.lblKnownCount.text = firstPerson!+" and \(GoneCount-1) others are going"
                }
                else if GoneCount == 2{
                    cell.lblKnownCount.text = firstPerson!+" and \(GoneCount-1) other is going"
                }
                else{
                    cell.lblKnownCount.text = firstPerson!+" is going"
                }
            }
            else{
                cell.lblGoneCount.isHidden = true
                cell.lblGoingHead.isHidden = true
                cell.goingView.isHidden = true
                //                cell.going_width.constant = 0
                cell.lblKnownCount.isHidden = true
            }
            
            let IntrstedCount = data["interested_count"].intValue
            if IntrstedCount > 0{
                cell.lblIntrestedCount.text = data["interested_count"].stringValue
                cell.lblIntrstdHead.isHidden = false
                cell.lblIntrestedCount.isHidden = false
                
            }
            else{
                cell.lblIntrstdHead.isHidden = true
                cell.lblIntrestedCount.isHidden = true
            }
            if GoneCount > 0 || IntrstedCount > 0{
                cell.bottomVw.isHidden = false
                self.reduceRowHeight = false
                //                self.tblViewEventList.beginUpdates()
                //                self.tblViewEventList.endUpdates()
            }
            else{
                cell.bottomVw.isHidden = true
                self.reduceRowHeight = true
                //                self.tblViewEventList.beginUpdates()
                //                self.tblViewEventList.endUpdates()
            }
            if data["public_event"].boolValue{ //public_event
                cell.lblIspublic.text = "Public"
                //                cell.imgShare.isHidden = false
                //                cell.btnShare.isEnabled = true
                if data["is_sharable"].boolValue{
                    cell.imgShare.isHidden = false
                    cell.btnShare.isEnabled = true
                }
                else{
                    cell.imgShare.isHidden = true
                    cell.btnShare.isEnabled = false
                }
            }
            else{
                cell.lblIspublic.text = "Private"
                //                cell.imgShare.isHidden = true
                //                cell.btnShare.isEnabled = false
                if data["is_sharable"].boolValue{
                    cell.imgShare.isHidden = false
                    cell.btnShare.isEnabled = true
                }
                else{
                    cell.imgShare.isHidden = true
                    cell.btnShare.isEnabled = false
                }
            }
            
            let proPic = data["propic"].stringValue
            if proPic.count != 0{
                let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                let imgUrl = URL(string: temp)
                let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+imgUrl!.relativeString
                let url = URL(string: newUrlStr)
                DispatchQueue.main.async {
                    
                    cell.imgPropic.kf.indicatorType = .activity
                    
                    cell.imgPropic.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
                }
            }
            else{
                DispatchQueue.main.async {
                    
                    cell.imgPropic.image = #imageLiteral(resourceName: "Male Colored")
                }
            }
            
            let eventImage = data["event_image"].stringValue
            if eventImage.count != 0{
                let temp = "\(Helpers.imageURl)"+"\(BaseUrl.event_image)"+eventImage.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                let imgUrl = URL(string: temp)
                let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=1600&height=1200&url="+imgUrl!.relativeString
                let url = URL(string: newUrlStr)
                DispatchQueue.main.async {
                    
                    cell.imgEvent.kf.indicatorType = .activity
                    
                    cell.imgEvent.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "default_event_image.jpg"), options: nil, progressBlock: nil, completionHandler: nil)
                    
                }
            }
            else{
                DispatchQueue.main.async {
                    
                    cell.imgEvent.image = #imageLiteral(resourceName: "default_event_image.jpg")
                }
            }
        }
        else{
            var data = JSON()
            //            print(createdEventsArr)
            //            print(rsvpYesEventsArr)
            //            print(rsvpMaybeEventsArr)
            
            
            if indexPath.section == 0{
                if self.createdEventsArr.count != 0
                {
                    data = self.createdEventsArr[indexPath.row]
                }
            }
            else if indexPath.section == 1{
                if self.rsvpYesEventsArr.count != 0
                {
                    data = self.rsvpYesEventsArr[indexPath.row]
                }
            }
            else if indexPath.section == 2{
                if self.rsvpMaybeEventsArr.count != 0
                {
                    data = self.rsvpMaybeEventsArr[indexPath.row]
                }
            }
            else {
                if self.pastEventsArr.count != 0
                {
                    data = self.pastEventsArr[indexPath.row]
                }
            }
            if data != nil
            {
                cell.lblEventName.text = data["event_name"].stringValue
                
                cell.btnShare.indexPath = indexPath.row
                cell.btnShare.indexSection = indexPath.section
                cell.lblCreatedBy.text = data["full_name"].string ?? "Unknown"
                
                let convertedDate =  EventsTabViewController.convertTimeStampToDate(timestamp: data["created_at_"].stringValue)
                
                // print(convertedDate)
                cell.lblCreatedDate.text = "\(convertedDate)"
                
                var fDate = ""
                var tDate = ""
                
                var fdateOnly = [String]()
                var tdateOnly = [String]()
                var fDateArray = [String]()
                var tDateArray = [String]()
                var fYear = [String]()
                var tYear = [String]()
                
                if data["from_date"].stringValue.count > 0{
                    fDate = EventsTabViewController.convertTimeStamp(timestamp: data["from_date"].stringValue)
                    fdateOnly = fDate.components(separatedBy: " ")
                    fDateArray = fDate.components(separatedBy: "/")
                    fYear = fDateArray[2].components(separatedBy: " ")
                }
                if data["to_date"].stringValue.count > 0{
                    tDate = EventsTabViewController.convertTimeStamp(timestamp: data["to_date"].stringValue)
                    tdateOnly = tDate.components(separatedBy: " ")
                    tDateArray = tDate.components(separatedBy: "/")
                    tYear = tDateArray[2].components(separatedBy: " ")
                }
                
                if fdateOnly.count > 0 && tdateOnly.count > 0{
                    
                    if fdateOnly[0] == tdateOnly[0]{
                        cell.lblTime.text = "\(filterDayOnly(timestamp: data["from_date"].stringValue)) , \(fdateOnly[1]) - \(tdateOnly[1])"
                        
                    }
                    else{
                        if fYear[0] == tYear[0]{
                            cell.lblTime.text = "\(filterDayOnly(timestamp: data["from_date"].stringValue)) \(fDateArray[1]) \(fDateArray[0]) - \(filterDayOnly(timestamp: data["to_date"].stringValue)) \(tDateArray[1]) \(tDateArray[0]) , \(tYear[0]) "
                        }
                        else{
                            cell.lblTime.text = "\(filterDayOnly(timestamp: data["from_date"].stringValue)) \(fDateArray[1]) \(fDateArray[0]) \(fYear[0])  - \(filterDayOnly(timestamp: data["to_date"].stringValue)) \(tDateArray[1]) \(tDateArray[0]) \(tYear[0]) "
                        }
                    }
                }
                
                if fDateArray.count > 0{
                    cell.lblMonth.text = fDateArray[1].uppercased()
                    cell.lblDate.text = fDateArray[0]
                }
                
                if data["event_type"].string?.lowercased() == "online"{
                    if data["location"].stringValue.isEmpty{
                        cell.lblLocation.text = data["meeting_link"].stringValue
                        cell.imgLocationorUrl.image = #imageLiteral(resourceName: "pointer_icon")
                    }
                    else{
                        cell.lblLocation.text = "Unknown event"
                        cell.imgLocationorUrl.image = #imageLiteral(resourceName: "ic_location_details_")
                    }
                    if data["meeting_logo"].stringValue != ""{
                        let temp = "\(Helpers.imageURl)"+data["meeting_logo"].stringValue.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                        let imgUrl = URL(string: temp)
                        let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=30&height=30&url="+imgUrl!.relativeString
                        
                        let url = URL(string: newUrlStr)
                        
                        
                        cell.imgLocationorUrl.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
                    }
                    else{
                        cell.imgLocationorUrl.image = #imageLiteral(resourceName: "pointer_icon")
                    }
                }
                else{
                    cell.lblLocation.text = data["location"].stringValue
                    cell.imgLocationorUrl.image = #imageLiteral(resourceName: "ic_location_details_")
                }
                
                //                cell.lblEventCate.text = data["category"].stringValue.uppercased()
                cell.lblEventCate.text = data["category"].stringValue.capitalizingFirstLetter()
                
                cell.lblEventType.text = data["event_type"].stringValue
                cell.typeView.isHidden = true
                
                let GoneCount = data["going_count"].intValue
                if GoneCount > 0{
                    cell.lblGoneCount.text = data["going_count"].stringValue
                    cell.lblGoneCount.isHidden = false
                    cell.lblGoingHead.isHidden = false
                    cell.goingView.isHidden = false
                    //                    cell.going_width.constant = 90
                    let firstPerson = data["first_person_going"].string
                    cell.lblKnownCount.isHidden = false
                    if GoneCount > 1{
                        cell.lblKnownCount.text = firstPerson!+" and \(GoneCount-1) others are going"
                    }
                    else if GoneCount == 2{
                        cell.lblKnownCount.text = firstPerson!+" and \(GoneCount-1) other is going"
                    }
                    else{
                        cell.lblKnownCount.text = firstPerson!+" is going"
                    }
                }
                else{
                    cell.lblGoneCount.isHidden = true
                    cell.lblGoingHead.isHidden = true
                    cell.goingView.isHidden = true
                    //                    cell.going_width.constant = 0
                    cell.lblKnownCount.isHidden = true
                }
                
                let IntrstedCount = data["interested_count"].intValue
                if IntrstedCount > 0{
                    cell.lblIntrestedCount.text = data["interested_count"].stringValue
                    cell.lblIntrstdHead.isHidden = false
                    cell.lblIntrestedCount.isHidden = false
                    
                }
                else{
                    cell.lblIntrstdHead.isHidden = true
                    cell.lblIntrestedCount.isHidden = true
                }
                if GoneCount > 0 || IntrstedCount > 0{
                    cell.bottomVw.isHidden = false
                    self.reduceRowHeight = false
                    //                    self.tblViewEventList.beginUpdates()
                    //                    self.tblViewEventList.endUpdates()
                }
                else{
                    cell.bottomVw.isHidden = true
                    self.reduceRowHeight = true
                    //                    self.tblViewEventList.beginUpdates()
                    //                    self.tblViewEventList.endUpdates()
                }
                if data["is_public"].stringValue.lowercased() == "true"{
                    cell.lblIspublic.text = "Public"
                    if data["is_sharable"].boolValue{
                        cell.imgShare.isHidden = false
                        cell.btnShare.isEnabled = true
                    }
                    else{
                        cell.imgShare.isHidden = true
                        cell.btnShare.isEnabled = false
                    }
                }
                else{
                    cell.lblIspublic.text = "Private"
                    if data["is_sharable"].boolValue{
                        cell.imgShare.isHidden = false
                        cell.btnShare.isEnabled = true
                    }
                    else{
                        cell.imgShare.isHidden = true
                        cell.btnShare.isEnabled = false
                    }
                }
                
                let proPic = data["propic"].stringValue
                
                if proPic.count != 0{
                    let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                    let imgUrl = URL(string: temp)
                    let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+imgUrl!.relativeString
                    let url = URL(string: newUrlStr)
                    DispatchQueue.main.async {
                        
                        cell.imgPropic.kf.indicatorType = .activity
                        
                        cell.imgPropic.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
                    }
                }
                else{
                    DispatchQueue.main.async {
                        
                        cell.imgPropic.image = #imageLiteral(resourceName: "Male Colored")
                    }
                }
                
                let eventImage = data["event_image"].stringValue
                if eventImage.count != 0{
                    let temp = "\(Helpers.imageURl)"+"\(BaseUrl.event_image)"+eventImage.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                    let imgUrl = URL(string: temp)
                    let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=1600&height=1200&url="+imgUrl!.relativeString
                    let url = URL(string: newUrlStr)
                    DispatchQueue.main.async {
                        
                        cell.imgEvent.kf.indicatorType = .activity
                        //<<<<<<< HEAD
                        //
                        //                    cell.imgEvent.kf.setImage(with: url	, placeholder: #imageLiteral(resourceName: "default_event_image.jpg"), options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
                        //=======
                        
                        cell.imgEvent.kf.setImage(with: url	, placeholder: #imageLiteral(resourceName: "default_event_image.jpg"), options: nil, progressBlock: nil, completionHandler: nil)
                    }
                }
                else{
                    DispatchQueue.main.async {
                        cell.imgEvent.image = #imageLiteral(resourceName: "default_event_image.jpg")
                    }
                }
            }
        }
        cell.btnShare.addTarget(self, action: #selector(onClickShareAction(_:)), for: .touchUpInside)
        cell.lblLocation.handleURLTap { (url) in
            print(url)
            self.showActionSheet(titleArr: ["Open URL","Copy URL","Share URL"], title: "Select option") { (index) in
                if index == 100{}
                else if index == 0{
                    var data = JSON()
                    //        print(indexPath.row)
                    if self.selectedIndex == 0{
                        if indexPath.section == 0{
                            data = self.sharedEventsArr[indexPath.row]
                        }
                        else if indexPath.section == 1{
                            data = self.nearbyEventsArr[indexPath.row]
                        }
                        else{
                            data = self.publicEventsArr[indexPath.row]
                        }
                    }
                    else if self.selectedIndex == 1{
                        data = self.inviteEventsArr[indexPath.row]
                    }
                    else{
                        
                        if indexPath.section == 0{
                            data = self.createdEventsArr[indexPath.row]
                        }
                            
                        else if indexPath.section == 1{
                            data = self.rsvpYesEventsArr[indexPath.row]
                        }
                        else if indexPath.section == 2{
                            data = self.rsvpMaybeEventsArr[indexPath.row]
                        }
                        else{
                            data = self.pastEventsArr[indexPath.row]
                        }
                    }
                    UIPasteboard.general.string = data["meeting_pin"].stringValue
                    self.showToast(message: "Participant pin copied to clipboard")
                    DispatchQueue.main.asyncAfter(deadline: .now()+3) {
                        UIApplication.shared.open(url)
                    }
                }
                else if index == 1{
                    UIPasteboard.general.string = url.absoluteString
                }
                else if index == 2{
                    let textToShare = [ url.absoluteString ]
                    let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                    activityViewController.popoverPresentationController?.sourceView = self.view
                    self.present(activityViewController, animated: true, completion: nil)
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        if reduceRowHeight{
        //            return 445
        //        }
        //        else{
        //            return 465
        //        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var data = JSON()
        //        print(indexPath.row)
        if selectedIndex == 0{
            if indexPath.section == 0{
                data = self.sharedEventsArr[indexPath.row]
            }
            else if indexPath.section == 1{
                data = self.nearbyEventsArr[indexPath.row]
            }
            else{
                data = self.publicEventsArr[indexPath.row]
            }
        }
        else if selectedIndex == 1{
            data = inviteEventsArr[indexPath.row]
        }
        else{
            
            if indexPath.section == 0{
                data = self.createdEventsArr[indexPath.row]
            }
                
            else if indexPath.section == 1{
                data = self.rsvpYesEventsArr[indexPath.row]
            }
            else if indexPath.section == 2{
                data = self.rsvpMaybeEventsArr[indexPath.row]
            }
            else{
                data = self.pastEventsArr[indexPath.row]
            }
        }
        
        let stryboard = UIStoryboard.init(name: "third", bundle: nil)
        
        let vc = stryboard.instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
        if selectedIndex == 2{
            if UserDefaults.standard.value(forKey: "userId") as! String == data["created_by"].stringValue
            {
                vc.enableEventEdit = true
            }else
            {
                vc.enableEventEdit = false
            }
            
            //            if indexPath.section == 3{
            //                vc.pastEvents = true
            //            }
            //            else
            //            {
            //                vc.pastEvents = false
            //
            //            }
            
            
        }else
        {
            vc.enableEventEdit = false
            
        }
        vc.eventDetails = data
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y
        
        //        if !searchview.isHidden{
        //            self.notificationViewHeight.constant = 0
        //            self.notificationView.isHidden = true
        //            return
        //        }
        
        if(scrollView.panGestureRecognizer.translation(in: scrollView.superview).y > 0)
        {
            DispatchQueue.main.async {
                if  !self.notificationView.isHidden && self.notificationViewHeight.constant == self.headerHeight{
                    return
                }
                if self.notificationViewHeight.constant <= self.headerHeight{
                    //                    if  self.notificationView.isHidden{
                    self.notificationView.isHidden = false
                    
                    //                    }
                    //                    if self.notificationViewHeight.constant + (self.previousPageXOffset  - offset) >= self.headerHeight || self.notificationViewHeight.constant + (self.previousPageXOffset  - offset) <= 0 {
                    self.notificationViewHeight.constant = self.headerHeight
                    //                    }
                    //                    else{
                    //                        self.notificationViewHeight.constant = self.notificationViewHeight.constant + (self.previousPageXOffset  - offset)
                    //                    }
                }
                else if self.notificationViewHeight.constant > self.headerHeight{
                    self.notificationViewHeight.constant = self.headerHeight
                }
                self.previousPageXOffset = offset
                
            }
        }
        else{
            DispatchQueue.main.async {
                if  self.notificationView.isHidden && self.notificationViewHeight.constant == 0{
                    return
                }
                if self.notificationViewHeight.constant <= self.headerHeight {
                    //                    if self.notificationViewHeight.constant <= 0{
                    //                        self.notificationViewHeight.constant = 0
                    //                        if  !self.notificationView.isHidden{
                    //                            self.notificationView.isHidden = true
                    //
                    //                        }
                    //                    }
                    //                    if self.notificationViewHeight.constant - (offset - self.previousPageXOffset) >= self.headerHeight || self.notificationViewHeight.constant - (offset - self.previousPageXOffset) <= 0 {
                    self.notificationViewHeight.constant = 0
                    self.notificationView.isHidden = true
                    
                    //                    }
                    //                    else{
                    //                        self.notificationViewHeight.constant = self.notificationViewHeight.constant - (offset - self.previousPageXOffset)
                    //
                    //                    }
                }
                else if self.notificationViewHeight.constant > self.headerHeight{
                    self.notificationViewHeight.constant = self.headerHeight
                }
                self.previousPageXOffset = offset
                
            }
        }
        
    }
    
    //MARK:- Textfield Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        //        if textField == txtSearch
        //        {
        //            // selectWebAPI()
        //            if textField.text!.count > 0{
        //                btnSearchReset.isHidden = false
        //            }
        //            else{
        //                btnSearchReset.isHidden = true
        //            }
        textField.endEditing(true)
        //        }
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        self.view.endEditing(true)
        if textField == txtSearch
        {
            //            if textField.text!.count > 0{
            //
            //                btnSearchReset.isHidden = false
            //                if textField.text!.trimmingCharacters(in: .whitespaces).isEmpty {
            //                    txtSearch.text = ""
            //                    btnSearchReset.isHidden = true
            //
            //                      }
            //            }
            //            else{
            //                btnSearchReset.isHidden = true
            //            }
            selectWebAPI()
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == dropDownEventListing
        {
            dropDownEventListing.resignFirstResponder();
            // Additional code here
        }
        //        else
        //        {
        //        if textField.text!.count > 0 {
        //            btnSearchReset.isHidden = false
        //        }
        //        else{
        //            btnSearchReset.isHidden = true
        //            // selectWebAPI()
        //        }
        //        }
    }
    
    //    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
    //
    //            return true
    //
    //
    //    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //        if textField.text!.count > 0 {
        //            btnSearchReset.isHidden = false
        //        }
        //        else{
        //            btnSearchReset.isHidden = true
        //            // selectWebAPI()
        //        }
        return true
    }
    
    
    
    
}


extension EventsTabViewController: UISearchBarDelegate{
    //    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    //        return true
    //    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        searchBar.endEditing(true)
        //    searchBar.showsCancelButton = false
        self.searchTxt = ""
        selectWebAPI()
        
        
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchTxt = searchBar.text!
        selectWebAPI()
        searchBar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count == 0{
            self.searchTxt = ""
            selectWebAPI()
            searchBar.endEditing(true)
        }
    }
}

extension EventsTabViewController: SambagDatePickerViewControllerDelegate
    
{
    
    func sambagDatePickerDidSet(_ viewController: SambagDatePickerViewController, result: SambagDatePickerResult) {
        if self.dateTag == 0
        {
            //            txtStartDate.text = "\(result)"
            selectedStartDate = "\(result)"
            //            print(selectedStartDate)
            self.timetag = 0
            
        }
        else
        {
            //            txtEndDate.text = "\(result)"
            selectedEndDate = "\(result)"
            //            print(selectedEndDate)
            
            self.timetag = 1
            
        }
        viewController.dismiss(animated: true, completion: nil)
        let vc = SambagTimePickerViewController()
        vc.theme = theme
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    func sambagDatePickerDidCancel(_ viewController: SambagDatePickerViewController) {
        if self.dateTag == 0
        {
            txtStartDate.text = ""
            selectedStartDate = ""
            
        }
        else
        {
            txtEndDate.text = ""
            selectedEndDate = ""
        }
        viewController.dismiss(animated: true, completion: nil)
    }
    
    
}


extension EventsTabViewController: SambagTimePickerViewControllerDelegate {
    
    func sambagTimePickerDidSet(_ viewController: SambagTimePickerViewController, result: SambagTimePickerResult) {
        if self.timetag == 0
        {
            selectedStartDate += "   \(result)"
            
            self.selectedStartDateFormatted = self.convertDateFormat(date: selectedStartDate)
            
            //            print(selectedStartDateFormatted)
            
            if selectedStartDateFormatted != ""{
                if self.checkDateHigherThanCurrentDate(selectedDatestring: self.selectedStartDateFormatted)
                {
                    
                    self.showToast(message: "Please Select Start date higher than current date and time")
                    txtStartDate.text = ""
                    selectedStartDate = ""
                    self.selectedStartDateFormatted = ""
                }
                else
                {
                    
                    
                    
                    
                    //            sambagTimeDisplay
                    txtStartDate.text = Helpers.sambagTimeDisplay(dateAsString: "\(selectedStartDate)")
                    //                txtStartDate.text = "\(selectedStartDate)"
                    
                }
            }
            else
            {
                
                self.showToast(message: "Please Select valid date")
                txtStartDate.text = ""
                selectedStartDate = ""
                self.selectedStartDateFormatted = ""
            }
            
        }
        else
        {
            selectedEndDate += " \(result)"
            
            //            print(selectedEndDate)
            
            self.selectedEndDateFormatted = self.convertDateFormat(date: selectedEndDate)
            //            print(selectedEndDateFormatted)
            if selectedEndDateFormatted != ""
            {
                if self.checkDateHigherThanCurrentDate(selectedDatestring: self.selectedEndDateFormatted)
                {
                    //                Helpers.showAlertDialog(message: "Please Select date less than current date and time", target: self)
                    
                    self.showToast(message: "Please Select End date Higher than current date and time")
                    txtEndDate.text = ""
                    selectedEndDate = ""
                    selectedEndDateFormatted = ""
                }
                else if checkstartDateandEndDate(StartDate: selectedStartDateFormatted
                    , endDate: selectedEndDateFormatted)
                {
                    
                    self.showToast(message: "Please Select end date higher than startDate")
                    txtEndDate.text = ""
                    selectedEndDate = ""
                    selectedEndDateFormatted = ""
                }
                else if selectedStartDateFormatted == selectedEndDateFormatted{
                    self.showToast(message: "Please Select different endDate and startDate ")
                    txtEndDate.text = ""
                    selectedEndDate = ""
                    selectedEndDateFormatted = ""
                }
                else
                {
                    
                    
                    txtEndDate.text = Helpers.sambagTimeDisplay(dateAsString:"\(selectedEndDate)")
                    
                }
            }
            else{
                self.showToast(message: "Please Select valid date")
                txtEndDate.text = ""
                selectedEndDate = ""
                selectedEndDateFormatted = ""
            }
            
            
        }
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func sambagTimePickerDidCancel(_ viewController: SambagTimePickerViewController) {
        if self.timetag == 0
        {
            txtStartDate.text = ""
            selectedStartDate += ""
            self.selectedStartDateFormatted = ""
            
        }
        else
        {
            txtEndDate.text = ""
            selectedEndDate += ""
            self.selectedEndDateFormatted = ""
        }
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func convertDateFormat(date:String)->String
    {
        
        
        do{
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy hh:mm a"
            
            if  let convertedDate = try formatter.date(from: date){
                //                formatter.timeZone = TimeZone(identifier: "UTC")
                formatter.timeZone = TimeZone.current
                let date = formatter.string(from: convertedDate)
                let formatter2 = DateFormatter()
                formatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
                let convertedDate2 = formatter.date(from: date)
                formatter2.timeZone = TimeZone.current
                let date2 = formatter2.string(from: convertedDate2!)
                
                
                
                return date2
            }
            else{
                return ""
            }
            
        }catch{
            return ""
        }
    }
    
    
    func convertTimetoTimestamp(date:String)->Int
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"
        let convertedDate = formatter.date(from: date)
        
        //        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.timeZone = TimeZone.current
        let date = formatter.string(from: convertedDate!)
        let convertedDate2 = formatter.date(from: date)
        
        let timestamp = convertedDate2!.timeIntervalSince1970
        
        return Int(timestamp)
        
    }
    
    func compairDateWithCurrentDate (selectedDatestring:String)->Bool
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"
        
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        let convertedDateNow = formatter.string(from: Date())
        let convertedDateNow2 = formatter.date(from: convertedDateNow)
        formatter2.timeZone = TimeZone.current
        let dateNow = formatter2.string(from: convertedDateNow2!)
        //        print(selectedDatestring == dateNow)
        return selectedDatestring == dateNow
    }
    
    func checkDateHigherThanCurrentDate (selectedDatestring:String)->Bool
    {
        //        print(selectedDatestring)
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"
        
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        let convertedDateNow = formatter.string(from: Date())
        let convertedDateNow2 = formatter.date(from: convertedDateNow)
        formatter2.timeZone = TimeZone.current
        let dateNow = formatter2.string(from: convertedDateNow2!)
        //        print(dateNow)
        //        print(selectedDatestring < dateNow)
        return selectedDatestring < dateNow
    }
    
    func checkstartDateandEndDate (StartDate:String , endDate:String)->Bool
    {
        //        print(StartDate)
        //        print(endDate)
        //        print(StartDate > endDate)
        return StartDate > endDate
    }
    
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: 20, y: self.view.frame.size.height/2-30, width: self.view.frame.width-40, height: 60))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.numberOfLines = 2
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 10.0)
        toastLabel.adjustsFontSizeToFitWidth = true
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 6.0, delay: 0.3, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}

extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
