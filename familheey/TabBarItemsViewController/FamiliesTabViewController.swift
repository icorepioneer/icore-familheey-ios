//
//  FamiliesTabViewController.swift
//  familheey
//
//  Created by familheey on 16/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import Moya
import Kingfisher
import SwiftyJSON
import Firebase
import SideMenu


class FamiliesTabViewController: BaseClassViewController,CLLocationManagerDelegate{
    //    @IBOutlet weak var searchbarHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var pendingReqstVew: UIView!
    
    @IBOutlet weak var pendngRqstVew_height: NSLayoutConstraint!
    @IBOutlet weak var searchResultLbl: UILabel!
    @IBOutlet weak var noSearchResultView: UIView!
    @IBOutlet weak var lblSearchResult: UILabel!
    @IBOutlet weak var lblSearchResultInList: UILabel!
    
    @IBOutlet weak var heightForSearchTextInList: NSLayoutConstraint!
    
    @IBOutlet weak var notificationView: UIView!
    @IBOutlet weak var NotificationHeight: NSLayoutConstraint!
    @IBOutlet weak var btnAdd: UIButton!

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var bg_noFamilyView: UIView!
    @IBOutlet weak var tblListView: UITableView!
    @IBOutlet weak var lblNoti_Count: UILabel!
    @IBOutlet weak var viewOfNoti_Count: UIView!
    @IBOutlet weak var imgOfNoti_Bell: UIImageView!
    @IBOutlet weak var searchConstraintheight: NSLayoutConstraint!
    @IBOutlet weak var searchview: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearchReset: UIButton!
    
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    let locationManager = CLLocationManager()
    var FamilyArr : familyListResponse!

    var arrayOfNotificationList = [[String:Any]]()
    var ref: DatabaseReference!
    
    var notiUnreadCount:Int = 0
    var currentLocation : String!
    var searchTxt = ""
    var previousPageXOffset: CGFloat = 0.0
    let headerHeight: CGFloat = 56
    var offset = 0
    let limit = 8
    
    //MARK:- View Controller Loading Methods
    
    override func onScreenLoad() {
        // setupMenuBar()
        
        
        let headerNib = UINib.init(nibName: "FamilyListingView", bundle: Bundle.main)
        tblListView.register(headerNib, forHeaderFooterViewReuseIdentifier: "FamilyListingView")
        self.heightForSearchTextInList.constant = 0
        self.lblSearchResultInList.isHidden = true
        txtSearch.delegate = self
        Helpers.setleftView(textfield: txtSearch, customWidth: 30)
        
        if appDel.arrayOfAllCategory.count == 0{
            self.apiForAllCategoryList()
        }
        
        //        getMemberListAPI()
        searchview.isHidden = true
        searchConstraintheight.constant = 0
        setupRightView()
        NotificationCenter.default.addObserver(self, selector: #selector(childNodeAdded(notification:)), name: Notification.Name("ChildAdded"), object: nil)
        
    }
    
    func setupRightView(){
        let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "topicListingViewController") as! topicListingViewController
        
        let rightMenuNavigationController = SideMenuNavigationController(rootViewController: vc)
        SideMenuManager.default.leftMenuNavigationController = rightMenuNavigationController
        SideMenuManager.default.addPanGestureToPresent(toView: self.view)
        SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: self.view)
        rightMenuNavigationController.statusBarEndAlpha = 0
        rightMenuNavigationController.menuWidth = self.view.frame.width
        rightMenuNavigationController.dismissOnPush = false
        rightMenuNavigationController.dismissOnPresent = false
        rightMenuNavigationController.pushStyle = .subMenu
    }
    func openSearch(){
        notificationView.isHidden = false
        self.NotificationHeight.constant = self.headerHeight
        self.searchview.isHidden = false
        self.searchConstraintheight.constant = self.headerHeight
        //        notificationView.isHidden = true
        
        self.btnSearchReset.isHidden = false
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
            self.searchview.alpha = 1.0
        }, completion: nil)
    }
    func closeSearch(){
        
        UIView.animate(withDuration: 0.5, animations: {
            self.searchview.alpha = 0.0
            
        }) { (true) in
            
            self.searchview.isHidden = true
            self.searchConstraintheight.constant = 0
            //        notificationView.isHidden = false
            self.NotificationHeight.constant = self.headerHeight
            
        }
        
    }
    
    @objc func childNodeAdded(notification: Notification) {
        self.NotificationArrayListing()
    }
    
    func NotificationArrayListing(){
        let userId = UserDefaults.standard.value(forKey: "userId") as! String
        
        let noti_String :String = "\(Helpers.notificationUploadstring)\(userId)_notification"
        //        print(noti_String)
        //        let noti_String :String = "\(userId)_notification"
        let ref = Database.database().reference(withPath: noti_String).queryLimited(toLast: 1200)
        ref.observeSingleEvent(of: .value, with: { snapshot in
            
            if !snapshot.exists() {
                self.arrayOfNotificationList = []
                appDel.arrayOfNotificationList = []
                appDel.tempNotifi = []
                return
                
            }
            
            self.arrayOfNotificationList = []
            appDel.arrayOfNotificationList = []
            appDel.tempNotifi = []
            let dict:[String:Any] = snapshot.value as! [String:Any]
            
            for (key, value) in dict {
                var dictionary = [String:Any]()
                dictionary["key"] = key
                dictionary["value"] = value as! [String:Any]
                appDel.arrayOfNotificationList.append(dictionary)
                appDel.tempNotifi.append(value as! [String:Any])
            }
            
            //            appDel.arrayOfNotificationList = self.arrayOfNotificationList
            self.arrayOfNotificationList = appDel.arrayOfNotificationList
            
            self.notiUnreadCount = 0
            for data in self.arrayOfNotificationList{
                let dict  = data["value"] as! [String:Any]
                if dict["visible_status"] as! String == "unread"{
                    self.notiUnreadCount = self.notiUnreadCount + 1
                }
            }
            
            if self.notiUnreadCount == 0
            {
                self.lblNoti_Count.isHidden = true
                self.viewOfNoti_Count.isHidden = true
            }
            else{
                self.lblNoti_Count.isHidden = false
                self.viewOfNoti_Count.isHidden = false
                if self.notiUnreadCount > 99{
                    self.lblNoti_Count.text = "99+"
                }
                else{
                    self.lblNoti_Count.text = "\(self.notiUnreadCount)"
                }
            }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //        self.lblNoti_Count.isHidden = true
        // Helpers.getAccessToken(completion: <#Helpers.RefreshCompletion#>)
        self.viewOfNoti_Count.isHidden = true
        appDel.isFamilyFromSearch = false
        self.NotificationArrayListing()
        getFamilyList()
        self.navigationController?.navigationBar.isHidden  = true
        self.tabBarController?.tabBar.isHidden = false
        
        if UserDefaults.standard.value(forKey: "userProfile") != nil{
            let pic = UserDefaults.standard.value(forKey: "userProfile") as! String
            let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+pic.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let imgUrl = URL(string: temp)
            let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+imgUrl!.relativeString
            let url = URL(string: newUrlStr)
            imgProfile.kf.indicatorType = .activity
            
            imgProfile.kf.setImage(with: url, placeholder: UIImage(named: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        else{
            imgProfile.image = UIImage(named: "Male Colored")
        }
    }
    
    
    override func isNavigationControllerVisible() -> Bool {
        return false
    }
    
    //MARK:- Right navigation button
    
    func addRightNavigationButton(){
        let rightButtonItem = UIBarButtonItem(barButtonSystemItem: .bookmarks, target: self, action: #selector(onClickrightButtonAction))
        self.navigationItem.rightBarButtonItem = rightButtonItem
    }
    
    //MARK:- Button Actions
    
    
    @IBAction func btnNotificationOnClick(_ sender: Any) {
        self.lblNoti_Count.isHidden = true
        self.viewOfNoti_Count.isHidden = true
        //        appDel.firstTimeNot = true
        let storyboard = UIStoryboard.init(name: "third", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        vc.arrayOfNotificationList = self.arrayOfNotificationList
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @objc func onClickrightButtonAction(sender: UIBarButtonItem){
        
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let addMember = storyboard.instantiateViewController(withIdentifier: "viewRequestsTableViewController") as! viewRequestsTableViewController
        //    let addMember = storyboard.instantiateViewController(withIdentifier: "FolderListViewController") as! FolderListViewController
        self.navigationController?.pushViewController(addMember, animated: true)
        
    }
    
    //MARK:- Web API ,"offset":offset,"limit":limit
    func getMemberListAPI(){
        let parameter = ["user_id":UserDefaults.standard.value(forKey: "userId") as! String,"query":txtSearch.text!.trimmingCharacters(in: .whitespacesAndNewlines)] as [String : Any]
        ActivityIndicatorView.show("Loading.....")
        networkProvider.request(.getFamilies(parameter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    
                    if response.statusCode == 200{
                        
                        let someDictionaryFromJSON = try JSONSerialization.jsonObject(with: response.data, options: .allowFragments) as! [String: Any]
                        let tempArr = someDictionaryFromJSON
                        do{
                            let family = try familyListResponse(tempArr)
                            guard let familyMdl = family as? familyListResponse else{
                                return
                            }
                            if familyMdl.pending_count > 0{
                                self.pendingReqstVew.isHidden = false
                                self.pendngRqstVew_height.constant = 20
                            }
                            else{
                                self.pendingReqstVew.isHidden = true
                                self.pendngRqstVew_height.constant = 0
                            }
                            
                            if familyMdl.familyList!.count > 0{
                                self.FamilyArr = familyMdl
                                self.btnAdd.isHidden = false
                                self.tblListView.isHidden = false
                                self.bg_noFamilyView.isHidden = true
                                self.tblListView.delegate = self
                                self.tblListView.dataSource = self
                                
                                
                                if self.txtSearch.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
                                    
                                    self.heightForSearchTextInList.constant = 0
                                    self.lblSearchResultInList.isHidden = true
                                    self.lblSearchResult.isHidden = true
                                } else {
                                    self.heightForSearchTextInList.constant = 25
                                    self.lblSearchResultInList.isHidden = false
                                    self.lblSearchResult.isHidden = true
                                    
                                    self.lblSearchResultInList.text = "Search result for \"\(self.txtSearch.text!)\""
                                    
                                }
                                let count = familyMdl.familyList!.count
                                self.tblListView.reloadData()
                                ActivityIndicatorView.hiding()
                            }
                            else{
                                
                                self.tblListView.isHidden = true
                                self.heightForSearchTextInList.constant = 0
                                self.lblSearchResultInList.isHidden = true
                                self.lblSearchResult.isHidden = false
                                
                                if self.txtSearch.text != "" {
                                    
                                    self.lblSearchResult.text = "Search result for \"\(self.txtSearch.text!)\""
                                    self.bg_noFamilyView.isHidden = true
                                    self.noSearchResultView.isHidden = false
                                } else {
                                    
                                    self.bg_noFamilyView.isHidden = false
                                    self.noSearchResultView.isHidden = true
                                    self.btnAdd.isHidden = true
                                    appDel.noFamily = true
                                }
                                ActivityIndicatorView.hiding()
                            }
                        }catch{
                            return
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getMemberListAPI()
                        }
                    }
                    else{
                        self.displayAlertChoice(alertStr: "Something went wrong! Please try again later", title: "") { (result) in
                            self.getMemberListAPI()
                        }
                        ActivityIndicatorView.hiding()
                    }
                }catch let err {
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    //, "offset":"\(offset)", "limt":"\(limit)"
    
    func getFamilyList(){
        let params = [
            "user_id":UserDefaults.standard.value(forKey: "userId") as! String,
            "query":txtSearch.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        ]
        APIServiceManager.callServer.getFamilyList(url: EndPoint.viewFamily, params: params, success: { (responseMdl) in
            
            guard let familyMdl = responseMdl as? familyListResponse else{
                return
            }
            
            if familyMdl.statusCode == 200{
                
                if familyMdl.pending_count > 0{
                    self.pendingReqstVew.isHidden = false
                    self.pendngRqstVew_height.constant = 20
                }
                else{
                    self.pendingReqstVew.isHidden = true
                    self.pendngRqstVew_height.constant = 0
                }
                
                if familyMdl.familyList!.count > 0{
                    self.FamilyArr = familyMdl
                    self.btnAdd.isHidden = false
                    self.tblListView.isHidden = false
                    self.bg_noFamilyView.isHidden = true
                    self.tblListView.delegate = self
                    self.tblListView.dataSource = self
                    
                    if self.txtSearch.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
                        
                        self.heightForSearchTextInList.constant = 0
                        self.lblSearchResultInList.isHidden = true
                        self.lblSearchResult.isHidden = true
                    } else {
                        self.heightForSearchTextInList.constant = 25
                        self.lblSearchResultInList.isHidden = false
                        self.lblSearchResult.isHidden = true
                        
                        self.lblSearchResultInList.text = "Search result for \"\(self.txtSearch.text!)\""
                        
                    }
                    
                    self.tblListView.reloadData()
                    ActivityIndicatorView.hiding()
                }
                else{
                    
                    self.tblListView.isHidden = true
                    self.heightForSearchTextInList.constant = 0
                    self.lblSearchResultInList.isHidden = true
                    self.lblSearchResult.isHidden = false
                    
                    if self.txtSearch.text != "" {
                        
                        self.lblSearchResult.text = "Search result for \"\(self.txtSearch.text!)\""
                        self.bg_noFamilyView.isHidden = true
                        self.noSearchResultView.isHidden = false
                    } else {
                        
                        self.bg_noFamilyView.isHidden = false
                        self.noSearchResultView.isHidden = true
                        self.btnAdd.isHidden = true
                        appDel.noFamily = true
                    }
                    ActivityIndicatorView.hiding()
                }
                
            }
            else{
                self.displayAlert(alertStr: "Something went wrong! Please try again later", title: "Error")
                ActivityIndicatorView.hiding()
            }
            
        }) { (error) in
            ActivityIndicatorView.hiding()
            self.displayAlert(alertStr: error!.description, title: "Error")
        }
    }
    
    func getUserUpdateDetails() {
        
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        
        let parameter = ["user_id":UserDefaults.standard.value(forKey: "userId") as! String,"login_type":"iOS","login_device":UIDevice.current.name,"app_version":appVersion!,"deviceID":deviceID,"login_location":self.currentLocation!]
        
        networkProvider.request(.addUserHistory(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    if response.statusCode == 200{
                        do {
                            if let json = try JSONSerialization.jsonObject(with: response.data, options: []) as? [String: Any] {
                                appDel.isUserHistoryAdded = true
                            }
                        } catch let error as NSError {
                        }
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getUserUpdateDetails()
                        }
                    }
                } catch let err {
                    
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
        })
    }
    
    
    func apiForAllCategoryList() {
        
        let parameter = [:] as [String : Any]
        networkProvider.request(.FetchAllCategory(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    let jsonData = JSON(response.data as Any)
                    
                    if response.statusCode == 200
                    {
                        let arrayOfCategoryResponse:Array<JSON> = jsonData["data"].arrayValue
                        var arrayOfAllCategory = [String]()
                        for category in arrayOfCategoryResponse
                        {
                            arrayOfAllCategory.append(category["name"].stringValue.firstCapitalized)
                        }
                        appDel.arrayOfAllCategory = arrayOfAllCategory
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.apiForAllCategoryList()
                        }
                    }
                    else
                    {
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                } catch let err {
                    
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
        })
    }
    
    
    //MARK:- Button Actions
    @IBAction func searchAction(_ sender: Any){
        self.openSearch()
    }
    
    @IBAction func onClickAddFamily(_ sender: Any) { // Story board Change
        let storyBoard = UIStoryboard.init(name: "CreateFamily", bundle: nil)
        let addFamily = storyBoard.instantiateViewController(withIdentifier: "addFamilyOne") as! AddFamilyScreenOneViewController
        //  loginView.regType = regType
        self.navigationController?.pushViewController(addFamily, animated: true)
    }
    
    @IBAction func onClckPost(_ sender: Any) {
        
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let addMember = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
        self.navigationController?.pushViewController(addMember, animated: false)
        
    }
    
    @IBAction func onClickMyfamily(_ sender: Any) {
        //remya
        getMemberListAPI()
        //        getFamilyList()
    }
    @IBAction func onClickDiscover(_ sender: Any) {
        //        self.tabBarController?.selectedIndex = 3
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SearchTabViewController") as! SearchTabViewController
        //        vc.arrayOfNotificationList = self.arrayOfNotificationList
        vc.tabIndex = 100
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func profilePicClicked(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "UserProfileTableViewController") as! UserProfileTableViewController
        //vc.mutialFamilies =  "\(usersList[indexPath.row].familycount ?? "0")"
        //  vc.mutualConnections =  "\(usersList[indexPath.row].mutualfamilycount ?? "0")"
        let userId = UserDefaults.standard.value(forKey: "userId") as! String
        vc.userID = userId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickViewFamily(_ sender: UIButton){
        var grpId = ""
        
        grpId = "\(FamilyArr.familyList![sender.tag].faId)"
        
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
        intro.groupId = grpId
        self.navigationController?.pushViewController(intro, animated: true)
    }
    @IBAction func onClickSearchReset(_ sender: Any) {
        txtSearch.text = ""
        searchTxt = ""
        //remya
        getMemberListAPI()
        //        getFamilyList()()
        //        btnSearchReset.isHidden = true
        closeSearch()
        self.txtSearch.endEditing(true)
    }
    
    @IBAction func onClickPendingRequest(_ sender: Any) {
        let stryboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
        let vc = stryboard.instantiateViewController(withIdentifier: "PendingRequestViewController") as! PendingRequestViewController
        vc.isAdmin = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: - Feed back
    @IBAction func feedBackClicked(_ sender: Any) {
        let postoptionsTittle =  ["Calendar","Announcements","Messages","Feedback","Quick tour"]
        showActionSheet(titleArr: postoptionsTittle as NSArray, title: "Select options") { (index) in
            if index == 0{
                self.viewCalendar()
            }
            else if index == 1{
                self.viewAnnouncements()
            }
            else if index == 3{
                self.sendFeedBack()
            }
            else if index == 100{
            }
            else if index == 2{
                self.viewMessages()
            }
            else if index == 4{
                self.viewSubscription()
            }
            else{
            }
        }
    }
    
    func sendFeedBack(){
        let stryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = stryboard.instantiateViewController(withIdentifier: "FeedbackViewController") as! FeedbackViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func viewAnnouncements(){
        let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsViewController") as! AnnouncementsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func viewSubscription(){
        let stryboard = UIStoryboard.init(name: "Subscription", bundle: nil)
        let vc = stryboard.instantiateViewController(withIdentifier: "WalkthorughViewController") as! WalkthorughViewController
        appDel.quickTour = true
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func viewCalendar(){
        let stryboard = UIStoryboard.init(name: "third", bundle: nil)
        let vc = stryboard.instantiateViewController(withIdentifier: "CalendarEventsViewController") as! CalendarEventsViewController
        vc.isFromExplore = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func viewinvites(){
        let stryboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
        let vc = stryboard.instantiateViewController(withIdentifier: "PendingRequestViewController") as! PendingRequestViewController
        vc.isAdmin = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func viewMessages(){
        let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "topicListingViewController") as! topicListingViewController
        
        let rightMenuNavigationController = SideMenuNavigationController(rootViewController: vc)
        SideMenuManager.default.leftMenuNavigationController = rightMenuNavigationController
        SideMenuManager.default.addPanGestureToPresent(toView: self.view)
        SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: self.view)
        rightMenuNavigationController.statusBarEndAlpha = 0
        rightMenuNavigationController.menuWidth = self.view.frame.width
        rightMenuNavigationController.dismissOnPush = false
        rightMenuNavigationController.dismissOnPresent = false
        rightMenuNavigationController.pushStyle = .subMenu
        present(rightMenuNavigationController, animated: true, completion: nil)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension FamiliesTabViewController:  URLSessionDownloadDelegate {
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        //        print("downloadLocation:", location)
        
    }
    
}

extension FamiliesTabViewController:UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return FamilyArr.familyList!.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // return FamilyArr.familyList!.count
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        return UITableViewCell()
    }
    
    //    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    //         if txtSearch.text!.isEmpty{
    //            return 0
    //
    //        }
    //        else
    //         {
    //         return 20
    //        }
    //     }
    //
    //     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    //         if !txtSearch.text!.isEmpty{
    //         let viewTemp = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 20), reuseIdentifier: "cellvew")
    //         let label = UILabel(frame: CGRect(x: 16, y: 0, width: viewTemp.frame.width - 16, height: 20))
    //
    //
    //             label.text = "Search result for \"\(txtSearch.text!)\""
    //
    //         label.textColor = .black
    //         viewTemp.addSubview(label)
    //         viewTemp.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.9411764706, blue: 0.9450980392, alpha: 1)
    //         return viewTemp
    //        }
    //     }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        let header = self.tblListView.dequeueReusableHeaderFooterView(withIdentifier: "FamilyListingView") as! FamilyListingView
        
        header.btnAction.tag = section
        header.btnView.tag = section
        
        header.byTitle.text = "By"
        header.lblTitle.text = FamilyArr.familyList![section].faName
        if header.lblTitle.text!.isEmpty ||  header.lblTitle.text == nil{
            header.lblTitle.text = "Unknown"
        }
        header.lblType.text = FamilyArr.familyList![section].faCategory
        header.lblRegion.text = FamilyArr.familyList![section].faRegion
        header.lblCreatedBy.text = "\(FamilyArr.familyList![section].faAdmin)"
        // header.lblMembersCount.text = "\(FamilyArr.familyList![section].memberCount)"
        
        
        if FamilyArr.familyList![section].faLogo.count != 0{
            let temp = "\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+FamilyArr.familyList![section].faLogo.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let imgurl = URL(string: temp)
            let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+imgurl!.relativeString
            let url = URL(string: newUrlStr)
            header.imgLogo.kf.indicatorType = .activity
            
            header.imgLogo.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        else{
            header.imgLogo.image = #imageLiteral(resourceName: "Family Logo")
        }
        
        header.btnAction.isHidden = true
        header.btnView.addTarget(self, action: #selector(onClickViewFamily(_:)), for: .touchUpInside)
        
        header.lblKnownHead.text = "posts"
        
        let knownCount = FamilyArr.familyList![section].memberCount!
        
        if Int(knownCount)! > 0{
            header.lblMembersCount.text = FamilyArr.familyList![section].memberCount
            header.lblMemberHead.isHidden = false
            header.lblMembersCount.isHidden = false
        }
        else{
            header.lblMemberHead.isHidden = true
            header.lblMembersCount.isHidden = true
        }
        
        let eventCount = FamilyArr.familyList![section].knownCount!
        
        if Int(eventCount)! > 0{
            header.lblKnown.text = FamilyArr.familyList![section].knownCount
            header.lblKnownHead.isHidden = false
            header.lblKnown.isHidden = false
        }
        else{
            header.lblKnownHead.isHidden = true
            header.lblKnown.isHidden = true
        }
        
        return header
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView.isLast(for: indexPath, arrayCount: FamilyArr.familyList!.count){
            print("---------------------------------------------------\(offset)")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 145
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y
        if(scrollView.panGestureRecognizer.translation(in: scrollView.superview).y > 0)
        {
            DispatchQueue.main.async {
                if  !self.notificationView.isHidden && self.NotificationHeight.constant == self.headerHeight{
                    return
                }
                if self.NotificationHeight.constant <= self.headerHeight{
                    if  self.notificationView.isHidden{
                        self.notificationView.isHidden = false
                    }
                    if self.NotificationHeight.constant + (self.previousPageXOffset  - offset) >= self.headerHeight || self.NotificationHeight.constant + (self.previousPageXOffset  - offset) <= 0 {
                        self.NotificationHeight.constant = self.headerHeight
                    }
                    else{
                        self.NotificationHeight.constant = self.NotificationHeight.constant + (self.previousPageXOffset  - offset)
                    }
                }
                else if self.NotificationHeight.constant > self.headerHeight{
                    self.NotificationHeight.constant = self.headerHeight
                }
                self.previousPageXOffset = offset
                
            }
        }
        else{
            DispatchQueue.main.async {
                if  self.notificationView.isHidden && self.NotificationHeight.constant == 0{
                    return
                }
                if self.NotificationHeight.constant <= self.headerHeight {
                    if self.NotificationHeight.constant <= 0{
                        self.NotificationHeight.constant = 0
                        if  !self.notificationView.isHidden{
                            self.notificationView.isHidden = true
                            
                        }
                    }
                    if self.NotificationHeight.constant - (offset - self.previousPageXOffset) >= self.headerHeight || self.NotificationHeight.constant - (offset - self.previousPageXOffset) <= 0 {
                        self.NotificationHeight.constant = 0
                        self.notificationView.isHidden = true
                        
                        
                    }
                    else{
                        self.NotificationHeight.constant = self.NotificationHeight.constant - (offset - self.previousPageXOffset)
                        
                    }
                }
                else if self.NotificationHeight.constant > self.headerHeight{
                    self.NotificationHeight.constant = self.headerHeight
                }
                self.previousPageXOffset = offset
                
            }
        }
        
    }
    
    /* func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     if FamilyArr.familyList![indexPath.row].isBlocked == 1{
     self.displayAlert(alertStr: "You are corrently blocked from this group", title: "")
     }
     else{
     let stryboard = UIStoryboard.init(name: "second", bundle: nil)
     
     let family = stryboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
     let temp = FamilyArr.familyList![indexPath.row].faId
     family.groupId = String(temp)
     self.navigationController?.pushViewController(family, animated: true)
     }
     }*/
    
    
    //MARK: - location delegate methods
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        //        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        
        guard let location: CLLocation = manager.location else { return }
        fetchCityAndCountry(from: location) { city, country, error in
            guard let city = city, let country = country, error == nil else { return }
            //            print(city + ", " + country)
            self.currentLocation = "\(city) , \(country)"
            self.locationManager.stopUpdatingLocation()
            self.getUserUpdateDetails()
            
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        //        print("Error \(error)")
    }
    
    func fetchCityAndCountry(from location: CLLocation, completion: @escaping (_ city: String?, _ country:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            completion(placemarks?.first?.locality,
                       placemarks?.first?.country,
                       error)
            
        }
    }
}

extension FamiliesTabViewController: UISearchBarDelegate,UITextFieldDelegate{
    //    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    //        return true
    //    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        //        self.searchTxt = searchBar.text!
        //        print(self.searchTxt)
        //        getFamilyList()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        //        if searchText.count == 0{
        //            self.searchTxt = ""
        //             getFamilyList()
        //        }
    }
    
    //MARK:- textfield delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        textField.resignFirstResponder()
        //        if textField == txtSearch
        //        {
        //            // selectWebAPI()
        //            if textField.text!.count > 0{
        //                btnSearchReset.isHidden = false
        //            }
        //            else{
        //                btnSearchReset.isHidden = true
        //            }
        textField.endEditing(true)
        //        }
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        self.view.endEditing(true)
        if textField == txtSearch
        {
            //            if textField.text!.count > 0{
            //                btnSearchReset.isHidden = false
            //                if textField.text!.trimmingCharacters(in: .whitespaces).isEmpty {
            //                                  txtSearch.text = ""
            //                    btnSearchReset.isHidden = true
            //
            //                                    }
            //            }
            //            else{
            //                btnSearchReset.isHidden = true
            //            }
            //remya
            getMemberListAPI()
            //        getFamilyList()
            
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //        if textField.text!.count > 0 {
        //            btnSearchReset.isHidden = false
        //        }
        //        else{
        //            btnSearchReset.isHidden = true
        //            // selectWebAPI()
        //        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //        if textField.text!.count > 0 {
        //            btnSearchReset.isHidden = false
        //        }
        //        else{
        //            btnSearchReset.isHidden = true
        //        }
        return true
    }
}

