//
//  FeedbackViewController.swift
//  familheey
//
//  Created by familheey on 31/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON
import Kingfisher
import CropViewController
import SocketIO
import AVFoundation
import YPImagePicker


class FeedbackViewController: UIViewController, CropViewControllerDelegate {
    
    
    var config = YPImagePickerConfiguration()

    @IBOutlet weak var typeField                            : UITextField!
    @IBOutlet weak var descField                            : UITextField!
    @IBOutlet weak var imgVw                                : UIImageView!
    private var networkProvider                             = MoyaProvider<FamilyheeyApi>()
    var imagePicker                                         = UIImagePickerController()
    
    var manager : SocketManager!
     var  socket: SocketIOClient!
    
    @IBOutlet weak var txtDescField: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = true
        Helpers.setleftView(textfield: typeField, customWidth: 15)
      //  Helpers.setleftView(textfield: descField)
        
       /* let socketStr = BaseUrl.SocketQa
        manager = SocketManager(socketURL: URL(string: socketStr)!, config: [.log(true), .compress])
        socket = manager.defaultSocket
        print(socket)
        
        socket.on(clientEvent: .connect) {data, ack in
            print(data)
            print("socket connected")
        }
        
        socket.on(clientEvent: .error) { (data, eck) in
            print(data)
            print("socket error")
        }

        socket.on(clientEvent: .disconnect) { (data, eck) in
            print(data)
            print("socket disconnect")
        }
        
        socket.on(clientEvent: SocketClientEvent.reconnect) { (data, eck) in
            print(data)
            print("socket reconnect")
        }

        
        socket.on("post_channel_4") {data, ack in
            guard let cur = data[0] as? Double else { return }
            
            print(cur)
            
            self.socket.emitWithAck("canUpdate", cur).timingOut(after: 1) {data in
                self.socket.emit("update", ["amount": cur + 2.50])
            }
            
            ack.with("Got your currentAmount", "dude")
        }
        
        socket.connect()*/
    }
    
    //MARK: - Upload Image
    //below code for normal imagepicker
//    @IBAction func uploadImageClicked(_ sender: Any) {
//
//        self.view.endEditing(true)
//
//        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
//        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
//            self.openCamera()
//        }))
//
//        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
//            self.openGallary()
//        }))
//
//
//        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
//        self.present(alert, animated: true, completion: nil)
//    }
//
    //below code for YPImagepicker
    @IBAction func uploadImageClicked(_ sender: Any) {
        
        self.view.endEditing(true)
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
//            self.openCamera()
            self.selectImages(type:"camera")

            
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
//            self.openGallary()
            self.selectImages(type:"gallery")

        }))
        
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func submitClicked(_ sender: Any) {
      
        if typeField.text == ""{
            
            Helpers.showAlertDialog(message: "Please enter feedback type", target: self)
            return
            
        }
        self.view.endEditing(true)
        if imgVw.image == nil{
            noImage()
            
        }else{
            
            includeImage(img: imgVw.image!)
        }
      //  PostComment()
    }
    
    func includeImage(img : UIImage){
        
        let img1 = img.resized(withPercentage: 0.5)
        
        let imgData = UIImage.pngData(img1!)
        ActivityIndicatorView.show("Sending....")
        let param = ["type" : typeField.text!, "description" : txtDescField.text!, "user" : UserDefaults.standard.value(forKey: "userId") as! String, "device" : UIDevice.current.name, "os" : "iOS"]
         let originalData = NSData()
        Helpers.requestWith(fileParamName: "image", originalCover: "", urlAppendString: "receive_feedback", imageData: imgData(), originalImg: originalData as Data, parameters: param, mimeType: "image/png") { (response) in
            
            print("response : \(response)")
            
            let alert = UIAlertController(title: "Success", message: "Successfully submitted your feedback.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                self.backClicked(sender: self)
            }))
            
            self.present(alert, animated: true, completion: nil)
            
             ActivityIndicatorView.hiding()
        }
    }
    
    func noImage(){
        
        let param = ["type" : typeField.text!, "description" : txtDescField.text!, "user" : UserDefaults.standard.value(forKey: "userId") as! String, "device" : UIDevice.current.name, "os" : "iOS", "name":UserDefaults.standard.value(forKey: "user_fullname") as! String]
        
        print("Param : \(param)")
        ActivityIndicatorView.show("Sending....")
        
        networkProvider.request(.receive_feedback(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    
                    if response.statusCode == 200{
                        print("view contents : \(jsonData)")
                        
                        let alert = UIAlertController(title: "Success", message: "Successfully submitted your feedback.", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                            self.backClicked(sender: self)
                        }))
                      
                        self.present(alert, animated: true, completion: nil)
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.noImage()
                        }
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                   // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    
    //MARK:- Custom Methods
    
    
    
    func openCamera(){

            let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)

            switch (authStatus){

            case .notDetermined:
                showAlert(title: "Unable to access the Camera", message: "To enable access, go to Settings > Privacy > Camera and turn on Camera access for this app.")
            case .restricted:
                showAlert(title: "Unable to access the Camera", message: "To enable access, go to Settings > Privacy > Camera and turn on Camera access for this app.")
            case .denied:
                showAlert(title: "Unable to access the Camera", message: "To enable access, go to Settings > Privacy > Camera and turn on Camera access for this app.")
            case .authorized:
//                alert.dismiss(animated: true, completion: nil)
                if(UIImagePickerController .isSourceTypeAvailable(.camera)){
                    imagePicker.sourceType = .camera
                    imagePicker.showsCameraControls=true
                    imagePicker.allowsEditing=true
                    self.present(imagePicker, animated: true, completion: nil)
                }
                else
                {
                    
                }
            }
    }
    
    
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: UIAlertController.Style.alert)

        let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(okAction)

        let settingsAction = UIAlertAction(title: "Settings", style: .default, handler: { _ in
            // Take the user to Settings app to possibly change permission.
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        // Finished opening URL
                    })
                } else {
                    // Fallback on earlier versions
                    UIApplication.shared.openURL(settingsUrl)
                }
            }
        })
        alert.addAction(settingsAction)

        self.present(alert, animated: true, completion: nil)
    }
    
//    func openCamera(){
//        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
//            imagePicker.sourceType = UIImagePickerController.SourceType.camera
//            imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
//            imagePicker.allowsEditing = false
//            self.present(imagePicker, animated: true, completion: nil)
//        }
//        else{
//            // self.displayAlert(alertStr: "You don't have camera", title: "")
//        }
//    }
    
    func openGallary(){
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func selectedImage(img:UIImage){
        // let cropView = CropViewController(image: img)
        let cropView = CropViewController(croppingStyle: .default, image: img)
        cropView.delegate = self
        self.navigationController?.pushViewController(cropView, animated: true)
    }
    
    //MARK:- CropView SDK Delegate
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        
         imgVw.image = image
        self.navigationController?.popViewController(animated: true)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
        })
    }
    
    @IBAction func backClicked(sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension  FeedbackViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker .dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let choosenImage = info[.originalImage] as! UIImage
        selectedImage(img: choosenImage)
        picker.dismiss(animated: true, completion: nil)
        
    }
  
    //MARK: - YP Image picker
    
    func selectImages(type:String){
        config.library.maxNumberOfItems = 1
        config.library.minNumberOfItems = 1
        config.library.defaultMultipleSelection = false
        config.startOnScreen = .library
        if type.lowercased() == "camera"{
            config.library.mediaType = .photo
            config.screens = [.photo]
        }
        else{
            config.library.mediaType = .photo
            config.screens = [.library]
        }
        config.targetImageSize = .original
        config.usesFrontCamera = true
        config.showsPhotoFilters = true
        config.wordings.libraryTitle = "Gallery"
        config.library.isSquareByDefault = false
        config.video.compression = AVAssetExportPresetPassthrough
        
        let picker = YPImagePicker(configuration: config)
        
        picker.didFinishPicking { [unowned picker] items, cancelled in
            print("Items : \(items.count)")
         
            if cancelled {
                print("Picker was canceled")
                picker.dismiss(animated: true, completion: nil)
                return
            }
            
            
            
            for item in items {
                switch item {
                case .photo(let photo):
                    print(photo)
                    self.imgVw.image = photo.image

//                    let img2 = photo.image.resized(withPercentage: 0.5)
//
//                    let imgData = (img2?.pngData())!
//
//                    imgDataArr.append(imgData)
//
              
                    
                case .video(let _):
                    print("video")
                }
                
            }
            
            
       
            
            
            picker.dismiss(animated: true, completion: nil)
        }
        
        //        picker.didFinishPicking { [unowned picker] items, cancelled in
        //            if cancelled {
        //                print("Picker was canceled")
        //            }
        //            picker.dismiss(animated: true, completion: nil)
        //        }
        present(picker, animated: true, completion: nil)
    }
}

