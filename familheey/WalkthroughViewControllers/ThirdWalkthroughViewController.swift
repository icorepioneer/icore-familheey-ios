//
//  ThirdWalkthroughViewController.swift
//  familheey
//
//  Created by familheey on 25/04/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit

class ThirdWalkthroughViewController: UIViewController {
    @IBOutlet weak var bgView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        let rect = self.bgView.bounds
        let y = rect.size.height - 75.0
        let curveTo = rect.size.height
        
        let myBez = UIBezierPath()
        myBez.move(to: CGPoint(x: 0.0, y: y))
        myBez.addQuadCurve(
            to: CGPoint(x: rect.size.width, y: y),
            controlPoint: CGPoint(x: rect.size.width / 2.0, y: curveTo))
        myBez.addLine(to: CGPoint(x: rect.size.width, y: 0.0))
        myBez.addLine(to: CGPoint(x: 0.0, y: 0.0))
        myBez.close()
        
        let maskForPath = CAShapeLayer()
        maskForPath.path = myBez.cgPath
        self.bgView.layer.mask = maskForPath
        
    }
    
    //MARK:- Button actions
    @IBAction func onClickSkip(_ sender: Any) {
        if appDel.quickTour{
            self.navigationController?.popViewController(animated: true)
        }
        else{
            setUserDefaultValues.setUserDefaultValue(userDefaultValue: "1", userDefaultKey: "showGuide")
            if appDel.noFamily{
                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
               
                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "HomeNavigation") as! HomeNavigation
                //            self.noFamily = true
                appDel.postcreatedInPublic = false
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = redViewController
            }
            else{
               
                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "HomeNavigation") as! HomeNavigation
                //            self.noFamily = false
                appDel.postcreatedInPublic = false
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = redViewController
            }
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
