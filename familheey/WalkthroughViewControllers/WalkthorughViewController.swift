//
//  WalkthorughViewController.swift
//  familheey
//
//  Created by familheey on 25/04/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit

class WalkthorughViewController: UIPageViewController {
    
    fileprivate lazy var pages: [UIViewController] = {
        return [
            self.getViewController(withIdentifier: "FirstWalkthroughViewController"),
            self.getViewController(withIdentifier: "SecondWalkthroughViewController"),
            self.getViewController(withIdentifier: "ThirdWalkthroughViewController"),
            self.getViewController(withIdentifier: "FourthWalkthroughViewController"),
            self.getViewController(withIdentifier: "FifthWalkthroughViewController")
        ]
    }()

    fileprivate func getViewController(withIdentifier identifier: String) -> UIViewController{
        return UIStoryboard(name: "Subscription", bundle: nil).instantiateViewController(withIdentifier: identifier)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate   = self

        if let firstVC = pages.first
        {
            setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.isHidden = true
    }
    
     func moveFwd(){
        setViewControllers([pages[1]], direction: .forward, animated: true, completion: nil)
    }
 
}
extension WalkthorughViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate{
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = pages.firstIndex(of: viewController) else { return nil }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0          else { return nil }
        
        guard pages.count > previousIndex else { return nil        }
        
        return pages[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        guard let viewControllerIndex = pages.firstIndex(of: viewController) else { return nil }
        
        let nextIndex = viewControllerIndex + 1
        
        guard nextIndex < pages.count else { return nil }
        
        guard pages.count > nextIndex else { return nil         }
        
        return pages[nextIndex]
    }
}
