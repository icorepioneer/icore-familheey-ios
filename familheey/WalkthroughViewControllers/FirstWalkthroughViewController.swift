//
//  FirstWalkthroughViewController.swift
//  familheey
//
//  Created by familheey on 25/04/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit

class FirstWalkthroughViewController: UIViewController {
    @IBOutlet weak var bgView: UIView!
    
    var semiCirleLayer: CAShapeLayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        bgView.cornerRadius = 50
//        bgView.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
        
        if let id =  UserDefaults.standard.value(forKey: "userId") as? String{
            if id.isEmpty{
            }
            else{
                getPreloadFamilyList()
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        let radius = self.bgView.bounds.size.width
//        bgView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: CGFloat(radius))
        bgView.clipsToBounds = true
        
     /*   let arcCenter = CGPoint(x: self.view.bounds.size.width / 2, y: self.view.bounds.size.height / 2)
        
//        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let circleRadius = self.view.bounds.size.width
        let circlePath = UIBezierPath(arcCenter: arcCenter, radius: circleRadius, startAngle: CGFloat.pi, endAngle: CGFloat.pi * 2, clockwise: false)

        let mask = CAShapeLayer()
        mask.path = circlePath.cgPath
        
//        semiCirleLayer.fillColor = UIColor.red.cgColor
        self.bgView.layer.mask = mask*/
        let rect = self.bgView.bounds
        let y = rect.size.height - 75.0
        let curveTo = rect.size.height

        let myBez = UIBezierPath()
        myBez.move(to: CGPoint(x: 0.0, y: y))
        myBez.addQuadCurve(
            to: CGPoint(x: rect.size.width, y: y),
            controlPoint: CGPoint(x: rect.size.width / 2.0, y: curveTo))
        myBez.addLine(to: CGPoint(x: rect.size.width, y: 0.0))
        myBez.addLine(to: CGPoint(x: 0.0, y: 0.0))
        myBez.close()

        let maskForPath = CAShapeLayer()
        maskForPath.path = myBez.cgPath
        self.bgView.layer.mask = maskForPath
      
    }
    
    @IBAction func onClickMove(_ sender: Any) {
//        let vc = WalkthorughViewController()
//        vc.moveFwd()
    }
    
    @IBAction func onClickSkip(_ sender: Any) {
        if appDel.quickTour{
            self.navigationController?.popViewController(animated: true)
        }
        else{
            setUserDefaultValues.setUserDefaultValue(userDefaultValue: "1", userDefaultKey: "showGuide")
            if appDel.noFamily{
                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
               
                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "HomeNavigation") as! HomeNavigation
                //            self.noFamily = true
                appDel.postcreatedInPublic = false
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = redViewController
            }
            else{
               
                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "HomeNavigation") as! HomeNavigation
                //            self.noFamily = false
                appDel.postcreatedInPublic = false
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = redViewController
            }
        }
    }
    
    //MARK:- web
    func getPreloadFamilyList(){
        let param = [
            "userid" : UserDefaults.standard.value(forKey: "userId") as! String,
            "searchtxt" : "",
            "offset": 0,
            "limit": 30,
            "phonenumbers":[],
            "type":"family"
            ] as [String : Any]
        APIServiceManager.callServer.searchRequest(url: EndPoint.search, param: param, fromSearchTabViewController: true, success: { (response) in
            if let results = response as! SearchList?{

                if results.status_code == 200{

                    if results.searchResult2 != nil{
                        appDel.PreloadFamilyList.append(contentsOf: results.searchResult2!)
                    }
                    
                }
            }
            
            
        }) { (error) in
            
            
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
