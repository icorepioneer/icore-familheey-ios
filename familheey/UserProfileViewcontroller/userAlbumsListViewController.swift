//
//  userAlbumsListViewController.swift
//  familheey
//
//  Created by familheey on 25/01/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit
import Moya
import Alamofire
import Kingfisher
import SwiftyJSON

class userAlbumsListViewController: UIViewController,UIGestureRecognizerDelegate,UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate{
    @IBOutlet weak var backVew: UIView!
    @IBOutlet weak var txtSerach: UITextField!
    @IBOutlet weak var btnSearchReset: UIButton!
    @IBOutlet weak var noFamilyVew: UIView!
    
    @IBOutlet weak var btnCreateAlbumNoView: UIButton!
    @IBOutlet weak var lblSecondHeading: UILabel!
    @IBOutlet weak var btnAddNoDataView: UIButton!
    @IBOutlet weak var colVew: UICollectionView!
    @IBOutlet weak var btnAddAlbum: UIButton!
    @IBOutlet weak var deleteVew: UIView!
    
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    var dataArr = JSON()
    var arrSelectedIndex = [Int]()
    var arrrofUsers = [String]()
    var userId = ""
    var link_type = 0
    var isAdmin = ""
    var islinkable:Int = Int()
    var isFromdocument = false
    var facate = ""
    var isFromMultiSelect = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        getAllAlbums()
        Helpers.setleftView(textfield: txtSerach, customWidth: 30)
        txtSerach.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        getAllAlbums()
    }
    
    //MARK:- Web API
    func getAllAlbums(){
        var parameter =  [String : Any]()
        
        if isFromdocument{
            parameter = ["from_user":UserDefaults.standard.value(forKey: "userId") as! String, "profile_id":UserDefaults.standard.value(forKey: "userId") as! String, "folder_for":"users", "folder_type":"documents","txt":txtSerach.text!] as [String : Any]
        }
        else{
            
            //this condition only for show others profile
            if self.userId != ""
            {
                parameter = ["from_user":UserDefaults.standard.value(forKey: "userId") as! String, "profile_id":self.userId, "folder_for":"users", "folder_type":"albums","txt":txtSerach.text!] as [String : Any]
            }
            else
            {
                parameter = ["from_user":UserDefaults.standard.value(forKey: "userId") as! String, "profile_id":UserDefaults.standard.value(forKey: "userId") as! String, "folder_for":"users", "folder_type":"albums","txt":txtSerach.text!] as [String : Any]
            }
        }
        
        
        if self.dataArr.count == 0{
            ActivityIndicatorView.show("Loading....")
        }
        
        networkProvider.request(.listUserFolders(parameter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    
                    if response.statusCode == 200{
                        print("jsonData : \(jsonData)")
                        
                        self.dataArr = jsonData["data"]
                        
                        if self.dataArr.count > 0{
                            self.noFamilyVew.isHidden = true
                            self.btnAddAlbum.isHidden = false
                            self.colVew.isHidden = false
                            self.colVew.delegate = self
                            self.colVew.dataSource = self
                            self.colVew.reloadData()
                            
                            let userID = UserDefaults.standard.value(forKey: "userId") as! String
                            let createdId = self.dataArr["created_by"].stringValue
                            if userID == createdId{
                                self.setupLongPressGesture()
                            }
                            if userID == self.userId{
                                self.btnAddAlbum.isHidden = false
                            }
                            else{
                                self.btnAddAlbum.isHidden = true
                                self.btnCreateAlbumNoView.isHidden = true
                                self.lblSecondHeading.isHidden = true
                                self.btnAddNoDataView.isHidden = true
                            }
                            
                        }
                        else{
                            self.noFamilyVew.isHidden = false
                            self.colVew.isHidden = true
                            let userID = UserDefaults.standard.value(forKey: "userId") as! String
                            if userID == self.userId{
                                self.btnAddAlbum.isHidden = true
                                self.btnCreateAlbumNoView.isHidden = false
                                self.lblSecondHeading.isHidden = false
                                self.btnAddNoDataView.isHidden = false
                            }
                            else{
                                self.noFamilyVew.isHidden = false
                                self.btnAddAlbum.isHidden = true
                                self.btnCreateAlbumNoView.isHidden = true
                                self.lblSecondHeading.isHidden = true
                                self.btnAddNoDataView.isHidden = true
                            }
                            //                            self.btnAddAlbum.isHidden = true
                            //                            if self.userId != ""
                            //                            {
                            //                                self.btnCreateAlbumNoView.isHidden = true
                            //                                self.lblSecondHeading.isHidden = true
                            //                                self.btnAddNoDataView.isHidden = true
                            //                            }
                        }
                        
                        print("Data arr : \(self.dataArr)")
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getAllAlbums()
                        }
                    }
                    else{
                        self.noFamilyVew.isHidden = false
                        self.colVew.isHidden = true
                        self.btnAddAlbum.isHidden = true
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    self.noFamilyVew.isHidden = false
                    self.colVew.isHidden = true
                    self.btnAddAlbum.isHidden = true
                    
                    
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                
                self.noFamilyVew.isHidden = false
                self.colVew.isHidden = true
                self.btnAddAlbum.isHidden = true
                
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    func deleteFolder(fId:String){
        var parameter =  [String : Any]()
        parameter = ["id":fId, "is_active":false]
        ActivityIndicatorView.show("Loading....")
        
        networkProvider.request(.updateFolder(paraeter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    
                    if response.statusCode == 200{
                        self.getAllAlbums()
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.deleteFolder(fId: fId)
                        }
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    //MARK:- Custom methods
    func setupLongPressGesture() {
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress))
        longPressGesture.minimumPressDuration = 1.0 // 1 second press
        longPressGesture.delegate = self
        self.colVew.addGestureRecognizer(longPressGesture)
    }
    
    @objc func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer){
        if gestureRecognizer.state == .ended {
            let touchPoint = gestureRecognizer.location(in: self.colVew)
            if let indexPath = self.colVew.indexPathForItem(at: touchPoint){
                let cell = colVew.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
                self.colVew.allowsMultipleSelection = true
                self.isFromMultiSelect = true
                self.deleteVew.isHidden = false
                arrSelectedIndex.append(indexPath.row)
                let data = self.dataArr[indexPath.row]
                arrrofUsers.append(data["id"].stringValue)
                var indexPaths = [IndexPath]()
                indexPaths.append(indexPath)
                if let collectionView = colVew {
                    collectionView.reloadItems(at: indexPaths)
                }
            }
            
        }
    }
    
    //MARK:- UIButton Actions
    @IBAction func onClickCreateAlbum(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "third", bundle: nil)
        let addMember = storyboard.instantiateViewController(withIdentifier: "CreateAlbumViewController") as! CreateAlbumViewController
        addMember.isFrom =  "users"
        addMember.isFromDocs = self.isFromdocument
        //        addMember.faCate = self.familyArr![0].faCategory
        //        addMember.memberJoining = self.familyArr![0].memberApproval
        self.navigationController?.pushViewController(addMember, animated: false)
    }
    @IBAction func onClickBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func onClickDelete(_ sender: Any) {
        let alert = UIAlertController(title: "Familheey", message: "Do you want to delete \(self.arrrofUsers.count) albums", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { _ in
            
            /* let data = self.dataArr[indexPath.row]
             self.deleteFolder(fId: data["id"].stringValue)*/
            var parameter =  [String : Any]()
            parameter = ["folder_id":self.arrrofUsers,"user_id":UserDefaults.standard.value(forKey: "userId") as! String]
            print(parameter)
            ActivityIndicatorView.show("Loading....")
            
            self.networkProvider.request(.deleteFolder(parameter: parameter)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                        print(jsonData)
                        if response.statusCode == 200{
                            self.arrrofUsers = []
                            self.arrSelectedIndex = []
                            self.deleteVew.isHidden = true
                            self.getAllAlbums()
                        }
                        else if response.statusCode == 401 {
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                let btn = UIButton()
                                self.onClickDelete(btn)
                            }
                        }
                        
                        ActivityIndicatorView.hiding()
                        
                    }catch let err {
                        ActivityIndicatorView.hiding()
                        Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                    }
                case .failure( let error):
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                    break
                }
            }
            
        }))
        alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: { _ in
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func onClickSearchReset(_ sender: Any) {
        txtSerach.text = ""
        
        getAllAlbums()
        btnSearchReset.isHidden = true
        self.txtSerach.endEditing(true)
    }
    
    //MARK: - Convert Date
    
    func convertDateToDisplayDate(dateFromResponse:String)->String
    {
        let dateAsString               = dateFromResponse
        let dateFormatter              = DateFormatter()
        dateFormatter.dateFormat       = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        let date = dateFormatter.date(from: dateAsString)
        // print(date as Any)
        //  dateFormatter.dateFormat       = "dd MMM,h:mm a"
        dateFormatter.dateFormat       = "MMM dd yyyy"
        //  dateFormatter.dateStyle       = .short
        //  dateFormatter.timeStyle       = .none
        let DateFormatted = dateFormatter.string(from: date!)
        print(DateFormatted)
        return DateFormatted
    }
    
    //MARK:- collectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = colVew.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        let tempData = self.dataArr[indexPath.row]
        
        let userId = UserDefaults.standard.value(forKey: "userId") as! String
        let createdId = tempData["created_by"].stringValue
        if userId == createdId{
            self.setupLongPressGesture()
        }
        else{
            //            if self.isAdmin.lowercased() == "admin"{
            //                self.setupLongPressGesture()
            //            }
        }
        
        if let img = cell.viewWithTag(1) as? UIImageView{
            if tempData["cover_pic"].stringValue.count != 0 {
                img.kf.indicatorType   = .activity
                let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=475&height=400&url="+tempData["cover_pic"].stringValue.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                let url = URL(string: newUrlStr)
                img.kf.indicatorType = .activity
                
                img.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "imgNoImage_landscape"), options: nil, progressBlock: nil, completionHandler: nil)
            }
            else{
                img.image = #imageLiteral(resourceName: "imgNoImage_landscape")
            }
        }
        
        if let name = cell.viewWithTag(2) as? UILabel{
            if tempData["folder_name"].stringValue.count != 0{
                name.text = tempData["folder_name"].stringValue
            }
        }
        
        if let hosted = cell.viewWithTag(3) as? UILabel{
            if tempData["created_by_name"].stringValue.count != 0{
                hosted.text = tempData["created_by_name"].stringValue
                hosted.isHidden = false
                
            }else{
                hosted.text = "NA"
                hosted.isHidden = true
            }
        }
        
        if let dat = cell.viewWithTag(4) as? UILabel{
            if tempData["created_at"].stringValue.count != 0{
                dat.text = convertDateToDisplayDate(dateFromResponse: tempData["created_at"].stringValue)
            }else{
                dat.text = "NA"
            }
        }
        if let count = cell.viewWithTag(6) as? UILabel{
            if tempData["doc_count"].stringValue.count != 0{
                count.text =  tempData["doc_count"].stringValue
            }else{
                count.text = "0"
            }
        }
        if let bgView = cell.viewWithTag(5){
            if arrSelectedIndex.count == 0{
                bgView.isHidden = true
            }
            else{
                if arrSelectedIndex.contains(indexPath.row){
                    bgView.isHidden = false
                }
                else{
                    bgView.isHidden = true
                }
            }
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if isFromMultiSelect{
            if let index = arrSelectedIndex.firstIndex(of: indexPath.row){
                arrSelectedIndex.remove(at: index)
                arrrofUsers.remove(at: index)
            }
            else{
                arrSelectedIndex.append(indexPath.row)
                let data = self.dataArr[indexPath.row]
                arrrofUsers.append(data["id"].stringValue)
            }
            
            self.colVew.reloadData()
            if arrSelectedIndex.count == 0{
                self.deleteVew.isHidden = true
                isFromMultiSelect = false
                
            }
            
        }
        else{
            let story = UIStoryboard.init(name: "third", bundle: nil)
            let AlbumDetailsViewController =  story.instantiateViewController(withIdentifier: "AlbumDetailsViewController") as! AlbumDetailsViewController
            AlbumDetailsViewController.albumDetailsDict = self.dataArr[indexPath.row]
            AlbumDetailsViewController.isFrom = "users"
            //               AlbumDetailsViewController.groupId = self.groupID
            AlbumDetailsViewController.folder_type = "albums"
            AlbumDetailsViewController.createdBy = self.dataArr[indexPath.row]["created_by"].stringValue
            self.navigationController?.pushViewController(AlbumDetailsViewController, animated: true)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberOfCellInRow  = 2
        let padding : Int      = 5
        let collectionCellWidth : CGFloat = (self.colVew.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
        return CGSize(width: collectionCellWidth, height: collectionCellWidth * 1.3)
    }
    
    //MARK:- textfield delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        textField.resignFirstResponder()
        if textField == txtSerach
        {
            // selectWebAPI()
            if textField.text!.count > 0{
                btnSearchReset.isHidden = false
            }
            else{
                btnSearchReset.isHidden = true
            }
            textField.endEditing(true)
        }
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        self.view.endEditing(true)
        if textField == txtSerach
        {
            if textField.text!.count > 0{
                btnSearchReset.isHidden = false
            }
            else{
                btnSearchReset.isHidden = true
            }
            getAllAlbums()
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
            // selectWebAPI()
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
        }
        return true
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
