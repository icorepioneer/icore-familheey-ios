//
//  PostsInUserProfileViewController.swift
//  familheey
//
//  Created by familheey on 28/11/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON
import Kingfisher
import SafariServices

//PostsInUserProfileViewController

class PostsInUserProfileViewController: UIViewController, postUpdateDelegate  {
    
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var viewOfNoData: UIView!
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    var ArrayPosts = [JSON]()
    var searchTxt = ""
    var fromRefresh = false
    
    let maxNumberOfLines = 2
    var refreshControl = UIRefreshControl()
    
    var initiallimit = 20
    var offset = 0
    var limit = 150
    var lastFetchedIndex = 0;
    var postoptionsTittle = [String]()
    var documentInteractionController: UIDocumentInteractionController!
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearchRest: UIButton!
    @IBOutlet weak var tblListView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        limit = initiallimit
        lastFetchedIndex = initiallimit
        //        viewAnnouncement.isHidden = false
        txtSearch.delegate = self
        
        Helpers.setleftView(textfield: txtSearch, customWidth: 30)
        ArrayPosts.removeAll()
        
        refreshControl.attributedTitle = NSAttributedString(string: "Please wait...")
        refreshControl.addTarget(self, action: #selector(refreshPost), for: .valueChanged)
        tblListView.addSubview(refreshControl)
        //        getPosts()
    }
    override func viewWillAppear(_ animated: Bool) {
        getPosts()
        
    }
    
    //MARK:- Custom Methods
    @objc func refreshPost(refreshControl: UIRefreshControl) {
        refreshControl.endRefreshing()
        fromRefresh = true
        getPosts()
    }
    
    func getPosts(){
        ArrayPosts.removeAll()
        
        ActivityIndicatorView.show("Please wait....")
        let param = ["user_id" : UserDefaults.standard.value(forKey: "userId") as! String,"offset":"\(offset)","limit":"\(limit)","type":"post", "query":self.txtSearch.text!]
        
        networkProvider.request(.post_by_user(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        print("view contents : \(jsonData)")
                        
                        if let response = jsonData["data"].array{
                            
                            self.ArrayPosts = response
                            if  self.ArrayPosts.count == 0
                            {
                                self.lblNoData.isHidden = false
                                self.viewOfNoData.isHidden = false
                                self.tblListView.isHidden = true
                                if self.txtSearch.text == ""
                                {
                                    self.lblNoData.text = "No post yet?"
                                }
                                else
                                {
                                    self.lblNoData.text = "No results to show"
                                }
                                
                                
                            }
                            else
                            {
                                
                                self.lblNoData.isHidden = true
                                self.viewOfNoData.isHidden = true
                                self.tblListView.isHidden = false
                                self.tblListView.delegate = self
                                self.tblListView.dataSource = self
                                self.tblListView.reloadData()
                            }
                            
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getPosts()
                        }
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    // Open App Link
    func openAppLink(url:String){
        
        let param = ["url" : url]
        
        networkProvider.request(.openAppGetParams(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("@@##!!! REsult\(jsonData)")
                    if response.statusCode == 200{
                        let dataArr = jsonData["data"]
                        let subType = dataArr["sub_type"].stringValue
                        let type =  dataArr["type"].stringValue
                        
                        if subType == notificationSubTypes.publicfeed || subType == notificationSubTypes.familyfeed && type == notificationTypes.home{
                            let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
                            vc.postId = "\(dataArr["type_id"].intValue)"
                            self.navigationController?.pushViewController(vc, animated: false)
                            
                        }
                        else if type == notificationTypes.family{
                            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                            let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
                            intro.groupId = "\(dataArr["type_id"].intValue)"
                            self.navigationController?.pushViewController(intro, animated: false)
                        }
                        else{
                            let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
                            vc.enableEventEdit = false
                            vc.isFromNotification = "true"
                            vc.eventId = "\(dataArr["type_id"].intValue)"
                            self.navigationController?.pushViewController(vc, animated: false)
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.openAppLink(url: url)
                        }
                    }
                    else{
                        
                    }
                    
                }catch _ {
                    
                    
                }
            case .failure( _):
                break
            }
        }
    }
    
    //MARK:- Custom
    func postupdateSuccess(index:Int , SuccessData:JSON){
        
    }
    func deletePostForCreator(tag:Int){
        let alert = UIAlertController(title: "Familheey", message: "Do you really want to delete this post? Deleting this post will delete it from everywhere you posted it to and you will lose all associated conversations", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "DELETE", style: .destructive, handler: { _ in
            let post = self.ArrayPosts[tag]
            
            
            let param = ["id" : post["post_id"].stringValue,"user_id":UserDefaults.standard.value(forKey: "userId") as! String]
            
            ActivityIndicatorView.show("Please wait....")
            self.networkProvider.request(.deletePost(parameter: param)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                        print("view contents : \(jsonData)")
                        if response.statusCode == 200{
                            print("view contents : \(jsonData)")
                            self.getPosts()
//                            if tag < self.ArrayPosts.count{
//                                self.ArrayPosts.remove(at: tag)
//                                //                                    self.tblListView.deleteRows(at: [IndexPath.init(row: tag, section: 0)], with: .automatic)
//                                self.tblListView.reloadData()
//                            }
                        }
                        else if response.statusCode == 401 {
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                self.deletePostForCreator(tag: tag)
                            }
                        }
                        
                        ActivityIndicatorView.hiding()
                        
                    }catch let err {
                        ActivityIndicatorView.hiding()
                        // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                    }
                case .failure( let error):
                    ActivityIndicatorView.hiding()
                    //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                    break
                }
            }
            
        }))
        alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func removePostFromTimeline(tag:Int){
        let post = ArrayPosts[tag]
        
        let param = ["post_id" : post["post_id"].stringValue, "user_id": UserDefaults.standard.value(forKey: "userId") as! String]
        
        ActivityIndicatorView.show("Please wait....")
        networkProvider.request(.removePost(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        print("view contents : \(jsonData)")
                        if tag < self.ArrayPosts.count{
                            self.ArrayPosts.remove(at: tag)
                            //                            self.tblListView.deleteRows(at: [IndexPath.init(row: tag, section: 0)], with: .automatic)
                            self.tblListView.reloadData()
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.removePostFromTimeline(tag: tag)
                        }
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    func muteConversation(tag:Int){
        let post = ArrayPosts[tag]
        let param = ["post_id" : post["post_id"].stringValue, "user_id": UserDefaults.standard.value(forKey: "userId") as! String]
        
        ActivityIndicatorView.show("Please wait....")
        networkProvider.request(.mute_conversation(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        print("view contents : \(jsonData)")
                        let temp = jsonData["data"].dictionaryValue
                        if temp["is_active"]!.boolValue{
                            var temp = post
                            temp["muted"] = true
                            self.ArrayPosts[tag] = temp
                            print("#######NEW ARR - \(self.ArrayPosts[tag])")
                        }
                        else{
                            var temp = post
                            temp["muted"] = false
                            self.ArrayPosts[tag] = temp
                            print("#######NEW ARR - \(self.ArrayPosts[tag])")
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.muteConversation(tag: tag)
                        }
                    }
                    ActivityIndicatorView.hiding()
                }catch let err {
                    ActivityIndicatorView.hiding()
                    //  Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    //MARK: UIButton Actions
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickSearchRest(_ sender: Any) {
        txtSearch.text = ""
        searchTxt = ""
        
        self.getPosts()
        
        btnSearchRest.isHidden = true
        self.txtSearch.endEditing(true)
    }
    @IBAction func onClickFamilyName_NormalPost(_ sender: UIButton) {
        print("click normal family")
        
        var grpId = ""
        
        let post = ArrayPosts[sender.tag]
        
        
        
        grpId   = post["to_group_id"].stringValue
        
        //               grpId = "\(FamilyArr.familyList![sender.tag].faId)"
        
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
        intro.groupId = grpId
        self.navigationController?.pushViewController(intro, animated: true)
        
        
    }
    
    
    
    @IBAction func onClickFamilyName_SharedPost(_ sender: UIButton) {
        
        print("click shared family")
        
        var grpId = ""
        
        let post = ArrayPosts[sender.tag]
        grpId   = post["to_group_id"].stringValue
        
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
        intro.groupId = grpId
        self.navigationController?.pushViewController(intro, animated: true)
        
        
    }
    
    @IBAction func onClickReadmoreAction(_ sender: UIButton) {
        
        let cell = tblListView.cellForRow(at: IndexPath.init(row: sender.tag, section: 0)) as! PostImageTableViewCell
        if cell.labelPost.calculateMaxLines() > maxNumberOfLines &&  cell.labelPost.numberOfLines == maxNumberOfLines{
            
            tblListView.beginUpdates()
            cell.labelPost.numberOfLines = 0
            cell.readmoreButton.setTitle("view less", for: .normal)
            tblListView.endUpdates()
            
            
        }
        else if cell.labelPost.calculateMaxLines() > maxNumberOfLines &&  cell.labelPost.numberOfLines == 0 {
            UIView.performWithoutAnimation {
                tblListView.beginUpdates()
                cell.labelPost.numberOfLines = maxNumberOfLines
                cell.readmoreButton.setTitle("view more", for: .normal)
                tblListView.endUpdates()
            }
        }
        
    }
    
    @IBAction func onClickReadmoreShareAction(_ sender: UIButton) {
        
        let cell = tblListView.cellForRow(at: IndexPath.init(row: sender.tag, section: 0)) as! SharedPostTableViewCell
        if cell.labelPost.calculateMaxLines() > maxNumberOfLines &&  cell.labelPost.numberOfLines == maxNumberOfLines{
            
            tblListView.beginUpdates()
            cell.labelPost.numberOfLines = 0
            cell.readmoreButton.setTitle("view less", for: .normal)
            tblListView.endUpdates()
            
            
        }
        else if cell.labelPost.calculateMaxLines() > maxNumberOfLines &&  cell.labelPost.numberOfLines == 0 {
            UIView.performWithoutAnimation {
                tblListView.beginUpdates()
                cell.labelPost.numberOfLines = maxNumberOfLines
                cell.readmoreButton.setTitle("view more", for: .normal)
                tblListView.endUpdates()
            }
        }
        
    }
    
    @IBAction func onClickSharedUsersShareAction(_ sender: UIButton) {
        
        let post = ArrayPosts[sender.tag]
        
        if let Agcount =  post["aggregated_count"].string, Agcount == "1"{
            let mainStoryBoard = UIStoryboard(name: "fourth", bundle: nil)
            let vc = mainStoryBoard.instantiateViewController(withIdentifier: "UserFamilyListViewController") as! UserFamilyListViewController
            if post["orgin_id"].stringValue.count > 0{
                vc.postId = post["orgin_id"].stringValue
            }
            else{
                vc.postId = post["post_id"].stringValue
            }
            vc.isFrompost = true
            vc.isFrom = "groupshare"
            vc.familyID = post["to_group_id"].stringValue
            self.navigationController?.pushViewController(vc, animated:true)
        }
        else{
            let mainStoryBoard = UIStoryboard(name: "aggreatedPost", bundle: nil)
            let vc = mainStoryBoard.instantiateViewController(withIdentifier: "aggregatePostListViewController") as! aggregatePostListViewController
            vc.postRefId = post["post_ref_id"].stringValue
            self.navigationController?.pushViewController(vc, animated:true)
        }
        
    }
    
    //    @IBAction func onClickShareSharedPostAction(_ sender: UIButton) {
    //
    //        /* let post = ArrayPosts[sender.tag]
    //
    //         let mainStoryBoard = UIStoryboard(name: "third", bundle: nil)
    //         let vc = mainStoryBoard.instantiateViewController(withIdentifier: "PostShareViewController") as! PostShareViewController
    //         vc.postId = post["post_id"].stringValue
    //         self.navigationController?.pushViewController(vc, animated:true)*/
    //        self.ShareOptionArray = ["Share in Familheey","Share in Social Media"]
    //        showActionSheet(titleArr: ShareOptionArray as NSArray, title: "Select options") { (index) in
    //            if index == 0{
    //                let post = self.ArrayPosts[sender.tag]
    //                print(post)
    //                //                 DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
    //                let mainStoryBoard = UIStoryboard(name: "third", bundle: nil)
    //                let vc = mainStoryBoard.instantiateViewController(withIdentifier: "PostShareViewController") as! PostShareViewController
    //                vc.postId = post["post_id"].stringValue
    //                self.navigationController?.pushViewController(vc, animated: false)
    //                //                }
    //                //                 self.navigationController?.pushViewController(vc, animated: false)
    //            }
    //            else if index == 1{
    //                let post = self.ArrayPosts[sender.tag]
    //                let urlString =
    //                    Helpers.showActivityViewcontroller(url: "\(BaseUrl.PostUrlPrd)\(post["post_id"].stringValue)", VC: self)
    //            }
    //            else{
    //
    //            }
    //        }
    //    }
    
    @IBAction func onClickRightMenu(_ sender: UIButton) {
        let post = ArrayPosts[sender.tag]
        
        if let Agcount =  post["aggregated_count"].string, Agcount == "1"{
            if post["created_by"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")" {
                //                  postoptionsTittle =  ["Edit post","Share social media","Delete post"]
                if let sharedusers = post["shared_user_name"].string , !sharedusers.isEmpty{
                    postoptionsTittle =  ["Delete"]
                }
                else{
                    postoptionsTittle =  ["Edit","Delete"]
                }
                
                
            }
            else{
                //                  postoptionsTittle =  ["Remove Post","Share social media","Report"]
                postoptionsTittle =  ["Hide","Report"]
                
            }
            
            if post["muted"].stringValue == "true"{
                postoptionsTittle.insert("Unmute", at: 0)
            }
            else{
                postoptionsTittle.insert("Mute", at: 0)
            }
            showActionSheet(titleArr: postoptionsTittle as NSArray, title: "Select options") { (index) in
                if index == 2{
                    if post["created_by"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")"{
                        self.deletePostForCreator(tag: sender.tag)
                    }
                    else
                    {
                        let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "ReportPostViewController") as! ReportPostViewController
                        
                        vc.postDetails =  self.ArrayPosts[sender.tag]
                        vc.isFrom = "post"
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                    }
                }
                else if index == 1{
                    if post["created_by"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")"{
                        if let sharedusers = post["shared_user_name"].string , !sharedusers.isEmpty{
                            self.deletePostForCreator(tag: sender.tag)
                        }
                        else{
                            let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "CreatePostViewController") as! CreatePostViewController
                            
                            vc.fromEdit = true
                            vc.editPostDetails = post
                            vc.callBackDelegate = self
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                        
                    }else{
                        self.removePostFromTimeline(tag: sender.tag)
                    }
                }
                else if index == 100{
                }
                    //                  else if index == 2{
                    //                      Helpers.showActivityViewcontroller(url: "http://www.sampleurl.com", VC: self)
                    //                  }
                else{
                    self.muteConversation(tag: sender.tag)
                }
                
            }
        }
        else{
            let mainStoryBoard = UIStoryboard(name: "aggreatedPost", bundle: nil)
            let vc = mainStoryBoard.instantiateViewController(withIdentifier: "aggregatePostListViewController") as! aggregatePostListViewController
            vc.postRefId = post["post_ref_id"].stringValue
            self.navigationController?.pushViewController(vc, animated:true)
        }
    }
    
    @IBAction func onClickGroupShared(_ sender: UIButton) {
        let post = ArrayPosts[sender.tag]
        if let Agcount =  post["aggregated_count"].string, Agcount == "1"{
            let mainStoryBoard = UIStoryboard(name: "fourth", bundle: nil)
            let vc = mainStoryBoard.instantiateViewController(withIdentifier: "UserFamilyListViewController") as! UserFamilyListViewController
            vc.postId = post["post_id"].stringValue
            vc.isFrompost = true
            vc.isFrom = "groupshare"
            vc.familyID = post["to_group_id"].stringValue
            self.navigationController?.pushViewController(vc, animated:true)
        }
        else{
            let mainStoryBoard = UIStoryboard(name: "aggreatedPost", bundle: nil)
            let vc = mainStoryBoard.instantiateViewController(withIdentifier: "aggregatePostListViewController") as! aggregatePostListViewController
            vc.postRefId = post["post_ref_id"].stringValue
            self.navigationController?.pushViewController(vc, animated:true)
        }

    }
    
    @IBAction func onClickConversation(_ sender: UIButton) {
        let post = ArrayPosts[sender.tag]
        if let Agcount =  post["aggregated_count"].string, Agcount == "1"{
            let mainStoryBoard = UIStoryboard(name: "Conversations", bundle: nil)
            let vc = mainStoryBoard.instantiateViewController(withIdentifier: "ConversationsViewController") as! ConversationsViewController
            vc.post = post
            vc.conversationT = .post
            self.navigationController?.pushViewController(vc, animated:true)
        }
        else{
            let mainStoryBoard = UIStoryboard(name: "aggreatedPost", bundle: nil)
            let vc = mainStoryBoard.instantiateViewController(withIdentifier: "aggregatePostListViewController") as! aggregatePostListViewController
            vc.postRefId = post["post_ref_id"].stringValue
            self.navigationController?.pushViewController(vc, animated:true)
        }

    }
    
    @IBAction func onClickSharedBy(_ sender: UIButton) {
        let post = ArrayPosts[sender.tag]
        if let Agcount =  post["aggregated_count"].string, Agcount == "1"{
            let mainStoryBoard = UIStoryboard(name: "fourth", bundle: nil)
            let vc = mainStoryBoard.instantiateViewController(withIdentifier: "UserFamilyListViewController") as! UserFamilyListViewController
            //         if post["orgin_id"].stringValue.count > 0{
            //            vc.postId = post["orgin_id"].stringValue
            //        }
            //        else{
            vc.postId = post["post_id"].stringValue
            //        }
            vc.isFrompost = true
            vc.isFrom = "usershare"
            self.navigationController?.pushViewController(vc, animated:true)
        }
        else{
            let mainStoryBoard = UIStoryboard(name: "aggreatedPost", bundle: nil)
            let vc = mainStoryBoard.instantiateViewController(withIdentifier: "aggregatePostListViewController") as! aggregatePostListViewController
            vc.postRefId = post["post_ref_id"].stringValue
            self.navigationController?.pushViewController(vc, animated:true)
        }
    }
    
    @IBAction func onClickViews(_ sender: UIButton) {
        let post = ArrayPosts[sender.tag]
        if let Agcount =  post["aggregated_count"].string, Agcount == "1"{
            let mainStoryBoard = UIStoryboard(name: "fourth", bundle: nil)
            let vc = mainStoryBoard.instantiateViewController(withIdentifier: "UserFamilyListViewController") as! UserFamilyListViewController
            vc.postId = post["post_id"].stringValue
            vc.isFrompost = true
            vc.isFrom = "postview"
            self.navigationController?.pushViewController(vc, animated:true)
        }
        else{
            let mainStoryBoard = UIStoryboard(name: "aggreatedPost", bundle: nil)
            let vc = mainStoryBoard.instantiateViewController(withIdentifier: "aggregatePostListViewController") as! aggregatePostListViewController
            vc.postRefId = post["post_ref_id"].stringValue
            self.navigationController?.pushViewController(vc, animated:true)
        }
    }
    
    @IBAction func onClickPostShare(_ sender: UIButton) {
        let post = ArrayPosts[sender.tag]
        if let Agcount =  post["aggregated_count"].string, Agcount == "1"{
            let mainStoryBoard = UIStoryboard(name: "third", bundle: nil)
            let vc = mainStoryBoard.instantiateViewController(withIdentifier: "PostShareViewController") as! PostShareViewController
            vc.postId = post["post_id"].stringValue
            self.navigationController?.pushViewController(vc, animated:true)
        }
        else{
            let mainStoryBoard = UIStoryboard(name: "aggreatedPost", bundle: nil)
            let vc = mainStoryBoard.instantiateViewController(withIdentifier: "aggregatePostListViewController") as! aggregatePostListViewController
            vc.postRefId = post["post_ref_id"].stringValue
            self.navigationController?.pushViewController(vc, animated:true)
        }

    }
    
    @IBAction func onClickOpenRequest(_ sender: UIButton) {
        let need = ArrayPosts[sender.tag]
        if let postType = need["publish_type"].string, postType.lowercased() == "request"{
            let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "RequestDetailsViewController") as! RequestDetailsViewController
            vc.requestId = need["publish_id"].stringValue
            //        vc.requestDic = need
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            let storyboard = UIStoryboard.init(name: "third", bundle: nil)
            /* let addMember = storyboard.instantiateViewController(withIdentifier: "AlbumListingViewController") as! AlbumListingViewController
             addMember.groupID = need["to_group_id"].stringValue
             addMember.isFromPost = true
             addMember.isFromdocument = false
             addMember.albumId = need["publish_id"].stringValue
             
             
             self.navigationController?.pushViewController(addMember, animated: false)*/
            let AlbumDetailsViewController = storyboard.instantiateViewController(withIdentifier: "AlbumDetailsViewController") as! AlbumDetailsViewController
            //            AlbumDetailsViewController.albumDetailsDict = items
            AlbumDetailsViewController.isFrom = "groups"
            AlbumDetailsViewController.isFromPost = true
            AlbumDetailsViewController.groupId = need["to_group_id"].stringValue
            if let postType = need["publish_type"].string, postType.lowercased() == "albums"{
                AlbumDetailsViewController.folder_type = "albums"
            }
            else{
                AlbumDetailsViewController.folder_type = "documents"
                AlbumDetailsViewController.isFromDocs = true
            }
            
            AlbumDetailsViewController.createdBy = need["created_by"].stringValue
            AlbumDetailsViewController.albumId = need["publish_id"].stringValue
            self.navigationController?.pushViewController(AlbumDetailsViewController, animated: true)
        }
        
    }
    @IBAction func onClickThanksReadMore(_ sender: UIButton) {
        let cell = tblListView.cellForRow(at: IndexPath.init(row: sender.tag, section: 0)) as! PostThanksTableViewCell
        
        if cell.labelPost.calculateMaxLines() > maxNumberOfLines &&  cell.labelPost.numberOfLines == maxNumberOfLines{
            
            tblListView.beginUpdates()
            cell.labelPost.numberOfLines = 0
            cell.readmoreButton.setTitle("view less", for: .normal)
            tblListView.endUpdates()
            
            
        }
        else if cell.labelPost.calculateMaxLines() > maxNumberOfLines &&  cell.labelPost.numberOfLines == 0 {
            UIView.performWithoutAnimation {
                tblListView.beginUpdates()
                cell.labelPost.numberOfLines = maxNumberOfLines
                cell.readmoreButton.setTitle("view more", for: .normal)
                tblListView.endUpdates()
            }
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension PostsInUserProfileViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == txtSearch
        {
            // selectWebAPI()
            if textField.text!.count > 0{
                btnSearchRest.isHidden = false
            }
            else{
                btnSearchRest.isHidden = true
            }
            textField.endEditing(true)
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        self.view.endEditing(true)
        if textField == txtSearch
        {
            if textField.text!.count > 0{
                btnSearchRest.isHidden = false
            }
            else{
                btnSearchRest.isHidden = true
            }
            self.getPosts()
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text!.count > 0 {
            btnSearchRest.isHidden = false
        }
        else{
            btnSearchRest.isHidden = true
            // selectWebAPI()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text!.count > 0 {
            btnSearchRest.isHidden = false
        }
        else{
            btnSearchRest.isHidden = true
            // selectWebAPI()
        }
        return true
    }
}

extension PostsInUserProfileViewController : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ArrayPosts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard ArrayPosts.count != 0 else {
            return UITableViewCell.init()
        }
        
        let post = ArrayPosts[indexPath.row]
        
        if indexPath.row == 0{
            self.viewCurrentPost(index: indexPath.row)
        }
        
        if let postType = post["publish_type"].string, postType.lowercased() == "request" || postType.lowercased() == "albums" || postType.lowercased() == "documents"{
            
            let cellid = "PostThanksTableViewCell"
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! PostThanksTableViewCell
            
            
            cell.readmoreButton.tag = indexPath.row
            cell.buttonShare.tag = indexPath.row
            cell.buttonRightMenu.tag = indexPath.row
            cell.commentsButton.tag = indexPath.row
            cell.viewsButton.tag = indexPath.row
            cell.sharedUsersButton.tag = indexPath.row
            cell.buttonComments.tag = indexPath.row
            cell.buttonComments.tag = indexPath.row
            cell.btnFamilyClick.tag = indexPath.row
            cell.btnOpenRequest.tag = indexPath.row
            
            //User Details
            cell.labelUserName.text = post["created_user_name"].string ?? "Unknown"
            cell.labelPostedIn.text = post["group_name"].string ?? "Public"
            if cell.labelPostedIn.text == "Public"
            {
                cell.btnFamilyClick.isUserInteractionEnabled = false
            }
            else
            {
                cell.btnFamilyClick.isUserInteractionEnabled = true
                
            }
            //            cell.labelDate.text = post["createdAt"].string ?? ""
            cell.labelDate.text = HomeTabViewController.dateFormatterCommon(dateStr: post["createdAt"].stringValue)
            
            let proPic = post["propic"].string ?? ""
            
            DispatchQueue.main.async {
                
                if proPic.count != 0{
                    let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic
                    let imgUrl = URL(string: temp)
                    cell.imageViewProfilePic.kf.indicatorType = .activity
                    
                    cell.imageViewProfilePic.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                }
                else{
                    cell.imageViewProfilePic.image = #imageLiteral(resourceName: "Male Colored")
                }
            }
            cell.imageViewProfilePic.isTappable(id: post["created_by"].intValue)
            //Post
            cell.labelPost.text = post["snap_description"].string ?? ""
            
            //Details
            cell.lableNumberOfConversations.text = post["conversation_count"].stringValue
            cell.lblNumberOfShares.text = post["shared_user_count"].stringValue
            cell.labelNumberOfViews.text = post["views_count"].stringValue
            
            if let Attachment = post["post_attachment"].array, Attachment.count != 0{
                print("updating slide........")
                print(indexPath.row)
                cell.updateSliderThankYou(data: Attachment, Ptype: postType)
                
            }
            else{
                print("No Slide........")
                print(indexPath.row)
                
                cell.viewPostBg.isHidden = false
                cell.viewPageControllBG.isHidden = false
                if let urlArray = post["valid_urls"].array, urlArray.count != 0{
                    if urlArray.count == 1{
                        if let urlMetadata = post["url_metadata"].dictionary, urlMetadata.count != 0{
                            if let result = urlMetadata["urlMetadataResult"], result.count != 0{
                                cell.viewPostBg.isHidden = false
                                cell.viewPageControllBG.isHidden = true
                                
                                cell.updateSliderLink(data: [result])
                            }
                            else{
                                print(indexPath.row)
                                cell.viewPostBg.isHidden = true
                                cell.viewPageControllBG.isHidden = true
                            }
                        }
                        
                        
                    }
                    else{
                        print(indexPath.row)
                        cell.viewPostBg.isHidden = true
                        cell.viewPageControllBG.isHidden = true
                    }
                }
                else{
                    print(indexPath.row)
                    cell.viewPostBg.isHidden = true
                    cell.viewPageControllBG.isHidden = true
                }
            }
            
            if cell.labelPost.calculateMaxLines() > maxNumberOfLines{
                cell.labelPost.numberOfLines = maxNumberOfLines
                cell.readMoreView.isHidden = false
                
                //            tableView.layoutIfNeeded()
            }
            else{
                cell.labelPost.numberOfLines = 0
                cell.readMoreView.isHidden = true
                //            tableView.layoutIfNeeded()
            }
            //                cell.delegate = self
            
            
            // String(describing: UserDefaults.standard.value(forKey: "userId"))
            let user = UserDefaults.standard.value(forKey: "userId") as! String
            if  user  == post["created_by"].stringValue{
                
                cell.viewViews.isHidden = false
                cell.viewShared.isHidden = false
                if post["conversation_enabled"].bool ?? false{
                    cell.viewComments.isHidden = false
                }
                else{
                    cell.viewComments.isHidden = true
                }
            }
            else{
                
                cell.viewViews.isHidden = true
                cell.viewShared.isHidden = true
                if post["conversation_enabled"].bool ?? false{
                    cell.viewComments.isHidden = false
                }
                else{
                    cell.viewComments.isHidden = true
                }
            }
            
            if post["conversation_count_new"].intValue > 0{
                cell.lblNewConvrstn.isHidden = false
            }
            else{
                cell.lblNewConvrstn.isHidden = true
            }
            
            
            if post["is_shareable"].bool ?? false {
                
                cell.shareView.isHidden = false
                
            }
            else{
                
                cell.shareView.isHidden = true
                
            }
//            var stringArr = [String]()
//            if let mentionedUsers = post["publish_mention_users"].array, mentionedUsers.count != 0{
//                for index in mentionedUsers{
//                    let name = index["user_name"].stringValue
//                    let temArr = name.components(separatedBy: " ")
//                    if temArr.count > 0{
//                        let formattedName = "@\(temArr[0])"
//                        stringArr.append(formattedName)
//                    }
//                }
//                cell.lblMentionedNames.text = stringArr.joined(separator: ", ")
//            }
//            else{
//                cell.lblMentionedNames.text = ""
//            }
            cell.btnMentionedNames.tag = indexPath.row

             if let mentionedUsers = post["publish_mention_users"].array, mentionedUsers.count != 0{

                               if mentionedUsers.count == 1
                               {
            cell.lblMentionedNames.text = "@ \(mentionedUsers[0]["user_name"].stringValue.components(separatedBy: " ")[0])"
                                                   cell.lblMentionedNames.isHidden = false
                                cell.btnMentionedNames.isHidden = false
                                
                                                   cell.collVwOfMentionedNames.isHidden = true

                                                   cell.heightOfMentionedView.constant = 30
                              
                                
                                }
                                else
                               {
                                cell.lblMentionedNames.isHidden = true
                                cell.btnMentionedNames.isHidden = true

                                                                      cell.collVwOfMentionedNames.isHidden = false
                                cell.loadMentionedNames(data:mentionedUsers)

                                }
                                

                            }
                            else{
                                cell.lblMentionedNames.text = ""
                                cell.lblMentionedNames.isHidden = true
                                cell.collVwOfMentionedNames.isHidden = true
                                cell.btnMentionedNames.isHidden = true

                                cell.heightOfMentionedView.constant = 0

                            }
            cell.viewShared.isHidden = true
            if let type = post["publish_type"].string, type.lowercased() == "request"{
                cell.lblItemNames.isHidden = false
                if let mentionedItems = post["publish_mention_items"].array, mentionedItems.count != 0{
                    //                    for index in mentionedUsers{
                    //                        let name = index["user_name"].stringValue
                    //                        let temArr = name.components(separatedBy: " ")
                    //                        if temArr.count > 0{
                    //                            let formattedName = "@\(temArr[0])"
                    //                            stringArr.append(formattedName)
                    //                        }
                    //                    }
                    cell.lblItemNames.text = mentionedItems[0]["item_name"].stringValue
                }
                else{
                    cell.lblItemNames.text = ""
                }
                cell.btnOpenRequest.setTitle("Open Request", for: .normal)
                cell.viewViews.isHidden = true
            }
            else{
                cell.lblItemNames.text = ""
                cell.lblItemNames.isHidden = true
                cell.viewViews.isHidden = false
                
                cell.delegate = self
                if let type = post["publish_type"].string, type.lowercased() == "albums"{
                    cell.btnOpenRequest.setTitle("Open Album", for: .normal)
                }
                else{
                    cell.btnOpenRequest.setTitle("Open Folder", for: .normal)
                }
            }
            
            cell.labelPost.handleHashtagTap { hashtag in
                print("Success. You just tapped the \(hashtag) hashtag")
                self.txtSearch.text  = "#"+hashtag
                //                self.searchview.isHidden = false
                //                self.searchConstraintheight.constant = 36
                self.txtSearch.becomeFirstResponder()
                
            }
            cell.labelPost.handleMentionTap { (mension) in
                print("Success. You just tapped the \(mension) mension")
                
                
            }
            cell.labelPost.handleURLTap { (url) in
                print(url)
                if url.absoluteString.contains("familheey"){
                    self.openAppLink(url: url.absoluteString)
                }
                else{
                    if !(["http", "https"].contains(url.scheme?.lowercased())) {
                        var strUrl = url.absoluteString
                        strUrl = "http://"+strUrl
                        let Turl = URL(string: strUrl)
                        
                        let vc = SFSafariViewController(url: Turl!)
                        self.present(vc, animated: true, completion: nil)
                    }
                    else{
                        let vc = SFSafariViewController(url: url)
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            }
            
            return cell
            
        }
            
        else{
            if let shareduser = post["shared_user_names"].string , !shareduser.isEmpty{
                
                
                let cellid = "SharedPostTableViewCell"
                
                let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! SharedPostTableViewCell
                
                let post = ArrayPosts[indexPath.row]
                
                cell.readmoreButton.tag = indexPath.row
                cell.buttonShare.tag = indexPath.row
                cell.buttonSharedUsers.tag = indexPath.row
                cell.btnRightMrnu.tag = indexPath.row
//                cell.commentsButton.tag = indexPath.row
                cell.buttonComments.tag = indexPath.row
                cell.viewsButtons.tag = indexPath.row
//                cell.sharedUserButton.tag = indexPath.row
                
                
                //User Details
                
                if let sharedgroups = post["other_group_names"].string , !sharedgroups.isEmpty{
                    
                    let array = sharedgroups.components(separatedBy: ",")
                    
                    if array.count != 0{
                        if array.count == 1{
                            cell.labelUserName.text = "You shared a post to \(array[0])"
                        }
                        else{
                            if array[0].lowercased() == "null"{
                                if let username = post["to_user_name"].string, !username.isEmpty{
                                    cell.labelUserName.text = "You shared a post to \(username) and \(array.count-1) other groups"
                                }
                            }
                            else{
                                cell.labelUserName.text = "You shared a post to \(array[0]) and \(array.count-1) other groups"
                            }
                        }
                        
                    }
                    else{
                        cell.labelUserName.text = "You shared a post"
                    }
                }
                else if let username = post["to_user_name"].string, !username.isEmpty{
                    cell.labelUserName.text = "You shared a post to \(username)"
                }
                else{
                    cell.labelUserName.text = "You shared a post"
                }
                
                //            let array = shareduser.components(separatedBy: ",")
                //            if array.count != 0{
                //                if array.count == 1{
                //                    cell.labelUserName.text = "\(array[0]) shared a post"
                //                }
                //                else if array.count == 2{
                //                    cell.labelUserName.text = "\(array[0]) and 1 other shared a post"
                //                }
                //                else {
                //                    cell.labelUserName.text = "\(array[0]) and \(array.count-1) others shared a post"
                //                }
                //                //post["created_user_name"].string ?? ""
                //
                //            }
                
                //            cell.lblShared.isHidden = true
                //            cell.labelPostedIn.isHidden = true
                if post["to_user_id"].stringValue == UserDefaults.standard.value(forKey: "userId") as! String{
                    cell.lblShared.text = "Shared with"
                    cell.lblShare_width.constant = 80
                    cell.labelPostedIn.text = " you"
                }
                else{
                    cell.lblShared.text = "Shared in"
                    cell.lblShare_width.constant = 65
                    if post["group_name"].stringValue.count > 0{
                        cell.labelPostedIn.text = post["group_name"].string ?? ""
                    }
                    else{
                        cell.labelPostedIn.text = post["group_name"].string ?? "\(post["privacy_type"].stringValue)"
                    }
                    
                }
                
                //            cell.labelPostedInInner.text = post["parent_post_grp_name"].string ?? ""
                if post["parent_post_grp_name"].stringValue.count > 0{
                    cell.labelPostedInInner.text = post["parent_post_grp_name"].string ?? ""
                }
                else{
                    cell.labelPostedInInner.text = post["privacy_type"].stringValue
                }
                if post["parent_post_grp_name"].stringValue.count > 0{
                    cell.labelPostedIn.text = post["parent_post_grp_name"].string ?? ""
                }
                else{
                    cell.labelPostedIn.text = post["privacy_type"].stringValue
                }
                //            if post["parent_post_created_user_name"].stringValue.count > 0{
                //                cell.labelUserNameInner.text = post["parent_post_created_user_name"].string ?? ""
                //            }
                //            else{
                //                cell.labelUserNameInner.text = post["privacy_type"].stringValue ?? "\(post["privacy_type"].stringValue)"
                //            }
                cell.labelUserNameInner.text = post["parent_post_created_user_name"].string ?? ""
                
                //            cell.labelDate.text = post["createdAt"].string ?? ""
                cell.labelDate.text = HomeTabViewController.dateFormatterCommon(dateStr: post["createdAt"].stringValue)
                
                let proPic = post["parent_post_created_propic"].stringValue
                
                
                if proPic.count != 0{
                    let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic
                    let imgUrl = URL(string: temp)
                    cell.imageviewProfilePicInner.kf.indicatorType = .activity
                    cell.imageviewProfilePicInner.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                }
                else{
                    cell.imageviewProfilePicInner.image = #imageLiteral(resourceName: "Male Colored")
                }
                
                //Post
                cell.labelPost.text = post["snap_description"].string ?? ""
                
                //Details
                cell.lableNumberOfConversations.text = post["conversation_count"].stringValue
                cell.lblNumberOfShares.text = post["shared_user_count"].stringValue
                cell.lblNumberOfViews.text = post["views_count"].stringValue
                
                if let Attachment = post["post_attachment"].array, Attachment.count != 0{
                    cell.updateSlider(data: Attachment)
                }
                else{
                    print("No Slide........")
                    print(indexPath.row)
                    
                    if let urlArray = post["valid_urls"].array, urlArray.count != 0{
                        if urlArray.count == 1{
                            if let urlMetadata = post["url_metadata"].dictionary, urlMetadata.count != 0{
                                if let result = urlMetadata["urlMetadataResult"], result.count != 0{
                                    cell.viewPostBg.isHidden = false
                                    cell.viewPageControllBG.isHidden = true
                                    
                                    cell.updateSliderLink(data: [result])
                                }
                                else{
                                    print(indexPath.row)
                                    cell.viewPostBg.isHidden = true
                                    cell.viewPageControllBG.isHidden = true
                                }
                            }
                        }
                        else{
                            print(indexPath.row)
                            cell.viewPostBg.isHidden = true
                            cell.viewPageControllBG.isHidden = true
                        }
                    }
                    else{
                        print(indexPath.row)
                        cell.viewPostBg.isHidden = true
                        cell.viewPageControllBG.isHidden = true
                    }
                }
                
                
                if cell.labelPost.calculateMaxLines() > maxNumberOfLines{
                    cell.labelPost.numberOfLines = maxNumberOfLines
                    cell.readMoreView.isHidden = false
                    
                    //            tableView.layoutIfNeeded()
                }
                else{
                    cell.labelPost.numberOfLines = 0
                    cell.readMoreView.isHidden = true
                    //            tableView.layoutIfNeeded()
                }
                cell.delegate = self
                
                
                let user = UserDefaults.standard.value(forKey: "userId") as! String
                if  user  == post["created_by"].stringValue{
                    //                    cell.viewViews.isHidden = false
                    //                    cell.viewShared.isHidden = false
                    if post["conversation_enabled"].bool ?? false{
                        cell.ViewComments.isHidden = false
                    }
                    else{
                        cell.ViewComments.isHidden = true
                    }
                }
                else{
                    //                    cell.viewViews.isHidden = true
                    //                    cell.viewShared.isHidden = true
                    if post["conversation_enabled"].bool ?? false{
                        cell.ViewComments.isHidden = false
                    }
                    else{
                        cell.ViewComments.isHidden = true
                    }
                }
                
                if post["conversation_count_new"].intValue > 0{
                    cell.lblNewConverstn.isHidden = false
                }
                else{
                    cell.lblNewConverstn.isHidden = true
                }
                
                if post["is_shareable"].bool ?? false {
                    
                    cell.shareView.isHidden = false
                    
                }
                else{
                    
                    cell.shareView.isHidden = true
                    
                }
                
                cell.labelPost.handleHashtagTap { hashtag in
                    print("Success. You just tapped the \(hashtag) hashtag")
                    self.txtSearch.text  = "#"+hashtag
                    //                self.sea.isHidden = false
                    //                self.searchConstraintheight.constant = 36
                    self.txtSearch.becomeFirstResponder()
                    
                }
                cell.labelPost.handleMentionTap { (mension) in
                    print("Success. You just tapped the \(mension) mension")
                    self.txtSearch.text  = mension
                    //                self.searchview.isHidden = false
                    //                self.searchConstraintheight.constant = 36
                    self.txtSearch.becomeFirstResponder()
                    
                }
                cell.labelPost.handleURLTap { (url) in
                    print(url)
                    if url.absoluteString.contains("familheey"){
                        self.openAppLink(url: url.absoluteString)
                    }
                    else{
                        if !(["http", "https"].contains(url.scheme?.lowercased())) {
                            var strUrl = url.absoluteString
                            strUrl = "http://"+strUrl
                            let Turl = URL(string: strUrl)
                            
                            let vc = SFSafariViewController(url: Turl!)
                            self.present(vc, animated: true, completion: nil)
                        }
                        else{
                            let vc = SFSafariViewController(url: url)
                            self.present(vc, animated: true, completion: nil)
                        }
                    }
                }
                //pagination
                //                  if indexPath.row == self.ArrayPosts.count - 1 {
                //
                //                        offset = ArrayPosts.count + 1
                //                        if selectedIndex == 0{
                //                            getPosts()
                //                        }
                //                        else{
                //                            getpublicfeed()
                //                        }
                //
                //                    }
                return cell
            }
            else{
                let cellid = "PostImageTableViewCell"
                
                let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! PostImageTableViewCell
                
                
                cell.readmoreButton.tag = indexPath.row
                cell.buttonShare.tag = indexPath.row
                cell.buttonRightMenu.tag = indexPath.row
                cell.commentsButton.tag = indexPath.row
                cell.viewsButton.tag = indexPath.row
                cell.sharedUsersButton.tag = indexPath.row
                cell.buttonComments.tag = indexPath.row
                
                //User Details
                cell.labelUserName.text = post["created_user_name"].string ?? ""
                //            cell.labelPostedIn.text = post["group_name"].string ?? ""
                
                if post["group_name"].stringValue.count > 0{  // Change to other group name
                    cell.labelPostedIn.text = post["group_name"].string ?? ""
                }
                else{
                    cell.labelPostedIn.text = post["group_name"].string ?? "\(post["privacy_type"].stringValue)"
                }
                //            cell.lblPostedHead.isHidden = true
                //            cell.labelPostedIn.isHidden = true
                cell.labelDate.text = post["createdAt"].string ?? ""
                cell.labelDate.text = HomeTabViewController.dateFormatterCommon(dateStr: post["createdAt"].stringValue)
                
                let proPic = post["propic"].string ?? ""
                
                
                if proPic.count != 0{
                    let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic
                    let imgUrl = URL(string: temp)
                    cell.imageViewProfilePic.kf.indicatorType = .activity
                    cell.imageViewProfilePic.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                }
                else{
                    cell.imageViewProfilePic.image = #imageLiteral(resourceName: "Male Colored")
                }
                
                //Post
                cell.labelPost.text = post["snap_description"].string ?? ""
                
                //Details
                cell.lableNumberOfConversations.text = post["conversation_count"].stringValue
                cell.lblNumberOfShares.text = post["shared_user_count"].stringValue
                cell.labelNumberOfViews.text = post["views_count"].stringValue
                
                if let Attachment = post["post_attachment"].array, Attachment.count != 0{
                    cell.updateSlider(data: Attachment)
                }
                else{
                    print("No Slide........")
                    print(indexPath.row)
                    
                    if let urlArray = post["valid_urls"].array, urlArray.count != 0{
                        if urlArray.count == 1{
                            if let urlMetadata = post["url_metadata"].dictionary, urlMetadata.count != 0{
                                if let result = urlMetadata["urlMetadataResult"], result.count != 0{
                                    cell.viewPostBg.isHidden = false
                                    cell.viewPageControllBG.isHidden = true
                                    
                                    cell.updateSliderLink(data: [result])
                                }
                                else{
                                    print(indexPath.row)
                                    cell.viewPostBg.isHidden = true
                                    cell.viewPageControllBG.isHidden = true
                                }
                            }
                        }
                        else{
                            print(indexPath.row)
                            cell.viewPostBg.isHidden = true
                            cell.viewPageControllBG.isHidden = true
                        }
                    }
                    else{
                        print(indexPath.row)
                        cell.viewPostBg.isHidden = true
                        cell.viewPageControllBG.isHidden = true
                    }
                }
                
                
                if cell.labelPost.calculateMaxLines() > maxNumberOfLines{
                    cell.labelPost.numberOfLines = maxNumberOfLines
                    cell.readMoreView.isHidden = false
                    
                    //            tableView.layoutIfNeeded()
                }
                else{
                    cell.labelPost.numberOfLines = 0
                    cell.readMoreView.isHidden = true
                    //            tableView.layoutIfNeeded()
                }
                cell.delegate = self
                
                
                // String(describing: UserDefaults.standard.value(forKey: "userId"))
                let user = UserDefaults.standard.value(forKey: "userId") as! String
                if  user  == post["created_by"].stringValue{
                    
                    cell.viewViews.isHidden = false
                    cell.viewShared.isHidden = false
                    if post["conversation_enabled"].bool ?? false{
                        cell.viewComments.isHidden = false
                    }
                    else{
                        cell.viewComments.isHidden = true
                    }
                }
                else{
                    
                    cell.viewViews.isHidden = true
                    cell.viewShared.isHidden = true
                    if post["conversation_enabled"].bool ?? false{
                        cell.viewComments.isHidden = false
                    }
                    else{
                        cell.viewComments.isHidden = true
                    }
                }
                
                if post["conversation_count_new"].intValue > 0{
                    cell.lblNewConvrstn.isHidden = false
                }
                else{
                    cell.lblNewConvrstn.isHidden = true
                }
                
                if post["is_shareable"].bool ?? false {
                    cell.shareView.isHidden = false
                }
                else{
                    cell.shareView.isHidden = true
                }
                
                if let sharedgroups = post["other_group_names"].string , !sharedgroups.isEmpty{
                    
                    let array = sharedgroups.components(separatedBy: ",")
                    
                    if array.count != 0{
                        if array.count == 1{
                            //                        cell.labelUserName.text = "You posted in \(array[0])"
                            cell.labelUserName.text = post["created_user_name"].stringValue
                        }
                        else{
                            if array[0].lowercased() == "null"{
                                if let username = post["to_user_name"].string, !username.isEmpty{
                                    cell.labelUserName.text = "You shared a post to \(username) and \(array.count-1) other groups"
                                }
                            }
                            else{
                                cell.labelUserName.text = "You posted in \(array[0]) and \(array.count-1) other groups"
                            }
                        }
                        
                    }
                    else{
                        cell.labelUserName.text = post["created_user_name"].stringValue
                    }
                }
                else if let username = post["other_user_names"].string, !username.isEmpty{
                    //                cell.labelUserName.text = "You posted in \(username)'s timeline"
                    let array = username.components(separatedBy: ",")
                    
                    if array.count != 0{
                        if array.count == 1{
                            if user == post["created_by"].stringValue{
                                //                            cell.labelUserName.text = "You posted in \(array[0])"
                                cell.labelUserName.text = post["created_user_name"].stringValue
                                cell.labelPostedIn.text = "\(array[0])"
                                cell.lblPostedHead.text = "Posted to"
                            }
                            else{
                                cell.labelUserName.text = "\(post["created_user_name"].stringValue) posted in your timeline"
                            }
                            
                        }
                        else{
                            cell.labelUserName.text = "You posted in \(array[0]) and \(array.count-1) other groups"
                        }
                        
                    }
                    else{
                        cell.labelUserName.text = post["created_user_name"].stringValue
                    }
                }
                else{
                    cell.labelUserName.text = post["created_user_name"].stringValue
                }
                
                cell.labelPost.handleHashtagTap { hashtag in
                    print("Success. You just tapped the \(hashtag) hashtag")
                    self.txtSearch.text  = "#"+hashtag
                    //                self.searchview.isHidden = false
                    //                self.searchConstraintheight.constant = 36
                    self.txtSearch.becomeFirstResponder()
                    
                }
                cell.labelPost.handleMentionTap { (mension) in
                    print("Success. You just tapped the \(mension) mension")
                    self.txtSearch.text  = mension
                    //                self.searchview.isHidden = false
                    //                self.searchConstraintheight.constant = 36
                    self.txtSearch.becomeFirstResponder()
                    
                }
                cell.labelPost.handleURLTap { (url) in
                    print(url)
                    if url.absoluteString.contains("familheey"){
                        self.openAppLink(url: url.absoluteString)
                    }
                    else{
                        if !(["http", "https"].contains(url.scheme?.lowercased())) {
                            var strUrl = url.absoluteString
                            strUrl = "http://"+strUrl
                            let Turl = URL(string: strUrl)
                            
                            let vc = SFSafariViewController(url: Turl!)
                            self.present(vc, animated: true, completion: nil)
                        }
                        else{
                            let vc = SFSafariViewController(url: url)
                            self.present(vc, animated: true, completion: nil)
                        }
                    }
                }
                
                return cell
            }
            
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func getnumberOfLines(label:UILabel) -> Int{
        var   lineCount:NSInteger = 0
        let  textSize:CGSize = CGSize.init(width: label.frame.size.width, height: CGFloat(MAXFLOAT))
        let   rHeight:Int = lroundf(Float(label.sizeThatFits(textSize).height))
        let   charSize:Int = lroundf(Float(label.font!.lineHeight))
        lineCount = rHeight/charSize
        NSLog("No of lines: %i",lineCount)
        return lineCount
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if fromRefresh{
            fromRefresh = false
        }
        else{
            var currentCellOffset = scrollView.contentOffset
            currentCellOffset.x += (self.tblListView.bounds.width);
            
            let indepath = self.tblListView.indexPathsForVisibleRows?.first
            if indepath != nil{
                self.viewCurrentPost(index: (indepath?.row)!)
            }
        }
    }
    
    func viewCurrentPost(index:Int){
        
        if  ArrayPosts.count > index  {
            
            let post = ArrayPosts[index]
            
            let param = ["post_id" : post["post_id"].stringValue, "user_id": UserDefaults.standard.value(forKey: "userId") as! String]
            networkProvider.request(.add_view_count(parameter: param)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                        print("view contents : \(jsonData)")
                        if response.statusCode == 200{
                            print("view contents : \(jsonData)")
                        }
                        else if response.statusCode == 401 {
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                self.viewCurrentPost(index: index)                            }
                        }
                        
                    }catch _ {
                        
                    }
                case .failure( _):
                    break
                }
            }
        }
    }
    
}

extension PostsInUserProfileViewController : PostImageTableViewCellDelegate,UIDocumentInteractionControllerDelegate{
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    func share(url: URL) {
        print(url)
        documentInteractionController = UIDocumentInteractionController(url: url)
        //        documentInteractionController.uti = url.typeIdentifier ?? "public.data, public.content"
        //        documentInteractionController.name = url.localizedName ?? url.lastPathComponent
        documentInteractionController.delegate = self
        documentInteractionController.presentPreview(animated: true)
    }
    
    func gotoSinglePreview(data: String) {
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: "ChangePicturesViewController") as! ChangePicturesViewController
        vc.isCoverOrProfilePic = "CoverPic"
        vc.ids           = UserDefaults.standard.value(forKey: "userId") as! String
        vc.isFrom = "post"
        vc.inFor = "cover"
        vc.isAdmin = ""
        
        vc.imageUrl = data
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func gotoPreview(data: [JSON], index: Int){
        
        if let type = data[index]["type"].string ,( type.contains("text") || type.contains("pdf") || type.contains("doc") || type.contains("docx") || type.contains("xls") || type.contains("xlsx") || type.contains("msword") || type.contains("txt")) {
            var temp:String!
            temp = "\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+data[index]["filename"].stringValue
            let url = URL(string: temp)!
            ActivityIndicatorView.show("Loading....")
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else { return }
                let tmpURL = FileManager.default.temporaryDirectory
                    .appendingPathComponent(response?.suggestedFilename ?? "fileName.png")
                do {
                    try data.write(to: tmpURL)
                } catch {
                    print(error)
                    ActivityIndicatorView.hiding()
                }
                DispatchQueue.main.async {
                    /// STOP YOUR ACTIVITY INDICATOR HERE
                    ActivityIndicatorView.hiding()
                    print(tmpURL)
                    self.share(url: tmpURL)
                }
            }.resume()
            /*let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
             
             let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementDocPreviewViewController") as! AnnouncementDocPreviewViewController
             vc.UrlToLoad = temp
             self.present(vc, animated: true, completion: nil)*/
            
        }
        else if let type = data[index]["url"].string {
            if type.isEmpty{
                
            }
            else{
                print(type.contains("familheey"))
                if type.contains("familheey"){
                    self.openAppLink(url: type)
                }
                else{
                    //                    UIApplication.shared.open(url)
                    let url = URL(string: type)!
                    let vc = SFSafariViewController(url: url)
                    self.present(vc, animated: true, completion: nil)
                }
            }
            
        }
        else{
            let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsPreviewViewController") as! AnnouncementsPreviewViewController
            vc.attachments = data
            vc.isFromPost = true
            vc.selectedIndex = index
            self.navigationController?.addFadeAnimation()
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    //    {
    //
    //        let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
    //        let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsPreviewViewController") as! AnnouncementsPreviewViewController
    //        vc.attachments = data
    //        vc.isFromPost = true
    //        vc.selectedIndex = index
    //        self.navigationController?.addFadeAnimation()
    //        self.navigationController?.pushViewController(vc, animated: false)
    //    }
    
    
}
extension PostsInUserProfileViewController : PostThanksableViewCellDelegate{
    func gotoThanksPreview(data: [JSON], index: Int) {
        
        if let type = data[index]["type"].string ,( type.contains("text") || type.contains("pdf") || type.contains("doc") || type.contains("docx") || type.contains("xls") || type.contains("xlsx") || type.contains("msword") || type.contains("txt")) {
            var temp:String!
            temp = "\(Helpers.imageURl)"+"\(BaseUrl.document_image)"+data[index]["filename"].stringValue
            let url = URL(string: temp)!
            ActivityIndicatorView.show("Loading....")
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else { return }
                let tmpURL = FileManager.default.temporaryDirectory
                    .appendingPathComponent(response?.suggestedFilename ?? "fileName.png")
                do {
                    try data.write(to: tmpURL)
                } catch {
                    print(error)
                    ActivityIndicatorView.hiding()
                }
                DispatchQueue.main.async {
                    /// STOP YOUR ACTIVITY INDICATOR HERE
                    ActivityIndicatorView.hiding()
                    print(tmpURL)
                    self.share(url: tmpURL)
                }
            }.resume()
            /*let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
             
             let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementDocPreviewViewController") as! AnnouncementDocPreviewViewController
             vc.UrlToLoad = temp
             self.present(vc, animated: true, completion: nil)*/
        }
        else if let type = data[index]["url"].string {
            if type.isEmpty{
                
            }
            else{
                print(type.contains("familheey"))
                if type.contains("familheey"){
                    self.openAppLink(url: type)
                }
                else{
                    //                    UIApplication.shared.open(url)
                    let url = URL(string: type)!
                    let vc = SFSafariViewController(url: url)
                    self.present(vc, animated: true, completion: nil)
                }
            }
            
        }
        else{
            let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsPreviewViewController") as! AnnouncementsPreviewViewController
            vc.attachments = data
            vc.isFromPost = true
            vc.typeAlbum = true
            vc.selectedIndex = index
            self.navigationController?.addFadeAnimation()
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
    }
    
    func gotoThanksSinglePreview(data: String) {
        /*let storyboard = UIStoryboard.init(name: "second", bundle: nil)
         
         let vc = storyboard.instantiateViewController(withIdentifier: "ChangePicturesViewController") as! ChangePicturesViewController
         vc.isCoverOrProfilePic = "CoverPic"
         vc.ids           = UserDefaults.standard.value(forKey: "userId") as! String
         vc.isFrom = "post"
         vc.inFor = "cover"
         vc.isAdmin = ""
         vc.imageUrl = data
         self.imgClicked = true
         self.navigationController?.pushViewController(vc, animated: true)*/
        
    }
    func selectMentionedName(data: [JSON], index: Int) {
          
          let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                 let vc = storyboard.instantiateViewController(withIdentifier: "UserProfileTableViewController") as! UserProfileTableViewController
          let userId = data[index]["user_id"].stringValue
                 vc.userID = userId
                 self.navigationController?.pushViewController(vc, animated: true)
          
      }
      @IBAction func btnMentinedNameClicked(_ sender: UIButton)
          {
              let post = ArrayPosts[sender.tag]
          
              
              let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                          let vc = storyboard.instantiateViewController(withIdentifier: "UserProfileTableViewController") as! UserProfileTableViewController
                   let userId = post["publish_mention_users"][0]["user_id"].stringValue
                          vc.userID = userId
                          self.navigationController?.pushViewController(vc, animated: true)
          }
}
extension PostsInUserProfileViewController : SharedPostTableViewCellDelegate{
    func gotoSharedPreview(data: [JSON], index: Int){
        
        if let type = data[index]["type"].string ,( type.contains("text") || type.contains("pdf") || type.contains("doc") || type.contains("docx") || type.contains("xls") || type.contains("xlsx") || type.contains("msword") || type.contains("txt")) {
            var temp:String!
            temp = "\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+data[index]["filename"].stringValue
            let url = URL(string: temp)!
            ActivityIndicatorView.show("Loading....")
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else { return }
                let tmpURL = FileManager.default.temporaryDirectory
                    .appendingPathComponent(response?.suggestedFilename ?? "fileName.png")
                do {
                    try data.write(to: tmpURL)
                } catch {
                    print(error)
                    ActivityIndicatorView.hiding()
                }
                DispatchQueue.main.async {
                    /// STOP YOUR ACTIVITY INDICATOR HERE
                    ActivityIndicatorView.hiding()
                    print(tmpURL)
                    self.share(url: tmpURL)
                }
            }.resume()
            /* let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
             
             let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementDocPreviewViewController") as! AnnouncementDocPreviewViewController
             vc.UrlToLoad = temp
             self.present(vc, animated: true, completion: nil)*/
        }
        else if let type = data[index]["url"].string {
            if type.isEmpty{
                
            }
            else{
                print(type.contains("familheey"))
                if type.contains("familheey"){
                    self.openAppLink(url: type)
                }
                else{
                    //                    UIApplication.shared.open(url)
                    let url = URL(string: type)!
                    let vc = SFSafariViewController(url: url)
                    self.present(vc, animated: true, completion: nil)
                }
            }
            
        }
        else{
            let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsPreviewViewController") as! AnnouncementsPreviewViewController
            vc.attachments = data
            vc.isFromPost = true
            vc.selectedIndex = index
            self.navigationController?.addFadeAnimation()
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    //    {
    //
    //        let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
    //        let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsPreviewViewController") as! AnnouncementsPreviewViewController
    //        vc.attachments = data
    //        vc.isFromPost = true
    //        vc.selectedIndex = index
    //        self.navigationController?.addFadeAnimation()
    //        self.navigationController?.pushViewController(vc, animated: false)
    //    }
    
    func gotoSharedSinglePreview(data: String) {
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: "ChangePicturesViewController") as! ChangePicturesViewController
        vc.isCoverOrProfilePic = "CoverPic"
        vc.ids           = UserDefaults.standard.value(forKey: "userId") as! String
        vc.isFrom = "post"
        vc.inFor = "cover"
        vc.isAdmin = ""
        
        vc.imageUrl = data
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    
}
