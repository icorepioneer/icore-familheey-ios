//
//  UserProfileEditViewController.swift
//  familheey
//
//  Created by ANIL K on 12/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import CropViewController
import GooglePlaces

protocol userEditProfileDelegate:class {
    func EditDetails()
}

class UserProfileEditViewController: UIViewController, CropViewControllerDelegate, DatePickerPopupDelegate {
    var isNotification:Bool!
    var isSearchable:Bool!

    var userDetails : ExternalUserModel?
    var imagePicker = UIImagePickerController()
    var btnTag: Int = Int()
    var imgCover = UIImage()
    var imgLogo = UIImage()
    var imgCoverData = NSData()
    var imgLogoData = NSData()
    var originalPicData = NSData()
    var originalImageView = UIImageView()
    var originalName : String?
    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    var famName = ""
    weak var delegate:userEditProfileDelegate?
    var isFromDateEdit = false
    var selectedDob = Date()
    var selectedRow = String()
    var selectedLat : Double!
    var selectedLong :Double!
    var originLat : Double!
    var originLong : Double!
    
    @IBOutlet weak var tblUserProfile: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        tblUserProfile.dataSource = self
        tblUserProfile.delegate = self
        self.originalName = "\(String(describing: userDetails?.User?.fullname))"

        // Do any additional setup after loading the view.
    }
    override func viewWillDisappear(_ animated: Bool) {
        let full_name = userDetails?.User?.fullname
        if (full_name?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)!{
        var model = userDetails?.User
            model?.fullname = self.originalName ?? userDetails?.User?.fullname ?? ""
                     userDetails?.User = model
            
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        guard #available(iOS 13.0, *) else {
            
            if selectedRow == "living"{
                
                if appDel.selectedPlaceName1.count > 0{
                    
                    print("#######")
                    var temp = userDetails?.User
                    temp?.location = appDel.selectedPlaceName1
                    
                    userDetails?.User = temp
                    tblUserProfile.reloadRows(at: [IndexPath(row: 6, section: 0)], with: .none)
                    appDel.selectedPlaceName1 = ""
                    
                    
                }
            }else{
                
                if appDel.selectedPlaceName2.count > 0{
                    
                    var temp = userDetails?.User
                    
                    temp?.origin = appDel.selectedPlaceName2
                    userDetails?.User = temp
                    tblUserProfile.reloadRows(at: [IndexPath(row: 7, section: 0)], with: .none)
                    appDel.selectedPlaceName2 = ""
                }
            }
            return
        }
        
       /* if !UIDevice.current.systemVersion.contains("13"){
            
            if selectedRow == "living"{
                
                if appDel.selectedPlaceName1.count > 0{
                    
                    print("#######")
                    var temp = userDetails?.User
                    temp?.location = appDel.selectedPlaceName1
                    
                    userDetails?.User = temp
                    tblUserProfile.reloadRows(at: [IndexPath(row: 6, section: 0)], with: .none)
                    appDel.selectedPlaceName1 = ""
                    
                    
                }
            }else{
                
                if appDel.selectedPlaceName2.count > 0{
                    
                    var temp = userDetails?.User
                    
                    temp?.origin = appDel.selectedPlaceName2
                    userDetails?.User = temp
                    tblUserProfile.reloadRows(at: [IndexPath(row: 7, section: 0)], with: .none)
                    appDel.selectedPlaceName2 = ""
                }
            }
        }*/
    }
    
    //MARK:- Custom Methods
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else{
            // self.displayAlert(alertStr: "You don't have camera", title: "")
        }
    }
    
    func openGallary(){
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func selectedImage(img:UIImage){
        // let cropView = CropViewController(image: img)
        let cropView = CropViewController(croppingStyle: .default, image: img)
        cropView.aspectRatioLockEnabled = true
        cropView.aspectRatioLockDimensionSwapEnabled = true
        cropView.aspectRatioPickerButtonHidden = true
        if btnTag == 0{
            cropView.aspectRatioPreset = TOCropViewControllerAspectRatioPreset.presetSquare
        }
        else{
            cropView.aspectRatioPreset = TOCropViewControllerAspectRatioPreset.preset4x3
           // let originalPic = img
            self.originalPicData = img.jpegData(compressionQuality: 0.75) as! NSData
        }
        cropView.delegate = self
        self.navigationController?.pushViewController(cropView, animated: true)
    }
    
    //MARK:- CropView SDK Delegate
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        // 'image' is the newly cropped version of the original image
        
        if btnTag == 0{
            imgLogo = image
            
        }
        else{
            imgCover = image
        }
        tblUserProfile.reloadRows(at: [IndexPath(row: btnTag, section: 0)], with: .none)
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- DatePicker View Delegate
    func datePickerPopupDidSelectDate(_ dateString: String!) {
        
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let result = formatter.date(from: dateString)
        let tempD = formatter.string(from: result!)
        
        let formatter2 = DateFormatter()
        formatter2.dateStyle = .long
        formatter2.dateFormat = "MMM dd yyyy"

        if result != nil{
            let val = formatter2.string(from: result!)
            var temp = userDetails?.User
            temp?.dob = val
            print(val)
            print(tempD)
            
            userDetails?.User = temp
            tblUserProfile.reloadRows(at: [IndexPath(row: 4, section: 0)], with: .none)
        }
        
    }
    
    func datePickerPopupDidCancel() {
     
    }
    
    //Convert to date
    func convertSelectedDobtoDate(dateStr:String){
        let formatter2 = DateFormatter()
        formatter2.dateStyle = .long
        formatter2.dateFormat = "MMM dd yyyy"
        let val = formatter2.date(from: dateStr)
        print(val!)
    }

    //MARK:- Cell Actions
    
    @IBAction func genderChange(_ sender: UIButton){
        var gender = ""
        
        var temp = userDetails?.User
        
        if sender.tag == 1{
            gender = "male"
        }
        else if sender.tag == 2{
            gender = "female"
        }
        else {
            gender = ""
        }
        temp?.gender = gender
        
        userDetails?.User = temp
        tblUserProfile.reloadRows(at: [IndexPath(row: 5, section: 0)], with: .none)
    }
    
    @IBAction func onClickImagePick(_ sender: UIButton) {
        //let button = sender as! UIButton
        btnTag = sender.tag
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func onClickDob(_ sender: UIButton){
        
    }
    
    @IBAction func onClickLocations(_ sender: UIButton){

        if sender.tag == 6 {
            selectedRow = "living"
        }
        else{
            selectedRow = "origin"
        }
        
        if sender.tag == 4{
            isFromDateEdit = true
            let pickerPop = DatePickerPopup.getInstance
            pickerPop.delegate = self
            
            //note :- also need to change date min and max value in profile change DOB screen
            
            let cmaxal: Calendar = Calendar.current
            pickerPop.setMaximumDate(date: cmaxal.date(byAdding: .year, value: 0, to: Date())!)
            
            let cal: Calendar = Calendar.current
            pickerPop.setMinimumDate(date: cal.date(byAdding: .year, value: -150, to: Date())!)
            pickerPop.setCurrentDate(date: selectedDob)
            
            pickerPop.showInView(self.view)
        }
        else{
            
            guard #available(iOS 13.0, *) else {
                // Code for earlier iOS versions
                let story = UIStoryboard.init(name: "Main", bundle: nil)
                let SearchPlacesViewController = story.instantiateViewController(withIdentifier: "SearchPlacesViewController") as! SearchPlacesViewController
                SearchPlacesViewController.selectedField = selectedRow
                self.present(SearchPlacesViewController, animated: true, completion: nil)
                
                return
            }
            
            let acController = GMSAutocompleteViewController()
            
            acController.delegate = self
            present(acController, animated: true, completion: nil)
            
            /*if UIDevice.current.systemVersion.contains("13"){
                
                let acController = GMSAutocompleteViewController()
                acController.delegate = self
                present(acController, animated: true, completion: nil)
            }
            else{
                
                let story = UIStoryboard.init(name: "Main", bundle: nil)
                let SearchPlacesViewController = story.instantiateViewController(withIdentifier: "SearchPlacesViewController") as! SearchPlacesViewController
                SearchPlacesViewController.selectedField = selectedRow
                self.present(SearchPlacesViewController, animated: true, completion: nil)
            }*/
            
            
        }
        
        
    }
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickDone(_ sender: UIButton){
        self.view.endEditing(true)
        let id = UserDefaults.standard.value(forKey: "userId") as! String
        var param = [String:Any]()
        
        if let full_name = userDetails?.User?.fullname{
            if (full_name.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty){
                self.displayAlert(alertStr: "User name can't be empty", title: "")
                return
            }
            if !full_name.isValidUserName{
                self.displayAlert(alertStr: "Please enter a valid name, First letter of the name must be an alphabet also The name must contain at least two alphabets", title: "")
                return
                
            }
        }
        else{
            self.displayAlert(alertStr: "User name can't be empty", title: "")
            return
        }
     
        let tempDob = userDetails?.User?.dob
        if tempDob!.isEmpty{
            
            param = [
                "id":id,
                "full_name":userDetails?.User?.fullname,
                "email":userDetails?.User?.email,
                "gender":userDetails?.User?.gender,
                "location":userDetails?.User!.location as! String,
                "origin":userDetails?.User!.origin as! String,
                "notification": self.isNotification!,
                "searchable": self.isSearchable!
                ] as [String : Any]
        }
        else{
            param = [
                "id":id,
                "full_name":userDetails?.User?.fullname,
                "email":userDetails?.User?.email,
                "gender":userDetails?.User?.gender,
                "location":userDetails?.User!.location as! String,
                "origin":userDetails?.User!.origin as! String,
                "dob":userDetails?.User?.dob,
                "notification": self.isNotification!,
                "searchable": self.isSearchable!
                ] as [String : Any]
        }
        
        if self.selectedLong != nil && self.selectedLat != nil && self.originLong != nil && self.originLat != nil{
            param["lat"] = self.selectedLat
            param["long"] = self.selectedLong
            param["origin_lat"] = self.originLat
            param["origin_long"] = self.originLong
        }
        else{
            
        }
    
        print(param)
        APIServiceManager.callServer.EditUserProfile(url: EndPoint.profileEdit, param: param , success: { (responseMdl) in
            
            guard let responseMdl = responseMdl as? userModel else{
                return
            }
           // ActivityIndicatorView.hiding()
            print(responseMdl.photo)
            if responseMdl.status == 200{
                UserDefaults.standard.set(responseMdl.photo, forKey: "userProfile")
                self.appDelegate.isConversationUpdated = true
                self.delegate?.EditDetails()
                self.navigationController?.popViewController(animated: true)
            }
            
        }) { (error) in
            self.displayAlert(alertStr: error!.description, title: "")
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UserProfileEditViewController:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 11
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                if indexPath.row == 0{
        //            let cell = tableView.dequeueReusableCell(withIdentifier: "cellid", for: indexPath) as! UserProfileEditThreeTableViewCell
        //            cell.lblValue.isHidden = true
        //            cell.btnValue.tag = indexPath.row
        //
        //            if imgLogo.size.width != 0{
        //                cell.imgView.image = imgLogo
        //                self.imgLogoData = cell.imgView.image?.jpegData(compressionQuality: 0.5) as! NSData
        //
        //
        //
        //
        //            }
        //            else{
        //                if userDetails?.User?.propic.count != 0{
        //                    let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+(userDetails?.User!.propic)!
        //                    let imgUrl = URL(string: temp)
        //
        //                    cell.imgView.kf.setImage(with: imgUrl, placeholder: nil, options: nil, progressBlock: nil) { (image, error, cache, url) in
        //
        //                        if error == nil{
        //                            self.imgLogoData = cell.imgView.image?.jpegData(compressionQuality: 0.5) as! NSData
        //                        }
        //                    }
        //                }
        //                else{
        //                    cell.imgView.image = UIImage(named: "Male Colored")
        //                }
        //            }
        //            // imgLogoData = cell.imgView.image?.jpegData(compressionQuality: 0.5) as! NSData
        //
        //            cell.btnValue.addTarget(self, action: #selector(onClickImagePick(_:)), for: .touchUpInside)
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "headingCell", for: indexPath)
                    cell.textLabel!.text = "Basic Details"
        //            cell?.textLabel.text! = "Basic Settings"
                    return cell
                }
        if indexPath.row == 5{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! UserProfileEditTwoTableViewCell
            if let gender = userDetails?.User?.gender{
                if gender.lowercased() == "male"{
                    cell.imgMale.image = UIImage(named: "roundTick")
                    cell.imgFe.image = UIImage(named: "")
                    cell.imgNotSpecified.image = UIImage(named: "")
                    
                }
                else if gender.lowercased() == "female"{
                    cell.imgMale.image = UIImage(named: "")
                    cell.imgFe.image = UIImage(named: "roundTick")
                    cell.imgNotSpecified.image = UIImage(named: "")
                    
                }
                else{
                    cell.imgMale.image = UIImage(named: "")
                    cell.imgFe.image = UIImage(named: "")
                    cell.imgNotSpecified.image = UIImage(named: "roundTick")
                   
                }
            }
            cell.btnMale.tag = 1
            cell.btnFemale.tag = 2
            cell.btnNotSpecified.tag = 3
            cell.btnMale.addTarget(self, action: #selector(genderChange(_:)), for: .touchUpInside)
            cell.btnFemale.addTarget(self, action: #selector(genderChange(_:)), for: .touchUpInside)
            cell.btnNotSpecified.addTarget(self, action: #selector(genderChange(_:)), for: .touchUpInside)
            
            return cell
        }

//        else if indexPath.row == 5{
//            let cell = tableView.dequeueReusableCell(withIdentifier: "UserProfileEditFiveTableViewCell", for: indexPath) as! UserProfileEditFiveTableViewCell
//
//            cell.btnImageSelect.tag = indexPath.row
//
//            if imgCover.size.width != 0{
//                cell.imgView.image = imgCover
//                self.imgCoverData = cell.imgView.image?.jpegData(compressionQuality: 0.5) as! NSData
//            }
//            else{
//                if userDetails?.User?.coverPic.count != 0{
//                    let temp = "\(Helpers.imageURl)"+"\(BaseUrl.groupCover)"+(userDetails?.User!.coverPic)!
//                    let imgUrl = URL(string: temp)
//                    let tempOriginal = "\(Helpers.imageURl)"+"\(BaseUrl.groupCover)"+(userDetails?.User!.originalPic)!
//                    let imgOriginalUrl = URL(string: tempOriginal)
//
//                    self.originalImageView.kf.setImage(with: imgOriginalUrl, placeholder: nil, options: nil, progressBlock: nil) { (image, error, cache, url) in
//
//                        if error == nil{
//                            self.originalPicData = self.originalImageView.image?.jpegData(compressionQuality: 0.5) as! NSData
//                        }
//                    }
//
//                    cell.imgView.kf.setImage(with: imgUrl, placeholder: nil, options: nil, progressBlock: nil) { (image, error, cache, url) in
//
//                        if error == nil{
//                            self.imgCoverData = cell.imgView.image?.jpegData(compressionQuality: 0.5) as! NSData
//                        }
//                    }
//
//                }
//                else{
//                    cell.imgView.image = UIImage(named: "bgTopOnly")
//                   // imgCover = cell.imgView.image!
//                }
//            }
//
//
//            cell.btnImageSelect.addTarget(self, action: #selector(onClickImagePick(_:)), for: .touchUpInside)
//            return cell
//        }
       
            else if indexPath.row == 8{
                                 let cell = tableView.dequeueReusableCell(withIdentifier: "UserProfileEditSixTableViewCell", for: indexPath) as! UserProfileEditSixTableViewCell
                          cell.lblTitle.text = "Allow notifications"
            
            let temp = userDetails?.User
            self.isNotification = temp?.isNotification

            if self.isNotification
            {
                cell.switchbtn.isOn = true
            }
            else
            {
                cell.switchbtn.isOn = false
            }
                 
            cell.lblHeading.text = "Notification Settings"
                cell.switchbtn.tag = 0
                          return cell
                             }
                      else if indexPath.row == 9{
                                 let cell = tableView.dequeueReusableCell(withIdentifier: "UserProfileEditSixTableViewCell", for: indexPath) as! UserProfileEditSixTableViewCell
                          cell.lblTitle.text = "Make profile searchable"
            
            
             let temp = userDetails?.User
                        self.isSearchable = temp?.isSearchable

                        if self.isSearchable
                        {
                            cell.switchbtn.isOn = true

                        }
                        else
                        {
                            cell.switchbtn.isOn = false

                            
                        }
                             
            cell.lblHeading.text = "Privacy Settings"

                          cell.switchbtn.tag = 1

                                 return cell
                             }
            else if indexPath.row == 10{
                       let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! UserProfileEditFourTableViewCell
                       
                       return cell
                   }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellId", for: indexPath) as! UserProfileEditTableViewCell
            if indexPath.row == 1 {
                cell.lblHead.text = "Name"
                cell.txtValue.text = userDetails?.User?.fullname
                cell.btnAction.isHidden = true
                cell.txtValue.isEnabled = true
                cell.txtValue.delegate = self
            }
            else if indexPath.row == 2{
                cell.lblHead.text = "Phone"
                cell.txtValue.text = userDetails?.User?.phone
                cell.btnAction.isEnabled = false
                cell.btnAction.isHidden = false
                cell.txtValue.isEnabled = false
            }
            else if indexPath.row == 3{
                cell.lblHead.text = "Email"
                cell.txtValue.text = userDetails?.User?.email
                cell.btnAction.isEnabled = false
                cell.btnAction.isHidden = false
                cell.txtValue.isEnabled = false
            }
            else if indexPath.row == 4{
                cell.lblHead.text = "Date of birth"
                let tDate = userDetails?.User?.dob
                var DateFormatted = ""
                if isFromDateEdit{
                    DateFormatted = tDate!
                }
                else{
                    let tempDate = tDate!.components(separatedBy: "T")
                    
                    let dateAsString               = tDate
                    let dateFormatter              = DateFormatter()
                    dateFormatter.dateFormat       = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
                    
                    if dateAsString!.isEmpty{
                    }
                    else{
                      let date = dateFormatter.date(from: dateAsString!)
                        selectedDob = date!
                        // print(date as Any)
                        //  dateFormatter.dateFormat       = "dd MMM,h:mm a"
                        dateFormatter.dateFormat       = "MMM dd yyyy"
                        //  dateFormatter.dateStyle       = .short
                        //  dateFormatter.timeStyle       = .none
                        DateFormatted = dateFormatter.string(from: date!)
                        print(DateFormatted)
                    }
                }
        
                cell.txtValue.text = DateFormatted
                cell.btnAction.isEnabled = true
                cell.btnAction.isHidden = false
                cell.btnAction.tag = indexPath.row
                cell.btnAction.addTarget(self, action: #selector(onClickLocations(_:)), for: .touchUpInside)
            }
            else if indexPath.row == 6{
                
                cell.lblHead.text = "City you are residing in"
                cell.txtValue.text = userDetails?.User?.location
                cell.btnAction.isEnabled = true
                cell.btnAction.isHidden = false
                cell.btnAction.tag = indexPath.row
                cell.btnAction.addTarget(self, action: #selector(onClickLocations(_:)), for: .touchUpInside)
            }
            else if indexPath.row == 7{
                
                cell.lblHead.text = "City you are originally from"
                cell.txtValue.text = userDetails?.User?.origin
                cell.btnAction.isEnabled = true
                cell.btnAction.isHidden = false
                cell.btnAction.tag = indexPath.row
                cell.btnAction.addTarget(self, action: #selector(onClickLocations(_:)), for: .touchUpInside)
            }
          
            return cell
            
        }
        
        
    }
    
    
    
    @IBAction func switch_OnClick(_ sender: UISwitch) {
        if sender.tag == 0
        {
           if (sender.isOn == true){
            self.isNotification = true
            
               
           }
           else{
            self.isNotification = false
           }
            userDetails?.User?.isNotification = self.isNotification

        }
        else
        {
            if (sender.isOn == true){
                self.isSearchable = true
                          
                      }
                      else{
                self.isSearchable = false
                      }
            userDetails?.User?.isSearchable = self.isSearchable

        }
           
       }
      
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 5{
            return 105
        }
        else if indexPath.row == 0{
//            return 150
            return 30
        }
//        else if indexPath.row == 5{
//            return 160
//        }
        else if indexPath.row == 8 || indexPath.row == 9{
                return 80
            
            }
            
        else{
          return 110
        }
        
    }
}

extension  UserProfileEditViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker .dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let choosenImage = info[.originalImage] as! UIImage
        selectedImage(img: choosenImage)
        picker.dismiss(animated: true, completion: nil)
        
    }
}

extension UserProfileEditViewController : UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("......")
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        print("\(textField.text!)")
        self.famName = (textField.text)!
              var model = userDetails?.User
              model?.fullname = self.famName
              userDetails?.User = model
        if textField.text!.count > 0{
      
        }
        else{
            self.displayAlert(alertStr: "Name can't be empty", title: "")
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        //print(textField.text as! String)
        return true
    }
}

extension UserProfileEditViewController: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        // Get the place name from 'GMSAutocompleteViewController'
        // Then display the name in textField
        
        if selectedRow == "living"{
            print("#######")
            var temp = userDetails?.User
            temp?.location = "\(place.formattedAddress!)"
            self.selectedLong = place.coordinate.longitude
            self.selectedLat = place.coordinate.latitude
            self.originLat = self.selectedLat
            self.originLong = self.selectedLong
            
            userDetails?.User = temp
            tblUserProfile.reloadRows(at: [IndexPath(row: 6, section: 0)], with: .none)
            appDel.selectedPlaceName1 = ""
        }else{
            
            var temp = userDetails?.User
            temp?.origin = "\(place.formattedAddress!)"
            self.originLat = place.coordinate.latitude
            self.originLong = place.coordinate.longitude
            
            userDetails?.User = temp
            tblUserProfile.reloadRows(at: [IndexPath(row: 7, section: 0)], with: .none)
            appDel.selectedPlaceName2 = ""
        }
        
      
        print(place.coordinate.latitude)
        print(place.coordinate.longitude)
        
        dismiss(animated: true, completion: nil)
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // Handle the error
        print("Error: ", error.localizedDescription)
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        // Dismiss when the user canceled the action
        dismiss(animated: true, completion: nil)
    }
}
