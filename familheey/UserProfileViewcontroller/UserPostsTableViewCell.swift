//
//  UserPostsTableViewCell.swift
//  familheey
//
//  Created by Giri on 23/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class UserPostsTableViewCell: UITableViewCell {
    @IBOutlet weak var imageViewPost: UIImageView!
    @IBOutlet weak var imageViewuserProfilePic: UIImageView!
    @IBOutlet weak var lblUsername: UIStackView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblRightButtonTitle: UILabel!
    @IBOutlet weak var lblbottomBarButtontitle1: UILabel!
    @IBOutlet weak var lblBottomBarButtonTitle2: UILabel!
    @IBOutlet weak var lblBottomBarButtonTitle3: UILabel!
    @IBOutlet weak var imageviewBottomBar1: UIImageView!
    @IBOutlet weak var imageviewBottomBar2: UIImageView!
    @IBOutlet weak var imageviewBottomBar3: UIImageView!
    @IBOutlet weak var imageviewBottomBar4: UIImageView!
    @IBOutlet weak var imageviewBottomBar5: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
