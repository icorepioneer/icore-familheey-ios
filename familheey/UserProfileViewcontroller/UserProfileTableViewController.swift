//
//  UserProfileTableViewController.swift
//  familheey
//
//  Created by Giri on 23/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class UserProfileTableViewController: UITableViewController,UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, userEditProfileDelegate, EditAboutGroupDelegate, changeProPicDelegate {
    //   var hidePaymentHistory = false
    let loadingDefaultView = UIView()
    
    @IBOutlet weak var viewOfPaymentHistory: UIView!
    
    @IBOutlet weak var CollectionViewTitles: UICollectionView!
    @IBOutlet weak var lblNumberOfFamilies: UILabel!
    @IBOutlet weak var lblNumberOfConnections: UILabel!
    @IBOutlet weak var lblNumberOfContributions: UILabel!
    @IBOutlet weak var lblLivingIn: UILabel!
    @IBOutlet weak var lblOrigin: UILabel!
    @IBOutlet weak var lblJoinDate: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblPosition: UILabel!
    @IBOutlet weak var imageViewUserAvatar: UIImageView!
    @IBOutlet weak var btnProfileEdit: UIButton!
    @IBOutlet weak var btnUserNotification: UIButton!
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var imgEditCam: UIImageView!
    @IBOutlet weak var imgSett: UIImageView!
    @IBOutlet weak var lblAdd: UILabel!
    @IBOutlet weak var lblLeftBottomSelection: UILabel!
    @IBOutlet weak var lblRightBottomSelection: UILabel!
    @IBOutlet weak var vewMutalFamilies: UIView!
    @IBOutlet weak var rightView: UIView!
    @IBOutlet weak var topicStartView: UIView!
    
    var btnFloat = UIButton(type: .custom)
    var titleArray = [String]()
    var selectedIndex = 0
    var userID = ""
    var userDetails : ExternalUserModel?
    var mutualConnections = ""
    var mutialFamilies = ""
    var aboutString = ""
    var userOrigin = ""
    var userWork = ""
    
    var id:Int = Int()
    var isUser = ""
    var allInvitations = [invitations]()
    var isFromNotification = false
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var frntView: UIView!
    
    @IBOutlet weak var editCoverButt                   : UIButton!
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadingDefaultView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height:  self.view.frame.size.height)
        self.loadingDefaultView.backgroundColor = .white
        
        self.frntView.clipsToBounds = true
        self.frntView.layer.cornerRadius = 20
        self.frntView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        self.CollectionViewTitles.clipsToBounds = true
        self.CollectionViewTitles.layer.cornerRadius = 20
        self.CollectionViewTitles.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        CollectionViewTitles.delegate = self
        CollectionViewTitles.dataSource = self
        

        imgEditCam.isHidden = true
        //        if self.hidePaymentHistory
        //        {
        //            self.viewOfPaymentHistory.isHidden = true
        //        }
        //        else
        //        {
        //            self.viewOfPaymentHistory.isHidden = false
        //
        //        }
        
        self.tabBarController?.tabBar.isHidden = true
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //Show the navigation bar
        // self.navigationController?.setNavigationBarHidden(false, animated: animated)
        btnFloat.isHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Hide the navigation bar
        if !userID.isEmpty{
            getUserDetails(UId: userID)
        }
        
        self.tabBarController?.tabBar.isHidden = true
        
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    //MARK:- Custom floating btn
    
    func setFloatingButton(){
        btnFloat.frame = CGRect(x: UIScreen.main.bounds.size.width - 60, y: UIScreen.main.bounds.size.height - 90, width: 40, height: 40)
        //        if ActiveTab == 0{//Members
        //            //btnFloat.setTitle("+", for: .normal)
        //
        //
        //            //  btnFloat.titleLabel?.font = UIFont.boldSystemFont(ofSize: 28)
        //        }
        //        else if ActiveTab == 1{//Requests
        //            // btnFloat.setTitle("=", for: .normal)
        //            btnFloat.isHidden = true
        //        }
        //        else{//Contributions
        //            // btnFloat.setTitle("..", for: .normal)
        //            btnFloat.isHidden = true
        //        }
        btnFloat.isHidden = false
        btnFloat.setImage(UIImage(named: "chat_white"), for: .normal)
        
        btnFloat.contentVerticalAlignment = .fill
        btnFloat.contentHorizontalAlignment = .fill
        btnFloat.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
       
        btnFloat.setTitleColor(UIColor.white, for: .normal)
        btnFloat.backgroundColor = UIColor(named: "purpleBackground")
        btnFloat.clipsToBounds = true
        btnFloat.layer.cornerRadius = 20
//        btnFloat.imageView?.contentMode = .scaleAspectFit
        btnFloat.addTarget(self,action: #selector(onclickFloatingAction), for: UIControl.Event.touchUpInside)
        if let window = UIApplication.shared.keyWindow {
            window.addSubview(btnFloat)
        }
    }
    
    //MARK: - Button Actions
    
    @objc func onclickFloatingAction(){
        var userArr = [String]()
        userArr.append(self.userID)
        
        let storyboard = UIStoryboard.init(name: "Topic", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CreateTopicViewController") as! CreateTopicViewController
        vc.isFromCreate = false
        vc.isFromProfile  = true
        vc.toUsers = userArr
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickConnectionsActions(_ sender: Any) {
        //        userDetails?.count?.connections
        if (userDetails?.count!.connections)! == 0 {
            
            //        if (userDetails?.count!.mutualConnections)! == 0 {
            
        }
        else{
            let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "UserFamilyListViewController") as! UserFamilyListViewController
            
            vc.userID = "\(userDetails?.User?.id ?? 0)"
            vc.isFamilies = false
            
            let userId = UserDefaults.standard.value(forKey: "userId") as! String
            id = (userDetails?.User!.id)!
            if Int(userId) == id{
                vc.isConnection = true
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func onClickFamiliesAction(_ sender: Any) {
        if (userDetails?.count!.familyCount)! == 0{
            
        }
        else{
            let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "UserFamilyListViewController") as! UserFamilyListViewController
            vc.userID = "\(userDetails?.User?.id ?? 0)"
            vc.isFamilies = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    @IBAction func onClickBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickSettingsAction(_ sender: Any) {
        let userId = UserDefaults.standard.value(forKey: "userId") as! String
        if Int(userId) == id{
            //            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            //            let vc = storyboard.instantiateViewController(withIdentifier: "viewRequestsTableViewController") as! viewRequestsTableViewController
            //            self.navigationController?.pushViewController(vc, animated: true)
            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "UserProfileEditViewController") as! UserProfileEditViewController
            vc.userDetails = userDetails
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            let popOverVc = storyboard.instantiateViewController(withIdentifier: "SelectFamilyViewController") as! SelectFamilyViewController
            popOverVc.selectedUserID = "\(id)"
            self.navigationController?.pushViewController(popOverVc, animated: true)
            //            popOverVc.modalTransitionStyle = .crossDissolve
            //            popOverVc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            //            self.present(popOverVc, animated: true)
        }
        
    }
    
    @IBAction func onClickCoverPic(_ sender: Any) {
        
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: "ChangePicturesViewController") as! ChangePicturesViewController
        // vc.isFrom = "Profile"
        vc.isCoverOrProfilePic = "CoverPic"
        vc.ids           = UserDefaults.standard.value(forKey: "userId") as! String
        vc.isFrom = "user"
        vc.inFor = "cover"
        vc.isAdmin = isUser
        vc.delegate = self
        
        if userDetails?.User!.originalPic.count != 0{
            let temp = "\(Helpers.imageURl)"+"\(BaseUrl.user_original)"+(userDetails?.User!.originalPic)!
            
            vc.imageUrl   = temp
        }
        else{
            if userDetails?.User!.coverPic.count != 0{
                let temp = "\(Helpers.imageURl)"+"\(BaseUrl.groupCover)"+(userDetails?.User!.coverPic)!
                
                vc.imageUrl   = temp
                
            }else{
                vc.imageUrl   = ""
            }
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func onCliCKpaymentHistory(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "PaymentHistory", bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: "PaymentHistoryMainViewController") as! PaymentHistoryMainViewController
        appDel.paymentHistoryFromFamily = false
        appDel.memberIdForPaymentHistory = 0
        appDel.groupIdForPaymentHistory = ""
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func onClickProfileEdit(_ sender: Any) {
        
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: "ChangePicturesViewController") as! ChangePicturesViewController
        //vc.isFrom = "Profile"
        vc.isCoverOrProfilePic = "ProfilePic"
        vc.ids           = UserDefaults.standard.value(forKey: "userId") as! String
        vc.isFrom = "user"
        vc.inFor = "profile"
        vc.isAdmin = isUser
        vc.delegate = self
        
        if userDetails?.User?.propic.count != 0 {
            // print("\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+(userDetails?.User!.propic)!)
            
            vc.imageUrl      = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+(userDetails?.User!.propic)!
        }else
        {
            vc.imageUrl      = ""
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
        
        //        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        //        let vc = storyboard.instantiateViewController(withIdentifier: "UserProfileEditViewController") as! UserProfileEditViewController
        //        vc.userDetails = userDetails
        //        vc.delegate = self
        //        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickEditIntro(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let addMember = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsEditAboutViewController") as! FamilyDetailsEditAboutViewController
        addMember.delegate = self
        addMember.aboutString = aboutString
        addMember.isFrom = "user"
        addMember.workedit = "introduction"
        addMember.groupId = UserDefaults.standard.value(forKey: "userId") as! String
        self.navigationController?.pushViewController(addMember, animated: true)
    }
    
    @IBAction func onClickEditWork(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let addMember = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsEditAboutViewController") as! FamilyDetailsEditAboutViewController
        addMember.delegate = self
        addMember.workString = userWork
        addMember.isFrom = "user"
        addMember.workedit = "work"
        
        addMember.groupId = UserDefaults.standard.value(forKey: "userId") as! String
        self.navigationController?.pushViewController(addMember, animated: true)
    }
    
    
    
    
    @IBAction func onClickAcceptAction(_ sender: UIButton) {
        print(sender.tag)
        let invitation = allInvitations[sender.tag]
        
        if !Connectivity.isConnectedToInternet(){
            self.displayAlertChoice(alertStr: "No Internet! Please Check your connectivity", title: "") { (result) in
                //                self.getFamilyDetails(groupId: self.groupId)
            }
        }
        
        APIServiceManager.callServer.invitationResponse(url: EndPoint.invitationResp, tableId: "\(invitation.req_id)", user_id: UserDefaults.standard.value(forKey: "userId") as! String, group_id: "\(invitation.group_id)", status: "accepted", fromId: "\(invitation.from_id)", success: { (response) in
            //            ActivityIndicatorView.hiding()
            
            if let results = response as! requestSuccessModel?{
                if results.status_code == 200{
                    self.getAllInvitations()
                }
            }
            
        }) { (error) in
            ActivityIndicatorView.hiding()
            
        }
        
    }
    @IBAction func onClickRejectAction(_ sender: UIButton) {
        print(sender.tag)
        let invitation = allInvitations[sender.tag]
        
        if !Connectivity.isConnectedToInternet(){
            self.displayAlertChoice(alertStr: "No Internet! Please Check your connectivity", title: "") { (result) in
                //                self.getFamilyDetails(groupId: self.groupId)
            }
        }
        
        APIServiceManager.callServer.invitationResponse(url: EndPoint.invitationResp, tableId: "\(invitation.req_id)", user_id: UserDefaults.standard.value(forKey: "userId") as! String, group_id: "\(invitation.group_id)", status: "rejected", fromId: "\(invitation.from_id)", success: { (response) in
            //            ActivityIndicatorView.hiding()
            
            if let results = response as! requestSuccessModel?{
                if results.status_code == 200{
                    self.getAllInvitations()
                }
            }
            
        }) { (error) in
            ActivityIndicatorView.hiding()
            
        }
        
    }
    @IBAction func onClickMutalFamilies(_ sender: Any) {
        if (userDetails?.count!.mutalFamilies)! == 0{
            
        }
        else{
            let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "UserFamilyListViewController") as! UserFamilyListViewController
            
            vc.userID = "\(userDetails?.User?.id ?? 0)"
            vc.isFamilies = true
            vc.isFromProfile = true
            let userId = UserDefaults.standard.value(forKey: "userId") as! String
            id = (userDetails?.User!.id)!
            //        if Int(userId) == id{
            //            vc.isConnection = true
            //        }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func onClickStartChat(_ sender: Any) {
        var userArr = [String]()
        userArr.append(self.userID)
        
        let storyboard = UIStoryboard.init(name: "Topic", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CreateTopicViewController") as! CreateTopicViewController
        vc.isFromCreate = false
        vc.isFromProfile  = true
        vc.toUsers = userArr
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Custom Functions
    
    func getAllInvitations(){
        ActivityIndicatorView.show("Loading....")
        if !Connectivity.isConnectedToInternet(){
            self.displayAlertChoice(alertStr: "No Internet! Please Check your connectivity", title: "") { (result) in
                self.getAllInvitations()
            }
        }
        APIServiceManager.callServer.getAllinvitations(url: EndPoint.listallInvitation, user_id: UserDefaults.standard.value(forKey: "userId") as! String, success: { (response) in
            ActivityIndicatorView.hiding()
            if let results = response as! InvitationsModel?{
                self.allInvitations = results.result!
                self.tableView.reloadData()
            }
            
        }, failure: { (error) in
            ActivityIndicatorView.hiding()
        })
    }
    
    func getUserDetails(UId:String){
        if !Connectivity.isConnectedToInternet(){
            self.displayAlertChoice(alertStr: "No Internet! Please Check your connectivity", title: "") { (result) in
                self.getUserDetails(UId: self.userID)
            }
        }
        
        self.loadingDefaultView.showDefaultAnimation(target:self)
        let crntUser = UserDefaults.standard.value(forKey: "userId") as! String
        ActivityIndicatorView.show("Please wait!")
        APIServiceManager.callServer.getUserDetails(url: EndPoint.userDetails, userId: crntUser, profileId: UId ,success: { (response) in
            
            ActivityIndicatorView.hiding()
            
            
            guard let loginMdl = response as? ExternalUserModel else{
                return
            }
            print(loginMdl)
            if loginMdl.status_code == 200{
                self.loadingDefaultView.removeFromSuperview()
                
                self.rightView.isHidden = false
                self.userDetails = loginMdl
                self.updateUserDetails()
                self.tableView.reloadData()
                self.CollectionViewTitles.reloadData()
                if self.isFromNotification{
                    self.isFromNotification = false
                    self.selectedIndex = 4
                    //                    self.CollectionViewTitles.reloadData()
                    self.getAllInvitations()
                }
            }
            
        }) { (error) in
            self.loadingDefaultView.removeFromSuperview()
            
            ActivityIndicatorView.hiding()
            
            //            let alert = UIAlertController(title: "error", message: "something wrong.", preferredStyle: .alert)
            //
            //
            //
            //
            //                          alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            //                              self.loadingDefaultView.hideDefaultAnimation(target:self)
            //
            //                              self.navigationController?.popViewController(animated: true)
            //
            //                          }))
            //
            //                          self.present(alert, animated: true)
            self.navigationController?.popViewController(animated: true)
            self.tableView.reloadData()
        }
        
    }
    func updateUserDetails(){
        let userId = UserDefaults.standard.value(forKey: "userId") as! String
        id = (userDetails?.User!.id)!
        if Int(userId) == id{
            btnProfileEdit.isEnabled = true
            btnUserNotification.isEnabled = true
            imgSett.isHidden = false
            lblAdd.isHidden = true
            imgEditCam.isHidden = false
            vewMutalFamilies.isHidden = true
            topicStartView.isHidden = true
            isUser = "admin"
            UserDefaults.standard.set(userDetails?.User?.propic, forKey: "userProfile")
            UserDefaults.standard.set(userDetails?.User?.fullname, forKey: "user_fullname")
            self.viewOfPaymentHistory.isHidden = false
            
            lblRightBottomSelection.text = "Connections"
            if (userDetails?.count!.connections)! > 0 {
                let count = userDetails?.count?.connections
                lblNumberOfConnections.text = "\(count!)"
            }
            else{
                lblNumberOfConnections.text = "0"
            }
            titleArray = ["ABOUT ME","MY POSTS","MY REQUEST","ALBUMS","INVITES"]
            self.CollectionViewTitles.reloadData()
        }
        else{
            topicStartView.isHidden = true
            setFloatingButton()
            
            self.viewOfPaymentHistory.isHidden = true
            lblRightBottomSelection.text = "Mutual Connections"
            if (userDetails?.count!.mutualConnections)! > 0 {
                let count = userDetails?.count?.mutualConnections
                // print(count)
                lblNumberOfConnections.text = "\(count!)"
            }
            else{
                lblNumberOfConnections.text = "0"
            }
            
            if (userDetails?.count!.mutalFamilies)! > 0{
                let count = userDetails?.count?.mutalFamilies
                lblNumberOfContributions.text = "\(count!)"
            }
            else{
                lblNumberOfContributions.text = "0"
            }
            
            btnProfileEdit.isEnabled = true
            btnUserNotification.isEnabled = true
            imgSett.isHidden = true
            if appDelegate.noFamily{
                lblAdd.isHidden = true
                btnUserNotification.isEnabled = false
            }
            else{
                lblAdd.isHidden = false
            }
            imgEditCam.isHidden = true
            titleArray =  ["ABOUT ME","ALBUMS"]
            self.CollectionViewTitles.reloadData()
        }
        
        
        
        if let name = userDetails?.User?.fullname{
            lblUserName.text = name
        }
        if let livingIn = userDetails?.User?.location{
            lblLivingIn.text = livingIn
        }
        if let origin = userDetails?.User?.origin{
            lblOrigin.text = origin
        }
        
        lblJoinDate.text = formatDateString(dateString: userDetails?.User?.createdAt ?? "")
        
        if (userDetails?.count!.familyCount)! > 0{
            let count = userDetails?.count?.familyCount
            lblNumberOfFamilies.text = "\(count!)"
        }
        else
        {
            lblNumberOfFamilies.text = "0"
        }
        
        /* if (userDetails?.count!.connections)! > 0 {
         let count = userDetails?.count?.connections
         lblNumberOfConnections.text = "\(count!)"
         }
         else{
         lblNumberOfConnections.text = "0"
         }*/
        
        if userDetails?.User?.propic.count != 0 {
            let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+(userDetails?.User!.propic)!
            let imgUrl = URL(string: temp)
            
            let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+imgUrl!.relativeString
            let urlImg = URL(string: newUrlStr)
            imageViewUserAvatar.kf.indicatorType = .activity
            //            imageViewUserAvatar.kf.setImage(with: urlImg, placeholder: #imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
            imageViewUserAvatar.kf.setImage(with: urlImg, placeholder: #imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
            
        }
        else{
            imageViewUserAvatar.image = #imageLiteral(resourceName: "Male Colored")
        }
        
        if userDetails?.User!.coverPic.count != 0{
            let temp = "\(Helpers.imageURl)"+"\(BaseUrl.groupCover)"+(userDetails?.User!.coverPic)!
            
            let imgUrl = URL(string: temp)
            let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=1600&height=1200&url="+imgUrl!.relativeString
            let urlImg = URL(string: newUrlStr)
            imgCover.kf.indicatorType = .activity
            //            imgCover.kf.setImage(with: urlImg, placeholder: #imageLiteral(resourceName: "profileDefault"), options: nil, progressBlock: nil, completionHandler: nil)
            imgCover.kf.setImage(with: urlImg, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
            
            
        }else{
            imgCover.image = #imageLiteral(resourceName: "user_profile_default__cover_image")
        }
        
        if userDetails?.User?.location.count != 0{
            lblPosition.text = userDetails?.User?.location
        }
        //        lblNumberOfContributions.text = "0"
        
        self.aboutString = userDetails?.User?.about ?? ""
        if self.aboutString == "null"{
            self.aboutString = ""
        }
        self.userOrigin = userDetails?.User?.origin ?? ""
        self.userWork = userDetails?.User?.work ?? ""
        
        if selectedIndex == 0{
            self.updateUserAbout(data: self.aboutString, origin: self.userOrigin ,work:self.userWork)
        }
        
    }
    func formatDateString(dateString:String) -> String{
        if dateString.isEmpty{
            return "Nill"
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        if let dateobj = dateFormatter.date(from: dateString) {
            dateFormatter.dateFormat = "MMM dd yyyy"
            return dateFormatter.string(from: dateobj)
        }
        else{
            return "Nill"
        }
    }
    
    func updateUserAbout(data: String, origin:String , work:String) {
        self.aboutString = data
        self.userOrigin = origin
        self.userWork = work
        //        tableView.reloadData()
        selectedIndex = 0
        tableView.beginUpdates()
        tableView.reloadRows(at: [IndexPath.init(row: 0, section: 0)], with: .none)
        tableView.endUpdates()
    }
    
    func updateAbout(data: String,workedit:Bool) {
        print(data)
        if !workedit
        {
            self.aboutString = data
            updateUserAbout(data: aboutString, origin: userOrigin ,work: self.userWork)
        }
        else
        {
            self.userWork = data
            updateUserAbout(data: aboutString, origin: userOrigin ,work: self.userWork)
        }
    }
    
    //MARK:- Custom delegate
    func EditDetails() {
        getUserDetails(UId: userID)
    }
    
    func changeProPic() {
        getUserDetails(UId: userID)
    }
    //MARK:- collectionView Delegates
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return titleArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchTitlesCollectionViewCell", for: indexPath as IndexPath) as! SearchTitlesCollectionViewCell
        cell.lblTitle.text = titleArray[indexPath.item]
        if indexPath.item == selectedIndex{
            cell.viewSelection.isHidden = false
            cell.viewSelection.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            cell.lblTitle.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            cell.lblTitle.font = UIFont.boldSystemFont(ofSize: 15)
            tableView.reloadData()
        }
        else{
            cell.viewSelection.isHidden = true
            cell.lblTitle.textColor = .lightGray
            cell.lblTitle.font = UIFont.systemFont(ofSize: 15)
            //cell.viewSelection.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            tableView.reloadData()
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item == 4{
            selectedIndex = indexPath.item
            
            let userId = UserDefaults.standard.value(forKey: "userId") as! String
            if Int(userId) == id{
                getAllInvitations()
            }
            collectionView.reloadData()
        }
        else if indexPath.item == 1{
            
            let userId = UserDefaults.standard.value(forKey: "userId") as! String
            id = (userDetails?.User!.id)!
            if Int(userId) == id{
                let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "PostsInUserProfileViewController") as! PostsInUserProfileViewController
                
                //            vc.userDetails = userDetails
                //            vc.delegate = self
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else{
                let storyboard = UIStoryboard.init(name: "CreateFamily", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "userAlbumsListViewController") as! userAlbumsListViewController
                //            vc.userDetails = userDetails
                //            vc.delegate = self
                vc.userId = self.userID
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
        }
        else if indexPath.item == 2{
            let userId = UserDefaults.standard.value(forKey: "userId") as! String
            id = (userDetails?.User!.id)!
            if Int(userId) == id{
                let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "RequestInUserProfileViewController") as! RequestInUserProfileViewController
                
                //            vc.userDetails = userDetails
                //            vc.delegate = self
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else if indexPath.item == 3{
            let storyboard = UIStoryboard.init(name: "CreateFamily", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "userAlbumsListViewController") as! userAlbumsListViewController
            //            vc.userDetails = userDetails
            //            vc.delegate = self
            vc.userId = self.userID
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            selectedIndex = indexPath.item
            
            tableView.reloadData()
            collectionView.reloadData()
            //            getUserDetails(UId: userID)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //        let numberOfCellInRow : Int = self.titleArray.count
        //        let padding : CGFloat = 0.1
        //        let collectionCellWidth : CGFloat = (self.CollectionViewTitles.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
        //        return CGSize(width: collectionCellWidth , height: 60)
        var w = titleArray[indexPath.row].size(withAttributes: nil)
        w.width = w.width + 25
        return CGSize(width: w.width, height: 60)
    }
    
    
    //MARK: - Tableview Delegates
    
    /* override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
     switch(section) {
     case 0:return "Today"
     case 1:return "YesterDay"
     default :return ""
     }
     }
     override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
     
     let title = self.tableView(tableView, titleForHeaderInSection: section)
     if (title == "") {
     return 0.0
     }
     return 20.0
     }*/
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.selectedIndex == 0{
            return 1
        }
        else if self.selectedIndex == 4{
            return allInvitations.count
        }
        else{
            return 0
        }
        
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if selectedIndex == 0{
            tableView.backgroundColor = .white
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserAboutMeTableViewCell", for: indexPath) as! UserAboutMeTableViewCell
            cell.selectionStyle = .none
            cell.txtAboutMe.text = aboutString
            //            cell.lblOrigin.text = userOrigin
            cell.lblOrigin.text = userWork
            
            
            if isUser == "admin"{
                cell.imgEdit.isHidden = false
                cell.btnEdit.isEnabled = true
                cell.imgWorkEdit.isHidden = false
                cell.btnWorkEdit.isEnabled = true
                
            }
            else{
                cell.imgEdit.isHidden = true
                cell.btnEdit.isEnabled = false
                cell.imgWorkEdit.isHidden = true
                cell.btnWorkEdit.isEnabled = false
            }
            
            return cell
        }
        else if selectedIndex == 4{
            
            if allInvitations.count == 0{
                return UITableViewCell.init()
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserRequestTableViewCell", for: indexPath) as! UserRequestTableViewCell
            
            tableView.backgroundColor = #colorLiteral(red: 0.9568627451, green: 0.9450980392, blue: 0.9764705882, alpha: 1)
            
            let invitation = allInvitations[indexPath.row]
            
            cell.lblName.text = invitation.full_name
            // cell.lblRequs.text = invitation.location
            cell.lblDesign.text = "Invited you to his family \"\(invitation.group_name)\""
            
            if invitation.logo.count != 0{
                let url = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+invitation.logo
                let imgUrl = URL(string: url)
                let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=1600&height=1200&url="+imgUrl!.relativeString
                let urlImg = URL(string: newUrlStr)
                cell.imgProfile.kf.indicatorType = .activity
                cell.imgProfile.kf.setImage(with: urlImg, placeholder:#imageLiteral(resourceName: "Family Logo") , options: nil, progressBlock: nil, completionHandler: nil)
            }
            else{
                cell.imgProfile.image = #imageLiteral(resourceName: "Male Colored")
                
            }
            
            cell.btnAccept.tag = indexPath.row
            cell.btnReject.tag = indexPath.row
            return cell
        }
        else{
            return UITableViewCell()
        }
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if self.selectedIndex == 0{
            return UIView()
        }
        else{
            if allInvitations.count == 0{
                let headerView = UIView.init(frame: CGRect.init(x: 50, y: 10, width: tableView.frame.width-40, height: 40))
                headerView.backgroundColor = .clear
                let label = UILabel()
                label.frame = CGRect.init(x: 5, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
                label.textColor = .darkGray
                label.text = "No pending request"
                headerView.addSubview(label)
                return headerView
            }
            else{
                return UIView()
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        if self.selectedIndex == 0{
            return 0
        }
        else{
            if allInvitations.count == 0{
                return 100
            }
            return 0
        }
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectedIndex == 0{
            return UITableView.automaticDimension
        }
        else{
            return 100
        }
    }
    
}
