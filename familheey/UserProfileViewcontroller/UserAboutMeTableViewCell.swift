
//
//  UserAboutMeTableViewCell.swift
//  familheey
//
//  Created by familheey on 16/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class UserAboutMeTableViewCell: UITableViewCell {

    @IBOutlet weak var txtAboutMe: UILabel!
    @IBOutlet weak var lblOrigin: UILabel!
    @IBOutlet weak var imgEdit: UIImageView!
    @IBOutlet weak var btnEdit: UIButton!
    
    
    @IBOutlet weak var imgWorkEdit: UIImageView!
    @IBOutlet weak var btnWorkEdit: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
