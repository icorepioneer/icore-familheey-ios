//
//  UserFamilyListViewController.swift
//  familheey
//
//  Created by familheey on 11/11/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON
import Kingfisher

class UserFamilyListViewController: UIViewController {
    
    @IBOutlet weak var lblTittle: UILabel!
    @IBOutlet weak var tableViewMembers: UITableView!
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    
    var userID = ""
    var familyID = ""
    var isFamilies = false
    var isConnection = false
    var isFromProfile = false
    var eventID = ""
    var arrayUsers = [JSON]()
    var arrayFamiles = [GroupDetail]()
    var arraySharedUsers = [JSON]()
    var arrayMutualUsers = [JSON]()
    var secondUserId = ""
    var fromFamilies = false
    
    var isFrompost = false
    var postId = ""
    var isFrom = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if isFamilies{
            lblTittle.text = "Families"
            if isFromProfile{
                getMutalFamilies()
            }
            else{
                getFamiles()
            }
        }
        else if !eventID.isEmpty{
            lblTittle.text = "Shared By"
            getSharedMembers()
        }
        else if isFrompost{
            if isFrom.lowercased() == "postview"{
                lblTittle.text = "Viewed By"
                getViewdUsers()
            }
            else if isFrom.lowercased() == "usershare"{
                lblTittle.text = "Shared By"
                //                getSharedByUsers()
                commonSharedList()
            }
            else{
                lblTittle.text = "Shared By"
                getSharedInGroup()
            }
        }
        else{
            lblTittle.text = "Mutual Connections"
            if fromFamilies{
                lblTittle.text = "Mutual Connections"
                getMutualMembers()
            }
            else{
                if isConnection{
                    lblTittle.text = "My Connections"
                    getConnections()
                }
                else{
                    lblTittle.text = "Mutual Connections"
                    getMembers()
                }
            }
        }
    }
    
    //MARK:- Get members
    func getMembers(){
        
        let parameter = ["user_one_id":UserDefaults.standard.value(forKey: "userId") as! String,"user_two_id":userID] as [String : Any]
        print(parameter)
        
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.getMutualConnections(parameter: parameter)) {(result) in
            switch result {
            case .success(let response):
                do {
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    
                    if response.statusCode == 200{
                        
                        self.arrayUsers = jsonData["data"].arrayValue
                        self.tableViewMembers.delegate = self
                        self.tableViewMembers.dataSource = self
                        self.tableViewMembers.reloadData()
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getMembers()
                        }
                    }
                    else{
                        
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                    
                } catch let err {
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        }
    }
    
    func getConnections(){
        let parameter = ["user_id":UserDefaults.standard.value(forKey: "userId") as! String] as [String : Any]
        print(parameter)
        
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.getConnections(parameter: parameter)) {(result) in
            switch result {
            case .success(let response):
                do {
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    
                    if response.statusCode == 200{
                        
                        self.arrayUsers = jsonData.arrayValue
                        self.tableViewMembers.delegate = self
                        self.tableViewMembers.dataSource = self
                        self.tableViewMembers.reloadData()
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getConnections()
                        }
                    }
                    else{
                        
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                    
                } catch let err {
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        }
    }
    
    func getViewdUsers(){
        let parameter = ["post_id": self.postId,"user_id":UserDefaults.standard.value(forKey: "userId")]
        print(parameter)
        
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.viewedUsers(parameter: parameter )) {(result) in
            switch result {
            case .success(let response):
                do {
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    
                    if response.statusCode == 200{
                        
                        self.arrayUsers = jsonData["data"].arrayValue
                        self.tableViewMembers.delegate = self
                        self.tableViewMembers.dataSource = self
                        self.tableViewMembers.reloadData()
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getViewdUsers()
                        }
                    }
                    else{
                        
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                    
                } catch let err {
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        }
    }
    
    func getSharedByUsers(){
        let parameter = ["post_id": self.postId, "user_id":UserDefaults.standard.value(forKey: "userId") as! String]
        print(parameter)
        
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.sharedUsers(parameter: parameter)) {(result) in
            switch result {
            case .success(let response):
                do {
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    
                    if response.statusCode == 200{
                        
                        self.arrayUsers = jsonData.arrayValue
                        self.tableViewMembers.delegate = self
                        self.tableViewMembers.dataSource = self
                        self.tableViewMembers.reloadData()
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getSharedByUsers()
                        }
                    }
                    else{
                        
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                    
                } catch let err {
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        }
    }
    func commonSharedList(){
        
        let parameter = ["origin_post_id": self.postId, "user_id":UserDefaults.standard.value(forKey: "userId") as! String]
        print(parameter)
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.getCommonSharedUserList(parameter: parameter)) {(result) in
            switch result {
            case .success(let response):
                do {
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    
                    if response.statusCode == 200{
                        
                        self.arrayUsers = jsonData.arrayValue
                        self.tableViewMembers.delegate = self
                        self.tableViewMembers.dataSource = self
                        self.tableViewMembers.reloadData()
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.commonSharedList()
                        }
                    }
                    else{
                        
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                    
                } catch let err {
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        }
    }
    func getSharedInGroup(){
        let parameter = ["post_id": self.postId, "group_id":self.familyID]
        print(parameter)
        
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.sharedInGroups(parameter: parameter)) {(result) in
            switch result {
            case .success(let response):
                do {
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    
                    if response.statusCode == 200{
                        
                        self.arrayUsers = jsonData.arrayValue
                        self.tableViewMembers.delegate = self
                        self.tableViewMembers.dataSource = self
                        self.tableViewMembers.reloadData()
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getSharedInGroup()
                        }
                    }
                    else{
                        
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                    
                } catch let err {
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        }
    }
    
    func getSharedMembers(){
        
        let parameter = ["user_id":UserDefaults.standard.value(forKey: "userId") as! String,"event_id":eventID] as [String : Any]
        print(parameter)
        
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.getEventShareList(parameter: parameter)) {(result) in
            switch result {
            case .success(let response):
                do {
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    
                    if response.statusCode == 200{
                        
                        self.arraySharedUsers = jsonData["data"].arrayValue
                        self.tableViewMembers.delegate = self
                        self.tableViewMembers.dataSource = self
                        self.tableViewMembers.reloadData()
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getSharedMembers()
                        }
                    }
                    else{
                        
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                    
                } catch let err {
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        }
        
        
    }
    
    func getMutualMembers(){
        let parameter = ["user_id":UserDefaults.standard.value(forKey: "userId") as! String,"group_id":familyID] as [String : Any]
        print(parameter)
        
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.getMutualConnectionsList(parameter: parameter)) {(result) in
            switch result {
            case .success(let response):
                do {
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    
                    if response.statusCode == 200{
                        
                        self.arrayMutualUsers = jsonData["data"].arrayValue
                        self.tableViewMembers.delegate = self
                        self.tableViewMembers.dataSource = self
                        self.tableViewMembers.reloadData()
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getMutualMembers()
                        }
                    }
                    else{
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                    
                } catch let err {
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        }
    }
    
    //MARK:- Get Families
    func getFamiles(){
        ActivityIndicatorView.show("Please wait!")
        
        APIServiceManager.callServer.getGroupsToAddMember(url: EndPoint.GroupsToAddMember, memberId: UserDefaults.standard.value(forKey: "userId") as! String, userId: userID, searchTxt: "" , success: { (response) in
            ActivityIndicatorView.hiding()
            if let results = response as! GroupsToAddMembersList?{
                self.arrayFamiles = results.result!
            }
            self.tableViewMembers.dataSource = self
            self.tableViewMembers.delegate = self
            self.tableViewMembers.reloadData()
            
        }) { (error) in
            ActivityIndicatorView.hiding()
            
        }
        
    }
    
    func getMutalFamilies(){
        let parameter = ["user_id_one":UserDefaults.standard.value(forKey: "userId") as! String,"user_id_two":userID] as [String : Any]
        print(parameter)
        
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.getUserMutualGroup(parameter: parameter)) {(result) in
            switch result {
            case .success(let response):
                do {
                    let jsonData = JSON(response.data as Any)
                    
                    if response.statusCode == 200{
                        //                    do{
                        //                        if let tempDic = jsonData.dictionary as? JSONDictionary{
                        //                            let memberlist = try GroupsToAddMembersList(tempDic)
                        //                            print("$$$$$$$$$$\(memberlist)")
                        //                        }
                        //
                        //                        success(memberlist)
                        //                        return
                        //                    }catch{
                        //                        print("catched")
                        //                        return
                        //                    }
                        self.arrayUsers = jsonData["data"].arrayValue
                        print(self.arrayUsers)
                        self.tableViewMembers.delegate = self
                        self.tableViewMembers.dataSource = self
                        self.tableViewMembers.reloadData()
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getMutalFamilies()
                        }
                    }
                    else{
                        
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                    
                } catch let err {
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure(let error):
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        }
    }
    
    //MARK:- Button Actions
    
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func listRightButtonAction(_ sender: UIButton) {
        
        var user = JSON()
        if fromFamilies{
            user = self.arrayMutualUsers[sender.tag]
        }
        else{
            user = self.arrayUsers[sender.tag]
        }
        // if let id = user["id"].stringValue {
        print(user)
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let popOverVc = storyboard.instantiateViewController(withIdentifier: "SelectFamilyViewController") as! SelectFamilyViewController
        if isFrompost{
            popOverVc.selectedUserID = user["user_id"].stringValue
        }
        else{
            popOverVc.selectedUserID = user["id"].stringValue
        }
        self.navigationController?.pushViewController(popOverVc, animated: true)
        
        //            popOverVc.modalTransitionStyle = .crossDissolve
        //            popOverVc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        //            self.present(popOverVc, animated: true)
        
        //            let nav = UINavigationController(rootViewController: popoverContent)
        //            nav.modalPresentationStyle = UIModalPresentationStyle.popover
        //            let popover = nav.popoverPresentationController
        ////            popoverContent.preferredContentSize = CGSizeMake(500,600)
        ////            popover?.delegate = self
        //            popover?.sourceView = self.view
        ////            popover?.sourceRect = CGRectMake(100,100,0,0)
        //
        //            self.present(nav, animated: true, completion: nil)
        // }
        
    }
    
    @IBAction func joinAction(_ sender: UIButton) {
        let family = arrayFamiles[sender.tag]
        print(arrayFamiles[sender.tag])
        print(family.id)
        print(UserDefaults.standard.value(forKey: "userId") as! String)
        if family.id != nil{
            ActivityIndicatorView.show("Please wait!")
            /* APIServiceManager.callServer.sendMemberInvitation(url: EndPoint.inviteMember, fromId: UserDefaults.standard.value(forKey: "userId") as! String , toId: userID, groupId: "\(family.group_id)", success: { (response) in
             ActivityIndicatorView.hiding()
             self.displayAlert(alertStr: "Member added successfully!!!", title: "")
             self.dismiss(animated: true, completion: nil)
             }) { (error) in
             ActivityIndicatorView.hiding()
             }*/
            APIServiceManager.callServer.joinFamilyRequest(url: EndPoint.joinFamily, userId:  UserDefaults.standard.value(forKey: "userId") as! String, groupId: "\(family.id)", success: { (response) in
                ActivityIndicatorView.hiding()
                
                guard let result = response as? JoinResult else{
                    return
                }
                print(result)
                if result.joinData?.status.lowercased() == "accepeted" || result.joinData?.status.lowercased() == "joined" {
                    ActivityIndicatorView.hiding()
                    
                    self.displayAlert(alertStr: "Member added successfully!!!", title: "")
                    self.dismiss(animated: true, completion: nil)
                    
                    var fam = self.arrayFamiles[sender.tag]
                    fam.JOINED = true
                    self.arrayFamiles[sender.tag] = fam
                    self.tableViewMembers.reloadData()
                    //   self.lblJoin.isHidden = false
                    //   self.getFamilyDetails(groupId: self.groupId)
                }
                else{
                    if result.joinData?.status.lowercased() == "pending"{
                        var fam = self.arrayFamiles[sender.tag]
                        fam.request_status = "pending"
                        self.arrayFamiles[sender.tag] = fam
                        self.tableViewMembers.reloadData()
                    }
                    //  self.lblJoin.text = "Pending"
                }
                //                    if result.status_code == 200{
                //                        self.displayAlert(alertStr: "Join request send", title: "")
                //                    }
                //                    var fam = self.FamilyList[sender.tag]
                //
                //                    if fam.member_joining == 3{
                //                        fam.status = "accepeted"
                //                        fam.is_joined = "1"
                //                    }
                //                    else{
                //                        fam.status = "pending"
                //                    }
                //
                //                    self.FamilyList[sender.tag] = fam
                //                    self.tableView.beginUpdates()
                //                    self.tableView.reloadRows(at: [IndexPath.init(row: sender.tag, section: 0)], with: .none)
                //                    self.tableView.endUpdates()
                
            }) { (error) in
                ActivityIndicatorView.hiding()
                
            }
            
        }
    }
    
}
extension UserFamilyListViewController : UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFamilies{
            if isFromProfile{
                return arrayUsers.count
            }
            else{
                return arrayFamiles.count
            }
        }
        else if !eventID.isEmpty{
            return arraySharedUsers.count
        }
        else{
            if fromFamilies{
                return arrayMutualUsers.count
            }
            else{
                return arrayUsers.count
            }
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if isFamilies{
            if isFromProfile{
                let family = arrayUsers[indexPath.row]
                let cell = tableView.dequeueReusableCell(withIdentifier: "SharedPeopleCell", for: indexPath) as! SharedPeopleCell
                cell.rightButton.tag = indexPath.row
                
                cell.lblName.text = family["group_name"].stringValue
                cell.lblLocation.text = family["base_region"].stringValue
                cell.lblCreatedBy.text = "By \(family["full_name"].string ?? "Unknown")"
                cell.lblCategory.text = family["group_category"].stringValue
                
                cell.rightButton.isHidden = true
                cell.lblBtnTitle.isHidden = true
                cell.tempView.isHidden = true
//                familyMdl.familyList![0].memberJoining == 1{
                
                if family["logo"].stringValue.count != 0{
                    let temp = "\(Helpers.imageURl)"+"\(BaseUrl.groupLogo )"+family["logo"].stringValue
                    let imgurl = URL(string: temp)
                    let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+imgurl!.relativeString
                    let url = URL(string: newUrlStr)
                    cell.imgProfile.kf.indicatorType = .activity
                    
                    cell.imgProfile.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                }
                else{
                    cell.imgProfile.image = #imageLiteral(resourceName: "Family Logo")
                }
//                var memberJoining = family["member_joining"].intValue
//                var invitationStatus = family["invitation_status"].intValue
//
//                if memberJoining == 1{
//                    cell.lblBtnTitle.text = "Private"
//                    cell.rightButton.isEnabled = false
//                }
//                else{
//                    if invitationStatus == 1{
//                        cell.lblBtnTitle.text = "Pending"
//                        cell.rightButton.isEnabled = true
//                    }
//                    else{
//                        cell.lblBtnTitle.text = "Join"
//                        cell.rightButton.isEnabled = true
//                    }
//                }
                
                /*if family.JOINED == true{
                 //cell.rightButton.setTitle("MEMBER", for: .normal)
                 cell.lblBtnTitle.text = "MEMBER"
                 cell.rightButton.isEnabled = false
                 }
                 else{
                 if family.request_status.lowercased() == "pending"{
                 cell.lblBtnTitle.text = "PENDING"
                 cell.rightButton.isEnabled = false
                 }
                 else{
                 //cell.rightButton.setTitle("JOIN", for: .normal)
                 cell.lblBtnTitle.text = "JOIN"
                 cell.rightButton.isEnabled = true
                 }
                 }*/
                
                return cell
            }
            else{
                let family = arrayFamiles[indexPath.row]
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "SharedPeopleCell", for: indexPath) as! SharedPeopleCell
                
                cell.rightButton.tag = indexPath.row
                cell.lblName.text = family.group_name
                cell.lblLocation.text = family.base_region
                cell.lblCreatedBy.text = "By \(family.created_by ?? "Unknown")"
                cell.lblCategory.text = family.group_category
                
                if family.logo.count != 0{
                    let temp = "\(Helpers.imageURl)"+"\(BaseUrl.groupLogo )"+family.logo
                    let imgurl = URL(string: temp)
                    let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+imgurl!.relativeString
                    let url = URL(string: newUrlStr)
                    cell.imgProfile.kf.indicatorType = .activity
                    
                    cell.imgProfile.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "Family Detail"), options: nil, progressBlock: nil, completionHandler: nil)
                }
                else{
                    cell.imgProfile.image = #imageLiteral(resourceName: "Family Logo")
                }
                
//                print(family)

                let memberJoining = family.member_joining
                let invitationStatus = family.invitation_status
                let reqStatus = family.request_status
                if family.JOINED == true{
                    //cell.rightButton.setTitle("MEMBER", for: .normal)
                    cell.lblBtnTitle.text = "Member"
                    cell.rightButton.isEnabled = false
                }
                else
                {
                    if memberJoining == 1{
                        cell.lblBtnTitle.text = "Private"
                        cell.rightButton.isEnabled = false
                    }
                    else{
                        if invitationStatus == 1 || reqStatus.lowercased() == "pending"{
                            cell.lblBtnTitle.text = "Pending"
                            cell.rightButton.isEnabled = true
                        }
                        else{
                            cell.lblBtnTitle.text = "Join"
                            cell.rightButton.isEnabled = true
                        }
                    }
                }
                //                if family.JOINED == true{
                //                    //cell.rightButton.setTitle("MEMBER", for: .normal)
                //                    cell.lblBtnTitle.text = "Member"
                //                    cell.rightButton.isEnabled = false
//                }
//                else{
//                    if family.request_status.lowercased() == "pending"{
//                        cell.lblBtnTitle.text = "PENDING"
//                        cell.rightButton.isEnabled = false
//                    }
//                    else{
//                        //cell.rightButton.setTitle("JOIN", for: .normal)
//                        cell.lblBtnTitle.text = "JOIN"
//                        cell.rightButton.isEnabled = true
//                    }
//                }
                return cell
            }
        }
        else if !eventID.isEmpty{
            
            let user = self.arraySharedUsers[indexPath.row]
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddMembers_SearchTableViewCell", for: indexPath) as! AddMembers_SearchTableViewCell
            
            cell.btnAddMember.tag = indexPath.row
            cell.lblName.text = user["full_name"].string ?? "Unknown"
            cell.lblLocation.text = user["location"].stringValue
            cell.lblDate.isHidden = true
            if user["propic"].stringValue.count != 0{
                let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+user["propic"].stringValue
                let imgurl = URL(string: temp)
                let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+imgurl!.relativeString
                let url = URL(string: newUrlStr)
                cell.imageViewAvatar.kf.indicatorType = .activity
                cell.imageViewAvatar.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
            }
            else{
                cell.imageViewAvatar.image = #imageLiteral(resourceName: "Male Colored")
            }
            cell.viewRightButton.isHidden = true
            
            return cell
        }
        else if isFrom.lowercased() == "usershare"{
            var user = JSON()
            
            user = self.arrayUsers[indexPath.row]
            
            print(user)
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddMembers_SearchTableViewCell", for: indexPath) as! AddMembers_SearchTableViewCell
            
            cell.btnAddMember.tag = indexPath.row
            var to_name = user["to_group_name"].stringValue
            if to_name.isEmpty{
                to_name = user["to_user_name"].stringValue
            }
            if to_name.isEmpty{
                to_name = "Unknown"
            }
            cell.lblName.text = "\(user["shared_user_name"].string ?? "Unknown") shared to \(to_name)"
            cell.lblLocation.text = user["location"].stringValue
            cell.lblDate.text = HomeTabViewController.dateFormatterCommon(dateStr: user["created_date"].stringValue)
            if cell.lblDate.text!.isEmpty{
                cell.lblDate.isHidden = true
            }
            
            if user["propic"].stringValue.count != 0{
                let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+user["propic"].stringValue
                let imgurl = URL(string: temp)
                let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+imgurl!.relativeString
                let url = URL(string: newUrlStr)
                cell.imageViewAvatar.kf.indicatorType = .activity
                
                cell.imageViewAvatar.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
            }
            else{
                cell.imageViewAvatar.image = #imageLiteral(resourceName: "Male Colored")
            }
            
            cell.viewRightButton.isHidden = true
            
            return cell
        }
            
        else{
            var user = JSON()
            if fromFamilies{
                user = self.arrayMutualUsers[indexPath.row]
            }
            else{
                user = self.arrayUsers[indexPath.row]
            }
            
            print(user)
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddMembers_SearchTableViewCell", for: indexPath) as! AddMembers_SearchTableViewCell
            
            cell.btnAddMember.tag = indexPath.row
            cell.lblName.text = user["full_name"].string ?? "Unknown"
            cell.lblLocation.text = user["location"].stringValue
            cell.lblDate.isHidden = true
            if user["propic"].stringValue.count != 0{
                let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+user["propic"].stringValue
                let imgurl = URL(string: temp)
                let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+imgurl!.relativeString
                let url = URL(string: newUrlStr)
                cell.imageViewAvatar.kf.indicatorType = .activity
                cell.imageViewAvatar.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
            }
            else{
                cell.imageViewAvatar.image = #imageLiteral(resourceName: "Male Colored")
            }
            
            //     cell.viewRightButton.isHidden = false
            cell.viewRightButton.isHidden = true
            
            
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isFamilies{
            var grpId = ""
            if isFromProfile{
                let family = arrayUsers[indexPath.row]
                grpId = family["id"].stringValue
            }
            else{
                let family = arrayFamiles[indexPath.row]
                grpId = "\(family.id)"
            }
            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
            intro.groupId = grpId
            intro.isFromFamilyList = true
            self.navigationController?.pushViewController(intro, animated: true)
        }
        else if !eventID.isEmpty{
            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "UserProfileTableViewController") as! UserProfileTableViewController
            let user = self.arraySharedUsers[indexPath.row]
            vc.userID = user["id"].stringValue
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            var user = JSON()
            if self.fromFamilies{
                user = self.arrayMutualUsers[indexPath.row]
            }
            else{
                user = self.arrayUsers[indexPath.row]
            }
            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "UserProfileTableViewController") as! UserProfileTableViewController
            if isFrompost{
                vc.userID = user["user_id"].stringValue
            }
            else{
                vc.userID = user["id"].stringValue
            }
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isFamilies{
            return 155
        }
        else{
            
            return UITableView.automaticDimension
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if isFamilies{
            return 155
        }
        else{
            return UITableView.automaticDimension
        }
    }
}
