//
//  UserRequestTableViewCell.swift
//  familheey
//
//  Created by familheey on 31/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class UserRequestTableViewCell: UITableViewCell {
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDesign: UILabel!
    @IBOutlet weak var lblRequs: UILabel!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnReject: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
