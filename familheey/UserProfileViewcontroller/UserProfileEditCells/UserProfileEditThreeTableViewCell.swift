//
//  UserProfileEditThreeTableViewCell.swift
//  familheey
//
//  Created by familheey on 14/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class UserProfileEditThreeTableViewCell: UITableViewCell {
    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var btnValue: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
