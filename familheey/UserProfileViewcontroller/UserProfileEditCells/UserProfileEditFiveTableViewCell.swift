//
//  UserProfileEditFiveTableViewCell.swift
//  familheey
//
//  Created by familheey on 17/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class UserProfileEditFiveTableViewCell: UITableViewCell {
    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var btnImageSelect: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
