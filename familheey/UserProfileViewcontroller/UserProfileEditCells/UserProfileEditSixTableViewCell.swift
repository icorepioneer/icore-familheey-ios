//
//  UserProfileEditSixTableViewCell.swift
//  familheey
//
//  Created by Innovation Incubator on 24/01/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit

class UserProfileEditSixTableViewCell: UITableViewCell {
    @IBOutlet weak var switchbtn: UISwitch!
    
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
