//
//  UserProfileEditTwoTableViewCell.swift
//  familheey
//
//  Created by familheey on 14/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class UserProfileEditTwoTableViewCell: UITableViewCell {
    @IBOutlet weak var imgMale: UIImageView!
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var imgFe: UIImageView!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var imgNotSpecified: UIImageView!
    @IBOutlet weak var btnNotSpecified: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
