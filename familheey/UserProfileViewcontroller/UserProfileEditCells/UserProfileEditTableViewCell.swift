//
//  UserProfileEditTableViewCell.swift
//  familheey
//
//  Created by ANIL K on 12/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class UserProfileEditTableViewCell: UITableViewCell {
    @IBOutlet weak var lblHead: UILabel!
    @IBOutlet weak var txtValue: UITextField!
    @IBOutlet weak var btnAction: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
