//
//  AlbumDetailsViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 26/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Moya
import Alamofire
import SwiftyJSON
import CropViewController
import Kingfisher
import MobileCoreServices
import YPImagePicker
import AVKit
import Photos
import DKImagePickerController

class AlbumDetailsViewController: UIViewController, CropViewControllerDelegate,UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UIDocumentMenuDelegate,UIDocumentPickerDelegate,UIGestureRecognizerDelegate, imageDetailDelegate {

    
    @IBOutlet weak var readMoreView: UIView!
    @IBOutlet weak var readMore_height: NSLayoutConstraint!
    @IBOutlet weak var readmoreButton: UIButton!
    
    
    
    @IBOutlet weak var btnback: UIButton!
    
    @IBOutlet weak var backWhiteVwHeightConstrain           : NSLayoutConstraint!
    @IBOutlet weak var lblNoPhotoshow: UILabel!
    
    @IBOutlet weak var noCoverPicVw                         : UIView!
    @IBOutlet weak var coverImg                             : UIImageView!
    @IBOutlet weak var addPhotosVw                          : UIView!
    @IBOutlet weak var albumNameLbl                         : UILabel!
    
    
    @IBOutlet weak var albumDescTxtVw: UILabel!
    
    @IBOutlet weak var cover_hieght: NSLayoutConstraint!
    
    @IBOutlet weak var lblAddCover: UILabel!
    @IBOutlet weak var btnAddCover: UIButton!
    @IBOutlet weak var grdVew: UIView!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var playIcon: UIImageView!
    @IBOutlet weak var blckView: UIView!
    @IBOutlet weak var renameVew: UIView!
    @IBOutlet weak var txtRename: UITextField!
    
    @IBOutlet weak var deleteView: UIView!
    @IBOutlet weak var collVw                              : UICollectionView!
    @IBOutlet weak var addPhotosButt: UIButton!
    @IBOutlet weak var addPhotosBackButt: UIButton!
    @IBOutlet weak var topVwHeightConstrain: NSLayoutConstraint!
    
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    var imagePicker = UIImagePickerController()
    
    let maxNumberOfLines = 2
    var isFromMultiSelect = false
    var arrSelectedIndex = [Int]()
    var arrrofUsers = [String]()
    
    var groupId:String!
    var isFrom:String!
    var eventId:String!
    var folder_type:String!
    var createdBy = ""
    var isFromFamily = false
    var isAdmin = ""
    var isFromPost = false
    var isFromDocs = false
    var docData = NSData()
    var docFileName:[String] = []
    var albumId = ""
    var covrUrl = ""
    var isFromCreate = false
    var albumDetailsDict                                    = JSON()
    var dataArr                                             = JSON()
    var tempArrUsers                                        = [JSON]()
    var imgArr                                              = [Data]()
    var coverPicId = ""
    var strCover = ""
    var isCoverRemove = false
    var selectedDoc = 0
    let documentInteractionController = UIDocumentInteractionController()
    var selectedAttachments = NSMutableArray()
    
    var pickerController: DKImagePickerController!
    var imgDataArr = [Data]()
    var videoDataArr = [Data]()
    //    var config = YPImagePickerConfiguration()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isFromPost{
        }
        else{
            self.setupUIView()
        }
        self.callViewContents()
        
        //        let gradient: CAGradientLayer = CAGradientLayer()
        //
        //        // gradient.colors = [UIColor.init(white: 0, alpha: 3).cgColor, UIColor.init(white: 0, alpha: 0).cgColor]
        //        gradient.colors = [UIColor.black.cgColor, UIColor.clear.cgColor]
        //         //      gradient.locations = [0.0,1.0]
        //        //        gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
        //        //        gradient.endPoint = CGPoint(x: 1.0, y: 0.0)
        //        gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.grdVew.frame.size.width+50, height: self.grdVew.frame.size.height)
        //        self.grdVew.layer.insertSublayer(gradient, at: 0)
        

        // backWhiteVwHeightConstrain.constant = collVw.collectionViewLayout.collectionViewContentSize.height + 500
        
        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func onClickReadmoreAction(_ sender: UIButton) {
        
        if albumDescTxtVw.calculateMaxLines() > maxNumberOfLines &&  albumDescTxtVw.numberOfLines == maxNumberOfLines{
            
            albumDescTxtVw.numberOfLines = 0
           readmoreButton.setTitle("view less", for: .normal)
            
            
        }
        else if albumDescTxtVw.calculateMaxLines() > maxNumberOfLines &&  albumDescTxtVw.numberOfLines == 0 {
            UIView.performWithoutAnimation {
                albumDescTxtVw.numberOfLines = maxNumberOfLines
                readmoreButton.setTitle("view more", for: .normal)
            }
        }
        
    }
    
    @IBAction func addPhontosClicked(sender: UIButton){
        
        if isFromDocs{
            clickFunction()
        }
        else{
            /* let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
             alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
             self.openCamera()
             }))
             
             alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
             self.openGallary()
             }))
             
             
             alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
             self.present(alert, animated: true, completion: nil)*/
            
            let tempArr = ["Photos","Video"]
            self.showActionSheet(titleArr: tempArr as NSArray, title: "Select option") { (index) in
                if index == 0{
                    self.selectImages(type: "images")
                }
                else if index == 1{
                    self.selectImages(type: "videos")
                }
                else{
                }
            }
            
        }
    }
    
    
    //MARK:- Button actions
    @IBAction func onclickaddCovr(_ sender: Any) {
        
        let ImageDetailsViewController = self.storyboard?.instantiateViewController(withIdentifier: "ImageDetailsViewController") as! ImageDetailsViewController
        
        ImageDetailsViewController.imgDetails          = self.dataArr[0]
        ImageDetailsViewController.dataArr2            = self.dataArr
        ImageDetailsViewController.isFrom2             = isFrom
        ImageDetailsViewController.selectedIndex       = 0
        ImageDetailsViewController.coverUrl = self.covrUrl
        ImageDetailsViewController.isfromCreate = self.isFromCreate
        ImageDetailsViewController.fromCover = "cover"
        ImageDetailsViewController.createdBy = self.createdBy
        self.navigationController?.pushViewController(ImageDetailsViewController, animated: true)
    }
    
    @IBAction func onClickCoverAdd(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: "ChangePicturesViewController") as! ChangePicturesViewController
        //vc.isFrom = "events"
        vc.isCoverOrProfilePic = "CoverPic"
        vc.ids           = UserDefaults.standard.value(forKey: "userId") as! String
        vc.isFrom = "album"
        vc.inFor = "cover"
        vc.isAdmin = ""
        // vc.eventId = eventDetails[""].stringValue
        // vc.eventId = self.eventId
        // vc.delegate = self
        
        vc.imageUrl = self.covrUrl
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickEditAlbum(_ sender: Any) {
        let CreateAlbumViewController = self.storyboard?.instantiateViewController(withIdentifier: "CreateAlbumViewController") as! CreateAlbumViewController
        // CreateAlbumViewController.groupId = groupID
        CreateAlbumViewController.isFrom =  isFrom
        CreateAlbumViewController.isFromDocs = isFromDocs
        CreateAlbumViewController.isFromEdit = true
        //        if isFrom.lowercased() == "group"{
        //            CreateAlbumViewController.groupId = self.groupId
        //        }
        //        else{
        //            CreateAlbumViewController.eventId = self.eventId
        //        }
        //        CreateAlbumViewController.type = self.folder_type
        CreateAlbumViewController.name = albumDetailsDict["folder_name"].stringValue
        CreateAlbumViewController.desc = albumDetailsDict["description"].stringValue
        CreateAlbumViewController.folderId = albumDetailsDict["id"].stringValue
        CreateAlbumViewController.permission = albumDetailsDict["permissions"].stringValue
        CreateAlbumViewController.groupId = albumDetailsDict["group_id"].stringValue
        CreateAlbumViewController.createdBy = self.createdBy
        if albumDetailsDict["permission_users"].arrayValue.count > 0{
            CreateAlbumViewController.selectedUserIDArr = NSMutableArray(array: albumDetailsDict["permission_users"].arrayObject!)
        }
        self.navigationController?.pushViewController(CreateAlbumViewController, animated: true)
    }
    @IBAction func onClickDelete(_ sender: Any) {
        var alertStr = ""
        if isFromDocs{
            alertStr = "documents"
        }
        else{
            alertStr = "photos"
        }
        let alert = UIAlertController(title: "Familheey", message: "Do you want to delete \(self.arrrofUsers.count) \(alertStr)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { _ in
            
            /* let data = self.dataArr[indexPath.row]
             self.deleteFolder(fId: data["id"].stringValue)*/
            var parameter =  [String : Any]()
            parameter = ["id":self.arrrofUsers]
            print(parameter)
            if self.arrrofUsers.contains(self.coverPicId){
                self.isCoverRemove = true
            }
           ActivityIndicatorView.show("Loading....")

            self.networkProvider.request(.delete_file(parameter: parameter)) { (result) in
                switch result{

                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                        print(jsonData)
                        if response.statusCode == 200{
                            self.arrrofUsers = []
                            self.arrSelectedIndex = []
                            self.deleteView.isHidden = true
                            if self.isCoverRemove{
                                self.makeCoverPic()
                            }
                            else{
                                self.callViewContents()
                            }
                        }
                        
                        ActivityIndicatorView.hiding()
                        
                    }catch let err {
                        ActivityIndicatorView.hiding()
                        Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                    }
                case .failure( let error):
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                    break
                }
            }
            
        }))
        alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: { _ in
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func makeCoverPic(){
        var parameter = [String:Any]()
        
        if isCoverRemove{
            parameter = ["album_id": albumDetailsDict["id"].stringValue, "cover_pic" : ""]
        }
        
        print("Param : \(parameter)")
        ActivityIndicatorView.show("Loading....")
        
        networkProvider.request(.make_pic_cover(parameter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    
                    if response.statusCode == 200{
                        
                        if self.isCoverRemove{
                            // self.delegate?.callAfterDelete(removeCover: self.isFreomRemove)
                            self.callViewContents()
                        }
                        /* else{
                         if self.isfromCreate{
                         var array : [UIViewController] = (self.navigationController?.viewControllers)!
                         array.remove(at: array.count - 1)
                         array.remove(at: array.count - 1)
                         array.remove(at: array.count - 1)
                         self.navigationController?.viewControllers = array
                         }
                         else{
                         var array : [UIViewController] = (self.navigationController?.viewControllers)!
                         array.remove(at: array.count - 1)
                         array.remove(at: array.count - 1)
                         self.navigationController?.viewControllers = array
                         }
                         
                         self.backClicked(self)
                         }*/
                        print("Json data : \(jsonData)")
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
        
    }
    @IBAction func onClickRightMenu(_ sender: subclassedUIButton) {
        var arr = [String]()
        if isFrom.lowercased() == "groups"{
            if isAdmin.lowercased() == "admin"{
                arr = ["Rename","Download"]
            }
            else{
                let userId = UserDefaults.standard.value(forKey: "userId") as! String
                if self.createdBy == userId{
                    arr = ["Rename","Download"]
                }
                else{
                    arr = ["Download"]
                }
            }
        }
        else{
            let userId = UserDefaults.standard.value(forKey: "userId") as! String
            if self.createdBy == userId{
                arr = ["Rename","Download"]
            }
            else{
                arr = ["Download"]
            }
        }

       
        showActionSheet(titleArr: arr as! NSArray, title: "Select option") { (result) in
            if result == 100{
            }
            else if result == 0{
                if self.isFrom.lowercased() == "groups"{
                    if self.isAdmin.lowercased() == "admin"{
                        //  self.downloadDocumentFromUrl(index: sender.indexPath!)
                        self.selectedDoc = sender.indexPath!
                        let name = self.dataArr[sender.indexPath!]["file_name"].stringValue
                        let ext = name.components(separatedBy: ".").last
                        self.txtRename.text = name.components(separatedBy: ".").first
                        self.renameVew.isHidden = false
                        
                    }
                    else{
                        let userId = UserDefaults.standard.value(forKey: "userId") as! String
                        if self.createdBy == userId{
                            self.selectedDoc = sender.indexPath!
                            let name = self.dataArr[sender.indexPath!]["file_name"].stringValue
                            let ext = name.components(separatedBy: ".").last
                            self.txtRename.text = name.components(separatedBy: ".").first
                            self.renameVew.isHidden = false
                        }
                        else{
                            //  self.downloadDocumentFromUrl(index: sender.indexPath!)
                            
                            let data = self.dataArr[sender.indexPath!]
                            let urlStr = data["url"].stringValue
                            
                            self.downloadPDFfromUrl(urlStr: urlStr)
                        }
                    }
                }
                else{
                    let userId = UserDefaults.standard.value(forKey: "userId") as! String
                    if self.createdBy == userId{
                        self.selectedDoc = sender.indexPath!
                        let name = self.dataArr[sender.indexPath!]["file_name"].stringValue
                        let ext = name.components(separatedBy: ".").last
                        self.txtRename.text = name.components(separatedBy: ".").first
                        self.renameVew.isHidden = false
                    }
                    else{
                        let data = self.dataArr[sender.indexPath!]
                        let urlStr = data["url"].stringValue
                        
                        self.downloadPDFfromUrl(urlStr: urlStr)
                    }
                }
                
            }
            else if result == 1{
                //  self.downloadDocumentFromUrl(index: sender.indexPath!)
                  
                  let data = self.dataArr[sender.indexPath!]
                  let urlStr = data["url"].stringValue
                  
                  self.downloadPDFfromUrl(urlStr: urlStr)
            }
            else{
            }
        }
    }
    
    @IBAction func onClickRenameCancel(_ sender: Any) {
        self.renameVew.isHidden = true
    }
//MARK: - Download PDF
    
    func downloadPDFfromUrl(urlStr: String){
        
        let urlString = urlStr
        let url = URL(string: urlString)
        let fileName = String((url!.lastPathComponent)) as NSString
        // Create destination URL
        let documentsUrl:URL =  (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL?)!
        let destinationFileUrl = documentsUrl.appendingPathComponent("\(fileName)")
        //Create URL to the source file you want to download
        let fileURL = URL(string: urlString)
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        let request = URLRequest(url:fileURL!)
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            if let tempLocalUrl = tempLocalUrl, error == nil {
                // Success
                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                    print("Successfully downloaded. Status code: \(statusCode)")
                }
                
                do {
                    try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                    
                    print("tempLocalUrl : \(tempLocalUrl)")
                    print("destinationFileUrl : \(destinationFileUrl)")
                    
                    do {
                        //Show UIActivityViewController to save the downloaded file
                        let contents  = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
                        for indexx in 0..<contents.count {
                            if contents[indexx].lastPathComponent == destinationFileUrl.lastPathComponent {
                                
                                if FileManager.default.fileExists(atPath: destinationFileUrl.absoluteString) {
                                    // Delete file
                                    try FileManager.default.removeItem(atPath: destinationFileUrl.absoluteString)
                                } else {
                                    print("File does not exist")
                                }
                                
                                DispatchQueue.main.async {
                                    
                                    let activityViewController = UIActivityViewController(activityItems: [contents[indexx]], applicationActivities: nil)
                                    self.present(activityViewController, animated: true, completion: nil)
                                }
                                
                            }
                        }
                    }
                    catch (let err) {
                        print("error: \(err)")
                    }
                } catch (let writeError) {
                    print("Error creating a file \(destinationFileUrl) : \(writeError)")
                }
            } else {
                
                print("Error took place while downloading a file. Error description: \(error?.localizedDescription ?? "")")
            }
        }
        task.resume()
    }
    
    @IBAction func onClickRenameSave(_ sender: Any) {
        let name = self.dataArr[selectedDoc]["file_name"].stringValue
        let ext = name.components(separatedBy: ".").last
//        let id = self.dataArr[selectedDoc]["id"]
        let parameter = ["id":self.dataArr[selectedDoc]["id"].stringValue,"file_name":"\(txtRename.text!).\(ext!)"] as! [String : String]
        print("Param : \(parameter)")
        
//        DispatchQueue.main.async {
            ActivityIndicatorView.show("Loading....")
//        }
        
        networkProvider.request(.update_file_name(parameter: parameter)) { (result) in
            switch result{
                case .success( let response):
                    do{
                        ActivityIndicatorView.hiding()
                        let jsonData =  JSON(response.data)
                        print("view contents : \(jsonData)")
                        if response.statusCode == 200{
                            print("view contents : \(jsonData)")
                            self.renameVew.isHidden = true
                            self.callViewContents()

                        }

                       

                    }catch let err {
//                        ActivityIndicatorView.hiding()
                        // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                    }
                case .failure(let error):
//                    ActivityIndicatorView.hiding()
                    //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                    break
            }
        }
        
    }
    
    //MARK:- Custom Methods
    func setupUIView(){
        if DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_XP{
            
            topVwHeightConstrain.constant = 85
        }else{
            
            topVwHeightConstrain.constant = 65
        }
        
        backWhiteVwHeightConstrain.constant = self.view.frame.height
        
        print("albumDetailsDict : \(albumDetailsDict)")
        
        albumNameLbl.text                 = albumDetailsDict["folder_name"].stringValue
        albumDescTxtVw.text               = albumDetailsDict["description"].stringValue
        
        if albumDescTxtVw.calculateMaxLines() > maxNumberOfLines{
            albumDescTxtVw.numberOfLines = maxNumberOfLines
            readMoreView.isHidden = false
            
            //            tableView.layoutIfNeeded()
        }
        else{
            albumDescTxtVw.numberOfLines = 0
            readMoreView.isHidden = true
            //            tableView.layoutIfNeeded()
        }
        if albumDetailsDict["cover_pic"]  == JSON.null{
            btnback.setImage(UIImage(named: "backWhite"), for: .normal)
            noCoverPicVw.isHidden         = false
        }
        else if albumDetailsDict["cover_pic"].stringValue.count == 0{
            btnback.setImage(UIImage(named: "backWhite"), for: .normal)
            noCoverPicVw.isHidden         = false
        }
        else{
            let temp = self.albumDetailsDict["cover_pic"].stringValue
            let t = temp.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            
            let imgurl                     = URL(string: t!)
            covrUrl = imgurl!.relativeString
            print(covrUrl)
            coverImg.kf.indicatorType      = .activity
            let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=400&height=300&url="+imgurl!.relativeString
            let url = URL(string: newUrlStr)
            
            coverImg.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "imgNoImage_landscape") , options: nil, progressBlock: nil, completionHandler: nil)
            //          /  grdVew.isHidden = false
            btnback.setImage(UIImage(named: "backWhite"), for: .normal)
            noCoverPicVw.isHidden         = true
        }
        
        if isFromDocs{
            cover_hieght.constant = 0
            //            hight_ratio.isActive = false
            //            hight_ratio.constant = 0
            addPhotosButt.setTitle("+   Add documents", for: .normal)
            addPhotosBackButt.isHidden = false
            lblNoPhotoshow.text = "Documents will show up here after you add them."
        }
        else{
            cover_hieght.constant  = 281
            //            hight_ratio.isActive = true
            
            addPhotosButt.setTitle("+   Add Media", for: .normal)
            addPhotosBackButt.isHidden = true
            lblNoPhotoshow.text = "Media will show up here after you add them."
            
        }
        self.isFrom = albumDetailsDict["folder_for"].stringValue
        print(isFrom!)
        let userId = UserDefaults.standard.value(forKey: "userId") as! String
        if self.createdBy == userId{
            btnEdit.isHidden = false
            addPhotosButt.isHidden = false
            setupLongPressGesture()
        }
        else{
            if isFrom.lowercased() == "groups"{
                if isAdmin.lowercased() == "admin"{
                    btnEdit.isHidden = false
                    addPhotosButt.isHidden = false
                    setupLongPressGesture()
                }
                else{
                    btnEdit.isHidden = true
                    addPhotosButt.isHidden = true
                }
            }
            else{
                btnEdit.isHidden = true
                addPhotosButt.isHidden = true
            }
           
        }
    }
    
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else{
            // self.displayAlert(alertStr: "You don't have camera", title: "")
        }
        //selectImages(type: "")
        
    }
    
    func openGallary(){
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func selectedImage(img:UIImage){
        // let cropView = CropViewController(image: img)
        let cropView = CropViewController(croppingStyle: .default, image: img)
        cropView.delegate = self
        self.navigationController?.pushViewController(cropView, animated: true)
    }
    
    func setupLongPressGesture() {
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress))
        longPressGesture.minimumPressDuration = 1.0 // 1 second press
        longPressGesture.delegate = self
        self.collVw.addGestureRecognizer(longPressGesture)
    }
    
    @objc func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer){
        if gestureRecognizer.state == .ended {
            let touchPoint = gestureRecognizer.location(in: self.collVw)
            
            /* if let indexPath = self.collVw.indexPathForItem(at: touchPoint) {
             // get the cell at indexPath (the one you long pressed)
             let cell = self.collVw.cellForItem(at: indexPath)
             // do stuff with the cell
             let alert = UIAlertController(title: "Familheey", message: "Do you want to delete this folder", preferredStyle: .alert)
             alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { _ in
             
             let data = self.dataArr[indexPath.row]
             self.deleteFolder(fId: data["id"].stringValue)
             //                 let currentSignUp =
             //
             //                 self.deleteSignupID   = currentSignUp["id"].intValue
             //self.callDeleteSignUpSlotApi()
             }))
             alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: { _ in
             }))
             
             self.present(alert, animated: true, completion: nil)
             } else {
             print("couldn't find index path")
             }*/
            
            if let indexPath = self.collVw.indexPathForItem(at: touchPoint){
                let cell = collVw.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
                //  let cell = self.collVw.cellForItem(at: indexPath)
                print(indexPath.row)
                // arrSelectedIndex.removeAll()
                self.collVw.allowsMultipleSelection = true
                self.isFromMultiSelect = true
                self.deleteView.isHidden = false
                arrSelectedIndex.append(indexPath.row)
                let data = self.dataArr[indexPath.row]
                arrrofUsers.append(data["id"].stringValue)
                // cell?.borderColor = UIColor(white: 0, alpha: 0.25)
                //                cell.contentView.backgroundColor = UIColor.green
                
                var indexPaths = [IndexPath]()
                indexPaths.append(indexPath)
                
                if let collectionView = collVw {
                    collectionView.reloadItems(at: indexPaths)
                }
            }
            
        }
    }
    
    //MARK:- Custom Delegate
    func downloadDocumentFromUrl(index:Int){
       /* let data = self.dataArr[index]
        print(data)
        
        // Url in String format
        let urlStr = data["url"].stringValue
        // Converting string to URL Object
        let url = URL(string: data["url"].stringValue)
        let fileName = String((url!.lastPathComponent)) as NSString
        
        let urlSession = URLSession(configuration: .default, delegate: self, delegateQueue: OperationQueue())
        
        let downloadTask = urlSession.downloadTask(with: url!)
        downloadTask.resume()*/
        let data = self.dataArr[index]
        let urlStr = data["url"].stringValue
        
        let fileURL = URL(string: urlStr)!
        

        let documentsUrl:URL =  (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL?)!
        let destinationFileUrl = documentsUrl.appendingPathComponent("Familheey\(fileURL.lastPathComponent)")

        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)

        let request = URLRequest(url:fileURL)

        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            if let tempLocalUrl = tempLocalUrl, error == nil
            {
                if let statusCode = (response as? HTTPURLResponse)?.statusCode
                {
                    print("Successfully downloaded. Status code: \(statusCode)")
                }
                do
                {
                    if(FileManager.default.fileExists(atPath: destinationFileUrl.path))
                    {
                        try FileManager.default.removeItem(at: destinationFileUrl)
                        try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                    }
                    else
                    {
                        try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                    }

                    if let iCloudDocumentsURL = FileManager.default.url(forUbiquityContainerIdentifier: nil)?.appendingPathComponent("Downloads")
                    {

                        if(!FileManager.default.fileExists(atPath: iCloudDocumentsURL.path, isDirectory: nil))
                        {
                            try FileManager.default.createDirectory(at: iCloudDocumentsURL, withIntermediateDirectories: true, attributes: nil)
                        }
                    }

                    let iCloudDocumentsURL = FileManager.default.url(forUbiquityContainerIdentifier: nil)?.appendingPathComponent("Downloads").appendingPathComponent("Familheey\(fileURL.lastPathComponent)")

                    if let iCloudDocumentsURL = iCloudDocumentsURL
                    {
                        var isDir:ObjCBool = false
                        if(FileManager.default.fileExists(atPath: iCloudDocumentsURL.path, isDirectory: &isDir))
                        {
                            try FileManager.default.removeItem(at: iCloudDocumentsURL)
                            try FileManager.default.copyItem(at: tempLocalUrl, to: iCloudDocumentsURL)
                        }
                        else
                        {
                            try FileManager.default.copyItem(at: destinationFileUrl, to: iCloudDocumentsURL)
                        }
                    }

                }
                catch (let writeError)
                {
                    print("Error creating a file \(destinationFileUrl) : \(writeError)")
                }
            }
            else
            {
                print("Error took place while downloading a file. Error description");
            }
        }
        task.resume()
    }
    
    func callAfterDelete(removeCover: Bool) {
        if removeCover{
            self.noCoverPicVw.isHidden = false
        }
        callViewContents()
    }
    
    //MARK: -  Image picker
    
    func selectImages(type:String){
        
        pickerController = DKImagePickerController()
        selectedAttachments.removeAllObjects()
        DKImageExtensionController.unregisterExtension(for:.camera)
        
        if type.lowercased() == "images"{
            pickerController.assetType = .allPhotos
            pickerController.maxSelectableCount = 25
            
        }
        else{
            pickerController.assetType = .allVideos
            pickerController.maxSelectableCount = 1
            DKImageExtensionController.registerExtension(extensionClass: CustomCameraExtension.self, for: .camera)
        }
        pickerController.deselectAll()
        
        pickerController.didSelectAssets = { [unowned self] (assets: [DKAsset]) in
            
            if assets.count != 0{
                
                ActivityIndicatorView.show("Loading....")
                
                DKImageAssetExporter.sharedInstance.exportAssetsAsynchronously(assets: assets) { (info) in
                    //                ActivityIndicatorView.hiding()
                    self.imgDataArr = []
                    self.videoDataArr = []
                    
                    let dispatchQueue = DispatchQueue(label: "myQueue", qos: .background)
                    let semaphore = DispatchSemaphore(value: 0)
//                    dispatchQueue.async {
                        for item in assets {
                            switch item.type {
                            case .photo:
                                if let localTemporaryPath = item.localTemporaryPath,
                                    let imageData = try? Data(contentsOf: localTemporaryPath) {
                                    let img = UIImage.init(data: imageData)
                                    if let fixedData = img?.fixedOrientation.jpegData(compressionQuality: 0.75){
                                        self.imgDataArr.append(fixedData)
                                    }
                                }
                                
                                //                        if self.imgDataArr.count == assets.count{
                                //                            self.uploadImageMultipleImages(arr: self.imgDataArr)
                                
                                //                                for i in 0..<self.imgDataArr.count{
                                AWSS3Manager.shared.uploadVideo(videoUrl: item.localTemporaryPath!, folder: "Documents/", progress: { (progress) in
                                    print(progress)
                                    //                                guard let strongSelf = self else { return }
                                    
                                }) { [weak self] (uploadedFileUrl, error) in
                                    
                                    if let finalPath = uploadedFileUrl as? URL {
                                        print("Uploaded file url: " , finalPath)
                                        var img = UIImage()
                                        
                                        if let localTemporaryPath = item.localTemporaryPath,
                                        let imageData = try? Data(contentsOf: localTemporaryPath) {
                                            img = UIImage.init(data: imageData)!
                                        }
                                        
                                        var dict = [String:String]()
                                        dict["url"] = "Documents/\(finalPath.lastPathComponent)"
                                        dict["file_type"] = "image"
                                        dict["file_name"] = finalPath.lastPathComponent
                                        dict["original_name"] = finalPath.lastPathComponent
                                        dict["width"] = "\(String(describing: img.size.width))"
                                        dict["height"] = "\(String(describing: img.size.height))"
                                        print(dict)
                                        //                                    self?.updateS3(dic: dict)
                                        self?.selectedAttachments.add(dict)
//                                        semaphore.signal()
                                        
                                        if self?.selectedAttachments.count == assets.count{
                                            self?.updatImageeS3(Arrdic: self?.selectedAttachments ?? NSMutableArray())
                                        }
                                        
                                    } else {
                                        print("\(String(describing: error?.localizedDescription))")
                                        self?.displayAlert(alertStr: "Unable to upload file, please try again", title: "")
//                                        semaphore.signal()
                                    }
                                }
                                //                                }
                                //                        }
                                
                            case .video:
                                if let localTemporaryPath = item.localTemporaryPath{
                                    
                                    item.fetchFullScreenImage { (image, info) in
                                        AWSS3Manager.shared.uploadImage(image: image!, file_name: "video_thumb/", progress: { (progress) in
                                            ActivityIndicatorView.show("Uploading...")
                                            
                                        }) { [weak self] (uploadedThumbUrl, error) in
                                            
                                            if let finalPathThumb = uploadedThumbUrl as? URL {
                                                
                                                print("Uploaded file url: " , finalPathThumb)
                                                AWSS3Manager.shared.uploadVideo(videoUrl: localTemporaryPath, folder: "Documents/", progress: { (progress) in
                                                    print(progress)
                                                    guard let strongSelf = self else { return }
                                                    
                                                }) { [weak self] (uploadedFileUrl, error) in
                                                    
                                                    if let finalPath = uploadedFileUrl as? URL {
                                                        print("Uploaded file url: " , finalPath)
                                                        
                                                        var dict = [String:String]()
                                                        dict["url"] = "Documents/\(finalPath.lastPathComponent)"
                                                        dict["file_type"] = "video"
                                                        dict["file_name"] = finalPath.lastPathComponent
                                                        dict["original_name"] = finalPath.lastPathComponent
                                                        dict["video_thumb"] = "video_thumb/\(finalPathThumb.lastPathComponent)"
                                                        print(dict)
                                                        self?.updateS3(dic: dict)
                                                        
                                                    } else {
                                                        print("\(String(describing: error?.localizedDescription))")
                                                        self?.displayAlert(alertStr: "Unable to upload file, please try again", title: "")
                                                    }
                                                }
                                                
                                            } else {
                                                print("\(String(describing: error?.localizedDescription))")
                                                self?.displayAlert(alertStr: "Unable to upload file, please try again", title: "")
                                            }
                                        }
                                    }
                                }
                            }
                        }
//                    }
//                    semaphore.wait()
                }
            }
        }
        
        if pickerController.UIDelegate == nil {
            pickerController.UIDelegate = AssetClickHandler()
        }
        
        self.present(pickerController, animated: true) {}
        
    }
        
  /*  func weatherService() {

        dispatchQueue.async {
            for i in 1...10 {
                APIManager.apiGet(serviceName: self.weatherServiceURL, parameters: ["counter":i]) { (response:JSON?, error:NSError?, count:Int) in
                    if let error = error {
                        print(error.localizedDescription)
                        self.semaphore.signal()
                        return
                    }
                    guard let response = response else {
                        self.semaphore.signal()
                        return
                    }

                    print("\(count) ")

                    //Check by index, the last service in this case
                    if i == 10 {
                        print("Services Completed")
                    } else {
                        print("An error occurred")
                    }

                    // Signals that the 'current' API request has completed
                    self.semaphore.signal()
                }

                // Wait until the previous API request completes
                self.semaphore.wait()
            }
        }
        print("Start Fetching")
    }*/
    
    func getDatafrom(asset: PHAsset?, count:Int){
        if asset == nil{
            return
        }
        let manager = PHImageManager.default()
        if asset?.mediaType == .image{
            manager.requestImageData(for: asset!, options: nil) { (data, string, orientation, info) in
                guard let imageData = data else {
                    return
                }
                self.imgDataArr.append(imageData)
                if self.imgDataArr.count == count{
                    DispatchQueue.main.async {
                    self.uploadImageMultipleImages(arr: self.imgDataArr)
                    }
                }
            }
        }else if asset?.mediaType == .video{
            manager.requestAVAsset(forVideo: asset!, options: nil, resultHandler: { (avasset, audio, info) in
                if let avassetURL = avasset as? AVURLAsset {
                    guard let video = try? Data(contentsOf: avassetURL.url) else {
                        return
                    }
                    self.videoDataArr.append(video)
                    if self.videoDataArr.count == count{
                        DispatchQueue.main.async {
                            self.uploadVideoMultiple(arr: self.videoDataArr)
                        }
                    }
                }
            })
        }
        
    }
        
    
        
  /*
        config.library.maxNumberOfItems = 25
        config.library.minNumberOfItems = 1
        config.library.defaultMultipleSelection = true
        
        if type.lowercased() == "images"{
            config.library.mediaType = .photo
            config.screens = [.library, .photo]
            config.startOnScreen = .library
        }
        else{
            if appDel.phoneHasVideos{
                config.screens = [.library,.video]
                config.startOnScreen = .library
            }
            else{
                config.screens = [.video]
                config.startOnScreen = .video
            }
            config.library.mediaType = .video
//            config.screens = [.library, .video]
        }
        config.targetImageSize = .original
        config.usesFrontCamera = true
        config.showsPhotoFilters = true
        config.video.recordingTimeLimit = 600
        config.video.libraryTimeLimit = 3600
        config.showsVideoTrimmer = false
        config.wordings.libraryTitle = "Gallery"
        config.library.isSquareByDefault = false
        config.video.compression = AVAssetExportPresetPassthrough
        
        let picker = YPImagePicker(configuration: config)
        
        picker.didFinishPicking { [unowned picker] items, cancelled in
            print("Items : \(items.count)")
            //            if let photo = items.singlePhoto {
            //                print(photo.fromCamera) // Image source (camera or library)
            //                print(photo.image) // Final image selected by the user
            //                print(photo.originalImage) // original image selected by the user, unfiltered
            //                // print(photo.modifiedImage) // Transformed image, can be nil
            //                //   print(photo.exifMeta) // Print exif meta data of original image.
            //                self.uploadImage(img: photo.image)
            //            }
            //  else{
            if cancelled {
                print("Picker was canceled")
                picker.dismiss(animated: true, completion: nil)
                return
            }
            
            //            if items.count == 1{
            //
            //                self.uploadImage(img: items.singlePhoto!.image)
            //
            //            }
            //            else if items.count == 0{
            //                 picker.dismiss(animated: true, completion: nil)
            //            }
            //            else
            //            {
            var imgDataArr = [Data]()
            var videoDataArr = [Data]()
            
            for item in items {
                switch item {
                case .photo(let photo):
                    print(photo)
                    
                    let img2 = photo.image.resized(withPercentage: 0.75)
                    
                    let imgData = (img2?.pngData())!
                    
                    imgDataArr.append(imgData)
                    
                case .video(let video):
                    print(video)
                    let videoUrl = video.url
                    do {
                        let videoData = try NSData(contentsOf: videoUrl, options: NSData.ReadingOptions())
                         videoDataArr.append(videoData as Data)
                    } catch {
                        print(error)
                    }
                }
                
            }
            
            
            print("image ar : \(imgDataArr)")
            //                self.uploadImageMultipleImages(arr: imgDataArr)
            
            
            if imgDataArr.count != 0
            {
                self.uploadImageMultipleImages(arr: imgDataArr)
                
            }
            if videoDataArr.count != 0
            {
                self.uploadVideoMultiple(arr: videoDataArr)
                
            }
            // }
            
            
            //  }
            
            
            picker.dismiss(animated: true, completion: nil)
        }
        
        //        picker.didFinishPicking { [unowned picker] items, cancelled in
        //            if cancelled {
        //                print("Picker was canceled")
        //            }
        //            picker.dismiss(animated: true, completion: nil)
        //        }
        present(picker, animated: true, completion: nil)
    }
    
    
*/
    
    func clickFunction(){
        // let types = [kUTTypePDF, kUTTypeText, kUTTypeRTF, kUTTypeSpreadsheet, kUTTypeRTFD]
        let types = [kUTTypePDF, kUTTypeText, kUTTypeRTF, kUTTypeSpreadsheet, "com.microsoft.word.doc" as CFString]
        let importMenu = UIDocumentPickerViewController(documentTypes: types as [String], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
        
        
//        let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypeText), String(kUTTypePDF), String(kUTTypePNG), String(kUTTypeJPEG), String(kUTTypePlainText), String(kUTTypeImage)], in: .import)
    }
    
    //MARK:- CropView SDK Delegate
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        
        // coverImg.image = image
        self.navigationController?.popViewController(animated: true)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            
            self.uploadImage(img: image)
        })
    }
    
    //MARK: - Multiple Image Uploading
    
    func uploadImageMultipleImages(arr : [Data]){
        
        var param = ["folder_id" : albumDetailsDict["id"].stringValue, "user_id" : UserDefaults.standard.value(forKey: "userId") as! String, "group_id" : albumDetailsDict["group_id"].stringValue, "is_sharable" : "true"]
        
        if isFrom == "events"{
            
            param = ["folder_id" : albumDetailsDict["id"].stringValue, "user_id" : UserDefaults.standard.value(forKey: "userId") as! String, "event_id" : albumDetailsDict["event_id"].stringValue, "is_sharable" : "true"]
        }
        
        ActivityIndicatorView.show("Uploading...")
        Helpers.requestWithMultipleImages(fileParamName: "Documents", urlAppendString: "uploadFile", imageData: arr, parameters: param, mimeType: "image/png") { (response) in
            print("Response : \(response)")
            
            self.callViewContents()
        }
    }
    
    func uploadVideoMultiple(arr : [Data]?){
        
        var param = [String:Any]()
         param = ["folder_id" : albumDetailsDict["id"].stringValue, "user_id" : UserDefaults.standard.value(forKey: "userId") as! String, "group_id" : albumDetailsDict["group_id"].stringValue, "is_sharable" : "true"]
        
        if isFrom == "events"{
            
            param = ["folder_id" : albumDetailsDict["id"].stringValue, "user_id" : UserDefaults.standard.value(forKey: "userId") as! String, "event_id" : albumDetailsDict["event_id"].stringValue, "is_sharable" : "true"]
        }
     
        ActivityIndicatorView.show("Uploading...")
        Helpers.requestWithMultipleImages(fileParamName: "Documents", urlAppendString: "uploadFile", imageData: arr, parameters: param, mimeType: "video/quicktime/m4v") { (response) in
            print("Response : \(response)")
            
            self.callViewContents()
        }
        
        
    }
    
    //MARK:- update s3
    
    func updateS3(dic:[String:Any]){
        
        var param = [String:Any]()
        param = ["folder_id" : albumDetailsDict["id"].stringValue, "user_id" : UserDefaults.standard.value(forKey: "userId") as! String, "group_id" : albumDetailsDict["group_id"].stringValue, "is_sharable" : "true"]
        
        if isFrom == "events"{
            
            param = ["folder_id" : albumDetailsDict["id"].stringValue, "user_id" : UserDefaults.standard.value(forKey: "userId") as! String, "event_id" : albumDetailsDict["event_id"].stringValue, "is_sharable" : "true"]
        }
        
        
        param["document_files"] = [["file_type":dic["file_type"]!,
                                    "url":dic["url"]!,
                                    "file_name":dic["file_name"]!,
                                    "original_name":dic["original_name"]!,
                                    "video_thumb":dic["video_thumb"]!
            ]]
        print(param)
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: param,
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")
        }
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.uploadFile(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    ActivityIndicatorView.hiding()
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        self.callViewContents()
                    }
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                }
            case .failure(let error):
                ActivityIndicatorView.hiding()
                break
            }
        }
        
    }
    
    func updatImageeS3(Arrdic:NSMutableArray){
        
        var param = [String:Any]()
        param = ["folder_id" : albumDetailsDict["id"].stringValue, "user_id" : UserDefaults.standard.value(forKey: "userId") as! String, "group_id" : albumDetailsDict["group_id"].stringValue, "is_sharable" : "true","document_files":Arrdic]
        
        if isFrom == "events"{
            
            param = ["folder_id" : albumDetailsDict["id"].stringValue, "user_id" : UserDefaults.standard.value(forKey: "userId") as! String, "event_id" : albumDetailsDict["event_id"].stringValue, "is_sharable" : "true","document_files":Arrdic]
        }
        
        
       /* param["document_files"] = [["file_type":dic["file_type"]!,
                                    "url":dic["url"]!,
                                    "file_name":dic["file_name"]!,
                                    "original_name":dic["original_name"]!,
                                    "video_thumb":dic["video_thumb"]!
            ]]*/
        
        print(param)
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: param,
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")
        }
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.uploadFile(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    ActivityIndicatorView.hiding()
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        self.callViewContents()
                    }
                    
                }catch let err {
                   print("view contents : \(err)")
                    ActivityIndicatorView.hiding()
                }
            case .failure(let error):
                print("view contents : \(error)")
                ActivityIndicatorView.hiding()
                break
            }
        }
        
    }

    
    //MARK: - Upload Image
    
    func uploadImage(img : UIImage){
        
        var param = [String:Any]()
        var imgData = Data()
        
        var mime    = "image/png"
        
        
        if isFrom == "events"{
            if isFromDocs{
                
                let docFileNamestring = self.docFileName.joined(separator:"-")
                print(docFileNamestring)
                param = ["folder_id" : albumDetailsDict["id"].stringValue, "user_id" : UserDefaults.standard.value(forKey: "userId") as! String, "event_id" : albumDetailsDict["event_id"].stringValue, "is_sharable" : "true","file_name":docFileNamestring,"original_name":docFileNamestring]
                
                mime = pathExpention
                
                
                imgData = self.docData as Data
            }
            else{
                let img2 = img.resized(withPercentage: 0.5)
                
                imgData = (img2?.pngData())!
                
                param = ["folder_id" : albumDetailsDict["id"].stringValue, "user_id" : UserDefaults.standard.value(forKey: "userId") as! String, "event_id" : albumDetailsDict["event_id"].stringValue, "is_sharable" : "true"]
                
            }
            
        }
        else{
            if isFromDocs{
                let docFileNamestring = self.docFileName.joined(separator:"-")
                             print(docFileNamestring)
                param = ["folder_id" : albumDetailsDict["id"].stringValue, "user_id" : UserDefaults.standard.value(forKey: "userId") as! String, "group_id" : albumDetailsDict["group_id"].stringValue, "is_sharable" : "true","file_name":docFileNamestring,"original_name":docFileNamestring]
                mime = pathExpention
                
                imgData = self.docData as Data
            }
            else{
                let img2 = img.resized(withPercentage: 0.5)
                
                imgData = (img2?.pngData())!
                
                param = ["folder_id" : albumDetailsDict["id"].stringValue, "user_id" : UserDefaults.standard.value(forKey: "userId") as! String, "group_id" : albumDetailsDict["group_id"].stringValue, "is_sharable" : "true"]
            }
        }
        print(param)
        ActivityIndicatorView.show("Uploading...")
        let originalData = NSData()
        Helpers.requestWith(fileParamName: "Documents", originalCover: "", urlAppendString: "uploadFile", imageData: imgData, originalImg: originalData as Data, parameters: param, mimeType: mime) { (response) in
            
            print("response : \(response)")
            //ActivityIndicatorView.hiding()
            self.callViewContents()
        }
    }
    
    //MARK:- Call View Contents
    func callViewContents(){
        var parameter = [String:Any]()
        if isFromPost{
            parameter = ["user_id":UserDefaults.standard.value(forKey: "userId") as! String, "folder_id": albumId] as [String:Any]
        }
        else{
             parameter = ["user_id":UserDefaults.standard.value(forKey: "userId") as! String, "folder_id": albumDetailsDict["id"].stringValue] as [String:Any]
        }
        print("Param : \(parameter)")
        DispatchQueue.main.async {
                ActivityIndicatorView.show("Loading....")
        }
        networkProvider.request(.viewContents(parameter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        print("view contents : \(jsonData)")
                        
                        self.dataArr       = jsonData["documents"]
                        self.albumDetailsDict   = jsonData["folder_details"]
                        
                        if self.isFromPost{
                            self.setupUIView()
                        }
                    }
                    self.collVw.reloadData()
                    
                    if self.dataArr.count > 0{
                        self.collVw.isHidden                      = false
                        self.backWhiteVwHeightConstrain.constant  = self.collVw.collectionViewLayout.collectionViewContentSize.height + 500
                        self.lblAddCover.isHidden  = false
                        self.btnAddCover.isEnabled = true
                        // if self.isFromCreate{
                        if jsonData["cover_pic"].stringValue.count > 0{
                            let temp = self.albumDetailsDict["cover_pic"].stringValue
                            let t = temp.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                            
                            let imgurl = URL(string: t!)
                            
                            self.covrUrl = imgurl!.relativeString
                            if self.covrUrl.count == 0{
                                self.btnback.setImage(UIImage(named: "backWhite"), for: .normal)
                                self.noCoverPicVw.isHidden         = false
                            }
                            else{
                                let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=475&height=400&url="+imgurl!.relativeString
                                let url = URL(string: newUrlStr)
                                self.coverImg.kf.indicatorType      = .activity
                                self.coverImg.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "imgNoImage_landscape") , options: nil, progressBlock: nil, completionHandler: nil)
//                                self.grdVew.isHidden = false
                                self.btnback.setImage(UIImage(named: "backWhite"), for: .normal)
                                self.noCoverPicVw.isHidden      = true
                            }
                        }
                        else{
                            self.btnback.setImage(UIImage(named: "backWhite"), for: .normal)
                            self.noCoverPicVw.isHidden         = false
                            //  self.grdVew.isHidden = true
                        }
                        //  }
                    }else{
                        self.collVw.isHidden                      = true
                        self.backWhiteVwHeightConstrain.constant  = self.view.frame.height - 80
                        // self.coverImg = nil
                        self.noCoverPicVw.isHidden = false
                        self.lblAddCover.isHidden = true
                        self.btnAddCover.isEnabled = false
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    //MARK:- Document picker
    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    var pathExpention = String()
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        self.docFileName = []
        print("import result : \(myURL.pathExtension)")
        print("import result : \(myURL.lastPathComponent)")

        pathExpention = myURL.pathExtension
        let audioURLString = "\(myURL)"
        do{
            self.docData = try Data(contentsOf: myURL) as NSData
//            self.docFileName = myURL.lastPathComponent
            self.docFileName.append(myURL.lastPathComponent)
            let img = UIImage()
            self.uploadImage(img: img)
        }
        catch{
            print("Unable to load data: \(error)")
        }
        
        
        // let sendStr = [[audioURLString absoluteString] stringByReplacingOccurrencesOfString:@"file:///private" withString:@""];
        //        let sendStr = audioURLString.replacingOccurrences(of: "file:///private", with: "")
        //        let data = Data()
        //        print(dataArr)
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
    
    //MARK:- collectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.dataArr.count
    }
    
    
    func generateThumbnail(for asset:AVAsset) -> UIImage? {
        let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        let time = CMTimeMake(value: 1, timescale: 2)
        let img = try? assetImgGenerate.copyCGImage(at: time, actualTime: nil)
        if img != nil {
            let frameImg  = UIImage(cgImage: img!)
            return frameImg
        }
        return nil
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collVw.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        
        if isFrom == "events"{
            if isFromDocs{
                if let lblfileName = cell.viewWithTag(6) as? UILabel{
                    let fileName = self.dataArr[indexPath.row]["file_name"].stringValue
                    lblfileName.text = fileName
                    if let vedioBgView = cell.viewWithTag(3){
                        vedioBgView.isHidden = true
                    }
                }
                if let bgView = cell.viewWithTag(7){
                    bgView.isHidden = false
                }
                if let butn = cell.viewWithTag(8) as? subclassedUIButton{
                    butn.indexPath = indexPath.row
                }
            }
        }
            else{
                if isFromDocs{
                    if let lblfileName = cell.viewWithTag(6) as? UILabel{
                        let fileName = self.dataArr[indexPath.row]["original_name"].stringValue     //jeena 01-03-2021 change file_name to original_name
                        lblfileName.text = fileName
                                        
                        if let vedioBgView = cell.viewWithTag(3){
                            vedioBgView.isHidden = true
                        }
                    }
                    if let bgView = cell.viewWithTag(7){
                        bgView.isHidden = false
                    }
                    if let butn = cell.viewWithTag(8) as? subclassedUIButton{
                        butn.indexPath = indexPath.row
                    }
                }
        }
        
        if let img = cell.viewWithTag(1) as? UIImageView{
            
            let tempStr = self.dataArr[indexPath.row]["url"].stringValue
            let t = tempStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            let imgurl                = URL(string: t!) //URL(string: self.dataArr[indexPath.row]["url"].stringValue)
            //            "file_type" : "video\/quicktime\/m4v",
            
            let temp = imgurl!.relativeString
            if  self.covrUrl == temp{
                self.coverPicId = self.dataArr[indexPath.row]["id"].stringValue
            }
            else{
                
            }
            img.kf.indicatorType      = .activity
            
            if isFromDocs{
                let strExt = imgurl?.pathExtension
                
                if (strExt?.contains("pdf"))!{
                    img.kf.setImage(with: imgurl, placeholder: #imageLiteral(resourceName: "PdfImage") , options: nil, progressBlock: nil, completionHandler: nil)
                    
                }else if (strExt?.contains("doc"))!{
                    img.kf.setImage(with: imgurl, placeholder: #imageLiteral(resourceName: "DocumentImage") , options: nil, progressBlock: nil, completionHandler: nil)
                }else if (strExt?.contains("docx"))!{
                    img.kf.setImage(with: imgurl, placeholder: #imageLiteral(resourceName: "DocumentImage") , options: nil, progressBlock: nil, completionHandler: nil)
                    
                }else{
                    img.kf.setImage(with: imgurl, placeholder: #imageLiteral(resourceName: "DocumentTumb") , options: nil, progressBlock: nil, completionHandler: nil)
                }
                
                img.frame = CGRect(x: 0, y: 0, width: cell.frame.size.width, height: cell.frame.size.height - 20)
            }
            else{
                img.frame = CGRect(x: 0, y: 0, width: cell.frame.size.width, height: cell.frame.size.height+20)

                if self.dataArr[indexPath.row]["file_type"].stringValue.contains("image")
                {
                    let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=200&height=200&url="+self.dataArr[indexPath.row]["url"].stringValue.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                    print(newUrlStr)
                    let url = URL(string: newUrlStr)
                    
                    if let img = cell.viewWithTag(4) as? UIImageView{
                        img.isHidden = true
                    }
                    if let vedioBgView = cell.viewWithTag(3){
                        vedioBgView.isHidden = true
                    }
                    img.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "imgNoImage_landscape") , options: nil, progressBlock: nil, completionHandler: nil)
                }
                else
                {
                    //                    let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=200&height=200&gravity=smart&url="+imgurl.relativeString
                    //                    let url = URL(string: newUrlStr)
                    //
                    //                    let asset = AVAsset(url: url!)
                    //
                    //                    if let imgThumbnail = self.generateThumbnail(for: asset)
                    //                    {
                    //                        img.image = imgThumbnail
                    //                        //
                    //                    }
                    //                    else {
                    //                        print("Error: Thumbnail can be generated.")
                    //                        //                                                           return
                    //                    }
                    
                    //                     let temp = "\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+self.dataArr[indexPath.row]["url"].stringValue
                    
                    let temp = self.dataArr[indexPath.row]["url"].stringValue
                    let imgUrl = URL(string: temp)
                    if let img = cell.viewWithTag(4) as? UIImageView{
                        img.isHidden = false
                    }
                    if let vedioBgView = cell.viewWithTag(3){
                        vedioBgView.isHidden = false
                    }
                    img.kf.indicatorType  = .activity
                    //                                                                                                       cell.imgOfPlayVideo.isHidden = false
                    //                                                                                                       cell.viewOfVideoBg.isHidden = false
                    
                    if let thumb = self.dataArr[indexPath.row]["video_thumb"].string,!thumb.isEmpty{
                        
                       let temp = "\(Helpers.imaginaryImageBaseUrl)width=100&height=100&url=\(Helpers.imageURl)\(thumb)"
                        let url = URL.init(string: temp)
                        img.kf.setImage(with: url)
                    }
                    else{
                    
                    let asset = AVAsset(url: imgUrl!)
                    
                    if let imgThumbNail = self.generateThumbnail(for: asset)
                    {
                        img.image = imgThumbNail
                        //
                    }else {
                        print("Error: Thumbnail can be generated.")
                        //                                                           return
                    }
                    
                    }
                    
                }
            }
            
            
         /*   if let vedioBgView = cell.viewWithTag(3){
                       
                       if self.dataArr[indexPath.row]["file_type"].stringValue == "image/png"
                                      {
                           vedioBgView.isHidden = true
                       }
                       else{
                           
                           vedioBgView.isHidden = false

                       }
                       
                   }
                   if let vedioBgimgView = cell.viewWithTag(4) as? UIImageView{
                       if self.dataArr[indexPath.row]["file_type"].stringValue == "image/png"
                                                 {
                                      vedioBgimgView.isHidden = true
                                  }
                                  else{
                                      
                                      vedioBgimgView.isHidden = false

                                  }
                                  
                              }*/
        }
        
        if let bgView = cell.viewWithTag(2){
            
            if arrSelectedIndex.count == 0{
                bgView.isHidden = true
            }
            else{
                
                if arrSelectedIndex.contains(indexPath.row){
                    bgView.isHidden = false
                }
                else{
                    bgView.isHidden = true
                }
            }
            
        }
        
       
                   
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if isFromMultiSelect{
            
            if let index = arrSelectedIndex.firstIndex(of: indexPath.row){
                
                arrSelectedIndex.remove(at: index)
//                dataArr.remove(at: index)

                arrrofUsers.remove(at: index)
            }
            else{
                arrSelectedIndex.append(indexPath.row)
                let data = self.dataArr[indexPath.row]
                arrrofUsers.append(data["id"].stringValue)
            }
            
            self.collVw.reloadItems(at: [indexPath])
//            self.collVw.reloadData()
            if arrSelectedIndex.count == 0{
                self.deleteView.isHidden = true
                isFromMultiSelect = false
                
            }
            
        }
        else{
            if isFromDocs{
                let WebViewViewController = self.storyboard?.instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
                
                let imgurl                   = self.dataArr[indexPath.row]["url"].stringValue
                WebViewViewController.urlStr = imgurl
                
//                let imgurl                   = self.dataArr[indexPath.row]["file_name"].stringValue
//                let temp = "\(Helpers.imageURl)"+"\(BaseUrl.document_image)"+"\(imgurl)"
//                print(temp)
//
//                WebViewViewController.urlStr = temp
                
//                https://familheey-prod.s3.amazonaws.com/Documents/Documents-1580367444356.jpg
                 self.navigationController?.pushViewController(WebViewViewController, animated: true)
                
            }else{
                if self.dataArr[indexPath.row]["file_type"].stringValue.contains("image")
                               {
                let ImageDetailsViewController = self.storyboard?.instantiateViewController(withIdentifier: "ImageDetailsViewController") as! ImageDetailsViewController
                
                ImageDetailsViewController.imgDetails          = self.dataArr[indexPath.row]
                ImageDetailsViewController.dataArr2            = self.dataArr
                ImageDetailsViewController.isFrom2             = isFrom
                ImageDetailsViewController.selectedIndex       = indexPath.row
                ImageDetailsViewController.delegate = self
                ImageDetailsViewController.coverUrl = self.covrUrl
                ImageDetailsViewController.isfromCreate = self.isFromCreate
                ImageDetailsViewController.createdBy = self.createdBy
                self.navigationController?.pushViewController(ImageDetailsViewController, animated: true)
                }
                else
                {
                    
                    //                                 temp = "\(Helpers.imageURl)"+"\(BaseUrl.document_image)"+attachment["filename"].stringValue
                    
                    //                    let temp = self.dataArr[indexPath.row]["url"].stringValue
                    let temp = "\(Helpers.imageURl)"+"\(BaseUrl.document_image)"+self.dataArr[indexPath.row]["file_name"].stringValue
                    
                    let imgUrl = URL(string: temp)
                    
                    let assetURL = imgUrl
                    let playerVC = AVPlayerViewController()
                    let player = AVPlayer(playerItem: AVPlayerItem(url:assetURL!))
                    playerVC.player = player
                    //
                    //                                        picker.dismiss(animated: true, completion: { [weak self] in
                    self.present(playerVC, animated: true, completion: nil)
                    //                                        })
                }
            }
        }
    }
    
    func resolutionForLocalVideo(url: URL) -> CGSize? {
         guard let track = AVURLAsset(url: url).tracks(withMediaType: AVMediaType.video).first else { return nil }
         let size = track.naturalSize.applying(track.preferredTransform)
         return CGSize(width: abs(size.width), height: abs(size.height))
     }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let numberOfCellInRow                   = 3
        let padding : Int                       = 3
        let collectionCellWidth : CGFloat       = (self.collVw.frame.size.width/CGFloat(numberOfCellInRow)-5)
        return CGSize(width: collectionCellWidth, height: collectionCellWidth + 20)
    }
    
    @IBAction func backClicked(_ sender: Any) {
        if self.isFromCreate{
            var array : [UIViewController] = (self.navigationController?.viewControllers)!
            array.remove(at: array.count - 1)
            array.remove(at: array.count - 1)
            self.navigationController?.viewControllers = array
            self.navigationController?.popViewController(animated: true)
            //self.backClicked(self)
        }
        else if isFromPost{
            self.navigationController?.popToViewController(ofClass: HomeTabViewController.self)
        }
        else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
}

extension  AlbumDetailsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker .dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let choosenImage = info[.originalImage] as! UIImage
        selectedImage(img: choosenImage)
        picker.dismiss(animated: true, completion: nil)
    }
}

extension  AlbumDetailsViewController:URLSessionDownloadDelegate{
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        print("downloadLocation:", location)
        guard let url = downloadTask.originalRequest?.url else { return }
//        let documentsPath = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0]
        let documentsUrl:URL =  (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL?)!
        let destinationURL = documentsUrl.appendingPathComponent(url.lastPathComponent)
        // delete original copy
        try? FileManager.default.removeItem(at: destinationURL)
               // copy from temp to Document
        do {
            try FileManager.default.copyItem(at: location, to: destinationURL)
//                self.pdfURL = destinationURL
                print(destinationURL)
            } catch let error {
                print("Copy Error: \(error.localizedDescription)")
        }
        
       /* var error:NSError?

        let iCloudDocumentsURL = FileManager.default.url(forUbiquityContainerIdentifier: nil)?.appendingPathComponent("Documents")

        //is iCloud working?
        if  iCloudDocumentsURL != nil {

            //Create the Directory if it doesn't exist
            if (!FileManager.default.fileExists(atPath: iCloudDocumentsURL!.path, isDirectory: nil)) {
                    //This gets skipped after initial run saying directory exists, but still don't see it on iCloud
//                FileManager.defaultManager.createDirectoryAtURL(iCloudDocumentsURL!, withIntermediateDirectories: true, attributes: nil, error: nil)
                do{
                    try FileManager.default.createDirectory(at: iCloudDocumentsURL!, withIntermediateDirectories: true, attributes: nil)
                }catch {
                    print(error)
                }
            }
        } else {
            print("iCloud is NOT working!")
            //  return
        }

        if ((error) != nil) {
            print("Error creating iCloud DIR")
        }
        
        var isDir:ObjCBool = false
        if (FileManager.default.fileExists(atPath: iCloudDocumentsURL!.path, isDirectory: &isDir)) {
//            FileManager.defaultManager.removeItemAtURL(iCloudDocumentsURL!, error: &error)
            do{
                try  FileManager.default.removeItem(at: iCloudDocumentsURL!)
            }catch{
                print("Catched")
            }
        }

        //copy from my local to iCloud
        do{
            try FileManager.default.copyItem(at: location, to: iCloudDocumentsURL!)
            
        }catch{
            print("catched")
        }
//        if (error == nil && !FileManager.defaultManager.copyItemAtURL(destinationURL, toURL: iCloudDocumentsURL!)) {
//            print(error?.localizedDescription);
//        }*/
    }
}


//MARK:- Download code sample
/*// Create destination URL
let documentsUrl:URL =  (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL?)!
let destinationFileUrl = documentsUrl.appendingPathComponent("\(fileName)")

//Create URL to the source file you want to download
let fileURL = URL(string: urlStr)
let sessionConfig = URLSessionConfiguration.default
let session = URLSession(configuration: sessionConfig)
let request = URLRequest(url:fileURL!)
let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
if let tempLocalUrl = tempLocalUrl, error == nil {
        // Success
    if let statusCode = (response as? HTTPURLResponse)?.statusCode {
        print("Successfully downloaded. Status code: \(statusCode)")
    }
    do {
        try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
            do {
                //Show UIActivityViewController to save the downloaded file
                let contents  = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
                for indexx in 0..<contents.count {
                    if contents[indexx].lastPathComponent == destinationFileUrl.lastPathComponent {
                        let activityViewController = UIActivityViewController(activityItems: [contents[indexx]], applicationActivities: nil)
                         DispatchQueue.main.async {
                            self.present(activityViewController, animated: true, completion: nil)
                        }
                    }
                }
            }
            catch (let err) {
                print("error: \(err)")
            }
        } catch (let writeError) {
            print("Error creating a file \(destinationFileUrl) : \(writeError)")
        }
    } else {
        print("Error took place while downloading a file. Error description: \(error?.localizedDescription ?? "")")
    }
}
task.resume()*/
