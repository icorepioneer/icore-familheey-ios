//
//  AlbumListingViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 26/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

import Moya
import Alamofire
import Kingfisher
import SwiftyJSON

class AlbumListingViewController: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIGestureRecognizerDelegate,UITextFieldDelegate {
    
    //    let loadingDefaultView = UIView()
    
    @IBOutlet weak var backWhiteVw                     : UIView!
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var noAlbumVw                       : UIView!
    @IBOutlet weak var collVw                          : UICollectionView!
    @IBOutlet weak var albumNameLbl                    : UILabel!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearchReset: UIButton!
    @IBOutlet weak var aboutColctnVew: UICollectionView!
    @IBOutlet weak var deleteView: UIView!
    @IBOutlet weak var btnAddAlbum: UIButton!
    
    private var networkProvider                        = MoyaProvider<FamilyheeyApi>()
    
    var dataArr                                        = JSON()
    public var groupID                                 = String()
    var arrSelectedIndex = [Int]()
    var arrrofUsers = [String]()
    
    var famSettings = 0
    var announcementSet = 0
    var link_type = 0
    var isAdmin = ""
    var islinkable:Int = Int()
    var isFromdocument = false
    var facate = ""
    var isFromMultiSelect = false
    
    var aboutArr = ["FEED","ANNOUNCEMENT","REQUEST","EVENTS","ALBUMS","ABOUT US","DOCUMENTS","LINKED FAMILIES","MEMBERS"]
    var ActiveTab = 4
    var postId                          = 0
    var announcementId                  = 0
    var isMembershipActive = false
    var isFromPost = false
    var albumId = ""
    var famArr : [familyModel]?
    var memberJoining = 0
    var invitationStatus = 0
    
    let loadingDefaultView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        self.loadingDefaultView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height:  self.view.frame.size.height)
        //        self.loadingDefaultView.backgroundColor = .white
        if isFromPost{
            print("\(albumId) --- \(groupID)")
            self.loadingDefaultView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height:  self.view.frame.size.height)
            self.loadingDefaultView.backgroundColor = .white
        }
        else{
            if isAdmin.lowercased() == "admin"{
                if isMembershipActive{
                    aboutArr = ["FEED","ANNOUNCEMENT","REQUEST","EVENTS","ALBUMS","ABOUT US","DOCUMENTS","LINKED FAMILIES","MEMBERSHIP","MEMBERS"]
                }
            }
            
            aboutColctnVew.delegate = self
            aboutColctnVew.dataSource = self
            aboutColctnVew.reloadData()
            
            Helpers.setleftView(textfield: txtSearch, customWidth: 30)
            txtSearch.delegate = self
        }
        
        albumNameLbl.text = appDel.groupNamePublic
        let imgUrl = URL(string: appDel.groupImageUrlPublic)
        if imgUrl != nil{
            self.imgCover.kf.indicatorType = .activity
            
            let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=475&height=400&url="+imgUrl!.relativeString
            let urlImg = URL(string: newUrlStr)
            self.imgCover.kf.indicatorType = .activity
            
            self.imgCover.kf.setImage(with: urlImg, placeholder: #imageLiteral(resourceName: "Family Logo"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        else{
            self.imgCover.image = #imageLiteral(resourceName: "family_default_cover.png")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getAllAlbums()
    }
    //    override func viewDidAppear(_ animated: Bool) {
    //        let indexPath = IndexPath(item: ActiveTab, section: 0)
    //        self.aboutColctnVew.scrollToItem(at: indexPath, at:.centeredHorizontally, animated: false)
    //    }
    override func viewDidLayoutSubviews(){
        super.viewDidLayoutSubviews()
        let indexPath = IndexPath(item: ActiveTab, section: 0)
        aboutColctnVew.scrollToItem(at: indexPath, at:.centeredHorizontally, animated: true)
    }
    
    //MARK:- Web API
    func getAllAlbums(){
        var parameter =  [String : Any]()
        if isFromdocument{
            parameter = ["group_id":self.groupID, "user_id":UserDefaults.standard.value(forKey: "userId") as! String, "folder_for":"groups", "folder_type":"documents","txt":txtSearch.text!] as [String : Any]
        }
        else{
            parameter = ["group_id":self.groupID, "user_id":UserDefaults.standard.value(forKey: "userId") as! String, "folder_for":"groups", "folder_type":"albums","txt":txtSearch.text!] as [String : Any]
        }
        
        self.loadingDefaultView.showDefaultAnimation(target:self)
        if self.dataArr.count == 0{
            //            self.loadingDefaultView.showDefaultAnimation(target:self)
            
            ActivityIndicatorView.show("Loading....")
        }
        
        networkProvider.request(.GetAllFoldersGroup(parameter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    
                    if response.statusCode == 200{
                        print("jsonData : \(jsonData)")
                        //                        self.loadingDefaultView.removeFromSuperview()
                        
                        self.dataArr = jsonData["data"]
                        
                        if self.dataArr.count > 0{
                            if self.isFromPost{
                                self.loadingDefaultView.removeFromSuperview()
                                for (index,items) in self.dataArr{
                                    if let folderId = items["id"].int, folderId != nil{
                                        if self.albumId == "\(folderId)"{
                                            let AlbumDetailsViewController = self.storyboard?.instantiateViewController(withIdentifier: "AlbumDetailsViewController") as! AlbumDetailsViewController
                                            AlbumDetailsViewController.albumDetailsDict = items
                                            AlbumDetailsViewController.isFrom = "groups"
                                            AlbumDetailsViewController.isFromPost = true
                                            AlbumDetailsViewController.groupId = self.groupID
                                            AlbumDetailsViewController.folder_type = "albums"
                                            AlbumDetailsViewController.createdBy = items["created_by"].stringValue
                                            self.navigationController?.pushViewController(AlbumDetailsViewController, animated: true)
                                        }
                                    }
                                }
                                
                            }
                            else{
                                self.noAlbumVw.isHidden = true
                                self.collVw.isHidden = false
                                self.collVw.reloadData()
                                
                                let userId = UserDefaults.standard.value(forKey: "userId") as! String
                                let createdId = self.dataArr["created_by"].stringValue
                                if userId == createdId{
                                    self.setupLongPressGesture()
                                }
                                else{
                                    if self.isAdmin.lowercased() == "admin"{
                                        self.setupLongPressGesture()
                                    }
                                }
                            }
                        }
                        else{
                            self.noAlbumVw.isHidden = false
                            self.collVw.isHidden = true
                            self.btnAddAlbum.isHidden = true
                        }
                        
                        print("Data arr : \(self.dataArr)")
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getAllAlbums()
                        }
                    }
                    else{
                        //                        self.loadingDefaultView.removeFromSuperview()
                        
                        self.noAlbumVw.isHidden = false
                        self.collVw.isHidden = true
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    //                    self.loadingDefaultView.removeFromSuperview()
                    
                    self.noAlbumVw.isHidden = false
                    self.collVw.isHidden = true
                    
                    
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                //                self.loadingDefaultView.removeFromSuperview()
                
                self.noAlbumVw.isHidden = false
                self.collVw.isHidden = true
                
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    
    
    //MARK:-Custom Methods
    func setupLongPressGesture() {
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress))
        longPressGesture.minimumPressDuration = 0.5 // 1 second press
        longPressGesture.delegate = self
        self.collVw.addGestureRecognizer(longPressGesture)
    }
    
    @objc func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer){
        if gestureRecognizer.state == .ended {
            let touchPoint = gestureRecognizer.location(in: self.collVw)
            if let indexPath = self.collVw.indexPathForItem(at: touchPoint){
                let cell = collVw.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
                //  let cell = self.collVw.cellForItem(at: indexPath)
                print(indexPath.row)
                // arrSelectedIndex.removeAll()
                self.collVw.allowsMultipleSelection = true
                self.isFromMultiSelect = true
                self.deleteView.isHidden = false
                arrSelectedIndex.append(indexPath.row)
                let data = self.dataArr[indexPath.row]
                arrrofUsers.append(data["id"].stringValue)
                
                
                var indexPaths = [IndexPath]()
                indexPaths.append(indexPath)
                
                if let collectionView = collVw {
                    collectionView.reloadItems(at: indexPaths)
                }
            }
            
        }
    }
    
    func deleteFolder(fId:String){
        var parameter =  [String : Any]()
        parameter = ["id":fId, "is_active":false]
        ActivityIndicatorView.show("Loading....")
        
        networkProvider.request(.updateFolder(paraeter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    
                    if response.statusCode == 200{
                        self.getAllAlbums()
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.deleteFolder(fId: fId)
                        }
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    //MARK:- collectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 2{
            return aboutArr.count
        }
        else{
            return self.dataArr.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 2{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AboutFamilyCollectionViewCell", for: indexPath as IndexPath) as! AboutFamilyCollectionViewCell
            cell.lblName.text = aboutArr[indexPath.item]
            if indexPath.row == ActiveTab{
                cell.lblName.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                cell.lblName.font = .boldSystemFont(ofSize: 15)
                cell.imgUnderline.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                
            }
            else{
                cell.lblName.textColor = .black
                cell.lblName.font = .systemFont(ofSize: 14)
                cell.imgUnderline.backgroundColor = .white
            }
            return cell
        }
        else{
            let cell = collVw.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
            let tempData = self.dataArr[indexPath.row]
            let userId = UserDefaults.standard.value(forKey: "userId") as! String
            let createdId = tempData["created_by"].stringValue
            if userId == createdId{
                self.setupLongPressGesture()
            }
            else{
                if self.isAdmin.lowercased() == "admin"{
                    self.setupLongPressGesture()
                }
            }
            if let img = cell.viewWithTag(1) as? UIImageView{
                if tempData["cover_pic"].stringValue.count != 0 {
                    img.kf.indicatorType   = .activity
                    let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=400&height=300&gravity=smart&url="+tempData["cover_pic"].stringValue.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                    let url = URL(string: newUrlStr)
                    img.kf.indicatorType = .activity
                    
                    img.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                }
                else{
                    img.image = #imageLiteral(resourceName: "imgNoImage")
                }
            }
            
            if let name = cell.viewWithTag(2) as? UILabel{
                if tempData["folder_name"].stringValue.count != 0{
                    name.text = tempData["folder_name"].stringValue
                }
            }
            
            if let hosted = cell.viewWithTag(3) as? UILabel{
                if tempData["created_by_name"].stringValue.count != 0{
                    hosted.text = tempData["created_by_name"].stringValue
                }else{
                    hosted.text = "NA"
                }
            }
            
            if let count = cell.viewWithTag(6) as? UILabel{
                if tempData["doc_count"].stringValue.count != 0{
                    count.text =  tempData["doc_count"].stringValue
                }else{
                    count.text = "0"
                }
            }
            
            if let dat = cell.viewWithTag(4) as? UILabel{
                if tempData["created_at"].stringValue.count != 0{
                    dat.text = convertDateToDisplayDate(dateFromResponse: tempData["created_at"].stringValue)
                }else{
                    dat.text = "NA"
                }
            }
            if let bgView = cell.viewWithTag(5){
                
                if arrSelectedIndex.count == 0{
                    bgView.isHidden = true
                }
                else{
                    if arrSelectedIndex.contains(indexPath.row){
                        bgView.isHidden = false
                    }
                    else{
                        bgView.isHidden = true
                    }
                }
                
            }
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView.tag == 2{
            if indexPath.row == 0{ // Feeds
                attendingClicked(self)
            }
            else if indexPath.row == 1{ // Announcement
                membersClicked(self)
            }
            else if indexPath.row == 2{ // Request
                let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "RequestInFamilyViewController") as! RequestInFamilyViewController
                addMember.groupID = self.groupID
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.facate = self.facate
                addMember.famSettings = famSettings
                addMember.announcementSet = announcementSet
                addMember.postId                    = self.postId
                addMember.announcementId            = self.announcementId
                addMember.famArr = famArr
                addMember.memberJoining = self.memberJoining
                addMember.invitationStatus = self.invitationStatus
                addMember.isMembershipActive = self.isMembershipActive
                self.navigationController?.pushViewController(addMember, animated: false)
            }
            else if indexPath.row == 3{ //Events
                albumsClicked(self)
            }
            else if indexPath.row == 5{ //About us
                let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
                intro.groupId = self.groupID
                intro.isAboutUsClicked = true
                self.navigationController?.pushViewController(intro, animated: false)
            }
            else if indexPath.row == 8{ // Members
                if isMembershipActive{
                    if isAdmin.lowercased() == "admin"{
                        let storyboard = UIStoryboard.init(name: "MembershipMngmnt", bundle: nil)
                        let addMember = storyboard.instantiateViewController(withIdentifier: "MemebrshipDashboardViewController") as! MemebrshipDashboardViewController
                        addMember.groupID = self.groupID
                        addMember.isAdmin = self.isAdmin
                        addMember.link_type = self.link_type
                        addMember.islinkable = self.islinkable
                        //                        addMember.isFromdocument = true
                        addMember.facate = self.facate
                        addMember.famSettings = famSettings
                        addMember.announcementSet = announcementSet
                        addMember.postId                    = self.postId
                        addMember.announcementId            = self.announcementId
                        addMember.isMembershipActive = self.isMembershipActive
                        addMember.famArr = famArr
                        addMember.memberJoining = self.memberJoining
                        addMember.invitationStatus = self.invitationStatus
                        self.navigationController?.pushViewController(addMember, animated: true)
                    }
                    else{
                        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                        let addMember = storyboard.instantiateViewController(withIdentifier: "AddMembersNewViewController") as! AddMembersNewViewController
                        addMember.groupId = self.groupID
                        addMember.isAdmin = self.isAdmin
                        addMember.link_type = self.link_type
                        addMember.islinkable = self.islinkable
                        addMember.faCate = self.facate
                        addMember.famSettings = famSettings
                        addMember.announcementSet = announcementSet
                        addMember.postId                    = self.postId
                        addMember.announcementId            = self.announcementId
                        addMember.isMembershipActive = self.isMembershipActive
                        addMember.famArr = famArr
                        addMember.memberJoiningStatus = self.memberJoining
                        addMember.invitationStatus = self.invitationStatus
                        self.navigationController?.pushViewController(addMember, animated: false)
                    }
                }
                else{
                    let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                    let addMember = storyboard.instantiateViewController(withIdentifier: "AddMembersNewViewController") as! AddMembersNewViewController
                    addMember.groupId = self.groupID
                    addMember.isAdmin = self.isAdmin
                    addMember.link_type = self.link_type
                    addMember.islinkable = self.islinkable
                    addMember.faCate = self.facate
                    addMember.famSettings = famSettings
                    addMember.announcementSet = announcementSet
                    addMember.postId                    = self.postId
                    addMember.announcementId            = self.announcementId
                    addMember.isMembershipActive = self.isMembershipActive
                    addMember.famArr = famArr
                    addMember.memberJoiningStatus = self.memberJoining
                    addMember.invitationStatus = self.invitationStatus
                    self.navigationController?.pushViewController(addMember, animated: false)
                }
            }
            else if indexPath.row == 9{
                let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "AddMembersNewViewController") as! AddMembersNewViewController
                addMember.groupId = self.groupID
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.faCate = self.facate
                addMember.famSettings = famSettings
                addMember.announcementSet = announcementSet
                addMember.postId                    = self.postId
                addMember.announcementId            = self.announcementId
                addMember.isMembershipActive = self.isMembershipActive
                addMember.famArr = famArr
                addMember.memberJoiningStatus = self.memberJoining
                addMember.invitationStatus = self.invitationStatus
                self.navigationController?.pushViewController(addMember, animated: false)
            }
            else if indexPath.row == 4{ //Albums
                aboutUsClicked(self)
            }
            else if indexPath.row == 6{ // Documents
                let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "DocumentListingViewController") as! DocumentListingViewController
                addMember.groupID = self.groupID
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.isFromdocument = true
                addMember.facate = self.facate
                addMember.famSettings = famSettings
                addMember.announcementSet = announcementSet
                addMember.postId                    = self.postId
                addMember.announcementId            = self.announcementId
                addMember.famArr = famArr
                addMember.memberJoining = self.memberJoining
                addMember.invitationStatus = self.invitationStatus
                addMember.isMembershipActive = self.isMembershipActive
                self.navigationController?.pushViewController(addMember, animated: false)
            }
            else{ // Linked families
                let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "LinkedFamiliesListViewController") as! LinkedFamiliesListViewController
                addMember.groupID = self.groupID
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.facate = self.facate
                addMember.famSettings = famSettings
                addMember.announcementSet = announcementSet
                addMember.postId                    = self.postId
                addMember.announcementId            = self.announcementId
                addMember.famArr = famArr
                addMember.memberJoining = self.memberJoining
                addMember.invitationStatus = self.invitationStatus
                addMember.isMembershipActive = self.isMembershipActive
                self.navigationController?.pushViewController(addMember, animated: false)
            }
        }
        else{
            if isFromMultiSelect{
                
                if let index = arrSelectedIndex.firstIndex(of: indexPath.row){
                    
                    arrSelectedIndex.remove(at: index)
                    arrrofUsers.remove(at: index)
                }
                else{
                    arrSelectedIndex.append(indexPath.row)
                    let data = self.dataArr[indexPath.row]
                    arrrofUsers.append(data["id"].stringValue)
                }
                
                self.collVw.reloadData()
                if arrSelectedIndex.count == 0{
                    self.deleteView.isHidden = true
                    isFromMultiSelect = false
                    
                }
                
            }
            else{
                let AlbumDetailsViewController = self.storyboard?.instantiateViewController(withIdentifier: "AlbumDetailsViewController") as! AlbumDetailsViewController
                AlbumDetailsViewController.albumDetailsDict = self.dataArr[indexPath.row]
                AlbumDetailsViewController.isFrom = "groups"
                AlbumDetailsViewController.isAdmin = self.isAdmin
                AlbumDetailsViewController.groupId = self.groupID
                AlbumDetailsViewController.folder_type = "albums"
                AlbumDetailsViewController.createdBy = self.dataArr[indexPath.row]["created_by"].stringValue
                self.navigationController?.pushViewController(AlbumDetailsViewController, animated: true)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 2{
            let w = aboutArr[indexPath.row].size(withAttributes: nil)
            return CGSize(width: w.width, height: 60)
        }
        else{
            let numberOfCellInRow  = 2
            let padding : Int      = 5
            let collectionCellWidth : CGFloat = (self.collVw.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
            return CGSize(width: collectionCellWidth, height: collectionCellWidth * 1.3)
        }
    }
    
    //MARK: - Convert Date
    
    func convertDateToDisplayDate(dateFromResponse:String)->String
    {
        let dateAsString               = dateFromResponse
        let dateFormatter              = DateFormatter()
        dateFormatter.dateFormat       = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        let date = dateFormatter.date(from: dateAsString)
        // print(date as Any)
        //  dateFormatter.dateFormat       = "dd MMM,h:mm a"
        dateFormatter.dateFormat       = "MMM dd yyyy"
        //  dateFormatter.dateStyle       = .short
        //  dateFormatter.timeStyle       = .none
        let DateFormatted = dateFormatter.string(from: date!)
        print(DateFormatted)
        return DateFormatted
    }
    
    //MARK: - IB Actions
    
    @IBAction func membersClicked(_ sender: Any) { // Changed to Announcements tab clicked
        let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
        let addMember = storyboard.instantiateViewController(withIdentifier: "AnnouncementInFamilyViewController") as! AnnouncementInFamilyViewController
        addMember.groupId = self.groupID
        addMember.isAdmin = self.isAdmin
        addMember.link_type = self.link_type
        addMember.islinkable = self.islinkable
        addMember.facate = self.facate
        addMember.famSettings = famSettings
        addMember.announcementSet = announcementSet
        addMember.postId                    = self.postId
        addMember.announcementId            = self.announcementId
        addMember.famArr = famArr
        addMember.memberJoining = self.memberJoining
        addMember.invitationStatus = self.invitationStatus
        addMember.isMembershipActive = self.isMembershipActive
        self.navigationController?.pushViewController(addMember, animated: false)
    }
    
    @IBAction func albumsClicked(_ sender: Any) { // CHANGED to events tab clicked
        //        getAllAlbums()
        let storyboard = UIStoryboard.init(name: "third", bundle: nil)
        let addMember = storyboard.instantiateViewController(withIdentifier: "EventListingViewController") as! EventListingViewController
        addMember.groupID = self.groupID
        addMember.isAdmin = self.isAdmin
        addMember.link_type = self.link_type
        addMember.islinkable = self.islinkable
        addMember.facte = self.facate
        addMember.famSettings = famSettings
        addMember.announcementSet = announcementSet
        addMember.postId                    = self.postId
        addMember.announcementId            = self.announcementId
        addMember.famArr = famArr
        addMember.isMembershipActive = self.isMembershipActive
        //        addMember.faCate = self.familyArr![0].faCategory
        //        addMember.memberJoining = self.familyArr![0].memberApproval
        self.navigationController?.pushViewController(addMember, animated: false)
    }
    
    @IBAction func aboutUsClicked(_ sender: Any) { // Changed to albums clicked
        getAllAlbums()
    }
    
    @IBAction func createAlbumClicked(_ sender: Any) {
        if famSettings == 7{
            if self.isAdmin.lowercased() != "admin"{
                self.displayAlert(alertStr: "You don't have permission", title: "")
                return
            }
        }
        let storyboard = UIStoryboard.init(name: "third", bundle: nil)
        let addMember = storyboard.instantiateViewController(withIdentifier: "CreateAlbumViewController") as! CreateAlbumViewController
        addMember.groupId = self.groupID
        addMember.isFrom =  "groups"
        addMember.isFromDocs = self.isFromdocument
        //        addMember.faCate = self.familyArr![0].faCategory
        //        addMember.memberJoining = self.familyArr![0].memberApproval
        self.navigationController?.pushViewController(addMember, animated: false)
        
    }
    
    @IBAction func attendingClicked(_ sender: Any) { // Changed to Post tab click
        let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
        let addMember = storyboard.instantiateViewController(withIdentifier: "PostedInGroupsViewController") as! PostedInGroupsViewController
        addMember.groupId = self.groupID
        addMember.isAdmin = self.isAdmin
        addMember.link_type = self.link_type
        addMember.islinkable = self.islinkable
        addMember.facate = self.facate
        addMember.famSettings = famSettings
        addMember.announcementSet = announcementSet
        addMember.postId                    = self.postId
        addMember.announcementId            = self.announcementId
        addMember.memberJoining = self.memberJoining
        addMember.invitationStatus = self.invitationStatus
        addMember.famArr = famArr
        addMember.isMembershipActive = self.isMembershipActive
        self.navigationController?.pushViewController(addMember, animated: false)
    }
    
    @IBAction func addAlbumClicked(_ sender: Any) {
        if famSettings == 7{
            if self.isAdmin.lowercased() != "admin"{
                self.displayAlert(alertStr: "You don't have permission", title: "")
                return
            }
        }
        let CreateAlbumViewController = self.storyboard?.instantiateViewController(withIdentifier: "CreateAlbumViewController") as! CreateAlbumViewController
        CreateAlbumViewController.groupId = groupID
        CreateAlbumViewController.isFrom =  "groups"
        CreateAlbumViewController.isFromDocs = self.isFromdocument
        self.navigationController?.pushViewController(CreateAlbumViewController, animated: true)
    }
    
    @IBAction func backClicked(_ sender: Any) {
        
        // self.navigationController?.popToRootViewController(animated: true)
        self.navigationController?.popToViewController(ofClass: FamilyDetailsTableViewController.self)
    }
    
    @IBAction func onClickMore(_ sender: Any) {
        let arr = ["Member","About us","Documents","Linked families"]
        
        self.showActionSheet(titleArr: arr as! NSArray, title: "Choose option") { (index) in
            if index == 3{
                let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "LinkedFamiliesListViewController") as! LinkedFamiliesListViewController
                addMember.groupID = self.groupID
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.facate = self.facate
                // addMember.faCate = self.familyArr![0].faCategory
                //                addMember.memberJoining = self.familyArr![0].memberApproval
                self.navigationController?.pushViewController(addMember, animated: false)
            }
            else if index == 0{
                let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "AddMembersNewViewController") as! AddMembersNewViewController
                addMember.groupId = self.groupID
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.faCate = self.facate
                addMember.islinkable = self.islinkable
                //        addMember.faCate = self.familyArr![0].faCategory
                //        addMember.memberJoining = self.familyArr![0].memberApproval
                self.navigationController?.pushViewController(addMember, animated: false)
            }
            else if index == 1{
                let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
                intro.groupId = self.groupID
                self.navigationController?.pushViewController(intro, animated: false)
            }
            else if index == 100{
            }
            else{
                let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "DocumentListingViewController") as! DocumentListingViewController
                addMember.groupID = self.groupID
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.isFromdocument = true
                addMember.facate = self.facate
                //        addMember.faCate = self.familyArr![0].faCategory
                //        addMember.memberJoining = self.familyArr![0].memberApproval
                self.navigationController?.pushViewController(addMember, animated: false)
                //setActiveButton(index: 2)
            }
        }
    }
    
    @IBAction func onClickDelete(_ sender: Any) {
        let alert = UIAlertController(title: "Familheey", message: "Do you want to delete \(self.arrrofUsers.count) albums", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { _ in
            
            var parameter =  [String : Any]()
            parameter = ["folder_id":self.arrrofUsers,"user_id":UserDefaults.standard.value(forKey: "userId") as! String]
            print(parameter)
            ActivityIndicatorView.show("Loading....")
            
            self.networkProvider.request(.deleteFolder(parameter: parameter)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                        print(jsonData)
                        if response.statusCode == 200{
                            self.arrrofUsers = []
                            self.arrSelectedIndex = []
                            self.deleteView.isHidden = true
                            self.getAllAlbums()
                        }
                        else if response.statusCode == 401 {
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                let btn = UIButton()
                                self.onClickDelete(btn)
                            }
                        }
                        
                        ActivityIndicatorView.hiding()
                        
                    }catch let err {
                        ActivityIndicatorView.hiding()
                        Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                    }
                case .failure( let error):
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                    break
                }
            }
            
        }))
        alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: { _ in
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func onClickSearchReset(_ sender: Any) {
        txtSearch.text = ""
        
        getAllAlbums()
        btnSearchReset.isHidden = true
        self.txtSearch.endEditing(true)
    }
    
    //MARK:- textfield delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        textField.resignFirstResponder()
        if textField == txtSearch
        {
            // selectWebAPI()
            if textField.text!.count > 0{
                btnSearchReset.isHidden = false
            }
            else{
                btnSearchReset.isHidden = true
            }
            textField.endEditing(true)
        }
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        self.view.endEditing(true)
        if textField == txtSearch
        {
            if textField.text!.count > 0{
                btnSearchReset.isHidden = false
            }
            else{
                btnSearchReset.isHidden = true
            }
            getAllAlbums()
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
            // selectWebAPI()
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
        }
        return true
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
