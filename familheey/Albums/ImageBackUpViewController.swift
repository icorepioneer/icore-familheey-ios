//
//  ImageDetailsViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 28/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Moya
import Kingfisher
import SwiftyJSON


class ImageBackUpViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    private var networkProvider              = MoyaProvider<FamilyheeyApi>()
    @IBOutlet weak var imgVw                 : UIImageView!
    
    var imgDetails                           = JSON()
    var dataArr2                             = JSON()
    
    @IBOutlet weak var collVw                : UICollectionView!
    var selectedIndex                        = Int()
    var objCell = UICollectionViewCell()
    
    @IBOutlet weak var changeButt: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let imgurl = URL(string: self.imgDetails["url"].stringValue)
        imgVw.kf.indicatorType = .activity
        
        imgVw.kf.setImage(with: imgurl, placeholder: #imageLiteral(resourceName: "imgNoImage") , options: nil, progressBlock: nil, completionHandler: nil)
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        collVw.scrollToItem(at: IndexPath(row: selectedIndex, section: 1), at: .centeredHorizontally, animated: false)
    }
    
    @IBAction func optionClicked(_ sender: Any) {
        
        let alert = UIAlertController(title: "Image settings", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Make Album Cover", style: .default, handler: { _ in
            
            self.makeCoverPic()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - Make Cover Pic
    
    func makeCoverPic(){
        
        let parameter = ["album_id": dataArr2[selectedIndex]["folder_id"].stringValue, "cover_pic" : dataArr2[selectedIndex]["url"].stringValue]
        
        print("Param : \(parameter)")
        ActivityIndicatorView.show("Loading....")
        
        networkProvider.request(.make_pic_cover(parameter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    
                    if response.statusCode == 200{
                        
                        print("Json data : \(jsonData)")
                        
                        var array : [UIViewController] = (self.navigationController?.viewControllers)!
                        array.remove(at: array.count - 1)
                        array.remove(at: array.count - 1)
                        self.navigationController?.viewControllers = array
                        
                        self.backClicked(self)
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.makeCoverPic()
                        }
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    //MARK:- collectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.dataArr2.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collVw.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        objCell = cell
        if let img = cell.viewWithTag(1) as? UIImageView{
            
            let imgurl                = self.dataArr2[indexPath.row]["url"].url! //URL(string: self.dataArr[indexPath.row]["url"].stringValue)
            img.kf.indicatorType      = .activity
            img.kf.setImage(with: imgurl, placeholder: #imageLiteral(resourceName: "imgNoImage") , options: nil, progressBlock: nil, completionHandler: nil)
        }
        
        if let scrView = cell.viewWithTag(2) as? UIScrollView{
            let img = cell.viewWithTag(1) as? UIImageView
            let minScale = scrView.frame.size.width / img!.frame.size.width;
            scrView.minimumZoomScale = minScale
            scrView.maximumZoomScale = 3.0
            scrView.contentSize = img!.frame.size
            scrView.delegate = self
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplayingCell indexPath: IndexPath) {
        
        if let indd = getIndexPathFor(collection: collVw) as? IndexPath{
            
            print("index path : \(indd)")
            selectedIndex = indd.row
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let numberOfCellInRow                   = 1
        let padding : Int                       = 2
        let collectionCellWidth : CGFloat       = (self.collVw.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
        return CGSize(width: collectionCellWidth, height: collectionCellWidth)
    }
    
    func getIndexPathFor(collection: UICollectionView) -> IndexPath? {
        
        let point = collection.convert(self.view.bounds.origin, from: view)
        let indexPath = collection.indexPathForItem(at: point)
        return indexPath
    }
    
    @IBAction func backClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension ImageBackUpViewController : UIScrollViewDelegate{
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        //let cell = collVw.dequeueReusableCell(withReuseIdentifier: "cell", for: 0) as! UICollectionViewCell
        
        let img = objCell.viewWithTag(1) as? UIImageView
        return img
    }
}
