//
//  CreateAlbumViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 26/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

import Moya
import Alamofire
import Kingfisher
import SwiftyJSON

protocol createAlbumdelegate {
    func callBackFromCreateAlbum(dataArr:[JSON])
    func callBackFromCreateDocument()
}


class CreateAlbumViewController: UIViewController, SelectMembersDelegate {
    
    
    var albumDelegate:createAlbumdelegate!
    @IBOutlet weak var nameField                      : UITextField!
    @IBOutlet weak var descTxtVw                      : UITextView!
    private var networkProvider                       = MoyaProvider<FamilyheeyApi>()
    @IBOutlet weak var lblHead: UILabel!
    @IBOutlet weak var lblAlbumName: UILabel!
    @IBOutlet weak var btnAdd: UIButton!
    //    @IBOutlet weak var lblSettings: UILabel!
    @IBOutlet weak var lblSettings: UITextField!
    
    @IBOutlet weak var tblSettings: UITableView!
    @IBOutlet weak var lblUserCount: UILabel!
    @IBOutlet weak var vewSettings: UIView!
    @IBOutlet weak var lblSettingsTitle: UILabel!
    
    var eventId:String!
    var arraySettings = [String]()
    var groupId:String!
    var isFrom:String!
    var createdBy = ""
    var isAdmin = ""
    
    var isFromDocs = false
    var dataArr = [JSON]()
    
    var isFromEdit = false
    var name = ""
    var desc = ""
    var type = ""
    var folderId = ""
    var permission = "all"
    var selectedUserIDArr                     = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        descTxtVw.textContainerInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 5)
        Helpers.setleftView(textfield: lblSettings , customWidth: 15)
        
        if isFrom.lowercased() == "events"{
            lblSettings.isHidden = true
            vewSettings.isHidden = true
            lblUserCount.isHidden  = true
            lblSettingsTitle.isHidden = true
        }
        else if isFrom.lowercased() == "users"{
            lblSettings.isHidden = false
            vewSettings.isHidden = false
            lblUserCount.isHidden  = true
            lblSettingsTitle.isHidden = true
        }
        else{
            lblSettings.isHidden = false
            vewSettings.isHidden = false
            lblUserCount.isHidden  = false
            lblSettingsTitle.isHidden = false
        }
        if isFromEdit{
            nameField.text = name
            descTxtVw.text = desc
            if isFrom.lowercased() != "users"{
                let userId = UserDefaults.standard.value(forKey: "userId") as! String
                if self.createdBy == userId{
                    if permission.lowercased() == "all"{
                        lblSettings.text = "All members"
                    }
                    else if permission.lowercased() == "only-me"{
                        lblSettings.text = "Only me"
                    }
                    else{
                        lblSettings.text = "Selected members"
                    }
                    
                    if selectedUserIDArr.count > 1{
                        lblUserCount.text = "\(selectedUserIDArr.count) users selected"
                    }
                    else if selectedUserIDArr.count == 1{
                        lblUserCount.text = "\(selectedUserIDArr.count) user selected"
                    }
                    else{
                        lblUserCount.isHidden  = true
                    }
                }
                else{
                    lblSettings.isHidden = true
                    vewSettings.isHidden = true
                    lblUserCount.isHidden  = true
                    lblSettingsTitle.isHidden = true
                }
                
            }
            else
            {
                if permission.lowercased() == "all"{
                    lblSettings.text = "All"
                }
                else if permission.lowercased() == "only-me"{
                    lblSettings.text = "Only me"
                }
                else{
                    lblSettings.text = "My Connections"
                }
                
                
                lblUserCount.isHidden  = true
                
            }
            
            
            if isFromDocs{
                lblHead.text = "Update Folder"
                lblAlbumName.text = "Folder Name"
                lblSettingsTitle.text = "Who can view folder"
                btnAdd.setTitle("Update Folder", for: .normal)
                Helpers.setleftView(textfield: nameField , customWidth: 15)
            }
            else{
                lblHead.text = "Update Albums"
                lblAlbumName.text = "Album Name"
                lblSettingsTitle.text = "Who can view album"
                btnAdd.setTitle("Update Album", for: .normal)
                Helpers.setleftView(textfield: nameField, customWidth: 15)
            }
        }
        else{
            if isFromDocs{
                lblHead.text = "Create Folder"
                lblAlbumName.text = "Folder Name"
                lblSettingsTitle.text = "Who can view folder"
                btnAdd.setTitle("Add Folder", for: .normal)
                Helpers.setleftView(textfield: nameField, customWidth: 15)
            }
            else{
                lblHead.text = "Create Album"
                lblAlbumName.text = "Album Name"
                lblSettingsTitle.text = "Who can view album"
                btnAdd.setTitle("Add Album", for: .normal)
                Helpers.setleftView(textfield: nameField, customWidth: 15)
            }
            if isFrom.lowercased() == "users"{
                lblSettings.text = "All"
                lblUserCount.isHidden  = true
                //                arraySettings = ["All","Only me","Private"]
                
            }
            else
            {
                lblSettings.text = "All members"
                lblUserCount.isHidden  = true
                //                arraySettings = ["All members","Selected members","Only me"]
                
            }
        }
        
        if isFrom.lowercased() == "users"{
            arraySettings = ["Anyone","My Connections","Only me"]
        }
        else
        {
            arraySettings = ["All members","Selected members","Only me"]
            
        }
        
        tblSettings.register(UINib(nibName: "sideLabelTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        tblSettings.delegate = self
        tblSettings.dataSource = self
        
        // Do any additional setup after loading the view.
    }
    //MARK:- Upadate folder
    func updateFolderName(parameter:[String:Any]){
        ActivityIndicatorView.show("Loading....")
        
        networkProvider.request(.updateFolder(paraeter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print(jsonData)
                    if response.statusCode == 200{
                        
                        if self.isFrom.lowercased() == "events"{ // need to be edited
                            if self.isFromDocs{
                                self.navigationController?.popToViewController(ofClass: EventDetailsAddViewController.self)
                            }
                            else{
                                self.navigationController?.popToViewController(ofClass: EventDetailsAddViewController.self)
                            }
                        }
                        else if self.isFrom.lowercased() == "users"{
                            self.navigationController?.popToViewController(ofClass: userAlbumsListViewController.self)
                        }
                        else{
                            if self.isFromDocs{
                                self.navigationController?.popToViewController(ofClass: DocumentListingViewController.self)
                            }
                            else{
                                self.navigationController?.popToViewController(ofClass: AlbumListingViewController.self)
                            }
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.updateFolderName(parameter: parameter)
                        }
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    //MARK:- Delegate
    func selectMembersIds(groupIds: NSMutableArray) {
        selectedUserIDArr = groupIds
        if selectedUserIDArr.count > 1{
            lblUserCount.text = "\(selectedUserIDArr.count) users selected"
        }
        else{
            lblUserCount.text = "\(selectedUserIDArr.count) user selected"
        }
        lblUserCount.isHidden = false
        tblSettings.isHidden = true
        
    }
    
    //MARK:-Button actions
    @IBAction func addAlbum(_ sender: Any) {
        
        if nameField.text! == ""{
            if isFromDocs{
                Helpers.showAlertDialog(message: "Please enter the folder name.", target: self)
                return
            }
            Helpers.showAlertDialog(message: "Please enter the album name.", target: self)
            return
        }
        
        var permission = "all"
        if isFrom.lowercased() == "events"{
            permission = "all"
        }
            
        else if isFrom.lowercased() == "users"{
            if lblSettings.text?.lowercased() == "all"{
                permission = "all"
            }
            else if lblSettings.text?.lowercased() == "only me"{
                permission = "only-me"
            }
            else{
                permission = "private"
            }
            
        }
        else{
//            let userId = UserDefaults.standard.value(forKey: "userId") as! String
//            if self.createdBy == userId{
                if lblSettings.text?.lowercased() == "all members"{
                    permission = "all"
                }
                else if lblSettings.text?.lowercased() == "only me"{
                    permission = "only-me"
                }
                else{
                    permission = "selected"
                }
//            }
        }
        
        
        var parameter = [String:Any]()
        if isFromEdit{
            parameter = ["id":self.folderId, "folder_name":nameField.text!, "description":descTxtVw.text!,"permissions":permission,"users_id":selectedUserIDArr] as [String : Any]
            
        }
        else{
            if isFrom.lowercased() == "events"{
                if isFromDocs{
                    parameter = ["event_id":self.eventId!, "created_by":UserDefaults.standard.value(forKey: "userId") as! String, "folder_for":isFrom!,"folder_type":"documents", "folder_name":nameField.text!, "description":descTxtVw.text!,"permissions":permission,"users_id":selectedUserIDArr] as [String : Any]
                }
                else{
                    parameter = ["event_id":self.eventId!, "created_by":UserDefaults.standard.value(forKey: "userId") as! String, "folder_for":isFrom!,"folder_type":"albums", "folder_name":nameField.text!, "description":descTxtVw.text!,"permissions":permission,"users_id":selectedUserIDArr] as [String : Any]
                }
                
            }
            else if isFrom.lowercased() == "users"{
                if isFromDocs{
                    parameter = ["user_id":UserDefaults.standard.value(forKey: "userId") as! String, "created_by":UserDefaults.standard.value(forKey: "userId") as! String, "folder_for":"users","folder_type":"documents", "folder_name":nameField.text!, "description":descTxtVw.text!,"permissions":permission,"users_id":selectedUserIDArr] as [String : Any]
                }
                else{
                    parameter = ["user_id":UserDefaults.standard.value(forKey: "userId") as! String, "created_by":UserDefaults.standard.value(forKey: "userId") as! String, "folder_for":"users","folder_type":"albums", "folder_name":nameField.text!, "description":descTxtVw.text!,"permissions":permission,"users_id":selectedUserIDArr] as [String : Any]
                }
            }
            else{
                if isFromDocs{
                    parameter = ["group_id":self.groupId!, "created_by":UserDefaults.standard.value(forKey: "userId") as! String, "folder_for":"groups","folder_type":"documents", "folder_name":nameField.text!, "description":descTxtVw.text!,"permissions":permission,"users_id":selectedUserIDArr] as [String : Any]
                }
                else{
                    parameter = ["group_id":self.groupId!, "created_by":UserDefaults.standard.value(forKey: "userId") as! String, "folder_for":"groups","folder_type":"albums", "folder_name":nameField.text!, "description":descTxtVw.text!,"permissions":permission,"users_id":selectedUserIDArr] as [String : Any]
                }
            }
        }
        
        print("params : \(parameter)")
        if isFromEdit{
            updateFolderName(parameter: parameter)
        }
        else{
            ActivityIndicatorView.show("Loading....")
            
            networkProvider.request(.createFolder(parameter: parameter)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                        print(response.statusCode)
                        print(response)
                        if response.statusCode == 200{
                            print(jsonData)
                            self.dataArr  = jsonData["data"].arrayValue
                            
                            if self.isFrom == "events"
                            {
                                if self.isFromDocs{
                                    self.albumDelegate.callBackFromCreateDocument()
                                    self.navigationController?.popViewController(animated: true)
                                }
                                else
                                {
                                    // self.albumDelegate.callBackFromCreateAlbum(dataArr:self.dataArr)
                                    let AlbumDetailsViewController = self.storyboard?.instantiateViewController(withIdentifier: "AlbumDetailsViewController") as! AlbumDetailsViewController
                                    AlbumDetailsViewController.albumDetailsDict = self.dataArr[0]
                                    AlbumDetailsViewController.isFrom = self.isFrom
                                    AlbumDetailsViewController.isFromCreate = true
                                    AlbumDetailsViewController.createdBy = self.dataArr[0]["created_by"].stringValue
                                    self.navigationController?.pushViewController(AlbumDetailsViewController, animated: true)
                                }
                                
                                
                            }
                            else{
                                if self.isFromDocs{
                                    self.backClicked(self)
                                }
                                else
                                {
                                    // self.albumDelegate.callBackFromCreateAlbum(dataArr:self.dataArr)
                                    let AlbumDetailsViewController = self.storyboard?.instantiateViewController(withIdentifier: "AlbumDetailsViewController") as! AlbumDetailsViewController
                                    AlbumDetailsViewController.albumDetailsDict = self.dataArr[0]
                                    AlbumDetailsViewController.isFrom = self.isFrom
                                    AlbumDetailsViewController.isFromCreate = true
                                    AlbumDetailsViewController.createdBy = self.dataArr[0]["created_by"].stringValue
                                    self.navigationController?.pushViewController(AlbumDetailsViewController, animated: true)
                                }
                            }
                        }
                        else if response.statusCode == 401 {
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                let btn = UIButton()
                                self.addAlbum(btn)
                            }
                        }
                        
                        ActivityIndicatorView.hiding()
                    }catch let err {
                        ActivityIndicatorView.hiding()
                        Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                    }
                case .failure( let error):
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                    break
                }
            }
        }
    }
    
    @IBAction func backClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickSettings(_ sender: Any) {
        self.view.endEditing(true)
        if tblSettings.isHidden{
            self.tblSettings.isHidden = false
        }
        else{
            self.tblSettings.isHidden = true
        }
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension CreateAlbumViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arraySettings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellId = "cell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! sideLabelTableViewCell
        
        cell.lblName.text = arraySettings[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isFrom.lowercased() == "users"{
            self.lblSettings.text = arraySettings[indexPath.row]
            self.tblSettings.isHidden = true
        }
        else
        {
            
            
            if arraySettings[indexPath.row].lowercased() == "selected members"{
                self.lblSettings.text = arraySettings[indexPath.row]
                let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "SelectMembersViewController") as! SelectMembersViewController
                //vc.mutialFamilies =  "\(usersList[indexPath.row].familycount ?? "0")"
                //  vc.mutualConnections =  "\(usersList[indexPath.row].mutualfamilycount ?? "0")"
                //        var userId = UserDefaults.standard.value(forKey: "userId") as! String
                //        vc.userID = userId
                vc.groupId = self.groupId
                print(self.selectedUserIDArr)
                vc.selectedUserIDArr = self.selectedUserIDArr
                vc.delegate = self
                
                //            vc.isFromFolder = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else{
                self.lblSettings.text = arraySettings[indexPath.row]
                self.tblSettings.isHidden = true
                self.lblUserCount.isHidden = true
            }
        }
    }
}
