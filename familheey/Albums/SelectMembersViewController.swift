//
//  SelectMembersViewController.swift
//  familheey
//
//  Created by familheey on 02/01/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit
import Alamofire
import Moya
import Kingfisher
import SwiftyJSON
import Firebase

protocol SelectMembersDelegate : class {
    func selectMembersIds(groupIds:NSMutableArray)
}

class SelectMembersViewController: UIViewController {
    @IBOutlet weak var tblMemberView: UITableView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearchReset: UIButton!
    
    var memberArr = [viewMemberDetailsResult]()
    var groupId = ""
    var searchTxt = ""
    var selectedUserIDArr                     = NSMutableArray()
    weak var delegate: SelectMembersDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        Helpers.setleftView(textfield: txtSearch, customWidth: 30)
        txtSearch.delegate = self
        
        getMembers(groupId:groupId)
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Custom Actions
     func getMembers(groupId:String){
        APIServiceManager.callServer.getMembersList(url: EndPoint.viewMember, groupId: groupId, userId: UserDefaults.standard.value(forKey: "userId") as! String, query: txtSearch.text!, success: { (responseMdl) in
                       
        guard let result = responseMdl as? viewFamilyMembersResult else{
            return
        }
        ActivityIndicatorView.hiding()
        if result.status_code == 200{
            self.memberArr = result.joinData!
            if self.memberArr.count > 0{
    //                self.isPrimaryAdmin = self.memberArr[0].isAdmin
    //                self.lblMembersCount.text = "\(self.memberArr.count)"
    //                self.lblAdminsCount.text = "\(result.adminCount)"
    //                self.btnFloat.isHidden = false
                self.tblMemberView.delegate = self
                self.tblMemberView.dataSource = self
                self.tblMemberView.reloadData()
                self.tblMemberView.isHidden = false
                            
    //                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
    //                    self.tblListView.reloadData()
    //                })
            }
            else{
    //                self.lblMembersCount.isHidden = true
    //                self.lblAdminsCount.isHidden = true
    //                self.btnFloat.isHidden = true
            }
        }
                       
        }) { (error) in
                ActivityIndicatorView.hiding()
        }
    }
    
    
    func convertDateToDisplayDate(dateFromResponse:String)->String
    {
        if dateFromResponse != ""
        {
            let dateAsString               = dateFromResponse
            let dateFormatter              = DateFormatter()
            dateFormatter.dateFormat       = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            
            let date = dateFormatter.date(from: dateAsString)
            print(date as Any)
            dateFormatter.dateFormat       = "MMM dd yyyy"
            let DateFormatted = dateFormatter.string(from: date!)
            print(DateFormatted)
            return DateFormatted
        }else
        {
            return ""
        }
    }
    

    //MARK:- Button Actions
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickDone(_ sender: Any) {
        if selectedUserIDArr.count > 0{
            self.delegate?.selectMembersIds(groupIds: selectedUserIDArr)
            self.navigationController?.popViewController(animated: true)
        }
        else{
            self.displayAlert(alertStr: "Please select atleast one member", title: "")
        }
        
    }
    @IBAction func onClickSelect(_ sender: UIButton) {
        if selectedUserIDArr.contains(memberArr[sender.tag].userId){
            
            selectedUserIDArr.remove(memberArr[sender.tag].userId)
        }else{
            selectedUserIDArr.add(memberArr[sender.tag].userId)
        }
        
        print(selectedUserIDArr)
        tblMemberView.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .none)
    }
    @IBAction func onClickSerachReset(_ sender: Any) {
        txtSearch.text = ""
        searchTxt = ""
        getMembers(groupId: groupId)
        btnSearchReset.isHidden = true
        self.txtSearch.endEditing(true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}

extension SelectMembersViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        memberArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectMemberTableViewCell", for: indexPath) as! SelectMemberTableViewCell
        
        cell.btnSelect.tag = indexPath.row
        
        cell.lblName.text = memberArr[indexPath.row].fullname
        cell.lblMemberSince.text = "Member since \(self.convertDateToDisplayDate(dateFromResponse:memberArr[indexPath.row].memberSince))"
        
        if memberArr[indexPath.row].propic.count != 0{
            let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+memberArr[indexPath.row].propic
            let imgUrl = URL(string: temp)
        //                cell.imageViewAvatar.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
            let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+imgUrl!.relativeString
            let url = URL(string: newUrlStr)
            cell.imgProfile.kf.indicatorType = .activity

            cell.imgProfile.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
        }else{
            cell.imgProfile.image = #imageLiteral(resourceName: "Male Colored")
        }
        
        if selectedUserIDArr.contains(memberArr[indexPath.row].userId){
//            header.btnAction.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
            cell.btnSelect.setTitle("Selected", for: .normal)
            
        }else{
            
//            header.btnAction.titleLabel?.font = UIFont.systemFont(ofSize: 15)
             cell.btnSelect.setTitle("Select", for: .normal)
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
}
extension SelectMembersViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
           textField.resignFirstResponder()
           if textField == txtSearch
           {
               // selectWebAPI()
               if textField.text!.count > 0{
                   btnSearchReset.isHidden = false
               }
               else{
                   btnSearchReset.isHidden = true
               }
               textField.endEditing(true)
           }
           
           return true
       }
       func textFieldDidEndEditing(_ textField: UITextField){
           self.view.endEditing(true)
           if textField == txtSearch
           {
               if textField.text!.count > 0{
                   btnSearchReset.isHidden = false
               }
               else{
                   btnSearchReset.isHidden = true
               }
               getMembers(groupId: groupId)
           }
       }
       func textFieldDidBeginEditing(_ textField: UITextField) {
           if textField.text!.count > 0 {
               btnSearchReset.isHidden = false
           }
           else{
               btnSearchReset.isHidden = true
               // selectWebAPI()
           }
       }
       func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
           if textField.text!.count > 0 {
               btnSearchReset.isHidden = false
           }
           else{
               btnSearchReset.isHidden = true
           }
           return true
       }
}

