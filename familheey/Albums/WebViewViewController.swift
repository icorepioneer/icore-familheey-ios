//
//  WebViewViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 31/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import WebKit

class WebViewViewController: UIViewController, UIDocumentInteractionControllerDelegate {
    
    
    @IBOutlet weak var webKitView: WKWebView!
    @IBOutlet weak var activity              : UIActivityIndicatorView!
    var urlStr                               = String()
    var docController:UIDocumentInteractionController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        activity.isHidden = true
//        webVw.delegate = self
        print("urls: \(urlStr)")
        
        urlStr = urlStr.replacingOccurrences(of: " ", with: "")
        
//        let url: URL! = URL(string: urlStr)
        

//        webVw.loadRequest(URLRequest(url: url))
//         webView.navigationDelegate = self

//        self.webKitView.load(request as URLRequest)
       
        if let myURL = URL(string:urlStr) {
            if let data = try? Data(contentsOf: myURL) {
               webKitView.load(data, mimeType: "application/pdf", characterEncodingName: "", baseURL: myURL)
                
            }
        }
        
    }
  
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
           return self
    }
//    
//    func webViewDidStartLoad(_ webView: UIWebView) {
//
//        activity.isHidden = false
//    }
//
//    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
//    {
//     activity.isHidden = true
//    }
//
//    func webViewDidFinishLoad(_ webView: UIWebView)
//    {
//     activity.isHidden = true
//    }

    @IBAction func backClicked(sender : UIButton){
        
        self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


