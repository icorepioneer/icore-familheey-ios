//
//  ImageDetailsViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 28/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Moya
import Kingfisher
import SwiftyJSON
import AVKit

class ImageDetailsCollectionCell : UICollectionViewCell, UIScrollViewDelegate{
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib() {
        self.scrollView.minimumZoomScale = scrollView.frame.size.width / imgView.frame.size.width;
        self.scrollView.maximumZoomScale = 3.5
        self.scrollView.delegate = self
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imgView
    }
    
}

protocol imageDetailDelegate: class {
    func callAfterDelete(removeCover:Bool)
}

class ImageDetailsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    private var networkProvider              = MoyaProvider<FamilyheeyApi>()
    @IBOutlet weak var imgVw                 : UIImageView!
    @IBOutlet weak var grdView: UIView!
    
    @IBOutlet weak var btnMenu: UIButton!
    var imgDetails                           = JSON()
    var dataArr2                             = JSON()
    weak var delegate : imageDetailDelegate?
    @IBOutlet weak var collVw                : UICollectionView!
    var selectedIndex                        = Int()
    var isFrom2                              = ""
    var isFreomRemove                        = false
    var coverUrl = ""
    var isfromCreate = false
    var fromCover = ""
    var createdBy = ""
    var objCell = UICollectionViewCell()
    var attachedIndex = 0
    
    @IBOutlet weak var changeButt: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        collVw.isHidden = true
        let imgurl = URL(string: self.imgDetails["url"].stringValue)
        imgVw.kf.indicatorType = .activity
        
////        imgVw.kf.setImage(with: imgurl, placeholder: #imageLiteral(resourceName: "imgNoImage_landscape") , options: nil, progressBlock: nil, completionHandler: nil)
        
        
        //        let imgurl                   = self.imgDetails["file_name"].stringValue
        
        //below code - new changes
        //            let temp = "\(Helpers.imageURl)"+"\(BaseUrl.document_image)"+"\(self.imgDetails["original_name"].stringValue)"
        //        print(temp)
        //        let imgurl = URL(string: temp)
        //                      print(imgurl)
        //
        //        imgVw.kf.setImage(with: imgurl, placeholder: #imageLiteral(resourceName: "imgNoImage") , options: nil, progressBlock: nil, completionHandler: nil)
        
        // Do any additional setup after loading the view.
        let gradient: CAGradientLayer = CAGradientLayer()
        
        // gradient.colors = [UIColor.init(white: 0, alpha: 3).cgColor, UIColor.init(white: 0, alpha: 0).cgColor]
        gradient.colors = [UIColor.black.cgColor, UIColor.clear.cgColor]
        //      gradient.locations = [0.0,1.0]
        //        gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
        //        gradient.endPoint = CGPoint(x: 1.0, y: 0.0)
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.grdView.frame.size.width+50, height: self.grdView.frame.size.height)
        self.grdView.layer.insertSublayer(gradient, at: 0)
        let userId = UserDefaults.standard.value(forKey: "userId") as! String
        if self.createdBy == userId{
            btnMenu.isHidden = false
        }
        else{
            btnMenu.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        
        //        print("Selected :\(selectedIndex)")
        //
        //        let index = IndexPath(row: self.selectedIndex, section: 0)
        //        collVw.scrollToItem(at: index, at: .centeredHorizontally, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        print("Selected :\(selectedIndex)")
        
        let index = IndexPath(row: self.selectedIndex, section: 0)
        collVw.scrollToItem(at: index, at: .centeredHorizontally, animated: false)
        
        collVw.isHidden = false
        self.tabBarController?.tabBar.isHidden = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        //self.tabBarController?.tabBar.isHidden = false
    }
    
    @IBAction func optionClicked(_ sender: Any) {
        let userId = UserDefaults.standard.value(forKey: "userId") as! String
        if self.createdBy == userId{
            var titleArr = NSArray()
            if self.dataArr2[selectedIndex]["file_type"].stringValue.contains("image"){
                titleArr = ["Make Album Cover", "Remove Photo"]
            }
            else{
                titleArr = ["Make Album Cover", "Remove Video"]
            }
             
            
            showActionSheet(titleArr: titleArr as NSArray, title: "Image Settings") { (index) in
                if index == 0{
                    self.makeCoverPic()
                }
                else if index == 1{
                    self.removePhoto()
                }
                else{
                    
                }
            }
        }
        else{
        }
        //        let alert = UIAlertController(title: "Image settings", message: nil, preferredStyle: .actionSheet)
        //        alert.addAction(UIAlertAction(title: "Make Album Cover, Remove Photo", style: .default, handler: { _ in
        //
        //            self.makeCoverPic()
        //        }))
        //
        //        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        //        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - Make Cover Pic
    
    func makeCoverPic(){
        var parameter = [String:Any]()
        
        if isFreomRemove{
            parameter = ["album_id": dataArr2[selectedIndex]["folder_id"].stringValue, "cover_pic" : ""]
        }
        else{
            if self.dataArr2[selectedIndex]["file_type"].stringValue.contains("image"){
                parameter = ["album_id": dataArr2[selectedIndex]["folder_id"].stringValue, "cover_pic" : dataArr2[selectedIndex]["url"].stringValue]
            }
            else{
                let strUrl = "\(Helpers.imageURl)"+self.dataArr2[selectedIndex]["video_thumb"].stringValue
                parameter = ["album_id": dataArr2[selectedIndex]["folder_id"].stringValue, "cover_pic" : strUrl]
            }
        }
        
        print("Param : \(parameter)")
        ActivityIndicatorView.show("Loading....")
        
        networkProvider.request(.make_pic_cover(parameter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    
                    if response.statusCode == 200{
                        
                        if self.isFreomRemove{
                            self.delegate?.callAfterDelete(removeCover: self.isFreomRemove)
                            self.backClicked(self)
                        }
                        else{
                            if self.isfromCreate{
                                var array : [UIViewController] = (self.navigationController?.viewControllers)!
                                array.remove(at: array.count - 1)
                                array.remove(at: array.count - 1)
                                array.remove(at: array.count - 1)
                                self.navigationController?.viewControllers = array
                            }
                            else{
                                var array : [UIViewController] = (self.navigationController?.viewControllers)!
                                array.remove(at: array.count - 1)
                                array.remove(at: array.count - 1)
                                self.navigationController?.viewControllers = array
                            }
                            
                            self.backClicked(self)
                        }
                        print("Json data : \(jsonData)")
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.makeCoverPic()
                        }
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    func removePhoto(){
        var parameter = [String:Any]()
        var tempArr = [String]()
        tempArr.append(dataArr2[selectedIndex]["id"].stringValue)
        parameter = ["id": tempArr]
        
        let imgurl      = URL(string: dataArr2[selectedIndex]["url"].stringValue)
        let tempStr = imgurl!.relativeString
        
        if self.coverUrl == tempStr{
            isFreomRemove = true
        }
        //        else{
        //
        //        }
        print("Param : \(parameter)")
        ActivityIndicatorView.show("Loading....")
        
        networkProvider.request(.delete_file(parameter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    
                    if response.statusCode == 200{
                        if self.isFreomRemove{
                            self.makeCoverPic()
                        }
                        else{
                            self.delegate?.callAfterDelete(removeCover: self.isFreomRemove)
                            self.backClicked(self)
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.removePhoto()
                        }
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    func downloadDocumentFromUrl(index:Int){
        
        
        if let urlString = self.dataArr2[index]["url"].string, !urlString.isEmpty{
            var temp:String!
            
            temp = self.dataArr2[index]["url"].stringValue
            
            ActivityIndicatorView.show("Please wait downloading.......")
            let fileURL = URL(string: urlString)!
            
            let dateStr = "\(Date().timeIntervalSince1970)"
            let fileName1 = String((fileURL.lastPathComponent)) as NSString
            //        let tempStr = dateStr.trimmingCharacters(in: .whitespacesAndNewlines)
            //        let strArr = tempStr.components(separatedBy: "+")
            let dateArr = dateStr.components(separatedBy: ".")
            var strDateTemp = ""
            if dateArr.count > 0{
                strDateTemp = dateArr[0]
            }
            else{
                strDateTemp = dateStr
            }
            
            let fileName =  strDateTemp + "_" + (fileName1 as String)
            
            // Create destination URL
            let documentsUrl:URL =  (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL?)!
            let destinationFileUrl = documentsUrl.appendingPathComponent("\(fileName)")
            //Create URL to the source file you want to download
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig)
            let request = URLRequest(url:fileURL)
            let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
                if let tempLocalUrl = tempLocalUrl, error == nil {
                    // Success
                    if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                        print("Successfully downloaded. Status code: \(statusCode)")
                    }
                    ActivityIndicatorView.hiding()
                    do {
                        try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                        
                        print("tempLocalUrl : \(tempLocalUrl)")
                        print("destinationFileUrl : \(destinationFileUrl)")
                        
                        do {
                            //Show UIActivityViewController to save the downloaded file
                            let contents  = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
                            for indexx in 0..<contents.count {
                                if contents[indexx].lastPathComponent == destinationFileUrl.lastPathComponent {
                                    
                                    if FileManager.default.fileExists(atPath: destinationFileUrl.absoluteString) {
                                        // Delete file
                                        try FileManager.default.removeItem(atPath: destinationFileUrl.absoluteString)
                                    } else {
                                        print("File does not exist")
                                    }
                                    
                                    DispatchQueue.main.async {
                                        
                                        let activityViewController = UIActivityViewController(activityItems: [contents[indexx]], applicationActivities: nil)
                                        self.present(activityViewController, animated: true, completion: nil)
                                    }
                                    
                                }
                            }
                        }
                        catch (let err) {
                            print("error: \(err)")
                        }
                    } catch (let writeError) {
                        print("Error creating a file \(destinationFileUrl) : \(writeError)")
                    }
                } else {
                    
                    print("Error took place while downloading a file. Error description: \(error?.localizedDescription ?? "")")
                }
            }
            task.resume()
        }
        
        
    }
    
    //MARK:- collectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.dataArr2.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collVw.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ImageDetailsCollectionCell
        
        objCell = cell
        attachedIndex = indexPath.row
        if let img = cell.viewWithTag(1) as? UIImageView{
            var strUrl = ""

                
          
            if self.dataArr2[indexPath.row]["file_type"].stringValue.contains("image"){
                strUrl = "\(Helpers.imageURl)"+"\(BaseUrl.document_image)"+self.dataArr2[indexPath.row]["file_name"].stringValue
                if let imgPlay = cell.viewWithTag(4){
                    imgPlay.isHidden = true
                }
            }
            else if self.dataArr2[indexPath.row]["file_type"].stringValue.contains("video"){
                strUrl = "\(Helpers.imageURl)"+self.dataArr2[indexPath.row]["video_thumb"].stringValue
                if let imgPlay = cell.viewWithTag(4){
                    imgPlay.isHidden = false
                }
            }
            
//            let imgurl                = self.dataArr2[indexPath.row]["url"].url! //URL(string: self.dataArr[indexPath.row]["url"].stringValue)
            let imgurl = URL(string: strUrl)
            
            img.kf.indicatorType      = .activity
            img.kf.setImage(with: imgurl, placeholder: #imageLiteral(resourceName: "imgNoImage_landscape") , options: nil, progressBlock: nil, completionHandler: nil)
        }
        
        if let btnPlay = cell.viewWithTag(6) as? UIButton{
            btnPlay.tag = indexPath.row
        }
        
        
        //  let img = cell.viewWithTag(1) as? UIImageView
        //        let minScale = cell.scrollView.frame.size.width / cell.imgView.frame.size.width;
        //        print(minScale)
        //        cell.scrollView.minimumZoomScale = minScale
        //        cell.scrollView.maximumZoomScale = 3.0
        //        cell.scrollView.contentSize = cell.imgView.frame.size
        //        cell.scrollView.delegate = self
        //
        /*   if let scrView = cell.viewWithTag(2) as? UIScrollView{
         let img = cell.viewWithTag(1) as? UIImageView
         let minScale = scrView.frame.size.width / img!.frame.size.width;
         print(minScale)
         scrView.minimumZoomScale = minScale
         scrView.maximumZoomScale = 3.0
         scrView.contentSize = img!.frame.size
         scrView.delegate = self
         }*/
        
        
        
        if fromCover.lowercased() == "cover"{
            let imgurl                = self.dataArr2[indexPath.row]["url"].url!
            let temp = imgurl.relativeString
            if self.coverUrl == temp{
                if let imgvew = cell.viewWithTag(2) as? UIImageView{
                    imgvew.image = UIImage(named: "Green_tick")
                    imgvew.isHidden = false
                }
            }
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

    }
    
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        findCenterIndex()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let numberOfCellInRow                   = 1
        let padding : Int                       = 0
        let collectionCellWidth : CGFloat       = (self.view.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
        //        return CGSize(width: collectionCellWidth, height: collectionCellWidth)
        
        return CGSize(width: collectionCellWidth, height: self.collVw.frame.height)
        
    }
    
    //MARK: - Get index path
    private func findCenterIndex() {
        let center = self.view.convert(self.collVw.center, to: self.collVw)
        let index = collVw!.indexPathForItem(at: center)
        selectedIndex = index!.row
        print(index ?? "index not found")
    }
    
    
    @IBAction func backClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickDownload(_ sender: Any) {
        downloadDocumentFromUrl(index: attachedIndex)
    }
    
    
    @IBAction func onClickPlayVideo(_ sender: UIButton) {
        if self.dataArr2[sender.tag]["file_type"].stringValue.contains("video"){
            let temp = "\(Helpers.imageURl)"+"\(BaseUrl.document_image)"+self.dataArr2[sender.tag]["file_name"].stringValue
            
            let imgUrl = URL(string: temp)
            
            let assetURL = imgUrl
            let playerVC = AVPlayerViewController()
            let player = AVPlayer(playerItem: AVPlayerItem(url:assetURL!))
            playerVC.player = player
            self.present(playerVC, animated: true, completion: nil)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

//extension ImageDetailsViewController : UIScrollViewDelegate{
//    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
//        //let cell = collVw.dequeueReusableCell(withReuseIdentifier: "cell", for: 0) as! UICollectionViewCell
//        
//        let img = objCell.viewWithTag(1) as? UIImageView
//        return img
//    }
//}

