//
//  DocumentListingViewController.swift
//  familheey
//
//  Created by familheey on 31/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Moya
import Alamofire
import Kingfisher
import SwiftyJSON

class DocumentListingViewController: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIGestureRecognizerDelegate,UITextFieldDelegate{
    
    @IBOutlet weak var backWhiteVw                     : UIView!
    private var networkProvider                        = MoyaProvider<FamilyheeyApi>()
    
    
    @IBOutlet weak var btnCreateDocument: UIButton!
    @IBOutlet weak var lblNoDataHeading2: UILabel!
    
    @IBOutlet weak var btnNoDataImg: UIButton!
    @IBOutlet weak var lblNoDataHeading1: UILabel!
    var dataArr                                        = JSON()
    
    @IBOutlet weak var noAlbumVw                       : UIView!
    @IBOutlet weak var collVw                          : UICollectionView!
    public var groupID                                 = String()
    @IBOutlet weak var albumNameLbl                    : UILabel!
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var deleteView: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearchReset: UIButton!
    @IBOutlet weak var aboutClctnVew: UICollectionView!
    
    var link_type = 0
    var famSettings = 0
    var announcementSet = 0
    var isAdmin = ""
    var islinkable:Int = Int()
    var isFromdocument = false
    var facate = ""
    var arrSelectedIndex = [Int]()
    var arrrofUsers = [String]()
    var isFromMultiSelect = false
    var aboutArr = ["FEED","ANNOUNCEMENT","REQUEST","EVENTS","ALBUMS","ABOUT US","DOCUMENTS","LINKED FAMILIES","MEMBERS"]
    var ActiveTab = 6
    var postId                          = 0
    var announcementId                  = 0
    var isMembershipActive = false
    var famArr : [familyModel]?
    var memberJoining = 0
    var invitationStatus = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isAdmin.lowercased() == "admin"{
            if isMembershipActive{
                aboutArr = ["FEED","ANNOUNCEMENT","REQUEST","EVENTS","ALBUMS","ABOUT US","DOCUMENTS","LINKED FAMILIES","MEMBERSHIP","MEMBERS"]
            }
        }
        aboutClctnVew.delegate = self
        aboutClctnVew.dataSource = self
        aboutClctnVew.reloadData()
        
        Helpers.setleftView(textfield: txtSearch, customWidth: 30)
        txtSearch.delegate = self
        albumNameLbl.text = appDel.groupNamePublic
        
        let imgUrl = URL(string: appDel.groupImageUrlPublic)
        if imgUrl != nil{
            self.imgCover.kf.indicatorType = .activity
            
            let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=475&height=400&url="+imgUrl!.relativeString
            let urlImg = URL(string: newUrlStr)
            self.imgCover.kf.indicatorType = .activity
            
            self.imgCover.kf.setImage(with: urlImg, placeholder: #imageLiteral(resourceName: "Family Logo"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        else{
            self.imgCover.image = #imageLiteral(resourceName: "family_default_cover.png")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getAllAlbums()
    }
    override func viewDidLayoutSubviews(){
        super.viewDidLayoutSubviews()
        let indexPath = IndexPath(item: ActiveTab, section: 0)
        aboutClctnVew.scrollToItem(at: indexPath, at:.centeredHorizontally, animated: true)
    }
    
    //MARK:- Custom Methods
    func setupLongPressGesture() {
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress))
        longPressGesture.minimumPressDuration = 1.0 // 1 second press
        longPressGesture.delegate = self
        self.collVw.addGestureRecognizer(longPressGesture)
    }
    
    @objc func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer){
        if gestureRecognizer.state == .ended {
            let touchPoint = gestureRecognizer.location(in: self.collVw)
            
            /* if let indexPath = self.collVw.indexPathForItem(at: touchPoint) {
             // get the cell at indexPath (the one you long pressed)
             let cell = self.collVw.cellForItem(at: indexPath)
             // do stuff with the cell
             let alert = UIAlertController(title: "Familheey", message: "Do you want to delete this folder", preferredStyle: .alert)
             alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { _ in
             
             let data = self.dataArr[indexPath.row]
             self.deleteFolder(fId: data["id"].stringValue)
             //                 let currentSignUp =
             //
             //                 self.deleteSignupID   = currentSignUp["id"].intValue
             //self.callDeleteSignUpSlotApi()
             }))
             alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: { _ in
             }))
             
             self.present(alert, animated: true, completion: nil)
             } else {
             print("couldn't find index path")
             }*/
            
            if let indexPath = self.collVw.indexPathForItem(at: touchPoint){
                let cell = collVw.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
                //  let cell = self.collVw.cellForItem(at: indexPath)
                print(indexPath.row)
                // arrSelectedIndex.removeAll()
                self.collVw.allowsMultipleSelection = true
                self.isFromMultiSelect = true
                self.deleteView.isHidden = false
                arrSelectedIndex.append(indexPath.row)
                let data = self.dataArr[indexPath.row]
                arrrofUsers.append(data["id"].stringValue)
                // cell?.borderColor = UIColor(white: 0, alpha: 0.25)
                //                cell.contentView.backgroundColor = UIColor.green
                
                var indexPaths = [IndexPath]()
                indexPaths.append(indexPath)
                
                if let collectionView = collVw {
                    collectionView.reloadItems(at: indexPaths)
                }
            }
            
        }
    }
    
    
    //MARK:- Web API
    func getAllAlbums(){
        var parameter =  [String : Any]()
        if isFromdocument{
            parameter = ["group_id":self.groupID, "user_id":UserDefaults.standard.value(forKey: "userId") as! String, "folder_for":"groups", "folder_type":"documents","txt":txtSearch.text!] as [String : Any]
        }
        else{
            parameter = ["group_id":self.groupID, "user_id":UserDefaults.standard.value(forKey: "userId") as! String, "folder_for":"groups", "folder_type":"albums","txt":txtSearch.text!] as [String : Any]
        }
        
        print(parameter)
        if self.dataArr.count == 0{
            ActivityIndicatorView.show("Loading....")
        }
        
        networkProvider.request(.GetAllFoldersGroup(parameter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    
                    if response.statusCode == 200{
                        print("jsonData : \(jsonData)")
                        
                        self.dataArr = jsonData["data"]
                        print(self.dataArr)
                        
                        if self.dataArr.count > 0{
                            self.noAlbumVw.isHidden = true
                            self.collVw.isHidden = false
                            self.collVw.delegate = self
                            self.collVw.dataSource = self
                            self.collVw.reloadData()
                            
                            let userId = UserDefaults.standard.value(forKey: "userId") as! String
                            let createdId = self.dataArr["created_by"].stringValue
                            if userId == createdId{
                                self.setupLongPressGesture()
                            }
                            else{
                                if self.isAdmin.lowercased() == "admin"{
                                    self.setupLongPressGesture()
                                }
                            }
                        }
                        else{
                            self.noAlbumVw.isHidden = false
                            self.collVw.isHidden = true
                            if self.txtSearch.text!.isEmpty{
                                self.lblNoDataHeading1.isHidden = false
                                self.lblNoDataHeading2.isHidden = false
                                self.btnCreateDocument.isHidden = false
                                self.btnNoDataImg.isHidden = false
                                self.lblNoDataHeading1.text = "Haven't created any documents, yet?"
                            }
                            else{
                                self.lblNoDataHeading1.isHidden = false
                                self.lblNoDataHeading1.text = "No results to show"
                                self.lblNoDataHeading2.isHidden = true
                                self.btnCreateDocument.isHidden = true
                                self.btnNoDataImg.isHidden = true
                                
                            }
                            
                        }
                        
                        print("Data arr : \(self.dataArr)")
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getAllAlbums()
                        }
                    }
                    else{
                        self.noAlbumVw.isHidden = false
                        self.collVw.isHidden = true
                        self.lblNoDataHeading1.isHidden = false
                        self.lblNoDataHeading2.isHidden = false
                        self.btnCreateDocument.isHidden = false
                        self.btnNoDataImg.isHidden = false
                        self.lblNoDataHeading1.text = "Haven't created any documents, yet?"
                        
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    self.noAlbumVw.isHidden = false
                    self.collVw.isHidden = true
                    self.lblNoDataHeading1.isHidden = false
                    self.lblNoDataHeading2.isHidden = false
                    self.btnCreateDocument.isHidden = false
                    self.btnNoDataImg.isHidden = false
                    self.lblNoDataHeading1.text = "Haven't created any documents, yet?"
                    
                    
                    
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                
                self.noAlbumVw.isHidden = false
                self.collVw.isHidden = true
                self.lblNoDataHeading1.isHidden = false
                self.lblNoDataHeading2.isHidden = false
                self.btnCreateDocument.isHidden = false
                self.btnNoDataImg.isHidden = false
                self.lblNoDataHeading1.text = "Haven't created any documents, yet?"
                
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    //MARK:- collectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 2{
            return aboutArr.count
        }
        else{
            return self.dataArr.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 2{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AboutFamilyCollectionViewCell", for: indexPath as IndexPath) as! AboutFamilyCollectionViewCell
            cell.lblName.text = aboutArr[indexPath.item]
            if indexPath.row == ActiveTab{
                cell.lblName.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                cell.lblName.font = .boldSystemFont(ofSize: 15)
                cell.imgUnderline.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                
            }
            else{
                cell.lblName.textColor = .black
                cell.lblName.font = .systemFont(ofSize: 14)
                cell.imgUnderline.backgroundColor = .white
            }
            return cell
        }
        else{
            let cell = collVw.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
            let tempData = self.dataArr[indexPath.row]
            
            let userId = UserDefaults.standard.value(forKey: "userId") as! String
            let createdId = tempData["created_by"].stringValue
            if userId == createdId{
                self.setupLongPressGesture()
            }
            else{
                if self.isAdmin.lowercased() == "admin"{
                    self.setupLongPressGesture()
                }
            }
            
            if let img = cell.viewWithTag(1) as? UIImageView{
                if tempData["cover_pic"].stringValue.count != 0 {
                    
//                    img.kf.indicatorType   = .activity
                    img.kf.setImage(with: tempData["cover_pic"].url!, placeholder: #imageLiteral(resourceName: "FolderImg"), options: nil, progressBlock: nil, completionHandler: nil)
                }
                else{
                    img.image = #imageLiteral(resourceName: "FolderImg")
                }
            }
            
            if let hosted = cell.viewWithTag(3) as? UILabel{
                if tempData["created_by_name"].stringValue.count != 0{
                    hosted.text = "By \( tempData["created_by_name"].stringValue)"
                }else{
                    hosted.text = "NA"
                }
            }
            
            if let name = cell.viewWithTag(2) as? UILabel{
                if tempData["folder_name"].stringValue.count != 0{
                    name.text = tempData["folder_name"].stringValue
                }
            }
            
            
            
            if let dat = cell.viewWithTag(4) as? UILabel{
                if tempData["created_at"].stringValue.count != 0{
                    dat.text = convertDateToDisplayDate(dateFromResponse: tempData["created_at"].stringValue)
                }else{
                    
                    dat.text = "NA"
                }
                
            }
            
            if let bgView = cell.viewWithTag(5){
                
                if arrSelectedIndex.count == 0{
                    bgView.isHidden = true
                }
                else{
                    if arrSelectedIndex.contains(indexPath.row){
                        bgView.isHidden = false
                    }
                    else{
                        bgView.isHidden = true
                    }
                }
                
            }
            
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 2{
            if indexPath.row == 0{ // Feeds
                let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "PostedInGroupsViewController") as! PostedInGroupsViewController
                addMember.groupId = self.groupID
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.facate = self.facate
                addMember.famSettings = famSettings
                addMember.announcementSet = announcementSet
                addMember.postId                    = self.postId
                addMember.announcementId            = self.announcementId
                addMember.famArr = famArr
                addMember.memberJoining = self.memberJoining
                addMember.invitationStatus = self.invitationStatus
                addMember.isMembershipActive = self.isMembershipActive
                self.navigationController?.pushViewController(addMember, animated: false)
            }
            else if indexPath.row == 1{// Announcement
                let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "AnnouncementInFamilyViewController") as! AnnouncementInFamilyViewController
                addMember.groupId = self.groupID
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.facate = self.facate
                addMember.famSettings = famSettings
                addMember.announcementSet = announcementSet
                addMember.postId                    = self.postId
                addMember.announcementId            = self.announcementId
                addMember.famArr = famArr
                addMember.memberJoining = self.memberJoining
                addMember.invitationStatus = self.invitationStatus
                addMember.isMembershipActive = self.isMembershipActive
                self.navigationController?.pushViewController(addMember, animated: false)
            }
            else if indexPath.row == 2{ // Request
                let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "RequestInFamilyViewController") as! RequestInFamilyViewController
                addMember.groupID = self.groupID
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.facate = self.facate
                addMember.famSettings = famSettings
                addMember.announcementSet = announcementSet
                addMember.postId                    = self.postId
                addMember.announcementId            = self.announcementId
                addMember.famArr = famArr
                addMember.memberJoining = self.memberJoining
                addMember.invitationStatus = self.invitationStatus
                addMember.isMembershipActive = self.isMembershipActive
                self.navigationController?.pushViewController(addMember, animated: false)
            }
            else if indexPath.row == 3{ //Events
                let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "EventListingViewController") as! EventListingViewController
                addMember.groupID = self.groupID
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.famSettings = famSettings
                addMember.announcementSet = announcementSet
                addMember.facte = self.facate
                addMember.postId                    = self.postId
                addMember.announcementId            = self.announcementId
                addMember.famArr = famArr
                addMember.memberJoining = self.memberJoining
                addMember.invitationStatus = self.invitationStatus
                addMember.isMembershipActive = self.isMembershipActive
                self.navigationController?.pushViewController(addMember, animated: false)
            }
            else if indexPath.row == 5{ //About us
                aboutUsClicked(self)
            }
            else if indexPath.row == 8{ // Members or membership(when turn on via backend)
                if isMembershipActive{
                    if isAdmin.lowercased() == "admin"{
                        let storyboard = UIStoryboard.init(name: "MembershipMngmnt", bundle: nil)
                        let addMember = storyboard.instantiateViewController(withIdentifier: "MemebrshipDashboardViewController") as! MemebrshipDashboardViewController
                        addMember.groupID = self.groupID
                        addMember.isAdmin = self.isAdmin
                        addMember.link_type = self.link_type
                        addMember.islinkable = self.islinkable
                        //                        addMember.isFromdocument = true
                        addMember.facate = self.facate
                        addMember.famSettings = famSettings
                        addMember.announcementSet = announcementSet
                        addMember.postId                    = self.postId
                        addMember.announcementId            = self.announcementId
                        addMember.isMembershipActive = self.isMembershipActive
                        addMember.famArr = famArr
                        addMember.memberJoining = self.memberJoining
                        addMember.invitationStatus = self.invitationStatus
                        self.navigationController?.pushViewController(addMember, animated: true)
                    }
                    else{
                        membersClicked(self)
                    }
                }
                else{
                    membersClicked(self)
                }
            }
            else if indexPath.row == 9{ // Members if membership is true
                membersClicked(self)
            }
            else if indexPath.row == 4{ //Albums
                attendingClicked(self)
            }
            else if indexPath.row == 6{ // Documents
                getAllAlbums()
            }
            else{ // Linked families
                let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "LinkedFamiliesListViewController") as! LinkedFamiliesListViewController
                addMember.groupID = self.groupID
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.facate = self.facate
                addMember.famSettings = famSettings
                addMember.announcementSet = announcementSet
                addMember.postId                    = self.postId
                addMember.announcementId            = self.announcementId
                addMember.famArr = famArr
                addMember.memberJoining = self.memberJoining
                addMember.invitationStatus = self.invitationStatus
                addMember.isMembershipActive = self.isMembershipActive
                self.navigationController?.pushViewController(addMember, animated: false)
            }
        }
        else{
            if isFromMultiSelect{
                
                if let index = arrSelectedIndex.firstIndex(of: indexPath.row){
                    arrSelectedIndex.remove(at: index)
                    arrrofUsers.remove(at: index)
                }
                else{
                    arrSelectedIndex.append(indexPath.row)
                    let data = self.dataArr[indexPath.row]
                    arrrofUsers.append(data["id"].stringValue)
                }
                self.collVw.reloadData()
                if arrSelectedIndex.count == 0{
                    self.deleteView.isHidden = true
                    isFromMultiSelect = false
                }
            }
            else{
                let AlbumDetailsViewController = self.storyboard?.instantiateViewController(withIdentifier: "AlbumDetailsViewController") as! AlbumDetailsViewController
                AlbumDetailsViewController.albumDetailsDict = self.dataArr[indexPath.row]
                AlbumDetailsViewController.isFromDocs = self.isFromdocument
                AlbumDetailsViewController.groupId = self.groupID
                AlbumDetailsViewController.folder_type = "documents"
                AlbumDetailsViewController.createdBy = self.dataArr[indexPath.row]["created_by"].stringValue
                self.navigationController?.pushViewController(AlbumDetailsViewController, animated: true)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 2{
            let w = aboutArr[indexPath.row].size(withAttributes: nil)
            return CGSize(width: w.width, height: 60)
        }
        else{
            let numberOfCellInRow  = 2
            let padding : Int      = 5
            let collectionCellWidth : CGFloat = (self.collVw.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
            return CGSize(width: collectionCellWidth, height: collectionCellWidth * 1.3)
        }
        
    }
    
    //MARK: - Convert Date
    
    func convertDateToDisplayDate(dateFromResponse:String)->String
    {
        let dateAsString               = dateFromResponse
        let dateFormatter              = DateFormatter()
        dateFormatter.dateFormat       = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        let date = dateFormatter.date(from: dateAsString)
        // print(date as Any)
        //  dateFormatter.dateFormat       = "dd MMM,h:mm a"
        dateFormatter.dateFormat       = "MMM dd yyyy"
        //  dateFormatter.dateStyle       = .short
        //  dateFormatter.timeStyle       = .none
        let DateFormatted = dateFormatter.string(from: date!)
        print(DateFormatted)
        return DateFormatted
    }
    
    //MARK: - IB Actions
    
    @IBAction func membersClicked(_ sender: Any) {
        
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let addMember = storyboard.instantiateViewController(withIdentifier: "AddMembersNewViewController") as! AddMembersNewViewController
        addMember.groupId = self.groupID
        addMember.isAdmin = self.isAdmin
        addMember.link_type = self.link_type
        addMember.islinkable = self.islinkable
        addMember.faCate = self.facate
        addMember.famSettings = famSettings
        addMember.announcementSet = announcementSet
        addMember.postId                    = self.postId
        addMember.announcementId            = self.announcementId
        addMember.famArr = famArr
        addMember.memberJoiningStatus = self.memberJoining
        addMember.invitationStatus = self.invitationStatus
        addMember.isMembershipActive = self.isMembershipActive
        self.navigationController?.pushViewController(addMember, animated: false)
    }
    
    @IBAction func documentClicked(_ sender: Any) {
        
    }
    
    @IBAction func aboutUsClicked(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
        intro.groupId = self.groupID
        intro.isAboutUsClicked = true
        self.navigationController?.pushViewController(intro, animated: false)
    }
    
    @IBAction func createAlbumClicked(_ sender: Any) {
        if famSettings == 7{
            if self.isAdmin.lowercased() != "admin"{
                self.displayAlert(alertStr: "You don't have permission", title: "")
                return
            }
        }
        let storyboard = UIStoryboard.init(name: "third", bundle: nil)
        let addMember = storyboard.instantiateViewController(withIdentifier: "CreateAlbumViewController") as! CreateAlbumViewController
        addMember.groupId = self.groupID
        addMember.isFrom =  "groups"
        addMember.isFromDocs = self.isFromdocument
        //        addMember.faCate = self.familyArr![0].faCategory
        //        addMember.memberJoining = self.familyArr![0].memberApproval
        self.navigationController?.pushViewController(addMember, animated: false)
        
    }
    
    @IBAction func attendingClicked(_ sender: Any) {
        
        let storyboard = UIStoryboard.init(name: "third", bundle: nil)
        let addMember = storyboard.instantiateViewController(withIdentifier: "AlbumListingViewController") as! AlbumListingViewController
        addMember.groupID = self.groupID
        addMember.isAdmin = self.isAdmin
        addMember.link_type = self.link_type
        addMember.islinkable = self.islinkable
        addMember.facate = self.facate
        addMember.famSettings = famSettings
        addMember.announcementSet = announcementSet
        addMember.postId                    = self.postId
        addMember.announcementId            = self.announcementId
        addMember.memberJoining = self.memberJoining
        addMember.invitationStatus = self.invitationStatus
        addMember.famArr = self.famArr
        addMember.isMembershipActive = self.isMembershipActive
        self.navigationController?.pushViewController(addMember, animated: false)
    }
    
    @IBAction func addAlbumClicked(_ sender: Any) {
        if famSettings == 7{
            if self.isAdmin.lowercased() != "admin"{
                self.displayAlert(alertStr: "You don't have permission", title: "")
                return
            }
        }
        let CreateAlbumViewController = self.storyboard?.instantiateViewController(withIdentifier: "CreateAlbumViewController") as! CreateAlbumViewController
        CreateAlbumViewController.groupId = groupID
        CreateAlbumViewController.isFrom =  "groups"
        CreateAlbumViewController.isFromDocs = self.isFromdocument
        self.navigationController?.pushViewController(CreateAlbumViewController, animated: true)
    }
    
    @IBAction func backClicked(_ sender: Any) {
        
        // self.navigationController?.popToRootViewController(animated: true)
        self.navigationController?.popToViewController(ofClass: FamilyDetailsTableViewController.self)
    }
    
    @IBAction func onClickMore(_ sender: Any) {
        //        var arr = ["Documents","Linked Families","Post"]
        let arr = ["Feed","Announcement","Events","Linked families"]
        
        self.showActionSheet(titleArr: arr as! NSArray, title: "Choose option") { (index) in
            if index == 2{
                
                let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "EventListingViewController") as! EventListingViewController
                addMember.groupID = self.groupID
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                
                addMember.facte = self.facate
                //                addMember.faCate = self.familyArr![0].faCategory
                //                addMember.memberJoining = self.familyArr![0].memberApproval
                self.navigationController?.pushViewController(addMember, animated: false)
            }
            else if index == 0{
                let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "PostedInGroupsViewController") as! PostedInGroupsViewController
                addMember.groupId = self.groupID
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.facate = self.facate
                self.navigationController?.pushViewController(addMember, animated: false)
                
            }
            else if index == 1{
                let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "AnnouncementInFamilyViewController") as! AnnouncementInFamilyViewController
                addMember.groupId = self.groupID
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.facate = self.facate
                self.navigationController?.pushViewController(addMember, animated: false)
            }
            else if index == 100{
            }
            else{
                let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "LinkedFamiliesListViewController") as! LinkedFamiliesListViewController
                addMember.groupID = self.groupID
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.facate = self.facate
                // addMember.faCate = self.familyArr![0].faCategory
                //                addMember.memberJoining = self.familyArr![0].memberApproval
                self.navigationController?.pushViewController(addMember, animated: false)
            }
        }
    }
    @IBAction func onClickDelete(_ sender: Any) {
        var msg = ""
        if self.arrrofUsers.count == 1
        {
            msg = "Do you want to delete \(self.arrrofUsers.count) folder"
        }
        else
        {
            msg = "Do you want to delete \(self.arrrofUsers.count) folders"
        }
        let alert = UIAlertController(title: "Familheey", message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { _ in
            
            /* let data = self.dataArr[indexPath.row]
             self.deleteFolder(fId: data["id"].stringValue)*/
            var parameter =  [String : Any]()
            parameter = ["folder_id":self.arrrofUsers,"user_id":UserDefaults.standard.value(forKey: "userId") as! String]
            print(parameter)
            ActivityIndicatorView.show("Loading....")
            
            self.networkProvider.request(.deleteFolder(parameter: parameter)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                        print(jsonData)
                        if response.statusCode == 200{
                            self.arrrofUsers = []
                            self.arrSelectedIndex = []
                            self.deleteView.isHidden = true
                            self.getAllAlbums()
                        }
                        else if response.statusCode == 401 {
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                let btn = UIButton()
                                self.onClickDelete(btn)
                            }
                        }
                        
                        ActivityIndicatorView.hiding()
                        
                    }catch let err {
                        ActivityIndicatorView.hiding()
                        Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                    }
                case .failure( let error):
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                    break
                }
            }
            
        }))
        alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: { _ in
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func onClickSearchReset(_ sender: Any) {
        txtSearch.text = ""
        
        getAllAlbums()
        btnSearchReset.isHidden = true
        self.txtSearch.endEditing(true)
    }
    
    
    //MARK:- textfield delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        textField.resignFirstResponder()
        if textField == txtSearch
        {
            // selectWebAPI()
            if textField.text!.count > 0{
                btnSearchReset.isHidden = false
            }
            else{
                btnSearchReset.isHidden = true
            }
            textField.endEditing(true)
        }
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        self.view.endEditing(true)
        if textField == txtSearch
        {
            if textField.text!.count > 0{
                btnSearchReset.isHidden = false
            }
            else{
                btnSearchReset.isHidden = true
            }
            getAllAlbums()
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
            // selectWebAPI()
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
        }
        return true
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
