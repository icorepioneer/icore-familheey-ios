//
//  AddAjendaViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 21/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import CropViewController
import Moya
import SwiftyJSON
import Alamofire

protocol agendaCreationDelegate {
    func successFromAgendaCreation()
}

class AddAjendaViewController: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, CropViewControllerDelegate {
    @IBOutlet weak var imgClose: UIImageView!
    
    @IBOutlet weak var btnImageClose: UIButton!
    @IBOutlet weak var lblTimeHeading: UILabel!
    @IBOutlet weak var lblChooseDateHeading: UILabel!
    @IBOutlet weak var lblSlotHeading: UILabel!
    var imgAgenda = UIImage()
    var imgAgendaData = NSData()
    @IBOutlet weak var imgViewOfAgenda: UIImageView!
    var creationDelegate :agendaCreationDelegate!
    private var networkProvider                        = MoyaProvider<FamilyheeyApi>()
    
    @IBOutlet weak var dayLbl                          : UILabel!
    @IBOutlet weak var dateLbl                         : UILabel!
    @IBOutlet weak var timeLbl                         : UILabel!
    
    @IBOutlet weak var lblNavHeading: UILabel!
    @IBOutlet weak var welcomeSpeechTxtVw              : UITextView!
    @IBOutlet weak var collVw                          : UICollectionView!
    @IBOutlet var doneButt                             : UIButton!
    
    @IBOutlet weak var fromTime                        : UIButton!
    @IBOutlet weak var toTime                          : UIButton!
    @IBOutlet weak var slotTitleField                  : UITextField!
    @IBOutlet weak var descriptionTxtVw                : UITextView!
    
    @IBOutlet weak var backgroundVwHeightConstrain     : NSLayoutConstraint!
    var dateArr                                        = [Date]()
    
    var selectedFromDate                               = Date()
    var ajendaDetails                                  = JSON()
    var eventDetails = JSON()
    
    var ajendaID                                       = Int()
    var eventID                                        = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Ajenda : \(ajendaDetails)")
        print("Event Details : \(eventDetails)")
        self.setTextFiledPlaceholder()
        //        self.imgViewOfAgenda.layer.cornerRadius = 10
        //        self.imgViewOfAgenda.clipsToBounds = true
        if ajendaDetails.count > 0{
            
            ajendaID              = ajendaDetails["id"].intValue
            eventID               = String(ajendaDetails["event_id"].intValue)
            let dat               = stringToDate(str: ajendaDetails["event_date"].stringValue)
            
            print("Date Arr : \(dat)")
            dateArr.append(dat)
            
            Helpers.setleftView(textfield: slotTitleField, customWidth: 15)
            
            selectedFromDate      = dateArr[0]
            
            dayLbl.text           = selectedFromDate.dayOfWeek()!
            
            let formatter         = DateFormatter()
            formatter.dateStyle   = .long
            formatter.dateFormat  = "yyyy-MM-dd HH:mm:ss +0000"
            let result            = formatter.date(from: "\(selectedFromDate)")
            
            let formatter2        = DateFormatter()
            formatter2.dateStyle  = .full
            formatter2.dateFormat = "MMM dd yyyy"
            
            let val               = formatter2.string(from: result!)
            dateLbl.text          = "\(val)"
            
            
            //  dayLbl.text           = val.components(separatedBy: ",").first!
            //dateLbl.text          = "\(val.components(separatedBy: ",")[1]) \(val.components(separatedBy: ",").last!)"
            
            slotTitleField.text   = ajendaDetails["title"].stringValue
            descriptionTxtVw.text = ajendaDetails["description"].stringValue
            
            fromTime.setTitle(Helpers.convert24Hourto12HourFormat(val: ajendaDetails["event_start_time"].stringValue), for: .normal)
            toTime.setTitle(Helpers.convert24Hourto12HourFormat(val: ajendaDetails["event_end_time"].stringValue), for: .normal)
            
            doneButt.setTitle("Update", for: .normal)
            lblNavHeading.text = "Edit Agenda"
            
            let agenda_pic = (ajendaDetails["agenda_pic"].stringValue)
            
            if agenda_pic != ""
            {
                
                self.imgClose.isHidden = false
                self.btnImageClose.isHidden = false
                
                self.imgViewOfAgenda.isHidden = false
                //                self.imgViewOfAgenda.layer.cornerRadius = 5
                //                self.imgViewOfAgenda.clipsToBounds = true
                
                let temp = "\(Helpers.imageURl)"+"\(BaseUrl.agenda_pic)"+agenda_pic
                let imgUrl = URL(string: temp)
                imgViewOfAgenda.kf.indicatorType  = .activity
                //               imgViewOfAgenda.kf.setImage(with: imgUrl, placeholder: nil, options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
                imgViewOfAgenda.kf.setImage(with: imgUrl, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
                
            }
            else
            {
                self.imgViewOfAgenda.isHidden = true
                self.imgClose.isHidden = true
                self.btnImageClose.isHidden = true
                
                
                imgViewOfAgenda.image = nil
                
            }
            
        }else{
            let Fromdate = Date(timeIntervalSince1970: Double(eventDetails["from_date"].stringValue)!)
            print(Fromdate)
            let Todate = Date(timeIntervalSince1970: Double(eventDetails["to_date"].stringValue)!)
            print(Todate)
            
            print(Fromdate)
            
            let dat               = Date.dates(from: Fromdate, to: Todate)
            
            print("Date Arr : \(dat)")
            dateArr               = dat
            
            Helpers.setleftView(textfield: slotTitleField, customWidth: 15)
            
            selectedFromDate      = dateArr[0]
            
            dayLbl.text           = String(selectedFromDate.dayOfWeek()!.prefix(3))
            
            let formatter         = DateFormatter()
            formatter.dateStyle   = .long
            formatter.dateFormat  = "yyyy-MM-dd HH:mm:ss +0000"
            let result            = formatter.date(from: "\(selectedFromDate)")
            
            let formatter2        = DateFormatter()
            formatter2.dateStyle  = .long
            formatter2.dateFormat = "MMM dd yyyy"
            
            let val               = formatter2.string(from: result!)
            print("Val :\(val)")
            
            //  dayLbl.text           = String(val.components(separatedBy: ",").first!.prefix(3))
            
            //            let str               = val.components(separatedBy: " ")[1].prefix(3)
            
            //            dateLbl.text          = "\(val.components(separatedBy: " ")[0]) \(str), \(val.components(separatedBy: " ").last!)"
            dateLbl.text          = "\(val)"
            
            
            doneButt.setTitle("Done", for: .normal)
            lblNavHeading.text = "Create Agenda"
            self.imgViewOfAgenda.isHidden = true
            self.imgClose.isHidden = true
            self.btnImageClose.isHidden = true
            
        }
        
        backgroundVwHeightConstrain.constant = 820
        
        if DeviceType.IS_IPHONE_6{
            
            backgroundVwHeightConstrain.constant = 700
        }else if DeviceType.IS_IPHONE_6P{
            
            backgroundVwHeightConstrain.constant = 750
        }
        
        
        
        
        
        // timeLbl.text          = "\(formatter3.string(from: result!)) \n\(formatter3.string(from: result2!))"
        
        // Do any additional setup after loading the view.
    }
    
    func setTextFiledPlaceholder()
    {
        
        self.lblChooseDateHeading.attributedText = Helpers.setAttributedPlaceHolder(placeHolderName: "Choose a date")
        self.lblTimeHeading.attributedText = Helpers.setAttributedPlaceHolder(placeHolderName: "Time")
        self.lblSlotHeading.attributedText = Helpers.setAttributedPlaceHolder(placeHolderName: "Title")
        
        
        
    }
    //MARK: - String to date
    
    func stringToDate(str : String) -> Date{
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = .current
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: str)
        return date!
    }
    
    //MARK:- collectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return dateArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        
        if let vw = cell.viewWithTag(1) as? UIView{
            
            if selectedFromDate == dateArr[indexPath.row]{
                vw.layer.borderColor       = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                vw.layer.borderWidth       = 2
            }else{
                vw.layer.borderColor       = UIColor.lightGray.cgColor
                vw.layer.borderWidth       = 1
            }
            
        }
        
        
        if let lbl2 = cell.viewWithTag(2) as? UILabel{
            lbl2.text = dateArr[indexPath.row].nameOfMonth()!.uppercased()
        }
        
        if let lbl3 = cell.viewWithTag(3) as? UILabel{
            
            lbl3.text = dateArr[indexPath.row].dateOfMonth()!
        }
        
        if let lbl4 = cell.viewWithTag(4) as? UILabel{
            
            lbl4.text = dateArr[indexPath.row].dayOfWeek()!.uppercased()
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print(dateArr[indexPath.row])
        
        selectedFromDate = dateArr[indexPath.row]
        
        collVw.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let numberOfCellInRow  = 4
        let padding : Int      = 10
        let collectionCellWidth : CGFloat = (self.collVw.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
        return CGSize(width: collectionCellWidth, height: 60)
    }
    
    //MARK: - Button Actions
    
    var checkButt      = 1
    
    @IBAction func timeClicked(_ sender: UIButton) {
        
        if sender == fromTime{
            toTime.setTitle("To", for: .normal)
            
            checkButt = 1
        }else{
            if fromTime.title(for: .normal) == "From"
            {
                Helpers.showAlertDialog(message: "Please select From Time", target: self)
                
            }
            checkButt = 2
        }
        
        let vc = SambagTimePickerViewController()
        vc.theme = .light
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func addClicked(_ sender: Any) {
    }
    
    
    
    @IBAction func uploadImage(_ sender: Any) {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    //MARK: - Convert date
    
    func convertDateFormat(date:String)->String
    {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"
        //        let convertedDate = formatter.date(from: "16-09-2017 2:00 PM")
        let convertedDate = formatter.date(from: date)
        
        formatter.timeZone = TimeZone(identifier: "UTC")
        let date = formatter.string(from: convertedDate!)
        print(date)
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let convertedDate2 = formatter.date(from: date)
        formatter2.timeZone = TimeZone.current
        let date2 = formatter2.string(from: convertedDate2!)
        print(date2)
        
        //        let timestamp = convertedDate2!.timeIntervalSince1970
        //        print(timestamp)
        
        return date2
    }
    
    //MARK:- Custom Methods
    
    var imagePicker = UIImagePickerController()
    
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else{
            // self.displayAlert(alertStr: "You don't have camera", title: "")
        }
    }
    
    func openGallary(){
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func selectedImage(img:UIImage){
        // let cropView = CropViewController(image: img)
        let cropView = CropViewController(croppingStyle: .default, image: img)
        cropView.delegate = self
        self.navigationController?.pushViewController(cropView, animated: true)
    }
    
    //MARK:- CropView SDK Delegate
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        // 'image' is the newly cropped version of the original image
        
        imgAgenda = image
        self.imgClose.isHidden = false
        self.btnImageClose.isHidden = false
        self.imgViewOfAgenda.isHidden = false
        imgViewOfAgenda.image = image
        
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Done Action
    
    
    @IBAction func closeImage_OnClick(_ sender: Any) {
        
        self.imgViewOfAgenda.image = nil
        //        imgAgenda = n
        self.imgViewOfAgenda.isHidden = true
        self.imgClose.isHidden = true
        self.btnImageClose.isHidden = true
        
    }
    
    @IBAction func doneClicked(_ sender: Any) {
        
        self.view.endEditing(true)
        
        print("From date : \(selectedFromDate)")
        
        if doneButt.titleLabel?.text == "Done"{
            
            let image = self.imgViewOfAgenda.image ?? nil
            if image == nil
            {
                callCreateAjendaApi()
                
            }
            else
            {
                callCreateAjendaApiwithImage()
            }
        }else{
            let image = self.imgViewOfAgenda.image ?? nil
            if image == nil
            {
                callUpdateAjendaApi()
            }
            else
            {
                
                callUpdateAjendaApiwithImage()
            }
        }
        
    }
    //MARK: - Create Ajenda
    
    func callCreateAjendaApi(){
        
        if slotTitleField.text == ""{
            Helpers.showAlertDialog(message: "Please enter slot title", target: self)
            
            return
        }
        if fromTime.title(for: .normal) == "From"{
            Helpers.showAlertDialog(message: "Please select from time", target: self)
            return
        }
        
        if toTime.title(for: .normal) == "To"{
            Helpers.showAlertDialog(message: "Please to from time", target: self)
            return
        }
        
        
        let dateNoTime         = Helpers.dateWithOutTime(datDate: selectedFromDate)
        let time               = Helpers.convert12Hourto24HourFormat(val: fromTime.titleLabel!.text!)
        let hour               = Int(time.components(separatedBy: ":").first!)
        let minute             = Int(time.components(separatedBy: ":")[1])
        let firstDate          = dateNoTime.adding(hour: hour!, minutes: minute!)
        
        
        let dateNoTime2        = Helpers.dateWithOutTime(datDate: selectedFromDate)
        let time2              = Helpers.convert12Hourto24HourFormat(val: toTime.titleLabel!.text!)
        let hour2              = Int(time2.components(separatedBy: ":").first!)
        let minute2            = Int(time2.components(separatedBy: ":")[1])
        let secondDate         = dateNoTime2.adding(hour: hour2!, minutes: minute2!)
        ActivityIndicatorView.show("Loading....")
        
        let parameter: Parameters = ["user_id" : UserDefaults.standard.value(forKey: "userId") as! String, "event_id" : eventID, "title" : slotTitleField.text!, "start_date" : "\(firstDate)", "end_date" : "\(secondDate)","description":descriptionTxtVw.text!]
        
        
        
        networkProvider.request(.create_agenda(parameter: parameter), completion: {(result) in
            switch result {
            case .success(let response):
                do {
                    
                    if response.statusCode == 200{
                        
                        ActivityIndicatorView.hiding()
                        
                        let jsonData = JSON(response.data)
                        
                        print("Json data : \(jsonData)")
                        let AlertCus = CustomAlertVC.show(in: self, title: "Success", message: "You have successfully created ajenda", image:UIImage.init(named: "Green_tick")!)
                        AlertCus.addAction(CPAlertAction.init(title: "OK", type: .normal, handler: {
                            self.creationDelegate.successFromAgendaCreation()
                            self.navigationController?.popViewController(animated: true)
                        }))
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.callCreateAjendaApi()
                        }
                    }
                    else{
                        ActivityIndicatorView.hiding()
                    }
                    
                } catch let err {
                    ActivityIndicatorView.hiding()
                    print(err)
                }
                
            case .failure(let error):
                ActivityIndicatorView.hiding()
                break
            }
        })
    }
    
    func callCreateAjendaApiwithImage(){
        
        if slotTitleField.text == ""{
            Helpers.showAlertDialog(message: "Please enter slot title", target: self)
            
            return
        }
        if fromTime.title(for: .normal) == "From"{
            Helpers.showAlertDialog(message: "Please select from time", target: self)
            return
        }
        
        if toTime.title(for: .normal) == "To"{
            Helpers.showAlertDialog(message: "Please to from time", target: self)
            return
        }
        
        
        let dateNoTime         = Helpers.dateWithOutTime(datDate: selectedFromDate)
        let time               = Helpers.convert12Hourto24HourFormat(val: fromTime.titleLabel!.text!)
        let hour               = Int(time.components(separatedBy: ":").first!)
        let minute             = Int(time.components(separatedBy: ":")[1])
        let firstDate          = dateNoTime.adding(hour: hour!, minutes: minute!)
        
        
        let dateNoTime2        = Helpers.dateWithOutTime(datDate: selectedFromDate)
        let time2              = Helpers.convert12Hourto24HourFormat(val: toTime.titleLabel!.text!)
        let hour2              = Int(time2.components(separatedBy: ":").first!)
        let minute2            = Int(time2.components(separatedBy: ":")[1])
        let secondDate         = dateNoTime2.adding(hour: hour2!, minutes: minute2!)
        
        let image = self.imgViewOfAgenda.image ?? nil
        if image == nil
        {
            self.imgAgendaData = NSData()
        }else
        {
            self.imgAgendaData = self.imgViewOfAgenda.image?.jpegData(compressionQuality: 0.5)! as! NSData
        }
        ActivityIndicatorView.show("Loading....")
        
        let parameter: Parameters = ["user_id" : UserDefaults.standard.value(forKey: "userId") as! String, "event_id" : eventID, "title" : slotTitleField.text!, "start_date" : "\(firstDate)", "end_date" : "\(secondDate)","description":descriptionTxtVw.text!]
        
        let originalData = NSData()
        Helpers.requestWith(fileParamName: "agenda_pic", originalCover: "",urlAppendString: "create_agenda", imageData: self.imgAgendaData as Data, originalImg: originalData as Data, parameters: parameter, mimeType: "image/png") { (response) in
            
            print("response : \(response)")
            print("response : \(response["data"])")
            
            if response["data"] != nil{
                
                let AlertCus = CustomAlertVC.show(in: self, title: "Success", message: "Successfully created Agenda", image:UIImage.init(named: "Green_tick")!)
                AlertCus.addAction(CPAlertAction.init(title: "OK", type: .normal, handler: {
                    self.creationDelegate.successFromAgendaCreation()
                    self.navigationController?.popViewController(animated: true)
                    
                    
                }))
                
            }
            else
            {
                
                Helpers.showAlertDialog(message: response["name"].string ?? "", target: self)
            }
            ActivityIndicatorView.hiding()
        }
    }
    //MARK: - Update Ajenda
    
    func callUpdateAjendaApiwithImage(){
        
        if slotTitleField.text == ""{
            Helpers.showAlertDialog(message: "Please enter slot title", target: self)
            
            return
        }
        if fromTime.title(for: .normal) == "From"{
            Helpers.showAlertDialog(message: "Please select from time", target: self)
            return
        }
        
        if toTime.title(for: .normal) == "To"{
            Helpers.showAlertDialog(message: "Please to from time", target: self)
            return
        }
        
        
        ActivityIndicatorView.show("Loading....")
        
        
        let dateNoTime         = Helpers.dateWithOutTime(datDate: selectedFromDate)
        let time               = Helpers.convert12Hourto24HourFormat(val: fromTime.titleLabel!.text!)
        let hour               = Int(time.components(separatedBy: ":").first!)
        let minute             = Int(time.components(separatedBy: ":")[1])
        let firstDate          = dateNoTime.adding(hour: hour!, minutes: minute!)
        
        
        let dateNoTime2        = Helpers.dateWithOutTime(datDate: selectedFromDate)
        let time2              = Helpers.convert12Hourto24HourFormat(val: toTime.titleLabel!.text!)
        let hour2              = Int(time2.components(separatedBy: ":").first!)
        let minute2            = Int(time2.components(separatedBy: ":")[1])
        let secondDate         = dateNoTime2.adding(hour: hour2!, minutes: minute2!)
        
        ActivityIndicatorView.show("Loading....")
        
        
        self.imgAgendaData = self.imgViewOfAgenda.image?.jpegData(compressionQuality: 0.5)! as! NSData
        let parameter: Parameters = ["agenda_id" : "\(ajendaID)", "title" : slotTitleField.text!, "description" : descriptionTxtVw.text!, "event_id" : "\(eventID)","start_date" : "\(firstDate)", "end_date" : "\(secondDate)"]
        print("parameters are : \(parameter)")
        
        let originalData = NSData()
        
        Helpers.requestWith(fileParamName: "agenda_pic", originalCover: "",urlAppendString: "update_agenda", imageData: self.imgAgendaData as Data, originalImg: originalData as Data, parameters: parameter, mimeType: "image/png") { (response) in
            
            if response["data"] != nil{
                
                let AlertCus = CustomAlertVC.show(in: self, title: "Success", message: "Successfully Updated Agenda", image:UIImage.init(named: "Green_tick")!)
                AlertCus.addAction(CPAlertAction.init(title: "OK", type: .normal, handler: {
                    self.creationDelegate.successFromAgendaCreation()
                    self.navigationController?.popViewController(animated: true)
                }))
            }
            else
            {
                Helpers.showAlertDialog(message: response["name"].string ?? "", target: self)
            }
            ActivityIndicatorView.hiding()
        }
    }
    
    func callUpdateAjendaApi(){
        
        if slotTitleField.text == ""{
            Helpers.showAlertDialog(message: "Please enter slot title", target: self)
            
            return
        }
        if fromTime.title(for: .normal) == "From"{
            Helpers.showAlertDialog(message: "Please select from time", target: self)
            return
        }
        
        if toTime.title(for: .normal) == "To"{
            Helpers.showAlertDialog(message: "Please to from time", target: self)
            return
        }
        
        
        ActivityIndicatorView.show("Loading....")
        
        
        let dateNoTime         = Helpers.dateWithOutTime(datDate: selectedFromDate)
        let time               = Helpers.convert12Hourto24HourFormat(val: fromTime.titleLabel!.text!)
        let hour               = Int(time.components(separatedBy: ":").first!)
        let minute             = Int(time.components(separatedBy: ":")[1])
        let firstDate          = dateNoTime.adding(hour: hour!, minutes: minute!)
        
        
        let dateNoTime2        = Helpers.dateWithOutTime(datDate: selectedFromDate)
        let time2              = Helpers.convert12Hourto24HourFormat(val: toTime.titleLabel!.text!)
        let hour2              = Int(time2.components(separatedBy: ":").first!)
        let minute2            = Int(time2.components(separatedBy: ":")[1])
        let secondDate         = dateNoTime2.adding(hour: hour2!, minutes: minute2!)
        
        ActivityIndicatorView.show("Loading....")
        
        let parameter: Parameters = ["agenda_id" : "\(ajendaID)", "title" : slotTitleField.text!, "description" : descriptionTxtVw.text!, "event_id" : "\(eventID)","start_date" : "\(firstDate)", "end_date" : "\(secondDate)","agenda_pic":""]
        print("parameters are : \(parameter)")
        
        networkProvider.request(.update_agenda(parameter: parameter), completion: {(result) in
            
            print(result)
            
            switch result {
                
            case .success(let response):
                do {
                    
                    if response.statusCode == 200{
                        ActivityIndicatorView.hiding()
                        let jsonData = JSON(response.data)
                        let AlertCus = CustomAlertVC.show(in: self, title: "Success", message: "You have successfully updated ajenda", image:UIImage.init(named: "Green_tick")!)
                        AlertCus.addAction(CPAlertAction.init(title: "OK", type: .normal, handler: {
                            self.creationDelegate.successFromAgendaCreation()
                            self.navigationController?.popViewController(animated: true)
                        }))
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.callUpdateAjendaApi()
                        }
                    }
                    else{
                        ActivityIndicatorView.hiding()
                    }
                    
                } catch let err {
                    ActivityIndicatorView.hiding()
                    print(err)
                }
                
            case .failure(let error):
                ActivityIndicatorView.hiding()
                break
            }
        })
    }
    
    @IBAction func backClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension AddAjendaViewController: SambagTimePickerViewControllerDelegate{
    func sambagTimePickerDidSet(_ viewController: SambagTimePickerViewController, result: SambagTimePickerResult) {
        
        print("Time : \(result)")
        
        
        
        if checkButt == 1{
            
            fromTime.setTitle(result.description, for: .normal)
        }else{
            toTime.setTitle(result.description, for: .normal)
            print(toTime.titleLabel!.text!)
            let dateNoTime         = Helpers.dateWithOutTime(datDate: selectedFromDate)
            let time               = Helpers.convert12Hourto24HourFormat(val: fromTime.titleLabel!.text!)
            let hour               = Int(time.components(separatedBy: ":").first!)
            let minute             = Int(time.components(separatedBy: ":")[1])
            let firstDate          = dateNoTime.adding(hour: hour!, minutes: minute!)
            print(firstDate)
            
            let dateNoTime2        = Helpers.dateWithOutTime(datDate: selectedFromDate)
            let time2              = Helpers.convert12Hourto24HourFormat(val: toTime.titleLabel!.text!)
            let hour2              = Int(time2.components(separatedBy: ":").first!)
            let minute2            = Int(time2.components(separatedBy: ":")[1])
            let secondDate         = dateNoTime2.adding(hour: hour2!, minutes: minute2!)
            if !(firstDate.compare(secondDate) == .orderedAscending)
            {
                self.showToast(message: "Please Select To time Higher than From time ")
                toTime.setTitle("To", for: .normal)
                
            }
            
        }
        print("date : \(Date())")
        
        
        
        
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func sambagTimePickerDidCancel(_ viewController: SambagTimePickerViewController) {
        if checkButt == 1{
            fromTime.setTitle("From", for: .normal)
            
        }
        else
        {
            toTime.setTitle("To", for: .normal)
            
        }
        
        viewController.dismiss(animated: true, completion: nil)
    }
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: 20, y: self.view.frame.size.height/2-30, width: self.view.frame.width-40, height: 60))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.numberOfLines = 2
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 10.0)
        toastLabel.adjustsFontSizeToFitWidth = true
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 6.0, delay: 0.3, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}

extension  AddAjendaViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker .dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let choosenImage = info[.originalImage] as! UIImage
        selectedImage(img: choosenImage)
        picker.dismiss(animated: true, completion: nil)
        
    }
}
