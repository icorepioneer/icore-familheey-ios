//
//  EventShareViewController.swift
//  familheey
//
//  Created by familheey on 23/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Moya
import Alamofire
import SwiftyJSON

class OtherInviteCell: UITableViewCell{
    
    @IBOutlet weak var nameField              : UITextField!
    @IBOutlet weak var phoneNumberField       : UITextField!
    @IBOutlet weak var phoneCodeField         : UITextField!
    @IBOutlet weak var emailField             : UITextField!
    
    override func awakeFromNib() {
        
        Helpers.setleftView(textfield: nameField, customWidth: 15)
        Helpers.setleftView(textfield: phoneNumberField, customWidth: 15)
        Helpers.setleftView(textfield: emailField, customWidth: 15)
    }
}

class EventShareViewController: UIViewController,UITextFieldDelegate,popUpDelegate {
    var countryPhoneCode = ""
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var imgNoData: UIImageView!
    @IBOutlet weak var viewOfNoData: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    var searchTxt = ""
    @IBOutlet weak var imgOthers: UIImageView!
    @IBOutlet weak var imgPeople: UIImageView!
    @IBOutlet weak var imgFamily: UIImageView!
    @IBOutlet weak var btnFamily: UIButton!
    @IBOutlet weak var btnPeople: UIButton!
    @IBOutlet weak var btnOthers: UIButton!
    @IBOutlet weak var btnSearchReset: UIButton!
    var usersList = [SearchResultUser]()
    
    var isFromGlobalSearch = false
    
    @IBOutlet weak var tblListView: UITableView!
    
    var selectedUserIDArr                     = NSMutableArray()
    private var networkProvider               = MoyaProvider<FamilyheeyApi>()
    var typeOfInvitations                     = String()
    @IBOutlet weak var btnInvite: UIButton!
    @IBOutlet weak var lblHead: UILabel!
    
    
    var selectedIndex = 0
    var eventId = ""
//    var FamilyArr : familyListResponse!
    var FamilyArr = JSON()

    var peopleListArr                         = JSON()
    @IBOutlet weak var invitedPeopleNameLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        searchBar.delegate = self
        txtSearch.delegate = self
        Helpers.setleftView(textfield: txtSearch, customWidth: 40)
        self.tabBarController?.tabBar.isHidden = true
        
        //        let paddingView: UIView      = UIView(frame: CGRect(x: 0, y: 0, width: 35, height: 20))
        //        txtSearch.leftView           = paddingView
        //        txtSearch.leftViewMode       = .always
        
        //        print(eventId)
        
        if typeOfInvitations.lowercased() == "share"{
            btnInvite.setTitle("Share", for: .normal)
            lblHead.text = "Share Event"
        }
        else{
            btnInvite.setTitle("Invite", for: .normal)
            lblHead.text = "Invite Event"
        }
        btnInvite.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        tblListView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        
        let button = UIButton()
        self.onClickTabSelection(button)
        // getFamilyList()
        
        if selectedIndex == 0{
            let headerNib = UINib.init(nibName: "FamilyListingView", bundle: Bundle.main)
            tblListView.register(headerNib, forHeaderFooterViewReuseIdentifier: "FamilyListingView")
        }
    }
    
    
    //MARK:- Button ACtions
    @IBAction func onClickTabSelection(_ sender: UIButton){
        if sender.tag == 102{
            self.selectedIndex = 2
            selectedUserIDArr.removeAllObjects()
            invitedPeopleNameLbl.text = ""
            //            self.selectWebAPI()
            //            btnExplore.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            //            btnMyEvents.setTitleColor(UIColor(named: "greenBackgrounf"), for: .normal)
            //            btnInviteEvents.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            //
            //            imgExplore.backgroundColor  = UIColor(named: "lightGrayTextColor")
            //            imgMyEvents.backgroundColor  = UIColor(named: "greenBackgrounf")
            //            imgInvite.backgroundColor  = UIColor(named: "lightGrayTextColor")
            
            btnFamily.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            btnOthers.setTitleColor(UIColor(named: "greenBackgrounf"), for: .normal)
            btnPeople.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            
            imgFamily.backgroundColor  = UIColor(named: "lightGrayTextColor")
            imgOthers.backgroundColor  = UIColor(named: "greenBackgrounf")
            imgPeople.backgroundColor  = UIColor(named: "lightGrayTextColor")
            
            tblListView.backgroundColor = .white
            
            self.viewOfNoData.isHidden = true
            self.tblListView.isHidden = false
            self.btnInvite.isHidden = false
            
            self.tblListView.delegate = self
            self.tblListView.dataSource = self
            self.tblListView.scrollToTop()
            self.tblListView.reloadData()
            
        }
        else if sender.tag == 101{
            self.selectedIndex = 1
            selectedUserIDArr.removeAllObjects()
            invitedPeopleNameLbl.text = ""
            
            btnFamily.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            btnOthers.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            btnPeople.setTitleColor(UIColor(named: "greenBackgrounf"), for: .normal)
            
            imgFamily.backgroundColor  = UIColor(named: "lightGrayTextColor")
            imgOthers.backgroundColor  = UIColor(named: "lightGrayTextColor")
            imgPeople.backgroundColor  = UIColor(named: "greenBackgrounf")
            
            tblListView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
            self.callGroupMembers()
            
        }
        else{
            self.selectedIndex = 0
            selectedUserIDArr.removeAllObjects()
            invitedPeopleNameLbl.text = ""
            // getFamilyList()
            
            btnFamily.setTitleColor(UIColor(named: "greenBackgrounf"), for: .normal)
            btnOthers.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            btnPeople.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            
            imgFamily.backgroundColor  = UIColor(named: "greenBackgrounf")
            imgPeople.backgroundColor  = UIColor(named: "lightGrayTextColor")
            imgOthers.backgroundColor  = UIColor(named: "lightGrayTextColor")
            
            tblListView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
            
            getFamilyList()
        }
        tblListView.reloadData()
    }
    
    @IBAction func onClickBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func onClickDone(_ sender: Any) {
        
        if selectedIndex == 0{
            if selectedUserIDArr.count > 0{
                
                let params: Parameters = ["event_id" : eventId, "group_id" : selectedUserIDArr as NSArray, "view" : "template1", "from_user" : UserDefaults.standard.value(forKey: "userId") as! String, "type": typeOfInvitations]
                callinvite(param: params)
                
            }
        }
        else if selectedIndex == 1{
            
            if selectedUserIDArr.count > 0{
                
                let params: Parameters = ["event_id" : eventId, "user_id" : selectedUserIDArr as NSArray, "view" : "template1", "from_user" : UserDefaults.standard.value(forKey: "userId") as! String, "type": typeOfInvitations]
                callinvite(param: params)
            }
        }
            
        else if selectedIndex == 2{
            
            let cell: OtherInviteCell = tblListView.cellForRow(at: IndexPath(row: 0, section: 0)) as! OtherInviteCell
            
            if cell.nameField.text == ""{
                
                Helpers.showAlertDialog(message: "Please enter friend's name", target: self)
                
            }else if cell.phoneCodeField.text == ""{
                
                Helpers.showAlertDialog(message: "Please enter friend's country code", target: self)
                
            }else if cell.phoneNumberField.text == ""{
                
                Helpers.showAlertDialog(message: "Please enter friend's phone number", target: self)
                
            }
                //            else if !Helpers.validatePHnumber(enteredNumber: cell.phoneNumberField.text!){
                //
                //                Helpers.showAlertDialog(message: "Please verify phone number", target: self)
                //            }
            else if cell.emailField.text == ""{
                
                Helpers.showAlertDialog(message: "Please enter friend's email", target: self)
                
            }else if !Helpers.validateEmail(enteredEmail: cell.emailField.text!.trimmingCharacters(in: .whitespacesAndNewlines)){
                
                Helpers.showAlertDialog(message: "Please verify email address", target: self)
            }
            else{
                let tempStr = cell.emailField.text!.prefix(1)
                if !String(tempStr).isAlphanumeric{
                    self.displayAlert(alertStr: "Please start email with a alphanumeric characters", title: "")
                    return
                }
                
                let arr = NSMutableArray()
                
                let dict = NSMutableDictionary()
                dict.setValue(cell.nameField.text!, forKey: "full_name")
                dict.setValue(cell.emailField.text!, forKey: "email")
                dict.setValue("\(cell.phoneCodeField.text!) \(cell.phoneNumberField.text!)", forKey: "phone")
                arr.add(dict)
                
                
                let params: Parameters = ["event_id" : eventId, "others" : arr, "view" : "template1", "from_user" : UserDefaults.standard.value(forKey: "userId") as! String, "type": typeOfInvitations]
                callinvite(param: params)
            }
        }
    }
    
    @IBAction func onClickSearchReset(_ sender: Any) {
        txtSearch.text = ""
        searchTxt = ""
        isFromGlobalSearch = false
        self.btnSearchReset.isHidden = true
        if self.selectedIndex == 1{
            callGroupMembers()
        }
        else if self.selectedIndex == 0{
            getFamilyList()
        }
        else{
        }
        self.txtSearch.endEditing(true)
    }
    //MARK: - Call Invite Api
    
    func callinvite(param : Parameters){
        
        print("params : \(param)")
        
        ActivityIndicatorView.show("Loading....")
        
        networkProvider.request(.event_send_mail(parameter: param), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    let jsonData = JSON(response.data)
                    
                    print("Json data : \(jsonData)")
                    
                    if response.statusCode == 200{
                        if self.typeOfInvitations.lowercased() == "share"{
                            let alert = UIAlertController(title: "Success", message: "Event shared successfully", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                                
                                self.onClickBackAction(self)
                            }))
                            
                            self.present(alert, animated: true, completion: nil)
                        }
                        else{
                            let alert = UIAlertController(title: "Success", message: "You have successfully sent invitations", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                                
                                self.onClickBackAction(self)
                            }))
                            
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.callinvite(param: param)
                        }
                    }

                    else
                    {
                        ActivityIndicatorView.hiding()
                    }
                    
                    
                } catch let err {
                    
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
    //MARK:- WEBAPI
    
    func getFamilyList(){
        ActivityIndicatorView.show("Loading....")
        let parameter = ["user_id" : "\(UserDefaults.standard.value(forKey: "userId") as! String)" ,"query":self.txtSearch.text!]
        ActivityIndicatorView.show("Loading...")
        
        networkProvider.request(.getFamilyListForPost(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    let jsonData = JSON(response.data)
                    ActivityIndicatorView.hiding()
print(jsonData)
                    if response.statusCode == 200
                    {
                        self.FamilyArr = jsonData["data"]
                        if self.FamilyArr.count > 0{
                            self.tblListView.delegate = self
                            self.tblListView.dataSource = self
                            self.tblListView.isHidden = false
                            self.tblListView.reloadData()
                            
                            self.viewOfNoData.isHidden = true
                                              self.lblNoData.isHidden = true
                                              self.imgNoData.isHidden = true
                            self.btnInvite.isHidden = false

                        }
                        else{
                            self.tblListView.isHidden = true
                                             self.btnInvite.isHidden = true
                                             
                                             self.viewOfNoData.isHidden = false
                                             self.lblNoData.isHidden = false
                                             self.imgNoData.isHidden = false                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getFamilyList()
                        }
                    }
                    else{
                        ActivityIndicatorView.hiding()

                        self.tblListView.isHidden = true
                                       self.btnInvite.isHidden = true
                                       
                                       self.viewOfNoData.isHidden = false
                                       self.lblNoData.isHidden = false
                                       self.imgNoData.isHidden = false                    }
                } catch let err {
                    
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
//    func getFamilyList(){
//        let params = [
//            "user_id":UserDefaults.standard.value(forKey: "userId") as! String,
//            "query":self.txtSearch.text!,
//            "event_id":self.eventId
//        ]
//        APIServiceManager.callServer.getFamilyList(url: EndPoint.viewFamily, params: params, success: { (responseMdl) in
//            guard let familyMdl = responseMdl as? familyListResponse else{
//                // self.callGroupMembers()
//                return
//            }
//            // ActivityIndicatorView.hiding()
//
//            if familyMdl.statusCode == 200{
//                print(familyMdl.familyList!)
//                if familyMdl.familyList!.count > 0{
//                    self.FamilyArr = familyMdl
//                    self.tblListView.delegate = self
//                    self.tblListView.dataSource = self
//
//                    self.viewOfNoData.isHidden = true
//                    self.lblNoData.isHidden = true
//                    self.imgNoData.isHidden = true
//
//                    self.tblListView.isHidden = false
//
//                    self.btnInvite.isHidden = false
//
//
//
//                    self.tblListView.reloadData()
//                }
//                else{
//                    self.tblListView.isHidden = true
//                    self.btnInvite.isHidden = true
//
//                    self.viewOfNoData.isHidden = false
//                    self.lblNoData.isHidden = false
//                    self.imgNoData.isHidden = false
//
//                }
//                // self.callGroupMembers()
//                ActivityIndicatorView.hiding()
//
//            }
//            else{
//                //  self.callGroupMembers()
//                ActivityIndicatorView.hiding()
//                self.tblListView.isHidden = true
//                self.btnInvite.isHidden = true
//
//                self.viewOfNoData.isHidden = false
//                self.lblNoData.isHidden = false
//                self.imgNoData.isHidden = false
//            }
//
//        }) { (error) in
//            //  self.callGroupMembers()
//            // ActivityIndicatorView.hiding()
//            self.displayAlert(alertStr: error!.description, title: "Error")
//        }
//    }
    
    //MARK: - Get Group members
    
    func callGroupMembers(){
        
        ActivityIndicatorView.show("Loading....")
        
        //    let userId = UserDefaults.standard.value(forKey: "userId") as! Int
        
        let parameter = ["user_id" : "\(UserDefaults.standard.value(forKey: "userId") as! String)", "event_id" : eventId ,"query":self.txtSearch.text!]
        
        print("param : \(parameter)")
        ActivityIndicatorView.show("Loading...")
        
        networkProvider.request(.user_group_members(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    let jsonData = JSON(response.data)
                    print(jsonData)
                    if response.statusCode == 200{
                        self.peopleListArr = jsonData["data"]
                        if self.peopleListArr.count > 0{
                            self.tblListView.delegate = self
                            self.tblListView.dataSource = self
                            
                            self.viewOfNoData.isHidden = true
                            self.lblNoData.isHidden = true
                            self.imgNoData.isHidden = true
                            
                            self.tblListView.isHidden = false
                            
                            self.btnInvite.isHidden = false
                            
                            
                            
                            self.tblListView.reloadData()
                        }
                        else{
                            self.tblListView.isHidden = true
                            self.btnInvite.isHidden = true
                            
                            self.viewOfNoData.isHidden = false
                            self.lblNoData.isHidden = false
                            self.imgNoData.isHidden = false
                            
                        }
                        
                        
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.callGroupMembers()
                        }
                    }

                    else{
                        self.tblListView.isHidden = true
                        self.btnInvite.isHidden = true
                        
                        self.viewOfNoData.isHidden = false
                        self.lblNoData.isHidden = false
                        self.imgNoData.isHidden = false
                    }
                    
                    
                    
                } catch let err {
                    
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
    //MARK:- Global search functions
    func callSearchApi(){
        isFromGlobalSearch = true
        let param = [
            "userid" : UserDefaults.standard.value(forKey: "userId") as! String,
            "searchtxt" : txtSearch.text!,
            "offset":"0",
            "limit":"100",
            "phonenumbers":[],
            "type":"users",
            ] as [String : Any]
        
        ActivityIndicatorView.show("Please wait!")
        APIServiceManager.callServer.searchRequest(url: EndPoint.search, param: param, fromSearchTabViewController: false, success: { (response) in
            
            ActivityIndicatorView.hiding()
            self.usersList = [SearchResultUser]()
            
            
            if let results = response as! SearchList?{
                self.usersList = results.searchResult!
                
            }
            
            if self.usersList.isEmpty {
                
                self.tblListView.isHidden = true
            } else {
                self.tblListView.isHidden = false
                
                self.tblListView.reloadData()
            }
            
            
        }) { (error) in
            ActivityIndicatorView.hiding()
            self.usersList = [SearchResultUser]()
            //self.FamilyList = [SearchResultGroup]()
            
            if self.usersList.isEmpty {
                
                self.tblListView.isHidden = true
            } else {
                
                self.tblListView.reloadData()
            }
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension EventShareViewController : UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        if selectedIndex == 0{
//            return FamilyArr.familyList!.count
            return FamilyArr.count

        }else if selectedIndex == 1{
            if isFromGlobalSearch{
                return usersList.count
            }
            else{
                return peopleListArr.count
            }
            
        }
        else{
            return 1
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedIndex == 0{
            return 0
        }else if selectedIndex == 2{
            
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if selectedIndex == 2{
            let cell = tblListView.dequeueReusableCell(withIdentifier: "cell3") as! OtherInviteCell
            let currentLocale = NSLocale.current as NSLocale
            let countryCode = currentLocale.object(forKey: .countryCode) as! String//get the set country name, code of your iphone
            let countryName = currentLocale.displayName(forKey: NSLocale.Key.countryCode, value: countryCode)
            
            print("country code is \(countryCode)")
            print(ViewController.getCountryCallingCode(countryRegionCode: countryCode))
            
            self.countryPhoneCode = ViewController.getCountryCallingCode(countryRegionCode: countryCode)
            
            if self.countryPhoneCode.count > 0{
                //                      onCodeButt.setTitle(countryName, for: .normal)
                cell.phoneCodeField.text = ViewController.getCountryCallingCode(countryRegionCode: countryCode)
                
            }
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    
    @IBAction func onClickCode(_ sender: Any) {
        
        self.view.endEditing(true)
        
        let popUp = popupTableViewController()
        self.addChild(popUp)
        // popUp.frmGndrFlag = fromGndr
        popUp.view.frame = UIScreen.main.bounds
        self.view.addSubview(popUp.view)
        popUp.delegate = self
        self.view.bringSubviewToFront(popUp.view)
    }
    
    //MARK:- Protocol PopupDelegate
    func selectCountry(id: String, name: String) {
        print(id)
        let cell: OtherInviteCell = tblListView.cellForRow(at: IndexPath(row: 0, section: 0)) as! OtherInviteCell
        cell.phoneCodeField.text = id
        //        onCodeButt.setTitle(name, for: .normal)
        
        //        txtFieldUnderline.isHidden = false
        
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
        //
        //            self.txtPhone.becomeFirstResponder()
        //        })
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        // if selectedIndex == 0{
        let header = self.tblListView.dequeueReusableHeaderFooterView(withIdentifier: "FamilyListingView") as! FamilyListingView
        
        header.btnAction.tag       = section
        header.btnView.tag         = section
        header.btnView.addTarget(self, action: #selector(inviteFamilySelection(sender:)), for: .touchUpInside)
        header.btnAction.addTarget(self, action: #selector(inviteFamilySelection(sender:)), for: .touchUpInside)
        
        
        if selectedIndex == 1{
            header.byTitle.text = ""
            header.lblType.text = ""//FamilyArr.familyList![section].faType
            header.lblType.isHidden = true
            header.lblCreatedBy.isHidden = true
            header.lblKnownHead.isHidden = true
            header.lblMemberHead.isHidden = true
            header.lblCreatedBy.text = ""//FamilyArr.familyList![section].createdByName
            header.lblMembersCount.text = ""//FamilyArr.familyList![section].memberCount
            header.lblKnown.text = ""//FamilyArr.familyList![section].knownCount
            
            if self.isFromGlobalSearch{
                header.lblTitle.text = usersList[section].full_name
                header.lblRegion.text = usersList[section].origin?.firstUppercased
                
                let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+usersList[section].propic!
                let imgurl = URL(string: temp)
                header.imgLogo.kf.indicatorType = .activity
                
                header.imgLogo.kf.setImage(with: imgurl, placeholder: UIImage(named: "imgNoImage") , options: nil, progressBlock: nil, completionHandler: nil)
                
                if selectedUserIDArr.contains(usersList[section].userid){
                    header.btnAction.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
                    header.btnAction.setTitle("Selected", for: .normal)
                }
                else{
                    header.btnAction.isEnabled = true
                    header.btnView.isEnabled = true
                    header.btnAction.titleLabel?.font = UIFont.systemFont(ofSize: 15)
                    header.btnAction.setTitle("Select", for: .normal)
                    header.btnAction.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                }
            }
            else{
                header.lblTitle.text = self.peopleListArr.arrayValue[section]["full_name"].stringValue.firstUppercased
                
                //            header.lblCreated_height.constant = 0
                header.lblRegion.text = self.peopleListArr.arrayValue[section]["location"].stringValue.firstUppercased
                
                
                let test = self.peopleListArr.arrayValue[section]["invitation_status"].intValue
                if selectedUserIDArr.contains(self.peopleListArr.arrayValue[section]["user_id"].intValue){
                    header.btnAction.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
                    header.btnAction.setTitle("Selected", for: .normal)
                    
                }else{
                    if test == 1{
                        header.btnAction.titleLabel?.font = UIFont.systemFont(ofSize: 15)
                        header.btnAction.setTitle("Invited", for: .normal)
                        header.btnAction.backgroundColor = .white
                        header.btnAction.layer.borderWidth = 1
                        header.btnAction.layer.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                        header.btnAction.isEnabled = false
                        header.btnView.isEnabled = false
                    }
                    else{
                        header.btnAction.isEnabled = true
                        header.btnView.isEnabled = true
                        header.btnAction.titleLabel?.font = UIFont.systemFont(ofSize: 15)
                        header.btnAction.setTitle("Select", for: .normal)
                        header.btnAction.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                    }
                }
                
                let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+self.peopleListArr.arrayValue[section]["propic"].stringValue
                let imgurl = URL(string: temp)
                header.imgLogo.kf.indicatorType = .activity
                
                header.imgLogo.kf.setImage(with: imgurl, placeholder: UIImage(named: "imgNoImage") , options: nil, progressBlock: nil, completionHandler: nil)
            }
            
        }
        else if selectedIndex == 2{
        }
        else{
            header.lblType.isHidden = false
            header.lblCreatedBy.isHidden = false
            header.lblKnownHead.isHidden = false
            header.lblMemberHead.isHidden = false
            //            header.lblCreated_height.constant = 18
            header.byTitle.text = "By"
            
            header.lblTitle.text = self.FamilyArr.arrayValue[section]["group_name"].stringValue.firstUppercased
            header.lblType.text = self.FamilyArr.arrayValue[section]["group_type"].stringValue
            header.lblRegion.text = self.FamilyArr.arrayValue[section]["base_region"].stringValue
            header.lblCreatedBy.text = self.FamilyArr.arrayValue[section]["created_by"].stringValue ?? ""
            
             header.lblMemberHead.isHidden = true
            header.lblMembersCount.isHidden = true
            header.lblKnownHead.isHidden = true
            header.lblKnown.isHidden = true
//            let memberCount = FamilyArr.familyList![section].memberCount
//            let knownCount = FamilyArr.familyList![section].knownMemberCount
//
//            if Int(memberCount!)! > 0{
//                header.lblMembersCount.text = FamilyArr.familyList![section].memberCount
//                header.lblMemberHead.isHidden = false
//                header.lblMembersCount.isHidden = false
//            }
//            else{
//                header.lblMemberHead.isHidden = true
//                header.lblMembersCount.isHidden = true
//            }
//
//            if Int(knownCount)! > 0{
//                header.lblKnown.text = FamilyArr.familyList![section].knownMemberCount
//                header.lblKnownHead.isHidden = false
//                header.lblKnown.isHidden = false
//            }
//            else{
//                header.lblKnownHead.isHidden = true
//                header.lblKnown.isHidden = true
//            }
            
            
            
            //            header.lblKnown.text = FamilyArr.familyList![section].know
            
//            let test = FamilyArr.familyList![section].invitationStatus
            
            if selectedUserIDArr.contains(self.FamilyArr.arrayValue[section]["id"].intValue){
                header.btnAction.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
                header.btnAction.setTitle("Selected", for: .normal)
            }else{
                if self.FamilyArr.arrayValue[section]["id"].intValue == 1{
                    header.btnAction.titleLabel?.font = UIFont.systemFont(ofSize: 15)
                    header.btnAction.setTitle("Invited", for: .normal)
                    header.btnAction.backgroundColor = .white
                    header.btnAction.layer.borderWidth = 1
                    header.btnAction.layer.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                    header.btnAction.isEnabled = false
                    header.btnView.isEnabled = false
                }
                else{
                    header.btnAction.isEnabled = true
                    header.btnView.isEnabled = true
                    header.btnAction.titleLabel?.font = UIFont.systemFont(ofSize: 15)
                    header.btnAction.setTitle("Select", for: .normal)
                    header.btnAction.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                }
            }
            
            if FamilyArr.arrayValue[section]["logo"].stringValue.count != 0{
                let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=150&height=150&url="+"\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+FamilyArr.arrayValue[section]["logo"].stringValue
                let imgurl = URL(string: temp)
                print(imgurl)
                header.imgLogo.kf.indicatorType = .activity
                
                header.imgLogo.kf.setImage(with: imgurl, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
            }
            else{
                header.imgLogo.image = UIImage(named: "Family Logo")
            }
        }
        
        
        return header
        // }
        //        else{
        //
        //        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if selectedIndex == 2{
            
            return 570
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if  selectedIndex == 0{
            return 150
        }
        else if selectedIndex == 1{
            return 110
        }
        else{
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    //MARK: - Invite famy selection
    
    @IBAction func inviteFamilySelection(sender: UIButton){
        
        print("tag : \(sender.tag)")
        
        if selectedIndex == 1{
            
            if isFromGlobalSearch{
                if selectedUserIDArr.contains(self.usersList[sender.tag].userid){
                    
                    selectedUserIDArr.remove(self.usersList[sender.tag].userid)
                }else{
                    selectedUserIDArr.add(self.usersList[sender.tag].userid)
                }
            }
            else{
                if selectedUserIDArr.contains(self.peopleListArr.arrayValue[sender.tag]["user_id"].intValue){
                    
                    selectedUserIDArr.remove(self.peopleListArr.arrayValue[sender.tag]["user_id"].intValue)
                }else{
                    selectedUserIDArr.add(self.peopleListArr.arrayValue[sender.tag]["user_id"].intValue)
                }
            }
            
            
        }else{
            
            if selectedUserIDArr.contains(self.FamilyArr.arrayValue[sender.tag]["id"].intValue){
                
                selectedUserIDArr.remove(self.FamilyArr.arrayValue[sender.tag]["id"].intValue)
            }else{
                selectedUserIDArr.add(self.FamilyArr.arrayValue[sender.tag]["id"].intValue)
            }
            
            if selectedUserIDArr.count > 0{
                
            }else{
                invitedPeopleNameLbl.text = ""
            }
        }
        
        
        
        tblListView.reloadSections(IndexSet(integer : sender.tag), with: .none)
        
        //print(FamilyArr.familyList![sender.tag].faId)
    }
    
    //MARK:- textfield dellegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        textField.resignFirstResponder()
        /* if self.selectedIndex == 1{
         if textField == txtSearch
         {
         callGroupMembers()
         }
         }
         else if self.selectedIndex == 0{
         if textField == txtSearch
         {
         getFamilyList()
         }
         
         }
         else{
         }*/
        if textField.text!.count > 0{
            self.btnSearchReset.isHidden = false
        }
        else{
            self.btnSearchReset.isHidden = true
        }
        textField.endEditing(true)
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        self.view.endEditing(true)
        if textField.text!.count > 0{
            self.btnSearchReset.isHidden = false
        }
        else{
            self.btnSearchReset.isHidden = true
        }
        
        
        if self.selectedIndex == 1{
            if textField == txtSearch{
                callGroupMembers()
            }
        }
        else if self.selectedIndex == 0{
            if textField == txtSearch
            {
                getFamilyList()
            }
            
        }
        else{
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text!.count > 0{
            self.btnSearchReset.isHidden = false
        }
        else{
            self.btnSearchReset.isHidden = true
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
        }
        return true
    }
    
    
    
    static func getCountryCallingCode(countryRegionCode:String)->String{
        
        let prefixCodes = ["AF": "+93", "AE": "+971", "AL": "+355", "AN": "+599", "AS":"+1", "AD": "+376", "AO": "+244", "AI": "+1", "AG":"+1", "AR": "+54","AM": "+374", "AW": "+297", "AU":"+61", "AT": "43","AZ": "994", "BS": "1", "BH":"973", "BF": "226","BI": "257", "BD": "880", "BB": "1", "BY": "+375", "BE":"+32","BZ": "+501", "BJ": "+229", "BM": "+1", "BT":"+975", "BA": "+387", "BW": "+267", "BR": "+55", "BG": "+359", "BO": "+591", "BL": "+590", "BN": "+673", "CC": "+61", "CD":"+243","CI": "+225", "KH":"+855", "CM": "+237", "CA": "+1", "CV": "+238", "KY":"+345", "CF":"+236", "CH": "+41", "CL": "+56", "CN":"+86","CX": "+61", "CO": "+57", "KM": "+269", "CG":"+242", "CK": "+682", "CR": "+506", "CU":"+53", "+CY":"+537","CZ": "+420", "DE": "+49", "DK": "+45", "DJ":"+253", "DM": "+1", "DO": "+1", "DZ": "+213", "EC": "+593", "EG":"+20", "ER": "+291", "EE":"+372","ES": "+34", "ET": "+251", "FM": "+691", "FK": "+500", "FO": "+298", "FJ": "+679", "FI":"+358", "FR": "+33", "GB":"+44", "GF": "+594", "GA":"+241", "GS": "+500", "GM":"+220", "GE":"+995","GH":"+233", "GI": "+350", "GQ": "+240", "GR": "+30", "GG": "+44", "GL": "+299", "GD":"+1", "GP": "+590", "GU": "+1", "GT": "+502", "GN":"+224","GW": "+245", "GY": "+592", "HT": "+509", "HR": "+385", "HN":"+504", "HU": "+36", "HK": "+852", "IR": "+98", "IM": "+44", "IL": "+972", "IO":"+246", "IS": "+354", "IN": "+91", "ID":"+62", "IQ":"+964", "IE": "+353","IT":"+39", "JM":"+1", "JP": "+81", "JO": "+962", "JE":"+44", "KP": "+850", "KR": "+82","KZ":"+77", "KE": "+254", "KI": "+686", "KW": "+965", "KG":"+996","KN":"+1", "LC": "+1", "LV": "+371", "LB": "+961", "LK":"+94", "LS": "+266", "LR":"+231", "LI": "+423", "LT": "+370", "LU": "+352", "LA": "+856", "LY":"+218", "MO": "+853", "MK": "+389", "MG":"+261", "MW": "+265", "MY": "+60","MV": "+960", "ML":"+223", "MT": "+356", "MH": "+692", "MQ": "+596", "MR":"+222", "MU": "+230", "MX": "+52","MC": "+377", "MN": "+976", "ME": "+382", "MP": "+1", "MS": "+1", "MA":"+212", "MM": "+95", "MF": "+590", "MD":"+373", "MZ": "+258", "NA":"+264", "NR":"+674", "NP":"+977", "NL": "+31","NC": "+687", "NZ":"+64", "NI": "+505", "NE": "+227", "NG": "+234", "NU":"+683", "NF": "+672", "NO": "+47","OM": "+968", "PK": "+92", "PM": "+508", "PW": "+680", "PF": "+689", "PA": "+507", "PG":"+675", "PY": "+595", "PE": "+51", "PH": "+63", "PL":"+48", "PN": "+872","PT": "+351", "PR": "+1","PS": "+970", "QA": "+974", "RO":"+40", "RE":"+262", "RS": "+381", "RU": "+7", "RW": "+250", "SM": "+378", "SA":"+966", "SN": "+221", "SC": "+248", "SL":"+232","SG": "+65", "SK": "+421", "SI": "+386", "SB":"+677", "SH": "+290", "SD": "+249", "SR": "+597","SZ": "+268", "SE":"+46", "SV": "+503", "ST": "+239","SO": "+252", "SJ": "+47", "SY":"+963", "TW": "+886", "TZ": "+255", "TL": "+670", "TD": "+235", "TJ": "+992", "TH": "+66", "TG":"+228", "TK": "+690", "TO": "+676", "TT": "+1", "TN":"+216","TR": "+90", "TM": "+993", "TC": "+1", "TV":"+688", "UG": "+256", "UA": "+380", "US": "+1", "UY": "+598","UZ": "+998", "VA":"+379", "VE":"+58", "VN": "+84", "VG": "+1", "VI": "+1","VC":"+1", "VU":"+678", "WS": "+685", "WF": "+681", "YE": "+967", "YT": "+262","ZA": "+27" , "ZM": "+260", "ZW":"+263"]
        let countryDialingCode = prefixCodes[countryRegionCode]
        return countryDialingCode!
        
    }
}


/*extension EventShareViewController: UISearchBarDelegate{
 //    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
 //        return true
 //    }
 func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
 self.searchTxt = searchBar.text!
 if self.selectedIndex == 1{
 callGroupMembers()
 }
 else if self.selectedIndex == 0{
 getFamilyList()
 
 }
 else{
 }
 //        print(self.searchTxt)
 self.searchBar.endEditing(true)
 }
 
 func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
 if searchText.count == 0{
 self.searchTxt = ""
 getFamilyList()
 self.searchBar.endEditing(true)
 }
 }
 
 
 
 
 
 }*/
