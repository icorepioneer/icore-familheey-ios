//
//  SharedPeopleListViewController.swift
//  familheey
//
//  Created by familheey on 11/11/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class SharedPeopleCell: UITableViewCell{
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var lblCreatedBy: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblBtnTitle: UILabel!
    @IBOutlet weak var tempView: UIView!
    
    override func awakeFromNib() {
    }
}


class SharedPeopleListViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
