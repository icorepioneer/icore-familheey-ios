//
//  ViewSignUpTableViewCell.swift
//  familheey
//
//  Created by Innovation Incubator on 28/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class ViewSignUpTableViewCell: UITableViewCell {

    @IBOutlet weak var lblSlotCount: UILabel!
    @IBOutlet weak var btnDelete: subclassedUIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var lblUpdateDate: UILabel!
    @IBOutlet weak var lblContributorName: UILabel!
    @IBOutlet weak var imgOfUser: UIImageView!
    @IBOutlet weak var viewOfBorder: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
