//
//  ViewSignUpViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 28/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON
import Alamofire


class ViewSignUpViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,ContributionAddDelegate {
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    
    @IBOutlet weak var imgViewNoSignUp: UIImageView!
    var CurrentSlotDetails = JSON()
    var arrayOfContributedUser = [JSON]()
    @IBOutlet weak var tblViewOfSignUpUsers: UITableView!
    @IBOutlet weak var lblNoData: UILabel!
    
    @IBOutlet weak var viewOfNoSignUp: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblViewOfSignUpUsers.dataSource = self
        self.tblViewOfSignUpUsers.delegate = self
        
        self.apiSignUpsContributionList()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBack_OnClick(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    //   MARK: - Api calling methods
    func apiSignUpsContributionList() {
        //            "is_active" : true,
        //            "item_quantity" : 50,
        //            "user_id" : 989,
        //            "event_id" : 83,
        //            "updatedAt" : "2019-10-24T06:27:11.981Z",
        //            "createdAt" : "2019-10-24T05:10:25.254Z",
        //            "id" : 1,
        //            "slot_title" : "test",
        //            "start_date" : "2019-10-25T05:03:09.528Z",
        //            "end_date" : "2019-10-26T05:03:09.528Z",
        //            "slot_description" : "test event signup items description"
        //        },
        
        
        let parameter = ["event_id": CurrentSlotDetails["event_id"].stringValue,
                         "item_id":CurrentSlotDetails["id"].stringValue] as [String : Any]
        
        print(parameter)
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.item_contributor_list(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    print(jsonData["status"])
                    print(response.statusCode)
                    if response.statusCode == 200
                    {
                        
                        self.arrayOfContributedUser = jsonData["data"].arrayValue
                        print(self.arrayOfContributedUser)
                        
                        
                        if self.arrayOfContributedUser.count != 0
                        {
                            self.viewOfNoSignUp.isHidden = true
                            self.imgViewNoSignUp.isHidden = true
                            self.lblNoData.isHidden = true
                            
                            self.tblViewOfSignUpUsers.isHidden = false
                            
                            self.tblViewOfSignUpUsers.reloadData()
                        }
                        else
                        {
                            self.viewOfNoSignUp.isHidden = false
                            self.imgViewNoSignUp.isHidden = false
                            self.lblNoData.isHidden = false
                            
                            self.tblViewOfSignUpUsers.isHidden = true
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.apiSignUpsContributionList()
                        }
                    }
                        
                    else{
                        print(jsonData["code"])
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                } catch let err {
                    
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfContributedUser.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ViewSignUpTableViewCell", for: indexPath) as! ViewSignUpTableViewCell
        let currentContributeUser = self.arrayOfContributedUser[indexPath.row].dictionaryValue
        
        
        
        let ContributePic = (currentContributeUser["propic"]!.stringValue)
        if ContributePic != ""{
            let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+ContributePic
            let imgUrl = URL(string: temp)
            cell.imgOfUser.kf.indicatorType = .activity
            //            cell.imgOfUser.kf.setImage(with: imgUrl, placeholder: nil, options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
            
            cell.imgOfUser.kf.setImage(with: imgUrl, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
            
        }
        else
        {
            cell.imgOfUser.image = #imageLiteral(resourceName: "Male Colored")
            
        }
        cell.lblContributorName.text = currentContributeUser["full_name"]?.string ?? "Unknown"
        cell.lblSlotCount.text = "\(currentContributeUser["quantity_collected"]!.stringValue) Slots"
        print(UserDefaults.standard.value(forKey: "userId") as! String)
        print(currentContributeUser["user_id"]!.stringValue)
        
        if UserDefaults.standard.value(forKey: "userId") as! String == currentContributeUser["contribute_user_id"]!.stringValue
        {
            cell.btnEdit.isHidden = false
        }
        else
        {
            cell.btnEdit.isHidden = true
            
        }
        cell.lblUpdateDate.text = "Responded on : \(self.convertDateToDisplayDate(dateFromResponse: currentContributeUser["updated_on"]!.stringValue))"
        cell.btnEdit.tag = indexPath.row
        cell.btnEdit.addTarget(self, action: #selector(btnEdit_OnClik(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func btnEdit_OnClik(sender:UIButton)
    {
        
        let currentContributeUser = self.arrayOfContributedUser[sender.tag]
        
        // print("current Agenda: \(currentAgenda)")
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContributionAddViewController") as! ContributionAddViewController
        vc.CurrentSlotDetails = currentContributeUser
        vc.isUpdate = true
        vc.contributionDelegate = self
        //        vc.modalPresentationStyle = .popover
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: true, completion: nil)
        
    }
    
    func convertDateToDisplayDate(dateFromResponse:String)->String
    {
        let dateAsString               = dateFromResponse
        let dateFormatter              = DateFormatter()
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        dateFormatter.locale = .current
        dateFormatter.dateFormat       = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        let date = dateFormatter.date(from: dateAsString)
        print(date as Any)
        dateFormatter.timeZone = .current
        dateFormatter.dateFormat       =  "MMM dd yyyy h:mm a" //"MMM dd yyyy"
        let DateFormatted = dateFormatter.string(from: date!)
        print(DateFormatted)
        return DateFormatted
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    func callBackforSignupListing() {
        
        self.apiSignUpsContributionList()
        
    }
    
}
