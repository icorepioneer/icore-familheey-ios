//
//  ContributionAddViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 28/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Alamofire
import Moya
import SwiftyJSON

protocol ContributionAddDelegate {
    func callBackforSignupListing()
}

class ContributionAddViewController: UIViewController,UITextFieldDelegate {
    var eventId:String!
    var isUpdate:Bool!
    var contributionDelegate:ContributionAddDelegate!
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    var CurrentSlotDetails = JSON()
    @IBOutlet weak var lblCount: UITextField!
    @IBOutlet weak var lblAvailability: UILabel!
    @IBOutlet weak var lblSlotName: UILabel!
    @IBOutlet weak var lblHeading: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblCount.delegate = self
        self.lblSlotName.text = "\(CurrentSlotDetails["slot_title"]) , \(CurrentSlotDetails["slot_description"])"
        self.lblAvailability.text = "\(CurrentSlotDetails["needed"].stringValue) of \(CurrentSlotDetails["item_quantity"].stringValue) Available"
        if isUpdate
        {
            self.lblCount.text = "\(CurrentSlotDetails["quantity_collected"])"
        }
        // Do any additional setup after loading the view.
    }
    //MARK: - Textfield delegate methods
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func btnOuter_OnClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnDoneOnClick(_ sender: Any) {
        //        let count = Int(lblCount.text!)
        //        let neeeded = Int(CurrentSlotDetails["needed"].stringValue)
        if lblCount.text == ""
        {
            Helpers.showAlertDialog(message: "Please enter your contribution count", target: self)
            return
            
        }
        else if !isUpdate
        {
            if self.lblCount.text!.compare(CurrentSlotDetails["needed"].stringValue, options: .numeric) == .orderedDescending {
                
                //            Helpers.showAlertDialog(message: "Thank you for your contribution... but we only need  \(CurrentSlotDetails["needed"].stringValue)", target: self)
                
                Helpers.showAlertDialog(message: "Only \(CurrentSlotDetails["needed"].stringValue) available", target: self)
                
                self.lblCount.text = ""
                return
            }
            else{
                self.apiForAddContributionItem()
                
            }
        }
        else if isUpdate{
            
            //            let myFloat = (myString as NSString).floatValue
            let count = (lblCount.text as! NSString).floatValue
            let quantitycollected =   (CurrentSlotDetails["quantity_collected"].stringValue as! NSString).floatValue
            let difference:Float = count - quantitycollected
            let differencestring :String = "\(difference)"
            print(differencestring)
            print(CurrentSlotDetails["needed"].stringValue)
            let neededFloat  = (CurrentSlotDetails["needed"].stringValue as! NSString).floatValue
            print(neededFloat)
            let neededstring :String = "\(neededFloat)"
            print(neededstring)
            //            if differencestring.compare(neededstring, options: .numeric) == .orderedSame
            //            {
            //              print("saammmemjr")
            //            }
            
            
            
            if differencestring.compare("0", options: .numeric) == .orderedDescending
            {
                if differencestring.compare(neededstring, options: .numeric) == .orderedDescending {
                    if differencestring.compare(neededstring, options: .numeric) == .orderedSame
                    {
                        
                        self.apiForUpdateContributionItem()
                        
                    }
                    else
                    {
                        //                    Helpers.showAlertDialog(message: "Thank you for your contribution... but we only need  \(CurrentSlotDetails["needed"].stringValue)", target: self)
                        Helpers.showAlertDialog(message: "Only \(CurrentSlotDetails["needed"].stringValue) available", target: self)
                        self.lblCount.text = ""
                        return
                    }
                }
                else
                {
                    self.apiForUpdateContributionItem()
                    
                }
            }
            else
            {
                self.apiForUpdateContributionItem()
                
            }
            
            
            
        }
        else
        {
            if isUpdate
            {
                self.apiForUpdateContributionItem()
                
            }
            else
            {
                self.apiForAddContributionItem()
            }
        }
    }
    
    
    //   MARK: - Api calling methods
    func apiForAddContributionItem() {
        
        
        
        //        add_eventitems_contribution
        //            {
        //                "event_id":"83",
        //                "contribute_user_id":"989",
        //                "contribute_items_id":"1",
        //                "contribute_item_quantity":"5"
        //        }
        let parameter = ["event_id":self.eventId!,"contribute_user_id":UserDefaults.standard.value(forKey: "userId") as! String,"contribute_items_id":CurrentSlotDetails["id"].stringValue,
                         "contribute_item_quantity":lblCount.text!] as [String : Any]
        
        print(parameter)
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.add_eventitems_contribution(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    print(jsonData["status"])
                    print(response.statusCode)
                    if response.statusCode == 200{
                        let AlertCus = CustomAlertVC.show(in: self, title: "Success", message: "Successfully added Your Contribution ", image:UIImage.init(named: "Green_tick")!)
                        AlertCus.addAction(CPAlertAction.init(title: "OK", type: .normal, handler: {
                            self.contributionDelegate.callBackforSignupListing()
                            self.dismiss(animated: true, completion: nil)
                        }))
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.apiForAddContributionItem()
                        }
                    }

                    else{
                        let alert = UIAlertController(title: NSLocalizedString("Failed", comment: ""), message: jsonData["message"].stringValue, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: { _ -> Void in
                            
                            self.contributionDelegate.callBackforSignupListing()
                            self.dismiss(animated: true, completion: nil)
                        }))
                        
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    
                } catch let err {
                    
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
    
    func apiForUpdateContributionItem() {
        
        
        
        let parameter = ["id":CurrentSlotDetails["contr_id"].stringValue,
                         "contribute_item_quantity":lblCount.text!] as [String : Any]
        
        print(parameter)
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.update_eventitems_contribution(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    print(jsonData["status"])
                    print(response.statusCode)
                    if response.statusCode == 200{
                        let AlertCus = CustomAlertVC.show(in: self, title: "Success", message: "Successfully updated Your Contribution ", image:UIImage.init(named: "Green_tick")!)
                        AlertCus.addAction(CPAlertAction.init(title: "OK", type: .normal, handler: {
                            self.contributionDelegate.callBackforSignupListing()
                            self.dismiss(animated: true, completion: nil)
                        }))
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.apiForUpdateContributionItem()
                        }
                    }

                    else{
                        print(jsonData["code"])
                        let alert = UIAlertController(title: NSLocalizedString("Failed", comment: ""), message: jsonData["message"].stringValue, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertAction.Style.default, handler: { _ -> Void in
                            
                            self.contributionDelegate.callBackforSignupListing()
                            self.dismiss(animated: true, completion: nil)
                        }))
                        
                        self.present(alert, animated: true, completion: nil)
                    }
                } catch let err {
                    
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
