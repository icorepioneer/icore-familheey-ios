//
//  AddSignUpViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 26/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Alamofire
import Moya
import SwiftyJSON
protocol signUpCreationDelegate {
    func callBackforSignupListing()
}

class AddSignUpViewController: UIViewController , UITextFieldDelegate,UITextViewDelegate{
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    var eventId:String!
    var isUpdate:Bool!
    var slotDetails = JSON()
    var creationDelegate:signUpCreationDelegate!
    var timetag:Int!
    var dateTag:Int!
    var selectedStartDate:String = ""
    var selectedEndDate:String = ""
    var selectedStartDateFormatted:String = ""
    var selectedEndDateFormatted:String = ""
    @IBOutlet weak var txtEndDate: UITextField!
    @IBOutlet weak var txtStartDate: UITextField!
    @IBOutlet weak var lblEndDateHeading: UILabel!
    @IBOutlet weak var lblStartDateHeading: UILabel!
    @IBOutlet weak var txtCount: UITextField!
    @IBOutlet weak var lblCountHeading: UILabel!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var lblDescriptionHeading: UILabel!
    @IBOutlet weak var txtSlotTitle: UITextField!
    @IBOutlet weak var lblSlotTitleHeading: UILabel!
    @IBOutlet weak var lblNavHeading: UILabel!
    var timeCancelEnable:Bool = false
    
    @IBOutlet weak var btnAdd: UIButton!
    var theme: SambagTheme = .light

    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.setTextFiledPlaceholder()
if isUpdate
{
    self.lblNavHeading.text = "Edit Sign Ups"
    self.btnAdd.setTitle("Save Changes", for: .normal)
    self.txtSlotTitle.text = self.slotDetails["slot_title"].stringValue
    self.txtDescription.text = self.slotDetails["slot_description"].stringValue
    self.txtCount.text = self.slotDetails["item_quantity"].stringValue
    if self.slotDetails["start_date"].stringValue.contains("00:00:00") {
        print("exists")
         self.txtStartDate.text = convertDateToDisplayDatewithoutTime(dateFromResponse:self.slotDetails["start_date"].stringValue)
        timeCancelEnable = true
    }
    else
    {
    self.txtStartDate.text = convertDateToDisplayDate(dateFromResponse:self.slotDetails["start_date"].stringValue)
        timeCancelEnable = false
    }
    
    
    if self.slotDetails["end_date"].stringValue.contains("23:59:59") {
           print("exists")
    self.txtEndDate.text = convertDateToDisplayDatewithoutTime(dateFromResponse:self.slotDetails["end_date"].stringValue)
        timeCancelEnable = true
    }
    else{
        self.txtEndDate.text = convertDateToDisplayDate(dateFromResponse:self.slotDetails["end_date"].stringValue)
        timeCancelEnable = false
    }
//    let start = self.txtStartDate.text as! String
//    let end = self.txtEndDate.text as! String
//
//   self.selectedStartDateFormatted = self.slotDetails["start_date"].stringValue
//   self.selectedEndDateFormatted = self.slotDetails["end_date"].stringValue
    self.selectedStartDateFormatted = convertTformatToOurFormat(date:self.slotDetails["start_date"].stringValue)
     self.selectedEndDateFormatted = convertTformatToOurFormat(date:self.slotDetails["end_date"].stringValue)
    
    
    
    
        }
        else
{
    self.lblNavHeading.text = "Add Sign Ups"
    self.btnAdd.setTitle("Add", for: .normal)
timeCancelEnable = false

    
        }

        // Do any additional setup after loading the view.
    }
    
    func convertDateToDisplayDate(dateFromResponse:String)->String
    {
       let dateAsString               = dateFromResponse
        let dateFormatter              = DateFormatter()
        dateFormatter.dateFormat       = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        let date = dateFormatter.date(from: dateAsString)
        print(date as Any)
        dateFormatter.dateFormat       = "MMM dd yyyy   h:mm a"
        let DateFormatted = dateFormatter.string(from: date!)
        print(DateFormatted)
        return DateFormatted
    }
    func convertDateToDisplayDatewithoutTime(dateFromResponse:String)->String
    {
       let dateAsString               = dateFromResponse
        let dateFormatter              = DateFormatter()
        dateFormatter.dateFormat       = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        let date = dateFormatter.date(from: dateAsString)
        print(date as Any)
        dateFormatter.dateFormat       = "MMM dd yyyy"
        let DateFormatted = dateFormatter.string(from: date!)
        print(DateFormatted)
        return DateFormatted
    }
    func setTextFiledPlaceholder()
    {
        
        self.lblSlotTitleHeading.attributedText = Helpers.setAttributedPlaceHolder(placeHolderName: "Title")
//        self.lblDescriptionHeading.attributedText = Helpers.setAttributedPlaceHolder(placeHolderName: "Description")
        self.lblCountHeading.attributedText = Helpers.setAttributedPlaceHolder(placeHolderName: "Count")
        self.lblStartDateHeading.attributedText = Helpers.setAttributedPlaceHolder(placeHolderName: "Start Date & Time")
        self.lblEndDateHeading.attributedText = Helpers.setAttributedPlaceHolder(placeHolderName: "End Date & Time")
        
        Helpers.setleftView(textfield: txtSlotTitle, customWidth: 15)
        Helpers.setleftView(textfield: txtCount, customWidth: 15)
        Helpers.setleftView(textfield: txtStartDate, customWidth: 15)
        Helpers.setleftView(textfield: txtEndDate, customWidth: 15)
        txtSlotTitle.delegate = self
        txtCount.delegate = self
        txtDescription.delegate = self

        
    }
    //MARK: - Textfield delegate methods
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: - Button actions
    @IBAction func btnBackOnClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDateOnClick(_ sender: UIButton) {
        
        if sender.tag == 0
        {
            self.dateTag = 0
            let vc = SambagDatePickerViewController()
            vc.theme = theme
            vc.delegate = self
            present(vc, animated: true, completion: nil)
            
        }
        else
        {
            if self.txtStartDate.text == ""
            {
                Helpers.showAlertDialog(message: "Please select Start Date", target: self)
                
            }
            else
            {
                self.dateTag = 1
                let vc = SambagDatePickerViewController()
                vc.theme = theme
                vc.delegate = self
                present(vc, animated: true, completion: nil)
                
            }
        }
        
    }
    
    @IBAction func btnAddSignupOnclick(_ sender: Any) {
        if self.isUpdate
        {
            if DatavalidationForSubmit(){
                self.apiForUpdateSignUps()
            }
        }
        else
        {
        if DatavalidationForSubmit(){
            self.apiForAddSignUps()
        }
        }
        
    }
    
    
    //MARK:- VALIDATION
    func DatavalidationForSubmit()->Bool{
        
        if txtSlotTitle.text == "" {
            
            Helpers.showAlertDialog(message: "Slot Title is empty", target: self)
            return false
        }
        else if txtCount.text == "" {
            
            Helpers.showAlertDialog(message: "Count is empty", target: self)
            return false
            
        }
            else
            if txtStartDate.text == "" {
                
                Helpers.showAlertDialog(message: "Start Date is empty", target: self)
                return false
            }
            else if txtEndDate.text == "" {
                
                Helpers.showAlertDialog(message: "End Date is empty", target: self)
                return false
            }
      
            
        else {
            
            return true
            
        }
    }
    
 //   MARK: - Api calling methods
    func apiForAddSignUps() {
        
        
//        let startDateTimestamp = self.convertTimetoTimestamp(date: self.selectedStartDate)
//        let endDateTimestamp = self.convertTimetoTimestamp(date: self.selectedEndDate)
        
        
        let parameter = ["event_id":self.eventId!,"user_id":UserDefaults.standard.value(forKey: "userId") as! String,"slot_title":self.txtSlotTitle.text!,
                         "item_quantity":self.txtCount.text!,
                         "slot_description":self.txtDescription.text!,
                         "start_date":self.selectedStartDateFormatted,
                         "end_date":self.selectedEndDateFormatted] as [String : Any]
        
        print(parameter)
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.addSignUps(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    print(jsonData["status"])
                    print(response.statusCode)
                    if response.statusCode == 200{
                        let AlertCus = CustomAlertVC.show(in: self, title: "Success", message: "Successfully added Sign up ", image:UIImage.init(named: "Green_tick")!)
                        AlertCus.addAction(CPAlertAction.init(title: "OK", type: .normal, handler: {
                            self.creationDelegate.callBackforSignupListing()
                            self.navigationController?.popViewController(animated: true)
                        }))
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.apiForAddSignUps()
                        }
                    }

                    else{
                        print(jsonData["code"])
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                    
                } catch let err {
                    
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
    func apiForUpdateSignUps() {
        
        let parameter = ["event_id":self.slotDetails["event_id"].stringValue,"id":self.slotDetails["id"].stringValue,"slot_title":self.txtSlotTitle.text!,
                         "item_quantity":self.txtCount.text!,
                         "slot_description":self.txtDescription.text!,
                         "start_date":self.selectedStartDateFormatted,
                         "end_date":self.selectedEndDateFormatted] as [String : Any]
        
        print(parameter)
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.UpdateSignUps(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    print(jsonData["status"])
                    print(response.statusCode)
                    if response.statusCode == 200{
                        let AlertCus = CustomAlertVC.show(in: self, title: "Success", message: "Successfully Update Sign up ", image:UIImage.init(named: "Green_tick")!)
                        AlertCus.addAction(CPAlertAction.init(title: "OK", type: .normal, handler: {
                            self.creationDelegate.callBackforSignupListing()
                            self.navigationController?.popViewController(animated: true)
                        }))
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.apiForUpdateSignUps()
                        }
                    }

                    else
                    {
                        
                        print(jsonData["code"])
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                    
                } catch let err {
                    
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension AddSignUpViewController: SambagDatePickerViewControllerDelegate
    
{
    
    func sambagDatePickerDidSet(_ viewController: SambagDatePickerViewController, result: SambagDatePickerResult) {
        if self.dateTag == 0
        {
            //            txtStartDate.text = "\(result)"
            selectedStartDate = "\(result)"
            print(selectedStartDate)
            self.timeCancelEnable = false
            self.timetag = 0
            txtEndDate.text = ""
            selectedEndDate += ""
            self.selectedEndDateFormatted = ""
            viewController.dismiss(animated: true, completion: nil)
                   let vc = SambagTimePickerViewController()
                   vc.theme = theme
            vc.skipEnable = true
                   vc.delegate = self
                   present(vc, animated: true, completion: nil)
            
        }
        else
        {
            //            txtEndDate.text = "\(result)"
            selectedEndDate = "\(result)"
            print(selectedEndDate)
            
            self.timetag = 1
            if timeCancelEnable
            {
                viewController.dismiss(animated: true, completion: nil)
                
                
                
                        
                   
                    selectedEndDate += ""

                    self.selectedEndDateFormatted = self.convertDateFormatwithoutTime(date: "\(selectedEndDate) 23:59:59")
                    print(selectedEndDateFormatted)
                    if selectedEndDateFormatted != ""
                    {
                    if self.checkDateHigherThanCurrentDate(selectedDatestring: self.selectedEndDateFormatted)
                    {
                        //                Helpers.showAlertDialog(message: "Please Select date less than current date and time", target: self)

                        self.showToast(message: "Please Select End date Higher than current date and time")
                        txtEndDate.text = ""
                        selectedEndDate = ""
                        selectedEndDateFormatted = ""
                    }
                    else if checkstartDateandEndDate(StartDate: selectedStartDateFormatted
                        , endDate: selectedEndDateFormatted)
                    {
                        //                Helpers.showAlertDialog(message: "Please Select end date higher than startDate", target: self)
                        self.showToast(message: "Please Select end date higher than startDate")
                        txtEndDate.text = ""
                        selectedEndDate = ""
                        selectedEndDateFormatted = ""
                    }
                    else if selectedStartDateFormatted == selectedEndDateFormatted{
                        self.showToast(message: "Please Select different endDate and startDate ")
                        txtEndDate.text = ""
                        selectedEndDate = ""
                        selectedEndDateFormatted = ""
                    }
                    else
                    {


                        txtEndDate.text = Helpers.sambagTimeDisplaywithoutTime(dateAsString:"\(selectedEndDate)")

                    }
                    }
                    else
                    {
                        self.showToast(message: "Please Select valid date")
                        txtEndDate.text = ""
                        selectedEndDate = ""
                        selectedEndDateFormatted = ""

                    }

                

            }
            else
            {
                viewController.dismiss(animated: true, completion: nil)
                       let vc = SambagTimePickerViewController()
                       vc.theme = theme
                       vc.delegate = self
                       present(vc, animated: true, completion: nil)
            }
            
        }
       
    }
    
    func sambagDatePickerDidCancel(_ viewController: SambagDatePickerViewController) {
        if self.dateTag == 0
        {
//            txtStartDate.text = ""
//            selectedStartDate = ""
            
        }
        else
        {
            txtEndDate.text = ""
            selectedEndDate = ""
        }
        viewController.dismiss(animated: true, completion: nil)
    }
    
    
}


extension AddSignUpViewController: SambagTimePickerViewControllerDelegate {
    
    func sambagTimePickerDidSet(_ viewController: SambagTimePickerViewController, result: SambagTimePickerResult) {
        if self.timetag == 0
        {
            selectedStartDate += "   \(result)"
            print(selectedStartDate)
            
            self.selectedStartDateFormatted = self.convertDateFormat(date: selectedStartDate)
            
            print(selectedStartDateFormatted)
            self.timeCancelEnable = false
            
              if selectedStartDateFormatted != ""{
            if self.checkDateHigherThanCurrentDate(selectedDatestring: self.selectedStartDateFormatted)
            {
                // Helpers.showAlertDialog(message: "Please Select date less than current date and time", target: self)
                
                self.showToast(message: "Please Select Start date higher than current date and time")
                txtStartDate.text = ""
                selectedStartDate = ""
                self.selectedStartDateFormatted = ""
            }
            else
            {
                txtStartDate.text = Helpers.sambagTimeDisplay(dateAsString:"\(selectedStartDate)")
            }
            
        }
        else
        {
            self.showToast(message: "Please Select valid date")
            txtStartDate.text = ""
            selectedStartDate = ""
            self.selectedStartDateFormatted = ""
            
        }
        }
        else
        {
            selectedEndDate += "   \(result)"
            
            self.selectedEndDateFormatted = self.convertDateFormat(date: selectedEndDate)
            print(selectedEndDateFormatted)
            if selectedEndDateFormatted != ""
            {
            if self.checkDateHigherThanCurrentDate(selectedDatestring: self.selectedEndDateFormatted)
            {
                //                Helpers.showAlertDialog(message: "Please Select date less than current date and time", target: self)
                
                self.showToast(message: "Please Select End date Higher than current date and time")
                txtEndDate.text = ""
                selectedEndDate = ""
                selectedEndDateFormatted = ""
            }
            else if checkstartDateandEndDate(StartDate: selectedStartDateFormatted
                , endDate: selectedEndDateFormatted)
            {
                //                Helpers.showAlertDialog(message: "Please Select end date higher than startDate", target: self)
                self.showToast(message: "Please Select end date higher than startDate")
                txtEndDate.text = ""
                selectedEndDate = ""
                selectedEndDateFormatted = ""
            }
            else if selectedStartDateFormatted == selectedEndDateFormatted{
                self.showToast(message: "Please Select different endDate and startDate ")
                txtEndDate.text = ""
                selectedEndDate = ""
                selectedEndDateFormatted = ""
            }
            else
            {
                
                
                txtEndDate.text = Helpers.sambagTimeDisplay(dateAsString:"\(selectedEndDate)")
                
            }
            }
            else
            {
                self.showToast(message: "Please Select valid date")
                txtEndDate.text = ""
                selectedEndDate = ""
                selectedEndDateFormatted = ""
                
            }
            
        }
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func sambagTimePickerDidCancel(_ viewController: SambagTimePickerViewController) {
//            if self.timetag == 0
//            {
//                txtStartDate.text = ""
//                selectedStartDate += ""
//                self.selectedStartDateFormatted = ""
//
//            }
//            else
//            {
//                txtEndDate.text = ""
//                selectedEndDate += ""
//                self.selectedEndDateFormatted = ""
//            }

        
        
        
        
        if self.timetag == 0
        {
            selectedStartDate += ""
            print(selectedStartDate)

            self.selectedStartDateFormatted = self.convertDateFormatwithoutTime(date: "\(selectedStartDate) 00:00:00")

            print(selectedStartDateFormatted)

              if selectedStartDateFormatted != ""{
            if self.checkDateHigherThanCurrentDate(selectedDatestring: self.selectedStartDateFormatted)
            {
                // Helpers.showAlertDialog(message: "Please Select date less than current date and time", target: self)

                self.showToast(message: "Please Select Start date higher than current date and time")
                txtStartDate.text = ""
                selectedStartDate = ""
                self.selectedStartDateFormatted = ""
            }
            else
            {
                txtStartDate.text = Helpers.sambagTimeDisplaywithoutTime(dateAsString:"\(selectedStartDate)")
                timeCancelEnable  = true
            }

        }
        else
        {
            self.showToast(message: "Please Select valid date")
            txtStartDate.text = ""
            selectedStartDate = ""
            self.selectedStartDateFormatted = ""

        }
        }
        else
        {
//            selectedEndDate += "   00:00 AM"
            if !timeCancelEnable
            {
                self.showToast(message: "End time should be selected")
            }
            else
            {
                
           
            selectedEndDate += ""

            self.selectedEndDateFormatted = self.convertDateFormatwithoutTime(date: "\(selectedEndDate) 23:59:59")
            print(selectedEndDateFormatted)
            if selectedEndDateFormatted != ""
            {
            if self.checkDateHigherThanCurrentDate(selectedDatestring: self.selectedEndDateFormatted)
            {
                //                Helpers.showAlertDialog(message: "Please Select date less than current date and time", target: self)

                self.showToast(message: "Please Select End date Higher than current date and time")
                txtEndDate.text = ""
                selectedEndDate = ""
                selectedEndDateFormatted = ""
            }
            else if checkstartDateandEndDate(StartDate: selectedStartDateFormatted
                , endDate: selectedEndDateFormatted)
            {
                //                Helpers.showAlertDialog(message: "Please Select end date higher than startDate", target: self)
                self.showToast(message: "Please Select end date higher than startDate")
                txtEndDate.text = ""
                selectedEndDate = ""
                selectedEndDateFormatted = ""
            }
            else if selectedStartDateFormatted == selectedEndDateFormatted{
                self.showToast(message: "Please Select different endDate and startDate ")
                txtEndDate.text = ""
                selectedEndDate = ""
                selectedEndDateFormatted = ""
            }
            else
            {


                txtEndDate.text = Helpers.sambagTimeDisplaywithoutTime(dateAsString:"\(selectedEndDate)")

            }
            }
            else
            {
                self.showToast(message: "Please Select valid date")
                txtEndDate.text = ""
                selectedEndDate = ""
                selectedEndDateFormatted = ""

            }

        }
        }
        
        
        
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func convertDateFormat(date:String)->String
    {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"
        //        let convertedDate = formatter.date(from: "16-09-2017 2:00 PM")
        let convertedDate = formatter.date(from: date)
        
        formatter.timeZone = TimeZone(identifier: "UTC")
        let date = formatter.string(from: convertedDate!)
        print(date)
        let formatter2 = DateFormatter()
//        formatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
//        formatter2.dateFormat = "yyyy-MM-dd hh:mm:ss aa"
        formatter2.dateFormat = "yyyy-MM-dd HH:mm:ss"

        
        let convertedDate2 = formatter.date(from: date)
        formatter2.timeZone = TimeZone.current
        let date2 = formatter2.string(from: convertedDate2!)
        print(date2)
        
        //        let timestamp = convertedDate2!.timeIntervalSince1970
        //        print(timestamp)
        
        return date2
    }
    
    
        func convertTformatToOurFormat(date:String)->String
        {
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            //        let convertedDate = formatter.date(from: "16-09-2017 2:00 PM")
            let convertedDate = formatter.date(from: date)
            
            formatter.timeZone = TimeZone(identifier: "UTC")
            let date = formatter.string(from: convertedDate!)
            print(date)
            let formatter2 = DateFormatter()
    //        formatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    //        formatter2.dateFormat = "yyyy-MM-dd hh:mm:ss aa"
            formatter2.dateFormat = "yyyy-MM-dd HH:mm:ss"

            
            let convertedDate2 = formatter.date(from: date)
            formatter2.timeZone = TimeZone.current
            let date2 = formatter2.string(from: convertedDate2!)
            print(date2)
            
            //        let timestamp = convertedDate2!.timeIntervalSince1970
            //        print(timestamp)
            
            return date2
        }
        func convertDateFormatwithoutTime(date:String)->String
        {
            
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
            //        let convertedDate = formatter.date(from: "16-09-2017 2:00 PM")
            let convertedDate = formatter.date(from: date)
            
            formatter.timeZone = TimeZone(identifier: "UTC")
            let date = formatter.string(from: convertedDate!)
            print(date)
            let formatter2 = DateFormatter()
    //        formatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    //        formatter2.dateFormat = "yyyy-MM-dd hh:mm:ss aa"
            formatter2.dateFormat = "yyyy-MM-dd HH:mm:ss"

            
            let convertedDate2 = formatter.date(from: date)
            formatter2.timeZone = TimeZone.current
            let date2 = formatter2.string(from: convertedDate2!)
            print(date2)
            
            //        let timestamp = convertedDate2!.timeIntervalSince1970
            //        print(timestamp)
            
            return date2
        }
    
    
//    func convertDateFormatwithoutTime(date:String)->String
//       {
//
//           let formatter = DateFormatter()
//           formatter.dateFormat = "dd-MM-yyyy"
//           //        let convertedDate = formatter.date(from: "16-09-2017 2:00 PM")
//           let convertedDate = formatter.date(from: date)
//
//           formatter.timeZone = TimeZone(identifier: "UTC")
//           let date = formatter.string(from: convertedDate!)
//           print(date)
//           let formatter2 = DateFormatter()
//           formatter2.dateFormat = "yyyy-MM-dd"
//           let convertedDate2 = formatter.date(from: date)
//           formatter2.timeZone = TimeZone.current
//           let date2 = formatter2.string(from: convertedDate2!)
//           print(date2)
//
//           //        let timestamp = convertedDate2!.timeIntervalSince1970
//           //        print(timestamp)
//
//           return date2
//       }
       
    func convertTimetoTimestamp(date:String)->Int
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"
        //        let convertedDate = formatter.date(from: "16-09-2017 2:00 PM")
        let convertedDate = formatter.date(from: date)
        
        formatter.timeZone = TimeZone(identifier: "UTC")
        let date = formatter.string(from: convertedDate!)
        print(date)
        let convertedDate2 = formatter.date(from: date)
        
        let timestamp = convertedDate2!.timeIntervalSince1970
        print(timestamp)
        
        //        return "\(timestamp)"
        return Int(timestamp)
        
    }
    
    func compairDateWithCurrentDate (selectedDatestring:String)->Bool
    {
        print(selectedDatestring)
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"
        
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        let convertedDateNow = formatter.string(from: Date())
        let convertedDateNow2 = formatter.date(from: convertedDateNow)
        formatter2.timeZone = TimeZone.current
        let dateNow = formatter2.string(from: convertedDateNow2!)
        print(dateNow)
        print(selectedDatestring == dateNow)
        return selectedDatestring == dateNow
    }
    
    func checkDateHigherThanCurrentDate (selectedDatestring:String)->Bool
    {
        print(selectedDatestring)
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"
        
        let formatter2 = DateFormatter()
//        formatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        formatter2.dateFormat = "yyyy-MM-dd HH:mm:ss"

        
        let convertedDateNow = formatter.string(from: Date())
        let convertedDateNow2 = formatter.date(from: convertedDateNow)
        formatter2.timeZone = TimeZone.current
        let dateNow = formatter2.string(from: convertedDateNow2!)
        print(dateNow)
        print(selectedDatestring < dateNow)
        return selectedDatestring < dateNow
    }
    
    func checkstartDateandEndDate (StartDate:String , endDate:String)->Bool
    {
        print(StartDate)
        print(endDate)
        print(StartDate > endDate)
        return StartDate > endDate
    }
    
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: 20, y: self.view.frame.size.height/2-30, width: self.view.frame.width-40, height: 60))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.numberOfLines = 2
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 10.0)
        toastLabel.adjustsFontSizeToFitWidth = true
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 6.0, delay: 0.3, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}
