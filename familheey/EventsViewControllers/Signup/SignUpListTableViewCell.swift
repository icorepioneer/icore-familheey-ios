//
//  SignUpListTableViewCell.swift
//  familheey
//
//  Created by Innovation Incubator on 27/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class SignUpListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgEdit: UIImageView!
    @IBOutlet weak var lblSlotsFill: UILabel!
    @IBOutlet weak var imgViewOfGreenTick: UIImageView!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var btnViewSignUps: UIButton!
    @IBOutlet weak var lblSlotAvailabilty: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblSlotName: UILabel!
    
    @IBOutlet weak var viewOfBorder: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
