//
//  EventListTableViewCell.swift
//  familheey
//
//  Created by Innovation Incubator on 15/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class EventListTableViewCell: UITableViewCell {

    @IBOutlet weak var imgOfEventProPic: UIImageView!
    @IBOutlet weak var lblEventName: UILabel!
    @IBOutlet weak var lblEventType: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblTo: UILabel!
    
    @IBOutlet weak var imgLocation: UIImageView!
    
    @IBOutlet weak var lblSharable: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblFrom: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
