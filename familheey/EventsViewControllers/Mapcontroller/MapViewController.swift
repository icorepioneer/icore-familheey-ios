//
//  MapViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 28/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController ,MKMapViewDelegate,UIGestureRecognizerDelegate{

    var startLat:Double!
    var startLong:Double!
    var endLat:Double!
    var endLong:Double!

    
    @IBOutlet weak var mapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()

        mapView.delegate = self
//        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
//        gestureRecognizer.delegate = self
//        mapView.addGestureRecognizer(gestureRecognizer)
        
        print(self.endLat as Double)
        print(self.endLong as Double)
       
        
        if startLat != nil && startLong != nil && endLong != nil && endLat != nil{
            self.showRouteOnMap(pickupCoordinate: CLLocationCoordinate2DMake(startLat, startLong), destinationCoordinate: CLLocationCoordinate2DMake(endLat, endLong))
        }
        else{
            self.displayAlert(alertStr: "Unable to find location, Please turn on the location in the settings", title: "")
        }
        // Do any additional setup after loading the view.
    }
    
    //MARK: - Button actions
    @IBAction func btnBackOnClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDirectionOnClick(_ sender: Any) {
        
        if startLat != nil && startLong != nil && endLong != nil && endLat != nil{
                
            let source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: self.startLat, longitude: self.startLong)))
                  source.name = "Source"

                  let destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: self.endLat, longitude: self.endLong)))
                  destination.name = "Destination"

                  MKMapItem.openMaps(with: [source, destination], launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
             }
             else{
                 self.displayAlert(alertStr: "Unable to find location, Please try again", title: "")
             }
      }
      
    // MARK: - showRouteOnMap
    
    func showRouteOnMap(pickupCoordinate: CLLocationCoordinate2D, destinationCoordinate: CLLocationCoordinate2D) {
        
        
        
        let sourcePlacemark = MKPlacemark(coordinate: pickupCoordinate, addressDictionary: nil)
        let destinationPlacemark = MKPlacemark(coordinate: destinationCoordinate, addressDictionary: nil)
        
        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
        
        let sourceAnnotation = MKPointAnnotation()
        
        if let location = sourcePlacemark.location {
            sourceAnnotation.coordinate = location.coordinate
        }
        
        let destinationAnnotation = MKPointAnnotation()
        
        if let location = destinationPlacemark.location {
            destinationAnnotation.coordinate = location.coordinate
        }
        
        self.mapView.showAnnotations([sourceAnnotation,destinationAnnotation], animated: true )
        
        let directionRequest = MKDirections.Request()
        directionRequest.source = sourceMapItem
        directionRequest.destination = destinationMapItem
        directionRequest.transportType = .automobile
        
        // Calculate the direction
        let directions = MKDirections(request: directionRequest)
        
        directions.calculate {
            (response, error) -> Void in
            
            guard let response = response else {
                if let error = error {
                    print("Error: \(error)")
                }
                
                return
            }
            
            let route = response.routes[0]
            
            self.mapView.addOverlay((route.polyline), level: MKOverlayLevel.aboveRoads)
            
            let rect = route.polyline.boundingMapRect
            self.mapView.setRegion(MKCoordinateRegion(rect), animated: true)
        }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
    
//    @objc func handleTap(_ gestureReconizer: UILongPressGestureRecognizer)
//    {
//
//        let location = gestureReconizer.location(in: mapView)
//        let coordinate = mapView.convert(location,toCoordinateFrom: mapView)
//
//
//
//        // Add annotation:
//        let annotation = MKPointAnnotation()
//        annotation.coordinate = coordinate
//        mapView.addAnnotation(annotation)
//    }
    // MARK: - MKMapViewDelegate
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        let renderer = MKPolylineRenderer(overlay: overlay)
        
        renderer.strokeColor = UIColor(red: 17.0/255.0, green: 147.0/255.0, blue: 255.0/255.0, alpha: 1)
        
        renderer.lineWidth = 5.0
        
        return renderer
    }
}
