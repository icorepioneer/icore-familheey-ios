//
//  EventUpdateViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 17/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import CropViewController
import Moya
import SwiftyJSON
import UserNotifications
import Alamofire
import CoreLocation
import MapKit
import ActiveLabel


class EventDetailsViewController: UIViewController ,CropViewControllerDelegate ,UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UITextViewDelegate, CLLocationManagerDelegate, contactShowCallBackDelegate {
    
    @IBOutlet weak var grdView: UIView!
    @IBOutlet weak var tempView: UIView!
    @IBOutlet weak var lblHostedBy: UILabel!
    @IBOutlet weak var lblLocation: ActiveLabel!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var lblEventCreator: UILabel!
    @IBOutlet weak var btnFree: UIButton!
    @IBOutlet weak var lblEventType: UILabel!
    @IBOutlet weak var lblEventName: UILabel!
    @IBOutlet weak var btnCategory: UILabel!
    @IBOutlet weak var imgOfEvents: UIImageView!
    @IBOutlet weak var imgCoverPicEdit: UIImageView!
    @IBOutlet weak var rsvpBtmView: UIView!
    @IBOutlet weak var lblEventTime: UILabel!
    @IBOutlet weak var btnCoverPic: UIButton!
    @IBOutlet weak var topVwTopConstrain                    : NSLayoutConstraint!
    @IBOutlet weak var collVw                               : UICollectionView!
    @IBOutlet weak var backgrounfVw                         : UIView!
    @IBOutlet weak var txtVw                                : UITextView!
    @IBOutlet weak var backbroundVwHeightConstrain          : NSLayoutConstraint!
    @IBOutlet weak var scrollOuterVwHeightConstrain         : NSLayoutConstraint!
    @IBOutlet weak var inviteButt                           : UIButton!
    @IBOutlet weak var goingButt                            : UIButton!
    @IBOutlet weak var notInterestedButt                    : UIButton!
    @IBOutlet weak var interestedButt                       : UIButton!
    @IBOutlet weak var morePeopleBlackVw                    : UIView!
    @IBOutlet weak var morePeopleField                      : UITextField!
    @IBOutlet weak var queariesLbl                          : UILabel!
    @IBOutlet weak var addQueariesButt                      : UIButton!
    @IBOutlet weak var shareButt                            : UIButton!
    @IBOutlet weak var imgCalndr                            : UIImageView!
    @IBOutlet weak var btnEditEvent: UIButton!
    @IBOutlet weak var calendarButt                         : UIButton!
    @IBOutlet weak var doneButt                             : UIButton!
    @IBOutlet weak var changeButt                           : UIButton!
    @IBOutlet weak var pleaseEnterLbl                       : UILabel!
    @IBOutlet weak var btmVwHeightConstrain                 : NSLayoutConstraint!
    @IBOutlet weak var backWhiteVwHeightConstrain           : NSLayoutConstraint!
    @IBOutlet weak var dateTimeLbl: UILabel!
    @IBOutlet weak var rsvpView: UIStackView!
    @IBOutlet weak var blckBgView: UIControl!
    @IBOutlet weak var bgClrView: UIView!
    @IBOutlet weak var whitView: UIView!
    @IBOutlet weak var btmView: UIView!
    @IBOutlet weak var lblEventUrl: UILabel!
    @IBOutlet weak var btnShareEventUrl: UIButton!
    @IBOutlet weak var imgShareEventUrl: UIImageView!
    @IBOutlet weak var lblRsvpReqHead: UILabel!
    @IBOutlet weak var onlineView: UIView!
    @IBOutlet weak var imgLoctnorUrl: UIImageView!
    @IBOutlet weak var btnMap: UIButton!
    @IBOutlet weak var DialNumView: UIView!
    @IBOutlet weak var dial_height: NSLayoutConstraint!
    @IBOutlet weak var desc_top: NSLayoutConstraint!
    @IBOutlet weak var lblDialInNumb: UILabel!
    @IBOutlet weak var lblPartiPin: UILabel!
    @IBOutlet weak var imgMeetingLogo: UIImageView!
    
    let reminderTimeTitleStrings = ["1 Hour before","2 Hours before","5 Hours before","24 hours before"]
    
    var startLat:Double!
    var startLong:Double!
    var endLat:Double!
    var endLong:Double!
    var original:UIImage!
    var reminderID : String?
    var eventId : String!
    var eventDetails = JSON()
    var rsvpArr = JSON()
    var contactArr = JSON()
    var isFromNotification = "false"
    var imagePicker = UIImagePickerController()
    var fromCreate = false
    var isFromSearch = false
    var isFrom = ""
    var groupId = ""
    var faEventCreate = false
    var donePressed = false
    var fromCalendar = false
    var calnderId = ""
    let locationManager = CLLocationManager()
    var currentLocation : String!
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    let notificationCenter                                  = UNUserNotificationCenter.current()
    var triggerDate                                         = Date()
    var amCreator                                           : Bool!
    var pastEvents:Bool = false
    var titleArr                                            : [String]!
    var imgEventData = NSData()
    var originalData = NSData()
    var enableEventEdit:Bool! = false
    var hideOtherCountView:Bool! = false
    var selectedImage = UIImage()
    var eventPublicstring:String!
    var otherCount = ""
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    @IBOutlet weak var textviewDialInNumber: UITextView!
    
    @IBOutlet weak var textViewDialInPin: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        blckBgView.isHidden = true
        tempView.isHidden = false
        changeButt.underline()
        self.tabBarController?.tabBar.isHidden = true
//        self.tabBarController?.tabBar.isTranslucent = true
        
        self.calendarButt.frame = CGRect(x: self.calendarButt.frame.origin.x, y: self.calendarButt.frame.origin.y, width: self.calendarButt.frame.height, height: self.calendarButt.frame.height)
        
        
        if DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_XP{
            
            topVwTopConstrain.constant   = 40
        }else{
            
            topVwTopConstrain.constant   = 50
        }
        
        if fromCreate{
            if faEventCreate{
                self.eventId = eventDetails["id"].stringValue
            }
            else{
                self.eventId = eventDetails["id"].stringValue
            }
        }
        else if fromCalendar{
            self.eventId = self.calnderId
        }
        else if isFromNotification.lowercased() == "true"{
            
        }
        else{
            if isFromSearch{
            }
            else{
                self.eventId = eventDetails["event_id"].stringValue
            }
        }
        
        if self.enableEventEdit{
            btnEditEvent.isHidden = false
        }
        else
        {
            btnEditEvent.isHidden = true
            
        }
        
        blckBgView.isHidden = true
        rsvpBtmView.isHidden = true
        shareButt.isHidden = true
        btnFree.isHidden = true
        //  getEventDetailsById(eventID: self.eventId)
        
        self.bgClrView.clipsToBounds = true
        self.bgClrView.layer.cornerRadius = 15
        self.bgClrView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        self.whitView.clipsToBounds = true
        self.whitView.layer.cornerRadius = 15
        self.whitView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        /* let gradient: CAGradientLayer = CAGradientLayer()
         
         // gradient.colors = [UIColor.init(white: 0, alpha: 3).cgColor, UIColor.init(white: 0, alpha: 0).cgColor]
         gradient.colors = [UIColor.black.cgColor, UIColor.clear.cgColor]
         //      gradient.locations = [0.0,1.0]
         //        gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
         //        gradient.endPoint = CGPoint(x: 1.0, y: 0.0)
         gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.grdView.frame.size.width+50, height: self.grdView.frame.size.height)
         self.grdView.layer.insertSublayer(gradient, at: 0)*/
        
        lblLocation.handleURLTap { (url) in
            print(url)
            self.showActionSheet(titleArr: ["Open URL","Copy URL","Share URL"], title: "Select option") { (index) in
                if index == 100{}
                else if index == 0{
                    UIPasteboard.general.string = self.eventDetails["meeting_pin"].stringValue
                    self.showToast(message: "Participant pin copied to clipboard")
                    DispatchQueue.main.asyncAfter(deadline: .now()+3) {
                        UIApplication.shared.open(url)
                    }
                    
                }
                else if index == 1{
                    UIPasteboard.general.string = url.absoluteString
                }
                else if index == 2{
                    let textToShare = [ url.absoluteString ]
                    let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                    activityViewController.popoverPresentationController?.sourceView = self.view
                    self.present(activityViewController, animated: true, completion: nil)
                }
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        getEventDetailsById(eventID: self.eventId)
        
    }
    
    
    
    //MARK:- contactShow delegate method
    
    func callbackforRefreshData(){
        getEventDetailsById(eventID: self.eventId)
        
    }
    
    func callToAddContact(){
        let stryboard = UIStoryboard.init(name: "fourth", bundle: nil)
        let vc = stryboard.instantiateViewController(withIdentifier: "AddContactsQueryViewController") as! AddContactsQueryViewController
        vc.eventId = self.eventId
        vc.isFrom = "edit"
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    //MARK:- UITextViewDelegates
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Add description" {
            textView.text = ""
            textView.textColor = UIColor.darkGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "Add description"
            textView.textColor = UIColor.lightGray
        }
    }
    
    @IBAction func btnCoverPick_Change_onclick
        (_ sender: UIButton) {
        //let button = sender as! UIButton
        
        /*  let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
         alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
         self.openCamera()
         }))
         
         alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
         self.openGallary()
         }))
         
         
         alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
         self.present(alert, animated: true, completion: nil)*/
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: "ChangePicturesViewController") as! ChangePicturesViewController
        //vc.isFrom = "events"
        vc.isCoverOrProfilePic = "CoverPic"
        vc.ids           = UserDefaults.standard.value(forKey: "userId") as! String
        vc.isFrom = "events"
        vc.inFor = "cover"
        vc.pastevents = self.pastEvents
        if amCreator{
            vc.isAdmin = "admin"
        }
        // vc.eventId = eventDetails[""].stringValue
        vc.eventId = self.eventId
        // vc.delegate = self  event_image
        
        if eventDetails["event_original_image"].stringValue.count != 0{
            let temp = "\(Helpers.imageURl)"+"\(BaseUrl.event_original)"+eventDetails["event_original_image"].stringValue
            
            vc.imageUrl   = temp
            
        }else{
            if eventDetails["event_image"].stringValue.count != 0{
                let temp = "\(Helpers.imageURl)"+"\(BaseUrl.event_image)"+eventDetails["event_image"].stringValue
                
                vc.imageUrl   = temp
            }
            else{
                vc.imageUrl = ""
            }
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewWillLayoutSubviews() {
        
        backgrounfVw.roundCorners(corners: [.topLeft, .topRight], radius: 25)
        backgrounfVw.clipsToBounds = true
    }
    
    //MARK:- WEB API
    
    func getEventDetailsById(eventID:String){
        let parameter = ["user_id":UserDefaults.standard.value(forKey: "userId") as! String, "event_id":eventId!] as [String : Any]
        
        ActivityIndicatorView.show("Loading....")
        
        networkProvider.request(.getEventDetails(parameter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    
                    if response.statusCode == 200{
                        self.tempView.isHidden = true
                        
                        self.eventDetails = jsonData["data"]["event"][0]
                        self.rsvpArr = jsonData["data"]["rsvp"]
                        self.contactArr = jsonData["data"]["contacts"]
                        
                        if self.eventDetails.count == 0{
                            ActivityIndicatorView.hiding()
                            self.displayAlertChoice(alertStr: "Oops! This content is no longer available.", title: "", completion: { (result) in
                                self.navigationController?.popViewController(animated: true)
                            })
                            return
                        }
                        
                        if UserDefaults.standard.value(forKey: "userId") as! String == self.eventDetails["created_by"].stringValue
                        {
                            
                            self.amCreator = true
                        }
                        else
                        {
                            self.amCreator = false
                            
                        }
                        var tDate = ""
                        
                        if self.eventDetails["to_date"].stringValue.count > 0{
                            tDate = self.convertTimeStampString(timestamp: self.eventDetails["to_date"].stringValue)
                            
                            
                            if self.checkDateLessThanCurrentDate(selectedDatestring:tDate ){
                                self.pastEvents = true
                            }
                            else{
                                self.pastEvents = false
                            }
                        }
                        
                        self.endLat = self.eventDetails["lat"].doubleValue
                        self.endLong = self.eventDetails["long"].doubleValue
                        
                        self.setupView()
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                            
                            self.setupView()
                            
                        })
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                            
                            self.setupView()
                            
                        })
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {
                            
                            self.setupView()
                            
                        })
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getEventDetailsById(eventID: eventID)
                        }
                    }
                    else{
                        self.displayAlertChoice(alertStr: "Oops! This content is no longer available.", title: "", completion: { (result) in
                            self.navigationController?.popViewController(animated: true)
                        })
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }
                
            case .failure( let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                self.navigationController?.popViewController(animated: true)
                break
            }
        }
    }
    
    func onClickResponseToRSVP(status:String, otherCount:String){
        
        var parameter = [String:Any]()
        if donePressed{
            parameter = ["user_id":UserDefaults.standard.value(forKey: "userId") as! String, "event_id":eventId as String, "resp":status, "others_count":otherCount,"created_by":self.eventDetails["created_by"].stringValue,"guest_count_update":true] as [String : Any]
        }
        else{
            parameter = ["user_id":UserDefaults.standard.value(forKey: "userId") as! String, "event_id":eventId as String, "resp":status, "others_count":otherCount,"created_by":self.eventDetails["created_by"].stringValue] as [String : Any]
        }
        
        ActivityIndicatorView.show("Loading....")
        
        networkProvider.request(.respondToRSVP(parameter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    ActivityIndicatorView.hiding()
                    if response.statusCode == 200{
                        
                        
                        //                        let result = jsonData["data"]
                        //                        var temp = self.rsvpArr[0]
                        //                        temp = result
                        //
                        //                        self.rsvpArr = temp
                        
                        
                        if status == "going"{
                            self.imgCalndr.image = #imageLiteral(resourceName: "menu")
                            self.calendarButt.setImage(UIImage(named: ""), for: .normal)
                            self.blckBgView.isHidden = false
                            
                            if self.donePressed
                            {
                                
                                let result = jsonData["data"]
                                var temp = self.rsvpArr[0]
                                temp = result
                                
                                self.rsvpArr[0]["others_count"] = result["others_count"]
                                
                                self.blckBgView.isHidden = true
                                
                            }
                            else
                            {
                                self.blckBgView.isHidden = false
                            }
                            if self.eventDetails["allow_others"].stringValue == "true"{
                                self.btmVwHeightConstrain.constant        = 175
                                self.backWhiteVwHeightConstrain.constant  = 350
                                
                                
                                self.pleaseEnterLbl.isHidden              = false
                                self.morePeopleField.isHidden             = false
                                self.doneButt.isHidden                    = false
                            }else{
                                
                                self.btmVwHeightConstrain.constant        = 0
                                self.backWhiteVwHeightConstrain.constant  = 175
                                
                                
                                self.pleaseEnterLbl.isHidden              = true
                                self.morePeopleField.isHidden             = true
                                self.doneButt.isHidden                    = true
                            }
                            
                            
                            
                            //                            self.setRemainder()
                            /*      if self.donePressed{
                             DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: { //commented by Giri
                             
                             self.onClickHide(self)
                             })
                             } */
                            
                        }else if status == "interested"{
                            self.imgCalndr.image = #imageLiteral(resourceName: "menu.png")
                            self.calendarButt.setImage(UIImage(named: ""), for: .normal)
                            self.blckBgView.isHidden = false
                            
                            self.btmVwHeightConstrain.constant        = 0
                            self.backWhiteVwHeightConstrain.constant  = 175
                            
                            self.pleaseEnterLbl.isHidden              = true
                            self.morePeopleField.isHidden             = true
                            self.doneButt.isHidden                    = true
                            
                            
                            //                            self.setRemainder()
                            /*    DispatchQueue.main.asyncAfter(deadline: .now() + 5, execute: { //commented by Giri
                             
                             self.onClickHide(self)
                             })
                             */
                        }else{
                            self.imgCalndr.image = #imageLiteral(resourceName: "ic_calendar_")
                            self.calendarButt.setImage(UIImage(named: ""), for: .normal)
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.onClickResponseToRSVP(status: status, otherCount: otherCount)
                        }
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }
                //                catch let err {
                //                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
            //                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
            
            ActivityIndicatorView.hiding()
        }
        
    }
    
    
    //MARK: - Time Stamp
    
    
    
    func checkDateLessThanCurrentDate (selectedDatestring:String)->Bool{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mma"
        
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "dd/MMM/yyyy hh:mma"
        
        let convertedDateNow = formatter.string(from: Date())
        let convertedDateNow2 = formatter.date(from: convertedDateNow)
        //        formatter2.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        formatter2.locale = .current
        let dateNow = formatter2.string(from: convertedDateNow2!)
        
        let temp = formatter2.date(from: selectedDatestring)
        
        let order = Calendar.current.compare(temp!, to: Date(), toGranularity: .hour)
        
        switch order {
        case .orderedAscending:
            print("true")
            return true
        case .orderedDescending:
            print("false")
            return false
        case .orderedSame:
            print("same")
            return false
            
        }
    }
    
    //MARK:- Custom Methods
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: 20, y: self.view.frame.size.height/2-30, width: self.view.frame.width-40, height: 60))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.numberOfLines = 2
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 10.0)
        toastLabel.adjustsFontSizeToFitWidth = true
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 6.0, delay: 0.3, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else{
            // self.displayAlert(alertStr: "You don't have camera", title: "")
        }
    }
    
    func openGallary(){
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func selectedImage(img:UIImage){
        // let cropView = CropViewController(image: img)
        original = img
        let cropView = CropViewController(croppingStyle: .default, image: img)
        cropView.aspectRatioLockEnabled = true
        cropView.aspectRatioLockDimensionSwapEnabled = true
        cropView.aspectRatioPickerButtonHidden = true
        // cropView.customAspectRatio = CGSize(width: 4, height: 3)
        cropView.aspectRatioPreset = TOCropViewControllerAspectRatioPreset.preset4x3
        cropView.delegate = self
        self.navigationController?.pushViewController(cropView, animated: true)
    }
    
    func editEvent(){
        let alert = UIAlertController(title: "Familheey", message: "Do you want to Edit this Event?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { _ in
            let stryboard = UIStoryboard.init(name: "third", bundle: nil)
            
            let vc = stryboard.instantiateViewController(withIdentifier: "CreateEventStep1ViewController") as! CreateEventStep1ViewController
            //        vc.typeOfInvitations = "invitation"
            //        vc.eventId = eventId
            vc.eventDetails = self.eventDetails
            vc.fromEventEdit = true
            vc.eventId = self.eventId
            
            self.navigationController?.pushViewController(vc, animated: true)
            
        }))
        alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func cancelEvent(index:Int){
        
        if index == 1{
            deleteorCancel(index: index)
        }
        else{
            let alert = UIAlertController(title: "Familheey", message: "Do you want to Delete this Event?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "DELETE", style: .destructive, handler: { _ in
                self.deleteorCancel(index: index)
            }))
            alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    func deleteorCancel(index:Int){
        var param = [String:String]()
        if index == 1{
            param = ["event_id" : self.eventId, "user_id": UserDefaults.standard.value(forKey: "userId") as! String, "status":"cancel"]
        }
        else{
            param = ["event_id" : self.eventId, "user_id": UserDefaults.standard.value(forKey: "userId") as! String, "status":"delete"]
            
        }
        ActivityIndicatorView.show("Please wait....")
        networkProvider.request(.event_deleteorcancel(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    if response.statusCode == 200{
                        self.navigationController?.popToViewController(ofClass: EventsTabViewController.self)
                        if index == 1{
                            self.displayAlert(alertStr: "Event cancel successfully", title: "")
                        }
                        else{
                            self.displayAlert(alertStr: "Event deleted successfully", title: "")
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.deleteorCancel(index: index)
                        }
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
        
    }
    
    //MARK: - St up view
    
    func setupView(){
        
        rsvpBtmView.isHidden = false
        
        self.lblEventUrl.text = eventDetails["event_url"].stringValue ?? ""
        
        if eventDetails["is_public"] == true{
            eventPublicstring = "Public"
            if eventDetails["is_sharable"].stringValue == "true"{
                shareButt.isHidden = false
                btnFree.isHidden = false
            }
            else{
                shareButt.isHidden = true
                btnFree.isHidden = true
            }
        }
        else{
            eventPublicstring = "Private"
            if eventDetails["is_sharable"].stringValue == "true"{
                shareButt.isHidden = false
                btnFree.isHidden = false
            }
            else{
                shareButt.isHidden = true
                btnFree.isHidden = true
            }
        }
        
        if eventDetails["event_type"] == "Regular"{
            eventPublicstring += " : Regular"
            titleArr = ["DETAILS", "AGENDA", "GUESTS","ALBUMS","DOCUMENTS"]
        }
        else if eventDetails["event_type"].stringValue.lowercased() == "online"{
            eventPublicstring += " : Online"
            titleArr = ["DETAILS", "AGENDA", "GUESTS","ALBUMS","DOCUMENTS"]
        }
        else{
            eventPublicstring += " : Sign Up event"
            titleArr = ["DETAILS","AGENDA", "GUESTS", "SIGN UP","ALBUMS","DOCUMENTS"]
        }
        
        if eventDetails["rsvp"].boolValue{
            self.lblRsvpReqHead.isHidden = false
            self.rsvpView.isHidden = false
        }
        else{
            self.lblRsvpReqHead.isHidden = true
            self.rsvpView.isHidden = true
        }
        
        //        if eventDetails["rsvp"].stringValue.lowercased() == "true"{
        if eventDetails["created_by"].stringValue == UserDefaults.standard.value(forKey: "userId") as! String{
            self.rsvpView.isHidden = true
            self.morePeopleBlackVw.isHidden = true
            
            self.inviteButt.isHidden   = false
            // self.shareButt.isHidden    = false
        }
        else{
            self.rsvpView.isHidden = false
            if eventDetails["allow_others"].stringValue.lowercased() == "true"{
                self.morePeopleBlackVw.isHidden = false
                
                self.inviteButt.isHidden   = true
                //   self.shareButt.isHidden    = true
            }
            else{
                self.morePeopleBlackVw.isHidden = true
                
                self.inviteButt.isHidden        = true
                //  self.shareButt.isHidden         = true
            }
        }
        
        collVw.delegate = self
        collVw.dataSource = self
        
        lblEventType.text              = eventPublicstring!
        
        btnCategory.text               = "  \(eventDetails["category"].stringValue.firstCapitalized)  "
        
        
        if eventDetails["created_by_name"].stringValue.lowercased() == eventDetails["event_host"].stringValue.lowercased(){
            lblEventCreator.text              = "Created by \(eventDetails["created_by_name"].stringValue.firstCapitalized)"
            lblHostedBy.text              = "Hosted by \(eventDetails["event_host"].stringValue.firstCapitalized)"
            lblEventCreator.isHidden = true
        }
        else{
            lblEventCreator.text              = "Created by \(eventDetails["created_by_name"].stringValue.firstCapitalized)"
            lblHostedBy.text              = "Hosted by \(eventDetails["event_host"].stringValue.firstCapitalized)"
            lblEventCreator.isHidden = false
        }
        
        txtVw.text                     = eventDetails["description"].stringValue
        lblEventName.text              = eventDetails["event_name"].stringValue
        
        let event_pic = (eventDetails["event_image"].stringValue)
        if event_pic != "" && event_pic != "undefined" {
            let temp = "\(Helpers.imageURl)"+"\(BaseUrl.event_image)"+event_pic
            let imgUrl = URL(string: temp)
            imgOfEvents.kf.indicatorType  = .activity
            //            let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=475&height=357&url="+imgUrl!.relativeString
            
            let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=800&height=600&url="+imgUrl!.relativeString
            
            let url = URL(string: newUrlStr)
            imgOfEvents.kf.indicatorType = .activity
            
            imgOfEvents.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
            
        }
        else{
            
            imgOfEvents.image = #imageLiteral(resourceName: "default_event_image.jpg")
        }
        
        let fromDate                   = convertTimeStamp(val: eventDetails["from_date"].doubleValue)
        let toDate                     = convertTimeStamp(val: eventDetails["to_date"].doubleValue)
        
        var fDate = ""
        var tDate = ""
        var fdateOnly = [String]()
        var tdateOnly = [String]()
        var fDateArray = [String]()
        var tDateArray = [String]()
        var fYear = [String]()
        var tYear = [String]()
        
        if eventDetails["from_date"].stringValue.count > 0{
            fDate = convertTimeStampString(timestamp: eventDetails["from_date"].stringValue)
            fdateOnly = fDate.components(separatedBy: " ")
            fDateArray = fDate.components(separatedBy: "/")
            fYear = fDateArray[2].components(separatedBy: " ")
        }
        if eventDetails["to_date"].stringValue.count > 0{
            tDate = convertTimeStampString(timestamp: eventDetails["to_date"].stringValue)
            tdateOnly = tDate.components(separatedBy: " ")
            tDateArray = tDate.components(separatedBy: "/")
            tYear = tDateArray[2].components(separatedBy: " ")
        }
        
        if fdateOnly.count > 0 && tdateOnly.count > 0{
            
            if fdateOnly[0] == tdateOnly[0]{
                lblDateTime.text = "\(filterDayOnly(timestamp: eventDetails["from_date"].stringValue)), \(fDateArray[1]) \(fDateArray[0]) \(fYear[0])"
            }
            else{
                if fYear[0] == tYear[0]{
                    lblDateTime.text = "\(filterDayOnly(timestamp: eventDetails["from_date"].stringValue)), \(fDateArray[1]) \(fDateArray[0]) - \(filterDayOnly(timestamp: eventDetails["to_date"].stringValue)), \(tDateArray[1]) \(tDateArray[0]), \(tYear[0])"
                }
                else{
                    lblDateTime.text = "\(filterDayOnly(timestamp: eventDetails["from_date"].stringValue)), \(fDateArray[1]) \(fDateArray[0]) \(fYear[0])  - \(filterDayOnly(timestamp: eventDetails["to_date"].stringValue)), \(tDateArray[1]) \(tDateArray[0]) \(tYear[0])"
                }
            }
        }
        
        let fromTime                       = "\(fromDate.time.components(separatedBy: "at").last!) - \(toDate.time.components(separatedBy: "at").last!)"
        lblEventTime.text                  = fromTime
        
        txtVw.delegate                     = self
        
        if txtVw.text == ""{
            
            txtVw.text                     = "Add description"
            txtVw.textColor                = UIColor.lightGray
            txtVw.returnKeyType            = .done
        }
        
        btnCategory.layer.cornerRadius     = 5
        btnCategory.clipsToBounds          = true
        
        
        //            self.txtVw.translatesAutoresizingMaskIntoConstraints = true
        //            self.txtVw.sizeToFit()
        //        
        
        
        var dateVal                       = convertTimeStamp2(val: eventDetails["from_date"].doubleValue)
        dateVal                           = Helpers.dateWithOutTime(datDate: dateVal)
        
        var timeStr                       = fromDate.time.components(separatedBy: "at").last!
        
        timeStr                           = Helpers.convert12Hourto24HourFormat(val: timeStr)
        let hour                          = Int(timeStr.components(separatedBy: ":").first!)
        let minute                        = Int(timeStr.components(separatedBy: ":")[1])
        dateVal                           = dateVal.adding(hour: hour!, minutes: minute!)
        
        let dayBefore                     = dateVal.adding(hour: 0, minutes: -10)
        triggerDate = dayBefore
        
        if let remind_on = eventDetails["remind_on"].string{
            dateTimeLbl.text =  EventsTabViewController.convertTimeStampToDate(timestamp: remind_on)
        }
        else{
            dateTimeLbl.text = changeFormatee(Datee: dayBefore)
        }
        
        dateVal                           = Helpers.dateWithOutTime(datDate: dayBefore)
        let timeBefore = dateVal.adding(hour: 0, minutes: -15)
        
        dateVal                           = dateVal.adding(hour: hour!, minutes: minute!)
        let dd                            = convertDateAndTime(dat: dateVal)
        
        if eventDetails["event_type"].stringValue.lowercased() == "online"{
            let formatter2 = DateFormatter()
            formatter2.dateFormat = "dd/MMM/yyyy hh:mma"
            
            let from = NSDate(timeIntervalSince1970: eventDetails["from_date"].doubleValue)
            let to = NSDate(timeIntervalSince1970: eventDetails["to_date"].doubleValue)
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyyMMddHHmm"
            dateFormatter.timeZone = .current
            let datefrom = Int(dateFormatter.string(from:from as Date))
            let dateto = Int(dateFormatter.string(from:to as Date))
            let todayDate = Int(dateFormatter.string(from: NSDate() as Date))
            
            if datefrom != nil && dateto != nil && todayDate != nil{
                if (datefrom! - 15) <= todayDate! && dateto! >= todayDate!{
                    onlineView.isHidden = false
                }
                else{
                    onlineView.isHidden = true
                }
            }
            else{
                onlineView.isHidden = true
                
            }
            
            btnMap.isHidden = true
            self.DialNumView.isHidden = false
            //            self.dial_height.constant = 120
            
            if eventDetails["location"].stringValue.isEmpty{
                lblLocation.text               = eventDetails["meeting_link"].stringValue
                //                imgLoctnorUrl.image = #imageLiteral(resourceName: "pointer_icon")
            }
            else{
                lblLocation.text               = "Unknown location"
                imgLoctnorUrl.image = #imageLiteral(resourceName: "ic_location_details_")
            }
            textviewDialInNumber.text = eventDetails["meeting_dial_number"].stringValue
            textviewDialInNumber.textContainer.lineFragmentPadding = 0
            textviewDialInNumber.textContainerInset = .zero
            textViewDialInPin.textContainer.lineFragmentPadding = 0
            textViewDialInPin.textContainerInset = .zero
            
            lblDialInNumb.text = "" //eventDetails["meeting_dial_number"].stringValue
            lblPartiPin.text = "" //eventDetails["meeting_pin"].stringValue
            textViewDialInPin.text = eventDetails["meeting_pin"].stringValue
            if eventDetails["meeting_logo"].stringValue != ""{
                let temp = "\(Helpers.imageURl)"+eventDetails["meeting_logo"].stringValue
                let imgUrl = URL(string: temp)
                let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=20&height=20&url="+imgUrl!.relativeString
                
                let url = URL(string: newUrlStr)
                
                
                imgLoctnorUrl.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
            }
            else{
                imgLoctnorUrl.image = #imageLiteral(resourceName: "pointer_icon")
            }
        }
        else{
            onlineView.isHidden = true
            btnMap.isHidden = false
            self.DialNumView.isHidden = true
            //            self.dial_height.constant = 120
            //            self.desc_top.constant = 0
            imgLoctnorUrl.image = #imageLiteral(resourceName: "ic_location_details_")
            lblLocation.text               = eventDetails["location"].stringValue
        }
        
        if !amCreator && (eventDetails["event_image"].stringValue == "undefined" || eventDetails["event_image"].stringValue == "")
        {
            self.btnCoverPic.isHidden = true
            self.imgCoverPicEdit.isHidden = true
        }
        else
        {
            self.btnCoverPic.isHidden = false
            self.imgCoverPicEdit.isHidden = false
            
        }
        
        if rsvpArr.count > 0{
            
            imgCalndr.image = #imageLiteral(resourceName: "menu.png")
            calendarButt.setImage(UIImage(named: ""), for: .normal)
            let data = rsvpArr[0]
            
            if data["attendee_type"].stringValue.lowercased() == "going"{
                goingButt.layer.borderColor = UIColor.green.cgColor
                goingButt.setTitleColor(#colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1), for: .normal)
                
                notInterestedButt.layer.borderColor = UIColor.lightGray.cgColor
                notInterestedButt.setTitleColor(.lightGray, for: .normal)
                
                interestedButt.layer.borderColor = UIColor.lightGray.cgColor
                interestedButt.setTitleColor(.lightGray, for: .normal)
                goingButt.isEnabled = false
                notInterestedButt.isEnabled = true
                interestedButt.isEnabled = true
                
            }
            else if data["attendee_type"].stringValue.lowercased() == "interested"{
                interestedButt.layer.borderColor = UIColor.green.cgColor
                interestedButt.setTitleColor(#colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1), for: .normal)
                
                goingButt.layer.borderColor = UIColor.lightGray.cgColor
                goingButt.setTitleColor(.lightGray, for: .normal)
                
                notInterestedButt.layer.borderColor = UIColor.lightGray.cgColor
                notInterestedButt.setTitleColor(.lightGray, for: .normal)
                
                goingButt.isEnabled = true
                notInterestedButt.isEnabled = true
                interestedButt.isEnabled = false
                
            }
            else{
                imgCalndr.image = #imageLiteral(resourceName: "imgEvents_grey")
                calendarButt.setImage(UIImage(named: ""), for: .normal)
                notInterestedButt.layer.borderColor = UIColor.green.cgColor
                notInterestedButt.setTitleColor(#colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1), for: .normal)
                
                interestedButt.layer.borderColor = UIColor.lightGray.cgColor
                interestedButt.setTitleColor(.lightGray, for: .normal)
                
                goingButt.layer.borderColor = UIColor.lightGray.cgColor
                goingButt.setTitleColor(.lightGray, for: .normal)
                
                goingButt.isEnabled = true
                notInterestedButt.isEnabled = false
                interestedButt.isEnabled = true
            }
        }
        else{
            imgCalndr.image = #imageLiteral(resourceName: "ic_calendar_")
            calendarButt.setImage(UIImage(named: ""), for: .normal)
        }
        
        if amCreator{
            
            if !pastEvents{
                
                if contactArr.count > 0{
                    addQueariesButt.setTitle("Show", for: .normal)
                }
                else{
                    addQueariesButt.setTitle("Add", for: .normal)
                }
                
                
                
                self.btnEditEvent.isHidden = false
                self.shareButt.isHidden = false
                self.inviteButt.isHidden = false
                self.btnShareEventUrl.isHidden = false
                self.imgShareEventUrl.isHidden = false
                self.btnFree.isHidden = false
                
                if eventDetails["event_type"].stringValue.lowercased() == "online"{
                    let formatter2 = DateFormatter()
                    formatter2.dateFormat = "dd/MMM/yyyy hh:mma"
                    
                    let from = NSDate(timeIntervalSince1970: eventDetails["from_date"].doubleValue)
                    let to = NSDate(timeIntervalSince1970: eventDetails["to_date"].doubleValue)
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyyMMddHHmm"
                    dateFormatter.timeZone = .current
                    dateFormatter.locale = .current
                    let datefrom = Int(dateFormatter.string(from:from as Date))
                    let dateto = Int(dateFormatter.string(from:to as Date))
                    let todayDate = Int(dateFormatter.string(from: NSDate() as Date))
                    
                    if datefrom != nil && dateto != nil && todayDate != nil{
                        if (datefrom! - 15) <= todayDate! && dateto! >= todayDate!{
                            onlineView.isHidden = false
                        }
                        else{
                            onlineView.isHidden = true
                        }
                    }
                    else{
                        onlineView.isHidden = true
                        
                    }
                    
                    //                    self.backbroundVwHeightConstrain.constant       = self.txtVw.contentSize.height + 370
                }
                else{
                    self.onlineView.isHidden = true
                    //                    self.backbroundVwHeightConstrain.constant       = self.txtVw.contentSize.height + 250
                }
                
                //                self.scrollOuterVwHeightConstrain.constant      = self.backbroundVwHeightConstrain.constant + 500
                //
                //                if DeviceType.IS_IPHONE_XP{
                //
                //                    self.scrollOuterVwHeightConstrain.constant      = self.backbroundVwHeightConstrain.constant + 530
                //                }
            }
            else
            {
                
                if contactArr.count > 0{
                    addQueariesButt.setTitle("Show", for: .normal)
                }
                else{
                    queariesLbl.isHidden                       = true
                    addQueariesButt.isHidden                   = true
                }
                
                self.btnEditEvent.isHidden = true
                self.shareButt.isHidden = true
                self.inviteButt.isHidden = true
                self.btnShareEventUrl.isHidden = true
                self.imgShareEventUrl.isHidden = true
                self.btnFree.isHidden = true
                self.onlineView.isHidden = true
                if eventDetails["event_type"].stringValue.lowercased() == "online"{
                    //                    backbroundVwHeightConstrain.constant       = txtVw.contentSize.height + 320
                }
                else{
                    //                    backbroundVwHeightConstrain.constant       = txtVw.contentSize.height + 200
                }
                
                //                scrollOuterVwHeightConstrain.constant      = backbroundVwHeightConstrain.constant + 500
                //                if DeviceType.IS_IPHONE_XP{
                //
                //                    self.scrollOuterVwHeightConstrain.constant      = self.backbroundVwHeightConstrain.constant + 530
                //                }
            }
            
        }else{
            if contactArr.count > 0{
                addQueariesButt.setTitle("Show", for: .normal)
            }
            else{
                queariesLbl.isHidden                       = true
                addQueariesButt.isHidden                   = true
            }
            if eventDetails["event_type"].stringValue.lowercased() == "online"{
                //                backbroundVwHeightConstrain.constant       = txtVw.contentSize.height + 320
            }
            else{
                //                backbroundVwHeightConstrain.constant       = txtVw.contentSize.height + 200
            }
            
            //            scrollOuterVwHeightConstrain.constant      = backbroundVwHeightConstrain.constant + 500
            //
            //            if DeviceType.IS_IPHONE_XP{
            //
            //                self.scrollOuterVwHeightConstrain.constant      = self.backbroundVwHeightConstrain.constant + 530
            //            }
        }
        
        
        self.view.layoutIfNeeded()
        
    }
    
    
    func changeFormatee(Datee:Date) -> String{
        let dateFormatter          = DateFormatter()
        dateFormatter.dateFormat   = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone     = NSTimeZone(name: "UTC") as TimeZone?
        dateFormatter.locale       = .current
        let date                   = dateFormatter.string(from: Datee as Date)
        let datew                  = dateFormatter.date(from: date)!
        dateFormatter.dateFormat   = "MMM dd yyyy hh:mm a"
        return dateFormatter.string(from: datew)
    }
    
    //MARK:- CropView SDK Delegate
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.selectedImage = image
        //            imgOfEvents.image = image
        self.callEventUpdatewithImage()
        //        tblUserProfile.reloadRows(at: [IndexPath(row: btnTag, section: 0)], with: .none)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: -API for event image update
    
    func callEventUpdatewithImage(){
        let image = self.selectedImage
        if image == nil{
            self.imgEventData = NSData()
        }else{
            self.imgEventData = self.selectedImage.jpegData(compressionQuality: 0.75)! as NSData
        }
        
        if self.original.size.width > 0{
            self.originalData = NSData()
        }
        else{
            self.originalData = self.original.jpegData(compressionQuality: 0.75)! as NSData
        }
        //        self.imgAgendaData = self.imgAgenda.jpegData(compressionQuality: 0.5)! as NSData
        ActivityIndicatorView.show("Loading....")
        
        let parameter: Parameters = ["user_id" : UserDefaults.standard.value(forKey: "userId") as! String, "id" : self.eventId!]
        
        Helpers.requestWith(fileParamName: "event_image", originalCover: "event_original_image",urlAppendString: "update_events", imageData: self.imgEventData as Data, originalImg: self.originalData as Data, parameters: parameter, mimeType: "image/png") { (response) in
            
            if response["data"]  != nil {
                let responseData = response["data"].dictionaryValue
                
            }
            else
            {
                Helpers.showAlertDialog(message: "Error in image upload,Please try again", target: self)
            }
            
            self.getEventDetailsById(eventID: self.eventId)
            
            ActivityIndicatorView.hiding()
        }
        
        
    }
    //MARK:- Button Action
    @IBAction func btnChangeReminderAction(_ sender: Any) {
        
        self.showActionSheet(titleArr: reminderTimeTitleStrings as NSArray, title: "Please select") { (index) in
            
            switch index {
            case 0: //1 Hr before
                
                self.changeDateForReminder(hoursBefore: -1, time: 60)
                
                break
                
            case 1: //2 Hrs before
                self.changeDateForReminder(hoursBefore: -2, time: 120)
                
                break
                
            case 2: //5 Hrs before
                self.changeDateForReminder(hoursBefore: -5, time: 300)
                
                break
                
            case 3: //24 Hrs before
                self.changeDateForReminder(hoursBefore: -24,time: 1440)
                
                break
                
            default:
                break
            }
            
        }
    }
    
    func changeDateForReminder(hoursBefore:Int, time: Int){
        
        let date = Date(timeIntervalSince1970: eventDetails["from_date"].doubleValue)
        let dayBefore = date.adding(hour: hoursBefore, minutes: 0)
        triggerDate = dayBefore
        let dateFormatter = DateFormatter()
        dateFormatter.locale = .current
        dateFormatter.dateFormat = "MMMM d,yyyy h:mm a"
        let strDate = dateFormatter.string(from: dayBefore)
        dateTimeLbl.text = "\(strDate)"
        
        createRemainder(remainder: "\(time)")
        
        //        let content = UNMutableNotificationContent()
        //        let categoryIdentifire = "Delete Notification Type"
        //
        //        content.title = "\(eventDetails["event_name"].stringValue) Reminder"
        //        content.body = "You have new event at \(eventDetails["location"].stringValue)"
        //        content.sound = UNNotificationSound.default
        //        content.badge = 1
        //        content.categoryIdentifier = categoryIdentifire
        //
        //        var Cal = Calendar(identifier: .gregorian)
        //        Cal.timeZone = TimeZone(secondsFromGMT: 0)!
        //
        //        let triggerDay = Calendar.current.dateComponents([.hour, .minute, .second], from: dayBefore)
        //        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDay, repeats: false)
        //
        //        let identifier = "\(String(describing: eventId))"
        //        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        //
        //        notificationCenter.add(request) { (error) in
        //            if let error = error {
        //                print("Error \(error.localizedDescription)")
        //            }
        //        }
        //
        //        let snoozeAction = UNNotificationAction(identifier: "Snooze", title: "Snooze", options: [])
        //        let deleteAction = UNNotificationAction(identifier: "DeleteAction", title: "Delete", options: [.destructive])
        //        let category = UNNotificationCategory(identifier: categoryIdentifire,
        //                                              actions: [snoozeAction, deleteAction],
        //                                              intentIdentifiers: [],
        //                                              options: [])
        //
        //        notificationCenter.setNotificationCategories([category])
        
        
        
        /*       //To Print Current Local notifications
         let center = UNUserNotificationCenter.current()
         
         center.getPendingNotificationRequests { (notifications) in
         print("Count: \(notifications.count)")
         for item in notifications {
         print(item.content)
         }
         }
         */
        
        
    }
    
    @IBAction func btnBack_OnClick(_ sender: Any) {
        
        if isFromNotification == "true"
        {
            self.navigationController?.popViewController(animated: true)
            
        }
        else if isFrom.lowercased() == "family"{
            if faEventCreate{
                var array : [UIViewController] = (self.navigationController?.viewControllers)!
                array.remove(at: array.count - 1)
                array.remove(at: array.count - 1)
                array.remove(at: array.count - 1)
                self.navigationController?.viewControllers = array
                self.navigationController?.popViewController(animated: true)
            }
            else{
                self.navigationController?.popViewController(animated: true)
            }
        }
        else if isFrom.lowercased() == "album"{
            self.navigationController?.popViewController(animated: true)
        }
        else if fromCalendar{
            
            appDel.isBackToCalendar = true
            self.navigationController?.popViewController(animated: true)
        }
            
        else{
            appDel.isFromEventDetails = true
            self.navigationController?.popToViewController(ofClass: EventsTabViewController.self)
        }
        //
        //
    }
    
    @IBAction func onClickMap(_ sender: Any) {
        //        let stryboard = UIStoryboard.init(name: "third", bundle: nil)
        //        print(self.endLat as Double)
        //        let vc = stryboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        //        vc.startLat = self.startLat
        //        vc.startLong = self.startLong
        //        vc.endLat = self.endLat
        //        vc.endLong = self.endLong
        //        self.navigationController?.pushViewController(vc, animated: true)
        
        
        
        
        if self.startLat != nil && self.startLong != nil && self.endLong != nil && self.endLat != nil{
            
            let source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: self.startLat, longitude: self.startLong)))
            source.name = "Source"
            
            let destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: self.endLat, longitude: self.endLong)))
            destination.name = "Destination"
            
            MKMapItem.openMaps(with: [source, destination], launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
        }
        else if let lat =  self.endLat, let lng = self.endLong , lat != 0.0 && lng != 0.0{
            let coordinate = CLLocationCoordinate2DMake(lat,lng)
            let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
            mapItem.name = "Destination"
            mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
        }
        else{
            self.displayAlert(alertStr: "Unable to find location, Please try again", title: "")
        }
        //      FamilyDetailsTableViewController
        //        AlbumListingViewController
        //        documentslisting
        
    }
    @IBAction func onClickAddContact(_ sender: Any) {
        if contactArr.count > 0{
            let stryboard = UIStoryboard.init(name: "third", bundle: nil)
            let vc = stryboard.instantiateViewController(withIdentifier: "ShowContactsViewController") as! ShowContactsViewController
            vc.modalPresentationStyle = .fullScreen
            
            //            vc.modalTransitionStyle = .crossDissolve
            vc.eventId = self.eventId
            vc.amCreator = self.amCreator
            vc.pastEvents = self.pastEvents
            vc.contactArr = self.contactArr.arrayValue
            vc.delegate = self
            //            var vcArray = self.navigationController?.viewControllers
            //            vcArray!.removeLast()
            //            vcArray!.append(vc)
            let navigationController = UINavigationController(rootViewController: vc)
            present(navigationController, animated: true, completion: nil)
            
        }
        else{
            
            let stryboard = UIStoryboard.init(name: "fourth", bundle: nil)
            let vc = stryboard.instantiateViewController(withIdentifier: "AddContactsQueryViewController") as! AddContactsQueryViewController
            vc.eventId = self.eventId
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    @IBAction func onClickEventShareAction(_ sender: Any) {
        Helpers.showActivityViewcontroller(url:lblEventUrl.text!, VC: self)
        
    }
    
    //MARK: - Invite
    
    @IBAction func inviteClicked(_ sender: Any) {
        
        let stryboard = UIStoryboard.init(name: "Main", bundle: nil)
        
        let vc = stryboard.instantiateViewController(withIdentifier: "EventShareViewController") as! EventShareViewController
        vc.typeOfInvitations = "invitation"
        vc.eventId = eventId
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //Edit button action
    @IBAction func btneditEvent(_ sender: Any) {
        
        let postoptionsTittle = ["Edit event","Cancel event","Delete event"]
        showActionSheet(titleArr: postoptionsTittle as NSArray, title: "Select options") { (index) in
            if index == 0{
                self.editEvent()
            }
            else if index == 1{
                self.cancelEvent(index: index)
            }
            else if index == 100{
            }
            else{
                self.cancelEvent(index: index)
            }
        }
    }
    
    
    @IBAction func onClickHide(_ sender: Any) {
        self.blckBgView.isHidden = true
    }
    
    @IBAction func rsvpAction(_ sender: UIButton) {
        donePressed = false
        
        if sender == goingButt{
            
            goingButt.layer.borderColor = UIColor.green.cgColor
            goingButt.setTitleColor(#colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1), for: .normal)
            
            notInterestedButt.layer.borderColor = UIColor.lightGray.cgColor
            notInterestedButt.setTitleColor(.lightGray, for: .normal)
            
            interestedButt.layer.borderColor = UIColor.lightGray.cgColor
            interestedButt.setTitleColor(.lightGray, for: .normal)
            onClickResponseToRSVP(status: "going", otherCount: "0")
            
            goingButt.isEnabled = false
            notInterestedButt.isEnabled = true
            interestedButt.isEnabled = true
            if let reminderId = eventDetails["reminder_id"].int{
                createRemainder(remainder: "")
                
            }
            else{
                createRemainder(remainder: "10")
            }
            /*  if eventDetails["allow_others"].stringValue.lowercased() == "true"{
             self.morePeopleBlackVw.isHidden = false
             }
             else{
             self.morePeopleBlackVw.isHidden = true
             onClickResponseToRSVP(status: "going", otherCount: <#String#>)
             }*/
        }
        else if sender == interestedButt{
            
            interestedButt.layer.borderColor = UIColor.green.cgColor
            interestedButt.setTitleColor(#colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1), for: .normal)
            
            goingButt.layer.borderColor = UIColor.lightGray.cgColor
            goingButt.setTitleColor(.lightGray, for: .normal)
            
            notInterestedButt.layer.borderColor = UIColor.lightGray.cgColor
            notInterestedButt.setTitleColor(.lightGray, for: .normal)
            
            onClickResponseToRSVP(status: "interested", otherCount: "0")
            goingButt.isEnabled = true
            notInterestedButt.isEnabled = true
            interestedButt.isEnabled = false
            if let reminderId = eventDetails["reminder_id"].int{
                createRemainder(remainder: "")
                
            }
            else{
                createRemainder(remainder: "10")
            }
            
        }
        else if sender == notInterestedButt{
            
            notInterestedButt.layer.borderColor = UIColor.green.cgColor
            notInterestedButt.setTitleColor(#colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1), for: .normal)
            
            interestedButt.layer.borderColor = UIColor.lightGray.cgColor
            interestedButt.setTitleColor(.lightGray, for: .normal)
            
            goingButt.layer.borderColor = UIColor.lightGray.cgColor
            goingButt.setTitleColor(.lightGray, for: .normal)
            goingButt.isEnabled = true
            notInterestedButt.isEnabled = false
            interestedButt.isEnabled = true
            onClickResponseToRSVP(status: "not-going", otherCount: "0")
            deleteReminder()
        }
        else{
            if rsvpArr.count > 0 {
                let data = rsvpArr[0]
                if data["attendee_type"].stringValue.lowercased() == "going"{
                    self.blckBgView.isHidden = false
                    if self.eventDetails["allow_others"].stringValue == "true"{
                        self.btmVwHeightConstrain.constant        = 175
                        self.backWhiteVwHeightConstrain.constant  = 350
                        
                        
                        self.pleaseEnterLbl.isHidden              = false
                        self.morePeopleField.isHidden             = false
                        self.doneButt.isHidden                    = false
                        
                        guard data["others_count"].stringValue.count > 0 else{
                            self.morePeopleField.text = "0"
                            return
                        }
                        self.morePeopleField.text = data["others_count"].stringValue
                    }else{
                        
                        self.btmVwHeightConstrain.constant        = 0
                        self.backWhiteVwHeightConstrain.constant  = 175
                        
                        
                        self.pleaseEnterLbl.isHidden              = true
                        self.morePeopleField.isHidden             = true
                        self.doneButt.isHidden                    = true
                    }
                    
                }
                else if data["attendee_type"].stringValue.lowercased() == "interested"{
                    self.blckBgView.isHidden = false
                    guard data["others_count"].stringValue.count > 0 else{
                        self.morePeopleField.text = "0"
                        return
                    }
                    self.morePeopleField.text = data["others_count"].stringValue
                }
                else{
                    let CalendarEventsViewController = self.storyboard?.instantiateViewController(withIdentifier: "CalendarEventsViewController") as! CalendarEventsViewController
                    CalendarEventsViewController.isFromExplore = ""
                    self.navigationController?.pushViewController(CalendarEventsViewController, animated: true)
                }
            }
            else{
                let CalendarEventsViewController = self.storyboard?.instantiateViewController(withIdentifier: "CalendarEventsViewController") as! CalendarEventsViewController
                CalendarEventsViewController.isFromExplore = ""
                self.navigationController?.pushViewController(CalendarEventsViewController, animated: true)
            }
        }
    }
    @IBAction func onClickSaveGoingRSVP(_ sender: Any) {
        
    }
    
    @IBAction func changeClicked(_ sender: Any) {
    }
    
    @IBAction func doneClicked(_ sender: Any) {
        donePressed = true
        if morePeopleField.text != ""{
            //            self.hideOtherCountView = true
            onClickResponseToRSVP(status: "going", otherCount: morePeopleField.text!)
        }
        else{
            onClickResponseToRSVP(status: "going", otherCount: "0")
            
        }
        
    }
    
    
    @IBAction func shareClicked(_ sender: UIButton) {
        
        let stryboard = UIStoryboard.init(name: "Main", bundle: nil)
        
        let vc = stryboard.instantiateViewController(withIdentifier: "EventShareViewController") as! EventShareViewController
        if eventDetails["is_public"].stringValue.lowercased() == "true"{
            vc.typeOfInvitations = "share"
        }
        else{
            vc.typeOfInvitations = "invitation"
        }
        vc.eventId = eventId
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func onClickjoin(_ sender: Any) {
        let temp = eventDetails["meeting_link"].stringValue
        if temp.isEmpty{
        }
        else{
            UIPasteboard.general.string = self.eventDetails["meeting_pin"].stringValue
            self.showToast(message: "Participant pin copied to clipboard")
            DispatchQueue.main.asyncAfter(deadline: .now()+3) {
                let url = URL(string: temp)!
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
    
    //MARK: - Time Stamp
    
    func convertTimeStampString(timestamp:String) -> String {
        let date = Date(timeIntervalSince1970: Double(timestamp) as! TimeInterval)
        //    dateFormatter.dateFormat       = "EEE MMM dd yyy"
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MMM/yyyy hh:mma"
        //            formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.timeZone = TimeZone.current
        
        
        let localDatef = formatter.string(from: date)
        return localDatef
    }
    
    func convertTimeStamp (val : Double) -> (d: String, m: String, y: String, time: String) {
        let date                     = Date(timeIntervalSince1970: val)
        let dateFormatter            = DateFormatter()
        dateFormatter.timeStyle      = DateFormatter.Style.short //Set time style
        dateFormatter.dateStyle      = DateFormatter.Style.long //Set date style
        dateFormatter.timeZone       = .current
        
        let localDate = dateFormatter.string(from: date)
        
        return (date.dateOfMonth()!, date.nameOfMonth()!, date.yearOfDate()!, localDate)
    }
    
    func filterDayOnly(timestamp:String) -> String {
        let date = Date(timeIntervalSince1970: Double(timestamp) as! TimeInterval)
        //    dateFormatter.dateFormat       = "EEE MMM dd yyy"
        
        let formatter = DateFormatter()
        formatter.dateFormat = "EEE"
        //            formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.timeZone = TimeZone.current
        
        
        let localDatef = formatter.string(from: date)
        return localDatef
    }
    
    //MARK: - Time Stamp
    
    func convertTimeStamp2 (val : Double) -> Date {
        let date = Date(timeIntervalSince1970: val)
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.short //Set time style
        dateFormatter.dateStyle = DateFormatter.Style.long //Set date style
        dateFormatter.timeZone = .current
        
        return date
    }
    
    func convertDateAndTime(dat : Date) -> String {
        
        let dateFormatter          = DateFormatter()
        dateFormatter.dateFormat   = "yyyy-MM-dd HH:mm:ss +0000"
        dateFormatter.dateStyle    = .long
        dateFormatter.timeStyle    = .none
        let val                    = dateFormatter.string(from: dat)
        return val
    }
    
    //MARK:- collectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        return titleArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchTitlesCollectionViewCell", for: indexPath as IndexPath) as! SearchTitlesCollectionViewCell
        cell.lblTitle.text = titleArr[indexPath.item]
        if indexPath.row == 0{
            cell.lblTitle.font                  = UIFont.boldSystemFont(ofSize: 17)
            cell.lblTitle.textColor             = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            cell.lblTitle.adjustsFontSizeToFitWidth = true
            cell.viewSelection.isHidden         = false
        }else{
            cell.lblTitle.font                  = UIFont.systemFont(ofSize: 14)
            cell.lblTitle.adjustsFontSizeToFitWidth = true

            cell.lblTitle.textColor             = .darkGray
            cell.viewSelection.isHidden         = true
            cell.viewSelection.backgroundColor  = UIColor.lightGray
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row != 0
        {
            
            let storyboard = UIStoryboard.init(name: "third", bundle: nil)
            
            let vc = storyboard.instantiateViewController(withIdentifier: "EventDetailsAddViewController") as! EventDetailsAddViewController
            vc.selectedIndex = indexPath.row
            vc.eventDetails = self.eventDetails
            vc.eventId = self.eventId
            vc.pastEvents = self.pastEvents
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let numberOfCellInRow  = 4 //titleArr.count
        let padding : Int      = 5
        let collectionCellWidth : CGFloat = (self.collVw.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
        return CGSize(width: collectionCellWidth, height: 60)
    }
    
    //MARK: - setRemainder
    
    func setRemainder(){
        
        createRemainder(remainder: "10")
    }
    
    //MARK:- Create Remainder
    
    func createRemainder(remainder : String){
        
        var parameter : Parameters = ["user_id": UserDefaults.standard.value(forKey: "userId") as! String, "event_id": eventId!, "remind_on" : remainder,"event_date":eventDetails["from_date"].stringValue]  //Create
        
        if let reminderId = eventDetails["reminder_id"].int, let reminderDate = eventDetails["remind_on"].string{
            
            parameter = ["user_id": UserDefaults.standard.value(forKey: "userId") as! String, "event_id": eventId!,"event_date":eventDetails["from_date"].stringValue,"reminder_id":reminderId, "remind_date":reminderDate]  //Update
            
            if !remainder.isEmpty{
                parameter["remind_on"] = remainder
            }
        }
        
        ActivityIndicatorView.show("Loading....")
        
        networkProvider.request(.create_reminder(parameter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    
                    
                    if response.statusCode == 200{
                        self.getEventDetailsById(eventID: self.eventId)
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.createRemainder(remainder: remainder)
                        }
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    //MARK:- Delete Reminder
    
    func deleteReminder(){
        
        if let reminderId = eventDetails["reminder_id"].int{
            
            let parameter : Parameters = ["reminder_id":reminderId]
            
            ActivityIndicatorView.show("Loading....")
            
            networkProvider.request(.delete_reminder(parameter: parameter)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                        
                        if response.statusCode == 200{
                            self.getEventDetailsById(eventID: self.eventId)
                        }
                        else if response.statusCode == 401 {
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                self.deleteReminder()
                            }
                        }
                        else{
                            self.getEventDetailsById(eventID: self.eventId)
                        }
                        
                        ActivityIndicatorView.hiding()
                        
                    }catch let err {
                        ActivityIndicatorView.hiding()
                        self.getEventDetailsById(eventID: self.eventId)
                        Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                    }
                case .failure( let error):
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                    break
                }
            }
        }
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension  EventDetailsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker .dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let choosenImage = info[.originalImage] as! UIImage
        selectedImage(img: choosenImage)
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    //MARK: - location delegate methods
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        self.startLat = locValue.latitude
        self.startLong = locValue.longitude
        
        
        guard let location: CLLocation = manager.location else { return }
        fetchCityAndCountry(from: location) { city, country, error in
            guard let city = city, let country = country, error == nil else { return }
            print(city + ", " + country)
            self.currentLocation = "\(city) , \(country)"
            self.locationManager.stopUpdatingLocation()
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }
    
    func fetchCityAndCountry(from location: CLLocation, completion: @escaping (_ city: String?, _ country:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            completion(placemarks?.first?.locality,
                       placemarks?.first?.country,
                       error)
            
        }
    }
}
