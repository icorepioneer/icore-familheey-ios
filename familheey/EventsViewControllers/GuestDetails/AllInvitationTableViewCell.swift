//
//  AllInvitationTableViewCell.swift
//  familheey
//
//  Created by Innovation Incubator on 08/11/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class AllInvitationTableViewCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var imgLocation: UIImageView!
    @IBOutlet weak var imgOfUser: UIImageView!
    @IBOutlet weak var lblPhNo: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
