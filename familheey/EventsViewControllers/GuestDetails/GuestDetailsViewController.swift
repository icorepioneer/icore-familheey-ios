//
//  GuestDetailsViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 24/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON
import Kingfisher

class GuestDetailsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {
    var arrayOfGuestDetails = [JSON]()
    var arrayOfAllInivGroup = [JSON]()
    var arrayOfAllInivPeople = [JSON]()
    var arrayOfAllInivOther = [JSON]()
    
    @IBOutlet weak var btnSearchReset: UIButton!
    
    @IBOutlet weak var txtSearch: UITextField!
    
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var viewOfNoData: UIView!
    
    @IBOutlet weak var imgOfNoData: UIImageView!
    
    //MARK:- textfield delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        if textField == txtSearch
        {
            if textField.text!.count > 0{
                btnSearchReset.isHidden = false
            }
            else{
                btnSearchReset.isHidden = true
            }
            // self.setSearchCondition()
            textField.endEditing(true)
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        self.view.endEditing(true)
        if textField == txtSearch
        {
            self.setSearchCondition()
        }
        if textField.text!.count > 0{
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text!.count > 0{
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text!.count > 0{
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
        }
        return true
    }
    
    //MARK:- Web api
    func setSearchCondition()
    {
        if selectedIndex == 0
        {
            self.apiForGuestAttendingDetails()
            
        }
        else if selectedIndex == 1
        {
            self.apiForGuestNotAttendingDetails()
            
        }
        else if selectedIndex == 2
        {
            self.apiForGuestNotAttendingDetails()
            
        }
        else
        {
            self.apiForGuestAllInvitationList()
            
        }
        
    }
    
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    var selectedIndex :Int!
    var invitationTabIndex = 0
    var invitationType:String = "family"
    var  eventId : String!
    var selctedRSVPStatus:String!
    
    @IBOutlet weak var btnAttending: UIButton!
    @IBOutlet weak var imgViewAttending: UIImageView!
    @IBOutlet weak var tblViewGuestListing: UITableView!
    
    @IBOutlet weak var btnNotAttending: UIButton!
    @IBOutlet weak var imgViewNotAttending: UIImageView!
    @IBOutlet weak var imgViewMayAttend: UIImageView!
    @IBOutlet weak var btnMayAttend: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        self.txtSearch
        if selectedIndex == 3
        {
            self.btnAttending.setTitle("FAMILY", for: .normal)
            self.btnMayAttend.setTitle("PEOPLE", for: .normal)
            self.btnNotAttending.setTitle("OTHER", for: .normal)
            invitationTabIndex = 0
            invitationType = "family"
            self.apiForGuestAllInvitationList()
            
            
        }
        Helpers.setleftView(textfield: self.txtSearch,customWidth:30)
        self.txtSearch.delegate = self
        self.setTabfunctionality()
        
        //        self.tblViewGuestListing.dataSource = self
        self.tblViewGuestListing.delegate = self
        
        tblViewGuestListing.register(UINib(nibName: "AllInvitationTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: - Button Actions
    @IBAction func btnTabsOnClick(_ sender: UIButton) {
        if selectedIndex == 3
        {
            self.selectedIndex = 3
            self.invitationTabIndex = sender.tag
            self.setTabfunctionality()
        }
        else
        {
            self.selectedIndex = sender.tag
            self.setTabfunctionality()
        }
        
        
    }
    
    @IBAction func onClickSearchReset(_ sender: Any) {
        txtSearch.text = ""
        self.btnSearchReset.isHidden = true
        self.setSearchCondition()
        self.txtSearch.endEditing(true)
    }
    @IBAction func btnBack_OnClick(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    func setTabfunctionality()
    {
        if selectedIndex == 0
        {
            self.selctedRSVPStatus = ""
            //            btnAttending.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
            //            btnAttending .setTitleColor(#colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1), for: .normal)
            //            imgViewAttending.backgroundColor  = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            //            btnMayAttend.titleLabel?.font = UIFont.systemFont(ofSize: 15)
            //            btnMayAttend .setTitleColor(.darkGray, for: .normal)
            //            imgViewMayAttend.backgroundColor  = .lightGray
            //            btnNotAttending.titleLabel?.font = UIFont.systemFont(ofSize: 15)
            //            btnNotAttending .setTitleColor(.darkGray, for: .normal)
            //            imgViewNotAttending.backgroundColor  = .lightGray
            //            self.apiForGuestAttendingDetails()
            
            
            btnMayAttend.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            btnNotAttending.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            btnAttending.setTitleColor(UIColor(named: "greenBackgrounf"), for: .normal)
            
            imgViewMayAttend.backgroundColor  = UIColor(named: "lightGrayTextColor")
            imgViewNotAttending.backgroundColor  = UIColor(named: "lightGrayTextColor")
            imgViewAttending.backgroundColor  = UIColor(named: "greenBackgrounf")
            self.apiForGuestAttendingDetails()
            
            
            //            btnExplore.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            //            btnMyEvents.setTitleColor(UIColor(named: "greenBackgrounf"), for: .normal)
            //            btnInviteEvents.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            //
            //            imgExplore.backgroundColor  = UIColor(named: "lightGrayTextColor")
            //            imgMyEvents.backgroundColor  = UIColor(named: "greenBackgrounf")
            //            imgInvite.backgroundColor  = UIColor(named: "lightGrayTextColor")
        }
        else if selectedIndex == 1
        {
            self.selctedRSVPStatus = "interested"
            //            btnMayAttend.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
            //            btnMayAttend .setTitleColor(#colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1), for: .normal)
            //            imgViewMayAttend.backgroundColor  = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            //            btnAttending.titleLabel?.font = UIFont.systemFont(ofSize: 15)
            //            btnAttending .setTitleColor(.darkGray, for: .normal)
            //            imgViewAttending.backgroundColor  = .lightGray
            //            btnNotAttending.titleLabel?.font = UIFont.systemFont(ofSize: 15)
            //            btnNotAttending .setTitleColor(.darkGray, for: .normal)
            ////            imgViewNotAttending.backgroundColor  = .lightGray
            //            self.apiForGuestNotAttendingDetails()
            
            
            btnAttending.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            btnNotAttending.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            btnMayAttend.setTitleColor(UIColor(named: "greenBackgrounf"), for: .normal)
            
            imgViewAttending.backgroundColor  = UIColor(named: "lightGrayTextColor")
            imgViewNotAttending.backgroundColor  = UIColor(named: "lightGrayTextColor")
            imgViewMayAttend.backgroundColor  = UIColor(named: "greenBackgrounf")
            self.apiForGuestNotAttendingDetails()
            
            
        }
        else if selectedIndex == 2
        {
            self.selctedRSVPStatus = "not-going"
            
            //            btnNotAttending.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
            //            btnNotAttending .setTitleColor(#colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1), for: .normal)
            //            imgViewNotAttending.backgroundColor  = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            //            btnAttending.titleLabel?.font = UIFont.systemFont(ofSize: 15)
            //            btnAttending .setTitleColor(.darkGray, for: .normal)
            //            imgViewAttending.backgroundColor  = .lightGray
            //            btnMayAttend.titleLabel?.font = UIFont.systemFont(ofSize: 15)
            //            btnMayAttend .setTitleColor(.darkGray, for: .normal)
            //            imgViewMayAttend.backgroundColor  = .lightGray
            //            self.apiForGuestNotAttendingDetails()
            
            btnMayAttend.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            btnAttending.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
            btnNotAttending.setTitleColor(UIColor(named: "greenBackgrounf"), for: .normal)
            
            imgViewMayAttend.backgroundColor  = UIColor(named: "lightGrayTextColor")
            imgViewAttending.backgroundColor  = UIColor(named: "lightGrayTextColor")
            imgViewNotAttending.backgroundColor  = UIColor(named: "greenBackgrounf")
            self.apiForGuestNotAttendingDetails()
            
            
        }
        else
        {
            if invitationTabIndex == 0
            {
                self.invitationType = "family"
                //                btnAttending.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
                //                btnAttending .setTitleColor(#colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1), for: .normal)
                //                imgViewAttending.backgroundColor  = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                //                btnMayAttend.titleLabel?.font = UIFont.systemFont(ofSize: 15)
                //                btnMayAttend .setTitleColor(.darkGray, for: .normal)
                //                imgViewMayAttend.backgroundColor  = .lightGray
                //                btnNotAttending.titleLabel?.font = UIFont.systemFont(ofSize: 15)
                //                btnNotAttending .setTitleColor(.darkGray, for: .normal)
                //                imgViewNotAttending.backgroundColor  = .lightGray
                btnMayAttend.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
                btnNotAttending.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
                btnAttending.setTitleColor(UIColor(named: "greenBackgrounf"), for: .normal)
                
                imgViewMayAttend.backgroundColor  = UIColor(named: "lightGrayTextColor")
                imgViewNotAttending.backgroundColor  = UIColor(named: "lightGrayTextColor")
                imgViewAttending.backgroundColor  = UIColor(named: "greenBackgrounf")
                if self.arrayOfAllInivGroup.count != 0
                {
                    self.viewOfNoData.isHidden = true
                    self.lblNoData.isHidden = true
                    self.imgOfNoData.isHidden = true
                    self.tblViewGuestListing.dataSource = self
                    self.tblViewGuestListing.isHidden = false
                    self.tblViewGuestListing.reloadData()
                }
                else
                {
                    self.viewOfNoData.isHidden = false
                    self.lblNoData.isHidden = false
                    self.imgOfNoData.isHidden = false
                    
                    self.tblViewGuestListing.isHidden = true
                }
                
                
            }
            else if invitationTabIndex == 1
            {
                self.invitationType = "people"
                //                btnMayAttend.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
                //                btnMayAttend .setTitleColor(#colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1), for: .normal)
                //                imgViewMayAttend.backgroundColor  = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                //                btnAttending.titleLabel?.font = UIFont.systemFont(ofSize: 15)
                //                btnAttending .setTitleColor(.darkGray, for: .normal)
                //                imgViewAttending.backgroundColor  = .lightGray
                //                btnNotAttending.titleLabel?.font = UIFont.systemFont(ofSize: 15)
                //                btnNotAttending .setTitleColor(.darkGray, for: .normal)
                //                imgViewNotAttending.backgroundColor  = .lightGray
                
                btnAttending.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
                btnNotAttending.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
                btnMayAttend.setTitleColor(UIColor(named: "greenBackgrounf"), for: .normal)
                
                imgViewAttending.backgroundColor  = UIColor(named: "lightGrayTextColor")
                imgViewNotAttending.backgroundColor  = UIColor(named: "lightGrayTextColor")
                imgViewMayAttend.backgroundColor  = UIColor(named: "greenBackgrounf")
                if self.arrayOfAllInivPeople.count != 0
                {
                    self.viewOfNoData.isHidden = true
                    self.lblNoData.isHidden = true
                    self.imgOfNoData.isHidden = true
                    self.tblViewGuestListing.dataSource = self
                    self.tblViewGuestListing.isHidden = false
                    self.tblViewGuestListing.reloadData()
                }
                else
                {
                    self.viewOfNoData.isHidden = false
                    self.lblNoData.isHidden = false
                    self.imgOfNoData.isHidden = false
                    
                    self.tblViewGuestListing.isHidden = true
                }
                
            }
            else if invitationTabIndex == 2
            {
                self.invitationType = "other"
                
                //                btnNotAttending.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
                //                btnNotAttending .setTitleColor(#colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1), for: .normal)
                //                imgViewNotAttending.backgroundColor  =  #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                //                btnAttending.titleLabel?.font = UIFont.systemFont(ofSize: 15)
                //                btnAttending .setTitleColor(.darkGray, for: .normal)
                //                imgViewAttending.backgroundColor  = .lightGray
                //                btnMayAttend.titleLabel?.font = UIFont.systemFont(ofSize: 15)
                //                btnMayAttend .setTitleColor(.darkGray, for: .normal)
                //                imgViewAttending.backgroundColor  = .lightGray
                btnMayAttend.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
                btnAttending.setTitleColor(UIColor(named: "lightGrayTextColor"), for: .normal)
                btnNotAttending.setTitleColor(UIColor(named: "greenBackgrounf"), for: .normal)
                
                imgViewMayAttend.backgroundColor  = UIColor(named: "lightGrayTextColor")
                imgViewAttending.backgroundColor  = UIColor(named: "lightGrayTextColor")
                imgViewNotAttending.backgroundColor  = UIColor(named: "greenBackgrounf")
                if self.arrayOfAllInivOther.count != 0
                {
                    self.viewOfNoData.isHidden = true
                    self.lblNoData.isHidden = true
                    self.imgOfNoData.isHidden = true
                    self.tblViewGuestListing.dataSource = self
                    self.tblViewGuestListing.isHidden = false
                    self.tblViewGuestListing.reloadData()
                }
                else
                {
                    self.viewOfNoData.isHidden = false
                    self.lblNoData.isHidden = false
                    self.imgOfNoData.isHidden = false
                    
                    self.tblViewGuestListing.isHidden = true
                }
                
            }
        }
        
    }
    
    func apiForGuestAttendingDetails() {
        //        let parameter = ["user_id":UserDefaults.standard.value(forKey: "userId") as! String] as [String : Any]
        let parameter = ["event_id":self.eventId!,"current_id":UserDefaults.standard.value(forKey: "userId") as! String,"query":self.txtSearch.text!] as [String : Any]
        print(parameter)
        
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.getGuestAttendingDetails(parameter: parameter), completion: {(result) in
            switch result {
            case .success(let response):
                do {
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    
                    if response.statusCode == 200{
                        self.arrayOfGuestDetails = jsonData.arrayValue
                        if self.arrayOfGuestDetails.count != 0
                        {
                            self.viewOfNoData.isHidden = true
                            self.lblNoData.isHidden = true
                            self.imgOfNoData.isHidden = true
                            
                            self.tblViewGuestListing.isHidden = false
                            self.tblViewGuestListing.dataSource = self
                            self.tblViewGuestListing.reloadData()
                        }
                        else
                        {
                            self.viewOfNoData.isHidden = false
                            self.lblNoData.isHidden = false
                            self.imgOfNoData.isHidden = false
                            
                            self.tblViewGuestListing.isHidden = true
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.apiForGuestAttendingDetails()
                        }
                    }
                        
                    else{
                        self.viewOfNoData.isHidden = false
                        self.lblNoData.isHidden = false
                        self.imgOfNoData.isHidden = false
                        
                        self.tblViewGuestListing.isHidden = true
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                } catch let err {
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
    func apiForGuestNotAttendingDetails() {
        //        let parameter = ["user_id":UserDefaults.standard.value(forKey: "userId") as! String] as [String : Any]
        
        let parameter = ["event_id":self.eventId!,"current_id":UserDefaults.standard.value(forKey: "userId") as! String,"rsvp_status":self.selctedRSVPStatus!,"query":self.txtSearch.text!] as [String : Any]
        print(parameter)
        
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.getGuestNotAttendingDetails(parameter: parameter), completion: {(result) in
            switch result {
            case .success(let response):
                do {
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    
                    if response.statusCode == 200{
                        self.arrayOfGuestDetails = jsonData.arrayValue
                        if self.arrayOfGuestDetails.count != 0
                        {
                            self.viewOfNoData.isHidden = true
                            self.lblNoData.isHidden = true
                            self.imgOfNoData.isHidden = true
                            
                            self.tblViewGuestListing.isHidden = false
                            self.tblViewGuestListing.dataSource = self
                            
                            self.tblViewGuestListing.reloadData()
                        }
                        else
                        {
                            self.viewOfNoData.isHidden = false
                            self.lblNoData.isHidden = false
                            self.imgOfNoData.isHidden = false
                            
                            self.tblViewGuestListing.isHidden = true
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.apiForGuestNotAttendingDetails()
                        }
                    }
                        
                    else{
                        self.viewOfNoData.isHidden = false
                        self.lblNoData.isHidden = false
                        self.imgOfNoData.isHidden = false
                        
                        self.tblViewGuestListing.isHidden = true
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                    
                } catch let err {
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
    
    func apiForGuestAllInvitationList() {
        //        let parameter = ["user_id":UserDefaults.standard.value(forKey: "userId") as! String] as [String : Any]
        
        //        "event_id":self.eventId!
        let parameter = ["user_id":UserDefaults.standard.value(forKey: "userId") as! String,"query":self.txtSearch.text!,"event_id":self.eventId!] as [String : Any]
        print(parameter)
        
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.getGuestAllInvitation(parameter: parameter), completion: {(result) in
            switch result {
            case .success(let response):
                do {
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    
                    if response.statusCode == 200{
                        
                        
                        self.arrayOfAllInivOther = jsonData["data"]["others"].arrayValue
                        self.arrayOfAllInivGroup = jsonData["data"]["groups"].arrayValue
                        self.arrayOfAllInivPeople = jsonData["data"]["users"].arrayValue
                        print(self.arrayOfAllInivOther)
                        print(self.arrayOfAllInivGroup)
                        print(self.arrayOfAllInivPeople)
                        
                        
                        if self.invitationTabIndex == 0
                        {
                            if self.arrayOfAllInivGroup.count != 0
                            {
                                self.viewOfNoData.isHidden = true
                                self.lblNoData.isHidden = true
                                self.imgOfNoData.isHidden = true
                                self.tblViewGuestListing.dataSource = self
                                self.tblViewGuestListing.isHidden = false
                                self.tblViewGuestListing.reloadData()
                            }
                            else
                            {
                                self.viewOfNoData.isHidden = false
                                self.lblNoData.isHidden = false
                                self.imgOfNoData.isHidden = false
                                
                                self.tblViewGuestListing.isHidden = true
                            }
                        }
                        else  if self.invitationTabIndex == 1
                        {
                            if self.arrayOfAllInivPeople.count != 0
                            {
                                self.viewOfNoData.isHidden = true
                                self.lblNoData.isHidden = true
                                self.imgOfNoData.isHidden = true
                                
                                self.tblViewGuestListing.isHidden = false
                                self.tblViewGuestListing.reloadData()
                            }
                            else
                            {
                                self.viewOfNoData.isHidden = false
                                self.lblNoData.isHidden = false
                                self.imgOfNoData.isHidden = false
                                
                                self.tblViewGuestListing.isHidden = true
                            }
                        }
                        else
                        {
                            if self.arrayOfAllInivOther.count != 0
                            {
                                self.viewOfNoData.isHidden = true
                                self.lblNoData.isHidden = true
                                self.imgOfNoData.isHidden = true
                                
                                self.tblViewGuestListing.isHidden = false
                                self.tblViewGuestListing.reloadData()
                            }
                            else
                            {
                                self.viewOfNoData.isHidden = false
                                self.lblNoData.isHidden = false
                                self.imgOfNoData.isHidden = false
                                
                                self.tblViewGuestListing.isHidden = true
                            }
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.apiForGuestAllInvitationList()
                        }
                    }

                    else{
                        self.viewOfNoData.isHidden = false
                        self.lblNoData.isHidden = false
                        self.imgOfNoData.isHidden = false
                        
                        self.tblViewGuestListing.isHidden = true
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                    
                } catch let err {
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
    //MARK: - Tableview datasource and delegate method
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedIndex != 3
        {
            return self.arrayOfGuestDetails.count
        }
        else
        {
            
            if self.invitationTabIndex == 0
            {
                return self.arrayOfAllInivGroup.count
            }
            else  if self.invitationTabIndex == 1
            {
                return self.arrayOfAllInivPeople.count
            }
            else if self.invitationTabIndex == 2
            {
                return self.arrayOfAllInivOther.count
            }
            return 0
        }
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//
//        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 40))
//        headerView.backgroundColor = .white
//        let label = UILabel()
//        label.frame = CGRect.init(x: 5, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
//
//
//
//        //        self.lblAttending.text = "\(jsonData["data"]["allCount"])"
//        //        self.lblMayAttend.text = "\(jsonData["data"]["interested"])"
//        //        self.lblNotAttending.text = "\(jsonData["data"]["notGoing"])"
//        //        self.lblInvitationSend.text = "\(jsonData["data"]["invitationsSend"])"
//        var headerTitle:String!
//        if self.selectedIndex == 0
//        {
//            headerTitle = "\(self.arrayOfGuestDetails.count)"
//        }
//        else  if self.selectedIndex == 1
//        {
//            headerTitle = "\(self.arrayOfGuestDetails.count)"
//        }
//        else if self.selectedIndex == 2
//        {
//            headerTitle = "\(self.arrayOfGuestDetails.count)"
//        }
//        else
//        {
//            if self.invitationTabIndex == 0
//            {
//                headerTitle = "\(self.arrayOfAllInivGroup.count)"
//            }
//            else  if self.invitationTabIndex == 1
//            {
//                headerTitle = "\(self.arrayOfAllInivPeople.count)"
//            }
//            else if self.invitationTabIndex == 2
//            {
//                headerTitle = "\(self.arrayOfAllInivOther.count)"
//            }
//        }
//
//        var myMutableStringfirst = NSMutableAttributedString()
//        myMutableStringfirst = NSMutableAttributedString(string: headerTitle as String, attributes: [NSAttributedString.Key.font:UIFont.boldSystemFont(ofSize: 18)])
//        myMutableStringfirst.addAttribute(NSAttributedString.Key.foregroundColor, value:#colorLiteral(red: 0.2274509804, green: 0.1333333333, blue: 0.3843137255, alpha: 1) , range: NSRange(location:0,length:headerTitle.count-1))
//
//        var myMutableStringSecond = NSMutableAttributedString()
//        myMutableStringSecond = NSMutableAttributedString(string: " members", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 15)])
//        //        myMutableStringSecond.addAttribute(NSAttributedString.Key.foregroundColor, value:#colorLiteral(red: 0.2274509804, green: 0.1333333333, blue: 0.3843137255, alpha: 1) , range: NSRange(location:0,length:headerTitle.count-1))
//
//
//        myMutableStringfirst.append(myMutableStringSecond)
//
//
//
//        label.attributedText = myMutableStringfirst
//        //        label.text = DateFormatted
//
//
//        headerView.addSubview(label)
//
//        return headerView
//
//    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 40
//
//
//    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if selectedIndex != 3
        {
            return 100
        }
        else
        {
            return 80
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if selectedIndex != 3
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "GuestDetailTableViewCell", for: indexPath) as! GuestDetailTableViewCell
            
            
            
            let data = self.arrayOfGuestDetails[indexPath.row]
            if self.selectedIndex == 0
            {
                if data["others_count"].intValue == 0
                {
                    cell.lblGuestName.text = "\(data["full_name"].string ?? "Unknown")"
                    
                }
                else
                {
                    cell.lblGuestName.text = "\(data["full_name"].string ?? "Unknown") with \(data["others_count"].intValue) others"
                    
                }
            }else
            {
                cell.lblGuestName.text = "\(data["full_name"].string ?? "Unknown")"
                
            }
            cell.lblGuestLocation.text = "\(data["location"].stringValue)"
            cell.lblFirstCount.text = "\(data["total_family"].stringValue)"
            cell.lblSecondCount.text = "\(data["mutual_contact"].stringValue)"
            cell.lblFirstCountTitle.text = "families"
            cell.lblSecondCoutTitle.text = "mutual"
            let guestPic = (data["propic"].stringValue)
            if guestPic != ""
            {
                
                
                
                let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+guestPic
                let imgUrl = URL(string: temp)
                //            cell.imgViewOfGuest.kf.setImage(with: imgUrl, placeholder: nil, options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
                cell.imgViewOfGuest.kf.indicatorType = .activity
                
                cell.imgViewOfGuest.kf.setImage(with: imgUrl, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
                
            }
            else
            {
                cell.imgViewOfGuest.image = #imageLiteral(resourceName: "Male Colored")
                
            }
            
            
            
            
            //        let url = URL(string: "https://familheey.s3.amazonaws.com/logo/logo-1569301404268.png")
            //        cell.imgOfAgenda.kf.setImage(with: url)
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AllInvitationTableViewCell
            var data = JSON()
            if self.invitationTabIndex == 0
            {
                
                
                data = self.arrayOfAllInivGroup[indexPath.row]
                cell.lblName.text = data["group_name"].stringValue
                cell.lblEmail.text = data["location"].stringValue
//                cell.lblPhNo.text = data["event_name"].stringValue
                cell.lblPhNo.isHidden = true
                cell.imgLocation.isHidden = false

                
                let propic = data["logo"].stringValue
                          
                          
                          if propic != ""{
                              var temp = ""
                              temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=150&height=150&url="+"\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+propic
                              
                              
                              let imgUrl = URL(string: temp)
                              cell.imgOfUser.kf.indicatorType  = .activity
                              
                              //            cell.imgViewUser.kf.setImage(with: imgUrl, placeholder: nil, options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
                              cell.imgOfUser.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Family Logo"), options: nil, progressBlock: nil, completionHandler: nil)
                              
                          }
                          else{
                              cell.imgOfUser.image = #imageLiteral(resourceName: "Family Logo")
                          }
               
                
            }
            else  if self.invitationTabIndex == 1
            {
                data = self.arrayOfAllInivPeople[indexPath.row]
                cell.lblName.text = data["full_name"].string ?? "Unknown"
                cell.lblEmail.text = data["location"].stringValue
//                cell.lblPhNo.text = data["event_name"].stringValue
                cell.lblPhNo.isHidden = true
                cell.imgLocation.isHidden = false

                let propic = data["propic"].stringValue
                                         
                                         
                                         if propic != ""{
                                             var temp = ""
                                             temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+propic
                                             
                                             
                                             let imgUrl = URL(string: temp)
                                             cell.imgOfUser.kf.indicatorType  = .activity
                                             
                                             //            cell.imgViewUser.kf.setImage(with: imgUrl, placeholder: nil, options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
                                             cell.imgOfUser.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
                                             
                                         }
                                         else{
                                             cell.imgOfUser.image = #imageLiteral(resourceName: "Male Colored")
                                         }
            }
            else if self.invitationTabIndex == 2
            {
                data = self.arrayOfAllInivOther[indexPath.row]
                cell.lblName.text = data["full_name"].string ?? "Unknown"
                cell.lblEmail.text = data["phone"].stringValue
                cell.lblPhNo.text = data["email"].stringValue
                cell.imgOfUser.image = #imageLiteral(resourceName: "Male Colored")
                cell.imgLocation.isHidden = true
                cell.lblPhNo.isHidden = false


            }
            return cell
            
        }
        
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
