//
//  GuestDetailTableViewCell.swift
//  familheey
//
//  Created by Innovation Incubator on 25/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class GuestDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var lblSecondCoutTitle: UILabel!
    @IBOutlet weak var lblSecondCount: UILabel!
    @IBOutlet weak var lblGuestLocation: UILabel!
    @IBOutlet weak var imgViewOfGuest: UIImageView!
    
    @IBOutlet weak var lblFirstCountTitle: UILabel!
    @IBOutlet weak var lblFirstCount: UILabel!
    @IBOutlet weak var lblGuestName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
