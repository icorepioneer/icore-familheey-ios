//
//  CreateEventViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 11/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Moya
import iOSDropDown
import SwiftyJSON


class CreateEventStep1ViewController: UIViewController,UITextFieldDelegate{
    @IBOutlet weak var heightOfCategoryView: NSLayoutConstraint!
    @IBOutlet weak var txtEventName: UITextField!
    @IBOutlet weak var txtEventType: DropDown!
    @IBOutlet weak var txtEventCategory: DropDown!
    @IBOutlet weak var HeadingEventType: UILabel!
    @IBOutlet weak var HeadingEventName: UILabel!
    @IBOutlet weak var HeadingCategory: UILabel!
    @IBOutlet weak var txtOtherCategory: UITextField!
    //    @IBOutlet weak var imgPublic: UIImageView!
    //    @IBOutlet weak var imgPrivate: UIImageView!
    @IBOutlet weak var imgFree: UIImageView!
    @IBOutlet weak var imgTicketed: UIImageView!
    @IBOutlet weak var swithPrivate: UISwitch!
    @IBOutlet weak var swithSharable: UISwitch!
    @IBOutlet weak var swithOthersJoin: UISwitch!
    @IBOutlet weak var heightOfTicketType: NSLayoutConstraint!
    @IBOutlet weak var viewOfTicketType: UIView!
    @IBOutlet weak var lblNavHeading: UILabel!
    
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    
    var  eventId : String! = ""
    var isFree:String! = ""
    var isFrom = ""
    var groupId = ""
    var faEventCreate = false
    
    var eventDetails = JSON()
    var fromEventEdit = false
    var ispublic:Bool!
    var isSharing:Bool!
    var otherJoin:Bool!
    var theme: SambagTheme = .light
    var arrayOfEventType = ["Regular","Sign Up","Online"]
    var arrayOfEventCategory = ["Family", "Art", "Activities", "Causes", "Re-union", "Religion", "Business", "Investment and Finance", "Education", "Travel and Tourism", "Fashion", "Dance", "Food", "Music", "Crafts", "Networking", "Health and wellness", "Environment", "Culture", "Real Estate", "Literature", "Sports", "Movies", "Film", "Theatre", "Party", "Others"]
    var arrayOfAllCategory = [String]()
    var selectedEventType:String = ""
    var selectedEventCategory:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //   myScrollView.contentSize = CGSize(width: self.view.frame.width, height: 1150)
        print(eventId!)
        print(eventDetails)
        print(fromEventEdit)
        self.viewOfTicketType.isHidden = true
        self.heightOfTicketType.constant = 0
        self.heightOfCategoryView.constant = 75
        self.txtOtherCategory.isHidden = true
        txtEventType.isSearchEnable = false
        txtEventCategory.isSearchEnable = false
        
        if fromEventEdit{
            lblNavHeading.text = "Edit Event"
            txtEventName.text = eventDetails["event_name"].stringValue
            txtEventType.text = eventDetails["event_type"].stringValue
            txtEventCategory.text = eventDetails["category"].stringValue.firstCapitalized
            selectedEventType = eventDetails["event_type"].stringValue
            selectedEventCategory = eventDetails["category"].stringValue
            //            if eventDetails["ticket_type"].stringValue == "free"
            //            {
            //
            //                    imgFree.image = #imageLiteral(resourceName: "Green_tick.png")
            //                    imgTicketed.image = #imageLiteral(resourceName: "gray_circle.png")
            //                    isFree = "free"
            //            }
            //            else
            //            {
            //
            //                imgTicketed.image = #imageLiteral(resourceName: "Green_tick.png")
            //                imgFree.image = #imageLiteral(resourceName: "gray_circle.png")
            //                isFree = "ticketed"
            //            }
            
            if eventDetails["is_public"].boolValue == false
            {
                self.swithPrivate.isOn = true
                ispublic = false
            }
            else
            {
                self.swithPrivate.isOn = false
                ispublic = true
                
            }
            if eventDetails["is_sharable"].boolValue == false
            {
                self.swithSharable.isOn = false
                isSharing = false
            }
            else
            {
                self.swithSharable.isOn = true
                isSharing = true
            }
            
            if (eventDetails["allow_others"].boolValue == false){
                otherJoin = true
                self.swithOthersJoin.isOn = false
                
                
            }
            else{
                otherJoin = true
                self.swithOthersJoin.isOn = true
                
            }
            
            
        }
        else
        {
            lblNavHeading.text = "Add Event"
            ispublic = false
            //            imgFree.image = #imageLiteral(resourceName: "Green_tick.png")
            //            isFree = "free"
            //            imgTicketed.image = #imageLiteral(resourceName: "gray_circle.png")
            isSharing = false
            otherJoin = false
            txtEventType.text = "Online"
            txtEventCategory.text = "Activities"
            swithOthersJoin.isEnabled = false
            
            
        }
        self.setTextFiledPlaceholder()
        
        
        txtEventName.delegate = self
        txtEventType.delegate = self
        txtEventCategory.delegate = self
        txtOtherCategory.delegate = self
        self.txtEventName.autocapitalizationType      = .sentences
        self.txtOtherCategory.autocapitalizationType      = .sentences
        
        Helpers.setleftView(textfield: txtEventName, customWidth: 15)
        Helpers.setleftView(textfield: txtEventType, customWidth: 15)
        Helpers.setleftView(textfield: txtEventCategory, customWidth: 15)
        Helpers.setleftView(textfield: txtOtherCategory, customWidth: 15)
        
        txtEventType.optionArray = self.arrayOfEventType.sorted()
        txtEventType.didSelect {(selectedText , index ,id) in
            self.selectedEventType = selectedText
            if selectedText.lowercased() == "online"{
                self.swithOthersJoin.isOn = false
                self.swithOthersJoin.isEnabled = false
            }
            else{
                self.swithOthersJoin.isEnabled = true
            }
            print(selectedText)
        }
        
        
        if appDel.arrayOfAllCategory.count == 0
        {
            self.apiForAllCategoryList()
        }
        else{
            txtEventCategory.optionArray = appDel.arrayOfAllCategory
            
        }
        txtEventCategory.didSelect {(selectedText , index ,id) in
            self.selectedEventCategory = selectedText
            if selectedText == "Others"{
                self.heightOfCategoryView.constant = 120
                self.txtOtherCategory.text = ""
                self.txtOtherCategory.isHidden = false
            }
            else
            {
                self.heightOfCategoryView.constant = 75
                self.txtOtherCategory.isHidden = true
            }
            print(selectedText)
        }
        
        // Do any additional setup after loading the view.
        
        //        self.apiForAllCategoryList()
    }
    
    func setTextFiledPlaceholder(){
        self.HeadingEventType.attributedText = Helpers.setAttributedPlaceHolder(placeHolderName: "Event Type")
        self.HeadingEventName.attributedText = Helpers.setAttributedPlaceHolder(placeHolderName: "Event Name")
        self.HeadingCategory.attributedText = Helpers.setAttributedPlaceHolder(placeHolderName: "Category")
    }
    
    
    @IBAction func btnFree_OnClick(_ sender: Any) {
        //        if let button = sender as? UIButton {
        //            button.tintColor = .clear
        //            if button.tag == 1
        //            {
        //                imgTicketed.image = #imageLiteral(resourceName: "Green_tick.png")
        //                imgFree.image = #imageLiteral(resourceName: "gray_circle.png")
        //                isFree = "ticketed"
        //
        //
        //            }
        //            else
        //            {
        //                imgFree.image = #imageLiteral(resourceName: "Green_tick.png")
        //                imgTicketed.image = #imageLiteral(resourceName: "gray_circle.png")
        //                isFree = "free"
        //
        //
        //            }
        //        }
        
    }
    
    @IBAction func btnPrivateOnClick(_ sender: UISwitch) {
        if (sender.isOn == true){
            ispublic = false
            
        }
        else{
            ispublic = true
            
        }
        
    }
    @IBAction func btnsharable_OnClick(_ sender: UISwitch) {
        if (sender.isOn == true){
            isSharing = true
            
        }
        else{
            isSharing = false
        }
        
    }
    
    @IBAction func btnOthersJoin_OnClick(_ sender: UISwitch) {
        if (sender.isOn == true){
            otherJoin = true
            
        }
        else{
            otherJoin = false
        }
        
    }
    
    
    
    
    
    //    @IBAction func didChangeTheme() {
    //        switch theme {
    //        case .light: theme = .dark
    //        case .dark: theme = .light
    //        default: break
    //        }
    //    }
    
    
    
    //MARK:- VALIDATION
    func DatavalidationForSubmit()->Bool{
        
        if txtEventName.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            
            Helpers.showAlertDialog(message: "Event Name is empty", target: self)
            return false
        }
        else if txtEventType.text == "" {
            
            Helpers.showAlertDialog(message: "Event Type is empty", target: self)
            return false
            
        }
        else if txtEventCategory.text == "" {
            
            Helpers.showAlertDialog(message: "Event Category is empty", target: self)
            return false
        }
        else if txtEventCategory.text == "Others"
        {
            if txtOtherCategory.text == "" {
                
                Helpers.showAlertDialog(message: "Please enter Category Type", target: self)
                return false
            }
            else
            {
                return true
                
            }
        }
        else {
            
            return true
            
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtOtherCategory
        {
            if arrayOfAllCategory.contains(self.txtOtherCategory.text!)
            {
                Helpers.showAlertDialog(message: "Entered category is already exist ,Please select category in above options", target: self)
                
            }
            else
            {
                
            }
        }
    }
    func apiForAllCategoryList() {
        
        let parameter = [:] as [String : Any]
        print(parameter)
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.FetchAllCategory(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    print(jsonData["status"])
                    print(response.statusCode)
                    
                    if response.statusCode == 200{
                        let arrayOfCategoryResponse:Array<JSON> = jsonData["data"].arrayValue
                        print(jsonData["data"]["name"])
                        //                        self.arrayOfAllCategory = arrayOfCategoryResponse["name"] as! [String]
                        self.arrayOfAllCategory = []
                        for category in arrayOfCategoryResponse
                        {
                            self.arrayOfAllCategory.append(category["name"].stringValue.firstCapitalized)
                        }
                        print(self.arrayOfAllCategory)
                        self.txtEventCategory.optionArray = self.arrayOfAllCategory.sorted()
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.apiForAllCategoryList()
                        }
                    }

                    else
                    {
                        
                        print(jsonData["code"])
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                    
                    
                } catch let err {
                    
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    //MARK: - Button Actions
    @IBAction func btnContinue_OnClick(_ sender: Any) {
        if DatavalidationForSubmit(){
            //            "ticket_type":self.isFree!
            var parameter = [String : Any]()
            if txtEventCategory.text == "Others"
            {
                parameter = ["created_by":UserDefaults.standard.value(forKey: "userId") as! String,"event_name":self.txtEventName.text!,"event_type":self.txtEventType.text!,"event_category":self.txtOtherCategory.text!.lowercased(),"is_public":"\(self.ispublic!)","is_sharable":self.isSharing!,"allow_others":self.otherJoin!] as [String : Any]
            }else
            {
                parameter = ["created_by":UserDefaults.standard.value(forKey: "userId") as! String,"event_name":self.txtEventName.text!,"event_type":self.txtEventType.text!,"event_category":self.txtEventCategory.text!.lowercased(),"is_public":"\(self.ispublic!)","is_sharable":self.isSharing!,"allow_others":self.otherJoin!] as [String : Any]
            }
            
            
            //            self.apiForCreateEventStep1()
            let storyboard = UIStoryboard.init(name: "third", bundle: nil)
            //                // let addMember = storyboard.instantiateViewController(withIdentifier: "viewRequestsTableViewController") as! viewRequestsTableViewController
            let vc = storyboard.instantiateViewController(withIdentifier: "CreateEventsStep2ViewController") as! CreateEventsStep2ViewController
            vc.dataFromPrevious = parameter
            vc.eventDetails = self.eventDetails
            vc.fromEventEdit = self.fromEventEdit
            vc.eventId = self.eventId
            vc.isFrom = self.isFrom
            vc.groupId = self.groupId
            vc.faEventCreate = self.faEventCreate
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    @IBAction func btnBack_OnClick(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
}




//extension UITextField {
//    func setLeftPaddingPoints(_ amount:CGFloat){
//        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
//        self.leftView = paddingView
//        self.leftViewMode = .always
//    }
//    func setRightPaddingPoints(_ amount:CGFloat) {
//        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
//        self.rightView = paddingView
//        self.rightViewMode = .always
//    }
//}



//class UITextFieldPadding : UITextField {
//
//  let padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
//
//  required init?(coder aDecoder: NSCoder) {
//    super.init(coder: aDecoder)
//  }
//
//  override func textRect(forBounds bounds: CGRect) -> CGRect {
//    return bounds.inset(by: padding)
//  }
//
//  override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
//    return bounds.inset(by: padding)
//  }
//
//  override func editingRect(forBounds bounds: CGRect) -> CGRect {
//    return bounds.inset(by: padding)
//  }
//}
