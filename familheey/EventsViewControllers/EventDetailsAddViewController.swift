//
//  EventDetailsAddViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 19/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON


class EventDetailsAddViewController: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITableViewDataSource,UITableViewDelegate,agendaCreationDelegate,signUpCreationDelegate,UIGestureRecognizerDelegate,ContributionAddDelegate,createAlbumdelegate, UITextFieldDelegate{
    @IBOutlet var viewOfDocuments: UIView!
    @IBOutlet weak var viewOfAlbum: UIView!
    @IBOutlet var viewOfSignups: UIView!
    @IBOutlet var viewOfAgenda: UIView!
    //    @IBOutlet var viewOfItems: UIView!
    @IBOutlet weak var viewOfGuest: UIView!
    @IBOutlet weak var viewOfTab: UIView!
    @IBOutlet weak var btnInviteTop: UIButton!
    @IBOutlet weak var btnAttending: UIButton!
    @IBOutlet weak var btnInvitationSend: UIButton!
    
    @IBOutlet weak var deleteDocView: UIView!
    @IBOutlet weak var deleteView: UIView!
    @IBOutlet weak var btnNotInteresting: UIButton!
    @IBOutlet weak var btnMayAttend: UIButton!
    
    
    //Guest ui design elements
    
    @IBOutlet weak var imgViewOfEvent: UIImageView!
    @IBOutlet weak var lblNavHeading: UILabel!
    @IBOutlet weak var lblInvitationSend: UILabel!
    @IBOutlet weak var lblNotAttending: UILabel!
    @IBOutlet weak var lblAttending: UILabel!
    @IBOutlet weak var lblMayAttend: UILabel!
    @IBOutlet weak var imgViewNoAgenda: UIImageView!
    @IBOutlet weak var viewOfNoAgenda: UIView!
    @IBOutlet weak var btnAddAgendaInListing: UIButton!
    @IBOutlet weak var btnAgendaAdd: UIButton!
    @IBOutlet weak var lblNoDataHeading2: UILabel!
    @IBOutlet weak var lblNoDataHeading1: UILabel!
    @IBOutlet weak var viewOfSignUpData: UIView!
    @IBOutlet weak var viewOfNoSignups: UIView!
    @IBOutlet weak var imgViewOfNoSignups: UIImageView!
    @IBOutlet weak var lblNoSignUpHeading1: UILabel!
    @IBOutlet weak var lblNoSignUpHeading2: UILabel!
    @IBOutlet weak var btnAddSignUp: UIButton!
    @IBOutlet weak var btnAddSignupInList: UIButton!
    @IBOutlet weak var viewOfNoAlbum: UIView!
    @IBOutlet weak var btnCreateAlbum: UIButton!
    @IBOutlet weak var lblNoAlbumHeading1: UILabel!
    @IBOutlet weak var lblNoAlbumHeading2: UILabel!
    @IBOutlet weak var btnNewAlbum: UIButton!
    @IBOutlet weak var collViewOfAlbum: UICollectionView!
    @IBOutlet weak var btCreateAlbumList: UIButton!
    @IBOutlet weak var txtSearchAlbums: UITextField!
    @IBOutlet weak var btnSearchRestAlbum: UIButton!
    
    @IBOutlet weak var viewOfNoDocuments: UIView!
    @IBOutlet weak var lblNoDocHeading1: UILabel!
    @IBOutlet weak var lblNoDocHeading2: UILabel!
    
    @IBOutlet weak var collViewOfDocuments: UICollectionView!
    @IBOutlet weak var btnCreateDoc2: UIButton!
    @IBOutlet weak var btnCreateDoc1: UIButton!
    @IBOutlet weak var btnCreateDocInList: UIButton!
    
    @IBOutlet weak var txtSearchtDoc: UITextField!
    @IBOutlet weak var btnSearchResetDoc: UIButton!
    
    @IBOutlet weak var tblViewOfAgenda: UITableView!
    //    @IBOutlet weak var tblViewOfItems: UITableView!
    @IBOutlet weak var tblViewOfSignups: UITableView!
    @IBOutlet weak var collVw                         : UICollectionView!
    
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    
    var arrayOfDocumentsList = JSON()
    var arrayOfAlbumList                                        = JSON()
    var  eventId : String!
    var deleteAjendaID = Int()
    var deleteSignupID = Int()
    
    var amCreator:Bool!
    var pastEvents:Bool! = false
    
    
    var eventDetails = JSON()
    var arrayOfFullAgenda = [JSON]()
    var guestCountResponse = JSON()
    var arrayOfSignUpList = [JSON]()
    var arrSelectedIndex = [Int]()
    var arrrofUsers = [String]()
    var isFromMultiSelect = false
        
    var titleArr : [String]!
    var selectedIndex:Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Helpers.setleftView(textfield: txtSearchAlbums, customWidth: 30)
        txtSearchAlbums.delegate = self
        Helpers.setleftView(textfield: txtSearchtDoc, customWidth: 30)
        txtSearchtDoc.delegate = self
        
        
        
        deleteView.isHidden = true
        deleteDocView.isHidden = true
        
        print("Event Details : \(eventDetails)")
        
        lblNavHeading.text              = eventDetails["event_name"].stringValue
        
        let event_pic = (eventDetails["event_image"].stringValue)
        
        if event_pic != "" && event_pic != "undefined"
        {
            
            //            imgOfEvents.isHidden = false
            
            
            let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.event_image)"+event_pic
            let imgUrl = URL(string: temp)
            imgViewOfEvent.kf.indicatorType  = .activity
            
            //            imgViewOfEvent.kf.setImage(with: imgUrl, placeholder: nil, options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
            imgViewOfEvent.kf.setImage(with: imgUrl, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
            
        }
        else{
            
            //            imgOfEvents.isHidden = true
            //            imgOfEvents.image =
            imgViewOfEvent.image = #imageLiteral(resourceName: "default_event_image.jpg")
            
            
        }
        if UserDefaults.standard.value(forKey: "userId") as! String == eventDetails["created_by"].stringValue
        {
            //            self.btnAgendaAdd.isHidden = false
            //            self.btnAddAgendaInListing.isHidden = false
            self.amCreator = true
        }
        else
        {
            self.amCreator = false
            
            //            self.btnAgendaAdd.isHidden = true
            //            self.btnAddAgendaInListing.isHidden = true
        }
        //        var fDate = ""
        
        //        if eventDetails["from_date"].stringValue.count > 0{
        //                   fDate = convertTimeStampString(timestamp: eventDetails["from_date"].stringValue)
        //            if self.checkDateLessThanCurrentDate(selectedDatestring:fDate )
        //            {
        //                self.pastEvents = true
        //
        //
        //
        //            }
        //            else
        //            {
        //                self.pastEvents = false
        //
        //            }
        //
        //               }
        if eventDetails["event_type"] == "Regular"
        {
            titleArr = ["AGENDA", "GUESTS","ALBUMS","DOCUMENTS"]
 
        }
        else if eventDetails["event_type"].stringValue.lowercased() == "online"{
            titleArr = ["AGENDA", "GUESTS","ALBUMS","DOCUMENTS"]
      
        }
        else
        {
            titleArr = ["AGENDA", "GUESTS", "SIGN UP","ALBUMS","DOCUMENTS"]
        }
       selectedIndex = selectedIndex - 1

        print(selectedIndex)
        
        self.viewOfGuest.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.viewOfTab.frame.height)
        self.viewOfAgenda.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.viewOfTab.frame.height)
        self.viewOfSignups.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.viewOfTab.frame.height)
        self.viewOfAlbum.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.viewOfTab.frame.height)
        self.viewOfDocuments.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.viewOfTab.frame.height)
        //        if selectedIndex == 0
        //        {
        //        self.viewOfTab.addSubview(self.viewOfAgenda)
        //        }
        //       else if selectedIndex == 1
        //        {
        //            self.viewOfTab.addSubview(self.viewOfGuest)
        //        }
        //        else if selectedIndex == 2
        //        {
        //            if eventDetails["event_type"] == "Regular"
        //            {
        //                self.viewOfTab.addSubview(self.viewOfAlbum)
        //
        //            }
        //            else
        //                {
        //                    self.viewOfTab.addSubview(self.viewOfSignups)
        //
        //            }
        //
        //        }
        //        else if selectedIndex == 3
        //        {
        //            if eventDetails["event_type"] == "Regular"
        //            {
        //                self.viewOfTab.addSubview(self.viewOfDocuments)
        //
        //            }
        //            else
        //            {
        //                self.viewOfTab.addSubview(self.viewOfAlbum)
        //
        //            }
        //
        //        }
        //        else
        //        {
        //            self.viewOfTab.addSubview(self.viewOfDocuments)
        //
        //        }
        //
        if amCreator{
            if pastEvents
            {
                btnInviteTop.isHidden = true
                
            }
            else
            {
                btnInviteTop.isHidden = false
            }
        }
        else{
            btnInviteTop.isHidden = true
        }
        
        self.tblViewOfAgenda.dataSource = self
        self.tblViewOfAgenda.delegate = self
        self.tblViewOfSignups.dataSource = self
        self.tblViewOfSignups.delegate = self
        self.collViewOfAlbum.dataSource = self
        self.collViewOfAlbum.delegate = self
        self.collViewOfDocuments.dataSource = self
        self.collViewOfDocuments.delegate = self
        
        
        //        self.tblViewOfAgenda.estimatedRowHeight = 120
        //        self.tblViewOfAgenda.rowHeight = UITableView.automaticDimension
        //
        //        self.tblViewOfSignups.estimatedRowHeight = 130
        //        self.tblViewOfSignups.rowHeight = UITableView.automaticDimension
        
        tblViewOfAgenda.tableHeaderView = UIView.init()
        tblViewOfAgenda.tableFooterView = UIView.init()
        tblViewOfSignups.tableHeaderView = UIView.init()
        tblViewOfSignups.tableFooterView = UIView.init()
        if amCreator{
            if !pastEvents
            {
                
                setupLongPressGesture()
                
            }
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: - Time Stamp
    
    func convertTimeStampString(timestamp:String) -> String {
        let date = Date(timeIntervalSince1970: Double(timestamp) as! TimeInterval)
        //    dateFormatter.dateFormat       = "EEE MMM dd yyy"
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MMM/yyyy hh:mm a"
        //            formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.timeZone = TimeZone.current
        
        
        let localDatef = formatter.string(from: date)
        print(localDatef)
        return localDatef
    }
    
    func checkDateLessThanCurrentDate (selectedDatestring:String)->Bool
    {
        print(selectedDatestring)
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MMM/yyyy hh:mm a"
        
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "dd/MMM/yyyy hh:mm a"
        
        
        let convertedDateNow = formatter.string(from: Date())
        let convertedDateNow2 = formatter.date(from: convertedDateNow)
        formatter2.timeZone = TimeZone.current
        let dateNow = formatter2.string(from: convertedDateNow2!)
        print(dateNow)
        print(selectedDatestring < dateNow)
        return selectedDatestring < dateNow
    }
    override func viewWillAppear(_ animated: Bool) {
        
        
        if selectedIndex == 0
        {
            
            self.apiForAgendaListing()
            
            
        }
        else if selectedIndex == 1
        {
            
            self.apiForGetGuestCount()
        }
        else if selectedIndex == 2
        {
            if eventDetails["event_type"] == "Regular" || eventDetails["event_type"].stringValue.lowercased() == "online"
            {
                
                self.ApiForAllAlbumsListing()
            }
            else
            {
                self.apiForSignUpListing()
                
            }
            
        }
            
        else if selectedIndex == 3
        {
            if eventDetails["event_type"] == "Regular" || eventDetails["event_type"].stringValue.lowercased() == "online"
            {
                
                self.ApiForAllDocumentsListing()
                
                
            }
            else
            {
                
                self.ApiForAllAlbumsListing()
                
            }
        }
        else
        {
            self.ApiForAllDocumentsListing()
        }
        
    }
    @IBAction func btnBack_OnClick(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: false)
    }
    
    //MARK:- InviteButton Action
    @IBAction func onClickDelete(_ sender: Any) {
        let alert = UIAlertController(title: "Familheey", message: "Do you want to delete \(self.arrrofUsers.count) albums", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { _ in
            
            /* let data = self.dataArr[indexPath.row]
             self.deleteFolder(fId: data["id"].stringValue)*/
            var parameter =  [String : Any]()
            parameter = ["folder_id":self.arrrofUsers,"user_id":UserDefaults.standard.value(forKey: "userId") as! String]
            print(parameter)
            ActivityIndicatorView.show("Loading....")
            
            self.networkProvider.request(.deleteFolder(parameter: parameter)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                        print(jsonData)
                        if response.statusCode == 200{
                            self.arrrofUsers = []
                            self.arrSelectedIndex = []
                            self.deleteView.isHidden = true
                            self.deleteDocView.isHidden = true
                            self.isFromMultiSelect = false
                            
                            if self.eventDetails["event_type"] == "Regular" || self.eventDetails["event_type"].stringValue.lowercased() == "online"{
                                if self.selectedIndex == 2{
                                    self.ApiForAllAlbumsListing()
                                }
                                else{
                                    self.ApiForAllDocumentsListing()
                                }
                            }
                            else{
                                if self.selectedIndex == 3{
                                    self.ApiForAllAlbumsListing()
                                }
                                else{
                                    self.ApiForAllDocumentsListing()
                                }
                            }
                        }
                        else if response.statusCode == 401 {
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                let btn = UIButton()
                                
                                self.onClickDelete(btn)
                            }
                        }
                        
                        ActivityIndicatorView.hiding()
                        
                    }catch let err {
                        ActivityIndicatorView.hiding()
                        Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                    }
                case .failure( let error):
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                    break
                }
            }
            
        }))
        alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: { _ in
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnInvite_OnClick(_ sender: Any) {
        
        let stryboard = UIStoryboard.init(name: "Main", bundle: nil)
        
        let vc = stryboard.instantiateViewController(withIdentifier: "EventShareViewController") as! EventShareViewController
        vc.typeOfInvitations = "invitation"
        vc.eventId = eventId
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- collectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collViewOfAlbum
        {
            return arrayOfAlbumList.count
        }
        else if collectionView == collViewOfDocuments
        {
            return arrayOfDocumentsList.count
        }
        else
        {
            return titleArr.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collViewOfAlbum{
            
            let cell = collViewOfAlbum.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
            let tempData = self.arrayOfAlbumList[indexPath.row]
            
            if let img = cell.viewWithTag(1) as? UIImageView{
                if tempData["cover_pic"].stringValue.count != 0 {
                    
                    img.kf.indicatorType  = .activity
                    img.kf.setImage(with: tempData["cover_pic"].url!, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                }
                else{
                    img.image = #imageLiteral(resourceName: "imgNoImage")
                }
            }
            
            if let name = cell.viewWithTag(2) as? UILabel{
                if tempData["folder_name"].stringValue.count != 0{
                    name.text = tempData["folder_name"].stringValue
                }else{
                    name.text = "NA"
                }
            }
            
            if let hosted = cell.viewWithTag(3) as? UILabel{
                if tempData["created_by_name"].stringValue.count != 0{
                    hosted.text = tempData["created_by_name"].stringValue
                }else
                {
                    hosted.text = "NA"
                }
            }
            //            if let count = cell.viewWithTag(6) as? UILabel{
            //                if tempData["doc_count"].stringValue.count != 0{
            //                    count.text =  tempData["doc_count"].stringValue
            //                }else{
            //                    count.text = "0"
            //                }
            //            }
            if let dat = cell.viewWithTag(4) as? UILabel{
                if tempData["created_at"].stringValue.count != 0{
                    //                    let temp = tempData["created_at"].stringValue
                    
                    //                    let arr = temp.components(separatedBy: "T")
                    //
                    //                    dat.text = arr.first
                    
                    dat.text = self.convertDateToDisplayDateInAlbumOrDocuments(dateFromResponse:tempData["created_at"].stringValue)
                    
                }else{
                    dat.text = "NA"
                }
                
            }
            
            if let bgView = cell.viewWithTag(5){
                
                if arrSelectedIndex.count == 0{
                    bgView.isHidden = true
                }
                else{
                    
                    if arrSelectedIndex.contains(indexPath.row){
                        bgView.isHidden = false
                    }
                    else{
                        bgView.isHidden = true
                    }
                }
                
            }
            
            return cell
        }
        else if collectionView == collViewOfDocuments{
            let cell = collViewOfDocuments.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
            let tempData = self.arrayOfDocumentsList[indexPath.row]
            
            if let img = cell.viewWithTag(1) as? UIImageView{
                if tempData["cover_pic"].stringValue.count != 0 {
                    
                    img.kf.indicatorType  = .activity
                    img.kf.setImage(with: tempData["cover_pic"].url!, placeholder: #imageLiteral(resourceName: "FolderImg"), options: nil, progressBlock: nil, completionHandler: nil)
                }
                else{
                    img.image = #imageLiteral(resourceName: "FolderImg")
                }
            }
            
            if let name = cell.viewWithTag(2) as? UILabel{
                if tempData["folder_name"].stringValue.count != 0{
                    name.text = tempData["folder_name"].stringValue
                }else{
                    name.text = "NA"
                }
            }
            
            if let hosted = cell.viewWithTag(3) as? UILabel{
                if tempData["created_by_name"].stringValue.count != 0{
                    hosted.text = tempData["created_by_name"].stringValue
                }else
                {
                    hosted.text = "NA"
                }
            }
            
            if let dat = cell.viewWithTag(4) as? UILabel{
                if tempData["created_at"].stringValue.count != 0{
                    //                    let temp = tempData["created_at"].stringValue
                    //                    let arr = temp.components(separatedBy: "T")
                    //
                    //                    dat.text = arr.first
                    
                    dat.text = self.convertDateToDisplayDateInAlbumOrDocuments(dateFromResponse:tempData["created_at"].stringValue)
                    
                }else{
                    dat.text = "NA"
                }
                
            }
            if let bgView = cell.viewWithTag(5){
                if arrSelectedIndex.count == 0{
                    bgView.isHidden = true
                }
                else{
                    if arrSelectedIndex.contains(indexPath.row){
                        bgView.isHidden = false
                    }
                    else{
                        bgView.isHidden = true
                    }
                }
            }
            
            return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchTitlesCollectionViewCell", for: indexPath as IndexPath) as! SearchTitlesCollectionViewCell
            cell.lblTitle.text = titleArr[indexPath.item]
            
            
            
            if indexPath.item == selectedIndex{
                if selectedIndex == 0
                {
                    self.apiForAgendaListing()
                    self.viewOfGuest.removeFromSuperview()
                    self.viewOfSignups.removeFromSuperview()
                    self.viewOfAlbum.removeFromSuperview()
                    self.viewOfDocuments.removeFromSuperview()
                    
                    //     self.apiForAgendaListing()
                    
                    self.viewOfTab.addSubview(self.viewOfAgenda)
                    
                }
                else if selectedIndex == 1
                {
                    self.apiForGetGuestCount()
                    self.viewOfAgenda.removeFromSuperview()
                    self.viewOfSignups.removeFromSuperview()
                    //                self.apiForGetGuestCount()
                    self.viewOfAlbum.removeFromSuperview()
                    self.viewOfDocuments.removeFromSuperview()
                    
                    self.viewOfTab.addSubview(self.viewOfGuest)
                }
                else if selectedIndex == 2
                {
                    if eventDetails["event_type"] == "Regular" || eventDetails["event_type"].stringValue.lowercased() == "online"
                    {
                        
                        self.ApiForAllAlbumsListing()
                        self.viewOfAgenda.removeFromSuperview()
                        self.viewOfGuest.removeFromSuperview()
                        self.viewOfSignups.removeFromSuperview()
                        self.viewOfDocuments.removeFromSuperview()
                        
                        self.viewOfTab.addSubview(self.viewOfAlbum)
                    }
                    else
                    {
                        self.apiForSignUpListing()
                        self.viewOfAgenda.removeFromSuperview()
                        self.viewOfGuest.removeFromSuperview()
                        self.viewOfAlbum.removeFromSuperview()
                        self.viewOfDocuments.removeFromSuperview()
                        
                        self.viewOfTab.addSubview(self.viewOfSignups)
                    }
                    
                }
                    
                else if selectedIndex == 3
                {
                    if eventDetails["event_type"] == "Regular" || eventDetails["event_type"].stringValue.lowercased() == "online"
                    {
                        self.viewOfAgenda.removeFromSuperview()
                        self.viewOfGuest.removeFromSuperview()
                        self.viewOfSignups.removeFromSuperview()
                        self.viewOfAlbum.removeFromSuperview()
                        
                        self.ApiForAllDocumentsListing()
                        
                        self.viewOfTab.addSubview(self.viewOfDocuments)
                        
                    }
                    else
                    {
                        self.viewOfAgenda.removeFromSuperview()
                        self.viewOfGuest.removeFromSuperview()
                        self.viewOfSignups.removeFromSuperview()
                        self.viewOfDocuments.removeFromSuperview()
                        
                        self.ApiForAllAlbumsListing()
                        
                        self.viewOfTab.addSubview(self.viewOfAlbum)
                        
                    }
                    
                }
                    
                else
                {
                    self.viewOfAgenda.removeFromSuperview()
                    self.viewOfGuest.removeFromSuperview()
                    self.viewOfSignups.removeFromSuperview()
                    self.viewOfAlbum.removeFromSuperview()
                    
                    self.ApiForAllDocumentsListing()
                    
                    self.viewOfTab.addSubview(self.viewOfDocuments)
                    
                }
                
                cell.lblTitle.font                  = UIFont.boldSystemFont(ofSize: 17)
                
                if cell.lblTitle.text               == "DOCUMENTS"{
                    cell.lblTitle.font              = UIFont.boldSystemFont(ofSize: 15)
                }
                
                cell.lblTitle.textColor             = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                cell.viewSelection.backgroundColor  = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                cell.viewSelection.isHidden         = false
            }else{
                
                cell.lblTitle.font                  = UIFont.systemFont(ofSize: 14)
                cell.lblTitle.textColor             = .darkGray
                cell.viewSelection.isHidden         = true
                cell.viewSelection.backgroundColor  = UIColor.lightGray
            }
            
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collViewOfAlbum {
            if isFromMultiSelect{
                
                if let index = arrSelectedIndex.firstIndex(of: indexPath.row){
                    
                    arrSelectedIndex.remove(at: index)
                    arrrofUsers.remove(at: index)
                }
                else{
                    arrSelectedIndex.append(indexPath.row)
                    let data = self.arrayOfAlbumList[indexPath.row]
                    arrrofUsers.append(data["id"].stringValue)
                }
                
                self.collVw.reloadData()
                if arrSelectedIndex.count == 0{
                    self.deleteView.isHidden = true
                    self.deleteDocView.isHidden = true
                    
                    isFromMultiSelect = false
                    
                }
                
            }
            else{
                let AlbumDetailsViewController = self.storyboard?.instantiateViewController(withIdentifier: "AlbumDetailsViewController") as! AlbumDetailsViewController
                let tempData = self.arrayOfAlbumList[indexPath.row]
                AlbumDetailsViewController.albumDetailsDict = self.arrayOfAlbumList[indexPath.row]
                AlbumDetailsViewController.isFrom = "events"
                AlbumDetailsViewController.isFromDocs = false
                
                AlbumDetailsViewController.groupId = self.eventId
                AlbumDetailsViewController.folder_type = "albums"
                AlbumDetailsViewController.createdBy = tempData["created_by"].stringValue
                self.navigationController?.pushViewController(AlbumDetailsViewController, animated: true)
            }
        }
        else if collectionView == collViewOfDocuments
        {
            if isFromMultiSelect{
                if let index = arrSelectedIndex.firstIndex(of: indexPath.row){
                    arrSelectedIndex.remove(at: index)
                    arrrofUsers.remove(at: index)
                }
                else{
                    arrSelectedIndex.append(indexPath.row)
                    let data = self.arrayOfAlbumList[indexPath.row]
                    arrrofUsers.append(data["id"].stringValue)
                }
                self.collVw.reloadData()
                if arrSelectedIndex.count == 0{
                    self.deleteView.isHidden = true
                    self.deleteDocView.isHidden = true
                    isFromMultiSelect = false
                }
            }
            else{
                let AlbumDetailsViewController = self.storyboard?.instantiateViewController(withIdentifier: "AlbumDetailsViewController") as! AlbumDetailsViewController
                let tempData = self.arrayOfDocumentsList[indexPath.row]
                AlbumDetailsViewController.albumDetailsDict = self.arrayOfDocumentsList[indexPath.row]
                AlbumDetailsViewController.isFrom = "events"
                AlbumDetailsViewController.isFromDocs = true
                AlbumDetailsViewController.groupId = self.eventId
                AlbumDetailsViewController.folder_type = "documents"
                AlbumDetailsViewController.createdBy = tempData["created_by"].stringValue
                self.navigationController?.pushViewController(AlbumDetailsViewController, animated: true)
            }
            
            
        }
            
        else{
            selectedIndex = indexPath.item
            collVw.reloadData()
            if selectedIndex == 0
            {
                self.viewOfGuest.removeFromSuperview()
                self.viewOfSignups.removeFromSuperview()
                self.viewOfAlbum.removeFromSuperview()
                self.viewOfDocuments.removeFromSuperview()
                self.apiForAgendaListing()
                self.viewOfTab.addSubview(self.viewOfAgenda)
                
            }
            else if selectedIndex == 1
            {
                self.viewOfAgenda.removeFromSuperview()
                self.viewOfSignups.removeFromSuperview()
                self.viewOfAlbum.removeFromSuperview()
                self.viewOfDocuments.removeFromSuperview()
                
                self.apiForGetGuestCount()
                self.viewOfTab.addSubview(self.viewOfGuest)
            }
            else if selectedIndex == 2
            {
                if eventDetails["event_type"] == "Regular" || eventDetails["event_type"].stringValue.lowercased() == "online"
                {
                    
                    self.ApiForAllAlbumsListing()
                    self.viewOfAgenda.removeFromSuperview()
                    self.viewOfGuest.removeFromSuperview()
                    self.viewOfSignups.removeFromSuperview()
                    self.viewOfDocuments.removeFromSuperview()
                    
                    self.viewOfTab.addSubview(self.viewOfAlbum)
                }
                else
                {
                    self.apiForSignUpListing()
                    self.viewOfAgenda.removeFromSuperview()
                    self.viewOfGuest.removeFromSuperview()
                    self.viewOfAlbum.removeFromSuperview()
                    self.viewOfDocuments.removeFromSuperview()
                    
                    
                    self.viewOfTab.addSubview(self.viewOfSignups)
                }
                
            }
                
                
                
            else if selectedIndex == 3
            {
                if eventDetails["event_type"].stringValue.lowercased() == "regular"
                {
                    self.viewOfAgenda.removeFromSuperview()
                    self.viewOfGuest.removeFromSuperview()
                    self.viewOfSignups.removeFromSuperview()
                    self.viewOfAlbum.removeFromSuperview()
                    
                    self.ApiForAllDocumentsListing()
                    
                    self.viewOfTab.addSubview(self.viewOfDocuments)
                    
                }
                else
                {
                    self.viewOfAgenda.removeFromSuperview()
                    self.viewOfGuest.removeFromSuperview()
                    self.viewOfSignups.removeFromSuperview()
                    self.viewOfDocuments.removeFromSuperview()
                    
                    self.ApiForAllAlbumsListing()
                    
                    self.viewOfTab.addSubview(self.viewOfAlbum)
                    
                }
            }
            else
            {
                self.viewOfAgenda.removeFromSuperview()
                self.viewOfGuest.removeFromSuperview()
                self.viewOfSignups.removeFromSuperview()
                self.viewOfAlbum.removeFromSuperview()
                
                self.ApiForAllDocumentsListing()
                
                self.viewOfTab.addSubview(self.viewOfDocuments)
                
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collViewOfAlbum{
            let numberOfCellInRow  = 2
            let padding : Int      = 5
            let collectionCellWidth : CGFloat = (self.collViewOfAlbum.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
            return CGSize(width: collectionCellWidth, height: collectionCellWidth*1.3)
        }
        else  if collectionView == collViewOfDocuments{
            let numberOfCellInRow  = 2
            let padding : Int      = 5
            let collectionCellWidth : CGFloat = (self.collViewOfDocuments.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
            return CGSize(width: collectionCellWidth, height: collectionCellWidth*1.3)
        }
        else
        {
            let numberOfCellInRow  = 4//titleArr.count
            let padding : Int      = 3
            let collectionCellWidth : CGFloat = (self.collVw.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
            return CGSize(width: collectionCellWidth, height: 60)
            //            return titleArr[indexPath.row].size(withAttributes: nil)
        }
    }
    
    
    func convertDateToDisplayDate(dateFromResponse:String)->String
    {
        if dateFromResponse != ""
        {
            
            let formatter         = DateFormatter()
            formatter.dateStyle   = .long
            //            formatter.dateFormat  = "yyyy-MM-dd HH:mm:ss +0000"
            //            formatter.dateFormat = "dd-MM-yyyy hh:mm a"
            //            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            
            
            //                    formatter.timeZone = .current
            formatter.timeZone = TimeZone(identifier: "UTC")
            
            
            let result            = formatter.date(from: "\(dateFromResponse)")
            
            let formatter2        = DateFormatter()
            formatter2.dateStyle  = .long
            formatter2.dateFormat = "MMM dd yyyy"
            //            formatter2.dateFormat = "MMM dd yyyy hh:mm a"
            //            formatter2.dateFormat = "MMM dd yyyy"
            
            
            let val               = formatter2.string(from: result!)
            print("Val :\(val)")
            return val
            //            let dateAsString               = dateFromResponse
            //            let dateFormatter              = DateFormatter()
            ////            dateFormatter.dateFormat       = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            //            dateFormatter.dateFormat       = "yyyy-MM-ddTHH:mm:ssZ"
            //
            //            let date = dateFormatter.date(from: dateAsString)
            //            print(date as Any)
            //            dateFormatter.dateFormat       = "MMM dd yyyy h:mm a"
            //            let DateFormatted = dateFormatter.string(from: date!)
            //            print(DateFormatted)
            //            return DateFormatted
        }else
        {
            return ""
        }
    }
    
    
    
    func convertDateToDisplayDatewithoutTime(dateFromResponse:String)->String
    {
        if dateFromResponse != ""
        {
            
            let formatter         = DateFormatter()
            //                formatter.dateStyle   = .long
            //            formatter.dateFormat  = "yyyy-MM-dd HH:mm:ss +0000"
            //            formatter.dateFormat = "dd-MM-yyyy hh:mm a"
            //            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            
            
            //                    formatter.timeZone = .current
            //                formatter.timeZone = TimeZone(identifier: "UTC")
            
            
            let result            = formatter.date(from: "\(dateFromResponse)")
            
            let formatter2        = DateFormatter()
            //   formatter2.dateStyle  = .long
            formatter2.dateFormat = "MMM dd yyyy"
            //            formatter2.dateFormat = "MMM dd yyyy hh:mm a"
            //            formatter2.dateFormat = "MMM dd yyyy"
            
            
            let val               = formatter2.string(from: result!)
            print("Val :\(val)")
            return val
            //            let dateAsString               = dateFromResponse
            //            let dateFormatter              = DateFormatter()
            ////            dateFormatter.dateFormat       = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            //            dateFormatter.dateFormat       = "yyyy-MM-ddTHH:mm:ssZ"
            //
            //            let date = dateFormatter.date(from: dateAsString)
            //            print(date as Any)
            //            dateFormatter.dateFormat       = "MMM dd yyyy h:mm a"
            //            let DateFormatted = dateFormatter.string(from: date!)
            //            print(DateFormatted)
            //            return DateFormatted
        }else
        {
            return ""
        }
    }
    
    
    func convertDateToDisplayDatewithTime(dateFromResponse:String)->String
    {
        if dateFromResponse != ""
        {
            
            let formatter         = DateFormatter()
            //                formatter.dateStyle   = .long
            //            formatter.dateFormat  = "yyyy-MM-dd HH:mm:ss +0000"
            //            formatter.dateFormat = "dd-MM-yyyy hh:mm a"
            //            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            
            
            //                                    formatter.timeZone = .current
            //                formatter.timeZone = TimeZone(identifier: "UTC")
            
            
            let result            = formatter.date(from: "\(dateFromResponse)")
            
            let formatter2        = DateFormatter()
            //                formatter2.dateStyle  = .long
            formatter2.dateFormat = "MMM dd hh:mm a"
            //            formatter2.dateFormat = "MMM dd yyyy hh:mm a"
            //            formatter2.dateFormat = "MMM dd yyyy"
            
            
            let val               = formatter2.string(from: result!)
            print("Val :\(val)")
            return val
            //            let dateAsString               = dateFromResponse
            //            let dateFormatter              = DateFormatter()
            ////            dateFormatter.dateFormat       = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            //            dateFormatter.dateFormat       = "yyyy-MM-ddTHH:mm:ssZ"
            //
            //            let date = dateFormatter.date(from: dateAsString)
            //            print(date as Any)
            //            dateFormatter.dateFormat       = "MMM dd yyyy h:mm a"
            //            let DateFormatted = dateFormatter.string(from: date!)
            //            print(DateFormatted)
            //            return DateFormatted
        }else
        {
            return ""
        }
    }
    func convertDateToDisplayDateInAlbumOrDocuments(dateFromResponse:String)->String
    {
        if dateFromResponse != ""
        {
            
            let formatter         = DateFormatter()
            formatter.dateStyle   = .long
            //            formatter.dateFormat  = "yyyy-MM-dd HH:mm:ss +0000"
            //            formatter.dateFormat = "dd-MM-yyyy hh:mm a"
            //            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
            
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z''"
            
            //                    formatter.timeZone = .current
            formatter.timeZone = TimeZone(identifier: "UTC")
            
            
            let result            = formatter.date(from: "\(dateFromResponse)")
            
            let formatter2        = DateFormatter()
            formatter2.dateStyle  = .long
            //            formatter2.dateFormat = "MMM dd yyyy"
            formatter2.dateFormat = "MMM dd yyyy"
            
            
            let val               = formatter2.string(from: result!)
            print("Val :\(val)")
            return val
            //            let dateAsString               = dateFromResponse
            //            let dateFormatter              = DateFormatter()
            ////            dateFormatter.dateFormat       = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            //            dateFormatter.dateFormat       = "yyyy-MM-ddTHH:mm:ssZ"
            //
            //            let date = dateFormatter.date(from: dateAsString)
            //            print(date as Any)
            //            dateFormatter.dateFormat       = "MMM dd yyyy h:mm a"
            //            let DateFormatted = dateFormatter.string(from: date!)
            //            print(DateFormatted)
            //            return DateFormatted
        }else
        {
            return ""
        }
    }
    
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    //
    //        let numberOfCellInRow  = titleArr.count
    //        let padding : Int      = 5
    //        let collectionCellWidth : CGFloat = (self.collVw.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
    //        return CGSize(width: collectionCellWidth , height: 70)
    //    }
    
    
    
    //MARK: - Button Action
    
    @IBAction func addAjenda(_ sender: Any) {
        
        let story = UIStoryboard(name: "third", bundle: nil)
        let AddAjendaViewController = story.instantiateViewController(withIdentifier: "AddAjendaViewController") as? AddAjendaViewController
        AddAjendaViewController?.eventID = self.eventId
        AddAjendaViewController?.eventDetails = self.eventDetails
        AddAjendaViewController!.creationDelegate = self
        
        self.navigationController?.pushViewController(AddAjendaViewController!, animated: true)
    }
    
    
    
    @IBAction func btnAddAgendaInNodata_OnClick(_ sender: Any) {
        
        let story = UIStoryboard(name: "third", bundle: nil)
        let AddAjendaViewController = story.instantiateViewController(withIdentifier: "AddAjendaViewController") as? AddAjendaViewController
        AddAjendaViewController?.eventID = self.eventId
        AddAjendaViewController?.eventDetails = self.eventDetails
        AddAjendaViewController!.creationDelegate = self
        
        self.navigationController?.pushViewController(AddAjendaViewController!, animated: true)
    }
    
    
    @IBAction func btnGuestCountOnClick(_ sender: UIButton) {
        if amCreator
        {
            
            if sender.tag == 0
            {
                if self.lblAttending.text == "0"
                {
                    //                Helpers.showAlertDialog(message: "no one has been attended this event yet....", target: self)
                    //                sender.isUserInteractionEnabled = false
                    
                }
                else
                {
                    let story = UIStoryboard(name: "third", bundle: nil)
                    let vc = story.instantiateViewController(withIdentifier: "GuestDetailsViewController") as? GuestDetailsViewController
                    vc!.selectedIndex = sender.tag
                    vc!.eventId = self.eventId
                    self.navigationController?.pushViewController(vc!, animated: true)
                }
            }
            else if sender.tag == 1
            {
                if self.lblMayAttend.text == "0"
                {
                    
                    //                Helpers.showAlertDialog(message: "no one has been showed interest in this event !", target: self)
                }
                else
                {
                    let story = UIStoryboard(name: "third", bundle: nil)
                    let vc = story.instantiateViewController(withIdentifier: "GuestDetailsViewController") as? GuestDetailsViewController
                    vc!.selectedIndex = sender.tag
                    vc!.eventId = self.eventId
                    self.navigationController?.pushViewController(vc!, animated: true)
                }
            }
            else if sender.tag == 2
            {
                if self.lblNotAttending.text == "0"
                {
                    
                    
                    //                Helpers.showAlertDialog(message: "nothing shown", target: self)
                }
                else
                {
                    let story = UIStoryboard(name: "third", bundle: nil)
                    let vc = story.instantiateViewController(withIdentifier: "GuestDetailsViewController") as? GuestDetailsViewController
                    vc!.selectedIndex = sender.tag
                    vc!.eventId = self.eventId
                    self.navigationController?.pushViewController(vc!, animated: true)
                }
            }
            else
            {
                if self.lblInvitationSend.text == "0"
                {
                    
                    
                    //                Helpers.showAlertDialog(message: "nothing shown", target: self)
                }
                else
                {
                    let story = UIStoryboard(name: "third", bundle: nil)
                    let vc = story.instantiateViewController(withIdentifier: "GuestDetailsViewController") as? GuestDetailsViewController
                    vc!.selectedIndex = sender.tag
                    vc!.eventId = self.eventId
                    self.navigationController?.pushViewController(vc!, animated: true)
                }
            }
            
            
            //        self.lblInvitationSend.text = "0"
            //        let story = UIStoryboard(name: "third", bundle: nil)
            //        let vc = story.instantiateViewController(withIdentifier: "GuestDetailsViewController") as? GuestDetailsViewController
            //        vc!.selectedIndex = sender.tag
            //            vc!.eventId = self.eventId
            //            vc!.guestCountResponse = self.guestCountResponse
            //
            //        self.navigationController?.pushViewController(vc!, animated: true)
        }
        else
        {
            Helpers.showAlertDialog(message: "This functionality is only available for the event creator ", target: self)
        }
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tblViewOfAgenda{
            return self.arrayOfFullAgenda.count
        }else
        {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == tblViewOfAgenda{
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 40))
            headerView.backgroundColor = .clear
            print(self.arrayOfFullAgenda[section])
            let label = UILabel()
            label.frame = CGRect.init(x: 5, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
            
            let dateAsString               = self.arrayOfFullAgenda[section]["date"].stringValue
            let dateFormatter              = DateFormatter()
            dateFormatter.dateFormat       = "yyyy-MM-dd"
            
            let date = dateFormatter.date(from: dateAsString)
            print(date as Any)
            dateFormatter.dateFormat       = "EEE MMM dd yyy"
            let DateFormatted = dateFormatter.string(from: date!)
            print(DateFormatted)
            
            
            var myMutableString = NSMutableAttributedString()
            myMutableString = NSMutableAttributedString(string: DateFormatted as String, attributes: [NSAttributedString.Key.font:UIFont.boldSystemFont(ofSize: 18)])
            myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value:#colorLiteral(red: 0.2274509804, green: 0.1333333333, blue: 0.3843137255, alpha: 1) , range: NSRange(location:0,length:3))
            
            
            myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.darkGray, range: NSRange(location:3,length:DateFormatted.count-3))
            myMutableString.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 18), range: NSRange(location:3,length:DateFormatted.count-3))
            
            
            label.attributedText = myMutableString
            //        label.text = DateFormatted
            
            
            headerView.addSubview(label)
            
            return headerView
        }
        else
        {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == tblViewOfAgenda{
            return 40
        }
        else
        {
            return 0
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblViewOfAgenda{
            return self.arrayOfFullAgenda[section]["data"].count
        }
        else
        {
            return self.arrayOfSignUpList.count
        }
    }
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //
    //            if tableView == tblViewOfSignups
    //        {
    //            return 150
    //        }
    //        else  if tableView == tblViewOfAgenda
    //        {
    //            return 120
    //        }
    //        else
    //        {
    //            return 0
    //        }
    //
    //
    //
    //    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }
    
    @IBAction func btnAgendaImagetap_onclick(_ sender: subclassedUIButton) {
        //
        let currentAgenda = self.arrayOfFullAgenda[sender.indexSection!]["data"][sender.indexPath!]
        let agenda_pic = (currentAgenda["agenda_pic"].stringValue)
        if agenda_pic != ""
        {
            
            
            let temp = "\(Helpers.imageURl)"+"\(BaseUrl.agenda_pic)"+agenda_pic
            let imgUrl = URL(string: temp)
            
            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            
            let vc = storyboard.instantiateViewController(withIdentifier: "ChangePicturesViewController") as! ChangePicturesViewController
            vc.isCoverOrProfilePic = "CoverPic"
            vc.ids           = UserDefaults.standard.value(forKey: "userId") as! String
            vc.isFrom = "event"
            vc.inFor = "cover"
            vc.isAdmin = ""
            //               self.imgClicked = true
            vc.imageUrl = temp
            self.navigationController?.pushViewController(vc, animated: true)
            
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblViewOfAgenda{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AgendaAddTableViewCell", for: indexPath) as! AgendaAddTableViewCell
            let currentAgenda = self.arrayOfFullAgenda[indexPath.section]["data"][indexPath.row].dictionaryValue
            print(currentAgenda)
            cell.lblStartTime.text = "\(Helpers.convert24Hourto12HourFormat(val: currentAgenda["event_start_time"]!.stringValue)) - \(Helpers.convert24Hourto12HourFormat(val: currentAgenda["event_end_time"]!.stringValue))"
            cell.lblAgendaName.text = currentAgenda["title"]?.stringValue
            cell.lblAgendaDescription.text = currentAgenda["description"]?.stringValue
            cell.btnImageTap.tag = indexPath.row
            let agenda_pic = (currentAgenda["agenda_pic"]!.stringValue)
            
            if agenda_pic != ""
            {
                
                cell.imgOfAgenda.isHidden = false
                cell.btnImageTap.isHidden = false
                cell.btnImageTap.indexPath              = indexPath.row
                cell.btnImageTap.indexSection           = indexPath.section
                
                cell.widthOfImageAgenda.constant = 40
                cell.leadingOfAgendaTitle.constant = 20
                
                let temp = "\(Helpers.imageURl)"+"\(BaseUrl.agenda_pic)"+agenda_pic
                let imgUrl = URL(string: temp)
                cell.imgOfAgenda.kf.indicatorType  = .activity
                
                //            cell.imgOfAgenda.kf.setImage(with: imgUrl, placeholder: nil, options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
                cell.imgOfAgenda.kf.setImage(with: imgUrl, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
            }
            else
            {
                cell.widthOfImageAgenda.constant = 0
                cell.leadingOfAgendaTitle.constant = 0
                cell.imgOfAgenda.isHidden = true
                cell.btnImageTap.isHidden = true
                
                cell.imgOfAgenda.image = #imageLiteral(resourceName: "Family Logo")
                
            }
            //        if indexPath.row == 1
            //        {
            //            cell.widthOfImageAgenda.constant = 0
            //            cell.leadingOfAgendaTitle.constant = 0
            //            cell.imgOfAgenda.isHidden = true
            //        }
            //        else
            //        {
            //            cell.imgOfAgenda.isHidden = false
            //            cell.widthOfImageAgenda.constant = 40
            //            cell.leadingOfAgendaTitle.constant = 20
            //
            //
            //            let url = URL(string: "https://familheey.s3.amazonaws.com/logo/logo-1569301404268.png")
            //            cell.imgOfAgenda.kf.setImage(with: url)
            //        }
            
            if self.amCreator
            {
                if !pastEvents{
                    cell.btnEdit.isHidden = false
                    cell.btnDelete.isHidden = false
                }else
                {
                    cell.btnEdit.isHidden = true
                    cell.btnDelete.isHidden = true
                }
            }
            else
            {
                cell.btnEdit.isHidden = true
                cell.btnDelete.isHidden = true
            }
            cell.btnEdit.indexPath              = indexPath.row
            cell.btnEdit.indexSection           = indexPath.section
            
            cell.btnDelete.indexPath              = indexPath.row
            cell.btnDelete.indexSection           = indexPath.section
            
            cell.btnEdit.addTarget(self, action: #selector(btnEditAgenda_OnClik(sender:)), for: .touchUpInside)
            
            cell.btnDelete.addTarget(self, action: #selector(btnDeleteAgenda_OnClik(sender:)), for: .touchUpInside)
            
            
            return  cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SignUpListTableViewCell", for: indexPath) as! SignUpListTableViewCell
            let currentSignUp = self.arrayOfSignUpList[indexPath.row].dictionaryValue
            print(currentSignUp)
            
            
            //        {
            //            "is_active" : true,
            //            "item_quantity" : 50,
            //            "user_id" : 989,
            //            "event_id" : 83,
            //            "updatedAt" : "2019-10-24T06:27:11.981Z",
            //            "createdAt" : "2019-10-24T05:10:25.254Z",
            //            "id" : 1,
            //            "slot_title" : "test",
            //            "start_date" : "2019-10-25T05:03:09.528Z",
            //            "end_date" : "2019-10-26T05:03:09.528Z",
            //            "slot_description" : "test event signup items description"
            //        },
            cell.viewOfBorder.borderColor = #colorLiteral(red: 0.937254902, green: 0.9411764706, blue: 0.9450980392, alpha: 1)
            
            cell.lblSlotName.text = currentSignUp["slot_title"]?.stringValue
            cell.lblDescription.text = currentSignUp["slot_description"]?.stringValue
            
            //below code only display date
            //        cell.lblDate.text = "\(convertDateToDisplayDate(dateFromResponse:currentSignUp["start_date"]!.stringValue)) - \(convertDateToDisplayDate(dateFromResponse:currentSignUp["end_date"]!.stringValue))"
            
            
            if currentSignUp["start_date"]!.stringValue.contains("00:00:00") {
                cell.lblDate.text = "\(convertDateToDisplayDatewithoutTime(dateFromResponse:currentSignUp["start_date"]!.stringValue)) - \(convertDateToDisplayDatewithoutTime(dateFromResponse:currentSignUp["end_date"]!.stringValue))"
            }
            else
            {
                cell.lblDate.text = "\(convertDateToDisplayDatewithTime(dateFromResponse:currentSignUp["start_date"]!.stringValue)) - \(convertDateToDisplayDatewithTime(dateFromResponse:currentSignUp["end_date"]!.stringValue))"
                
            }
            
            
            
            if currentSignUp["item_quantity"]!.stringValue == currentSignUp["quantity_collected"]!.stringValue
            {
                cell.imgViewOfGreenTick.isHidden = false
                cell.lblSlotsFill.isHidden = false
                cell.lblSlotAvailabilty.isHidden = true
                cell.lblCount.isHidden = true
                cell.lblSlotsFill.text = "All Filled!"
                cell.btnSignUp.isHidden = true
                
            }
            else
            {
                cell.imgViewOfGreenTick.isHidden = true
                cell.lblSlotsFill.isHidden = true
                cell.lblSlotAvailabilty.isHidden = false
                cell.lblCount.isHidden = false
                cell.btnSignUp.isHidden = false
                
                var myMutableStringfirst = NSMutableAttributedString()
                myMutableStringfirst = NSMutableAttributedString(string: currentSignUp["needed"]!.stringValue, attributes: [NSAttributedString.Key.font:UIFont.boldSystemFont(ofSize: 15)])
                var myMutableStringSecond = NSMutableAttributedString()
                myMutableStringSecond = NSMutableAttributedString(string:" of \( currentSignUp["item_quantity"]!.stringValue)", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 13)])
                
                myMutableStringfirst.append(myMutableStringSecond)
                
                //        myMutableStringfirst.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.darkGray, range: nil)
                cell.lblCount.attributedText = myMutableStringfirst
                cell.lblSlotAvailabilty.text = "Available"
            }
            
            if self.amCreator
            {
                if !pastEvents
                {
                    cell.btnEdit.isHidden = false
                    cell.imgEdit.isHidden = false
                }
                else
                {
                    cell.btnEdit.isHidden = true
                    cell.imgEdit.isHidden = true
                }
            }
            else
            {
                cell.btnEdit.isHidden = true
                cell.imgEdit.isHidden = true
                
                
            }
            cell.btnEdit.tag = indexPath.row
            cell.btnEdit.addTarget(self, action: #selector(btnEditSignUp_OnClik(sender:)), for: .touchUpInside)
            cell.btnViewSignUps.tag = indexPath.row
            cell.btnSignUp.tag = indexPath.row
            
            
            cell.btnViewSignUps.addTarget(self, action: #selector(btnViewSignUp_OnClik(sender:)), for: .touchUpInside)
            cell.btnSignUp.addTarget(self, action: #selector(btnSignUp_OnClik(sender:)), for: .touchUpInside)
            
            
            return  cell
            
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // let cell = tableView.cellForRow(at: indexPath) as! AgendaAddTableViewCell
        //        cell.viewOfCircle.backgroundColor = #colorLiteral(red: 0.2274509804, green: 0.1333333333, blue: 0.3843137255, alpha: 1)
        //        cell.viewOfLine.backgroundColor = #colorLiteral(red: 0.2274509804, green: 0.1333333333, blue: 0.3843137255, alpha: 1)
        //        cell.viewOfBorder.borderColor = #colorLiteral(red: 0.2274509804, green: 0.1333333333, blue: 0.3843137255, alpha: 1)
    }
    
    //MARK:- Edit Action
    
    @objc func btnEditAgenda_OnClik(sender:subclassedUIButton)
    {
        let currentAgenda = self.arrayOfFullAgenda[sender.indexSection!]["data"][sender.indexPath!]
        
        // print("current Agenda: \(currentAgenda)")
        
        let AddAjendaViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddAjendaViewController") as! AddAjendaViewController
        AddAjendaViewController.ajendaDetails = currentAgenda
        AddAjendaViewController.creationDelegate = self
        
        self.navigationController?.pushViewController(AddAjendaViewController, animated: true)
    }
    
    @objc func btnDeleteAgenda_OnClik(sender:subclassedUIButton)
    {
        
        let alert = UIAlertController(title: "Familheey", message: "Do you want to delete this agenda", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { _ in
            
            let currentAgenda = self.arrayOfFullAgenda[sender.indexSection!]["data"][sender.indexPath!]
            
            self.deleteAjendaID   = currentAgenda["id"].intValue
            self.callDeleteAgendaApi()
        }))
        alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
        
        
        
        
    }
    
    //MARK: - Edit sign up and delete signup functions
    
    
    
    @IBAction func btnAddSignUpOnClick(_ sender: Any) {
        let story = UIStoryboard(name: "third", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "AddSignUpViewController") as? AddSignUpViewController
        vc?.isUpdate = false
        vc?.eventId = self.eventId
        vc?.creationDelegate = self
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    
    @objc func btnEditSignUp_OnClik(sender:UIButton)
    {
        let currentSignUp = self.arrayOfSignUpList[sender.tag]
        
        // print("current Agenda: \(currentAgenda)")
        
        let AddSignUpViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddSignUpViewController") as! AddSignUpViewController
        print(currentSignUp)
        AddSignUpViewController.slotDetails = currentSignUp
        AddSignUpViewController.isUpdate = true
        AddSignUpViewController.eventId = self.eventId
        AddSignUpViewController.creationDelegate = self
        
        self.navigationController?.pushViewController(AddSignUpViewController, animated: true)
    }
    
    
    @objc func btnViewSignUp_OnClik(sender:UIButton)
    {
        let currentSignUp = self.arrayOfSignUpList[sender.tag]
        
        // print("current Agenda: \(currentAgenda)")
        
        let ViewSignUpViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewSignUpViewController") as! ViewSignUpViewController
        //        AddSignUpViewController.slotDetails = currentSignUp
        //        AddSignUpViewController.isUpdate = true
        //        AddSignUpViewController.eventId = self.eventId
        //        AddSignUpViewController.creationDelegate = self
        ViewSignUpViewController.CurrentSlotDetails = currentSignUp
        self.navigationController?.pushViewController(ViewSignUpViewController, animated: true)
    }
    
    
    @objc func btnSignUp_OnClik(sender:UIButton)
    {
        let currentSignUp = self.arrayOfSignUpList[sender.tag]
        
        // print("current Agenda: \(currentAgenda)")
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContributionAddViewController") as! ContributionAddViewController
        vc.CurrentSlotDetails = currentSignUp
        vc.isUpdate = false
        vc.eventId = self.eventId
        vc.contributionDelegate = self
        //        vc.modalPresentationStyle = .popover
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: true, completion: nil)
        
    }
    
    func setupLongPressGesture() {
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress))
        longPressGesture.minimumPressDuration = 1.0 // 1 second press
        longPressGesture.delegate = self
        self.tblViewOfSignups.addGestureRecognizer(longPressGesture)
    }
    
    @objc func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer){
        if gestureRecognizer.state == .ended {
            let touchPoint = gestureRecognizer.location(in: self.tblViewOfSignups)
            
            if let indexPath = tblViewOfSignups.indexPathForRow(at: touchPoint) {
                let cell = self.tblViewOfSignups.cellForRow(at: indexPath) as! SignUpListTableViewCell
                cell.viewOfBorder.borderColor = #colorLiteral(red: 0.2274509804, green: 0.1333333333, blue: 0.3843137255, alpha: 1)
                let alert = UIAlertController(title: "Familheey", message: "Do you want to delete this Signup", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { _ in
                    
                    let currentSignUp = self.arrayOfSignUpList[indexPath.row]
                    
                    self.deleteSignupID   = currentSignUp["id"].intValue
                    self.callDeleteSignUpSlotApi()
                }))
                alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: { _ in
                    cell.viewOfBorder.borderColor = #colorLiteral(red: 0.937254902, green: 0.9411764706, blue: 0.9450980392, alpha: 1)
                    
                }))
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    @objc func btnDeleteSignUp_OnClik(sender:UIButton)
    {
        
        let alert = UIAlertController(title: "Familheey", message: "Do you want to delete this Signup", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { _ in
            
            let currentSignUp = self.arrayOfSignUpList[sender.tag]
            
            self.deleteSignupID   = currentSignUp["id"].intValue
            self.callDeleteSignUpSlotApi()
        }))
        alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
        
        
        
        
    }
    
    func callDeleteSignUpSlotApi(){
        
        
        
        ActivityIndicatorView.show("Loading....")
        let parameter = ["id" : "\(deleteSignupID)"] as [String : Any]
        
        print("parameters are : \(parameter)")
        
        networkProvider.request(.deleteSignUps(parameter: parameter), completion: {(result) in
            
            print(result)
            
            switch result {
                
            case .success(let response):
                do {
                    
                    if response.statusCode == 200{
                        
                        ActivityIndicatorView.hiding()
                        
                        let jsonData = JSON(response.data)
                        
                        print("Json data : \(jsonData)")
                        
                        
                        let alert = UIAlertController(title: "Success", message: "You have successfully delete SignUp", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                            self.apiForSignUpListing()
                        }))
                        
                        self.present(alert, animated: true, completion: nil)
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.callDeleteSignUpSlotApi()
                        }
                    }
                    else{
                        ActivityIndicatorView.hiding()
                    }
                    
                } catch let err {
                    ActivityIndicatorView.hiding()
                    print(err)
                }
                
            case .failure(let error):
                ActivityIndicatorView.hiding()
                break
            }
        })
    }
    func apiForAgendaListing() {
        
        //        let parameter = ["event_id":"48"] as [String : Any]
        let parameter = ["event_id":self.eventId!] as [String : Any]
        
        print(parameter)
        
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.AgendaListing(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    if response.statusCode == 200
                    {
                        self.arrayOfFullAgenda = jsonData["data"].arrayValue
                        print(self.arrayOfFullAgenda.count)
                        
                        
                        if self.arrayOfFullAgenda.count != 0
                        {
                            self.viewOfNoAgenda.isHidden = true
                            self.imgViewNoAgenda.isHidden = true
                            self.lblNoDataHeading1.isHidden = true
                            self.lblNoDataHeading2.isHidden = true
                            
                            self.btnAgendaAdd.isHidden = true
                            
                            self.tblViewOfAgenda.isHidden = false
                            if self.amCreator
                            {
                                if !self.pastEvents{
                                    self.btnAddAgendaInListing.isHidden = false
                                }
                                else
                                {
                                    self.btnAddAgendaInListing.isHidden = true
                                    
                                    
                                }
                                
                            }
                            else
                            {
                                self.btnAddAgendaInListing.isHidden = true
                                
                            }
                            self.tblViewOfAgenda.reloadData()
                            
                        }
                        else{
                            self.viewOfNoAgenda.isHidden = false
                            self.imgViewNoAgenda.isHidden = false
                            self.lblNoDataHeading1.isHidden = false
                            self.lblNoDataHeading2.isHidden = false
                            if self.amCreator
                            {
                                if !self.pastEvents{
                                    
                                    self.btnAgendaAdd.isHidden = false
                                    self.lblNoDataHeading1.text = "No agenda yet? let's add a few..."
                                    self.lblNoDataHeading2.text = "Simply set your 1st agenda!"
                                }
                                else
                                {
                                    self.btnAgendaAdd.isHidden = true
                                    
                                    self.lblNoDataHeading1.text = "No agenda added yet. "
                                    self.lblNoDataHeading2.text = "Please check back later."
                                }
                                
                            }
                            else{
                                self.btnAgendaAdd.isHidden = true
                                
                                self.lblNoDataHeading1.text = "No agenda added yet. "
                                self.lblNoDataHeading2.text = "Please check back later."
                                
                                //                                self.lblNoDataHeading2.text = ""
                                
                                
                            }
                            self.tblViewOfAgenda.isHidden = true
                            self.btnAddAgendaInListing.isHidden = true
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.apiForAgendaListing()
                        }
                    }
                    else
                    {
                        
                        print(jsonData["code"])
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                } catch let err {
                    
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
    
    //MARK: - DeleteAgenda
    
    func callDeleteAgendaApi(){
        
        
        
        ActivityIndicatorView.show("Loading....")
        let parameter = ["agenda_id" : "\(deleteAjendaID)", "is_active":false] as [String : Any]
        
        print("parameters are : \(parameter)")
        
        networkProvider.request(.update_agenda(parameter: parameter), completion: {(result) in
            
            print(result)
            
            switch result {
                
            case .success(let response):
                do {
                    
                    if response.statusCode == 200{
                        
                        ActivityIndicatorView.hiding()
                        
                        let jsonData = JSON(response.data)
                        
                        print("Json data : \(jsonData)")
                        
                        
                        let alert = UIAlertController(title: "Success", message: "You have successfully delete agenda", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                            self.apiForAgendaListing()
                        }))
                        
                        self.present(alert, animated: true, completion: nil)
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.callDeleteAgendaApi()
                        }
                    }
                    else{
                        ActivityIndicatorView.hiding()
                    }
                    
                } catch let err {
                    ActivityIndicatorView.hiding()
                    print(err)
                }
                
            case .failure(let error):
                ActivityIndicatorView.hiding()
                break
            }
        })
    }
    //MARK:- GUEST count section
    
    func apiForGetGuestCount() {
        
        //        let parameter = ["event_id":"48"] as [String : Any]
        let parameter = ["event_id":self.eventId!] as [String : Any]
        
        print(parameter)
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.guest_count(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    print(response.statusCode)
                    if response.statusCode == 200
                    {
                        self.lblAttending.text = "\(jsonData["data"]["allCount"])"
                        self.lblMayAttend.text = "\(jsonData["data"]["interested"])"
                        self.lblNotAttending.text = "\(jsonData["data"]["notGoing"])"
                        self.lblInvitationSend.text = "\(jsonData["data"]["invitationsSend"])"
                        if self.lblAttending.text == "0"
                        {
                            self.btnAttending.isUserInteractionEnabled = false
                        }
                        else
                        {
                            self.btnAttending.isUserInteractionEnabled = true
                            
                        }
                        if self.lblMayAttend.text == "0"
                        {
                            self.btnMayAttend.isUserInteractionEnabled = false
                        }
                        else
                        {
                            self.btnMayAttend.isUserInteractionEnabled = true
                            
                        }
                        if self.lblNotAttending.text == "0"
                        {
                            self.btnNotInteresting.isUserInteractionEnabled = false
                        }
                        else
                        {
                            self.btnNotInteresting.isUserInteractionEnabled = true
                            
                        }
                        if self.lblInvitationSend.text == "0"
                        {
                            self.btnInvitationSend.isUserInteractionEnabled = false
                        }
                        else
                        {
                            self.btnInvitationSend.isUserInteractionEnabled = true
                            
                        }
                        
                        
                        self.guestCountResponse = jsonData
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.apiForGetGuestCount()
                        }
                    }
                    else
                    {
                        self.lblAttending.text = "0"
                        self.lblMayAttend.text = "0"
                        self.lblNotAttending.text = "0"
                        self.lblInvitationSend.text = "0"
                        self.btnAttending.isUserInteractionEnabled = false
                        self.btnMayAttend.isUserInteractionEnabled = false
                        self.btnNotInteresting.isUserInteractionEnabled = false
                        self.btnInvitationSend.isUserInteractionEnabled = false
                        
                        print(jsonData["code"])
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                    
                    
                    
                } catch let err {
                    
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
    //MARK: - Api for signup listing
    
    func apiForSignUpListing() {
        
        //        let parameter = ["event_id":"48"] as [String : Any]
        let parameter = ["event_id":self.eventId!] as [String : Any]
        
        print(parameter)
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.ListSignUps(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    print(response.statusCode)
                    if response.statusCode == 200
                    {
                        self.arrayOfSignUpList = jsonData["data"].arrayValue
                        if self.arrayOfSignUpList.count != 0
                        {
                            self.viewOfNoSignups.isHidden = true
                            self.imgViewOfNoSignups.isHidden = true
                            self.lblNoSignUpHeading1.isHidden = true
                            self.lblNoSignUpHeading2.isHidden = true
                            self.btnAddSignUp.isHidden = true
                            self.btnAddSignupInList.isHidden = false
                            self.viewOfSignUpData.isHidden = false
                            self.tblViewOfSignups.isHidden = false
                            if self.amCreator
                            {
                                if !self.pastEvents{
                                    self.btnAddSignupInList.isHidden = false
                                }
                                else
                                {
                                    self.btnAddSignupInList.isHidden = true
                                    
                                }
                                
                            }
                            else
                            {
                                self.btnAddSignupInList.isHidden = true
                                
                            }
                            self.tblViewOfSignups.reloadData()
                        }
                        else{
                            self.viewOfNoSignups.isHidden = false
                            self.imgViewOfNoSignups.isHidden = false
                            self.lblNoSignUpHeading1.isHidden = false
                            self.lblNoSignUpHeading2.isHidden = false
                            if self.amCreator
                            {
                                if !self.pastEvents{
                                    
                                    self.btnAddSignUp.isHidden = false
                                    self.lblNoSignUpHeading1.text = "No Signups yet? let's add a few..."
                                    self.lblNoSignUpHeading2.text = "Simply set your 1st SignUps!"
                                }
                                else
                                {
                                    self.btnAddSignUp.isHidden = true
                                    
                                    self.lblNoSignUpHeading1.text = "No SignUps added yet. "
                                    self.lblNoSignUpHeading2.text = "Please check back later."
                                }
                            }
                            else{
                                self.btnAddSignUp.isHidden = true
                                
                                self.lblNoSignUpHeading1.text = "No SignUps added yet. "
                                self.lblNoSignUpHeading2.text = "Please check back later."
                                
                                
                            }
                            self.tblViewOfSignups.isHidden = true
                            self.viewOfSignUpData.isHidden = true
                            
                            self.btnAddSignupInList.isHidden = true
                        }
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.apiForSignUpListing()
                        }
                    }
                    else
                    {
                        //                        self.lblAttending.text = "0"
                        //                        self.lblMayAttend.text = "0"
                        //                        self.lblNotAttending.text = "0"
                        //                        self.lblInvitationSend.text = "0"
                        //                        print(jsonData["code"])
                        Helpers.showAlertDialog(message: jsonData["message"].string ?? "", target: self)
                    }
                    
                    
                    
                } catch let err {
                    
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
    //MARK:-Custom Methods
    func setupLongPressGestureAlbum() {
        //        if selectedIndex == 3{
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPressDoc))
        longPressGesture.minimumPressDuration = 1.0 // 1 second press
        longPressGesture.delegate = self
        self.collViewOfDocuments.addGestureRecognizer(longPressGesture)
        //        }
        //        else{
        let longPressGesture1:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPressAlbum))
        longPressGesture1.minimumPressDuration = 1.0 // 1 second press
        longPressGesture1.delegate = self
        self.collViewOfAlbum.addGestureRecognizer(longPressGesture1)
        //        }
        
    }
    
    @objc func handleLongPressAlbum(_ gestureRecognizer: UILongPressGestureRecognizer){
        if gestureRecognizer.state == .ended {
            let touchPoint = gestureRecognizer.location(in: self.collViewOfAlbum)
            let touchP = gestureRecognizer.location(in: self.collViewOfDocuments)
            
            if let indexPath = self.collViewOfAlbum.indexPathForItem(at: touchPoint){
                let cell = collViewOfAlbum.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
                //  let cell = self.collVw.cellForItem(at: indexPath)
                print(indexPath.row)
                // arrSelectedIndex.removeAll()
                self.collViewOfAlbum.allowsMultipleSelection = true
                self.isFromMultiSelect = true
                self.deleteView.isHidden = false
                arrSelectedIndex.append(indexPath.row)
                let data = self.arrayOfAlbumList[indexPath.row]
                arrrofUsers.append(data["id"].stringValue)
                // cell?.borderColor = UIColor(white: 0, alpha: 0.25)
                //                cell.contentView.backgroundColor = UIColor.green
                
                var indexPaths = [IndexPath]()
                indexPaths.append(indexPath)
                
                if let collectionView = collViewOfAlbum {
                    collectionView.reloadItems(at: indexPaths)
                }
            }
        }
    }
    @objc func handleLongPressDoc(_ gestureRecognizer: UILongPressGestureRecognizer){
        if gestureRecognizer.state == .ended {
            let touchPoint = gestureRecognizer.location(in: self.collViewOfDocuments)
            
            if let indexPath = self.collViewOfDocuments.indexPathForItem(at: touchPoint){
                let cell = collViewOfDocuments.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
                //  let cell = self.collVw.cellForItem(at: indexPath)
                print(indexPath.row)
                // arrSelectedIndex.removeAll()
                self.collViewOfDocuments.allowsMultipleSelection = true
                self.isFromMultiSelect = true
                self.deleteDocView.isHidden = false
                arrSelectedIndex.append(indexPath.row)
                let data = self.arrayOfDocumentsList[indexPath.row]
                arrrofUsers.append(data["id"].stringValue)
                // cell?.borderColor = UIColor(white: 0, alpha: 0.25)
                //                cell.contentView.backgroundColor = UIColor.green
                
                var indexPaths = [IndexPath]()
                indexPaths.append(indexPath)
                
                if let collectionView = collViewOfDocuments {
                    collectionView.reloadItems(at: indexPaths)
                }
            }
        }
    }
    
    //MARK:- Delete Album
    func deleteAlbums(fId:String){
        var parameter =  [String : Any]()
        parameter = ["id":fId, "is_active":false]
        ActivityIndicatorView.show("Loading....")
        
        networkProvider.request(.updateFolder(paraeter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    
                    if response.statusCode == 200{
                        //self.getAllAlbums()
                        self.deleteView.isHidden = true
                        self.isFromMultiSelect = true
                        
                        self.ApiForAllAlbumsListing()
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.deleteAlbums(fId: fId)
                        }
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    @IBAction func onClickSearchReset(_ sender: Any) {
        
        if eventDetails["event_type"].stringValue.lowercased() == "regular"{
            if selectedIndex == 2{
                txtSearchAlbums.text = ""
                btnSearchRestAlbum.isHidden = true
                self.ApiForAllAlbumsListing()
                
                //                self.txtSearchAlbums.endEditing(true)
            }
            else{
                txtSearchtDoc.text = ""
                btnSearchResetDoc.isHidden = true
                self.ApiForAllDocumentsListing()
            }
        }
        else{
            if selectedIndex == 3{
                txtSearchAlbums.text = ""
                btnSearchRestAlbum.isHidden = true
                self.ApiForAllAlbumsListing()
            }
            else{
                txtSearchtDoc.text = ""
                btnSearchResetDoc.isHidden = true
                self.ApiForAllDocumentsListing()
            }
        }
        /* if selectedIndex == 2{
         if eventDetails["event_type"] == "Regular"{
         txtSearchAlbums.text = ""
         self.ApiForAllAlbumsListing()
         btnSearchRestAlbum.isHidden = true
         self.txtSearchAlbums.endEditing(true)
         }
         }
         else if selectedIndex == 3{
         if eventDetails["event_type"] == "Regular"{
         txtSearchAlbums.text = ""
         self.ApiForAllAlbumsListing()
         btnSearchRestAlbum.isHidden = true
         self.txtSearchAlbums.endEditing(true)
         }
         else{
         txtSearchtDoc.text = ""
         self.ApiForAllDocumentsListing()
         btnSearchResetDoc.isHidden = true
         self.txtSearchtDoc.endEditing(true)
         }
         }*/
    }
    
    
    //MARK:- Web API
    func ApiForAllAlbumsListing(){
        let parameter = ["event_id":self.eventId!, "user_id":UserDefaults.standard.value(forKey: "userId") as! String, "folder_for":"events","folder_type":"albums","txt":txtSearchAlbums.text!] as [String : Any]
        
        ActivityIndicatorView.show("Loading....")
        
        networkProvider.request(.GetAllFoldersEvents(parameter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("jsonData : \(jsonData)")
                    
                    
                    if response.statusCode == 200{
                        print("jsonData : \(jsonData)")
                        
                        self.arrayOfAlbumList = jsonData["data"]
                        
                        if self.arrayOfAlbumList.count > 0{
                            self.viewOfNoAlbum.isHidden = true
                            self.btnNewAlbum.isHidden = true
                            self.btnCreateAlbum.isHidden = true
                            
                            self.collViewOfAlbum.isHidden = false
                            if self.amCreator{
                                //                                if !self.pastEvents{
                                //
                                //                                self.btCreateAlbumList.isHidden = false
                                //                                }
                                //                                else
                                //                                {
                                //                                    self.btCreateAlbumList.isHidden = false
                                //
                                //                                }
                                self.btCreateAlbumList.isHidden = false
                                
                                
                                
                            }
                            else{
                                self.btCreateAlbumList.isHidden = true
                            }
                            self.viewOfAlbum.isHidden = false
                            self.collViewOfAlbum.reloadData()
                            
                            if self.amCreator{
                                if !self.pastEvents{
                                    self.setupLongPressGestureAlbum()
                                }
                            }
                        }
                        else{
                            self.viewOfNoAlbum.isHidden = false
                            if self.amCreator
                            {
                                if !self.pastEvents{
                                    self.btnCreateAlbum.isHidden = false
                                    self.btnNewAlbum.isHidden = false
                                    self.lblNoAlbumHeading1.text = "Haven't created any albums, yet"
                                    self.lblNoAlbumHeading2.text = "Lets add a new one!"
                                }
                                else
                                {
                                    self.btnCreateAlbum.isHidden = true
                                    self.btnNewAlbum.isHidden = true
                                    self.lblNoAlbumHeading1.text = "No albums added yet."
                                    self.lblNoAlbumHeading2.text = " Please check back later."
                                }
                                
                            }
                            else
                            {
                                self.btnCreateAlbum.isHidden = true
                                self.btnNewAlbum.isHidden = true
                                
                                self.lblNoAlbumHeading1.text = "No albums added yet."
                                self.lblNoAlbumHeading2.text = " Please check back later."
                                
                                
                            }
                            self.collViewOfAlbum.isHidden = true
                            
                            
                            self.btCreateAlbumList.isHidden = true
                            
                        }
                        
                        print("Data arr : \(self.arrayOfAlbumList)")
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.ApiForAllAlbumsListing()
                        }
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    
    func ApiForAllDocumentsListing(){
        let parameter = ["event_id":self.eventId!, "user_id":UserDefaults.standard.value(forKey: "userId") as! String, "folder_for":"events","folder_type":"documents","txt":txtSearchtDoc.text!] as [String : Any]
        
        
        ActivityIndicatorView.show("Loading....")
        
        networkProvider.request(.GetAllFoldersEvents(parameter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    
                    print("jsonData : \(jsonData)")
                    
                    if response.statusCode == 200{
                        print("jsonData : \(jsonData)")
                        
                        self.arrayOfDocumentsList = jsonData["data"]
                        
                        if self.arrayOfDocumentsList.count > 0{
                            self.viewOfNoDocuments.isHidden = true
                            self.lblNoDocHeading1.isHidden = true
                            self.lblNoDocHeading2.isHidden = true
                            self.btnCreateDoc1.isHidden = true
                            self.btnCreateDoc2.isHidden = true
                            self.collViewOfDocuments.isHidden = false
                            if self.amCreator
                            {
                                //                                if !self.pastEvents{
                                //
                                //                                self.btnCreateDocInList.isHidden = false
                                //                                }
                                //                                else
                                //                                {
                                //                                    self.btnCreateDocInList.isHidden = true
                                
                                //                                }
                                
                                self.btnCreateDocInList.isHidden = false
                                
                                
                            }
                            else
                            {
                                self.btnCreateDocInList.isHidden = true
                                
                            }
                            if self.amCreator{
                                if !self.pastEvents{
                                    self.setupLongPressGestureAlbum()
                                }
                            }
                            //                            self.btnCreateDocInList.isHidden = false
                            //                            self.viewOfAlbum.isHidden = true
                            self.collViewOfDocuments.reloadData()
                            
                        }
                        else{
                            self.viewOfNoDocuments.isHidden = false
                            self.lblNoDocHeading1.isHidden = false
                            self.lblNoDocHeading2.isHidden = false
                            if self.amCreator
                            {
                                if !self.pastEvents{
                                    self.btnCreateDoc1.isHidden = false
                                    self.btnCreateDoc2.isHidden = false
                                    self.lblNoDocHeading1.text = "Haven't created any documents, yet"
                                    self.lblNoDocHeading2.text = "Lets add a new one!"
                                }
                                else
                                {
                                    self.btnCreateDoc1.isHidden = true
                                    self.btnCreateDoc2.isHidden = true
                                    
                                    self.lblNoDocHeading1.text = "No documents added yet."
                                    self.lblNoDocHeading2.text = " Please check back later."
                                }
                            }
                            else
                            {
                                self.btnCreateDoc1.isHidden = true
                                self.btnCreateDoc2.isHidden = true
                                
                                self.lblNoDocHeading1.text = "No documents added yet."
                                self.lblNoDocHeading2.text = " Please check back later."
                                
                                
                            }
                            
                            
                            self.collViewOfDocuments.isHidden = true
                            self.btnCreateDocInList.isHidden = true
                            
                        }
                        
                        print("Data arr : \(self.arrayOfAlbumList)")
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.ApiForAllDocumentsListing()
                        }
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    //Add album button action
    @IBAction func addAlbumClicked(_ sender: Any) {
        
        let CreateAlbumViewController = self.storyboard?.instantiateViewController(withIdentifier: "CreateAlbumViewController") as! CreateAlbumViewController
        CreateAlbumViewController.isFrom = "events"
        
        CreateAlbumViewController.isFromDocs = false
        CreateAlbumViewController.eventId = self.eventId
        CreateAlbumViewController.albumDelegate = self
        self.navigationController?.pushViewController(CreateAlbumViewController, animated: false)
    }
    
    @IBAction func createDocumentsClicked(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "third", bundle: nil)
        let addMember = storyboard.instantiateViewController(withIdentifier: "CreateAlbumViewController") as! CreateAlbumViewController
        addMember.isFrom =  "events"
        addMember.isFromDocs = true
        addMember.eventId = self.eventId
        addMember.albumDelegate = self
        
        //        addMember.faCate = self.familyArr![0].faCategory
        //        addMember.memberJoining = self.familyArr![0].memberApproval
        self.navigationController?.pushViewController(addMember, animated: false)
        
    }
    
    //MARK: - Add documents on
    //MARK:- Agenda delegate method
    func successFromAgendaCreation()
    {
        self.apiForAgendaListing()
    }
    func callBackforSignupListing() {
        
        self.apiForSignUpListing()
        
    }
    func callBackFromCreateAlbum(dataArr:[JSON]){
        print(dataArr)
        // self.ApiForAllAlbumsListing()
        let AlbumDetailsViewController = self.storyboard?.instantiateViewController(withIdentifier: "AlbumDetailsViewController") as! AlbumDetailsViewController
        AlbumDetailsViewController.albumDetailsDict = dataArr[0]
        AlbumDetailsViewController.isFrom = "events"
        self.navigationController?.pushViewController(AlbumDetailsViewController, animated: true)
    }
    func callBackFromCreateDocument(){
        self.ApiForAllDocumentsListing()
    }
    
    //MARK:- textfield delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        textField.resignFirstResponder()
        
        if textField.text!.count > 0{
            if eventDetails["event_type"].stringValue.lowercased() == "regular"{
                if selectedIndex == 2{
                    btnSearchRestAlbum.isHidden = false
                }
                else{
                    btnSearchResetDoc.isHidden = false
                }
            }
            else{
                if selectedIndex == 3{
                    btnSearchRestAlbum.isHidden = false
                }
                else{
                    btnSearchResetDoc.isHidden = false
                }
            }
            /* if selectedIndex == 2{
             if eventDetails["event_type"].stringValue.lowercased() == "regular"{
             btnSearchRestAlbum.isHidden = false
             }
             }
             else if selectedIndex == 3{
             if eventDetails["event_type"].stringValue.lowercased() == "regular"{
             btnSearchRestAlbum.isHidden = false
             }
             else{
             btnSearchResetDoc.isHidden = false
             }
             }*/
        }
        else{
            if eventDetails["event_type"].stringValue.lowercased() == "regular"{
                if selectedIndex == 2{
                    btnSearchRestAlbum.isHidden = true
                }
                else{
                    btnSearchResetDoc.isHidden = true
                }
            }
            else{
                if selectedIndex == 3{
                    btnSearchRestAlbum.isHidden = true
                }
                else{
                    btnSearchResetDoc.isHidden = true
                }
            }
            
            /* if selectedIndex == 2{
             if eventDetails["event_type"].stringValue.lowercased() == "regular"{
             btnSearchRestAlbum.isHidden = true
             }
             }
             else if selectedIndex == 3{
             if eventDetails["event_type"].stringValue.lowercased() == "regular"{
             btnSearchRestAlbum.isHidden = true
             }
             else{
             btnSearchResetDoc.isHidden = true
             }
             }*/
        }
        textField.endEditing(true)
        //        }
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        self.view.endEditing(true)
        //        if textField == txtSearchAlbums
        //        {
        if textField.text!.count > 0{
            if eventDetails["event_type"].stringValue.lowercased() == "regular"{
                if selectedIndex == 2{
                    btnSearchRestAlbum.isHidden = false
                }
                else{
                    btnSearchResetDoc.isHidden = false
                }
            }
            else{
                if selectedIndex == 3{
                    btnSearchRestAlbum.isHidden = false
                }
                else{
                    btnSearchResetDoc.isHidden = false
                }
            }
        }
        else{
            if eventDetails["event_type"].stringValue.lowercased() == "regular"{
                if selectedIndex == 2{
                    btnSearchRestAlbum.isHidden = true
                }
                else{
                    btnSearchResetDoc.isHidden = true
                }
            }
            else{
                if selectedIndex == 3{
                    btnSearchRestAlbum.isHidden = true
                }
                else{
                    btnSearchResetDoc.isHidden = true
                }
            }
        }
        if eventDetails["event_type"].stringValue.lowercased() == "regular"{
            if selectedIndex == 2{
                self.ApiForAllAlbumsListing()
            }
            else{
                self.ApiForAllDocumentsListing()
            }
        }
        else{
            if selectedIndex == 3{
                self.ApiForAllAlbumsListing()
            }
            else{
                self.ApiForAllDocumentsListing()
            }
        }
        /* if selectedIndex == 2{
         if eventDetails["event_type"].stringValue.lowercased() == "regular"{
         self.ApiForAllAlbumsListing()
         }
         }
         else if selectedIndex == 3{
         if eventDetails["event_type"].stringValue.lowercased() == "regular"{
         self.ApiForAllAlbumsListing()
         }
         else{
         self.ApiForAllDocumentsListing()
         }
         }*/
        //        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text!.count > 0 {
            if eventDetails["event_type"].stringValue.lowercased() == "regular"{
                if selectedIndex == 2{
                    btnSearchRestAlbum.isHidden = false
                }
                else{
                    btnSearchResetDoc.isHidden = false
                }
            }
            else{
                if selectedIndex == 3{
                    btnSearchRestAlbum.isHidden = false
                }
                else{
                    btnSearchResetDoc.isHidden = false
                }
            }
        }
        else{
            if eventDetails["event_type"].stringValue.lowercased() == "regular"{
                if selectedIndex == 2{
                    btnSearchRestAlbum.isHidden = true
                }
                else{
                    btnSearchResetDoc.isHidden = true
                }
            }
            else{
                if selectedIndex == 3{
                    btnSearchRestAlbum.isHidden = true
                }
                else{
                    btnSearchResetDoc.isHidden = true
                }
            }
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text!.count > 0 {
            //            btnSearchRestAlbum.isHidden= false
            if eventDetails["event_type"].stringValue.lowercased() == "regular"{
                if selectedIndex == 2{
                    btnSearchRestAlbum.isHidden = false
                }
                else{
                    btnSearchResetDoc.isHidden = false
                }
            }
            else{
                if selectedIndex == 3{
                    btnSearchRestAlbum.isHidden = false
                }
                else{
                    btnSearchResetDoc.isHidden = false
                }
            }
        }
        else{
            if eventDetails["event_type"].stringValue.lowercased() == "regular"{
                if selectedIndex == 2{
                    btnSearchRestAlbum.isHidden = true
                }
                else{
                    btnSearchResetDoc.isHidden = true
                }
            }
            else{
                if selectedIndex == 3{
                    btnSearchRestAlbum.isHidden = true
                }
                else{
                    btnSearchResetDoc.isHidden = true
                }
            }
        }
        return true
    }
    
}
