//
//  ShowContactsViewController.swift
//  familheey
//
//  Created by familheey on 27/11/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON
import Alamofire
protocol contactShowCallBackDelegate {
    func callbackforRefreshData()
    func callToAddContact()
}

class showContactTableViewCell : UITableViewCell{
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgEdit: UIImageView!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var txtPhone: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

class ShowContactsViewController: UIViewController {
    var delegate:contactShowCallBackDelegate!
    var pastEvents:Bool = false
   
    var amCreator = false
    var eventId = ""
    var contactArr = [JSON]()
    private var networkProvider = MoyaProvider<FamilyheeyApi>()

    @IBOutlet weak var tbListView: UITableView!
    @IBOutlet weak var btnAdd: UIButton!
    
    override func viewDidLoad() {
        self.navigationController?.navigationBar.isHidden = true
        super.viewDidLoad()
        self.view.frame = UIScreen.main.bounds
        if amCreator{
            if !pastEvents
            {
            self.btnAdd.isHidden = false
            }
            else
            {
                self.btnAdd.isHidden = true

            }
        }
        else{
            self.btnAdd.isHidden = true
        }
        
        if contactArr.count > 0{
            self.tbListView.delegate = self
            self.tbListView.dataSource = self
            self.tbListView.reloadData()
        }
        
        self.tbListView.tableFooterView = UIView.init()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if appDel.editContact{
            appDel.editContact = false
            self.dismiss(animated: false, completion: nil)
            delegate.callbackforRefreshData()
        }
    }
    
    
    //MARK:- Custom Methods
    func editContact(tag:Int){
        let temp = self.contactArr[tag]
        
        let stryboard = UIStoryboard.init(name: "fourth", bundle: nil)
        let vc = stryboard.instantiateViewController(withIdentifier: "AddContactsQueryViewController") as! AddContactsQueryViewController
        vc.eventId = self.eventId
        vc.isFromEdit = true
        appDel.editContact  = true
        vc.contact = temp
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func deleteContact(tag:Int){
        let temp = self.contactArr[tag]

        let params: Parameters = ["id":temp["id"].stringValue]
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.deleteEventContact(parameter: params), completion: {(result) in
                
        switch result {
                        
        case .success(let response):
                        
            do {
                let jsonData = JSON(response.data)
                print("Json data : \(jsonData)")
                if response.statusCode == 200{
                    
                    var temp = self.contactArr
                    temp.remove(at: tag)
                    self.contactArr = temp
                    
                    self.tbListView.reloadData()
                }
                else if response.statusCode == 401 {
                    Helpers.getAccessToken { (accessToken) in
                        setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                        self.deleteContact(tag: tag)
                    }
                }

                else{
                    ActivityIndicatorView.hiding()
                }
            } catch let err {
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: err.localizedDescription, target: self)
            }
                        
            case .failure(let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }

    //MARK:- Button Actions
    @IBAction func onClickBack(_ sender: Any) {
//        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
        delegate.callbackforRefreshData()
    }
    @IBAction func onClickAdd(_ sender: Any) {
        delegate.callToAddContact()
        self.dismiss(animated: false, completion: nil)
//        let stryboard = UIStoryboard.init(name: "fourth", bundle: nil)
//        let vc = stryboard.instantiateViewController(withIdentifier: "AddContactsQueryViewController") as! AddContactsQueryViewController
//        vc.eventId = self.eventId
//        vc.isFrom = "edit"
//
        
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickEdit(_ sender: UIButton){
        let tempArr = ["Edit", "Delete"]
        
        self.showActionSheet(titleArr: tempArr as NSArray, title: "Select Option") { (index) in
            if index == 0{
                self.editContact(tag: sender.tag)
            }
            else if index == 100{
            }
            else{
                self.deleteContact(tag: sender.tag)
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ShowContactsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.contactArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "showContactTableViewCell", for: indexPath) as! showContactTableViewCell

        let temp = self.contactArr[indexPath.row]
        
        cell.lblName.text = temp["name"].stringValue
//        cell.lblPhone.text = temp["phone"].stringValue
        cell.txtPhone.text = temp["phone"].stringValue
        cell.lblEmail.text = temp["email"].stringValue
        
        if amCreator{
            if !pastEvents
            {
            cell.imgEdit.isHidden = false
            cell.btnEdit.isEnabled = true
            }
            else
            {
                cell.imgEdit.isHidden = true
                cell.btnEdit.isEnabled = false
            }
        }
        else{
            cell.imgEdit.isHidden = true
            cell.btnEdit.isEnabled = false
        }
        cell.btnEdit.tag = indexPath.row
        cell.btnEdit.addTarget(self, action: #selector(onClickEdit(_:)) , for: .touchUpInside)
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
