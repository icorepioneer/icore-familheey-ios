//
//  AddContactsQueryViewController.swift
//  familheey
//
//  Created by familheey on 15/11/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Moya
import Alamofire
import SwiftyJSON

class AddContactTableViewCell: UITableViewCell{
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    
}

class AddContactsQueryViewController: UIViewController, popUpDelegate {
    @IBOutlet weak var txName: UITextField!
    @IBOutlet weak var txtCode: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnAdd: UIButton!
    
    var eventId = ""
    var isFromEdit = false
    var contact = JSON()
    var isFrom = ""
    
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isFromEdit{
            txName.text = self.contact["name"].stringValue
            let temp = self.contact["phone"].stringValue.components(separatedBy: "-")
            txtCode.text = temp.first
            txtPhone.text = temp.last
            txtEmail.text = self.contact["email"].stringValue
            self.btnAdd.setTitle("Save", for: .normal)
        }
        else{
            self.btnAdd.setTitle("Add", for: .normal)
            
            let currentLocale = NSLocale.current as NSLocale
            let countryCode = currentLocale.object(forKey: .countryCode) as! String//get the set country name, code of your iphone
            //                        let countryName = currentLocale.displayName(forKey: NSLocale.Key.countryCode, value: countryCode)
            
            print("country code is \(countryCode)")
            print(ViewController.getCountryCallingCode(countryRegionCode: countryCode))
            
            //                        self.countryCode = ViewController.getCountryCallingCode(countryRegionCode: countryCode)
            
            if ViewController.getCountryCallingCode(countryRegionCode: countryCode).count > 0{
                //                onCodeButt.setTitle(countryName, for: .normal)
                txtCode.text = ViewController.getCountryCallingCode(countryRegionCode: countryCode)
                //                ViewController.getCountryCallingCode(countryRegionCode: countryCode)
            }
        }
        
        print(eventId)
        
    }
    
    
    //MARK:- Protocol PopupDelegate
    func selectCountry(id: String, name: String) {
        print(id)
        txtCode.text = id
        //        onCodeButt.setTitle(name, for: .normal)
        
        //        txtFieldUnderline.isHidden = false
        
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
        //
        //            self.txtPhone.becomeFirstResponder()
        //        })
    }
    
    
    
    //MARK:- Button Actions
    
    @IBAction func onClickCountryCode(_ sender: Any) {
        self.view.endEditing(true)
        
        let popUp = popupTableViewController()
        self.addChild(popUp)
        // popUp.frmGndrFlag = fromGndr
        popUp.view.frame = UIScreen.main.bounds
        self.view.addSubview(popUp.view)
        popUp.delegate = self
        self.view.bringSubviewToFront(popUp.view)
    }
    
    @IBAction func onClickAddContact(_ sender: Any) {
        if isFromEdit{
            if txName.text!.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
                Helpers.showAlertDialog(message: "Please enter name", target: self)
                return
            }
            
            if txtPhone.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" && txtEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
                Helpers.showAlertDialog(message: "Please enter phone number or email", target: self)
                return
            }
            
            if txtPhone.text!.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                if txtCode.text!.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
                    Helpers.showAlertDialog(message: "Please enter country code", target: self)
                    return
                }
            }
            
            
            if txtEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                if !Helpers.validateEmail(enteredEmail: txtEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines)){
                    self.displayAlert(alertStr: "Please Enter a valid email", title: "")
                    return
                }
                let tempStr = txtEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines).prefix(1)
                if !String(tempStr).isAlphanumeric{
                    self.displayAlert(alertStr: "Please start email with a alphanumeric characters", title: "")
                    return
                }
            }
            
            
            let phone = "\(txtCode.text!.trimmingCharacters(in: .whitespacesAndNewlines))-\(txtPhone.text!.trimmingCharacters(in: .whitespacesAndNewlines))"
            
            let tempPh = "\(txtCode.text!.trimmingCharacters(in: .whitespacesAndNewlines))\(txtPhone.text!.trimmingCharacters(in: .whitespacesAndNewlines))"
            
            if !tempPh.isPhoneNumber{
                print("not valid")
                Helpers.showAlertDialog(message: "Please enter a valid  phone number", target: self)
                return
            }
            else{
                print(" valid")
                
            }
            
            
            let params: Parameters = ["event_id" : eventId, "name" : txName.text!.trimmingCharacters(in: .whitespacesAndNewlines), "phone" : phone , "email" : txtEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines), "id":self.contact["id"].stringValue]
            ActivityIndicatorView.show("Loading....")
            networkProvider.request(.editEventContact(parameter: params), completion: {(result) in
                
                switch result {
                    
                case .success(let response):
                    
                    do {
                        let jsonData = JSON(response.data)
                        print("Json data : \(jsonData)")
                        if response.statusCode == 200{
                           /* var array : [UIViewController] = (self.navigationController?.viewControllers)!
                            array.remove(at: array.count - 1)
                            array.remove(at: array.count - 1)
                            self.navigationController?.viewControllers = array
                            self.navigationController?.popViewController(animated: true)*/
                            self.navigationController?.popToViewController(ofClass: EventDetailsViewController.self)
                        }
                        else if response.statusCode == 401 {
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                let btn = UIButton()
                                self.onClickAddContact(btn)
                            }
                        }

                        else{
                            ActivityIndicatorView.hiding()
                        }
                    } catch let err {
                        ActivityIndicatorView.hiding()
                        Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                    }
                    
                case .failure(let error):
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                }
                ActivityIndicatorView.hiding()
            })
        }
        else{
            let phone = "\(txtCode.text!.trimmingCharacters(in: .whitespacesAndNewlines))-\(txtPhone.text!.trimmingCharacters(in: .whitespacesAndNewlines))"
            
            let tempPh = "\(txtCode.text!.trimmingCharacters(in: .whitespacesAndNewlines))\(txtPhone.text!.trimmingCharacters(in: .whitespacesAndNewlines))"
            if txName.text!.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
                Helpers.showAlertDialog(message: "Please enter name", target: self)
                return
            }
                
            else if txtPhone.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" && txtEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
                Helpers.showAlertDialog(message: "Please enter phone number or email", target: self)
                return
            }
                
            else  if txtPhone.text!.trimmingCharacters(in: .whitespacesAndNewlines) != "" &&  txtCode.text!.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
                
                Helpers.showAlertDialog(message: "Please enter country code", target: self)
                return
                
                
            }
                
                
            else   if txtEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines) != "" && !Helpers.validateEmail(enteredEmail: txtEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines)){
                
                self.displayAlert(alertStr: "Please Enter a valid email", title: "")
                return
            }
                //                   let tempStr = txtEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines).prefix(1)
            else if txtEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines) != "" && !String(txtEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines).prefix(1)).isAlphanumeric{
                self.displayAlert(alertStr: "Please start email with a alphanumeric characters", title: "")
                return
            }
                
                
                
                
            else if !tempPh.isPhoneNumber{
                print("not valid")
                Helpers.showAlertDialog(message: "Please enter a valid  phone number", target: self)
                return
            }
            else{
                
                let params: Parameters = ["event_id" : eventId, "name" : txName.text!.trimmingCharacters(in: .whitespacesAndNewlines), "phone" : phone , "email" : txtEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines)]
                
                ActivityIndicatorView.show("Loading....")
                
                networkProvider.request(.addEventContact(parameter: params), completion: {(result) in
                    
                    switch result {
                        
                    case .success(let response):
                        
                        do {
                            
                            let jsonData = JSON(response.data)
                            
                            print("Json data : \(jsonData)")
                            
                            if response.statusCode == 200{
                                if self.isFrom.lowercased() == "edit"{
                                    //                                    var array : [UIViewController] = (self.navigationController?.viewControllers)!
                                    //                                    array.remove(at: array.count - 1)
                                    //                                    array.remove(at: array.count - 1)
                                    //                                    self.navigationController?.viewControllers = array
                                    self.navigationController?.popViewController(animated: true)
                                }
                                else{
                                    self.navigationController?.popViewController(animated: true)
                                }
                            }
                            else if response.statusCode == 401 {
                                Helpers.getAccessToken { (accessToken) in
                                    setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                    let btn = UIButton()
                                    self.onClickAddContact(btn)
                                }
                            }

                            else{
                                ActivityIndicatorView.hiding()
                            }
                            
                            
                        } catch let err {
                            ActivityIndicatorView.hiding()
                            Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                        }
                        
                    case .failure(let error):
                        ActivityIndicatorView.hiding()
                        Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                    }
                    ActivityIndicatorView.hiding()
                })
            }
        }
    }
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
