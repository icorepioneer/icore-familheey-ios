//
//  CreateEventsStep2ViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 11/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import iOSDropDown
import Alamofire
//import GoogleMaps
import GooglePlaces
import Moya
import SwiftyJSON

class CreateEventsStep2ViewController: UIViewController,UITextFieldDelegate,calenderselectionDelegate {
    
    @IBOutlet weak var scrlVew_height: NSLayoutConstraint!
    @IBOutlet weak var scrlView: UIScrollView!
    @IBOutlet weak var date_top: NSLayoutConstraint!
    @IBOutlet weak var venuView: UIView!
    @IBOutlet weak var venu_height: NSLayoutConstraint!
    @IBOutlet weak var lblNavHeading: UILabel!
    @IBOutlet weak var imgOfUrlAvailability: UIImageView!
    @IBOutlet weak var lblEventPageHeading: UILabel!
    @IBOutlet weak var lblHostedHeading: UILabel!
    @IBOutlet weak var lblEndDateHeading: UILabel!
    @IBOutlet weak var lblStartDateHeading: UILabel!
    @IBOutlet weak var txtStartDate: UITextField!
    @IBOutlet weak var txtEndDate: UITextField!
    @IBOutlet weak var txtVenue: UITextField!
    @IBOutlet weak var lblVenueHeading: UILabel!
    @IBOutlet weak var txtHostedBy: UITextField!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var txtEventPage: UITextField!
    @IBOutlet weak var imgRSVPYes: UIImageView!
    @IBOutlet weak var imgRSVPNo: UIImageView!
    @IBOutlet weak var txtWebinalLink: UITextField!
    @IBOutlet weak var txtDialNumber: UITextField!
    @IBOutlet weak var txtParticipantPin: UITextField!
    @IBOutlet weak var onlineWebinarView: UIView!
    @IBOutlet weak var lblWebLink: UILabel!
    
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    
    var dataFromPrevious = [String:Any]()
    var eventDetails = JSON()
    var fromEventEdit = false
    var startDateTimestamp:Int! = 0
    var endDateTimestamp:Int! = 0
    var eventId : String! = ""
    var isFrom = ""
    var groupId = ""
    var faEventCreate = false
    var timetag:Int!
    var dateTag:Int!
    var selectedStartDate:String = ""
    var selectedEndDate:String = ""
    var selectedStartDateFormatted:String = ""
    var selectedEndDateFormatted:String = ""
    var selectedLat : Double!
    var selectedLong :Double!
    var placeDropDownName = [String]()
    var placeDropDownId = [String]()
    var theme: SambagTheme = .light
    var isRSVPYes:Bool!
    
    //    @IBOutlet weak var imgSharingYes: UIImageView!
    //    @IBOutlet weak var imgSharingNo: UIImageView!
    //    var isSharingYes:Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(eventId!)
        
        selectedLat = 0
        selectedLong = 0
        
        
        if self.fromEventEdit{
            lblNavHeading.text = "Edit Event"
            self.txtHostedBy.text = eventDetails["created_by_name"].stringValue
            self.txtVenue.text = eventDetails["location"].stringValue
            self.selectedLat = eventDetails["lat"].doubleValue
            self.selectedLong = eventDetails["long"].doubleValue
            self.txtDescription.text = eventDetails["description"].stringValue
            self.startDateTimestamp = eventDetails["from_date"].intValue
            self.endDateTimestamp = eventDetails["to_date"].intValue
            self.txtWebinalLink.text = eventDetails["meeting_link"].stringValue
            self.txtParticipantPin.text = eventDetails["meeting_pin"].stringValue
            self.txtDialNumber.text = eventDetails["meeting_dial_number"].stringValue
            
            self.txtStartDate.text = self.convertTimeStampToDate(timestamp: eventDetails["from_date"].stringValue)
            self.txtEndDate.text = self.convertTimeStampToDate(timestamp: eventDetails["to_date"].stringValue)
            self.selectedStartDateFormatted = self.convertTimestampToFormattedDate(date: self.txtStartDate.text!)
            
            self.selectedEndDateFormatted = self.convertTimestampToFormattedDate(date: self.txtEndDate.text!)
            
            self.txtEventPage.text = eventDetails["event_page"].stringValue
            
            if eventDetails["rsvp"].boolValue == true{
                imgRSVPNo.image = #imageLiteral(resourceName: "gray_circle.png")
                isRSVPYes = true
                imgRSVPYes.image = #imageLiteral(resourceName: "Green_tick.png")
            }
            else{
                imgRSVPNo.image = #imageLiteral(resourceName: "Green_tick.png")
                imgRSVPYes.image = #imageLiteral(resourceName: "gray_circle.png")
                isRSVPYes = false
            }
        }
        else{
            imgRSVPNo.image = #imageLiteral(resourceName: "gray_circle.png")
            isRSVPYes = true
            imgRSVPYes.image = #imageLiteral(resourceName: "Green_tick.png")
            
            self.txtHostedBy.text = (UserDefaults.standard.value(forKey: "user_fullname") as! String)
            
        }
        
        let type = dataFromPrevious["event_type"] as! String
        if type.lowercased() == "online"{
            venu_height.constant = 0
            date_top.constant = 0
            venuView.isHidden = true
            
            //            scrlVew_height.constant = self.view.frame.size.height + 200
            scrlView.contentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height + 200)
        }
        else{
            venu_height.constant = 75.0
            date_top.constant = 20
            venuView.isHidden = false
            onlineWebinarView.removeFromSuperview()
            //            scrlVew_height.constant = self.view.frame.size.height
            scrlView.contentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height)
        }
        
        Helpers.setleftView(textfield: txtStartDate, customWidth: 15)
        Helpers.setleftView(textfield: txtEndDate, customWidth: 15)
        Helpers.setleftView(textfield: txtVenue, customWidth: 15)
        Helpers.setleftView(textfield: txtHostedBy, customWidth: 15)
        Helpers.setleftView(textfield:  txtEventPage, customWidth: 15)
        Helpers.setleftView(textfield:  txtWebinalLink, customWidth: 15)
        Helpers.setleftView(textfield:  txtDialNumber, customWidth: 15)
        Helpers.setleftView(textfield:  txtParticipantPin, customWidth: 15)
        
        self.txtVenue.autocapitalizationType      = .sentences
        self.txtHostedBy.autocapitalizationType      = .sentences
        
        txtStartDate.delegate = self
        txtEndDate.delegate = self
        txtVenue.delegate = self
        txtHostedBy.delegate = self
        txtEventPage.delegate = self
        
        
        self.setTextFiledPlaceholder()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
//        if !UIDevice.current.systemVersion.contains("13"){
//
//            txtVenue.text = appDel.selectedEventVenue
//        }
        guard #available(iOS 13.0, *) else {
            txtVenue.text = appDel.selectedEventVenue
            return
        }

    }
    
    
    //MARK:- Custom methods
    func convertTimeStampToDate(timestamp:String)->String
    {
        let date = Date(timeIntervalSince1970: Double(timestamp) as! TimeInterval)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd yyyy hh:mm a"
        //        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.timeZone = NSTimeZone.local
        
        
        let localDatef = formatter.string(from: date)
        print(localDatef)
        return localDatef
        
    }
    func convertTimeStamp(timestamp:String) -> String {
        let date = Date(timeIntervalSince1970: Double(timestamp) as! TimeInterval)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-mm-yyyy hh:mma"
        //formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.timeZone = NSTimeZone.local
        
        
        let localDatef = formatter.string(from: date)
        print(localDatef)
        return localDatef
    }
    func setTextFiledPlaceholder(){
        self.lblHostedHeading.attributedText = Helpers.setAttributedPlaceHolder(placeHolderName: "Hosted By")
        self.lblStartDateHeading.attributedText = Helpers.setAttributedPlaceHolder(placeHolderName: "Start Date & Time")
        self.lblEndDateHeading.attributedText = Helpers.setAttributedPlaceHolder(placeHolderName: "End Date & Time")
        self.lblVenueHeading.attributedText = Helpers.setAttributedPlaceHolder(placeHolderName: "Venue")
        self.lblWebLink.attributedText = Helpers.setAttributedPlaceHolder(placeHolderName: "Webinar Link")
        
        //        self.lblEventPageHeading.attributedText = Helpers.setAttributedPlaceHolder(placeHolderName: "Event page URL")
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (string == " ") {
            return false
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtEventPage{
            if fromEventEdit{
                let temp = self.eventDetails["event_url"].stringValue.components(separatedBy: "/")
                let urlStr = temp.last
                if textField.text! == urlStr{
                    print("same")
                }
                else{
                    self.apiForEventURLavailability()
                }
            }
            else{
                self.apiForEventURLavailability()
            }
        }
    }
    //MARK:- Button Action
    @IBAction func btnBack_OnClick(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func showSambagDatePickerViewController(_ sender: UIButton) {
        
        if sender.tag == 0
        {
            //            self.txtEndDate.text = ""
            //            self.dateTag = 0
            //            let vc = SambagDatePickerViewController()
            //            vc.theme = theme
            //            vc.delegate = self
            //            present(vc, animated: true, completion: nil)
            
            //            self.txtEndDate.text = ""
            self.dateTag = 0
            let CalendarEventsViewController = self.storyboard?.instantiateViewController(withIdentifier: "CalendarEventsViewController") as! CalendarEventsViewController
            CalendarEventsViewController.isFromExplore = ""
            CalendarEventsViewController.isCreateEvent = true
            CalendarEventsViewController.dateTag = 0
            CalendarEventsViewController.dateCallBackDelegate = self
            //            self.present(CalendarEventsViewController, animated: false, completion: nil)
            self.navigationController?.pushViewController(CalendarEventsViewController, animated: true)
            
        }
        else
        {
            if self.txtStartDate.text == ""
            {
                Helpers.showAlertDialog(message: "Please select Start Date", target: self)
                
            }
            else
            {
                //                self.dateTag = 1
                //                let vc = SambagDatePickerViewController()
                //                vc.theme = theme
                //                vc.delegate = self
                //                present(vc, animated: true, completion: nil)
                
                self.dateTag = 1
                let CalendarEventsViewController = self.storyboard?.instantiateViewController(withIdentifier: "CalendarEventsViewController") as! CalendarEventsViewController
                CalendarEventsViewController.isFromExplore = ""
                CalendarEventsViewController.isCreateEvent = true
                CalendarEventsViewController.dateTag = 1
                CalendarEventsViewController.dateCallBackDelegate = self
                //                           self.present(CalendarEventsViewController, animated: false, completion: nil)
                
                self.navigationController?.pushViewController(CalendarEventsViewController, animated: true)
                
            }
        }
    }
    
    @IBAction func btnVenue_OnClicked(_ sender: Any) {
        guard #available(iOS 13.0, *) else {
            // Code for earlier iOS versions
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let SearchPlacesViewController = storyboard.instantiateViewController(withIdentifier: "SearchPlacesViewController") as! SearchPlacesViewController
            SearchPlacesViewController.selectedField = "eventCreation"
            self.present(SearchPlacesViewController, animated: true, completion: nil)
            
            return
        }
        
        let acController = GMSAutocompleteViewController()
        
        acController.delegate = self
        present(acController, animated: true, completion: nil)
        
        
        /*if UIDevice.current.systemVersion.contains("13"){
         
         let acController = GMSAutocompleteViewController()
         
         acController.delegate = self
         present(acController, animated: true, completion: nil)
         }else{
         
         let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
         let SearchPlacesViewController = storyboard.instantiateViewController(withIdentifier: "SearchPlacesViewController") as! SearchPlacesViewController
         SearchPlacesViewController.selectedField = "eventCreation"
         self.present(SearchPlacesViewController, animated: true, completion: nil)
         }*/
        
    }
    
    
    
    
    //    @IBAction func btnFree_OnClick(_ sender: Any) {
    //        if let button = sender as? UIButton {
    //            button.tintColor = .clear
    //            if button.tag == 1
    //            {
    //                imgTicketed.image = #imageLiteral(resourceName: "radio-on-button.png")
    //                imgFree.image = #imageLiteral(resourceName: "radio-off-button.png")
    //                isFree = "ticketed"
    //
    //
    //            }
    //            else
    //            {
    //                imgFree.image = #imageLiteral(resourceName: "radio-on-button.png")
    //                imgTicketed.image = #imageLiteral(resourceName: "radio-off-button.png")
    //                isFree = "free"
    //
    //
    //            }
    //        }
    //
    //    }
    @IBAction func btnRSVP_OnClick(_ sender: Any) {
        if let button = sender as? UIButton {
            button.tintColor = .clear
            if button.tag == 1
            {
                imgRSVPNo.image = #imageLiteral(resourceName: "Green_tick.png")
                imgRSVPYes.image = #imageLiteral(resourceName: "gray_circle.png")
                isRSVPYes = false
                
            }
            else
            {
                imgRSVPYes.image = #imageLiteral(resourceName: "Green_tick.png")
                imgRSVPNo.image = #imageLiteral(resourceName: "gray_circle.png")
                isRSVPYes = true
                
            }
        }
        
    }
    
    
    //MARK:- VALIDATION
    func DatavalidationForSubmit()->Bool{
        
        let type = dataFromPrevious["event_type"] as! String
        if txtStartDate.text == "" {
            
            Helpers.showAlertDialog(message: "Start Date is empty", target: self)
            return false
        }
        else if txtEndDate.text == "" {
            
            Helpers.showAlertDialog(message: "End Date is empty", target: self)
            return false
        }
        else if type.lowercased() != "online"{
            if txtVenue.text == "" {
                Helpers.showAlertDialog(message: "Venue is empty", target: self)
                return false
            }
            else{
                return true
            }
        }
            //        else if txtEventPage.text == "" {
            //
            //            Helpers.showAlertDialog(message: "Event page url is empty", target: self)
            //            return false
            //        }
        else {
            
            return true
            
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    
    
    
    @IBAction func btnSave_OnClick(_ sender: Any) {
        if dataValidation(){
            if fromEventEdit
            {
                self.apiForCreateEventUpdate()
            }
            else
            {
                self.apiForCreateEventStep()
                
            }
        }
    }
    func dataValidation()-> Bool{
        let type = dataFromPrevious["event_type"] as! String
        
        if txtHostedBy.text == "" {
            
            Helpers.showAlertDialog(message: "Host Name is empty", target: self)
            return false
        }
        else if type.lowercased() != "online" {
            if txtVenue.text == "" {
                Helpers.showAlertDialog(message: "Venue is empty", target: self)
                return false
            }
            /*added by Jeena 03-02-2021 to solve crash issue*/
            else if txtStartDate.text == "" {
                
                Helpers.showAlertDialog(message: "Start Date is empty", target: self)
                return false
                
            }
            else if txtEndDate.text == "" {
                
                Helpers.showAlertDialog(message: "End Date is empty", target: self)
                return false
                
            }
            /*-------------------end jeena---------------------**/
            else{
                return true
            }
            
        }
        else if txtStartDate.text == "" {
            
            Helpers.showAlertDialog(message: "Start Date is empty", target: self)
            return false
            
        }
        else if txtEndDate.text == "" {
            
            Helpers.showAlertDialog(message: "End Date is empty", target: self)
            return false
            
        }
            //        else if txtEventPage.text == "" {
            //
            //            Helpers.showAlertDialog(message: "Event Page URL is empty", target: self)
            //            return false
            //
            //        }
        else{
            return true
        }
        
    }
    func apiForEventURLavailability() {
        
        
        let parameter = ["text":txtEventPage.text!] as [String : Any]
        print(parameter)
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.eventURL_availabilitychecking(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    print(jsonData["status"])
                    print(response.statusCode)
                    if response.statusCode == 200
                    {
                        self.imgOfUrlAvailability.isHidden = false
                        self.imgOfUrlAvailability.image = #imageLiteral(resourceName: "Green_tick.png")
                        
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.apiForEventURLavailability()
                        }
                    }
                        
                    else
                    {
                        self.imgOfUrlAvailability.isHidden = true
                        
                        Helpers.showAlertDialog(message: jsonData["message"].stringValue, target: self)
                        self.txtEventPage.text = ""
                    }
                    
                    
                    
                } catch let err {
                    
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    func apiForCreateEventStep() {
        let type = dataFromPrevious["event_type"] as! String
        
        self.startDateTimestamp = self.convertTimetoTimestamp(date: self.selectedStartDate)
        self.endDateTimestamp = self.convertTimetoTimestamp(date: self.selectedEndDate)
        //        "ticket_type":dataFromPrevious["ticket_type"]!,
        
        var parameter = [String:Any]()
        if isFrom.lowercased() == "family"{
            if type.lowercased() == "online"{
                parameter = ["created_by":dataFromPrevious["created_by"]as! String,"event_name":dataFromPrevious["event_name"]as! String,"event_type":dataFromPrevious["event_type"]as! String,"category":dataFromPrevious["event_category"]as! String,"is_public":dataFromPrevious["is_public"]!,"is_sharable":dataFromPrevious["is_sharable"]!,"allow_others":dataFromPrevious["allow_others"]!,"event_host":self.txtHostedBy.text!,"location":self.txtVenue.text!,"lat":self.selectedLat!,"long":self.selectedLong!,"description":self.txtDescription.text!,"rsvp":self.isRSVPYes!,"event_page":"\(self.txtEventPage.text!)","from_date":startDateTimestamp!,"to_date":endDateTimestamp!, "group_id":self.groupId,"meeting_link":txtWebinalLink.text!,"meeting_dial_number":txtDialNumber.text!,"meeting_pin":txtParticipantPin.text!] as [String : Any]
            }
            else{
                parameter = ["created_by":dataFromPrevious["created_by"]as! String,"event_name":dataFromPrevious["event_name"]as! String,"event_type":dataFromPrevious["event_type"]as! String,"category":dataFromPrevious["event_category"]as! String,"is_public":dataFromPrevious["is_public"]!,"is_sharable":dataFromPrevious["is_sharable"]!,"allow_others":dataFromPrevious["allow_others"]!,"event_host":self.txtHostedBy.text!,"location":self.txtVenue.text!,"lat":self.selectedLat!,"long":self.selectedLong!,"description":self.txtDescription.text!,"rsvp":self.isRSVPYes!,"event_page":"\(self.txtEventPage.text!)","from_date":startDateTimestamp!,"to_date":endDateTimestamp!, "group_id":self.groupId,"meeting_link":"","meeting_dial_number":"","meeting_pin":""] as [String : Any]
            }
            
        }
        else{
            var isPublic = Bool()
            let isPubTem = dataFromPrevious["is_public"] as! String
            if isPubTem.lowercased() == "false"{
                isPublic = false
            }
            else{
                isPublic = true
            }
            if type.lowercased() == "online"{
                parameter = ["created_by":dataFromPrevious["created_by"]as! String,"event_name":dataFromPrevious["event_name"]as! String,"event_type":dataFromPrevious["event_type"]as! String,"category":dataFromPrevious["event_category"]as! String,"is_public":isPublic ,"is_sharable":dataFromPrevious["is_sharable"]!,"allow_others":dataFromPrevious["allow_others"]!,"event_host":self.txtHostedBy.text!,"location":self.txtVenue.text!,"lat":self.selectedLat!,"long":self.selectedLong!,"description":self.txtDescription.text!,"rsvp":self.isRSVPYes!,"event_page":"\(self.txtEventPage.text!)","from_date":startDateTimestamp!,"to_date":endDateTimestamp!,"group_id":"","meeting_link":txtWebinalLink.text!,"meeting_dial_number":txtDialNumber.text!,"meeting_pin":txtParticipantPin.text!] as [String : Any]
            }
            else{
                parameter = ["created_by":dataFromPrevious["created_by"]as! String,"event_name":dataFromPrevious["event_name"]as! String,"event_type":dataFromPrevious["event_type"]as! String,"category":dataFromPrevious["event_category"]as! String,"is_public":isPublic ,"is_sharable":dataFromPrevious["is_sharable"]!,"allow_others":dataFromPrevious["allow_others"]!,"event_host":self.txtHostedBy.text!,"location":self.txtVenue.text!,"lat":self.selectedLat!,"long":self.selectedLong!,"description":self.txtDescription.text!,"rsvp":self.isRSVPYes!,"event_page":"\(self.txtEventPage.text!)","from_date":startDateTimestamp!,"to_date":endDateTimestamp!,"group_id":"","meeting_link":"","meeting_dial_number":"","meeting_pin":""] as [String : Any]
            }
            
        }
        
        
        print(parameter)
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.createEvent(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    print(jsonData["status"])
                    print(response.statusCode)
                    if response.statusCode == 200{
                        self.eventDetails = jsonData["data"]
                        print(self.eventDetails)
                        let AlertCus = CustomAlertVC.show(in: self, title: "Success", message: "Successfully created event ,Please update event details", image:UIImage.init(named: "Green_tick")!)
                        AlertCus.addAction(CPAlertAction.init(title: "OK", type: .normal, handler: {
                            let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                            
                            let vc = storyboard.instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
                            
                            vc.eventDetails = self.eventDetails
                            vc.fromCreate = true
                            vc.enableEventEdit = true
                            vc.groupId = self.groupId
                            vc.faEventCreate = self.faEventCreate
                            vc.isFrom = self.isFrom
                            self.navigationController?.pushViewController(vc, animated: true)
                        }))
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.apiForCreateEventStep()
                        }
                    }
                        
                    else{
                        
                        print(jsonData["code"])
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                } catch let err {
                    
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    func apiForCreateEventUpdate() {
        let type = dataFromPrevious["event_type"] as! String
        var parameter = [String:Any]()
        if self.selectedStartDate != ""{
            self.startDateTimestamp = self.convertTimetoTimestamp(date: self.selectedStartDate)
        }
        if self.selectedEndDate != ""{
            self.endDateTimestamp = self.convertTimetoTimestamp(date: self.selectedEndDate)
        }
        
        if type.lowercased() == "online"{
            parameter = ["id":self.eventId!,"user_id":UserDefaults.standard.value(forKey: "userId") as! String,"created_by":dataFromPrevious["created_by"]as! String,"event_name":dataFromPrevious["event_name"]as! String,"event_type":dataFromPrevious["event_type"]as! String,"category":dataFromPrevious["event_category"]as! String,"is_public":dataFromPrevious["is_public"]!,"is_sharable":dataFromPrevious["is_sharable"]!,"allow_others":dataFromPrevious["allow_others"]!,"event_host":self.txtHostedBy.text!,"location":self.txtVenue.text!,"lat":self.selectedLat!,"long":self.selectedLong!,"description":self.txtDescription.text!,"rsvp":self.isRSVPYes!,"event_page":"\(self.txtEventPage.text!)","from_date":startDateTimestamp!,"to_date":endDateTimestamp!,"meeting_link":txtWebinalLink.text!,"meeting_dial_number":txtDialNumber.text!,"meeting_pin":txtParticipantPin.text!] as [String : Any]
        }
        else{
            parameter = ["id":self.eventId!,"user_id":UserDefaults.standard.value(forKey: "userId") as! String,"created_by":dataFromPrevious["created_by"]as! String,"event_name":dataFromPrevious["event_name"]as! String,"event_type":dataFromPrevious["event_type"]as! String,"category":dataFromPrevious["event_category"]as! String,"is_public":dataFromPrevious["is_public"]!,"is_sharable":dataFromPrevious["is_sharable"]!,"allow_others":dataFromPrevious["allow_others"]!,"event_host":self.txtHostedBy.text!,"location":self.txtVenue.text!,"lat":self.selectedLat!,"long":self.selectedLong!,"description":self.txtDescription.text!,"rsvp":self.isRSVPYes!,"event_page":"\(self.txtEventPage.text!)","from_date":startDateTimestamp!,"to_date":endDateTimestamp!,"meeting_link":"","meeting_dial_number":"","meeting_pin":""] as [String : Any]
        }
        
        print(parameter)
        ActivityIndicatorView.show("Loading....")
        
        networkProvider.request(.createEventUpdate(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    print(jsonData["status"])
                    print(response.statusCode)
                    if response.statusCode == 200{
                        
                        self.eventDetails = jsonData["data"]
                        let AlertCus = CustomAlertVC.show(in: self, title: "Success", message: "Successfully Updated event ", image:UIImage.init(named: "Green_tick")!)
                        AlertCus.addAction(CPAlertAction.init(title: "OK", type: .normal, handler: {
                            self.navigationController?.popToViewController(ofClass: EventDetailsViewController.self)
                            
                        }))
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.apiForCreateEventUpdate()
                        }
                    }
                        
                    else{
                        print(jsonData["code"])
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                } catch let err {
                    
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
            
        })
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    
    
}


extension CreateEventsStep2ViewController: SambagDatePickerViewControllerDelegate{
    func didSelectDate(result:String,dateTag:Int){
        if dateTag == 0{
            //            txtStartDate.text = "\(result)"
            selectedStartDate = "\(result)"
            print(selectedStartDate)
            self.timetag = 0
            
        }
        else{
            //            txtEndDate.text = "\(result)"
            selectedEndDate = "\(result)"
            print(selectedEndDate)
            
            self.timetag = 1
            
        }
        let vc = SambagTimePickerViewController()
        vc.theme = theme
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    func sambagDatePickerDidSet(_ viewController: SambagDatePickerViewController, result: SambagDatePickerResult) {
        if self.dateTag == 0
        {
            //            txtStartDate.text = "\(result)"
            selectedStartDate = "\(result)"
            print(selectedStartDate)
            self.timetag = 0
            
        }
        else
        {
            //            txtEndDate.text = "\(result)"
            selectedEndDate = "\(result)"
            print(selectedEndDate)
            
            self.timetag = 1
            
        }
        viewController.dismiss(animated: true, completion: nil)
        let vc = SambagTimePickerViewController()
        vc.theme = theme
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    func sambagDatePickerDidCancel(_ viewController: SambagDatePickerViewController) {
        if self.dateTag == 0
        {
            txtStartDate.text = ""
            selectedStartDate = ""
            
        }
        else
        {
            txtEndDate.text = ""
            selectedEndDate = ""
        }
        viewController.dismiss(animated: true, completion: nil)
    }
    
}


extension CreateEventsStep2ViewController: SambagTimePickerViewControllerDelegate {
    
    func sambagTimePickerDidSet(_ viewController: SambagTimePickerViewController, result: SambagTimePickerResult) {
        if self.timetag == 0
        {
            self.txtEndDate.text = ""
            selectedEndDate = ""
            
            
            selectedStartDate += "   \(result)"
            
            self.selectedStartDateFormatted = self.convertDateFormat(date: selectedStartDate)
            
            print(selectedStartDateFormatted)
            
            if selectedStartDateFormatted != ""{
                
                if self.checkDateHigherThanCurrentDate(selectedDatestring: self.selectedStartDateFormatted)
                {
                    // Helpers.showAlertDialog(message: "Please Select date less than current date and time", target: self)
                    
                    self.showToast(message: "Please Select Start date higher than current date and time")
                    txtStartDate.text = ""
                    selectedStartDate = ""
                    self.selectedStartDateFormatted = ""
                }
                else
                {
                    txtStartDate.text = Helpers.sambagTimeDisplay(dateAsString:"\(selectedStartDate)")
                }
            }
            else
            {
                self.showToast(message: "Please Select valid date")
                txtStartDate.text = ""
                selectedStartDate = ""
                self.selectedStartDateFormatted = ""
            }
            
        }
        else
        {
            selectedEndDate += " \(result)"
            
            print(selectedEndDate)
            
            self.selectedEndDateFormatted = self.convertDateFormat(date: selectedEndDate)
            print(selectedEndDateFormatted)
            if selectedEndDateFormatted != ""
            {
                if self.checkDateHigherThanCurrentDate(selectedDatestring: self.selectedEndDateFormatted)
                {
                    //                Helpers.showAlertDialog(message: "Please Select date less than current date and time", target: self)
                    
                    self.showToast(message: "Please Select End date Higher than current date and time")
                    txtEndDate.text = ""
                    selectedEndDate = ""
                    selectedEndDateFormatted = ""
                }
                else if checkstartDateandEndDate(StartDate: selectedStartDateFormatted
                    , endDate: selectedEndDateFormatted)
                {
                    //                Helpers.showAlertDialog(message: "Please Select end date higher than startDate", target: self)
                    self.showToast(message: "Please Select end date higher than startDate")
                    txtEndDate.text = ""
                    selectedEndDate = ""
                    selectedEndDateFormatted = ""
                }
                else if selectedStartDateFormatted == selectedEndDateFormatted{
                    self.showToast(message: "Please Select different endDate and startDate ")
                    txtEndDate.text = ""
                    selectedEndDate = ""
                    selectedEndDateFormatted = ""
                }
                else
                {
                    
                    
                    txtEndDate.text = Helpers.sambagTimeDisplay(dateAsString:"\(selectedEndDate)")
                    
                }
            }
            else
            {
                self.showToast(message: "Please Select valid date")
                txtEndDate.text = ""
                selectedEndDate = ""
                selectedEndDateFormatted = ""
                
            }
            
            
        }
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func sambagTimePickerDidCancel(_ viewController: SambagTimePickerViewController) {
        if self.timetag == 0
        {
            txtStartDate.text = ""
            selectedStartDate += ""
            self.selectedStartDateFormatted = ""
            
        }
        else
        {
            txtEndDate.text = ""
            selectedEndDate += ""
            self.selectedEndDateFormatted = ""
        }
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func convertDateFormat(date:String)->String
    {
        
        //        let formatter = DateFormatter()
        //        formatter.dateFormat = "dd-MM-yyyy hh:mm a"
        //        //        let convertedDate = formatter.date(from: "16-09-2017 2:00 PM")
        //        let convertedDate = formatter.date(from: date)
        //
        //        formatter.timeZone = TimeZone(identifier: "UTC")
        //        let date = formatter.string(from: convertedDate!)
        //        print(date)
        //        let formatter2 = DateFormatter()
        //        formatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        //        let convertedDate2 = formatter.date(from: date)
        //        formatter2.timeZone = TimeZone.current
        //        let date2 = formatter2.string(from: convertedDate2!)
        //        print(date2)
        //
        //        //        let timestamp = convertedDate2!.timeIntervalSince1970
        //        //        print(timestamp)
        //
        //        return date2
        
        
        do{
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy hh:mm a"
            //        let convertedDate = formatter.date(from: "16-09-2017 2:00 PM")
            if  let convertedDate = try formatter.date(from: date){
                //                formatter.timeZone = TimeZone(identifier: "UTC")
                formatter.timeZone = NSTimeZone.local
                let date = formatter.string(from: convertedDate)
                print(date)
                let formatter2 = DateFormatter()
                formatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
                let convertedDate2 = formatter.date(from: date)
                formatter2.timeZone = NSTimeZone.local
                let date2 = formatter2.string(from: convertedDate2!)
                print(date2)
                
                //        let timestamp = convertedDate2!.timeIntervalSince1970
                //        print(timestamp)
                
                return date2
            }
            else{
                return ""
            }
            
        }catch{
            return ""
        }
    }
    
    func convertTimestampToFormattedDate(date:String)->String
    {
        
        do{
            let formatter = DateFormatter()
            formatter.dateFormat = "MMM dd yyyy hh:mm a"
            //        let convertedDate = formatter.date(from: "16-09-2017 2:00 PM")
            if  let convertedDate = try formatter.date(from: date){
                //                formatter.timeZone = TimeZone(identifier: "UTC")
                formatter.timeZone = NSTimeZone.local
                let date = formatter.string(from: convertedDate)
                print(date)
                let formatter2 = DateFormatter()
                formatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
                let convertedDate2 = formatter.date(from: date)
                formatter2.timeZone = NSTimeZone.local
                let date2 = formatter2.string(from: convertedDate2!)
                print(date2)
                
                //        let timestamp = convertedDate2!.timeIntervalSince1970
                //        print(timestamp)
                
                return date2
            }
            else{
                return ""
            }
            
        }catch{
            return ""
        }
    }
    
    
    func convertTimetoTimestamp(date:String)->Int
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"
        //        let convertedDate = formatter.date(from: "16-09-2017 2:00 PM")
        let convertedDate = formatter.date(from: date)
        
        //        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.timeZone = NSTimeZone.local
        let date = formatter.string(from: convertedDate!)
        print(date)
        let convertedDate2 = formatter.date(from: date)
        
        let timestamp = convertedDate2!.timeIntervalSince1970
        print(timestamp)
        
        //        return "\(timestamp)"
        return Int(timestamp)
        
    }
    
    func compairDateWithCurrentDate (selectedDatestring:String)->Bool
    {
        print(selectedDatestring)
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"
        
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        let convertedDateNow = formatter.string(from: Date())
        let convertedDateNow2 = formatter.date(from: convertedDateNow)
        formatter2.timeZone = NSTimeZone.local
        let dateNow = formatter2.string(from: convertedDateNow2!)
        print(dateNow)
        print(selectedDatestring == dateNow)
        return selectedDatestring == dateNow
    }
    
    func checkDateHigherThanCurrentDate (selectedDatestring:String)->Bool
    {
        print(selectedDatestring)
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy hh:mm a"
        
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        let convertedDateNow = formatter.string(from: Date())
        let convertedDateNow2 = formatter.date(from: convertedDateNow)
        formatter2.timeZone = NSTimeZone.local
        let dateNow = formatter2.string(from: convertedDateNow2!)
        print(dateNow)
        print(selectedDatestring < dateNow)
        return selectedDatestring < dateNow
    }
    
    func checkstartDateandEndDate (StartDate:String , endDate:String)->Bool
    {
        print(StartDate)
        print(endDate)
        print(StartDate > endDate)
        return StartDate > endDate
    }
    
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: 20, y: self.view.frame.size.height/2-30, width: self.view.frame.width-40, height: 60))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.numberOfLines = 2
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 10.0)
        toastLabel.adjustsFontSizeToFitWidth = true
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 6.0, delay: 0.3, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}

extension CreateEventsStep2ViewController: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        // Get the place name from 'GMSAutocompleteViewController'
        // Then display the name in textField
        
        print(place.addressComponents!)
        
        //        print("Place name: \(place.name)")
        //        print("Place address: \(place.formattedAddress)")
        //        print("Place attributions: \(place.attributions)")
        
        txtVenue.text = "\(place.formattedAddress!)"
        var keys = [String]()
        var locality = String()
        var country = String()
        
        place.addressComponents?.forEach{keys.append($0.type)}
        var values = [String]()
        place.addressComponents?.forEach({ (component) in
            keys.forEach{ component.type == $0 ? values.append(component.name): nil}
            
            print("All component type \(component.type)")
            
            if component.type == "locality"{
                locality = "\(component.name)"
            }
            if component.type == "country"{
                country = "\(component.name)"
            }
        })
        
        //        txtVenue.text = "\(place.name!) ,\(locality)"
        self.selectedLat = place.coordinate.latitude
        self.selectedLong = place.coordinate.longitude
        print(place.coordinate.latitude)
        print(place.coordinate.longitude)
        
        dismiss(animated: true, completion: nil)
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // Handle the error
        print("Error: ", error.localizedDescription)
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        // Dismiss when the user canceled the action
        dismiss(animated: true, completion: nil)
    }
}
