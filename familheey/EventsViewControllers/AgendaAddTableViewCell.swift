//
//  AgendaAddTableViewCell.swift
//  familheey
//
//  Created by Innovation Incubator on 21/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class AgendaAddTableViewCell: UITableViewCell {
    
    @IBOutlet weak var btnImageTap: subclassedUIButton!
    @IBOutlet weak var lblStartTime: UILabel!
    @IBOutlet weak var lblEndTime: UILabel!
    @IBOutlet weak var imgOfAgenda: UIImageView!
    
    @IBOutlet weak var lblAgendaDescription: UILabel!
    @IBOutlet weak var lblAgendaName: UILabel!
    
    @IBOutlet weak var viewOfBorder: UIView!
    @IBOutlet weak var viewOfLine: UIView!
    @IBOutlet weak var viewOfCircle: UIView!
    @IBOutlet weak var widthOfImageAgenda: NSLayoutConstraint!
    @IBOutlet weak var btnDelete: subclassedUIButton!
    @IBOutlet weak var btnEdit: subclassedUIButton!
    
    @IBOutlet weak var leadingOfAgendaTitle: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
