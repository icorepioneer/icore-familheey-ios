//
//  EventListsTableViewCell.swift
//  familheey
//
//  Created by familheey on 18/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import ActiveLabel

class EventListsTableViewCell: UITableViewCell {
    @IBOutlet weak var imgPropic: UIImageView!
    @IBOutlet weak var lblCreatedBy: UILabel!
    @IBOutlet weak var lblCreatedDate: UILabel!
    @IBOutlet weak var btnShare: subclassedUIButton!
    
    @IBOutlet weak var imgLocationorUrl: UIImageView!
    @IBOutlet weak var imgEvent: UIImageView!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblEventName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblLocation: ActiveLabel!
    @IBOutlet weak var lblGoneCount: UILabel!
    @IBOutlet weak var lblIntrestedCount: UILabel!
    @IBOutlet weak var lblKnownCount: UILabel!
    @IBOutlet weak var lblEventCate: UILabel!
    @IBOutlet weak var lblEventType: UILabel!
    @IBOutlet weak var lblIspublic: UILabel!
    @IBOutlet weak var lblTicketType: UILabel!
    
    @IBOutlet weak var btnGoing: UIButton!
    @IBOutlet weak var btnMaybe: UIButton!
    @IBOutlet weak var btnNotIntrested: UIButton!
    @IBOutlet weak var btnCalander: UIButton!
    
    @IBOutlet weak var imgShare: UIImageView!
    @IBOutlet weak var typeView: UIView!
    @IBOutlet weak var rsvpView: UIView!
    @IBOutlet weak var lblGoingHead: UILabel!
    @IBOutlet weak var lblIntrstdHead: UILabel!
    @IBOutlet weak var goingView: UIView!
    @IBOutlet weak var intrstedView: UIView!
//    @IBOutlet weak var going_width: NSLayoutConstraint!
    
    @IBOutlet weak var bottomVw                             : UIView!
    
    @IBOutlet weak var blackVw                              : GradientView!
    @IBOutlet weak var shareOtherVw                         : UIButton!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        blackVw.layer.cornerRadius            = 9
        blackVw.clipsToBounds                 = true
        blackVw.layer.maskedCorners           = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        
        // Initialization code
    }

 
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
