//
//  EventListingViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 26/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Moya
import Alamofire
import Kingfisher
import SwiftyJSON

class EventListingViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,UITextFieldDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var lblNoDataHeading1: UILabel!
    @IBOutlet weak var imgViewNoEvent: UIImageView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearchReset: UIButton!
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var backWhiteVw                     : UIView!
    //    @IBOutlet weak var searchField                     : UITextField!
    @IBOutlet weak var tbl                             : UITableView!
    private var networkProvider                        = MoyaProvider<FamilyheeyApi>()
    public var groupID                                 = String()
    
    var dataArr                                        = [JSON]()
    var sharedArr                                      = [JSON]()
    
    var link_type = 0
    var isAdmin = ""
    var islinkable = 0
    var facte = ""
    var famSettings = 0
    var announcementSet = 0
    var postId                          = 0
    var announcementId                  = 0
    var isMembershipActive = false
    var famArr : [familyModel]?
    var memberJoining = 0
    var invitationStatus = 0
    
    @IBOutlet weak var btnAddEvent: UIButton!
    @IBOutlet weak var noEventView: UIView!
    @IBOutlet weak var lblHead: UILabel!
    var reduceRowHeight = false
    @IBOutlet weak var aboutCollectionVew: UICollectionView!
    var aboutArr = ["FEED","ANNOUNCEMENT","REQUEST","EVENTS","ALBUMS","ABOUT US","DOCUMENTS","LINKED FAMILIES","MEMBERS"]
    var ActiveTab = 3
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isAdmin.lowercased() == "admin"{
            if isMembershipActive{
                aboutArr = ["FEED","ANNOUNCEMENT","REQUEST","EVENTS","ALBUMS","ABOUT US","DOCUMENTS","LINKED FAMILIES","MEMBERSHIP","MEMBERS"]
            }
        }
        aboutCollectionVew.delegate = self
        aboutCollectionVew.dataSource = self
        
        tbl.register(UINib(nibName: "EventListsTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        if famSettings == 7{
            if isAdmin.lowercased() == "admin"{
                btnAddEvent.isHidden = false
            }
            else{
                btnAddEvent.isHidden = true
            }
        }
        else{
            btnAddEvent.isHidden = false
        }
        
        
        self.lblHead.text = appDel.groupNamePublic
        
        let imgUrl = URL(string: appDel.groupImageUrlPublic)
        if imgUrl != nil{
            self.imgCover.kf.indicatorType = .activity
            
            let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=475&height=400&url="+imgUrl!.relativeString
            let urlImg = URL(string: newUrlStr)
            
            self.imgCover.kf.setImage(with: urlImg, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        else{
            self.imgCover.image = #imageLiteral(resourceName: "family_default_cover.png")
        }
        
        
        Helpers.setleftView(textfield: txtSearch, customWidth: 30)
        self.txtSearch.delegate = self
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        apiInviteEvents()
    }
    override func viewDidLayoutSubviews(){
        super.viewDidLayoutSubviews()
        aboutCollectionVew.reloadData()
        let indexPath = IndexPath(item: ActiveTab, section: 0)
        aboutCollectionVew.scrollToItem(at: indexPath, at:.centeredHorizontally, animated: true)
    }
    
    //MARK:- Custom
    func convertTimeStamp(timestamp:String) -> String {
        let date = Date(timeIntervalSince1970: Double(timestamp) as! TimeInterval)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MMM/yyyy hh:mma"
        //        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.timeZone = TimeZone.current
        
        
        let localDatef = formatter.string(from: date)
        print(localDatef)
        return localDatef
    }
    
    func filterDayOnly(timestamp:String) -> String {
        let date = Date(timeIntervalSince1970: Double(timestamp) as! TimeInterval)
        //    dateFormatter.dateFormat       = "EEE MMM dd yyy"
        
        let formatter = DateFormatter()
        formatter.dateFormat = "EEE"
        //        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.timeZone = TimeZone.current
        
        
        let localDatef = formatter.string(from: date)
        print(localDatef)
        return localDatef
    }
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: 20, y: self.view.frame.size.height/2-30, width: self.view.frame.width-40, height: 60))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.numberOfLines = 2
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 10.0)
        toastLabel.adjustsFontSizeToFitWidth = true
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 6.0, delay: 0.3, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    @IBAction func onClickShareAction(_ sender: subclassedUIButton){
        var data = JSON()
        print(sender.indexPath!)
        print(sender.indexSection!)
        //        if selectedIndex == 0{
        if sender.indexSection == 0{
            data = self.dataArr[sender.indexPath!]
        }
            //            else if sender.indexSection == 1{
            //                data = self.nearbyEventsArr[sender.indexPath!]
            //            }
        else{
            data = self.sharedArr[sender.indexPath!]
        }
        //        }
        //        else if selectedIndex == 1{
        //            data = inviteEventsArr[sender.indexPath!]
        //        }
        //        else{
        //            //            data = self.myEventsArr[sender.indexPath!]
        //
        //
        //            if sender.indexSection == 0{
        //                data = self.createdEventsArr[sender.indexPath!]
        //            }
        //            else if sender.indexSection == 1{
        //                data = self.rsvpYesEventsArr[sender.indexPath!]
        //            }
        //            else{
        //                data = self.rsvpMaybeEventsArr[sender.indexPath!]
        //            }
        //        }
        
        
        print(data)
        
        
        let stryboard = UIStoryboard.init(name: "Main", bundle: nil)
        
        let vc = stryboard.instantiateViewController(withIdentifier: "EventShareViewController") as! EventShareViewController
        
        vc.eventId = "\(data["event_id"])"
        if data["public_event"].stringValue.lowercased() == "true"{
            vc.typeOfInvitations = "share"
        }
        else{
            vc.typeOfInvitations = "invitation"
        }
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    //MARK:- WEB API
    
    func apiInviteEvents(){
        let parameter = ["group_id":self.groupID,"query":self.txtSearch.text!] as [String : Any]
        print(parameter)
        ActivityIndicatorView.show("Loading....")
        
        networkProvider.request(.GetAllEventsByGroup(parameter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    
                    if response.statusCode == 200{
                        self.dataArr = jsonData["data"]["event_invitation"].arrayValue
                        self.sharedArr = jsonData["data"]["event_share"].arrayValue
                        if self.dataArr.count > 0 || self.sharedArr.count > 0{
                            self.tbl.isHidden = false
                            self.noEventView.isHidden = true
                            self.lblNoDataHeading1.isHidden = true
                            self.imgViewNoEvent.isHidden = true
                            
                            self.tbl.reloadData()
                        }
                        else{
                            self.tbl.isHidden = true
                            self.noEventView.isHidden = false
                            if self.txtSearch.text!.isEmpty{
                                self.lblNoDataHeading1.isHidden = false
                                
                                self.lblNoDataHeading1.text = "No events yet? let's add a few..."
                                //                                                           self.lblNoDataHeading2.text = "Simply set your 1st event up!"
                                self.imgViewNoEvent.isHidden = false
                                
                                
                            }
                            else{
                                self.lblNoDataHeading1.isHidden = false
                                self.lblNoDataHeading1.text = "No results to show"
                                self.imgViewNoEvent.isHidden = false
                                
                            }
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.apiInviteEvents()
                        }
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    //MARK: - Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            return self.dataArr.count
        }
        else{
            return self.sharedArr.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellid = "cell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! EventListsTableViewCell
        
        cell.selectionStyle = .none
        cell.shareOtherVw.isHidden = true
        cell.btnShare.indexPath = indexPath.row
        cell.btnShare.indexSection = indexPath.section
        var data = JSON()
        if indexPath.section == 0{
            data = self.dataArr[indexPath.row]
        }
        else{
            data = self.sharedArr[indexPath.row]
        }
        
        // cell.lblCreatedDate.isHidden = true
        cell.lblTicketType.isHidden = true
        cell.typeView.isHidden = true
        
        cell.lblCreatedBy.text = data["created_by_name"].stringValue
        // let tempDate = data["created_at"].stringValue.components(separatedBy: "T")
        //        let tempTime = tempDate.last?.components(separatedBy: ".")
        //
        //        cell.lblCreatedDate.text = "\(tempDate.first ?? "") \(tempTime!.first ?? "")"
        cell.lblEventName.text = data["event_name"].stringValue
        // cell.lblLocation.text = data["location"].stringValue
        
        //"created_at_" not get in api
        
        let convertedDate =  convertTimeStampToDate(timestamp: data["created_at_"].stringValue)
        print(convertedDate)
        cell.lblCreatedDate.text = "\(convertedDate)"
        
        var fDate = ""
        var tDate = ""
        
        var fdateOnly = [String]()
        var tdateOnly = [String]()
        var fDateArray = [String]()
        var tDateArray = [String]()
        var fYear = [String]()
        var tYear = [String]()
        
        if data["from_date"].stringValue.count > 0{
            fDate = convertTimeStamp(timestamp: data["from_date"].stringValue)
            fdateOnly = fDate.components(separatedBy: " ")
            fDateArray = fDate.components(separatedBy: "/")
            fYear = fDateArray[2].components(separatedBy: " ")
        }
        if data["to_date"].stringValue.count > 0{
            tDate = convertTimeStamp(timestamp: data["to_date"].stringValue)
            tdateOnly = tDate.components(separatedBy: " ")
            tDateArray = tDate.components(separatedBy: "/")
            tYear = tDateArray[2].components(separatedBy: " ")
        }
        
        if fdateOnly.count > 0 && tdateOnly.count > 0{
            
            if fdateOnly[0] == tdateOnly[0]
            {
                cell.lblTime.text = "\(filterDayOnly(timestamp: data["from_date"].stringValue)) , \(fdateOnly[1]) - \(tdateOnly[1])"
            }
            else
            {
                if fYear[0] == tYear[0]
                {
                    cell.lblTime.text = "\(filterDayOnly(timestamp: data["from_date"].stringValue)) \(fDateArray[1]) \(fDateArray[0]) - \(filterDayOnly(timestamp: data["to_date"].stringValue)) \(tDateArray[1]) \(tDateArray[0]) , \(tYear[0]) "
                    
                }
                else
                {
                    cell.lblTime.text = "\(filterDayOnly(timestamp: data["from_date"].stringValue)) \(fDateArray[1]) \(fDateArray[0]) \(fYear[0])  - \(filterDayOnly(timestamp: data["to_date"].stringValue)) \(tDateArray[1]) \(tDateArray[0]) \(tYear[0]) "
                }
            }
        }
        
        if fDateArray.count > 0{
            cell.lblMonth.text = fDateArray[1].uppercased()
            cell.lblDate.text = fDateArray[0]
        }
        
        if data["event_type"].string?.lowercased() == "online"{
            if data["location"].stringValue.isEmpty{
                cell.lblLocation.text = data["meeting_link"].stringValue
                cell.imgLocationorUrl.image = #imageLiteral(resourceName: "pointer_icon")
            }
            else{
                cell.lblLocation.text = "Unknown event"
                cell.imgLocationorUrl.image = #imageLiteral(resourceName: "ic_location_details_")
            }
            if data["meeting_logo"].stringValue != ""{
                let temp = "\(Helpers.imageURl)"+data["meeting_logo"].stringValue
                let imgUrl = URL(string: temp)
                let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=30&height=30&url="+imgUrl!.relativeString
                
                let url = URL(string: newUrlStr)
                
                
                cell.imgLocationorUrl.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
            }
            else{
                cell.imgLocationorUrl.image = #imageLiteral(resourceName: "pointer_icon")
            }
        }
        else{
            cell.lblLocation.text = data["location"].stringValue
            cell.imgLocationorUrl.image = #imageLiteral(resourceName: "ic_location_details_")
        }
        
        cell.lblEventCate.text = data["category"].stringValue.uppercased()
        cell.lblEventType.text = data["event_type"].stringValue
        
        cell.lblMonth.text = fDateArray[1].uppercased()
        cell.lblDate.text = fDateArray[0]
        
        if data["public_event"].stringValue.lowercased() == "true"{
            cell.lblIspublic.text = "Public"
            if data["is_sharable"].stringValue.lowercased() == "true"{
                cell.imgShare.isHidden = false
                cell.btnShare.isEnabled = true
            }
            else{
                cell.imgShare.isHidden = false
                cell.btnShare.isEnabled = true
            }
        }
        else{
            cell.lblIspublic.text = "Private"
            //                    cell.imgShare.isHidden = true
            //                    cell.btnShare.isEnabled = false
            if data["is_sharable"].stringValue.lowercased() == "true"{
                cell.imgShare.isHidden = false
                cell.btnShare.isEnabled = true
            }
            else{
                cell.imgShare.isHidden = false
                cell.btnShare.isEnabled = true
            }
        }
        
        let GoneCount = data["going_count"].intValue
        if GoneCount > 0{
            cell.lblGoneCount.text = data["going_count"].stringValue
            cell.lblGoneCount.isHidden = false
            cell.lblGoingHead.isHidden = false
            cell.goingView.isHidden = false
            //            cell.going_width.constant = 90
            let firstPerson = data["first_person_going"].string
            cell.lblKnownCount.isHidden = false
            if GoneCount > 1{
                cell.lblKnownCount.text = firstPerson!+" and \(GoneCount-1) others are going"
            }
            else if GoneCount == 2{
                cell.lblKnownCount.text = firstPerson!+" and \(GoneCount-1) other is going"
            }
            else{
                cell.lblKnownCount.text = firstPerson!+" is going"
            }
        }
        else{
            cell.lblGoneCount.isHidden = true
            cell.lblGoingHead.isHidden = true
            cell.goingView.isHidden = true
            //            cell.going_width.constant = 0
            cell.lblKnownCount.isHidden = true
        }
        
        let IntrstedCount = data["interested_count"].intValue
        if IntrstedCount > 0{
            cell.lblIntrestedCount.text = data["interested_count"].stringValue
            cell.lblIntrstdHead.isHidden = false
            cell.lblIntrestedCount.isHidden = false
            
        }
        else{
            cell.lblIntrstdHead.isHidden = true
            cell.lblIntrestedCount.isHidden = true
        }
        
        if GoneCount > 0 || IntrstedCount > 0{
            cell.bottomVw.isHidden = false
            self.reduceRowHeight = false
            self.tbl.beginUpdates()
            self.tbl.endUpdates()
        }
        else{
            cell.bottomVw.isHidden = true
            self.reduceRowHeight = true
            self.tbl.beginUpdates()
            
            self.tbl.endUpdates()
        }
        
        let proPic = data["propic"].stringValue
        if proPic.count != 0{
            let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+proPic
            let imgUrl = URL(string: temp)
            
            let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+imgUrl!.relativeString
            let url = URL(string: newUrlStr)
            cell.imgPropic.kf.indicatorType = .activity
            
            cell.imgPropic.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        else{
            cell.imgPropic.image = #imageLiteral(resourceName: "Male Colored")
        }
        
        let eventImage = data["event_image"].stringValue
        if eventImage.count != 0{
            let temp = "\(Helpers.imageURl)"+"\(BaseUrl.event_image)"+eventImage
            let imgUrl = URL(string: temp)
            let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=400&height=300&url="+imgUrl!.relativeString
            let url = URL(string: newUrlStr)
            cell.imgEvent.kf.indicatorType = .activity
            
            cell.imgEvent.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
        }//[.forceRefresh]
        else{
            cell.imgEvent.image = #imageLiteral(resourceName: "default_event_image")
        }
        
        cell.btnShare.addTarget(self, action: #selector(onClickShareAction(_:)), for: .touchUpInside)
        cell.lblLocation.handleURLTap { (url) in
            print(url)
            self.showActionSheet(titleArr: ["Open URL","Copy URL","Share URL"], title: "Select option") { (index) in
                if index == 100{}
                else if index == 0{
                    var data = JSON()
                    if indexPath.section == 0{
                        data = self.dataArr[indexPath.row]
                    }
                    else{
                        data = self.sharedArr[indexPath.row]
                    }
                    UIPasteboard.general.string = data["meeting_pin"].stringValue
                    self.showToast(message: "Participant pin copied to clipboard")
                    DispatchQueue.main.asyncAfter(deadline: .now()+3) {
                        UIApplication.shared.open(url)
                    }
                }
                else if index == 1{
                    UIPasteboard.general.string = url.absoluteString
                }
                else if index == 2{
                    let textToShare = [ url.absoluteString ]
                    let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                    activityViewController.popoverPresentationController?.sourceView = self.view
                    self.present(activityViewController, animated: true, completion: nil)
                }
            }
        }
        return cell
    }
    
    //MARK:- Custom methods
    func convertTimeStampToDate(timestamp:String)->String{
        let date = Date(timeIntervalSince1970: Double(timestamp) as! TimeInterval)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd yyyy hh:mm a"
        formatter.timeZone = TimeZone.current
        
        
        let localDatef = formatter.string(from: date)
        print(localDatef)
        return localDatef
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        if reduceRowHeight{
        //            return 455
        //        }
        //        else{
        //            return 480
        //        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let stryboard = UIStoryboard.init(name: "third", bundle: nil)
        
        let vc = stryboard.instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
        
        var data = JSON()
        if indexPath.section == 0{
            data = self.dataArr[indexPath.row]
        }
        else{
            data = self.sharedArr[indexPath.row]
        }
        
        print(data)
        if UserDefaults.standard.value(forKey: "userId") as! String == data["created_by"].stringValue
        {
            vc.enableEventEdit = true
        }else
        {
            vc.enableEventEdit = false
        }
        vc.eventDetails = data
        vc.fromCreate = false
        vc.isFrom = "family"
        vc.faEventCreate = true
        
        self.navigationController?.pushViewController(vc, animated: true)
        
        //below data are neeeded
        //        vc.eventDetails = self.eventDetails
        //        vc.fromCreate = true
        //        vc.enableEventEdit = true
        //        vc.groupId = self.groupId
        //        vc.faEventCreate = self.faEventCreate
        //        vc.isFrom = self.isFrom
        
    }
    //MARK: - Search Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == txtSearch
        {
            if textField.text!.count > 0{
                btnSearchReset.isHidden = false
            }
            else{
                btnSearchReset.isHidden = true
            }
            textField.endEditing(true)
        }
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        self.view.endEditing(true)
        if textField == txtSearch
        {
            if textField.text!.count > 0{
                btnSearchReset.isHidden = false
            }
            else{
                btnSearchReset.isHidden = true
            }
            apiInviteEvents()        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
        }
        return true
    }
    
    
    @IBAction func onClickSearchReset(_ sender: Any) {
        txtSearch.text = ""
        apiInviteEvents()
        btnSearchReset.isHidden = true
        self.txtSearch.endEditing(true)
    }
    //MARK: - IB Actions
    
    
    @IBAction func membersClicked(_ sender: Any) {
        
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let addMember = storyboard.instantiateViewController(withIdentifier: "AddMembersNewViewController") as! AddMembersNewViewController
        addMember.groupId = self.groupID
        addMember.isAdmin = self.isAdmin
        addMember.link_type = self.link_type
        addMember.islinkable = self.islinkable
        addMember.faCate = self.facte
        addMember.famSettings = famSettings
        addMember.announcementSet = announcementSet
        addMember.postId                    = self.postId
        addMember.announcementId            = self.announcementId
        addMember.famArr = famArr
        addMember.memberJoiningStatus = self.memberJoining
        addMember.invitationStatus = self.invitationStatus
        self.navigationController?.pushViewController(addMember, animated: false)
    }
    
    @IBAction func albumsClicked(_ sender: Any) { // Changed to post tab clicked
        let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
        let addMember = storyboard.instantiateViewController(withIdentifier: "PostedInGroupsViewController") as! PostedInGroupsViewController
        addMember.groupId = self.groupID
        addMember.isAdmin = self.isAdmin
        addMember.link_type = self.link_type
        addMember.islinkable = self.islinkable
        addMember.facate = self.facte
        addMember.famSettings = famSettings
        addMember.announcementSet = announcementSet
        addMember.postId                    = self.postId
        addMember.announcementId            = self.announcementId
        addMember.famArr = famArr
        addMember.memberJoining = self.memberJoining
        addMember.invitationStatus = self.invitationStatus
        addMember.isMembershipActive = self.isMembershipActive
        self.navigationController?.pushViewController(addMember, animated: false)
        
    }
    
    @IBAction func aboutUsClicked(_ sender: Any) { // Changed to announcements tab click
        let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
        let addMember = storyboard.instantiateViewController(withIdentifier: "AnnouncementInFamilyViewController") as! AnnouncementInFamilyViewController
        addMember.groupId = self.groupID
        addMember.isAdmin = self.isAdmin
        addMember.link_type = self.link_type
        addMember.islinkable = self.islinkable
        addMember.facate = self.facte
        addMember.famSettings = famSettings
        addMember.announcementSet = announcementSet
        addMember.postId                    = self.postId
        addMember.announcementId            = self.announcementId
        addMember.famArr = famArr
        addMember.memberJoining = self.memberJoining
        addMember.invitationStatus = self.invitationStatus
        addMember.isMembershipActive = self.isMembershipActive
        self.navigationController?.pushViewController(addMember, animated: false)
    }
    
    @IBAction func onClickMore(_ sender: Any) {
        let arr = ["About us","Albums","Documents","Linked families"]
        
        self.showActionSheet(titleArr: arr as! NSArray, title: "Choose option") { (index) in
            if index == 2{
                let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "DocumentListingViewController") as! DocumentListingViewController
                addMember.groupID = self.groupID
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.isFromdocument = true
                addMember.facate = self.facte
                addMember.famArr = self.famArr
                //        addMember.faCate = self.familyArr![0].faCategory
                //        addMember.memberJoining = self.familyArr![0].memberApproval
                self.navigationController?.pushViewController(addMember, animated: false)
                //setActiveButton(index: 2)
            }
            else if index == 100{
            }
            else if index == 0{
                let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
                intro.groupId = self.groupID
                intro.isAboutUsClicked = true
                self.navigationController?.pushViewController(intro, animated: false)
            }
            else if index == 1{
                let AlbumListingViewController = self.storyboard?.instantiateViewController(withIdentifier: "AlbumListingViewController") as! AlbumListingViewController
                AlbumListingViewController.isAdmin = self.isAdmin
                AlbumListingViewController.link_type = self.link_type
                AlbumListingViewController.islinkable = self.islinkable
                AlbumListingViewController.groupID = self.groupID
                AlbumListingViewController.facate = self.facte
                self.navigationController?.pushViewController(AlbumListingViewController, animated: false)
            }
            else{
                let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                let addMember = storyboard.instantiateViewController(withIdentifier: "LinkedFamiliesListViewController") as! LinkedFamiliesListViewController
                addMember.groupID = self.groupID
                addMember.isAdmin = self.isAdmin
                addMember.link_type = self.link_type
                addMember.islinkable = self.islinkable
                addMember.facate = self.facte
                // addMember.faCate = self.familyArr![0].faCategory
                //                addMember.memberJoining = self.familyArr![0].memberApproval
                self.navigationController?.pushViewController(addMember, animated: false)
            }
        }
    }
    
    @IBAction func backClicked(_ sender: Any) {
        
        // self.navigationController?.popToRootViewController(animated: true)
        self.navigationController?.popToViewController(ofClass: FamilyDetailsTableViewController.self)
    }
    
    @IBAction func onClickAddEvent(_ sender: Any) {
        if famSettings == 7{
            if self.isAdmin.lowercased() != "admin"{
                self.displayAlert(alertStr: "You don't have permission", title: "")
                return
            }
        }
        
        let stryboard = UIStoryboard.init(name: "third", bundle: nil)
        
        let vc = stryboard.instantiateViewController(withIdentifier: "CreateEventStep1ViewController") as! CreateEventStep1ViewController
        vc.isFrom = "family"
        print(self.groupID)
        vc.groupId = self.groupID
        vc.faEventCreate = true
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickCalendarAction(_ sender: Any) {
        let stryboard = UIStoryboard.init(name: "third", bundle: nil)
        
        let vc = stryboard.instantiateViewController(withIdentifier: "CalendarEventsViewController") as! CalendarEventsViewController
        vc.isFromExplore = ""
        vc.isFromGroup = true
        vc.groupID = self.groupID
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- collectionView Delegates
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return aboutArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AboutFamilyCollectionViewCell", for: indexPath as IndexPath) as! AboutFamilyCollectionViewCell
        cell.lblName.text = aboutArr[indexPath.item]
        if indexPath.row == ActiveTab{
            cell.lblName.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            cell.lblName.font = .boldSystemFont(ofSize: 15)
            cell.imgUnderline.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            
        }
        else{
            cell.lblName.textColor = .black
            cell.lblName.font = .systemFont(ofSize: 14)
            cell.imgUnderline.backgroundColor = .white
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0{ // Feeds
            albumsClicked(self)
        }
        else if indexPath.row == 1{ //Announcements
            aboutUsClicked(self)
        }
        else if indexPath.row == 2{ // Request
            let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
            let addMember = storyboard.instantiateViewController(withIdentifier: "RequestInFamilyViewController") as! RequestInFamilyViewController
            addMember.groupID = self.groupID
            addMember.isAdmin = self.isAdmin
            addMember.link_type = self.link_type
            addMember.islinkable = self.islinkable
            addMember.facate = self.facte
            addMember.famSettings = famSettings
            addMember.announcementSet = announcementSet
            addMember.postId                    = self.postId
            addMember.announcementId            = self.announcementId
            addMember.famArr = famArr
            addMember.memberJoining = self.memberJoining
            addMember.invitationStatus = self.invitationStatus
            addMember.isMembershipActive = self.isMembershipActive
            self.navigationController?.pushViewController(addMember, animated: false)
        }
        else if indexPath.row == 3{ // Events
            apiInviteEvents()
        }
        else if indexPath.row == 5{ // About us
            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
            intro.groupId = self.groupID
            intro.isAboutUsClicked = true
            self.navigationController?.pushViewController(intro, animated: false)
        }
        else if indexPath.row == 8{ // Members
            if isMembershipActive{
                if isAdmin.lowercased() == "admin"{
                    let storyboard = UIStoryboard.init(name: "MembershipMngmnt", bundle: nil)
                    let addMember = storyboard.instantiateViewController(withIdentifier: "MemebrshipDashboardViewController") as! MemebrshipDashboardViewController
                    addMember.groupID = self.groupID
                    addMember.isAdmin = self.isAdmin
                    addMember.link_type = self.link_type
                    addMember.islinkable = self.islinkable
                    //                        addMember.isFromdocument = true
                    addMember.facate = self.facte
                    addMember.famSettings = famSettings
                    addMember.announcementSet = announcementSet
                    addMember.postId                    = self.postId
                    addMember.announcementId            = self.announcementId
                    addMember.isMembershipActive = self.isMembershipActive
                    addMember.famArr = famArr
                    addMember.memberJoining = self.memberJoining
                    addMember.invitationStatus = self.invitationStatus
                    addMember.isMembershipActive = self.isMembershipActive
                    self.navigationController?.pushViewController(addMember, animated: true)
                }
                else{
                    membersClicked(self)
                }
            }
            else{
                membersClicked(self)
            }
            
        }
        else if indexPath.row == 9{
            membersClicked(self)
        }
        else if indexPath.row == 4{ // Album
            let AlbumListingViewController = self.storyboard?.instantiateViewController(withIdentifier: "AlbumListingViewController") as! AlbumListingViewController
            AlbumListingViewController.isAdmin = self.isAdmin
            AlbumListingViewController.link_type = self.link_type
            AlbumListingViewController.islinkable = self.islinkable
            AlbumListingViewController.groupID = self.groupID
            AlbumListingViewController.facate = self.facte
            AlbumListingViewController.famSettings = famSettings
            AlbumListingViewController.announcementSet = announcementSet
            AlbumListingViewController.postId                    = self.postId
            AlbumListingViewController.announcementId            = self.announcementId
            AlbumListingViewController.famArr = famArr
            AlbumListingViewController.memberJoining = self.memberJoining
            AlbumListingViewController.invitationStatus = self.invitationStatus
            AlbumListingViewController.isMembershipActive = self.isMembershipActive
            self.navigationController?.pushViewController(AlbumListingViewController, animated: false)
        }
        else if indexPath.row == 6{ // Documents
            let storyboard = UIStoryboard.init(name: "third", bundle: nil)
            let addMember = storyboard.instantiateViewController(withIdentifier: "DocumentListingViewController") as! DocumentListingViewController
            addMember.groupID = self.groupID
            addMember.isAdmin = self.isAdmin
            addMember.link_type = self.link_type
            addMember.islinkable = self.islinkable
            addMember.isFromdocument = true
            addMember.facate = self.facte
            addMember.famSettings = famSettings
            addMember.announcementSet = announcementSet
            addMember.postId                    = self.postId
            addMember.announcementId            = self.announcementId
            addMember.famArr = famArr
            addMember.memberJoining = self.memberJoining
            addMember.invitationStatus = self.invitationStatus
            addMember.isMembershipActive = self.isMembershipActive
            self.navigationController?.pushViewController(addMember, animated: false)
            //setActiveButton(index: 2)
        }
        else{ // Linked family
            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            let addMember = storyboard.instantiateViewController(withIdentifier: "LinkedFamiliesListViewController") as! LinkedFamiliesListViewController
            addMember.groupID = self.groupID
            addMember.isAdmin = self.isAdmin
            addMember.link_type = self.link_type
            addMember.islinkable = self.islinkable
            addMember.facate = self.facte
            addMember.famSettings = famSettings
            addMember.announcementSet = announcementSet
            addMember.postId                    = self.postId
            addMember.announcementId            = self.announcementId
            addMember.famArr = famArr
            addMember.memberJoining = self.memberJoining
            addMember.invitationStatus = self.invitationStatus
            addMember.isMembershipActive = self.isMembershipActive
            self.navigationController?.pushViewController(addMember, animated: false)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = aboutArr[indexPath.row].size(withAttributes: nil)
        return CGSize(width: w.width, height: 60)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
