//
//  CollageMakeViewController.swift
//  familheey
//
//  Created by familheey on 19/08/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON
import Kingfisher
import CollageView
import Firebase
import ContactsUI
import Contacts
import SafariServices
import SideMenu

class collageViewCollectionCell: UICollectionViewCell{
    @IBOutlet weak var imgCollageDisplay: UIImageView!
    @IBOutlet weak var imgVideoPlayIcon: UIImageView!
    
}

class CollageMakeViewController: UIViewController {
    @IBOutlet weak var clctnView: UICollectionView!
    @IBOutlet weak var collageSdkView: CollageView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblNoti_Count: UILabel!
    @IBOutlet weak var viewOfNoti_Count: UIView!
    
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    
    var offset = 0
    var limit = 300
    var notiUnreadCount:Int = 0
    var stopPagination = false
    
    var ArrayPosts = [JSON]()
    var ArrayWithOutTextPost = [JSON]()
    var arrayOfNotificationList = [[String:Any]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.callElasticSearchApiforPost()
        
        //        collageSdkView.dataSource = self
        //        collageSdkView.reload()
        
        if UserDefaults.standard.value(forKey: "userProfile") != nil{
            let pic = UserDefaults.standard.value(forKey: "userProfile") as! String
            let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+pic
            let imgUrl = URL(string: temp)
            let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+imgUrl!.relativeString
            let url = URL(string: newUrlStr)
            imgProfile.kf.indicatorType = .activity
            
            imgProfile.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        else{
            self.getUserDetails()
        }
        
        let right: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action:nil)
        right.cancelsTouchesInView = false
        right.direction = .right
        right.delegate = self
        view.addGestureRecognizer(right)
        setupRightView()
        NotificationCenter.default.addObserver(self, selector: #selector(childNodeAdded(notification:)), name: Notification.Name("ChildAdded"), object: nil)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.lblNoti_Count.isHidden = true
        self.viewOfNoti_Count.isHidden = true
        self.notificationCount()
        
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.isTranslucent = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isTranslucent = true
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Web API
    
    func callElasticSearchApiforPost(){
        
        if offset == 0{
            ActivityIndicatorView.show("Please wait....")
        }else{
            
        }
        
        self.stopPagination = false
        
        let param = ["user_id" : UserDefaults.standard.value(forKey: "userId") as! String,"offset":"\(offset)","limit":"\(limit)","index":"post"]
        
        networkProvider.request(.getElasticRecords(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    //                    self.isNewDataLoading = false
                    let jsonData =  JSON(response.data)
                    
                    if response.statusCode == 200{
                        ActivityIndicatorView.hiding()
                        self.ArrayPosts = jsonData["hits"].arrayValue
                        //
                        
                        for postData in self.ArrayPosts{
                            let post = postData["_source"]
                            if let attachmentArr = post["post_attachment"].array, attachmentArr.count > 0{
                                let attachment = attachmentArr[0]
                                if let type = attachment["type"].string ,type.contains("pdf") || type.contains("doc") || type.contains("docx") || type.contains("msword"){
                                }
                                else{
                                    self.ArrayWithOutTextPost.append(post)
                                }
                                
                            }
                            else{
                            }
                        }
//                        print("#####!!!! \(self.ArrayWithOutTextPost) - \(self.ArrayWithOutTextPost.count)")
                        self.clctnView.delegate = self
                        self.clctnView.dataSource = self
                        if let layout = self.clctnView.collectionViewLayout as? ColllageCollectionViewLayouts{
                            layout.delegate = self
                        }
                        self.clctnView.reloadData()
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.callElasticSearchApiforPost()
                        }
                    }
                    else{
                        if self.offset == 0{
                            ActivityIndicatorView.hiding()
                        }
                        else{
                        }
                    }
                    
                }catch _ {
                    if self.offset == 0{
                        ActivityIndicatorView.hiding()
                    }
                    else{
                        
                    }
                }
            case .failure( _):
                if self.offset == 0{
                    ActivityIndicatorView.hiding()
                }
                else{
                }                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    func notificationCount(){
        let userId = UserDefaults.standard.value(forKey: "userId") as! String
        
        let noti_String :String = "\(Helpers.notificationUploadstring)\(userId)_notification"

        let ref = Database.database().reference(withPath: noti_String).queryLimited(toLast: 1200)
        ref.observeSingleEvent(of: .value, with: { snapshot in
            
            if !snapshot.exists() {
                self.arrayOfNotificationList = []
                appDel.arrayOfNotificationList = []
                appDel.tempNotifi = []
                return
                
            }
            
            self.arrayOfNotificationList = []
            appDel.arrayOfNotificationList = []
            appDel.tempNotifi = []
            let dict:[String:Any] = snapshot.value as! [String:Any]
            
            for (key, value) in dict {
                var dictionary = [String:Any]()
                dictionary["key"] = key
                dictionary["value"] = value as! [String:Any]
                appDel.arrayOfNotificationList.append(dictionary)
                appDel.tempNotifi.append(value as! [String:Any])
            }
            
            //            appDel.arrayOfNotificationList = self.arrayOfNotificationList
            self.arrayOfNotificationList = appDel.arrayOfNotificationList
            
            self.notiUnreadCount = 0
            for data in self.arrayOfNotificationList{
                let dict  = data["value"] as! [String:Any]
                if dict["visible_status"] as! String == "unread"{
                    self.notiUnreadCount = self.notiUnreadCount + 1
                }
            }
            
            if self.notiUnreadCount == 0
            {
                self.lblNoti_Count.isHidden = true
                self.viewOfNoti_Count.isHidden = true
            }
            else{
                self.lblNoti_Count.isHidden = false
                self.viewOfNoti_Count.isHidden = false
                if self.notiUnreadCount > 99{
                    self.lblNoti_Count.text = "99+"
                }
                else{
                    self.lblNoti_Count.text = "\(self.notiUnreadCount)"
                }
            }
        })
    }
    
    func getUserDetails(){
        let crntUser = UserDefaults.standard.value(forKey: "userId") as! String
        ActivityIndicatorView.show("Please wait!")
        APIServiceManager.callServer.getUserDetails(url: EndPoint.userDetails, userId: crntUser, profileId: crntUser ,success: { (response) in
            
            ActivityIndicatorView.hiding()
            guard let loginMdl = response as? ExternalUserModel else{
                return
            }
            
            
            UserDefaults.standard.set(loginMdl.User?.fullname, forKey: "user_fullname")
            
            if loginMdl.User?.propic.count != 0 {
                UserDefaults.standard.set(loginMdl.User?.propic, forKey: "userProfile")
                let temp = "\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+(loginMdl.User!.propic)
                let imgUrl = URL(string: temp)
                
                let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+imgUrl!.relativeString
                let urlImg = URL(string: newUrlStr)
                self.imgProfile.kf.indicatorType = .activity
                self.imgProfile.kf.setImage(with: urlImg, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
            }
            else{
                self.imgProfile.image = #imageLiteral(resourceName: "Male Colored")
            }
            
            
            
        }) { (error) in
            ActivityIndicatorView.hiding()
            
        }
        
    }
    
    //MARK:- Button Actions
     @objc func childNodeAdded(notification: Notification) {
        self.notificationCount()
    }
    
    @IBAction func onClickProfileAction(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "UserProfileTableViewController") as! UserProfileTableViewController
        //vc.mutialFamilies =  "\(usersList[indexPath.row].familycount ?? "0")"
        //  vc.mutualConnections =  "\(usersList[indexPath.row].mutualfamilycount ?? "0")"
        var userId = UserDefaults.standard.value(forKey: "userId") as! String
        vc.userID = userId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickNotificationAction(_ sender: Any) {
        self.lblNoti_Count.isHidden = true
        self.viewOfNoti_Count.isHidden = true
        let storyboard = UIStoryboard.init(name: "third", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        vc.arrayOfNotificationList = self.arrayOfNotificationList
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onClickMenuAction(_ sender: Any) {
        let postoptionsTittle =  ["Calendar","Announcements","Messages","Feedback","Quick tour"]
        showActionSheet(titleArr: postoptionsTittle as NSArray, title: "Select options") { (index) in
            if index == 0{
                self.viewCalendar()
            }
            else if index == 1{
                self.viewAnnouncements()
            }
            else if index == 2{
                self.viewMessages()
            }
            else if index == 4{
                self.viewSubscription()
            }
            else if index == 100{
            }
            else if index == 3{
                self.sendFeedBack()
                
            }
        }
    }
    
    @IBAction func onClickSearchAction(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SearchTabViewController") as! SearchTabViewController
        //        vc.arrayOfNotificationList = self.arrayOfNotificationList
        if appDel.noFamily{
            vc.tabIndex = 100
        }
        else{
            vc.tabIndex = 103
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickSearchActions(_ sender: UIButton) {
        if sender.tag == 100{
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SearchTabViewController") as! SearchTabViewController
            //        vc.arrayOfNotificationList = self.arrayOfNotificationList
            vc.tabIndex = 100
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if sender.tag == 101{
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SearchTabViewController") as! SearchTabViewController
            //        vc.arrayOfNotificationList = self.arrayOfNotificationList
            vc.tabIndex = 101
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if sender.tag == 102{
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SearchTabViewController") as! SearchTabViewController
            //        vc.arrayOfNotificationList = self.arrayOfNotificationList
            vc.tabIndex = 102
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SearchTabViewController") as! SearchTabViewController
            //        vc.arrayOfNotificationList = self.arrayOfNotificationList
            vc.tabIndex = 103
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    //MARK:- Custom methods
    func setupRightView(){
        let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "topicListingViewController") as! topicListingViewController
        
        let rightMenuNavigationController = SideMenuNavigationController(rootViewController: vc)
        SideMenuManager.default.leftMenuNavigationController = rightMenuNavigationController
        SideMenuManager.default.addPanGestureToPresent(toView: self.view)
        SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: self.view)
        rightMenuNavigationController.statusBarEndAlpha = 0
        rightMenuNavigationController.menuWidth = self.view.frame.width
        rightMenuNavigationController.dismissOnPush = false
        rightMenuNavigationController.dismissOnPresent = false
        rightMenuNavigationController.pushStyle = .subMenu
    }
    
    func sendFeedBack(){
        let stryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = stryboard.instantiateViewController(withIdentifier: "FeedbackViewController") as! FeedbackViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func viewAnnouncements(){
        let storyboard = UIStoryboard.init(name: "fourth", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AnnouncementsViewController") as! AnnouncementsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func viewSubscription(){
        let stryboard = UIStoryboard.init(name: "Subscription", bundle: nil)
        let vc = stryboard.instantiateViewController(withIdentifier: "WalkthorughViewController") as! WalkthorughViewController
        appDel.quickTour = true
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func viewCalendar(){
        let stryboard = UIStoryboard.init(name: "third", bundle: nil)
        let vc = stryboard.instantiateViewController(withIdentifier: "CalendarEventsViewController") as! CalendarEventsViewController
        vc.isFromExplore = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func viewMessages(){
        let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "topicListingViewController") as! topicListingViewController
        
        let rightMenuNavigationController = SideMenuNavigationController(rootViewController: vc)
        SideMenuManager.default.leftMenuNavigationController = rightMenuNavigationController
        SideMenuManager.default.addPanGestureToPresent(toView: self.view)
        SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: self.view)
        rightMenuNavigationController.statusBarEndAlpha = 0
        rightMenuNavigationController.menuWidth = self.view.frame.width
        rightMenuNavigationController.dismissOnPush = false
        rightMenuNavigationController.dismissOnPresent = false
        rightMenuNavigationController.pushStyle = .subMenu
        present(rightMenuNavigationController, animated: true, completion: nil)
    }
    
     func resize(_ image: UIImage) -> UIImage {
        let actualHeight = Float(image.size.height)
        let actualWidth = Float(image.size.width)
        var maxHeight: Float = actualHeight
        
        let numberOfCellInRow  = 3
        let padding : Int      = 1
        let collectionCellWidth : CGFloat = (self.clctnView.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
        
        let maxWidth: Float = Float(collectionCellWidth)
        
        let imgRatio: Float = actualWidth / actualHeight
        let maxRatio: Float = maxWidth / maxHeight
        let compressionQuality: Float = 0.90
        
        
        if actualWidth > actualHeight{
            maxHeight = Float(collectionCellWidth / CGFloat(imgRatio) + 25)
        }
        else{
            maxHeight = Float(collectionCellWidth * CGFloat(imgRatio) + 50) 
        }
        
        //50 percent compression
       /* if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }*/
        
        
        let rect = CGRect(x: 0.0, y: 0.0, width: CGFloat(maxWidth), height: CGFloat(maxHeight))
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = img?.jpegData(compressionQuality: CGFloat(compressionQuality))
        UIGraphicsEndImageContext()
        return UIImage(data: imageData!) ?? UIImage()
    }
    
    /* func temp(){
     // import Foundation
     
     var semaphore = DispatchSemaphore (value: 0)
     
     let parameters = "{\n \"query\": {\n    \"match_all\": {\n        \"boost\":\"1.25\"\n\t}\n  }\n}"
     let postData = parameters.data(using: .utf8)
     
     var request = URLRequest(url: URL(string: "https://search-familheey-dev-ydtpjopnwdkyjnpjselq24hzyq.us-east-1.es.amazonaws.com/post")!,timeoutInterval: Double.infinity)
     request.addValue("application/json", forHTTPHeaderField: "Content-Type")
     request.addValue("beaead3198f7da1e70d03ab969765e0821b24fc913697e929e726aeaebf0eba3", forHTTPHeaderField: "X-Amz-Content-Sha256")
     request.addValue("20200821T110252Z", forHTTPHeaderField: "X-Amz-Date")
     request.addValue("AWS4-HMAC-SHA256 Credential=AKIA4BAV5EODMCZZKMET/20200821/us-east-1/es/aws4_request, SignedHeaders=host;x-amz-content-sha256;x-amz-date, Signature=182f09e4a37538362b302324fe7989760d103f98e0c98f41456a15ac9251b6c5", forHTTPHeaderField: "Authorization")
     
     request.httpMethod = "GET"
     request.httpBody = postData
     
     let task = URLSession.shared.dataTask(with: request) { data, response, error in
     guard let data = data else {
     print(String(describing: error))
     return
     }
     print(String(data: data, encoding: .utf8)!)
     semaphore.signal()
     }
     
     task.resume()
     semaphore.wait()
     
     }*/
    
}


extension CollageMakeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,CollageMakerLayoutDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ArrayWithOutTextPost.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collageViewCollectionCell", for: indexPath as IndexPath) as! collageViewCollectionCell
        
        let post = self.ArrayWithOutTextPost[indexPath.row]
        
        if let Attachment = post["post_attachment"].array, Attachment.count != 0{
            let attachment = Attachment[0]
            if let type = attachment["type"].string ,type.contains("image"){
                cell.imgVideoPlayIcon.isHidden = true
                var  temp  = ""
                temp = "\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+attachment["filename"].stringValue.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                
                let numberOfCellInRow  = 3
                let padding : Int      = 1
                let collectionCellWidth : CGFloat = (self.clctnView.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
                let height = attachment["height"].floatValue
                let width = attachment["width"].floatValue
                
                 //Helpers.imaginaryImageBaseUrl+"width=\(collectionCellWidth)&url="
                var imgurl = URL(string: BaseUrl.imaginaryURLForDetail2X + temp )
                
                if type.lowercased().contains("gif"){
                    imgurl = URL(string: temp )
                }
                
               /* if let imageData = try? Data(contentsOf: imgurl!) {

                    let img = UIImage.init(data: imageData)
                    let tempImg = self.resize(img!)
                    
                    cell.imgCollageDisplay.kf.indicatorType = .image(imageData: appDel.gifData)

                 //cell.imgCollageDisplay.frame.height

                    cell.imgCollageDisplay.image = tempImg
                }*/
//                 print("#####!!!! \(cell.frame.size.width) - \(cell.frame.size.height)")
                DispatchQueue.main.async {
                    cell.imgCollageDisplay.kf.indicatorType = .image(imageData: appDel.gifData)
//                    cell.imgCollageDisplay.kf.setImage(with: imgurl, placeholder:nil, options: nil)
                    if height > width{
                        cell.imgCollageDisplay.kf.setImage(with: imgurl, placeholder:#imageLiteral(resourceName: "url_imgSetDefault"), options: [.processor(
                            ResizingImageProcessor(referenceSize: CGSize(width: cell.frame.size.width, height: cell.frame.size.height), mode: .aspectFill)),
                                                                                                                                          .scaleFactor(UIScreen.main.scale),
                                                                                                                                          .transition(.fade(1)),
                                                                                                                                          .cacheOriginalImage
                        ])
                    }
                    else{
                        cell.imgCollageDisplay.kf.setImage(with: imgurl, placeholder:#imageLiteral(resourceName: "url_imgSetDefault"), options: [.processor(
                            ResizingImageProcessor(referenceSize: CGSize(width: cell.frame.size.width, height: cell.frame.size.height), mode: .aspectFit)),
                                                                                                                                          .scaleFactor(UIScreen.main.scale),
                                                                                                                                          .transition(.fade(1)),
                                                                                                                                          .cacheOriginalImage
                        ])
                    }


                }
            }
            else if  let type = attachment["type"].string ,type.contains("video"){
                cell.imgVideoPlayIcon.isHidden = false
                var temp = ""
                //"\(Helpers.imageURl)"+"default_video.jpg"
                
                if let thumb = attachment["video_thumb"].string, !thumb.isEmpty {
                    temp = "\(Helpers.imageURl)"+thumb
                    let imgurl = URL(string: BaseUrl.imaginaryURLForDetail2X + temp )
//                    print("#####!!!!@@@ \(cell.imgCollageDisplay.frame.width) - \(cell.imgCollageDisplay.frame.height)")
                    DispatchQueue.main.async {
                        cell.imgCollageDisplay.kf.indicatorType = .image(imageData: appDel.gifData)
                        cell.imgCollageDisplay.kf.setImage(with: imgurl, placeholder:#imageLiteral(resourceName: "url_imgSetDefault"), options: [.processor(
                            ResizingImageProcessor(referenceSize: CGSize(width: cell.frame.size.width, height: cell.frame.size.height), mode: .aspectFit)),
                                                                                                                                          .scaleFactor(UIScreen.main.scale),
                                                                                                                                          .transition(.fade(1)),
                                                                                                                                          .cacheOriginalImage
                        ])
//                        cell.imgCollageDisplay.kf.setImage(with: imgurl, placeholder:nil, options: nil)
                    }
                }
            }
        }
        else{
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let post = self.ArrayWithOutTextPost[indexPath.row]
        let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
        vc.postId = post["id"].stringValue
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let numberOfCellInRow  = 3
        let padding : Int      = 1
        let collectionCellWidth : CGFloat = (self.clctnView.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
        print(collectionCellWidth)
        return CGSize(width: collectionCellWidth, height: 120)
        
    }
    
    func collcectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        let post = self.ArrayWithOutTextPost[indexPath.row]
        
        if let Attachment = post["post_attachment"].array, Attachment.count != 0{
            let attachment = Attachment[0]
            
            let numberOfCellInRow  = 3
            let padding : Int      = 0
            let collectionCellWidth : CGFloat = (self.clctnView.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
            
            if let type = attachment["type"].string, type.contains("video"){
                var temp = ""
                if let thumb = attachment["video_thumb"].string, !thumb.isEmpty {
                    temp = "\(Helpers.imageURl)"+thumb
                    let imgurl = URL(string: BaseUrl.imaginaryURLForDetail2X + temp )
                    if let imageData = try? Data(contentsOf: imgurl!) {
                        
                        let img = UIImage.init(data: imageData)
                        let height = img?.size.height
                        let width = img?.size.width
                        let ratio = CGFloat(width!) / CGFloat(height!)
                        let vRatio = CGFloat(height!) / CGFloat(width!)
                        
                        if CGFloat(width!) > CGFloat(height!){
                            return collectionCellWidth / CGFloat(ratio)
                        }
                        else{
                            return collectionCellWidth * CGFloat(vRatio)
                        }
                    }
                     return 120
                }
                else{
                 return 120
                }
                   
            }
            else{
                let height = attachment["height"].floatValue
                let width = attachment["width"].floatValue
                
                let ratio = width / height
                let vRatio = height / width
                                
                if width > height{
                    return collectionCellWidth / CGFloat(ratio)
                }
                else{
//                    print("::::@@ \(vRatio)-- \(ratio)")
                    return collectionCellWidth * CGFloat(vRatio)
                }
                
//                if height > 600 && height < 800{
//                    return 200
//                }
//                else if height > 250 && height < 600{
//                    return 180
//                }
//                else if height > 800{
//                    return 220
//                }
                
//                return CGFloat(height)
            }
            
        }
        return 120.0
    }
    
}

extension CollageMakeViewController:UIGestureRecognizerDelegate{
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}
