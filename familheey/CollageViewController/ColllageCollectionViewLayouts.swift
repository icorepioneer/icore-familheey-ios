//
//  ColllageCollectionViewLayouts.swift
//  familheey
//
//  Created by familheey on 26/08/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit

protocol CollageMakerLayoutDelegate:AnyObject {
    func collcectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat
}

class ColllageCollectionViewLayouts: UICollectionViewLayout {

    weak var delegate: CollageMakerLayoutDelegate?
    
    private let numberOfColumns = 3
    private let cellPadding: CGFloat = 0
    
    private var cache: [UICollectionViewLayoutAttributes] = []
    
    private var contentHeight: CGFloat = 0
    private var contentWidth: CGFloat {
      guard let collectionView = collectionView else {
        return 0
      }
      let insets = collectionView.contentInset
      return collectionView.bounds.width - (insets.left + insets.right)
    }
    

    override var collectionViewContentSize: CGSize {
      return CGSize(width: contentWidth, height: contentHeight)
    }
    

    // Calculate instance of collectionview attribuites for every item in layout
    override func prepare() {
      guard
        cache.isEmpty,
        let collectionView = collectionView
        else {
          return
      }
      let columnWidth = contentWidth / CGFloat(numberOfColumns)
      var xOffset: [CGFloat] = []
      for column in 0..<numberOfColumns {
        xOffset.append(CGFloat(column) * columnWidth)
      }
      var column = 0
      var yOffset: [CGFloat] = .init(repeating: 0, count: numberOfColumns)
        
      for item in 0..<collectionView.numberOfItems(inSection: 0) {
        let indexPath = IndexPath(item: item, section: 0)
          
        var photoHeight = delegate?.collcectionView(collectionView, heightForPhotoAtIndexPath: indexPath) ?? 120
       
        if photoHeight > 0 {
        }
        else{
            photoHeight = 120.0
        }
         print("$$$$#####-----************** \(photoHeight)")
        
        let height = cellPadding * 15 + photoHeight
        let frame = CGRect(x: xOffset[column],
                           y: yOffset[column],
                           width: columnWidth,
                           height: height)
        let insetFrame = frame.insetBy(dx: cellPadding, dy: cellPadding)
          
        // 5
        let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
        attributes.frame = insetFrame
        cache.append(attributes)
          
        // 6
        contentHeight = max(contentHeight, frame.maxY)
        yOffset[column] = yOffset[column] + height
        
        column = column < (numberOfColumns - 1) ? (column + 1) : 0
      }
    }
    
    //To determine whichitem is visible in rectangle
    
    override func layoutAttributesForElements(in rect: CGRect)
        -> [UICollectionViewLayoutAttributes]? {
      var visibleLayoutAttributes: [UICollectionViewLayoutAttributes] = []
      
      // Loop through the cache and look for items in the rect
      for attributes in cache {
        if attributes.frame.intersects(rect) {
          visibleLayoutAttributes.append(attributes)
        }
      }
      return visibleLayoutAttributes
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath)
        -> UICollectionViewLayoutAttributes? {
      return cache[indexPath.item]
    }
    
}
