//
//  NotiFamilySelectTableViewCell.swift
//  familheey
//
//  Created by Innovation Incubator on 13/09/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit

class NotiFamilySelectTableViewCell: UITableViewCell {
    
    @IBOutlet weak var btnCheckMark: UIButton!
    @IBOutlet weak var imgCheckMark: UIImageView!
    @IBOutlet weak var lblCreatedBy: UILabel!
    @IBOutlet weak var ImgProfile: UIImageView!
    
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblFamilyName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
