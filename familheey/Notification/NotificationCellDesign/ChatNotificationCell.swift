//
//  ChatNotificationCell.swift
//  familheey
//
//  Created by Innovation Incubator on 21/09/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit

class ChatNotificationCell: UITableViewCell {
    @IBOutlet weak var viewOfGreen: UIView!
    @IBOutlet weak var btnThreedot: UIButton!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var imgOfPost: UIImageView!
    @IBOutlet weak var dotView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}


class TopicNotificationCell: UITableViewCell {
    @IBOutlet weak var viewOfGreen: UIView!
    @IBOutlet weak var btnThreedot: UIButton!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var imgOfPost: UIImageView!
    @IBOutlet weak var imgPropic: UIImageView!
    @IBOutlet weak var lblNsmr: UILabel!
    @IBOutlet weak var dotView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class PostNotificationCell: UITableViewCell {
    @IBOutlet weak var viewOfGreen: UIView!
    @IBOutlet weak var btnThreedot: UIButton!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var imgOfPost: UIImageView!
    @IBOutlet weak var imgPropic: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblGroupName: UILabel!
    @IBOutlet weak var dotView: UIView!
    
    @IBOutlet weak var heightOfDescription: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
