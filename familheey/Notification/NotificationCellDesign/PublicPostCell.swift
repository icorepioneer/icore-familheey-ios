//
//  PublicEventCell.swift
//  familheey
//
//  Created by Innovation Incubator on 17/09/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit

class PublicPostCell: UITableViewCell {

    
    @IBOutlet weak var lblLine: UILabel!
    @IBOutlet weak var dotView: UIView!
    @IBOutlet weak var heightOfLblTitle: NSLayoutConstraint!
    @IBOutlet weak var viewOfGreen: UIView!
    @IBOutlet weak var btnThreedot: UIButton!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblCreatorName: UILabel!
    @IBOutlet weak var imgOfCreator: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var imgOfPost: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
