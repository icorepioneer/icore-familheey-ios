//
//  NotificationRequestTableViewCell.swift
//  familheey
//
//  Created by Innovation Incubator on 16/09/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit

class NotificationRequestTableViewCell: UITableViewCell {
    
    @IBOutlet weak var heightOfLblTitle: NSLayoutConstraint!
       @IBOutlet weak var viewOfGreen: UIView!
       @IBOutlet weak var btnThreedot: UIButton!
       @IBOutlet weak var lblDate: UILabel!
       @IBOutlet weak var lblType: UILabel!
       @IBOutlet weak var lblCreatorName: UILabel!
       @IBOutlet weak var imgOfCreator: UIImageView!
       @IBOutlet weak var lblDescription: UILabel!
       @IBOutlet weak var lblMessage: UILabel!
       @IBOutlet weak var imgOfRequest: UIImageView!
    @IBOutlet weak var btnSupport: UIButton!
    @IBOutlet weak var heightOfBtn: NSLayoutConstraint!
    @IBOutlet weak var dotView: UIView!
    
    @IBOutlet weak var bottomOfBtnSupport: NSLayoutConstraint!
    @IBOutlet weak var lblLine: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
