//
//  PrivateEventViewCell.swift
//  familheey
//
//  Created by Innovation Incubator on 17/09/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit

class PrivateEventCell: UITableViewCell {

    @IBOutlet weak var topOfBtns: NSLayoutConstraint!
    
    @IBOutlet weak var imgLocation: UIImageView!
    @IBOutlet weak var heightOfBtnGoing: NSLayoutConstraint!
         @IBOutlet weak var viewOfGreen: UIView!
         @IBOutlet weak var btnThreedot: UIButton!
         @IBOutlet weak var lblDate: UILabel!
         @IBOutlet weak var lblCreatorName: UILabel!
         @IBOutlet weak var lblMessage: UILabel!
         @IBOutlet weak var imgOfEvent: UIImageView!
       
    @IBOutlet weak var dotView: UIView!
    @IBOutlet weak var imgOfCreator: UIImageView!
    @IBOutlet weak var lblCreatedDate: UILabel!
    @IBOutlet weak var btnGoing: UIButton!
       @IBOutlet weak var btnInterested: UIButton!
    @IBOutlet weak var btnNotInterested: UIButton!

       @IBOutlet weak var lblEventName: UILabel!
       @IBOutlet weak var lblLocation: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
