//
//  MemberJoinCell.swift
//  familheey
//
//  Created by Innovation Incubator on 17/09/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit

class MemberJoinCell: UITableViewCell {

    @IBOutlet weak var topOfBtnStack: NSLayoutConstraint!
    @IBOutlet weak var heightOfBtnJoin: NSLayoutConstraint!
      @IBOutlet weak var viewOfGreen: UIView!
      @IBOutlet weak var btnThreedot: UIButton!
      @IBOutlet weak var lblDate: UILabel!
      @IBOutlet weak var lblCreatorName: UILabel!
      @IBOutlet weak var lblMessage: UILabel!
      @IBOutlet weak var imgOfPost: UIImageView!
    @IBOutlet weak var dotView: UIView!
    
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnJoin: UIButton!
    @IBOutlet weak var lblMemberHeading: UILabel!
    @IBOutlet weak var lblMemberCount: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
