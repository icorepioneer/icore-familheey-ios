//
//  NotifSelectFamilyViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 12/09/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit

protocol FamilySelectionDelegate {
    func selctionCallback(SelectedGroupId:NSMutableArray)
}


class NotifSelectFamilyViewController: UIViewController {
    var selectionDelegate:FamilySelectionDelegate!
    var selectedGroupIDArr = NSMutableArray()
    var selectAll = false
    @IBOutlet weak var btnSelectAll: UIButton!
    @IBOutlet weak var lblSelectionCount: UILabel!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearchReset: UIButton!
    @IBOutlet weak var tblListView: UITableView!
    @IBOutlet weak var noFamilyView: UIView!
    @IBOutlet weak var btnDone: UIButton!
    
    var FamilyArr : familyListResponse!
    var searchTxt = ""
    weak var delegate: selectFamilyDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        print(selectedGroupIDArr)
        //        self.tblListView.register(UITableViewCell.self, forCellReuseIdentifier: "NotiFamilySelectTableViewCell")
        btnSelectAll.borderWidth = 0.5
        btnSelectAll.backgroundColor = UIColor.clear
        btnSelectAll.borderColor = .lightGray
        
        btnSelectAll.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        btnSelectAll.setTitle("Select All", for: .normal)
        //                                                btnSelectAll.borderColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
        //                                                btnSelectAll.setTitleColor(#colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1), for: .normal)
        btnSelectAll.setTitleColor(.darkGray, for: .normal)
        
        
        
        txtSearch.delegate = self
        Helpers.setleftView(textfield: txtSearch, customWidth: 30)
        getFamilyList()
        
        // Do any additional setup after loading the view.
    }
    //MARK:- Web API
    func getFamilyList(){
        let params = [
            "user_id":UserDefaults.standard.value(forKey: "userId") as! String,
            "query":self.txtSearch.text!
        ]
        APIServiceManager.callServer.getFamilyList(url: EndPoint.viewFamily, params: params,  success: { (responseMdl) in
            guard let familyMdl = responseMdl as? familyListResponse else{
                return
            }
            ActivityIndicatorView.hiding()
            if familyMdl.statusCode == 200{
                print(familyMdl.familyList!)
                //                    if self.txtSearch.text!.count > 0{
                //                        if familyMdl.familyList!.count > 0{
                //                            self.FamilyArr = familyMdl
                ////                            self.searchBar.isHidden = false
                //                            self.btnAdd.isHidden = false
                //                            self.tblListView.isHidden = false
                //                            self.bg_noFamilyView.isHidden = true
                //                            self.tblListView.delegate = self
                //                            self.tblListView.dataSource = self
                //
                //                            self.tblListView.reloadData()
                //                        }
                //                        else{
                //                        }
                //                    }
                //                    else{
                if familyMdl.familyList!.count > 0{
                    self.FamilyArr = familyMdl
                    self.lblSelectionCount.text = "\(familyMdl.familyList!.count - self.selectedGroupIDArr.count) selected"
                    if self.selectedGroupIDArr.count == 0
                    {
                        self.selectAll = true
                        
                        self.btnSelectAll.borderColor = .clear
                        self.btnSelectAll.setTitleColor(.white, for: .normal)
                        self.btnSelectAll.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                        self.btnSelectAll.setTitle("Deselect All", for: .normal)
                    }
                    else
                    {
                        self.selectAll = false
                        self.btnSelectAll.setTitleColor(.darkGray, for: .normal)
                        self.btnSelectAll.setTitle("Select All", for: .normal)
                        self.btnSelectAll.backgroundColor = UIColor.clear
                        self.btnSelectAll.borderColor = .lightGray
                        
                        
                    }
                    
                    
                    self.btnDone.isHidden = false
                    self.tblListView.isHidden = false
                    self.tblListView.delegate = self
                    self.tblListView.dataSource = self
                    self.tblListView.reloadData()
                }
                else{
                    self.noFamilyView.isHidden = false
                    self.tblListView.isHidden = true
                    self.btnDone.isHidden = true
                }
                //                    }
                
            }
            else{
                self.displayAlert(alertStr: "Something went wrong! Please try again later", title: "Error")
                ActivityIndicatorView.hiding()
            }
            
        }) { (error) in
            ActivityIndicatorView.hiding()
            self.displayAlert(alertStr: error!.description, title: "Error")
        }
    }
    
    
    //MARK:- UIButton Actions
    @IBAction func onClickSearchReset(_ sender: Any) {
        txtSearch.text = ""
        searchTxt = ""
        getFamilyList()
        btnSearchReset.isHidden = true
        self.txtSearch.endEditing(true)
    }
    
    //       @IBAction func onClickViewFamily(_ sender: UIButton){
    //           var grpId = ""
    //
    //           grpId = "\(FamilyArr.familyList![sender.tag].faId)"
    //           let grpName = FamilyArr.familyList![sender.tag].faName
    //
    //           delegate?.selectFamilyId(groupId: grpId, grpName: grpName)
    //           self.navigationController?.popViewController(animated: true)
    //       }
    
    @IBAction func onClikBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
        //           self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSelectAll_OnClick(_ sender: Any) {
        
        //         header.btnAction.borderWidth = 1
        //                       header.btnAction.borderColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
        //                       header.btnAction.setTitleColor(#colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1), for: .normal)
        //                       header.btnAction.backgroundColor = UIColor.clear
        //                       header.btnAction.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        //                       header.btnAction.setTitle("Select", for: .normal)
        if self.selectAll
        {
            self.selectAll = false
            selectedGroupIDArr.removeAllObjects()
            btnSelectAll.setTitleColor(.darkGray, for: .normal)
            btnSelectAll.setTitle("Select All", for: .normal)
            
            btnSelectAll.borderColor = .lightGray
            //                                          btnSelectAll.setTitleColor(#colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1), for: .normal)
            btnSelectAll.backgroundColor = UIColor.clear
            for i in 0 ... self.FamilyArr.familyList!.count-1
            {
                let grpId = FamilyArr.familyList![i].faId
                
                selectedGroupIDArr.add(grpId)
                
            }
            
        }
        else
        {
            self.selectAll = true
            
            btnSelectAll.borderColor = .clear
            btnSelectAll.setTitleColor(.white, for: .normal)
            btnSelectAll.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            btnSelectAll.setTitle("Deselect All", for: .normal)
            selectedGroupIDArr.removeAllObjects()
            
            //            for i in 0 ... self.FamilyArr.familyList!.count-1
            //            {
            //                let grpId = FamilyArr.familyList![i].faId
            //
            //                                  selectedGroupIDArr.add(grpId)
            //
            //            }
            
        }
        self.lblSelectionCount.text = "\(self.FamilyArr.familyList!.count - self.selectedGroupIDArr.count) selected"
        
        tblListView.reloadData()
        
    }
    
    @IBAction func btnDoneClicked(_ sender: Any) {
        //        if self.selectedGroupIDArr.count == 0
        //        {
        //        Helpers.showAlertDialog(message: "Please select atleast one family", target: self)
        //        }
        //        else
        //  {
        self.selectionDelegate.selctionCallback(SelectedGroupId:self.selectedGroupIDArr)
        self.dismiss(animated: true, completion: nil)
        
        //        }
        
    }
    @IBAction func onClickTouchOutSide(_ sender: Any) {
        //        self.selectionDelegate.selctionCallback(SelectedGroupId:self.selectedGroupIDArr)
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    
    @IBAction func btnCheckOnClick(_ sender: UIButton) {
        
        let grpId = self.FamilyArr.familyList![sender.tag].faId
        
        if selectedGroupIDArr.contains(grpId){
            selectedGroupIDArr.remove(grpId)
        }
        else{
            selectedGroupIDArr.add(grpId)
        }
        
        if selectedGroupIDArr.count == 0
        {
            self.selectAll = true
            
            btnSelectAll.borderColor = .clear
            btnSelectAll.setTitleColor(.white, for: .normal)
            btnSelectAll.backgroundColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            btnSelectAll.setTitle("Deselect All", for: .normal)
        }
        else
        {
            //            btnSelectAll.borderColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            btnSelectAll.borderColor = .lightGray
            
            btnSelectAll.setTitleColor(.darkGray, for: .normal)
            btnSelectAll.backgroundColor = UIColor.clear
            self.selectAll = false
            btnSelectAll.setTitle("Select All", for: .normal)
            
        }
        tblListView.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .none)
        self.lblSelectionCount.text = "\(self.FamilyArr.familyList!.count - self.selectedGroupIDArr.count) selected"
        
        //                  tblListView.reloadSections(IndexSet(integer : sender.tag), with: .none)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension NotifSelectFamilyViewController:UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // return FamilyArr.familyList!.count
        return FamilyArr.familyList!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //        @IBOutlet weak var btnCheckMark: UIButton!
        //           @IBOutlet weak var imgCheckMark: UIImageView!
        //           @IBOutlet weak var lblCreatedBy: UILabel!
        //           @IBOutlet weak var ImgProfile: UIImageView!
        //
        //           @IBOutlet weak var lblLocation: UILabel!
        //           @IBOutlet weak var lblFamilyName: UILabel!
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotiFamilySelectTableViewCell", for: indexPath) as! NotiFamilySelectTableViewCell
        
        cell.selectionStyle = .none
        cell.btnCheckMark.tag = indexPath.row
        cell.lblFamilyName?.text = FamilyArr.familyList![indexPath.row].faName
        cell.lblCreatedBy.text = "By \(FamilyArr.familyList![indexPath.row].faAdmin)"
        cell.lblLocation.text = FamilyArr.familyList![indexPath.row].faRegion
        
        
        
        if FamilyArr.familyList![indexPath.row].faLogo.count != 0{
            let temp = "\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+FamilyArr.familyList![indexPath.row].faLogo
            let imgurl = URL(string: temp)
            let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+imgurl!.relativeString
            let url = URL(string: newUrlStr)
            cell.ImgProfile.kf.indicatorType = .activity
            
            cell.ImgProfile.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "Family Logo"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        else{
            cell.ImgProfile.image = #imageLiteral(resourceName: "Family Logo")
        }
        //        cell.imgCheckMark.image = nil
        print(selectedGroupIDArr)
        let grpId = FamilyArr.familyList![indexPath.row].faId
        print(grpId)
        print(grpId)
        if !selectedGroupIDArr.contains(grpId){
            
            cell.imgCheckMark.image = #imageLiteral(resourceName: "Green_tick.png")
            
            //                    cell.imgCheckMark.backgroundColor = .green
            //                       header.btnAction.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
            //                       header.btnAction.setTitle("Selected", for: .normal)
            
        }else{
            cell.imgCheckMark.image = #imageLiteral(resourceName: "gray_circle.png")
            //                    cell.imgCheckMark.backgroundColor = .red
            
            //                       header.btnAction.borderWidth = 1
            //                       header.btnAction.borderColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
            //                       header.btnAction.setTitleColor(#colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1), for: .normal)
            //                       header.btnAction.backgroundColor = UIColor.clear
            //                       header.btnAction.titleLabel?.font = UIFont.systemFont(ofSize: 15)
            //                       header.btnAction.setTitle("Select", for: .normal)
        }
        
        return cell
        //        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
        
    }
    
    //    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    //
    //
    //        let header = self.tblListView.dequeueReusableHeaderFooterView(withIdentifier: "FamilyListingView") as! FamilyListingView
    //
    //        header.btnAction.tag = section
    //        header.btnView.tag = section
    //
    //        header.byTitle.text = "By"
    //        header.lblTitle.text = FamilyArr.familyList![section].faName
    //        header.lblType.text = FamilyArr.familyList![section].faCategory
    //        header.lblRegion.text = FamilyArr.familyList![section].faRegion
    //        header.lblCreatedBy.text = "\(FamilyArr.familyList![section].faAdmin)"
    //       // header.lblMembersCount.text = "\(FamilyArr.familyList![section].memberCount)"
    //
    //
    //        if FamilyArr.familyList![section].faLogo.count != 0{
    //            let temp = "\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+FamilyArr.familyList![section].faLogo
    //            let imgurl = URL(string: temp)
    //            let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+imgurl!.relativeString
    //            let url = URL(string: newUrlStr)
    //            header.imgLogo.kf.indicatorType = .activity
    //
    //            header.imgLogo.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
    //        }
    //        else{
    //            header.imgLogo.image = #imageLiteral(resourceName: "Family Logo")
    //        }
    //
    //         header.btnAction.isHidden = true
    //         header.btnView.addTarget(self, action: #selector(onClickViewFamily(_:)), for: .touchUpInside)
    //
    //         header.lblKnownHead.text = "posts"
    //
    //        let knownCount = FamilyArr.familyList![section].memberCount!
    //
    //        if Int(knownCount)! > 0{
    //            header.lblMembersCount.text = FamilyArr.familyList![section].memberCount
    //            header.lblMemberHead.isHidden = false
    //            header.lblMembersCount.isHidden = false
    //        }
    //        else{
    //            header.lblMemberHead.isHidden = true
    //            header.lblMembersCount.isHidden = true
    //        }
    //
    //        let eventCount = FamilyArr.familyList![section].knownMemberCount
    //
    //        if Int(eventCount)! > 0{
    //            header.lblKnown.text = FamilyArr.familyList![section].knownMemberCount
    //            header.lblKnownHead.isHidden = false
    //            header.lblKnown.isHidden = false
    //        }
    //        else{
    //            header.lblKnownHead.isHidden = true
    //            header.lblKnown.isHidden = true
    //        }
    //
    //        return header
    //
    //    }
    
    //    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    //        return 140
    //    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    
    /* func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     if FamilyArr.familyList![indexPath.row].isBlocked == 1{
     self.displayAlert(alertStr: "You are corrently blocked from this group", title: "")
     }
     else{
     let stryboard = UIStoryboard.init(name: "second", bundle: nil)
     
     let family = stryboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
     let temp = FamilyArr.familyList![indexPath.row].faId
     family.groupId = String(temp)
     self.navigationController?.pushViewController(family, animated: true)
     }
     }*/
}
extension NotifSelectFamilyViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        textField.resignFirstResponder()
        if textField == txtSearch
        {
            // selectWebAPI()
            if textField.text!.count > 0{
                btnSearchReset.isHidden = false
            }
            else{
                btnSearchReset.isHidden = true
            }
            textField.endEditing(true)
        }
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        self.view.endEditing(true)
        if textField == txtSearch
        {
            if textField.text!.count > 0{
                btnSearchReset.isHidden = false
            }
            else{
                btnSearchReset.isHidden = true
            }
            getFamilyList()
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
            // selectWebAPI()
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text!.count > 0 {
            btnSearchReset.isHidden = false
        }
        else{
            btnSearchReset.isHidden = true
        }
        return true
    }
}
