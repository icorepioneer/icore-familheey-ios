//
//  NotificationTableViewCell.swift
//  familheey
//
//  Created by Innovation Incubator on 31/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var btnThreeDot: UIButton!
    @IBOutlet weak var viewOfThreedot: UIView!
    @IBOutlet weak var lblThird: UILabel!
    @IBOutlet weak var lblSecond: UILabel!
    @IBOutlet weak var lblFirst: UILabel!
    @IBOutlet weak var viewOfGreen: UIView!
    @IBOutlet weak var imgViewUser: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
