//
//  NotificationViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 30/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import Firebase
import Moya
import SwiftyJSON

class NotificationViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var arrayOfNotificationList = [[String:Any]]()
    var tempNotifi = [[String:Any]]()
    var ref: DatabaseReference!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var imgViewNoData: UIImageView!
    @IBOutlet weak var viewOfNoData: UIView!
    @IBOutlet weak var tblviewNotificationList: UITableView!
    var networkProvider = MoyaProvider<FamilyheeyApi>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if appDel.isFromNotification{
            handleNavigations()
        }
        
        self.tabBarController?.tabBar.isHidden = true
        
        self.tblviewNotificationList.rowHeight = UITableView.automaticDimension
        self.tblviewNotificationList.estimatedRowHeight = 80;
        //        self.tblviewNotificationList.contentInset = UIEdgeInsets(top: -35, left: 0, bottom: 0, right: 0);
        self.tblviewNotificationList.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0);
        
        //        updateUI()        
        self.arrayOfNotificationList = appDel.arrayOfNotificationList
       // self.arrayOfNotificationList = self.arrayOfNotificationList.filter { }
        
        
        if self.arrayOfNotificationList.count > 0 {
            //            self.arrayOfNotificationList = appDel.arrayOfNotificationList
            //            // // print(self.arrayOfNotificationList)
            
            self.arrayOfNotificationList.sort{
               
                    (($0["value"] as! Dictionary<String, AnyObject>)["create_time"] as! String) > (($1["value"] as! Dictionary<String, AnyObject>)["create_time"] as! String)
                }
                
            
            //         // // print(self.arrayOfNotificationList[0])
            //   // // print(self.arrayOfNotificationList[1])
            
            self.tempNotifi = appDel.tempNotifi
            self.tempNotifi.sort{ ($0["create_time"] as! String) > ($1["create_time"] as! String)}
            // // print(self.tempNotifi[0])
            //   // // print(self.tempNotifi[1])
            
            
            
            self.updateUI()
            //            self.tblviewNotificationList.reloadData()
        }
        else{
            getNotifications()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tblviewNotificationList.reloadData()
        self.navigationController?.navigationBar.isHidden = true
        //        if appDel.fromNavigationBack
        //        {
        //        getNotifications()
        //            appDel.fromNavigationBack = false
        //        }
        
    }
    
    //MARK:- Custom and button actions
    
    func updateUI(){
        if self.arrayOfNotificationList.count == 0{
            self.tblviewNotificationList.isHidden = true
            self.viewOfNoData.isHidden = false
            self.imgViewNoData.isHidden = false
            self.lblNoData.isHidden = false
            if appDel.notification_auto_delete
            {
                self.lblNoData.text = "All your notifications are deleted by the system! You will receive upcoming notifications!"
            }
            else
            {
                self.lblNoData.text = "No notification to show"
            }
            self.tabBarController?.tabBar.isHidden = true
            //   self.deleteButton.isHidden = true
            
        }
        else{
            self.viewOfNoData.isHidden = true
            self.imgViewNoData.isHidden = true
            self.lblNoData.isHidden = true
            //  self.deleteButton.isHidden = false
            self.tblviewNotificationList.isHidden = false
            self.tblviewNotificationList.dataSource = self
            self.tblviewNotificationList.delegate = self
            self.tblviewNotificationList.reloadData()
        }
    }
    
    func getNotifications(){
        self.arrayOfNotificationList = []
        ActivityIndicatorView.show("Please wait....")
        
        let userId = UserDefaults.standard.value(forKey: "userId") as! String
        var dbHandleQuery: DatabaseHandle?
        let noti_String :String = "\(Helpers.notificationUploadstring)\(userId)_notification"
        let ref = Database.database().reference(withPath: noti_String).queryLimited(toLast: 1200)
        ref.observeSingleEvent(of: .value, with: { snapshot in
            ActivityIndicatorView.hiding()
            
            if !snapshot.exists() {
                self.updateUI()
                return
            }
            
            let dict:[String:Any] = snapshot.value as! [String:Any]
            
            for (key, value) in dict {
                var dictionary = [String:Any]()
                dictionary["key"] = key
                dictionary["value"] = value as! [String:Any]
                self.arrayOfNotificationList.append(dictionary)
                self.tempNotifi.append(value as! [String:Any])
            }
            
            appDel.arrayOfNotificationList =  self.arrayOfNotificationList
            //            // // print(self.arrayOfNotificationList)
            
            self.arrayOfNotificationList.sort{
                (($0["value"] as! Dictionary<String, AnyObject>)["create_time"] as! String) > (($1["value"] as! Dictionary<String, AnyObject>)["create_time"] as! String)
            }
            
            // // print(self.arrayOfNotificationList[0])
            
            self.tempNotifi.sort{ ($0["create_time"] as! String) > ($1["create_time"] as! String) }
            
            // // print(self.tempNotifi[0])
            
            self.updateUI()
            //           self.tblviewNotificationList.reloadData()
            ActivityIndicatorView.hiding()
        })
    }
    
    func changeReadStatus(){
        self.tempNotifi.removeAll()
        for (index,currentNotification) in self.arrayOfNotificationList.enumerated()
        {
            let currentData = (currentNotification["value"]) as! [String:Any]
            
            let currentKey = (currentNotification["key"]) as! String
            let userId = UserDefaults.standard.value(forKey: "userId") as! String
            var dictOld = currentData
            
            if let dictNew = dictOld.updateValue("read", forKey: "visible_status") {
                print(dictOld)
                
            }
            
            var dictionary = [String:Any]()
            dictionary["key"] = currentKey
            dictionary["value"] = dictOld
            
            self.tempNotifi.append(dictionary)
            
            let noti_String :String = "\(Helpers.notificationUploadstring)\(userId)_notification"
            //        let noti_String :String = "\(userId)_notification"
            //        "36_notification"
            let ref = Database.database().reference(withPath: noti_String)
            ref.child(currentKey).setValue(dictOld)
            
        }
        self.arrayOfNotificationList.removeAll()
        self.arrayOfNotificationList = tempNotifi
        appDel.arrayOfNotificationList = self.arrayOfNotificationList
        updateUI()
        //        getNotifications()
        
    }
    
    func changeReadStatusSingle(selectedData:[String:Any],index:Int){
        let currentNotification = selectedData
        print(currentNotification)
        
        let currentData = (currentNotification["value"]) as! [String:Any]
        let currentKey = (currentNotification["key"]) as! String
        
        let userId = UserDefaults.standard.value(forKey: "userId") as! String
        var dictOld = currentData
        
        print(currentKey)
        print(dictOld)
        if let dictNew = dictOld.updateValue("read", forKey: "visible_status") {
            
        }
        
        var dictionary = [String:Any]()
        dictionary["key"] = currentKey
        dictionary["value"] = dictOld
        print(currentKey)
        print(dictOld)
        print("first:",self.arrayOfNotificationList[index])
        self.arrayOfNotificationList.remove(at: index)
        self.arrayOfNotificationList.insert(dictionary, at: index)
        print("second:",self.arrayOfNotificationList[index])
        appDel.arrayOfNotificationList = self.arrayOfNotificationList
        
        let noti_String :String = "\(Helpers.notificationUploadstring)\(userId)_notification"
        //        let noti_String :String = "\(userId)_notification"
        //        "36_notification"
        let ref = Database.database().reference(withPath: noti_String)
        ref.child(currentKey).setValue(dictOld)
        
//        updateUI()
                getNotifications()
        
    }
    
    @IBAction func backClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func deleteNotificationAction(_ sender: Any) {
        let alert = UIAlertController(title: "", message: "Do you want to clear all notifications?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "YES", style: .destructive, handler: { (acion) in
            self.deleteNotifications()
        }))
        
        alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    func deleteNotifications(){
        
        
        if let id =  UserDefaults.standard.value(forKey: "userId") as? String{
            ActivityIndicatorView.show("Please wait....")
            
            let param = ["user_id":id] as [String : Any]
            networkProvider.request(.deleteNotifications(parameter: param)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                        // // print("view contents : \(jsonData)")
                        if response.statusCode == 200{
                            self.arrayOfNotificationList.removeAll()
                            appDel.arrayOfNotificationList = self.arrayOfNotificationList 
                            self.updateUI()
                        }
                        else if response.statusCode == 401 {
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                self.deleteNotifications()
                            }
                        }
                        else{
                            self.displayAlert(alertStr: "Something went wrong, Please try again", title: "")
                        }
                        ActivityIndicatorView.hiding()
                        
                    }catch let err {
                        ActivityIndicatorView.hiding()
                        // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                    }
                case .failure( let error):
                    ActivityIndicatorView.hiding()
                    //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                    break
                }
            }
            
        }
        
    }
    func allReadNotifications(){
        
        
        if let id =  UserDefaults.standard.value(forKey: "userId") as? String{
            ActivityIndicatorView.show("Please wait....")
            
            let param = ["user_id":id] as [String : Any]
            networkProvider.request(.readAllNotifications(parameter: param)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                        // // print("view contents : \(jsonData)")
                        if response.statusCode == 200{
                            self.getNotifications()

//                            self.updateUI()
                        }
                        else if response.statusCode == 401 {
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                self.deleteNotifications()
                            }
                        }
                        else{
                            self.displayAlert(alertStr: "Something went wrong, Please try again", title: "")
                        }
                        ActivityIndicatorView.hiding()
                        
                    }catch let err {
                        ActivityIndicatorView.hiding()
                        // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                    }
                case .failure( let error):
                    ActivityIndicatorView.hiding()
                    //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                    break
                }
            }
            
        }
        
    }
    func deleteSingleNotifications(index:Int){
        
        let currentNotification = self.arrayOfNotificationList[index]
        
        
        if let id =  UserDefaults.standard.value(forKey: "userId") as? String{
            ActivityIndicatorView.show("Please wait....")
            let param = ["user_id":id,"notification_id":currentNotification["key"] as! String] as [String : Any]
            // print(param)
            networkProvider.request(.deleteSingleNotifications(parameter: param)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                        if response.statusCode == 200{
                            if self.arrayOfNotificationList.count == 1{
                                appDel.notification_auto_delete = false
                                self.switchNotifications()
                                self.arrayOfNotificationList.remove(at: index)
                                appDel.arrayOfNotificationList = self.arrayOfNotificationList

                            }

                            else{
                                //                                self.getNotifications()
                                self.arrayOfNotificationList.remove(at: index)
                                appDel.arrayOfNotificationList = self.arrayOfNotificationList
                                self.updateUI()
                            }
                        }
                            else if response.statusCode == 401 {
                                Helpers.getAccessToken { (accessToken) in
                                    setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                    self.deleteSingleNotifications(index: index)
                                }
                            }
                        else{
                            self.displayAlert(alertStr: "Something went wrong, Please try again", title: "")
                        }
                        ActivityIndicatorView.hiding()
                        
                    }catch let err {
                        ActivityIndicatorView.hiding()
                        // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                    }
                case .failure( let error):
                    ActivityIndicatorView.hiding()
                    //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                    break
                }
            }
            
        }
        
    }
    
    
    func switchNotifications(){
        
        
        if let id =  UserDefaults.standard.value(forKey: "userId") as? String{
            ActivityIndicatorView.show("Please wait....")
            var param = [String : Any]()
            
            param = ["id":id,"notification_auto_delete":false] as [String : Any]
            
            networkProvider.request(.editProfile(parameter: param)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                        // print("view contents : \(jsonData)")
                        if response.statusCode == 200{
                            //                            self.getNotifications()
                            self.updateUI()
                        }
                        else if response.statusCode == 401 {
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                self.switchNotifications()
                            }
                        }
                        else{
                            self.displayAlert(alertStr: "Something went wrong, Please try again", title: "")
                        }
                        ActivityIndicatorView.hiding()
                        
                    }catch let err {
                        ActivityIndicatorView.hiding()
                        // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                    }
                case .failure( let error):
                    ActivityIndicatorView.hiding()
                    //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                    break
                }
            }
            
        }
        
    }
    @IBAction func onClickThreeDot(_ sender: Any) {
        //        postoptionsTittle.removeAll()
        //        postoptionsTittle =  ["Send feedback","View announcement","Calendar"]
        
        var postoptionsTittle = [String]()
        if self.arrayOfNotificationList.count == 0{
            postoptionsTittle = ["Notification Settings","Mark all read"]
        }
        else
        {
            postoptionsTittle = ["Notification Settings","Mark all read","Delete All"]
            
        }
        //        if UserDefaults.standard.value(forKey: "notification") as! Bool
        //        {
        //            postoptionsTittle = ["Delete All","Off Notification"]
        //        }
        //        else
        //        {
        //            postoptionsTittle = ["Delete All","On Notification"]
        //        }
        
        
        //        postoptionsTittle =  ["Delete All","On Notification"]
        
        
        showActionSheet(titleArr: postoptionsTittle as NSArray, title: "Select options") { (index) in
            if index == 2 {
                let alert = UIAlertController(title: "", message: "Do you want to clear all notifications?", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "YES", style: .destructive, handler: { (acion) in
                    self.deleteNotifications()
                }))
                
                alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
                
                self.present(alert, animated: true, completion: nil)            }
            else if index == 0 {
                let storyboard = UIStoryboard.init(name: "NotificationSetting", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "NotificationSettingViewController") as! NotificationSettingViewController
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if index == 1 {
                self.allReadNotifications()
//                self.changeReadStatus()
                
            }
            else
            {
                //                self.switchNotifications()
            }
            
        }
    }
    //MARK: - Tableview datasource and delegate methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayOfNotificationList.count
    }
    
    /* func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
     if self.arrayOfNotificationList.count == 0{
     return UIView.init()
     }
     let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 35))
     headerView.backgroundColor = .clear
     let label = UILabel()
     label.frame = CGRect.init(x: 20, y: 5, width: headerView.frame.width-30, height: headerView.frame.height-10)
     
     //            let dateAsString               = self.arrayOfFullAgenda[section]["date"].stringValue
     //            let dateFormatter              = DateFormatter()
     //            dateFormatter.dateFormat       = "yyyy-MM-dd"
     //
     //            let date = dateFormatter.date(from: dateAsString)
     //            // print(date as Any)
     //            dateFormatter.dateFormat       = "EEE dd MMM,yyy"
     //            let DateFormatted = dateFormatter.string(from: date!)
     //            // print(DateFormatted)
     //
     //
     //            var myMutableString = NSMutableAttributedString()
     //            myMutableString = NSMutableAttributedString(string: DateFormatted as String, attributes: [NSAttributedString.Key.font:UIFont.boldSystemFont(ofSize: 18)])
     //            myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value:#colorLiteral(red: 0.2274509804, green: 0.1333333333, blue: 0.3843137255, alpha: 1) , range: NSRange(location:0,length:3))
     //
     //
     //            myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.darkGray, range: NSRange(location:3,length:DateFormatted.count-3))
     //            myMutableString.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 18), range: NSRange(location:3,length:DateFormatted.count-3))
     //
     //
     //            label.attributedText = myMutableString
     if self.arrayOfNotificationList.count == 1{
     label.text = "\(self.arrayOfNotificationList.count) Notification"
     
     }
     else{
     label.text = "\(self.arrayOfNotificationList.count) Notifications"
     
     }
     label.font = .systemFont(ofSize: 14.0)
     
     headerView.addSubview(label)
     
     return headerView
     
     }*/
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.arrayOfNotificationList.count == 0{
            return 0
        }
        return 20
    }
    
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //
    //            return 0
    //
    //    }
    func formatDateString(dateStr:String) -> String{
        
        if !dateStr.isEmpty{
            let dateFormatter              = DateFormatter()
            dateFormatter.dateFormat       = "yyyy-MM-dd'T'HH:mm:ss'Z'"
            dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
            let date = dateFormatter.date(from: dateStr)
            dateFormatter.dateFormat       = "MMM dd yyyy hh:mm a"
            dateFormatter.timeZone = NSTimeZone.local
            return date?.timeAgoSinceDateForNotification(toReturn: dateFormatter.string(from: date ?? Date())) ?? ""
        }
        else{
            return "1 s"
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let currentNotification = self.arrayOfNotificationList[indexPath.row]
        let currentData = currentNotification["value"] as! [String:Any]
        print(currentData)
        
        
        let type = currentData["type"] as! String
        let subType = currentData["sub_type"] as! String
        
        if (subType == notificationSubTypes.publicfeed || subType == notificationSubTypes.familyfeed) && type == notificationTypes.home{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "PublicPostCell", for: indexPath) as! PublicPostCell
            
            
            cell.btnThreedot.tag = indexPath.row
            
            
            let propic = currentData["cover_image"] as? String ?? ""
            
            let temp =  currentData["type"] as? String ?? ""
            
            if propic != ""{
                var temp = ""
                temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+propic
                
                
                let imgUrl = URL(string: temp)
                cell.imgOfPost.kf.indicatorType  = .activity
                
                //            cell.imgViewUser.kf.setImage(with: imgUrl, placeholder: nil, options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
                cell.imgOfPost.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Family Logo"), options: nil, progressBlock: nil, completionHandler: nil)
                
            }
            else{
                cell.imgOfPost.image = #imageLiteral(resourceName: "Family Logo")
            }
            let creatorPropic = currentData["created_by_propic"] as? String ?? ""
            cell.btnThreedot.tag = indexPath.row
            
            
            if creatorPropic != ""{
                var temp = ""
//                temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=20&height=20&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+creatorPropic
                temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=50&height=50&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+creatorPropic

                let imgUrl = URL(string: temp)
                cell.imgOfCreator.kf.indicatorType  = .activity
                cell.imgOfCreator.contentMode = .redraw
                cell.imgOfCreator.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
                
            }
            else{
                cell.imgOfCreator.image = #imageLiteral(resourceName: "Male Colored")
            }
            cell.viewOfGreen.backgroundColor = .white

            if currentData["visible_status"] as! String == "read"{
                
//                cell.viewOfGreen.backgroundColor = UIColor(rgb: 0xF2F2F2).withAlphaComponent(0.7)
                cell.lblMessage.font = .systemFont(ofSize: 15.0)
                cell.dotView.isHidden = true
            }
            else{
          //      cell.viewOfGreen.backgroundColor = .white
                cell.lblMessage.font = .boldSystemFont(ofSize: 15.0)
                cell.dotView.isHidden = false
                
            }
            
            let modifiedFont = NSString(format:"<span style=\"font-family: \(UIFont.systemFontSize); font-size: \(15)\">%@</span>" as NSString, currentData["message"] as! NSString)
            
            let attrStr = try! NSAttributedString(
                data: modifiedFont.data(using: String.Encoding.unicode.rawValue, allowLossyConversion: true)!,
                options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
                documentAttributes: nil)
            
            cell.lblMessage.text = currentData["message"] as? String ?? ""
            
            
            let description = currentData["description"] as? String ?? ""
            if description == ""
            {
                cell.heightOfLblTitle.constant = 0
                cell.lblTitle.isHidden = true
            }
            else
            {
                cell.heightOfLblTitle.constant = 20
                cell.lblTitle.isHidden = false
                cell.lblTitle.text = description
                
            }
            
            
            cell.lblCreatorName.text = currentData["created_by_user"] as? String ?? ""
            let privacy_type = currentData["privacy_type"] as? String ?? ""
            
            
            if privacy_type == "public"
            {
                let privacy_type = currentData["privacy_type"] as? String ?? ""
                cell.lblType.text = privacy_type.capitalizingFirstLetter()
                cell.lblLine.isHidden = false

            }
            else
            {
                cell.lblType.text = currentData["to_group_name"] as? String ?? ""
                if cell.lblType.text == ""
                {
                    cell.lblLine.isHidden = true
                }
                else{
                    cell.lblLine.isHidden = false

                }
                
                
            }
            
            
            let dateAsString = (currentData["create_time"] as? String ?? "")
            
            cell.lblDate.text = self.formatDateString(dateStr: dateAsString)
            
            
            return  cell
        }
            
        else if subType == notificationSubTypes.conversation && type == notificationTypes.post{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "PostNotificationCell", for: indexPath) as! PostNotificationCell
            
            
            
            cell.btnThreedot.tag = indexPath.row
            
            
            let propic = currentData["cover_image"] as? String ?? ""
            
            let temp =  currentData["type"] as? String ?? ""
            
            if propic != ""{
                var temp = ""
                temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+propic
                
                
                let imgUrl = URL(string: temp)
                cell.imgOfPost.kf.indicatorType  = .activity
                
                //            cell.imgViewUser.kf.setImage(with: imgUrl, placeholder: nil, options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
                cell.imgOfPost.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Family Logo"), options: nil, progressBlock: nil, completionHandler: nil)
                
            }
            else{
                cell.imgOfPost.image = #imageLiteral(resourceName: "Family Logo")
            }
            
            let creatorPropic = currentData["created_by_propic"] as? String ?? ""
            if creatorPropic != ""{
                var temp = ""
//                temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=180&height=80&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+creatorPropic
                temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=50&height=50&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+creatorPropic

                let imgUrl = URL(string: temp)
                cell.imgPropic.kf.indicatorType  = .activity
                cell.imgPropic.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
                
            }
            else{
                cell.imgPropic.image = #imageLiteral(resourceName: "Male Colored")
            }
            cell.viewOfGreen.backgroundColor = .white

            if currentData["visible_status"] as! String == "read"{
//                cell.viewOfGreen.backgroundColor = UIColor(rgb: 0xF2F2F2).withAlphaComponent(0.7)
                cell.lblMessage.font = .systemFont(ofSize: 15.0)
                cell.dotView.isHidden = true
            }
            else{
          //      cell.viewOfGreen.backgroundColor = .white
                cell.lblMessage.font = .boldSystemFont(ofSize: 15.0)
                cell.dotView.isHidden = false
            }
            
            let modifiedFont = NSString(format:"<span style=\"font-family: \(UIFont.systemFontSize); font-size: \(15)\">%@</span>" as NSString, currentData["message"] as! NSString)
            
            let attrStr = try! NSAttributedString(
                data: modifiedFont.data(using: String.Encoding.unicode.rawValue, allowLossyConversion: true)!,
                options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
                documentAttributes: nil)
            
            cell.lblMessage.text = currentData["message"] as? String ?? ""
            cell.lblDesc.text = currentData["description"] as? String ?? ""
            if cell.lblDesc.text == ""
                       {
                           cell.heightOfDescription.constant = 0

                       }
                       else
                       {
                           cell.heightOfDescription.constant = 25

                       }
            cell.lblName.text = currentData["created_by_user"] as? String ?? ""
            cell.lblGroupName.text = currentData["to_group_name"] as? String ?? ""
            
            let dateAsString = (currentData["create_time"] as? String ?? "")
            
            cell.lblDate.text = self.formatDateString(dateStr: dateAsString)
            
            
            return  cell
            
            
        }
        else if subType == notificationSubTypes.conversation && type == notificationTypes.announcement{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "PostNotificationCell", for: indexPath) as! PostNotificationCell
            
            
            
            cell.btnThreedot.tag = indexPath.row
            
            
            let propic = currentData["cover_image"] as? String ?? ""
            
            let temp =  currentData["type"] as? String ?? ""
            
            if propic != ""{
                var temp = ""
                temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+propic
                
                
                let imgUrl = URL(string: temp)
                cell.imgOfPost.kf.indicatorType  = .activity
                
                //            cell.imgViewUser.kf.setImage(with: imgUrl, placeholder: nil, options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
                cell.imgOfPost.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Family Logo"), options: nil, progressBlock: nil, completionHandler: nil)
                
            }
            else{
                cell.imgOfPost.image = #imageLiteral(resourceName: "Family Logo")
            }
            
            let creatorPropic = currentData["created_by_propic"] as? String ?? ""
            if creatorPropic != ""{
                var temp = ""
//                temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=180&height=80&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+creatorPropic
                temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=50&height=50&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+creatorPropic

                let imgUrl = URL(string: temp)
                cell.imgPropic.kf.indicatorType  = .activity
                cell.imgPropic.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
                
            }
            else{
                cell.imgPropic.image = #imageLiteral(resourceName: "Male Colored")
            }
            cell.viewOfGreen.backgroundColor = .white

            if currentData["visible_status"] as! String == "read"{
//                cell.viewOfGreen.backgroundColor = UIColor(rgb: 0xF2F2F2).withAlphaComponent(0.7)
                cell.lblMessage.font = .systemFont(ofSize: 15.0)
                cell.dotView.isHidden = true
            }
            else{
          //      cell.viewOfGreen.backgroundColor = .white
                cell.lblMessage.font = .boldSystemFont(ofSize: 15.0)
                cell.dotView.isHidden = false
            }
            
            let modifiedFont = NSString(format:"<span style=\"font-family: \(UIFont.systemFontSize); font-size: \(15)\">%@</span>" as NSString, currentData["message"] as! NSString)
            
            let attrStr = try! NSAttributedString(
                data: modifiedFont.data(using: String.Encoding.unicode.rawValue, allowLossyConversion: true)!,
                options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
                documentAttributes: nil)
            
            cell.lblMessage.text = currentData["message"] as? String ?? ""
            cell.lblDesc.text = currentData["description"] as? String ?? ""
            if cell.lblDesc.text == ""
            {
                cell.heightOfDescription.constant = 0

            }
            else
            {
                cell.heightOfDescription.constant = 25

            }
            
            cell.lblName.text = currentData["created_by_user"] as? String ?? ""
            cell.lblGroupName.text = currentData["to_group_name"] as? String ?? ""
            
            
            let dateAsString = (currentData["create_time"] as? String ?? "")
            
            cell.lblDate.text = self.formatDateString(dateStr: dateAsString)
            
            
            return  cell
            
            
        }
            
        else if subType == notificationSubTypes.request && type == notificationTypes.user{
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "MemberJoinCell", for: indexPath) as! MemberJoinCell
            //         heightOfBtnJoin
            
            cell.btnThreedot.tag = indexPath.row
            cell.btnJoin.tag = indexPath.row
            cell.btnCancel.tag = indexPath.row
            
            cell.btnJoin.setTitle("ACCEPT", for: .normal)
            cell.btnCancel.setTitle("DECLINE", for: .normal)
            
            let propic = currentData["cover_image"] as? String ?? ""
            
            let temp =  currentData["type"] as? String ?? ""
            
            if propic != ""{
                var temp = ""
                temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+propic
                
                
                let imgUrl = URL(string: temp)
                cell.imgOfPost.kf.indicatorType  = .activity
                
                //            cell.imgViewUser.kf.setImage(with: imgUrl, placeholder: nil, options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
                cell.imgOfPost.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Family Logo"), options: nil, progressBlock: nil, completionHandler: nil)
                
            }
            else{
                cell.imgOfPost.image = #imageLiteral(resourceName: "Family Logo")
            }
            cell.viewOfGreen.backgroundColor = .white

            if currentData["visible_status"] as! String == "read"{
//                cell.viewOfGreen.backgroundColor = UIColor(rgb: 0xF2F2F2).withAlphaComponent(0.7)
                cell.lblMessage.font = .systemFont(ofSize: 15.0)
                cell.dotView.isHidden = true
                
                cell.btnJoin.isHidden = true
                cell.btnCancel.isHidden = true
                cell.heightOfBtnJoin.constant = 0
                cell.topOfBtnStack.constant = 0
                
            }
            else{
          //      cell.viewOfGreen.backgroundColor = .white
                cell.lblMessage.font = .boldSystemFont(ofSize: 15.0)
                cell.dotView.isHidden = false
                
                cell.btnJoin.isHidden = false
                cell.btnCancel.isHidden = false
                cell.heightOfBtnJoin.constant = 30
                cell.topOfBtnStack.constant = 15

            }
            
            let modifiedFont = NSString(format:"<span style=\"font-family: \(UIFont.systemFontSize); font-size: \(15)\">%@</span>" as NSString, currentData["message"] as! NSString)
            
            let attrStr = try! NSAttributedString(
                data: modifiedFont.data(using: String.Encoding.unicode.rawValue, allowLossyConversion: true)!,
                options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
                documentAttributes: nil)
            
            cell.lblMessage.text = currentData["message"] as? String ?? ""
            
            
            
            
            
            cell.lblCreatorName.text = "By \(currentData["created_by_user"] as? String ?? "")"
            //                                          let privacy_type = currentData["privacy_type"] as! String
            
            cell.lblLocation.text = currentData["location"] as? String ?? ""
            let membercount = currentData["membercount"] as? String ?? ""
            if membercount == ""
            {
                cell.lblMemberCount.text = "0"
                cell.lblMemberHeading.text = "Member"
                
            }
            else
            {
                cell.lblMemberCount.text = membercount
                cell.lblMemberHeading.text = "Members"
                
            }
            
            
            let dateAsString = (currentData["create_time"] as? String ?? "")
            
            cell.lblDate.text = self.formatDateString(dateStr: dateAsString)
            
            
            return  cell
            
        }
            
        else  if subType == notificationSubTypes.guest_interested || subType == notificationSubTypes.guest_attending && type == notificationTypes.event
        {
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "PrivateEventCell", for: indexPath) as! PrivateEventCell
            
            
            
            
            cell.btnThreedot.tag = indexPath.row
            cell.btnGoing.tag = indexPath.row
            cell.btnInterested.tag = indexPath.row
            cell.btnNotInterested.tag = indexPath.row
            cell.heightOfBtnGoing.constant = 0
            cell.topOfBtns.constant = 0
            cell.btnGoing.isHidden = true
            cell.btnInterested.isHidden = true
            cell.btnNotInterested.isHidden = true
            
            
            let propic = currentData["cover_image"] as? String ?? ""
            
            let temp =  currentData["type"] as? String ?? ""
            
            if propic != ""{
                var temp = ""
                temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.event_image)"+propic
                
                
                let imgUrl = URL(string: temp)
                cell.imgOfEvent.kf.indicatorType  = .activity
                
                
                cell.imgOfEvent.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Family Logo"), options: nil, progressBlock: nil, completionHandler: nil)
                
            }
            else{
                cell.imgOfEvent.image = #imageLiteral(resourceName: "Family Logo")
            }
            
            
            let created_by_propic = currentData["created_by_propic"] as? String ?? ""
            
            
            if created_by_propic != ""{
                var temp = ""
                temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=50&height=50&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+created_by_propic
                
                
                let imgUrl = URL(string: temp)
                cell.imgOfCreator.kf.indicatorType  = .activity
                
                
                cell.imgOfCreator.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
                
            }
            else{
                cell.imgOfCreator.image = #imageLiteral(resourceName: "Male Colored")
            }
            let rsvp = currentData["rsvp"] as? Int ?? 0
            
            cell.viewOfGreen.backgroundColor = .white

            if currentData["visible_status"] as! String == "read"{
        //        cell.viewOfGreen.backgroundColor = UIColor(rgb: 0xF2F2F2).withAlphaComponent(0.7)
                cell.lblMessage.font = .systemFont(ofSize: 15.0)
                cell.dotView.isHidden = true
                //                cell.heightOfBtnGoing.constant = 0
                //                cell.btnGoing.isHidden = true
                //                cell.btnInterested.isHidden = true
                //                cell.btnNotInterested.isHidden = true
            }
            else{
          //      cell.viewOfGreen.backgroundColor = .white
                cell.lblMessage.font = .boldSystemFont(ofSize: 15.0)
                cell.dotView.isHidden = false
                
                //                if rsvp == 0
                //                {
                //                    cell.heightOfBtnGoing.constant = 0
                //                    cell.btnGoing.isHidden = true
                //                    cell.btnInterested.isHidden = true
                //                    cell.btnNotInterested.isHidden = true
                //                }
                //                else
                //                {
                //                    cell.heightOfBtnGoing.constant = 30
                //                    cell.btnGoing.isHidden = false
                //                    cell.btnInterested.isHidden = false
                //                    cell.btnNotInterested.isHidden = false
                //                }
            }
            
            let modifiedFont = NSString(format:"<span style=\"font-family: \(UIFont.systemFontSize); font-size: \(15)\">%@</span>" as NSString, currentData["message"] as! NSString)
            
            let attrStr = try! NSAttributedString(
                data: modifiedFont.data(using: String.Encoding.unicode.rawValue, allowLossyConversion: true)!,
                options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
                documentAttributes: nil)
            
            cell.lblMessage.text = (currentData["message"] as! String)
            let dateAsString = (currentData["create_time"] as? String ?? "")
            
            cell.lblDate.text = self.formatDateString(dateStr: dateAsString)
            
            
            //            let eventDetail = currentData["0"] as! [String :Any]
            
            cell.lblCreatorName.text = "\(currentData["created_by_user"] as? String ?? "")"
            
            //                                                 cell.lblLocation.text = currentData["location"] as? String ?? ""
            
            cell.lblEventName.text = currentData["event_name"] as? String ?? ""
            cell.lblCreatedDate.text = self.convertTimeStampToDate(timestamp:"\(currentData["from_date"] as? Int ?? 0)")
            let event_type = currentData["event_type"] as? String ?? ""
            if event_type == "Online"
            {
                cell.imgLocation.isHidden = true
                cell.lblLocation.isHidden = true
                
                
            }
            else
            {
                cell.imgLocation.isHidden = false
                cell.lblLocation.isHidden = false
                cell.lblLocation.text = currentData["location"] as? String ?? ""
                
            }
            
            //                                   cell.imgOfCreator.image = #imageLiteral(resourceName: "Male Colored")
            
            
            //                                               let created_by_propic = currentData["created_by_propic"] as! String
            //
            //
            //
            //                                                                                                     if created_by_propic != ""{
            //                                                                                                         var temp = ""
            //                                                                                                         temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+created_by_propic
            //
            //
            //                                                                                                         let imgUrl = URL(string: temp)
            //                                                                                                         cell.imgOfCreator.kf.indicatorType  = .activity
            //
            //
            //                                                                                                         cell.imgOfCreator.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
            //
            //                                                                                                     }
            //                                                                                                     else{
            //                                                                                                         cell.imgOfCreator.image = #imageLiteral(resourceName: "Male Colored")
            //                                                                                                     }
            
            return  cell
            
        }
        else if type == notificationTypes.event{
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "PrivateEventCell", for: indexPath) as! PrivateEventCell
            
            
            
            
            cell.btnThreedot.tag = indexPath.row
            cell.btnGoing.tag = indexPath.row
            cell.btnInterested.tag = indexPath.row
            cell.btnNotInterested.tag = indexPath.row
            
            
            let propic = currentData["cover_image"] as? String ?? ""
            
            let temp =  currentData["type"] as? String ?? ""
            
            if propic != ""{
                var temp = ""
                temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.event_image)"+propic
                
                
                let imgUrl = URL(string: temp)
                cell.imgOfEvent.kf.indicatorType  = .activity
                
                
                cell.imgOfEvent.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Family Logo"), options: nil, progressBlock: nil, completionHandler: nil)
                
            }
            else{
                cell.imgOfEvent.image = #imageLiteral(resourceName: "Family Logo")
            }
            //            }
            //            else
            //                                               {
            //                                                cell.imgOfEvent.image = #imageLiteral(resourceName: "Family Logo")
            //
            //            }
            
            
            let created_by_propic = currentData["created_by_propic"] as? String ?? ""
            
            
            if created_by_propic != ""{
                var temp = ""
                temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=50&height=50&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+created_by_propic
                
                
                let imgUrl = URL(string: temp)
                cell.imgOfCreator.kf.indicatorType  = .activity
                
                
                cell.imgOfCreator.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
                
            }
            else{
                cell.imgOfCreator.image = #imageLiteral(resourceName: "Male Colored")
            }
            
            
            
            //            cell.imgOfCreator.image = #imageLiteral(resourceName: "Male Colored")
            
            let rsvp = currentData["rsvp"] as? Int ?? 0
            cell.viewOfGreen.backgroundColor = .white

            if currentData["visible_status"] as! String == "read"{
        //        cell.viewOfGreen.backgroundColor = UIColor(rgb: 0xF2F2F2).withAlphaComponent(0.7)
                cell.lblMessage.font = .systemFont(ofSize: 15.0)
                cell.dotView.isHidden = true
                cell.heightOfBtnGoing.constant = 0
                cell.topOfBtns.constant = 0

                cell.btnGoing.isHidden = true
                cell.btnInterested.isHidden = true
                cell.btnNotInterested.isHidden = true
            }
            else{
          //      cell.viewOfGreen.backgroundColor = .white
                cell.lblMessage.font = .boldSystemFont(ofSize: 15.0)
                cell.dotView.isHidden = false
                
                if rsvp == 0
                {
                    cell.heightOfBtnGoing.constant = 0
                    cell.topOfBtns.constant = 0

                    cell.btnGoing.isHidden = true
                    cell.btnInterested.isHidden = true
                    cell.btnNotInterested.isHidden = true
                }
                else
                {
                    cell.heightOfBtnGoing.constant = 30
                    cell.topOfBtns.constant = 15

                    cell.btnGoing.isHidden = false
                    cell.btnInterested.isHidden = false
                    cell.btnNotInterested.isHidden = false
                }
            }
            
            let modifiedFont = NSString(format:"<span style=\"font-family: \(UIFont.systemFontSize); font-size: \(15)\">%@</span>" as NSString, currentData["message"] as! NSString)
            
            let attrStr = try! NSAttributedString(
                data: modifiedFont.data(using: String.Encoding.unicode.rawValue, allowLossyConversion: true)!,
                options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
                documentAttributes: nil)
            
            cell.lblMessage.text = (currentData["message"] as! String)
            let dateAsString = (currentData["create_time"] as! String)
            
            cell.lblDate.text = self.formatDateString(dateStr: dateAsString)
            
            
            //            let eventDetail = currentData["0"] as! [String :Any]
            
            cell.lblCreatorName.text = "\(currentData["created_by_user"] as? String ?? "")"
            
            
            cell.lblEventName.text = currentData["event_type"] as? String ?? ""
            cell.lblCreatedDate.text = self.convertTimeStampToDate(timestamp:"\(currentData["from_date"] as? Int ?? 0)")
            
            //       imgLocation
            let event_type = currentData["event_type"] as? String ?? ""
            if event_type == "Online" || event_type == "online"
            {
                cell.imgLocation.isHidden = true
                cell.lblLocation.isHidden = true
                
                
            }
            else
            {
                cell.imgLocation.isHidden = false
                cell.lblLocation.isHidden = false
                cell.lblLocation.text = currentData["location"] as? String ?? ""
                
            }
            
            
            //                                               let created_by_propic = currentData["created_by_propic"] as! String
            //
            //
            //
            //                                                                                                     if created_by_propic != ""{
            //                                                                                                         var temp = ""
            //                                                                                                         temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+created_by_propic
            //
            //
            //                                                                                                         let imgUrl = URL(string: temp)
            //                                                                                                         cell.imgOfCreator.kf.indicatorType  = .activity
            //
            //
            //                                                                                                         cell.imgOfCreator.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
            //
            //                                                                                                     }
            //                                                                                                     else{
            //                                                                                                         cell.imgOfCreator.image = #imageLiteral(resourceName: "Male Colored")
            //                                                                                                     }
            
            return  cell
            
        }
            //        {
            //
            //                let cell = tableView.dequeueReusableCell(withIdentifier: "ChatNotificationCell", for: indexPath) as! ChatNotificationCell
            //
            //
            //
            //                cell.btnThreedot.tag = indexPath.row
            //
            //
            //                                                     let propic = currentData["propic"] as! String
            //
            //                                                          let temp =  currentData["type"] as! String
            //
            //                                                          if propic != ""{
            //                                                              var temp = ""
            //                                                              temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+propic
            //
            //
            //                                                              let imgUrl = URL(string: temp)
            //                                                              cell.imgOfPost.kf.indicatorType  = .activity
            //
            //                                                              //            cell.imgViewUser.kf.setImage(with: imgUrl, placeholder: nil, options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
            //                                                              cell.imgOfPost.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
            //
            //                                                          }
            //                                                          else{
            //                                                              cell.imgOfPost.image = #imageLiteral(resourceName: "Male Colored")
            //                                                          }
            //
            //                                                          if currentData["visible_status"] as! String == "unread"{
            //                                                              cell.viewOfGreen.backgroundColor = UIColor(rgb: 0xF2F2F2).withAlphaComponent(0.7)
            //                                                          }
            //                                                          else{
            //                                                              cell.viewOfGreen.backgroundColor = .white
            //                                                          }
            //
            //                                                          let modifiedFont = NSString(format:"<span style=\"font-family: \(UIFont.systemFontSize); font-size: \(15)\">%@</span>" as NSString, currentData["message"] as! NSString)
            //
            //                                                          let attrStr = try! NSAttributedString(
            //                                                              data: modifiedFont.data(using: String.Encoding.unicode.rawValue, allowLossyConversion: true)!,
            //                                                              options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
            //                                                              documentAttributes: nil)
            //
            //                                                          cell.lblMessage.text = currentData["message"] as! String
            //
            //                                                        cell.lblDesc.text = currentData["description"] as! String
            //
            //
            //
            //
            //                                                          let dateAsString = (currentData["create_time"] as! String)
            //
            //                                                           cell.lblDate.text = self.formatDateString(dateStr: dateAsString)
            //
            //
            //                                                             return  cell
            //
            //
            //            }
        else if subType == notificationSubTypes.calendar{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatNotificationCell", for: indexPath) as! ChatNotificationCell
            
            
            
            cell.btnThreedot.tag = indexPath.row
            
            
            let propic = currentData["propic"] as? String ?? ""
            
            let temp =  currentData["type"] as? String ?? ""
            
            if propic != ""{
                var temp = ""
                temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+propic
                
                
                let imgUrl = URL(string: temp)
                cell.imgOfPost.kf.indicatorType  = .activity
                
                //            cell.imgViewUser.kf.setImage(with: imgUrl, placeholder: nil, options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
                cell.imgOfPost.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Family Logo"), options: nil, progressBlock: nil, completionHandler: nil)
                
            }
            else{
                cell.imgOfPost.image = #imageLiteral(resourceName: "Family Logo")
            }
            cell.viewOfGreen.backgroundColor = .white

            if currentData["visible_status"] as! String == "read"{
//                cell.viewOfGreen.backgroundColor = UIColor(rgb: 0xF2F2F2).withAlphaComponent(0.7)
                cell.lblMessage.font = .systemFont(ofSize: 15.0)
                cell.dotView.isHidden = true
            }
            else{
          //      cell.viewOfGreen.backgroundColor = .white
                cell.lblMessage.font = .boldSystemFont(ofSize: 15.0)
                cell.dotView.isHidden = false
            }
            
            let modifiedFont = NSString(format:"<span style=\"font-family: \(UIFont.systemFontSize); font-size: \(15)\">%@</span>" as NSString, currentData["message"] as! NSString)
            
            let attrStr = try! NSAttributedString(
                data: modifiedFont.data(using: String.Encoding.unicode.rawValue, allowLossyConversion: true)!,
                options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
                documentAttributes: nil)
            
            cell.lblMessage.text = currentData["message"] as? String ?? ""
            
            cell.lblDesc.text = currentData["description"] as? String ?? ""
            
            
            
            
            let dateAsString = (currentData["create_time"] as? String ?? "")
            
            cell.lblDate.text = self.formatDateString(dateStr: dateAsString)
            
            
            return  cell
            
            
        }
        else if subType == notificationSubTypes.member{
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "MemberJoinCell", for: indexPath) as! MemberJoinCell
            //         heightOfBtnJoin
            
            cell.btnJoin.isHidden = true
            cell.btnCancel.isHidden = true
            cell.heightOfBtnJoin.constant = 0
            cell.topOfBtnStack.constant = 0

            cell.btnThreedot.tag = indexPath.row
            cell.btnJoin.tag = indexPath.row
            cell.btnCancel.tag = indexPath.row
            
            
            let propic = currentData["propic"] as? String ?? ""
            
            let temp =  currentData["type"] as? String ?? ""
            
            if propic != ""{
                var temp = ""
                temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+propic
                
                
                let imgUrl = URL(string: temp)
                cell.imgOfPost.kf.indicatorType  = .activity
                
                //            cell.imgViewUser.kf.setImage(with: imgUrl, placeholder: nil, options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
                cell.imgOfPost.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Family Logo"), options: nil, progressBlock: nil, completionHandler: nil)
                
            }
            else{
                cell.imgOfPost.image = #imageLiteral(resourceName: "Family Logo")
            }
            cell.viewOfGreen.backgroundColor = .white

            if currentData["visible_status"] as! String == "read"{
        //        cell.viewOfGreen.backgroundColor = UIColor(rgb: 0xF2F2F2).withAlphaComponent(0.7)
                cell.lblMessage.font = .systemFont(ofSize: 15.0)
                cell.dotView.isHidden = true
            }
            else{
          //      cell.viewOfGreen.backgroundColor = .white
                cell.lblMessage.font = .boldSystemFont(ofSize: 15.0)
                cell.dotView.isHidden = false
            }
            
            let modifiedFont = NSString(format:"<span style=\"font-family: \(UIFont.systemFontSize); font-size: \(15)\">%@</span>" as NSString, currentData["message"] as! NSString)
            
            let attrStr = try! NSAttributedString(
                data: modifiedFont.data(using: String.Encoding.unicode.rawValue, allowLossyConversion: true)!,
                options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
                documentAttributes: nil)
            
            cell.lblMessage.text = currentData["message"] as? String ?? ""
            
            
            
            
            
            cell.lblCreatorName.text = "By \(currentData["created_by_user"] as? String ?? "")"
            //                                          let privacy_type = currentData["privacy_type"] as! String
            
            cell.lblLocation.text = currentData["location"] as? String ?? ""
            let membercount = currentData["membercount"] as? String ?? ""
            if membercount == ""
            {
                cell.lblMemberCount.text = "0"
                cell.lblMemberHeading.text = "Member"
                
            }
            else
            {
                cell.lblMemberCount.text = membercount
                cell.lblMemberHeading.text = "Members"
                
            }
            
            
            let dateAsString = (currentData["create_time"] as! String)
            
            cell.lblDate.text = self.formatDateString(dateStr: dateAsString)
            
            
            return  cell
            
        }
        else if subType == notificationSubTypes.membership{
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "MemberJoinCell", for: indexPath) as! MemberJoinCell
            //         heightOfBtnJoin
            
            cell.btnJoin.isHidden = true
            cell.btnCancel.isHidden = true
            cell.heightOfBtnJoin.constant = 0
            cell.topOfBtnStack.constant = 0

            cell.btnThreedot.tag = indexPath.row
            cell.btnJoin.tag = indexPath.row
            cell.btnCancel.tag = indexPath.row
            
            
            let propic = currentData["propic"] as? String ?? ""
            
            let temp =  currentData["type"] as? String ?? ""
            
            if propic != ""{
                var temp = ""
                temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+propic
                
                
                let imgUrl = URL(string: temp)
                cell.imgOfPost.kf.indicatorType  = .activity
                
                //            cell.imgViewUser.kf.setImage(with: imgUrl, placeholder: nil, options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
                cell.imgOfPost.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Family Logo"), options: nil, progressBlock: nil, completionHandler: nil)
                
            }
            else{
                cell.imgOfPost.image = #imageLiteral(resourceName: "Family Logo")
            }
            cell.viewOfGreen.backgroundColor = .white

            if currentData["visible_status"] as! String == "read"{
        //        cell.viewOfGreen.backgroundColor = UIColor(rgb: 0xF2F2F2).withAlphaComponent(0.7)
                cell.lblMessage.font = .systemFont(ofSize: 15.0)
                cell.dotView.isHidden = true
            }
            else{
          //      cell.viewOfGreen.backgroundColor = .white
                cell.lblMessage.font = .boldSystemFont(ofSize: 15.0)
                cell.dotView.isHidden = false
            }
            
            let modifiedFont = NSString(format:"<span style=\"font-family: \(UIFont.systemFontSize); font-size: \(15)\">%@</span>" as NSString, currentData["message"] as! NSString)
            
            let attrStr = try! NSAttributedString(
                data: modifiedFont.data(using: String.Encoding.unicode.rawValue, allowLossyConversion: true)!,
                options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
                documentAttributes: nil)
            
            cell.lblMessage.text = currentData["message"] as? String ?? ""
            
            
            
            
            
            cell.lblCreatorName.text = "By \(currentData["created_by_user"] as? String ?? "")"
            //                                          let privacy_type = currentData["privacy_type"] as! String
            
            cell.lblLocation.text = currentData["location"] as? String ?? ""
            let membercount = currentData["membercount"] as? String ?? ""
            if membercount == ""
            {
                cell.lblMemberCount.text = "0"
                cell.lblMemberHeading.text = "Member"
                
            }
            else
            {
                cell.lblMemberCount.text = membercount
                cell.lblMemberHeading.text = "Members"
                
            }
            
            
            let dateAsString = (currentData["create_time"] as! String)
            
            cell.lblDate.text = self.formatDateString(dateStr: dateAsString)
            
            
            return  cell
            
        }
            //                        else if subType == notificationSubTypes.request && type == notificationTypes.user{
            //
            //                                    let cell = tableView.dequeueReusableCell(withIdentifier: "MemberJoinCell", for: indexPath) as! MemberJoinCell
            //                        //         heightOfBtnJoin
            //
            //                                    cell.btnThreedot.tag = indexPath.row
            //                                    cell.btnJoin.tag = indexPath.row
            //                                    cell.btnCancel.tag = indexPath.row
            //
            //
            //                                                             let propic = currentData["cover_image"] as? String ?? ""
            //
            //                                                                  let temp =  currentData["type"] as? String ?? ""
            //
            //                                                                  if propic != ""{
            //                                                                      var temp = ""
            //                                                                      temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+propic
            //
            //
            //                                                                      let imgUrl = URL(string: temp)
            //                                                                      cell.imgOfPost.kf.indicatorType  = .activity
            //
            //                                                                      //            cell.imgViewUser.kf.setImage(with: imgUrl, placeholder: nil, options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
            //                                                                      cell.imgOfPost.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
            //
            //                                                                  }
            //                                                                  else{
            //                                                                      cell.imgOfPost.image = #imageLiteral(resourceName: "Male Colored")
            //                                                                  }
            //
            //                                                                 if currentData["visible_status"] as! String == "unread"{
            //                                                                                                                          cell.viewOfGreen.backgroundColor = UIColor(rgb: 0xF2F2F2).withAlphaComponent(0.7)
            //                                                                                                                        //
            //                                                                                                                        cell.btnJoin.isHidden = false
            //                                                                                                                        cell.btnCancel.isHidden = true
            //                                                                                                                        cell.heightOfBtnJoin.constant = 30
            //
            //                                                                                                                      }
            //                                                                                                                      else{
            //                                                                                                                    //      cell.viewOfGreen.backgroundColor = .white
            //                                                                                                                        cell.btnJoin.isHidden = true
            //                                                                                                                                                                              cell.btnCancel.isHidden = true
            //                                                                                                                                                                              cell.heightOfBtnJoin.constant = 0
            //                                                                                                                      }
            //
            //                                                                  let modifiedFont = NSString(format:"<span style=\"font-family: \(UIFont.systemFontSize); font-size: \(15)\">%@</span>" as NSString, currentData["message"] as! NSString)
            //
            //                                                                  let attrStr = try! NSAttributedString(
            //                                                                      data: modifiedFont.data(using: String.Encoding.unicode.rawValue, allowLossyConversion: true)!,
            //                                                                      options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
            //                                                                      documentAttributes: nil)
            //
            //                                                                  cell.lblMessage.text = currentData["message"] as! String
            //
            //
            //
            //
            //
            //                                          cell.lblCreatorName.text = "By \(currentData["created_by_user"] as? String ?? "")"
            //                        //                                          let privacy_type = currentData["privacy_type"] as! String
            //
            //                                              cell.lblLocation.text = currentData["location"] as? String ?? ""
            //                        let membercount = currentData["membercount"] as? String ?? ""
            //                                    if membercount == ""
            //                                    {
            //                                        cell.lblMemberCount.text = "0"
            //                                        cell.lblMemberHeading.text = "Member"
            //
            //                                    }
            //                                    else
            //                                    {
            //                                        cell.lblMemberCount.text = membercount
            //                                                       cell.lblMemberHeading.text = "Members"
            //
            //                                    }
            //
            //
            //                                                                  let dateAsString = (currentData["create_time"] as! String)
            //
            //                                                                   cell.lblDate.text = self.formatDateString(dateStr: dateAsString)
            //
            //
            //                                                                     return  cell
            //
            //                                                }
        else if subType == notificationSubTypes.request && type == notificationTypes.family{
            //                appDel.notificationData = currentData
            //                getMemberDetails(toRequest: true)
            
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "MemberJoinCell", for: indexPath) as! MemberJoinCell
            //         heightOfBtnJoin
            
            //                cell.btnJoin.isHidden = true
            //                cell.btnCancel.isHidden = true
            //                cell.heightOfBtnJoin.constant = 0
            
            cell.btnThreedot.tag = indexPath.row
            cell.btnJoin.tag = indexPath.row
            cell.btnCancel.tag = indexPath.row
            cell.btnJoin.setTitle("ACCEPT", for: .normal)
            cell.btnCancel.setTitle("DECLINE", for: .normal)
            
            let propic = currentData["cover_image"] as? String ?? ""
            
            let temp =  currentData["type"] as? String ?? ""
            
            if propic != ""{
                var temp = ""
                temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+propic
                
                
                let imgUrl = URL(string: temp)
                cell.imgOfPost.kf.indicatorType  = .activity
                
                //            cell.imgViewUser.kf.setImage(with: imgUrl, placeholder: nil, options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
                cell.imgOfPost.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Family Logo"), options: nil, progressBlock: nil, completionHandler: nil)
                
            }
            else{
                cell.imgOfPost.image = #imageLiteral(resourceName: "Family Logo")
            }
            cell.viewOfGreen.backgroundColor = .white

            if currentData["visible_status"] as! String == "read"{
        //        cell.viewOfGreen.backgroundColor = UIColor(rgb: 0xF2F2F2).withAlphaComponent(0.7)
                cell.lblMessage.font = .systemFont(ofSize: 15.0)
                cell.dotView.isHidden = true
                cell.btnJoin.isHidden = true
                cell.btnCancel.isHidden = true
                cell.heightOfBtnJoin.constant = 0
                cell.topOfBtnStack.constant = 0

            }
            else{
          //      cell.viewOfGreen.backgroundColor = .white
                cell.lblMessage.font = .boldSystemFont(ofSize: 15.0)
                cell.dotView.isHidden = false
                
                cell.btnJoin.isHidden = false
                cell.btnCancel.isHidden = false
                cell.heightOfBtnJoin.constant = 30
                cell.topOfBtnStack.constant = 15

            }
            
            let modifiedFont = NSString(format:"<span style=\"font-family: \(UIFont.systemFontSize); font-size: \(15)\">%@</span>" as NSString, currentData["message"] as! NSString)
            
            let attrStr = try! NSAttributedString(
                data: modifiedFont.data(using: String.Encoding.unicode.rawValue, allowLossyConversion: true)!,
                options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
                documentAttributes: nil)
            
            cell.lblMessage.text = currentData["message"] as? String ?? ""
            
            
            
            
            
            cell.lblCreatorName.text = "By \(currentData["created_by_user"] as? String ?? "")"
            //                                          let privacy_type = currentData["privacy_type"] as! String
            
            cell.lblLocation.text = currentData["location"] as? String ?? ""
            let membercount = currentData["membercount"] as? String ?? ""
            if membercount == ""
            {
                cell.lblMemberCount.text = "0"
                cell.lblMemberHeading.text = "Member"
                
            }
            else
            {
                cell.lblMemberCount.text = membercount
                cell.lblMemberHeading.text = "Members"
                
            }
            
            
            let dateAsString = (currentData["create_time"] as? String ?? "")
            
            cell.lblDate.text = self.formatDateString(dateStr: dateAsString)
            
            
            return  cell
            
            
            
        }
        else if subType == notificationSubTypes.request && type == notificationTypes.home{
            //                appDel.notificationData = currentData
            //                getMemberDetails(toRequest: true)
            
            
            //       if let id = currentData["type_id"] {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationRequestTableViewCell", for: indexPath) as! NotificationRequestTableViewCell
            
            
            cell.btnThreedot.tag = indexPath.row
            cell.btnSupport.tag = indexPath.row
            
            //                if currentData["cover_pic"].stringValue.count != 0 {
            //                    img.kf.indicatorType   = .activity
            //                    let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&gravity=smart&url="+currentData["cover_pic"].stringValue.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            //                           let url = URL(string: newUrlStr)
            //                           img.kf.indicatorType = .activity
            //
            //                        img.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
            //                }
            //                else{
            //                    img.image = #imageLiteral(resourceName: "imgNoImage")
            //                }
            
            let propic = currentData["cover_image"] as? String ?? ""
            
            
            if propic != ""{
                var temp = ""
                temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.groupCover)"+propic
                
                
                let imgUrl = URL(string: temp)
                cell.imgOfRequest.kf.indicatorType  = .activity
                
                //            cell.imgViewUser.kf.setImage(with: imgUrl, placeholder: nil, options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
                cell.imgOfRequest.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Family Logo"), options: nil, progressBlock: nil, completionHandler: nil)
                
            }
            else{
                cell.imgOfRequest.image = #imageLiteral(resourceName: "Family Logo")
            }
            let creatorPropic = currentData["created_by_propic"] as? String ?? ""
            
            
            if creatorPropic != ""{
                var temp = ""
                
                
                temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=50&height=50&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+creatorPropic
                
                
                let imgUrl = URL(string: temp)
                cell.imgOfCreator.kf.indicatorType  = .activity
                
                //            cell.imgViewUser.kf.setImage(with: imgUrl, placeholder: nil, options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
                cell.imgOfCreator.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
                
            }
            else{
                cell.imgOfCreator.image = #imageLiteral(resourceName: "Male Colored")
            }
                  cell.viewOfGreen.backgroundColor = .white

            if currentData["visible_status"] as! String == "read"{
        //        cell.viewOfGreen.backgroundColor = UIColor(rgb: 0xF2F2F2).withAlphaComponent(0.7)
                cell.lblMessage.font = .systemFont(ofSize: 15.0)
                cell.dotView.isHidden = true
                
                cell.btnSupport.isHidden = true
                cell.heightOfBtn.constant = 0
            }
            else{
          //      cell.viewOfGreen.backgroundColor = .white
                cell.lblMessage.font = .boldSystemFont(ofSize: 15.0)
                cell.dotView.isHidden = false
                
                
                if currentData["category"] as! String == "contribution_create" && currentData["category"] as? String ?? "" == "request"{
                    cell.btnSupport.isHidden = true
                    cell.heightOfBtn.constant = 0
                }
                else
                {
                    cell.btnSupport.isHidden = false
                    cell.heightOfBtn.constant = 30
                    
                }
            }
            
            
            cell.lblMessage.text = currentData["message"] as? String ?? ""
            
            
            let description = currentData["description"] as? String ?? ""
            if description == ""
            {
                cell.heightOfLblTitle.constant = 0
                cell.lblDescription.isHidden = true
            }
            else
            {
                cell.heightOfLblTitle.constant = 20
                cell.lblDescription.isHidden = false
                cell.lblDescription.text = description
                
            }
            
            cell.lblCreatorName.text = currentData["created_by_user"] as? String ?? ""
            //                                    let privacy_type = currentData["privacy_type"] as! String
            //            if privacy_type == "public"
            //            {
            //                cell.lblType.text = currentData["privacy_type"] as? String ?? ""
            //
            //            }
            //            else
            //            {
            //                cell.lblType.text = currentData["to_group_name"] as? String ?? ""
            //
            //            }
            
            cell.lblType.isHidden = true
            cell.lblLine.isHidden = true
            let dateAsString = (currentData["create_time"] as? String ?? "")
            
            cell.lblDate.text = self.formatDateString(dateStr: dateAsString)
            
            
            return  cell
            //  }
            
            
        }
            
            //                        else  if (subType == notificationSubTypes.publicfeed || subType == notificationSubTypes.familyfeed) && type == notificationTypes.home{
            //
            //            let cell = tableView.dequeueReusableCell(withIdentifier: "PublicPostCell", for: indexPath) as! PublicPostCell
            //                     return cell
            ////                            if let id = currentData["type_id"] {
            //
            //
            //                            //                    let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
            //            //                    let vc = storyboard.instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
            //            //                    vc.postId = "\(id)"
            //            //                    self.navigationController?.pushViewController(vc, animated: false)
            ////                            }
            //                        }
            
        else if subType == notificationSubTypes.link_family && type == notificationTypes.family
            {
                        
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: "MemberJoinCell", for: indexPath) as! MemberJoinCell
                        //         heightOfBtnJoin
                        
                        cell.btnThreedot.tag = indexPath.row
                        cell.btnJoin.tag = indexPath.row
                        cell.btnCancel.tag = indexPath.row
                        
                        cell.btnJoin.setTitle("ACCEPT", for: .normal)
                        cell.btnCancel.setTitle("DECLINE", for: .normal)
                        
                        let propic = currentData["cover_image"] as? String ?? ""
                        
                        let temp =  currentData["type"] as? String ?? ""
                        
                        if propic != ""{
                            var temp = ""
                            temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+propic
                            
                            
                            let imgUrl = URL(string: temp)
                            cell.imgOfPost.kf.indicatorType  = .activity
                            
                            //            cell.imgViewUser.kf.setImage(with: imgUrl, placeholder: nil, options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
                            cell.imgOfPost.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Family Logo"), options: nil, progressBlock: nil, completionHandler: nil)
                            
                        }
                        else{
                            cell.imgOfPost.image = #imageLiteral(resourceName: "Family Logo")
                        }
                        cell.viewOfGreen.backgroundColor = .white

                        if currentData["visible_status"] as! String == "read"{
            //                cell.viewOfGreen.backgroundColor = UIColor(rgb: 0xF2F2F2).withAlphaComponent(0.7)
                            cell.lblMessage.font = .systemFont(ofSize: 15.0)
                            cell.dotView.isHidden = true
                            
                            cell.btnJoin.isHidden = true
                            cell.btnCancel.isHidden = true
                            cell.heightOfBtnJoin.constant = 0
                            cell.topOfBtnStack.constant = 0
                            
                        }
                        else{
                      //      cell.viewOfGreen.backgroundColor = .white
                            cell.lblMessage.font = .boldSystemFont(ofSize: 15.0)
                            cell.dotView.isHidden = false
                            
                            cell.btnJoin.isHidden = false
                            cell.btnCancel.isHidden = false
                            cell.heightOfBtnJoin.constant = 30
                            cell.topOfBtnStack.constant = 15

                        }
                        
                        let modifiedFont = NSString(format:"<span style=\"font-family: \(UIFont.systemFontSize); font-size: \(15)\">%@</span>" as NSString, currentData["message"] as! NSString)
                        
                        let attrStr = try! NSAttributedString(
                            data: modifiedFont.data(using: String.Encoding.unicode.rawValue, allowLossyConversion: true)!,
                            options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
                            documentAttributes: nil)
                        
                        cell.lblMessage.text = currentData["message"] as? String ?? ""
                        
                        
                        
                        
                        
                        cell.lblCreatorName.text = "By \(currentData["created_by_user"] as? String ?? "")"
                        //                                          let privacy_type = currentData["privacy_type"] as! String
                        
                        cell.lblLocation.text = currentData["location"] as? String ?? ""
                        let membercount = currentData["membercount"] as? String ?? ""
                        if membercount == ""
                        {
                            cell.lblMemberCount.text = "0"
                            cell.lblMemberHeading.text = "Member"
                            
                        }
                        else
                        {
                            cell.lblMemberCount.text = membercount
                            cell.lblMemberHeading.text = "Members"
                            
                        }
                        
                        
                        let dateAsString = (currentData["create_time"] as? String ?? "")
                        
                        cell.lblDate.text = self.formatDateString(dateStr: dateAsString)
                        
                        
                        return  cell
                        
                    }
//        {
//
//            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatNotificationCell", for: indexPath) as! ChatNotificationCell
//
//
//
//            cell.btnThreedot.tag = indexPath.row
//
//
//            let propic = currentData["propic"] as? String ?? ""
//
//            let temp =  currentData["type"] as? String ?? ""
//
//            if propic != ""{
//                var temp = ""
//                temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+propic
//
//
//                let imgUrl = URL(string: temp)
//                cell.imgOfPost.kf.indicatorType  = .activity
//
//                //            cell.imgViewUser.kf.setImage(with: imgUrl, placeholder: nil, options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
//                cell.imgOfPost.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Family Logo"), options: nil, progressBlock: nil, completionHandler: nil)
//
//            }
//            else{
//                cell.imgOfPost.image = #imageLiteral(resourceName: "Family Logo")
//            }
//            cell.viewOfGreen.backgroundColor = .white
//
//            if currentData["visible_status"] as! String == "read"{
//        //        cell.viewOfGreen.backgroundColor = UIColor(rgb: 0xF2F2F2).withAlphaComponent(0.7)
//                cell.lblMessage.font = .systemFont(ofSize: 15.0)
//                cell.dotView.isHidden = true
//            }
//            else{
//          //      cell.viewOfGreen.backgroundColor = .white
//                cell.lblMessage.font = .boldSystemFont(ofSize: 15.0)
//                cell.dotView.isHidden = false
//            }
//
//            let modifiedFont = NSString(format:"<span style=\"font-family: \(UIFont.systemFontSize); font-size: \(15)\">%@</span>" as NSString, currentData["message"] as! NSString)
//
//            let attrStr = try! NSAttributedString(
//                data: modifiedFont.data(using: String.Encoding.unicode.rawValue, allowLossyConversion: true)!,
//                options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
//                documentAttributes: nil)
//
//            cell.lblMessage.text = currentData["message"] as? String ?? ""
//
//            cell.lblDesc.text = currentData["description"] as? String ?? ""
//
//
//
//
//            let dateAsString = (currentData["create_time"] as? String ?? "")
//
//            cell.lblDate.text = self.formatDateString(dateStr: dateAsString)
//
//
//            return  cell
//
//
//        }
        else if subType == notificationSubTypes.fetch_link && type == notificationTypes.family{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatNotificationCell", for: indexPath) as! ChatNotificationCell
            
            
            
            cell.btnThreedot.tag = indexPath.row
            
            
            let propic = currentData["propic"] as? String ?? ""
            
            let temp =  currentData["type"] as? String ?? ""
            
            if propic != ""{
                var temp = ""
                temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+propic
                
                
                let imgUrl = URL(string: temp)
                cell.imgOfPost.kf.indicatorType  = .activity
                
                //            cell.imgViewUser.kf.setImage(with: imgUrl, placeholder: nil, options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
                cell.imgOfPost.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Family Logo"), options: nil, progressBlock: nil, completionHandler: nil)
                
            }
            else{
                cell.imgOfPost.image = #imageLiteral(resourceName: "Family Logo")
            }
            cell.viewOfGreen.backgroundColor = .white

            if currentData["visible_status"] as! String == "read"{
        //        cell.viewOfGreen.backgroundColor = UIColor(rgb: 0xF2F2F2).withAlphaComponent(0.7)
                cell.lblMessage.font = .systemFont(ofSize: 15.0)
                cell.dotView.isHidden = true
            }
            else{
          //      cell.viewOfGreen.backgroundColor = .white
                cell.lblMessage.font = .boldSystemFont(ofSize: 15.0)
                cell.dotView.isHidden = true
            }
            
            let modifiedFont = NSString(format:"<span style=\"font-family: \(UIFont.systemFontSize); font-size: \(15)\">%@</span>" as NSString, currentData["message"] as! NSString)
            
            let attrStr = try! NSAttributedString(
                data: modifiedFont.data(using: String.Encoding.unicode.rawValue, allowLossyConversion: true)!,
                options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
                documentAttributes: nil)
            
            cell.lblMessage.text = currentData["message"] as? String ?? ""
            
            cell.lblDesc.text = currentData["description"] as? String ?? ""
            
            
            
            
            let dateAsString = (currentData["create_time"] as? String ?? "")
            
            cell.lblDate.text = self.formatDateString(dateStr: dateAsString)
            
            
            return  cell
            
            
        }
        else if subType == notificationSubTypes.requestFeed && type == notificationTypes.home{ 
            //                appDel.notificationData = currentData
            //                getMemberDetails(toRequest: true)
            
            
            //       if let id = currentData["type_id"] {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationRequestTableViewCell", for: indexPath) as! NotificationRequestTableViewCell
            
            
            cell.btnThreedot.tag = indexPath.row
            cell.btnSupport.tag = indexPath.row
            let propic = currentData["cover_image"] as? String ?? ""
            
            
            if propic != ""{
                var temp = ""
                temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+propic
                
                
                let imgUrl = URL(string: temp)
                cell.imgOfRequest.kf.indicatorType  = .activity
                
                //            cell.imgViewUser.kf.setImage(with: imgUrl, placeholder: nil, options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
                cell.imgOfRequest.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Family Logo"), options: nil, progressBlock: nil, completionHandler: nil)
                
            }
            else{
                cell.imgOfRequest.image = #imageLiteral(resourceName: "Family Logo")
            }
            let creatorPropic = currentData["created_by_propic"] as? String ?? ""
            
            
            if creatorPropic != ""{
                var temp = ""
                
                
                temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=50&height=50&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+creatorPropic
                
                
                let imgUrl = URL(string: temp)
                cell.imgOfCreator.kf.indicatorType  = .activity
                
                //            cell.imgViewUser.kf.setImage(with: imgUrl, placeholder: nil, options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
                cell.imgOfCreator.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
                
            }
            else{
                cell.imgOfCreator.image = #imageLiteral(resourceName: "Male Colored")
            }
            cell.viewOfGreen.backgroundColor = .white

            if currentData["visible_status"] as! String == "read"{
//                cell.viewOfGreen.backgroundColor = UIColor(rgb: 0xF2F2F2)
                cell.lblMessage.font = .systemFont(ofSize: 15)
                cell.dotView.isHidden = true
                
                cell.btnSupport.isHidden = true
                cell.heightOfBtn.constant = 0
                cell.bottomOfBtnSupport.constant = 0
            }
            else{
          //      cell.viewOfGreen.backgroundColor = .white
                cell.lblMessage.font = .boldSystemFont(ofSize: 15)
                cell.dotView.isHidden = false
                
                if currentData["category"] as? String ?? "" == "contribution_create" || currentData["category"] as? String ?? "" == "request"{
                    cell.btnSupport.isHidden = true
                    cell.heightOfBtn.constant = 0
                    cell.bottomOfBtnSupport.constant = 0

                }
                else
                {
                    cell.btnSupport.isHidden = false
                    cell.heightOfBtn.constant = 30
                    cell.bottomOfBtnSupport.constant = 15

                    
                }
            }
            
            
            cell.lblMessage.text = currentData["message"] as? String ?? ""
            
            
            let description = currentData["location"] as? String ?? ""
            if description == ""
            {
                cell.heightOfLblTitle.constant = 0
                cell.lblDescription.isHidden = true
            }
            else
            {
                cell.heightOfLblTitle.constant = 20
                cell.lblDescription.isHidden = false
                cell.lblDescription.text = description
                
            }
            
            cell.lblCreatorName.text = currentData["created_by_user"] as? String ?? ""
            //                                                            let privacy_type = currentData["privacy_type"] as! String
            //            if privacy_type == "public"
            //            {
            //                cell.lblType.text = currentData["privacy_type"] as? String ?? ""
            //
            //            }
            //            else
            //            {
            //                cell.lblType.text = currentData["to_group_name"] as? String ?? ""
            //
            //            }
            
            cell.lblType.isHidden = true
            cell.lblLine.isHidden = true
            let dateAsString = (currentData["create_time"] as? String ?? "")
            
            cell.lblDate.text = self.formatDateString(dateStr: dateAsString)
            
            
            return  cell
            //  }
            
            
        }
        else if subType == notificationSubTypes.item && type == notificationTypes.request{
            let cell = tableView.dequeueReusableCell(withIdentifier: "PublicPostCell", for: indexPath) as! PublicPostCell
            return cell//                            if let id = currentData["type_id"] {
            //                    getRequestDetails(id:"\(String(describing: id))")
            //                            }
        }
        else if subType == notificationSubTypes.about && type == notificationTypes.family{
            //                            if let id = currentData["type_id"] {
            //                    appDel.notificationData = currentData
            //                    goToFamilyDetails(id: "\(id)")
            //                            }
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "MemberJoinCell", for: indexPath) as! MemberJoinCell
            
            cell.btnThreedot.tag = indexPath.row
            cell.btnJoin.tag = indexPath.row
            cell.btnCancel.tag = indexPath.row
            cell.btnCancel.isHidden = true
            cell.btnJoin.setTitle("PAY", for: .normal)
            
            //                                                                          let str = currentData["message"] as! String
            //        // // print(str)
            //        if str.contains("</b>") {
            //            // // print("exists")
            //
            let propic = currentData["cover_image"] as? String ?? ""
            
            let temp =  currentData["type"] as? String ?? ""
            
            if propic != ""{
                var temp = ""
                temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+propic
                
                
                let imgUrl = URL(string: temp)
                cell.imgOfPost.kf.indicatorType  = .activity
                
                //            cell.imgViewUser.kf.setImage(with: imgUrl, placeholder: nil, options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
                cell.imgOfPost.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Family Logo"), options: nil, progressBlock: nil, completionHandler: nil)
                
            }
            else{
                cell.imgOfPost.image = #imageLiteral(resourceName: "Family Logo")
            }
            cell.viewOfGreen.backgroundColor = .white

            if currentData["visible_status"] as! String == "read"{
                
                
        //        cell.viewOfGreen.backgroundColor = UIColor(rgb: 0xF2F2F2).withAlphaComponent(0.7)
                cell.lblMessage.font = .systemFont(ofSize: 15.0)
                cell.dotView.isHidden = true
                
                cell.btnJoin.isHidden = true
                cell.btnCancel.isHidden = true
                cell.heightOfBtnJoin.constant = 0
                cell.topOfBtnStack.constant = 0

            }
            else{
                
          //      cell.viewOfGreen.backgroundColor = .white
                cell.lblMessage.font = .boldSystemFont(ofSize: 15.0)
                cell.dotView.isHidden = false
                
                cell.btnJoin.isHidden = false
                cell.btnCancel.isHidden = true
                cell.heightOfBtnJoin.constant = 30
                cell.topOfBtnStack.constant = 15

            }
            
            let modifiedFont = NSString(format:"<span style=\"font-family: \(UIFont.systemFontSize); font-size: \(15)\">%@</span>" as NSString, currentData["message"] as! NSString)
            
            let attrStr = try! NSAttributedString(
                data: modifiedFont.data(using: String.Encoding.unicode.rawValue, allowLossyConversion: true)!,
                options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
                documentAttributes: nil)
            
            cell.lblMessage.text = currentData["message"] as? String ?? ""
            cell.lblCreatorName.text = "By \(currentData["created_by_user"] as? String ?? "")"
            //                                          let privacy_type = currentData["privacy_type"] as! String
            
            cell.lblLocation.text = currentData["location"] as? String ?? ""
            let membercount = currentData["membercount"] as? String ?? ""
            if membercount == ""
            {
                cell.lblMemberCount.text = "0"
                cell.lblMemberHeading.text = "Member"
                
            }
            else
            {
                cell.lblMemberCount.text = membercount
                cell.lblMemberHeading.text = "Members"
                
            }
            
            
            let dateAsString = (currentData["create_time"] as? String ?? "")
            
            cell.lblDate.text = self.formatDateString(dateStr: dateAsString)
            
            
            return  cell
            
        }
      else  if type == notificationTypes.family{
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "MemberJoinCell", for: indexPath) as! MemberJoinCell
            //         heightOfBtnJoin
            
            cell.btnJoin.isHidden = true
            cell.btnCancel.isHidden = true
            cell.heightOfBtnJoin.constant = 0
            cell.topOfBtnStack.constant = 0

            cell.btnThreedot.tag = indexPath.row
            cell.btnJoin.tag = indexPath.row
            cell.btnCancel.tag = indexPath.row
          
                           
            
//            let propic = currentData["propic"] as? String ?? ""
            let propic = currentData["cover_image"] as? String ?? ""

            let temp =  currentData["type"] as? String ?? ""
            
            if propic != ""{
                var temp = ""
                temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+propic
                
                
                let imgUrl = URL(string: temp)
                cell.imgOfPost.kf.indicatorType  = .activity
                
                //            cell.imgViewUser.kf.setImage(with: imgUrl, placeholder: nil, options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
                cell.imgOfPost.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Family Logo"), options: nil, progressBlock: nil, completionHandler: nil)
                
            }
            else{
                cell.imgOfPost.image = #imageLiteral(resourceName: "Family Logo")
            }
            cell.viewOfGreen.backgroundColor = .white

            if currentData["visible_status"] as! String == "read"{
        //        cell.viewOfGreen.backgroundColor = UIColor(rgb: 0xF2F2F2).withAlphaComponent(0.7)
                cell.lblMessage.font = .systemFont(ofSize: 15.0)
                cell.dotView.isHidden = true
            }
            else{
          //      cell.viewOfGreen.backgroundColor = .white
                cell.lblMessage.font = .boldSystemFont(ofSize: 15.0)
                cell.dotView.isHidden = false
            }
            
            let modifiedFont = NSString(format:"<span style=\"font-family: \(UIFont.systemFontSize); font-size: \(15)\">%@</span>" as NSString, currentData["message"] as! NSString)
            
            let attrStr = try! NSAttributedString(
                data: modifiedFont.data(using: String.Encoding.unicode.rawValue, allowLossyConversion: true)!,
                options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
                documentAttributes: nil)
            
            cell.lblMessage.text = currentData["message"] as? String ?? ""
            
            
            
            
            
            cell.lblCreatorName.text = "By \(currentData["created_by_user"] as? String ?? "")"
            //                                          let privacy_type = currentData["privacy_type"] as! String
            
            cell.lblLocation.text = currentData["location"] as? String ?? ""
            let membercount = currentData["membercount"] as? String ?? ""
            if membercount == ""
            {
                cell.lblMemberCount.text = "0"
                cell.lblMemberHeading.text = "Member"
                
            }
            else
            {
                cell.lblMemberCount.text = membercount
                cell.lblMemberHeading.text = "Members"
                
            }
            
            
            let dateAsString = (currentData["create_time"] as! String)
            
            cell.lblDate.text = self.formatDateString(dateStr: dateAsString)
            
            
            return  cell
            
        }
            //                        else if type == notificationTypes.user{
            //                            let cell = tableView.dequeueReusableCell(withIdentifier: "PublicPostCell", for: indexPath) as! PublicPostCell
            //                                     return cell
            //
            //            //                let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            //            //                let intro = storyboard.instantiateViewController(withIdentifier: "UserProfileTableViewController") as! UserProfileTableViewController
            //            //                let userId = UserDefaults.standard.value(forKey: "userId") as! String
            //            //                intro.userID = userId
            //            //                self.navigationController?.pushViewController(intro, animated: false)
            //
            //                        }
            
        else if type == notificationTypes.announcement{
            //       if let id = currentData["type_id"] {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "PublicPostCell", for: indexPath) as! PublicPostCell
            
            
            cell.btnThreedot.tag = indexPath.row
            
            
            let propic = currentData["cover_image"] as? String ?? ""
            
            let temp =  currentData["type"] as? String ?? ""
            
            if propic != ""{
                var temp = ""
                temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+propic
                
                
                let imgUrl = URL(string: temp)
                cell.imgOfPost.kf.indicatorType  = .activity
                
                //            cell.imgViewUser.kf.setImage(with: imgUrl, placeholder: nil, options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
                cell.imgOfPost.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Family Logo"), options: nil, progressBlock: nil, completionHandler: nil)
                
            }
            else{
                cell.imgOfPost.image = #imageLiteral(resourceName: "Family Logo")
            }
            let creatorPropic = currentData["created_by_propic"] as? String ?? ""
            cell.btnThreedot.tag = indexPath.row
            
            
            if creatorPropic != ""{
                var temp = ""
                
                
                temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=50&height=50&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+creatorPropic
                
                
                let imgUrl = URL(string: temp)
                cell.imgOfCreator.kf.indicatorType  = .activity
                cell.imgOfCreator.contentMode = .redraw
                cell.imgOfCreator.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
                
            }
            else{
                cell.imgOfCreator.image = #imageLiteral(resourceName: "Male Colored")
            }
            cell.viewOfGreen.backgroundColor = .white

            if currentData["visible_status"] as! String == "read"{
        //        cell.viewOfGreen.backgroundColor = UIColor(rgb: 0xF2F2F2).withAlphaComponent(0.7)
                cell.lblMessage.font = .systemFont(ofSize: 15.0)
                cell.dotView.isHidden = true
            }
            else{
          //      cell.viewOfGreen.backgroundColor = .white
                cell.lblMessage.font = .boldSystemFont(ofSize: 15.0)
                cell.dotView.isHidden = false
            }
            
            let modifiedFont = NSString(format:"<span style=\"font-family: \(UIFont.systemFontSize); font-size: \(15)\">%@</span>" as NSString, currentData["message"] as! NSString)
            
            let attrStr = try! NSAttributedString(
                data: modifiedFont.data(using: String.Encoding.unicode.rawValue, allowLossyConversion: true)!,
                options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
                documentAttributes: nil)
            
            cell.lblMessage.text = currentData["message"] as? String ?? ""
            
            
            let description = currentData["description"] as? String ?? ""
            if description == ""
            {
                cell.heightOfLblTitle.constant = 0
                cell.lblTitle.isHidden = true
            }
            else
            {
                cell.heightOfLblTitle.constant = 20
                cell.lblTitle.isHidden = false
                cell.lblTitle.text = description
                
            }
            
            
            cell.lblCreatorName.text = currentData["created_by_user"] as? String ?? ""
            let privacy_type = currentData["privacy_type"] as? String ?? ""
            if privacy_type == "public"
            {
                cell.lblType.text = currentData["privacy_type"] as? String ?? ""
                
            }
            else
            {
                cell.lblType.text = currentData["to_group_name"] as? String ?? ""
                
            }
            
            
            let dateAsString = (currentData["create_time"] as? String ?? "")
            
            cell.lblDate.text = self.formatDateString(dateStr: dateAsString)
            
            
            return  cell
            //  }
        }
        else if type == notificationTypes.topic{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "TopicNotificationCell", for: indexPath) as! TopicNotificationCell
            
            
            
            cell.btnThreedot.tag = indexPath.row
            
            
            let propic = currentData["propic"] as? String ?? ""
            
            let temp =  currentData["type"] as? String ?? ""
            
            if propic != ""{
                var temp = ""
                temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+propic
                
                
                let imgUrl = URL(string: temp)
                cell.imgPropic.kf.indicatorType  = .activity
                cell.imgPropic.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Family Logo"), options: nil, progressBlock: nil, completionHandler: nil)
                
            }
            else{
                cell.imgPropic.image = #imageLiteral(resourceName: "Family Logo")
            }
            
            cell.imgOfPost.image = #imageLiteral(resourceName: "Family Logo")
            cell.viewOfGreen.backgroundColor = .white

            if currentData["visible_status"] as! String == "read"{
        //        cell.viewOfGreen.backgroundColor = UIColor(rgb: 0xF2F2F2).withAlphaComponent(0.7)
                cell.lblMessage.font = .systemFont(ofSize: 15.0)
                cell.dotView.isHidden = true
            }
            else{
          //      cell.viewOfGreen.backgroundColor = .white
                cell.lblMessage.font = .boldSystemFont(ofSize: 15.0)
                cell.dotView.isHidden = false
            }
            
            let modifiedFont = NSString(format:"<span style=\"font-family: \(UIFont.systemFontSize); font-size: \(15)\">%@</span>" as NSString, currentData["message"] as! NSString)
            
            let attrStr = try! NSAttributedString(
                data: modifiedFont.data(using: String.Encoding.unicode.rawValue, allowLossyConversion: true)!,
                options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
                documentAttributes: nil)
            
            cell.lblMessage.text = currentData["message"] as? String ?? ""
            
            cell.lblDesc.text = currentData["description"] as? String ?? ""
            
            cell.lblNsmr.text = currentData["created_by_user"] as? String ?? ""
            
            cell.lblDesc.isHidden = true
            let dateAsString = (currentData["create_time"] as? String ?? "")
            
            cell.lblDate.text = self.formatDateString(dateStr: dateAsString)
            
            
            return  cell
            
            
        }
        else
            
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatNotificationCell", for: indexPath) as! ChatNotificationCell
            
            
            
            cell.btnThreedot.tag = indexPath.row
            
            
            let propic = currentData["propic"] as? String ?? ""
            
            let temp =  currentData["type"] as? String ?? ""
            
            if propic != ""{
                var temp = ""
                temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+propic
                
                
                let imgUrl = URL(string: temp)
                cell.imgOfPost.kf.indicatorType  = .activity
                
                //            cell.imgViewUser.kf.setImage(with: imgUrl, placeholder: nil, options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
                cell.imgOfPost.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Family Logo"), options: nil, progressBlock: nil, completionHandler: nil)
                
            }
            else{
                cell.imgOfPost.image = #imageLiteral(resourceName: "Family Logo")
            }
            cell.viewOfGreen.backgroundColor = .white

            if currentData["visible_status"] as! String == "read"{
        //        cell.viewOfGreen.backgroundColor = UIColor(rgb: 0xF2F2F2).withAlphaComponent(0.7)
                cell.lblMessage.font = .systemFont(ofSize: 15.0)
                cell.dotView.isHidden = true
            }
            else{
          //      cell.viewOfGreen.backgroundColor = .white
                cell.lblMessage.font = .boldSystemFont(ofSize: 15.0)
                cell.dotView.isHidden = false
            }
            
            let modifiedFont = NSString(format:"<span style=\"font-family: \(UIFont.systemFontSize); font-size: \(15)\">%@</span>" as NSString, currentData["message"] as! NSString)
            
            let attrStr = try! NSAttributedString(
                data: modifiedFont.data(using: String.Encoding.unicode.rawValue, allowLossyConversion: true)!,
                options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
                documentAttributes: nil)
            
            cell.lblMessage.text = currentData["message"] as? String ?? ""
            
            cell.lblDesc.text = currentData["description"] as? String ?? ""
            
            
            
            
            let dateAsString = (currentData["create_time"] as? String ?? "")
            
            cell.lblDate.text = self.formatDateString(dateStr: dateAsString)
            
            
            return  cell
            
            
            
        }
        //        else
        //        {
        //        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell", for: indexPath) as! NotificationTableViewCell
        //        let currentNotification = self.arrayOfNotificationList[indexPath.row]
        //
        ////        let currentData = self.tempNotifi[indexPath.row]
        //        let currentData = currentNotification["value"] as! [String:Any]
        //        // // print(currentData)
        //
        //        let propic = currentData["propic"] as? String ?? ""
        //        cell.btnThreeDot.tag = indexPath.row
        //
        //        let temp =  currentData["type"] as? String ?? ""
        //
        //        //        // // print("########## \(temp)")
        //
        //        //  let linkedPage = currentData["link_to"] as? String ?? ""
        ////        let linkedPage = temp
        //
        //        if propic != ""{
        //            var temp = ""
        //
        //
        //            temp  = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+propic
        //
        //
        //            let imgUrl = URL(string: temp)
        //            cell.imgViewUser.kf.indicatorType  = .activity
        //
        //            //            cell.imgViewUser.kf.setImage(with: imgUrl, placeholder: nil, options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
        //            cell.imgViewUser.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
        //
        //        }
        //        else{
        //            cell.imgViewUser.image = #imageLiteral(resourceName: "Male Colored")
        //        }
        //        if currentData["visible_status"] as? String ?? "" == "unread"{
        //            cell.viewOfGreen.backgroundColor = #colorLiteral(red: 0.9254901961, green: 0.9254901961, blue: 0.9254901961, alpha: 1)
        //        }
        //        else{
        //            cell.viewOfGreen.backgroundColor = .white
        //        }
        //
        //        let modifiedFont = NSString(format:"<span style=\"font-family: \(UIFont.systemFontSize); font-size: \(15)\">%@</span>" as NSString, currentData["message"] as! NSString)
        //
        //        let attrStr = try! NSAttributedString(
        //            data: modifiedFont.data(using: String.Encoding.unicode.rawValue, allowLossyConversion: true)!,
        //            options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
        //            documentAttributes: nil)
        //
        //        cell.lblSecond.text = currentData["message"] as? String ?? ""
        //
        //        let dateAsString = (currentData["create_time"] as? String ?? "")
        //
        //         cell.lblThird.text = self.formatDateString(dateStr: dateAsString)
        //
        //        //           let htmlData = NSString(string: currentData["message"] as? String ?? "").data(using: String.Encoding.utf8.rawValue)
        //        //                let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html ,  NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20)] as [AnyHashable : Any]
        //        //        //        let options = [NSAttributedString.DocumentAttributeKey.documentType: NSAttributedString.DocumentType.html, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20)] as [AnyHashable : Any]
        //        //        //
        //        //                let attributedString = try! NSAttributedString(data: htmlData!,
        //        //                                                               options: options as! [NSAttributedString.DocumentReadingOptionKey : Any] ,
        //        //                                                               documentAttributes: nil)
        //        //                // // print(attributedString)
        //        //
        //        //
        //        //
        //        //                // set attributed text on a UILabel
        //        //                cell.lblSecond.attributedText = attributedString
        //
        //
        //
        //
        //
        //
        //        //        // // print(dateAsString)
        ////        cell.lblThird.text = "\(self.convertDateToDisplayDate(dateFromResponse: dateAsString))"
        //
        //        return  cell
        //
        //        }
        
    }
    func convertTimeStampToDate(timestamp:String)->String
    {
        if timestamp == "0"
        {
            return ""
        }
        else
        {
            let date = Date(timeIntervalSince1970: Double(timestamp) as! TimeInterval)
            
            let formatter = DateFormatter()
            formatter.dateFormat = "dd MMM yyyy"
            //        formatter.timeZone = TimeZone(identifier: "UTC")
            formatter.timeZone = NSTimeZone.local
            
            
            let localDatef = formatter.string(from: date)
            // // print(localDatef)
            return localDatef
        }
        
    }
    @IBAction func onClickCellThreeDot(_ sender: UIButton) {
        //        postoptionsTittle.removeAll()
        let currentNotification = self.arrayOfNotificationList[sender.tag]
        // // print(currentNotification)
        
        //        let currentData = self.tempNotifi[indexPath.row]
        let currentData = currentNotification["value"] as! [String:Any]
        // // print(currentData)
        
        
        // // print(self.arrayOfNotificationList[sender.tag])
        
        var postoptionsTittle = [String]()
        
        if currentData["visible_status"] as! String == "read"
        {
            postoptionsTittle = ["Delete"]
            
        }
        else
        {
            postoptionsTittle = ["Delete","Mark as read"]
            
        }
        showActionSheet(titleArr: postoptionsTittle as NSArray, title: "Select options") { (index) in
            if index == 0{
                let alert = UIAlertController(title: "", message: "Do you want to delete this notification?", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "YES", style: .destructive, handler: { (acion) in
                    self.deleteSingleNotifications(index:sender.tag)
                }))
                
                alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
                
                self.present(alert, animated: true, completion: nil)
            }
                
            else if index == 1 {
                // // print("read selected")
                let currentNotification = self.arrayOfNotificationList[sender.tag]
                // // print(currentNotification)
                //                    self.changeReadStatus()
                self.changeReadStatusSingle(selectedData: currentNotification, index: sender.tag)
                
            }
            else
            {
                //                    self.switchNotifications()
            }
            
        }
    }
    
    @IBAction func btnSupport_OnClick(_ sender: UIButton) {
        let currentDatawithKey = self.arrayOfNotificationList[sender.tag]
        let currentData = currentDatawithKey["value"] as! [String:Any]
        
        self.changeReadStatusSingle(selectedData:currentDatawithKey, index: sender.tag)
        
        let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "RequestDetailsViewController") as! RequestDetailsViewController
        vc.requestId = "\(currentData["type_id"] as! Int)"
        vc.fromNotiButton = true
        //        vc.requestDic = need
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    func familyJoin( userId: String, groupId: String, type: String, status: String, respondedId: String,currentDatawithKey:[String:Any],index:Int,forceUpdate:Bool)
    {
        if !Connectivity.isConnectedToInternet(){
                      self.displayAlertChoice(alertStr: "No Internet! Please Check your connectivity", title: "") { (result) in
                          //                self.getFamilyDetails(groupId: self.groupId)
                      }
                  }
        
        
            
            var parameter = [String:Any]()
            
           
            if forceUpdate
            {
                parameter = [
                                    "user_id":userId,
                                    "type":type,
                                    "status":status,
                                    "responded_by":respondedId,
                                    "group_id":groupId,
                                    "is_force_update":"true"
                                ]
        }
        else
            {
            parameter = [
                      "user_id":userId,
                      "type":type,
                      "status":status,
                      "responded_by":respondedId,
                      "group_id":groupId
                  ]
                    }
              print(parameter)
            ActivityIndicatorView.show("Loading....")
            
            networkProvider.request(.familyJoinFromNoti(parameter: parameter)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                         print(jsonData)
                        ActivityIndicatorView.hiding()
                        if response.statusCode == 200{
                            
                            self.changeReadStatusSingle(selectedData:currentDatawithKey, index: index)
                            
                            let message = jsonData["data"][0]["message"].string ?? ""
                            let action = jsonData["data"][0]["action"].string ?? ""

                            if message == ""
                            {
                                Helpers.showAlertDialog(message: "Successfully Updated", target: self)

                            }
                            else
                            {
                                if action  ==  status
                                {
                                    Helpers.showAlertDialog(message: message
                                    , target: self)
                                }
                                else
                                {
                                    let alert = UIAlertController(title: "", message: "\(message). Do you want to continue ?", preferredStyle: .actionSheet)
                                           alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { _ in
                                            
                                            self.familyJoin( userId: userId, groupId: groupId, type: type, status: status, respondedId: respondedId,currentDatawithKey:currentDatawithKey,index:index,forceUpdate:true)
                                           }))
                                           
                                          
                                           
                                           alert.addAction(UIAlertAction.init(title: "No", style: .cancel, handler: nil))
                                           self.present(alert, animated: true, completion: nil)
                                }
                            }
                        }
                        else if response.statusCode == 401 {
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                self.familyJoin( userId: userId, groupId: groupId, type: type, status: status, respondedId: respondedId,currentDatawithKey:currentDatawithKey,index:index,forceUpdate:forceUpdate)
                            }
                        }
                        
                        ActivityIndicatorView.hiding()
                        
                    }
                    //                catch let err {
                    //                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                //                }
                case .failure( let error):
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                    self.changeReadStatusSingle(selectedData:currentDatawithKey, index: index)

                    break
                }
                
                ActivityIndicatorView.hiding()
            }
            
        
    }
    func family_linkRequstJoin( from_id:String ,userId: String, type_id: String, type: String, status: String,currentDatawithKey:[String:Any],index:Int,forceUpdate:Bool)
    {
        if !Connectivity.isConnectedToInternet(){
                      self.displayAlertChoice(alertStr: "No Internet! Please Check your connectivity", title: "") { (result) in
                          //                self.getFamilyDetails(groupId: self.groupId)
                      }
                  }
        
        
            
            var parameter = [String:Any]()
            
           
            
             if forceUpdate
                     {
                         parameter = [
                             "from_group":from_id,
                             "to_group":type_id,
                             "type":type,
                             "status":status,
                             "responded_by":userId,
                            "is_force_update":"true"
                                         ]
                 }
                 else
                     {
                     parameter = [
                         "from_group":from_id,
                         "to_group":type_id,
                         "type":type,
                         "status":status,
                         "responded_by":userId
                     ]
                             }
              print(parameter)
            ActivityIndicatorView.show("Loading....")
            
            networkProvider.request(.familyJoinFromNoti(parameter: parameter)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                         print(jsonData)
                        ActivityIndicatorView.hiding()
                        if response.statusCode == 200{
                            
                            self.changeReadStatusSingle(selectedData:currentDatawithKey, index: index)
                            
                            let message = jsonData["data"][0]["message"].string ?? ""
                            let action = jsonData["data"][0]["action"].string ?? ""

                            if message == ""
                            {
                                Helpers.showAlertDialog(message: "Successfully Updated", target: self)

                            }
                            else
                            {
                                if action  ==  status
                                {
                                    Helpers.showAlertDialog(message: message
                                    , target: self)
                                }
                                else
                                {
                                    let alert = UIAlertController(title: "", message: "\(message). Do you want to continue ?", preferredStyle: .actionSheet)
                                           alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { _ in
                                            
                                            self.family_linkRequstJoin( from_id:from_id ,userId: userId, type_id: type_id, type: type, status: status,currentDatawithKey:currentDatawithKey,index:index,forceUpdate:true)
                                           }))
                                           
                                          
                                           
                                           alert.addAction(UIAlertAction.init(title: "No", style: .cancel, handler: nil))
                                           self.present(alert, animated: true, completion: nil)
                                }
                            }
                        }
                        else if response.statusCode == 401 {
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                self.family_linkRequstJoin( from_id:from_id ,userId: userId, type_id: type_id, type: type, status: status,currentDatawithKey:currentDatawithKey,index:index,forceUpdate:forceUpdate)
                            }
                        }
                        
                        ActivityIndicatorView.hiding()
                        
                    }
                    //                catch let err {
                    //                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                //                }
                case .failure( let error):
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                    self.changeReadStatusSingle(selectedData:currentDatawithKey, index: index)

                    break
                }
                
                ActivityIndicatorView.hiding()
            }
            
        
    }
    @IBAction func btnJoinOnClick(_ sender: UIButton) {
        let currentDatawithKey = self.arrayOfNotificationList[sender.tag]
        // // print(currentDatawithKey)
        let currentData = currentDatawithKey["value"] as! [String:Any]
        print(currentData)
        let type = currentData["type"] as! String
        let subType = currentData["sub_type"] as! String
        if subType == notificationSubTypes.about && type == notificationTypes.family{
            
            self.changeReadStatusSingle(selectedData:currentDatawithKey, index: sender.tag)
            
            if let id = currentData["type_id"] {
                appDel.notificationData = currentData
                goToFamilyDetails(id: "\(id)",fromButtonAction: true)
                
                
            }
        }
        else if subType == notificationSubTypes.request && type == notificationTypes.family{
            // // print(sender.tag)
            // // print(currentData)
            
            if !Connectivity.isConnectedToInternet(){
                self.displayAlertChoice(alertStr: "No Internet! Please Check your connectivity", title: "") { (result) in
                    //                self.getFamilyDetails(groupId: self.groupId)
                }
            }
            
            self.familyJoin( userId: "\(currentData["from_id"] as? Int ?? 0)", groupId: "\(currentData["type_id"] as! Int)", type: "request", status: "accepted", respondedId: UserDefaults.standard.value(forKey: "userId") as! String,currentDatawithKey:currentDatawithKey,index:sender.tag,forceUpdate:false)
//            APIServiceManager.callServer.groupRequestResponseNoti(url: EndPoint.groupRequestResp, userId: "\(currentData["from_id"] as? Int ?? 0)", groupId: "\(currentData["type_id"] as! Int)", type: "request", status: "accepted", respondedId: UserDefaults.standard.value(forKey: "userId") as! String, success: { (response) in
//                ActivityIndicatorView.hiding()
//                self.changeReadStatusSingle(selectedData:currentDatawithKey, index: sender.tag)
//
//                if let results = response as! requestSuccessModel?{
//
//                    if results.status_code == 200{
//
//
//                        //                            self.getNotifications()
//
//                    }
//                }
//
//            }) { (error) in
//                ActivityIndicatorView.hiding()
//                self.changeReadStatusSingle(selectedData:currentDatawithKey, index: sender.tag)
//
//            }
//
        }
        else if subType == notificationSubTypes.request && type == notificationTypes.user{
            // // print(sender.tag)
            // // print(currentData)
            
            if !Connectivity.isConnectedToInternet(){
                self.displayAlertChoice(alertStr: "No Internet! Please Check your connectivity", title: "") { (result) in
                    //                self.getFamilyDetails(groupId: self.groupId)
                }
            }
            
            APIServiceManager.callServer.invitationResponse(url: EndPoint.invitationResp, tableId: "",user_id: UserDefaults.standard.value(forKey: "userId") as! String , group_id: "\(currentData["group_id"] as! Int)", status: "accepted", fromId: "\(currentData["from_id"] as! Int)", success: { (response) in
                ActivityIndicatorView.hiding()
                self.changeReadStatusSingle(selectedData:currentDatawithKey, index: sender.tag)

                if let results = response as! requestSuccessModel?{
                    
                    if results.status_code == 200{
//                        self.changeReadStatusSingle(selectedData:currentDatawithKey, index: sender.tag)
                        
                        //                            self.getNotifications()
                        
                    }
                }
                
            }) { (error) in
                ActivityIndicatorView.hiding()
                //                     Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                self.changeReadStatusSingle(selectedData:currentDatawithKey, index: sender.tag)

                
            }
            
        }
        else if subType == notificationSubTypes.link_family && type == notificationTypes.family{
            let typeId = currentData["type_id"] as? Int ?? 0
            let fromID = currentData["from_id"] as? Int ?? 0
            
            
            if fromID == 0{
                //self.displayAlert(alertStr: "Something went wrong...", title: "")
                return
            }
            else if typeId == 0{
//                self.displayAlert(alertStr: "Something went wrong...", title: "")
                return
            }
            else{
               
                family_linkRequstJoin( from_id:"\(currentData["from_id"] as! Int)" ,userId: UserDefaults.standard.value(forKey: "userId") as! String, type_id: "\(currentData["type_id"] as! Int)", type: "fetch_link", status: "accepted",currentDatawithKey:currentDatawithKey,index:sender.tag,forceUpdate:false)
//                APIServiceManager.callServer.groupRequestResponseFromNotification(url: EndPoint.groupRequestResp, from_id: "\(currentData["from_id"] as! Int)", userId: UserDefaults.standard.value(forKey: "userId") as! String, type_id: "\(currentData["type_id"] as! Int)", type: "fetch_link", status: "accepted", success: { (responseMdl) in
//                    self.changeReadStatusSingle(selectedData:currentDatawithKey, index: sender.tag)
//
//                    guard let result = responseMdl as? requestSuccessModel else{
//                        return
//                    }
//
//                    if result.status_code == 200{
//                        //                                            self.getGroupRequests(grpId: self.groupID)
//                    }
//                    else{
//                        ActivityIndicatorView.hiding()
//                    }
//
//                }) { (error) in
//                    ActivityIndicatorView.hiding()
//                    self.changeReadStatusSingle(selectedData:currentDatawithKey, index: sender.tag)
//
//                }
            }
        }
        
    }
    
    
    
    @IBAction func btnMemberCancelOnClick(_ sender: UIButton) {
        let currentDatawithKey = self.arrayOfNotificationList[sender.tag]
        // // print(currentDatawithKey)
        let currentData = currentDatawithKey["value"] as! [String:Any]
        print(currentData)
        let type = currentData["type"] as! String
        let subType = currentData["sub_type"] as! String
        if subType == notificationSubTypes.request && type == notificationTypes.family{
            // // print(sender.tag)
            // // print(currentData)
            //
            if !Connectivity.isConnectedToInternet(){
                self.displayAlertChoice(alertStr: "No Internet! Please Check your connectivity", title: "") { (result) in
                    //                self.getFamilyDetails(groupId: self.groupId)
                }
            }
            self.familyJoin( userId: "\(currentData["from_id"] as? Int ?? 0)", groupId: "\(currentData["type_id"] as! Int)", type: "request", status: "rejected", respondedId: UserDefaults.standard.value(forKey: "userId") as! String,currentDatawithKey:currentDatawithKey,index:sender.tag,forceUpdate:false)
//            APIServiceManager.callServer.groupRequestResponseNoti(url: EndPoint.groupRequestResp, userId: "\(currentData["from_id"] as? Int ?? 0)", groupId: "\(currentData["type_id"] as! Int)", type: "request", status: "rejected", respondedId: UserDefaults.standard.value(forKey: "userId") as! String, success: { (response) in
//                ActivityIndicatorView.hiding()
//                self.changeReadStatusSingle(selectedData:currentDatawithKey, index: sender.tag)
//
//                if let results = response as! requestSuccessModel?{
//
//                    if results.status_code == 200{
////                        self.changeReadStatusSingle(selectedData:currentDatawithKey, index: sender.tag)
//
//                        //                            self.getNotifications()
//
//                    }
//                }
//
//            }) { (error) in
//                ActivityIndicatorView.hiding()
//                self.changeReadStatusSingle(selectedData:currentDatawithKey, index: sender.tag)
//
//            }
            
        }
            
        else if subType == notificationSubTypes.request && type == notificationTypes.user{
            // // print(sender.tag)
            // // print(currentData)
            //
            if !Connectivity.isConnectedToInternet(){
                self.displayAlertChoice(alertStr: "No Internet! Please Check your connectivity", title: "") { (result) in
                    //                self.getFamilyDetails(groupId: self.groupId)
                }
            }
            
            APIServiceManager.callServer.invitationResponse(url: EndPoint.invitationResp, tableId: "", user_id:UserDefaults.standard.value(forKey: "userId") as! String , group_id: "\(currentData["group_id"] as! Int)", status: "rejected", fromId:"\(currentData["from_id"] as! Int)" , success: { (response) in
                ActivityIndicatorView.hiding()
                self.changeReadStatusSingle(selectedData:currentDatawithKey, index: sender.tag)

                if let results = response as! requestSuccessModel?{
//
                    if results.status_code == 200{
//                        self.changeReadStatusSingle(selectedData:currentDatawithKey, index: sender.tag)
                        
                    }
                }
                
            }) { (error) in
                ActivityIndicatorView.hiding()
                self.changeReadStatusSingle(selectedData:currentDatawithKey, index: sender.tag)

                //                         Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                
            }
            
        }
        else if subType == notificationSubTypes.link_family && type == notificationTypes.family
        {
            let typeId = currentData["type_id"] as? Int ?? 0
            let fromID = currentData["from_id"] as? Int ?? 0
            
            if fromID == 0{
//                self.displayAlert(alertStr: "Something went wrong", title: "")
                return
            }
            else if typeId == 0{
//                self.displayAlert(alertStr: "Something went wrong", title: "")
                return
            }
            else{
               
                
                  family_linkRequstJoin( from_id:"\(currentData["from_id"] as! Int)" ,userId: UserDefaults.standard.value(forKey: "userId") as! String, type_id: "\(currentData["type_id"] as! Int)", type: "fetch_link", status: "rejected",currentDatawithKey:currentDatawithKey,index:sender.tag,forceUpdate:false)
//                              APIServiceManager.callServer.groupRequestResponseFromNotification(url: EndPoint.groupRequestResp, from_id: "\(currentData["from_id"] as! Int)", userId: UserDefaults.standard.value(forKey: "userId") as! String, type_id: "\(currentData["type_id"] as! Int)", type: "fetch_link", status: "rejected", success: { (responseMdl) in
//                    //                                tempDic.type
//                    self.changeReadStatusSingle(selectedData:currentDatawithKey, index: sender.tag)
//
//                    guard let result = responseMdl as? requestSuccessModel else{
//                        return
//                    }
//
//                    if result.status_code == 200{
//                    }
//                    else{
//                        ActivityIndicatorView.hiding()
//                    }
//
//                }) { (error) in
//                    ActivityIndicatorView.hiding()
//                    self.changeReadStatusSingle(selectedData:currentDatawithKey, index: sender.tag)
//
//                }
            }
        }
    }
    
   
   
     
     
    
    
    func onClickResponseToRSVP(status:String, otherCount:String,data:[String:Any],currentDatawithKey:[String:Any],index:Int){
        
        var parameter = [String:Any]()
        
        let id = data["type_id"] as! Int
        
        parameter = ["user_id":UserDefaults.standard.value(forKey: "userId") as! String, "event_id":"\(id)", "resp":status, "others_count":otherCount,"created_by":"\(data["created_by_user"] as? String ?? "")"] as [String : Any]
        //        }
        // // print(parameter)
        ActivityIndicatorView.show("Loading....")
        
        networkProvider.request(.respondToRSVP(parameter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    // // print(jsonData)
                    ActivityIndicatorView.hiding()
                    if response.statusCode == 200{
                        
                        self.changeReadStatusSingle(selectedData:currentDatawithKey, index: index)
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.onClickResponseToRSVP(status: status, otherCount: otherCount, data: data, currentDatawithKey: currentDatawithKey, index: index)
                        }
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }
                //                catch let err {
                //                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
            //                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
            
            ActivityIndicatorView.hiding()
        }
        
    }
    @IBAction func btnNotInterestOnClick(_ sender: UIButton) {
        let currentDatawithKey = self.arrayOfNotificationList[sender.tag]
        let currentData = currentDatawithKey["value"] as! [String:Any]
        
        
        onClickResponseToRSVP(status: "not-going", otherCount: "0",data:currentData,currentDatawithKey:currentDatawithKey, index: sender.tag)
        
    }
    
    @IBAction func btnInterestOnClick(_ sender: UIButton) {
        let currentDatawithKey = self.arrayOfNotificationList[sender.tag]
        let currentData = currentDatawithKey["value"] as! [String:Any]
        
        
        onClickResponseToRSVP(status: "interested", otherCount: "0",data:currentData,currentDatawithKey:currentDatawithKey, index: sender.tag)
        
    }
    
    @IBAction func btnGoingOnClick(_ sender: UIButton) {
        let currentDatawithKey = self.arrayOfNotificationList[sender.tag]
        let currentData = currentDatawithKey["value"] as! [String:Any]
        
        
        onClickResponseToRSVP(status: "going", otherCount: "0",data:currentData,currentDatawithKey:currentDatawithKey, index: sender.tag)
        
    }
    func convertDateToDisplayDate(dateFromResponse:String)->String
    {
        if dateFromResponse != ""{
            
            let formatter         = DateFormatter()
            formatter.dateStyle   = .long
            
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            formatter.timeZone = TimeZone(identifier: "UTC")
            
            
            let result            = formatter.date(from: "\(dateFromResponse)")
            let formatter2        = DateFormatter()
            formatter2.dateStyle  = .long
            formatter2.dateFormat = "MMM dd yyyy hh:mm a"
            
            let val               = formatter2.string(from: result!)
            return val
            
        }else
        {
            return ""
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentDatawithKey = self.arrayOfNotificationList[indexPath.row]
        let currentData = currentDatawithKey["value"] as! [String:Any]
        //        appDel.fromNavigationBack = true
        
        self.changeReadStatusSingle(selectedData:currentDatawithKey, index: indexPath.row)
        let type = currentData["type"] as! String
        let subType = currentData["sub_type"] as! String
        
        if subType == notificationSubTypes.guest_interested || subType == notificationSubTypes.guest_attending{
            if let id = currentData["type_id"] {
                
                let story = UIStoryboard(name: "third", bundle: nil)
                let vc = story.instantiateViewController(withIdentifier: "GuestDetailsViewController") as! GuestDetailsViewController
                if subType == notificationSubTypes.guest_interested{
                    vc.selectedIndex = 1
                }
                else{
                    vc.selectedIndex = 0
                }
                vc.eventId = "\(String(describing: id))"
                self.navigationController?.pushViewController(vc, animated: false)
            }
            return
        }
        else if subType == notificationSubTypes.calendar{
            let CalendarEventsViewController = self.storyboard?.instantiateViewController(withIdentifier: "CalendarEventsViewController") as! CalendarEventsViewController
            CalendarEventsViewController.isFromExplore = ""
            self.navigationController?.pushViewController(CalendarEventsViewController, animated: false)
            return
        }
        else if subType == notificationSubTypes.member{
            appDel.notificationData = currentData
            getMemberDetails(toRequest: false)
            return
        }
        else if subType == notificationSubTypes.membership{
            appDel.notificationData = currentData
            getMemberDetails(toRequest: false)
            return
        }
        else if subType == notificationSubTypes.request && type == notificationTypes.user{
            
            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            let intro = storyboard.instantiateViewController(withIdentifier: "UserProfileTableViewController") as! UserProfileTableViewController
            let userId = UserDefaults.standard.value(forKey: "userId") as! String
            intro.userID = userId
            intro.isFromNotification = true
            self.navigationController?.pushViewController(intro, animated: false)
            return
            
        }
        else if subType == notificationSubTypes.request && type == notificationTypes.family{
            appDel.notificationData = currentData
            getMemberDetails(toRequest: true)
            return
            
        }
        else  if (subType == notificationSubTypes.publicfeed || subType == notificationSubTypes.familyfeed) && type == notificationTypes.home{
            if let id = currentData["type_id"] {
                
                let storyboard = UIStoryboard.init(name: "PostDetails", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
                vc.postId = "\(id)"
                self.navigationController?.pushViewController(vc, animated: false)
            }
            return
        }
        else if subType == notificationSubTypes.conversation && type == notificationTypes.post{
            if let id = currentData["type_id"] {
                getPostDetails(id: "\(String(describing: id))")
            }
        }
        else if subType == notificationSubTypes.conversation && type == notificationTypes.announcement{
            if let id = currentData["type_id"] {
                getAnnouncementDetailsConversation(id: "\(String(describing: id))")
                return
            }
            return
        }
        else if subType == notificationSubTypes.link_family && type == notificationTypes.family{
            appDel.notificationData = currentData
            getMembersDetailsNdGotoLinkedFamilies()
            return
        }
        else if subType == notificationSubTypes.fetch_link && type == notificationTypes.family{
            appDel.notificationData = currentData
            getMembersDetailsNdGotoLinkedFamilies()
            return
            
        }
        else if subType == notificationSubTypes.requestFeed{
            appDel.notificationData = currentData
            if let id = currentData["type_id"] {
                getRequestDetails(id:"\(String(describing: id))")
                return
            }
            return
        }
        else if subType == notificationSubTypes.item && type == notificationTypes.request{
            appDel.notificationData = currentData
            if let id = currentData["type_id"] {
                getRequestDetails(id:"\(String(describing: id))")
                return
            }
            return
        }
        else if subType == notificationSubTypes.about && type == notificationTypes.family{
            if let id = currentData["type_id"] {
                appDel.notificationData = currentData
                goToFamilyDetails(id: "\(id)",fromButtonAction:false)
                return
            }
            
        }
        if type == notificationTypes.family{
            if let id = currentData["type_id"] {
                let storyboard = UIStoryboard.init(name: "second", bundle: nil)
                let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
                intro.groupId = "\(String(describing: id))"
                self.navigationController?.pushViewController(intro, animated: false)
            }
        }
        else if type == notificationTypes.user{
            
            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            let intro = storyboard.instantiateViewController(withIdentifier: "UserProfileTableViewController") as! UserProfileTableViewController
            let userId = UserDefaults.standard.value(forKey: "userId") as! String
            intro.userID = userId
            self.navigationController?.pushViewController(intro, animated: false)
            
        }
        else if type == notificationTypes.event{
            if let id = currentData["type_id"] {
                let storyboard = UIStoryboard.init(name: "third", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
                vc.isFromNotification = "true"
                vc.enableEventEdit = false
                vc.eventId = "\(String(describing: id))"
                self.navigationController?.pushViewController(vc, animated: false)
            }
        }
        else if type == notificationTypes.announcement{
            if let id = currentData["type_id"] {
                getAnnouncementDetails(id: "\(String(describing: id))")
            }
        }
        else if type == notificationTypes.topic{
            if let id = currentData["type_id"] {
                let storyboard = UIStoryboard.init(name: "Topic", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "TopicsConversationViewController") as! TopicsConversationViewController
                vc.topicID = "\(String(describing: id))"
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
        }
        
        
        
        
        
        //###### OLD  CODE #######
        
        /*   if type == notificationTypes.family{
         let storyboard = UIStoryboard.init(name: "second", bundle: nil)
         let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
         intro.groupId = "\(currentData["link_to"]!)"
         //          intro.groupId = "\(currentData["type_id"]!)"
         intro.isFromNotification = true
         
         self.navigationController?.pushViewController(intro, animated: false)
         }
         else if type == notificationTypes.user{
         let storyboard = UIStoryboard.init(name: "second", bundle: nil)
         let vc = storyboard.instantiateViewController(withIdentifier: "UserProfileTableViewController") as! UserProfileTableViewController
         vc.userID = "\(currentData["link_to"]!)"
         //            vc.userID = "\(currentData["type_id"]!)"
         self.navigationController?.pushViewController(vc, animated: true)
         }
         else if type.lowercased() == notificationTypes.post{
         let i = navigationController?.viewControllers.firstIndex(of: self)
         let previousViewController = navigationController?.viewControllers[i!-1]
         if previousViewController is HomeTabViewController {
         self.navigationController?.popViewController(animated: false)
         } else {
         self.tabBarController?.selectedIndex = 0
         }
         }
         else{
         let storyboard = UIStoryboard.init(name: "third", bundle: nil)
         let vc = storyboard.instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
         vc.isFromNotification = "true"
         vc.enableEventEdit = false
         vc.eventId = "\(currentData["link_to"]!)"
         //            vc.eventId = "\(currentData["type_id"]!)"
         self.navigationController?.pushViewController(vc, animated: true)
         }*/
    }
    
}


//MARK:- from table cellFor
//        cell.lblFirst.text = (currentData["type"] as! String)
//        cell.lblSecond.text = (currentData["message"] as! String)


//        var attrStr = try! NSAttributedString(
//            data: currentData["message"].data,(using: String.Encoding.unicode, allowLossyConversion: true)!,
//            options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
//            documentAttributes: nil)
//        cell.lblSecond.attributedText = attrStr
//

//        "<b>Dhilna Abraham</b> has added you to <b>block user</b>"
///working code
//        let str = currentData["message"] as! String
//        // // print(str)
//        if str.contains("</b>") {
//            // // print("exists")
//
//        let array = str.components(separatedBy: "</b>")
//        if array.count != 0
//        {
//        let nameFirst = array[0].components(separatedBy: "<b>")
//        // // print(nameFirst[1])
//        let nameMiddle = array[1].components(separatedBy: "<b>")
//        // // print(nameMiddle[0])
//        let nameLast = nameMiddle[1].components(separatedBy: "</b>")
//        // // print(nameLast[0])
//            let yourAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.black, .font: UIFont.boldSystemFont(ofSize: 15)]
//            let yourOtherAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.darkGray, .font: UIFont.systemFont(ofSize: 15)]
//
//            let partOne = NSMutableAttributedString(string: nameFirst[1], attributes: yourAttributes)
//            let partTwo = NSMutableAttributedString(string: nameMiddle[0], attributes: yourOtherAttributes)
//            let partThree = NSMutableAttributedString(string: nameLast[0], attributes: yourAttributes)
//
//            partOne.append(partTwo)
//            partOne.append(partThree)
//            // // print(partOne)
//            cell.lblSecond.attributedText = partOne
//
//
//        }
//        else{
//            let yourOtherAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.darkGray, .font: UIFont.systemFont(ofSize: 15)]
//            let partOne = NSMutableAttributedString(string: currentData["message"] as! String, attributes: yourOtherAttributes)
//            // // print(partOne)
//            cell.lblSecond.attributedText = partOne
//
//
//
//        }
//    }
//        else{
//            let yourOtherAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.darkGray, .font: UIFont.systemFont(ofSize: 15)]
//            let partOne = NSMutableAttributedString(string: currentData["message"] as! String, attributes: yourOtherAttributes)
//            // // print(partOne)
//            cell.lblSecond.attributedText = partOne
//
//        }

/////end working code

////second type working code
//        let htmlData = NSString(string: currentData["message"] as! String).data(using: String.Encoding.utf8.rawValue)
//        let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html ,  NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20)] as [AnyHashable : Any]
////        let options = [NSAttributedString.DocumentAttributeKey.documentType: NSAttributedString.DocumentType.html, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20)] as [AnyHashable : Any]
////
//        let attributedString = try! NSAttributedString(data: htmlData!,
//                                                       options: options as! [NSAttributedString.DocumentReadingOptionKey : Any] ,
//                                                       documentAttributes: nil)
//        // // print(attributedString)
//
//
//
//        // set attributed text on a UILabel
//        cell.lblSecond.attributedText = attributedString


//////second type end working code


//MARK:- from table didSelect
//        let userId = UserDefaults.standard.value(forKey: "userId") as! String
//        var dictOld = currentData
//        // // print(dictOld)
//
//        if let dictNew = dictOld.updateValue("read", forKey: "visible_status") {
//            // // print(dictNew)
//            // // print("The old value of \(dictNew) was replaced with a new one.")
//        }
//        // // print(dictOld)
//
//        let noti_String :String = "\(Helpers.notificationUploadstring)\(userId)_notification"
////        let noti_String :String = "\(userId)_notification"
//        //        "36_notification"
//        let ref = Database.database().reference(withPath: noti_String)
//                ref.child(currentKey).setValue(dictOld)



//MARK: - Date difference calculation

//    func dateDiff(dateStr:String) -> String {
//        var f:DateFormatter = NSDateFormatter()
//        f.timeZone = NSTimeZone.localTimeZone()
//        f.dateFormat = "yyyy-M-dd'T'HH:mm:ss.SSSZZZ"
//
//        var now = f.stringFromDate(NSDate())
//        var startDate = f.dateFromString(dateStr)
//        var endDate = f.dateFromString(now)
//        var calendar: NSCalendar = NSCalendar.currentCalendar
//
//        let calendarUnits = NSCalendar.Unit.CalendarUnitWeekOfMonth | NSCalendarUnit.CalendarUnitDay | NSCalendarUnit.CalendarUnitHour | NSCalendarUnit.CalendarUnitMinute | NSCalendarUnit.CalendarUnitSecond
//        let dateComponents = calendar.components(calendarUnits, fromDate: startDate!, toDate: endDate!, options: nil)
//
//        let weeks = abs(dateComponents.weekOfMonth)
//        let days = abs(dateComponents.day)
//        let hours = abs(dateComponents.hour)
//        let min = abs(dateComponents.minute)
//        let sec = abs(dateComponents.second)
//
//        var timeAgo = ""
//
//        if (sec > 0){
//            if (sec > 1) {
//                timeAgo = "\(sec) Seconds Ago"
//            } else {
//                timeAgo = "\(sec) Second Ago"
//            }
//        }
//
//        if (min > 0){
//            if (min > 1) {
//                timeAgo = "\(min) Minutes Ago"
//            } else {
//                timeAgo = "\(min) Minute Ago"
//            }
//        }
//
//        if(hours > 0){
//            if (hours > 1) {
//                timeAgo = "\(hours) Hours Ago"
//            } else {
//                timeAgo = "\(hours) Hour Ago"
//            }
//        }
//
//        if (days > 0) {
//            if (days > 1) {
//                timeAgo = "\(days) Days Ago"
//            } else {
//                timeAgo = "\(days) Day Ago"
//            }
//        }
//
//        if(weeks > 0){
//            if (weeks > 1) {
//                timeAgo = "\(weeks) Weeks Ago"
//            } else {
//                timeAgo = "\(weeks) Week Ago"
//            }
//        }
//
//        // // print("timeAgo is===> \(timeAgo)")
//        return timeAgo;
//    }
/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destination.
 // Pass the selected object to the new view controller.
 }
 */
extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}
