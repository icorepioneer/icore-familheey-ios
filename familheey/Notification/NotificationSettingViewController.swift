//
//  NotificationSettingViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 11/09/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit

import Moya
import Alamofire
import SwiftyJSON

class NotificationSettingViewController: UIViewController ,FamilySelectionDelegate{
    
    func selctionCallback(SelectedGroupId: NSMutableArray) {
        print(SelectedGroupId)
        self.arrayOfFamilyId = SelectedGroupId
        //        self.isSelectedFamilies = true
        self.switchNotifications()
        
    }
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var arrayOfFamilyId = NSMutableArray()
    
    private var networkProvider                        = MoyaProvider<FamilyheeyApi>()
    var notificationSettingData = JSON()
    @IBOutlet weak var swithPublic: UISwitch!
    @IBOutlet weak var swithConversation: UISwitch!
    @IBOutlet weak var swithNotCompletely: UISwitch!
    @IBOutlet weak var swithSpecificFamily: UISwitch!
    @IBOutlet weak var swithAnnouncement: UISwitch!
    @IBOutlet weak var swithEvent: UISwitch!
    
    @IBOutlet weak var topFromFamilySelection: NSLayoutConstraint!
    @IBOutlet weak var heightFamilySelection: NSLayoutConstraint!
    @IBOutlet weak var viewOfFamilySelection: UIView!
    var ispublic:Bool!
    var isConversation:Bool!
    var isNotiCompletely:Bool!
    //    var isSelectedFamilies:Bool!
    var isAnnouncment:Bool!
    var isEvent:Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if appDelegate.noFamily{
            
            self.viewOfFamilySelection.isHidden = true
            self.heightFamilySelection.constant = 0
            self.topFromFamilySelection.constant = 0
            
        }
        else{
            self.viewOfFamilySelection.isHidden = false
            self.heightFamilySelection.constant = 40
            self.topFromFamilySelection.constant = 20
            
        }
        self.get_notification_settings()
        ispublic = false
    }
    @IBAction func backClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func switchPublicOnClick(_ sender: UISwitch) {
        if (sender.isOn == true){
            ispublic = true
        }
        else{
            ispublic = false
        }
        self.switchNotifications()
    }
    @IBAction func swithConversationOnClick(_ sender: UISwitch) {
        if (sender.isOn == true){
            isConversation = true
        }
        else{
            isConversation = false
        }
        self.switchNotifications()
        
    }
    @IBAction func swithNotCompletelyOnClick(_ sender: UISwitch) {
        if (sender.isOn == true){
            isNotiCompletely = true
            
        }
        else{
            isNotiCompletely = false
            
        }
        self.switchNotifications()
        
    }
    
    @IBAction func swithSpecificFamilyOnClick(_ sender: UISwitch) {
        if (sender.isOn == true){
            //            isSelectedFamilies = true
            let viewController = UIApplication.topViewController()
            //                      let arrayContent = ArrayOfMembershipData[sender.tag]
            
            let storyboard = UIStoryboard.init(name: "NotificationSetting", bundle: nil)
            let popOverVc = storyboard.instantiateViewController(withIdentifier: "NotifSelectFamilyViewController") as! NotifSelectFamilyViewController
            //                      popOverVc.ArrayOfFundRequestpData = [arrayContent]
            popOverVc.selectedGroupIDArr = self.arrayOfFamilyId
            popOverVc.modalTransitionStyle = .crossDissolve
            popOverVc.selectionDelegate = self
            popOverVc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            viewController!.present(popOverVc, animated: true)
            
        }
        else{
            //            isSelectedFamilies = false
            //            self.arrayOfFamilyId = NSMutableArray()
            //            self.switchNotifications()
            let viewController = UIApplication.topViewController()
            //                      let arrayContent = ArrayOfMembershipData[sender.tag]
            
            let storyboard = UIStoryboard.init(name: "NotificationSetting", bundle: nil)
            let popOverVc = storyboard.instantiateViewController(withIdentifier: "NotifSelectFamilyViewController") as! NotifSelectFamilyViewController
            //                      popOverVc.ArrayOfFundRequestpData = [arrayContent]
            popOverVc.selectedGroupIDArr = self.arrayOfFamilyId
            popOverVc.modalTransitionStyle = .crossDissolve
            popOverVc.selectionDelegate = self
            popOverVc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            viewController!.present(popOverVc, animated: true)
            
        }
        //        self.switchNotifications()
    }
    @IBAction func swithAnnouncementOnClick(_ sender: UISwitch) {
        if (sender.isOn == true){
            isAnnouncment = true
            
        }
        else{
            isAnnouncment = false
            
        }
        self.switchNotifications()
    }
    
    @IBAction func swithEventOnClick(_ sender: UISwitch) {
        if (sender.isOn == true){
            isEvent = true
            
        }
        else{
            isEvent = false
            
        }
        self.switchNotifications()
    }
    
    
    
    
    //MARK:- Web API
    func get_notification_settings(){
        
        if !Connectivity.isConnectedToInternet(){
            self.displayAlertChoice(alertStr: "No Internet! Please Check your connectivity", title: "") { (result) in
                self.get_notification_settings()
            }
        }
        var parameter =  [String : Any]()
        parameter = [
            "user_id" : UserDefaults.standard.value(forKey: "userId") as! String
        ]
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.get_notification_settings(parameter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    
                    if response.statusCode == 200{
                        print("jsonData : \(jsonData)")
                        self.notificationSettingData = jsonData[0]
                        self.updateUI()
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.get_notification_settings()
                        }
                    }
                        
                    else{
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    func updateUI()
    {
        
        
        if self.notificationSettingData["public_notification"].boolValue == true
            
        {
            self.swithPublic.isOn = true
            ispublic = true
        }
        else
        {
            self.swithPublic.isOn = false
            ispublic = false
            
        }
        if self.notificationSettingData["conversation_notification"].boolValue == true
            
        {
            self.swithConversation.isOn = true
            isConversation = true
        }
        else
        {
            self.swithConversation.isOn = false
            isConversation = false
            
        }
        if self.notificationSettingData["notification"].boolValue == true
            
        {
            self.swithNotCompletely.isOn = true
            isNotiCompletely = true
        }
        else
        {
            self.swithNotCompletely.isOn = false
            isNotiCompletely = false
            
        }
        if self.notificationSettingData["family_notification_off"].count == self.notificationSettingData["family_count"].intValue && self.notificationSettingData["family_notification_off"].count != 0
        {
            arrayOfFamilyId.removeAllObjects()
            
            for i in 0 ... self.notificationSettingData["family_notification_off"].count - 1
            {
                let value = self.notificationSettingData["family_notification_off"][i].intValue
                print(value)
                self.arrayOfFamilyId.add(value)
            }
            
            self.swithSpecificFamily.isOn = false
            //  isSelectedFamilies = false
            
            
            
        }
        else{
            
            if self.notificationSettingData["family_notification_off"].count != 0 || self.notificationSettingData["family_notification_off"].count == 0
                
            {
                self.swithSpecificFamily.isOn = true
                //                                        isSelectedFamilies = true
                arrayOfFamilyId.removeAllObjects()
                if self.notificationSettingData["family_notification_off"].count != 0
                {
                    for i in 0 ... self.notificationSettingData["family_notification_off"].count - 1
                    {
                        let value = self.notificationSettingData["family_notification_off"][i].intValue
                        print(value)
                        self.arrayOfFamilyId.add(value)
                    }
                }
                print(self.arrayOfFamilyId)
                
            }
            
        }
        
        if self.notificationSettingData["announcement_notification"].boolValue == true
            
        {
            self.swithAnnouncement.isOn = true
            isAnnouncment = true
        }
        else
        {
            self.swithAnnouncement.isOn = false
            isAnnouncment = false
            
        }
        if self.notificationSettingData["event_notification"].boolValue == true
            
        {
            self.swithEvent.isOn = true
            isEvent = true
        }
        else
        {
            self.swithEvent.isOn = false
            isEvent = false
            
        }
        
        
    }
    func switchNotifications(){
        
        
        if let id =  UserDefaults.standard.value(forKey: "userId") as? String{
            ActivityIndicatorView.show("Please wait....")
            var param = [String : Any]()
            param = ["id":id,"notification":self.isNotiCompletely!,"public_notification":self.ispublic!,"conversation_notification":self.isConversation!,"announcement_notification":isAnnouncment!,"event_notification" : isEvent! ,"family_notification_off":self.arrayOfFamilyId] as [String : Any]
            
            print(param)
            
            networkProvider.request(.editProfile(parameter: param)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                        if response.statusCode == 200{
                            self.get_notification_settings()
                        }
                        else if response.statusCode == 401 {
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                self.switchNotifications()
                            }
                        }
                        else{
                            self.displayAlert(alertStr: "Something went wrong, Please try again", title: "")
                            self.get_notification_settings()
                            
                        }
                        ActivityIndicatorView.hiding()
                        
                    }catch let err {
                        ActivityIndicatorView.hiding()
                        // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                    }
                case .failure( let error):
                    ActivityIndicatorView.hiding()
                    self.get_notification_settings()
                    
                    //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                    break
                }
            }
            
        }
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
