//
//  ContactsTableViewCell.swift
//  familheey
//
//  Created by Giri on 24/02/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit

class ContactsTableViewCell: UITableViewCell {

    @IBOutlet weak var buttonInvite: UIButton!
    @IBOutlet weak var viewRightButton: UIView!
    @IBOutlet weak var imageViewAvatar: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
