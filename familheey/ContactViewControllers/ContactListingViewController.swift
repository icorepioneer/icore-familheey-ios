//
//  ContactListingViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 11/02/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI
import Moya
import SwiftyJSON
import CoreData

class ContactListingViewController: UIViewController {
    @IBOutlet weak var tableviewContacts: UITableView!
    @IBOutlet weak var textfieldSearch: UITextField!
    //    var contactsList = [JSON]()
    //    var SearchData = [JSON]()
    var search = ""
    var groupID = ""
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    var contactsParams = NSMutableArray()
    var contactsLists = NSMutableArray()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var CountryCode: String {
        
        let currentLocale = NSLocale.current as NSLocale
        let countryCode = currentLocale.object(forKey: .countryCode) as! String
        _ = currentLocale.displayName(forKey: NSLocale.Key.countryCode, value: countryCode)
        return ViewController.getCountryCallingCode(countryRegionCode: countryCode)
        
    }
    
    override func viewDidLoad() {
        textfieldSearch.delegate = self
        self.tableviewContacts.delegate = self
        self.tableviewContacts.dataSource = self
        self.tableviewContacts.tableFooterView = UIView.init()
        
        self.requestAccess { (granted) in
            if granted{
                self.fetchContacts(granted: granted)
            }
            else{
                //                self.callSearchApi()
                self.displayAlertChoice(alertStr: "Failed to get contacts", title: "") { (str) in
                    self.navigationController?.popViewController(animated: true)
                    
                }
            }
        }
        //        fetchContacts(granted: <#Bool#>)
        
    }
    
    //MARK:- Button Actions
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func inviteAction(_ sender: UIButton) {
        guard let dic = contactsParams[sender.tag] as? NSMutableDictionary else{
            return
        }
        if  let isExistinApp =  dic["isExistinApp"] as? Bool,isExistinApp {
            AddtoDamily(userid: "\(String(describing: dic["user_id"]!))", index: sender.tag)
        }
        else{
            inviteUser(users: ["\(String(describing: dic["phone"]!))"], index: sender.tag)
        }
    }
    
    
    //MARK:- Add to Damily
    func AddtoDamily(userid:String,index:Int){
        
        APIServiceManager.callServer.sendMemberInvitation(url: EndPoint.inviteMember, fromId: UserDefaults.standard.value(forKey: "userId") as! String, toId:userid  , groupId: groupID, success: { (responseMdl) in
            
            guard let result = responseMdl as? inviteMemberResult else{
                return
            }
            ActivityIndicatorView.hiding()
            if result.statuscode == 200{
                
                if let dic = self.contactsParams[index] as? NSMutableDictionary {
                    dic["isReqExist"] = "pending"
                    self.contactsParams[index] = dic
                    let idexpath = IndexPath.init(row: index, section: 0)
                    self.tableviewContacts.reloadRows(at: [idexpath], with: .fade)
                }
                else{
                    self.displayAlert(alertStr: "Something went wrong, please try again later!", title: "")
                }
            }
            else{
                self.displayAlert(alertStr: "Something went wrong, please try again later!", title: "")
                
            }
            
        }) { (error) in
            ActivityIndicatorView.hiding()
            
        }
        
        
    }
    
    //MARK:- invite user
    
    func inviteUser(users:[String],index:Int){
        
        let userId = UserDefaults.standard.value(forKey: "userId") as! String
        let params = ["group_id":groupID, "phone":users,"country_code":CountryCode,"from_user":userId,"type":"group"] as [String : Any]
        print(params)
        
        ActivityIndicatorView.show("Please wait!")
        networkProvider.request(.mob_bulk(parameter: params)) { (result) in
            switch result{
            case .success( let response):
                
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        
                        if let dic = self.contactsParams[index] as? NSMutableDictionary {
                            dic["isinvited"] = true
                            self.contactsParams[index] = dic
                            let idexpath = IndexPath.init(row: index, section: 0)
                            self.tableviewContacts.reloadRows(at: [idexpath], with: .fade)
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.inviteUser(users: users, index: index)
                        }
                    }
                    else{
                        self.displayAlert(alertStr: "Something went wrong, please try again later!", title: "")
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    self.displayAlert(alertStr: "Something went wrong, please try again later!", title: "")
                    
                    ActivityIndicatorView.hiding()
                    
                }
            case .failure( let error):
                self.displayAlert(alertStr: "Something went wrong, please try again later!", title: "")
                
                ActivityIndicatorView.hiding()
                
                break
            }
        }
        
        
    }
    
    //MARK:- Setup Contacts
    
    private func setupDataToSync(){
        
        self.contactsParams = NSMutableArray()
        //        let count = Int(contactsLists.count / 100)
        if contactsLists.count <= 100{
            syncContacts(with: contactsLists as! Array<Any>, end: true)
            return
        }
        let arrays = (contactsLists as Array).chunked(into: 100)
        let serialQueue = DispatchQueue(label: "queuename")
        
        if arrays.count != 0{
            
            for (index,items) in arrays.enumerated(){
                serialQueue.sync {
                    
                    //                    print(".........................................................................................")
                    if index == arrays.count - 1 {
                        syncContacts(with: items, end: true)
                        print("....................................ended...................................")
                    }
                    else{
                        syncContacts(with: items, end: false)
                        print("....................................\(index)...................................")
                        
                        
                    }
                }
                
            }
            
        }
        
    }
    
    
    //MARK:- Sync Contacts
    
    private func syncContacts(with Data:Array<Any>, end:Bool){
        
        let Params = ["contacts":Data,"group_id":groupID,"country_code":CountryCode] as [String : Any]
        //        print(Params)
        ActivityIndicatorView.show("Please wait!")
        networkProvider.request(.PhoneContactStatus(parameter: Params)) { (result) in
            switch result{
            case .success( let response):
                
                do{
                    if response.statusCode == 200{
                        
                        if  let sJson = try? JSONSerialization.jsonObject(with: response.data, options: .mutableContainers) as? NSDictionary
                        {
                            print(sJson)
                            if let contactArray = sJson["contacts"] as? NSMutableArray{
                                self.contactsParams.addObjects(from: contactArray as! [Any])
                                if end{
                                    self.contactsLists = self.contactsParams.mutableCopy() as! NSMutableArray
                                }
                                self.tableviewContacts.reloadData()
                                ActivityIndicatorView.hiding()
                            }
                        }
                    }
                    
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                
                break
            }
        }
    }
    
    //MARK: - Fetch Contacts
    func requestAccess(completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        let store = CNContactStore()
        self.contactsParams = NSMutableArray.init()
        switch CNContactStore.authorizationStatus(for: .contacts) {
        case .authorized:
            completionHandler(true)
        case .denied:
            showSettingsAlert(completionHandler)
        case .restricted, .notDetermined:
            store.requestAccess(for: .contacts) { granted, error in
                if granted {
                    completionHandler(true)
                } else {
                    DispatchQueue.main.async {
                        self.showSettingsAlert(completionHandler)
                    }
                }
            }
        }
    }
    
    private func showSettingsAlert(_ completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        let alert = UIAlertController(title: nil, message: "This app requires access to Contacts to proceed. Go to Settings to grant access.", preferredStyle: .alert)
        if
            let settings = URL(string: UIApplication.openSettingsURLString),
            UIApplication.shared.canOpenURL(settings) {
            alert.addAction(UIAlertAction(title: "Open Settings", style: .default) { action in
                completionHandler(false)
                UIApplication.shared.open(settings)
            })
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { action in
            completionHandler(false)
        })
        present(alert, animated: true)
    }
    private func fetchContacts(granted:Bool) {
        print("Attempting to fetch contacts today..")
        
        let store = CNContactStore()
        self.contactsParams = NSMutableArray.init()
        self.contactsLists = NSMutableArray.init()
        /*store.requestAccess(for: .contacts) { (granted, err) in
         if let err = err {
         print("Failed to request access:", err)
         self.displayAlertChoice(alertStr: "Failed to get contacts", title: "") { (str) in
         self.navigationController?.popViewController(animated: true)
         
         }
         return
         }*/
        
        if granted {
            print("Access granted")
            //                ActivityIndicatorView.show("Please wait!")
            
            
            let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey]
            let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
            request.sortOrder = CNContactSortOrder.givenName
            
            do {
                
                try store.enumerateContacts(with: request, usingBlock: { (contact, stopPointerIfYouWantToStopEnumerating) in
                    
                    let con = NSMutableDictionary()
                    if contact.givenName.isEmpty{
                        con["name"] = "Unknown"
                    }
                    else{
                        var lastName = ""
                        if contact.familyName.isEmpty{
                            lastName = ""
                        }
                        else{
                            lastName = contact.familyName
                        }
                        con["name"] = contact.givenName + " " + lastName
                        
                    }
                    var gotPlus = false
                    if let num = contact.phoneNumbers.first?.value.stringValue{
                        gotPlus = num.hasPrefix("+")
                        con["phone"] = num
                    }
                    if let number = Int(contact.phoneNumbers.first?.value.stringValue.components(separatedBy: CharacterSet.decimalDigits.inverted).joined() ?? "") {
                        con["phone"] = "\(number)"
                        
                        if gotPlus{
                            con["phone"] = "+\(number)"
                        }
                        
                    }
                    if con["phone"] != nil{
                        self.contactsParams.add(con)
                    }
                    
                })
                if self.contactsParams.count != 0{
                    self.contactsLists = self.contactsParams
                    //                        ActivityIndicatorView.hiding()
                    DispatchQueue.main.async{
                        //                        self.syncContacts()
                        self.setupDataToSync()
                    }
                }
                else{
                    //                        ActivityIndicatorView.hiding()
                    
                    self.displayAlertChoice(alertStr: "No contacts found to sync", title: "") { (str) in
                        self.navigationController?.popViewController(animated: true)
                        
                    }
                }
                
            } catch let err {
                //                    ActivityIndicatorView.hiding()
                
                print("Failed to enumerate contacts:", err)
                self.displayAlertChoice(alertStr: "Failed to get contacts", title: "") { (str) in
                    self.navigationController?.popViewController(animated: true)
                    
                }
            }
            
        } else {
            //                ActivityIndicatorView.hiding()
            
            print("Access denied..")
            self.displayAlertChoice(alertStr: "Can't access contacts", title: "") { (str) in
                self.navigationController?.popViewController(animated: true)
            }
            
        }
        //        }
    }
    
}

extension ContactListingViewController: UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if string.isEmpty
        {
            search = String(search.dropLast())
        }
        else
        {
            search=textField.text!+string
        }
        
        print("..............\(search)")
        //        search = textField.text!
        if search == ""{
            contactsParams.removeAllObjects()
            contactsParams.addObjects(from: contactsLists as! [Any])
            tableviewContacts.reloadData()
            return true
        }
        let searchPredicate = NSPredicate(format: "name CONTAINS[C] %@", search)
        let  arrrDict = self.contactsLists.filter { searchPredicate.evaluate(with: $0) };
        contactsParams.removeAllObjects()
        contactsParams.addObjects(from: arrrDict)
        tableviewContacts.reloadData()
        
        return true
    }
    
}
extension ContactListingViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactsParams.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactsTableViewCell", for: indexPath) as! ContactsTableViewCell
        
        guard let dic = contactsParams[indexPath.row] as? NSMutableDictionary else{return cell}
        
        cell.buttonInvite.tag = indexPath.row
        if let name = dic["name"] as? String{
            cell.lblName.text = name
        }
        if let phone = dic["phone"] as? String{
            cell.lblNumber.text = phone
        }
        
        if let isExistInGroup = dic["isExistInGroup"] as? Bool, let isExistinApp =  dic["isExistinApp"] as? Bool {
            
            if isExistInGroup && isExistinApp{
                cell.buttonInvite.isUserInteractionEnabled = false
                cell.viewRightButton.alpha = 0.5
                cell.buttonInvite.setTitle("Member", for: .normal)
            }
            else if isExistinApp{
                if let reqExist = dic["isReqExist"] as? String, reqExist  .lowercased() == "not-send" || reqExist.lowercased() == "rejected"{
                    cell.buttonInvite.isUserInteractionEnabled = true
                    cell.viewRightButton.alpha = 1.0
                    cell.buttonInvite.setTitle("Add to Family", for: .normal)
                }
                else if let isexist =  dic["isReqExist"] as? String, isexist.lowercased() == "pending"{
                    cell.buttonInvite.isUserInteractionEnabled = false
                    cell.viewRightButton.alpha = 0.5
                    cell.buttonInvite.setTitle("Pending", for: .normal)
                }
                else {
                    cell.buttonInvite.isUserInteractionEnabled = true
                    cell.viewRightButton.alpha = 1.0
                    cell.buttonInvite.setTitle("Invite", for: .normal)
                }
                
            }
            else{
                cell.buttonInvite.isUserInteractionEnabled = true
                cell.viewRightButton.alpha = 1.0
                cell.buttonInvite.setTitle("Invite", for: .normal)
            }
        }
        else{
            cell.buttonInvite.isUserInteractionEnabled = true
            cell.viewRightButton.alpha = 1.0
            cell.buttonInvite.setTitle("Invite", for: .normal)
        }
        if let isinvited  = dic["isinvited"] as? Bool, isinvited == true{
            cell.buttonInvite.isUserInteractionEnabled = false
            cell.viewRightButton.alpha = 0.5
            cell.buttonInvite.setTitle("Invited", for: .normal)
        }
        return cell
    }
    
}
extension Array {
    func chunked(into size: Int) -> [[Element]] {
        return stride(from: 0, to: count, by: size).map {
            Array(self[$0 ..< Swift.min($0 + size, count)])
        }
    }
}
