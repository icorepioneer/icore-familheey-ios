//
//  FolderListViewController.swift
//  familheey
//
//  Created by familheey on 30/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class FolderListViewController: UIViewController, FolderCreateDelagate {
 
    var groupId = ""
    var isMember:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        addRightNavigationButton()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        getFolderLists()
    }

    //MARK:- Add right navigation
    func addRightNavigationButton(){
        let rightButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(onClickrightButtonAction))
        self.navigationItem.rightBarButtonItem = rightButtonItem
    }
    
    //MARK:- Web API
    func getFolderLists(){
        APIServiceManager.callServer.listAllFolders(url: EndPoint.listFolder, userId: UserDefaults.standard.value(forKey: "userId") as! String , groupId: groupId, success: { (responseMdl) in
            guard let result = responseMdl as? FolderListResult else{
                return
            }
            ActivityIndicatorView.hiding()
            
            if result.status_code == 200{
                self.isMember = result.isMember
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadData"), object: result)
            }
            
        }) { (error) in
            
        }
    }
    
    //MARK:- Button Actions
    @objc func onClickrightButtonAction(sender: UIBarButtonItem){
        if isMember == 1{
            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            let popOverVc = storyboard.instantiateViewController(withIdentifier: "addMember") as! AddMembersViewController
            popOverVc.groupId = groupId
            popOverVc.delegate = self
            popOverVc.isFromInvite = "false"
            popOverVc.modalTransitionStyle = .crossDissolve
            popOverVc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            self.present(popOverVc, animated: true)
        }
        else{
            self.displayAlert(alertStr: "You are not a member in this group", title: "")
        }
    }
    
    
    //MARK:- Add folder delegate
    func folderCreated() {
        getFolderLists()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
