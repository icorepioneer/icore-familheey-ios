//
//  FolderDetailsViewController.swift
//  familheey
//
//  Created by familheey on 01/10/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit
import YPImagePicker


class FolderDetailsViewController: UIViewController {
    @IBOutlet weak var clctnListView: UICollectionView!
    
    var folderId = 0
    var isMember = 0
    
    var config = YPImagePickerConfiguration()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        // Do any additional setup after loading the view.
         addRightNavigationButton()
        self.clctnListView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        self.clctnListView.delegate = self
        self.clctnListView.dataSource = self
        
        print(folderId)
    }
    
    
    
    //Web API
    func showItems(){
        
    }
    
    func uploadItems(){
        
    }
    
    //MARK:- Custom function
    func addRightNavigationButton(){
        let rightButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(onClickrightButtonAction))
        self.navigationItem.rightBarButtonItem = rightButtonItem
    }
    
    func selectImages(type:String){
        if type.lowercased() == "images"{
            config.library.mediaType = .photo
        }
        else{
            config.library.mediaType = .video
        }
        
        let picker = YPImagePicker(configuration: config)
        
        picker.didFinishPicking { [unowned picker] items, _ in
            if let photo = items.singlePhoto {
                print(photo.fromCamera) // Image source (camera or library)
                print(photo.image) // Final image selected by the user
                print(photo.originalImage) // original image selected by the user, unfiltered
               // print(photo.modifiedImage) // Transformed image, can be nil
             //   print(photo.exifMeta) // Print exif meta data of original image.
            }
            picker.dismiss(animated: true, completion: nil)
        }
        present(picker, animated: true, completion: nil)
    }
    
    //MARK:- Button Actions
    @objc func onClickrightButtonAction(sender: UIBarButtonItem){
        if isMember == 1{
            let arr = ["Image","Video","Documents","Audio"]
            showActionSheet(titleArr: arr as NSArray, title: "Upload") { (index) in
                if index == 0{
                    self.selectImages(type: "images")
                }
                else{
                    
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension FolderDetailsViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)  as! FolderDetailsView_CollectionViewCell
        
        cell.imgThumb.image = UIImage(named: "folder_icon")
        
        return cell
    }
    
    
}
