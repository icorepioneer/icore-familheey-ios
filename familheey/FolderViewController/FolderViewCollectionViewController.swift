//
//  FolderViewCollectionViewController.swift
//  familheey
//
//  Created by familheey on 30/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"
var listArr:[FolderDetails]?
var ismember = 0

class FolderViewCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
   
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(loadList(notification:)), name: NSNotification.Name(rawValue: "loadData"), object: nil)

        // Do any additional setup after loading the view.
    }
    
    //MARK:- Notification Method
    @objc func loadList(notification: NSNotification){
        let tempDic = notification.object as! FolderListResult
        ismember = tempDic.isMember
        listArr = tempDic.result
        self.collectionView.reloadData()
    }
    
    
    //MARK:- Button Actions
    @IBAction func onClickImageTouch(_ sender:UIButton, event: UIEvent){
        let touch: UITouch = event.allTouches!.first!
        if (touch.tapCount == 2) {
            // do action
            print("double tap")
            let storyboard = UIStoryboard.init(name: "second", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "FolderDetailsViewController") as! FolderDetailsViewController
            vc.folderId = listArr![sender.tag].folderId
            vc.isMember = ismember
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        if listArr?.isEmpty == false{
           return listArr!.count
        }
        else{
            return 0
        }
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! FolderListView_CollectionViewCell
            //let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        cell.lblFolderName.text = listArr![indexPath.row].folderName
        cell.btnMenu.tag = indexPath.row
        cell.imgFolder.tag = indexPath.row
        cell.btnImgTap.tag = indexPath.row
        
        cell.btnImgTap.addTarget(self, action: #selector(onClickImageTouch(_:event:)), for: .touchDownRepeat)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 160, height: 160)
    
    }
    

    // MARK: UICollectionViewDelegate
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("taped")
    }

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
