//
//  FolderListView_CollectionViewCell.swift
//  familheey
//
//  Created by familheey on 30/09/19.
//  Copyright © 2019 familheey. All rights reserved.
//

import UIKit

class FolderListView_CollectionViewCell: UICollectionViewCell, UIGestureRecognizerDelegate {
    @IBOutlet weak var imgFolder: UIImageView!
    @IBOutlet weak var lblFolderName: UILabel!
    @IBOutlet weak var shareView: UIView!
    @IBOutlet weak var btnMenu: UIButton!
    
    @IBOutlet weak var btnImgTap: UIButton!
    
}
