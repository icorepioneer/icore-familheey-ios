//
//  RequestContributionTableViewCell.swift
//  familheey
//
//  Created by Innovation Incubator on 30/03/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit

class RequestContributionTableViewCell: UITableViewCell {

    @IBOutlet weak var heightOfNonApp: NSLayoutConstraint!
    @IBOutlet weak var lblNonAppMem: UILabel!
    @IBOutlet weak var imgOfGreenTick: UIImageView!
    @IBOutlet weak var btnImage: UIButton!
    @IBOutlet weak var viewOfCall: UIView!
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var lblDateOfContributor: UILabel!
    @IBOutlet weak var lblPlaceOfContibutor: UILabel!
    @IBOutlet weak var lblNameOfContributor: UILabel!
    @IBOutlet weak var imgOfContributor: UIImageView!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var diagonalView: UIView!
    
    @IBOutlet weak var btnPay: UIButton!
    @IBOutlet weak var payNowView: UIView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var btnAcknowledgw: UIButton!
    @IBOutlet weak var imgOfMultipleCont: UIImageView!
    @IBOutlet weak var btnSayThanks: UIButton!
//    @IBOutlet weak var viewOfLocation: UIView!
    
    @IBOutlet weak var viewOfLocation: UIStackView!
    @IBOutlet weak var height_viewOfLocation: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
