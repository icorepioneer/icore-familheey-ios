//
//  RequestTableViewCell.swift
//  familheey
//
//  Created by Innovation Incubator on 27/03/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit

class RequestTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var outerView: UIView!
//    @IBOutlet weak var viewOfTotalQty: UIView!
    
    @IBOutlet weak var viewOfContribution: UIView!
    @IBOutlet weak var viewOfSupport: UIView!
    @IBOutlet weak var viewOfQtyNeeded: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var buttonViewContribution: UIButton!
    @IBOutlet weak var buttonSupport: UIButton!
    
    @IBOutlet weak var lblsupport: UILabel!
    @IBOutlet weak var lblNeededQy: UILabel!
//    @IBOutlet weak var lblTotalQty: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
