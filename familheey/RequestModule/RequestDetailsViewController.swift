//
//  RequestDetailsViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 27/03/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON


class RequestDetailsViewController: UIViewController {
    var fromNotiButton = false
    @IBOutlet weak var viewOfContributionSuccess: UIControl!
    @IBOutlet weak var viewOfContributionAdd: UIControl!
    @IBOutlet weak var tblVwRequestList: UITableView!
    
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblPostedInFamilyName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblNeedDate: UILabel!
    @IBOutlet weak var lblNeedLocation: UILabel!
    @IBOutlet weak var imageViewUserProfile: UIImageView!
    @IBOutlet weak var fundTypeVew: UIView!
    var isAnonymous = false
    var isReqAdmin = false
    
    @IBOutlet weak var anonymousSwitch: UISwitch!
    @IBOutlet weak var headerView: UIView!
    
    // contribution poup
    @IBOutlet weak var popupLblTitle: UILabel!
    @IBOutlet weak var popupLblQuantity: UILabel!
    @IBOutlet weak var popupTetxfieldQantity: UITextField!
    @IBOutlet weak var popupCallNow: UIButton!
    @IBOutlet weak var popupSupportbutton: UIButton!
    //
    @IBOutlet weak var lblMyContributn: UILabel!
    
    @IBOutlet weak var viewOfThreeDot: UIView!
    @IBOutlet weak var viewOfPayLaterDetail: UIControl!
    @IBOutlet weak var txtNote: UITextView!
    
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var requestDic = JSON()
    var requestId = ""
    var selectedIndex = 0
    var arrayOfRequestListItems = [JSON] ()
    var strPaylaterNote = ""
    var paymentType = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblVwRequestList.delegate = self
        self.tblVwRequestList.dataSource = self
        self.tblVwRequestList.rowHeight = UITableView.automaticDimension
        self.tblVwRequestList.estimatedRowHeight = 210
        self.viewOfContributionAdd.isHidden  = true
        self.viewOfContributionSuccess.isHidden  = true
        
        if !requestId.isEmpty{
            getRequestDetails(id: requestId)
        }
        else{
            
            if let items = requestDic["items"].array{
                self.arrayOfRequestListItems = items
            }
            
            
            if requestDic["user_id"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")" {
                
                self.viewOfThreeDot.isHidden = false
                
            }
            else{
                self.viewOfThreeDot.isHidden = true
                
                
            }
            if let groups = requestDic["to_groups"].array, groups.count > 0{
                if groups.count == 1{
                    self.lblPostedInFamilyName.text = "Posted in \(groups[0]["group_name"].string ?? "Unknown")"
                }
                else if groups.count == 2{
                    self.lblPostedInFamilyName.text = "Posted in \(groups[0]["group_name"].string ?? "Unknown") and 1 other family"
                }
                else{
                    self.lblPostedInFamilyName.text = "Posted in \(groups[0]["group_name"].string ?? "Unknown") and \(groups.count - 1) other families"
                }
            }
            else{
                self.lblPostedInFamilyName.text = "Posted in Public"
            }
            self.lblUsername.text = requestDic["full_name"].string ?? "Unknown"
            self.lblNeedLocation.text = requestDic["request_location"].string ?? "Unknown"
            self.lblDate.text = HomeTabViewController.dateFormatterCommon(dateStr: requestDic["created_at"].stringValue )
            let enddate = requestDic["end_date"].int
            //            !enddate.isEmpty{
            if enddate == 0{
                self.lblNeedDate.text = "\(EventsTabViewController.convertTimeStampToDate(timestamp: requestDic["start_date"].stringValue ))"
            }
            else{
                self.lblNeedDate.text = "\(EventsTabViewController.convertTimeStampToDate(timestamp: requestDic["start_date"].stringValue )) - \(EventsTabViewController.convertTimeStampToDate(timestamp: requestDic["end_date"].stringValue ))"
            }
            //                }
            //                else{
            //                    self.lblNeedDate.text = "\(EventsTabViewController.convertTimeStampToDate(timestamp: requestDic["start_date"].stringValue ))"
            //            }
            let proPic = requestDic["propic"].stringValue
            
            DispatchQueue.main.async {
                
                if proPic.count != 0{
                    let temp = "\(Helpers.imaginaryImageBaseUrl)width=250&height=250&url=\(Helpers.imageURl)\(BaseUrl.userImage)\(proPic )"
                    let imgUrl = URL(string: temp)
                    self.imageViewUserProfile.kf.indicatorType = .activity
                    
                    self.imageViewUserProfile.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                }
                else{
                    self.imageViewUserProfile.image = #imageLiteral(resourceName: "Male Colored")
                }
            }
            self.imageViewUserProfile.isTappable(id: requestDic["user_id"].intValue )
            
            self.tblVwRequestList.reloadData()
            if self.requestDic["user_id"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")"{
                popupCallNow.isUserInteractionEnabled = false
                popupCallNow.alpha = 0.5
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(requestUpdateAfterPayment(notification:)), name: Notification.Name("RequestUpdates"), object: nil)
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        if appDel.isRequestUpdated{
            appDel.isRequestUpdated = false
            getRequestDetails(id: requestId)
            if !viewOfContributionAdd.isHidden{
                self.viewOfContributionAdd.isHidden = true
            }
        }
        
    }
    //MARK:- button Actions
    @IBAction func topbarMenuAction( _ sender:UIButton){
        
        if self.requestDic["user_id"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")"{
            showActionSheet(titleArr: ["Edit","Delete"], title: "Select options") { (index) in
                if index == 0{
                    let stryboard = UIStoryboard.init(name: "PostRequest", bundle: nil)
                    let vc = stryboard.instantiateViewController(withIdentifier: "EditRequestViewController") as! EditRequestViewController
                    vc.requestDic = self.requestDic
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else if index == 1{
                    self.deleteRequestCreater(id: self.requestDic["post_request_id"].stringValue)
                    
                }
                else if index == 100{
                    //Default
                }
            }
        }
        else{
            showActionSheet(titleArr: ["Report"], title: "Select options") { (index) in
                if index == 0{
                    //report
                }
                else if index == 100{
                    //Default
                }
            }
        }
    }
    @IBAction func supportAction( _ sender:UIButton){
        popupTetxfieldQantity.text = ""
        selectedIndex = sender.tag
        let item = arrayOfRequestListItems[selectedIndex]
        popupLblTitle.text = item["request_item_title"].stringValue
        
        /*if let mine = item["contribute_item_quantity"].int, mine>0{
         popupTetxfieldQantity.text = "\(mine)"
         popupSupportbutton.setTitle("Update", for: .normal)
         }
         else{
         popupSupportbutton.setTitle("Contribute", for: .normal)
         }*/
        if requestDic["request_type"].stringValue.lowercased() == "general" || requestDic["request_type"].stringValue.isEmpty{
            self.fundTypeVew.isHidden = true
            popupSupportbutton.isHidden = false
            popupLblQuantity.text = "\(RequestDetailsViewController.subtact(total:item["item_quantity"].int ?? 0, contributed: item["received_amount"].int ?? 0)) of \(item["item_quantity"].int ?? 0) still needed"
            lblMyContributn.text = "My Contribution - \(item["sum"].int ?? 0)"
        }
        else{
            self.fundTypeVew.isHidden = false
            self.anonymousSwitch.isOn = false
            self.isAnonymous = false
            popupSupportbutton.isHidden = true
            popupLblQuantity.text = "$\(RequestDetailsViewController.subtact(total:item["item_quantity"].int ?? 0, contributed: item["received_amount"].int ?? 0)) of $\(item["item_quantity"].int ?? 0) still needed"
            lblMyContributn.text = "My Contribution - $\(item["sum"].int ?? 0)"
        }
        
        popupSupportbutton.setTitle("Contribute", for: .normal)
        self.viewOfContributionAdd.isHidden = false
        
    }
    @IBAction func btnAnonymousOnClick(_ sender: UISwitch) {
        if (sender.isOn == true){
            isAnonymous = true
            
        }
        else{
            isAnonymous = false
            
        }
        
    }
    @IBAction func viewContributionAction( _ sender:UIButton){
        let item = arrayOfRequestListItems[sender.tag]
        
        let storyboard = UIStoryboard.init(name: "PostRequest", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "RequestContributionViewController") as! RequestContributionViewController
        vc.post_request_item_id = item["item_id"].stringValue
        vc.post_request_item_name = item["request_item_title"].stringValue
        vc.requestDic = requestDic
        vc.isReqAdmin = self.isReqAdmin
        vc.item = item
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func ContributeAction( _ sender: UIButton){
        let item = arrayOfRequestListItems[selectedIndex]
        
        if popupTetxfieldQantity.text!.trimmingCharacters(in: .whitespacesAndNewlines).count != 0 && popupTetxfieldQantity.text?.count != 0{
            self.viewOfContributionSuccess.isHidden = true
            self.viewOfContributionAdd.isHidden = true
            
            self.fromNotiButton = false
            /*if let mine = item["contribute_item_quantity"].int, mine>0{
             updateContribution(id: item["contribute_id"].stringValue)
             }else{
             createContribution(id: item["item_id"].stringValue)
             }*/
            createContribution(id: item["item_id"].stringValue)
        }
        else{
            self.displayAlert(alertStr: "Please fill the quantity field!!", title: "")
        }
        
    }
    
    @IBAction func onClickPayNow(_ sender: Any) {
       
        if popupTetxfieldQantity.text!.trimmingCharacters(in: .whitespacesAndNewlines).count != 0 && popupTetxfieldQantity.text != "0"{
            let alert = UIAlertController(title: "Choose an Option", message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Credit/Debit", style: .default, handler: { _ in
                self.viewOfContributionAdd.isHidden = true
                                  
                                  self.fromNotiButton = false
                self.onPaynowCardSelect()
            }))
            
            alert.addAction(UIAlertAction(title: "Cash", style: .default, handler: { _ in
                self.paymentType = "cash"
                self.viewOfContributionAdd.isHidden = true
                                  
                                  self.fromNotiButton = false
                self.onPayLaterDetailsSaveAction()
            }))
            alert.addAction(UIAlertAction(title: "Cheque", style: .default, handler: { _ in
                self.paymentType = "cheque"
                self.viewOfContributionAdd.isHidden = true
                                  
                                  self.fromNotiButton = false
                self.onPayLaterDetailsSaveAction()
            }))
            alert.addAction(UIAlertAction(title: "Others", style: .default, handler: { _ in
                self.paymentType = "others"
                self.viewOfContributionAdd.isHidden = true
                                  
                                  self.fromNotiButton = false
                self.onPayLaterDetailsSaveAction()
            }))
            
            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
            /*If you want work actionsheet on ipad
             then you have to use popoverPresentationController to present the actionsheet,
             otherwise app will crash on iPad */
            
            self.present(alert, animated: true, completion: nil)
        }
        else{
            self.displayAlert(alertStr: "Please fill the quantity field with a valid quanity!!", title: "")
        }
        
    }
    
    
    @IBAction func CancelContributeAction( _ sender:Any){
        self.viewOfContributionAdd.isHidden = true
    }
    @IBAction func cancelContributeSuccessAction( _ sender:Any){
        self.viewOfContributionSuccess.isHidden = true
    }
    @IBAction func CallnowAction( _ sender:UIButton){
        let phone = self.requestDic["phone"].stringValue
        if !phone.isEmpty{
            RequestContributionViewController.dialNumber(number: phone)
        }
    }
    @IBAction func backAction( _ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func familyAction( _ sender:UIButton){
        if let to = self.requestDic["to_groups"].array, to.count != 0{
            let item = arrayOfRequestListItems[sender.tag]
            let storyboard = UIStoryboard.init(name: "PostSection", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "RequestFamilyListViewController") as! RequestFamilyListViewController
            vc.arrayFamilylist = to
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func onClikTick(_ sender: Any) {
        self.viewOfContributionSuccess.isHidden = true
    }
    
    @IBAction func onClickPaylaterSave(_ sender: Any) {
        let item = arrayOfRequestListItems[selectedIndex]
        strPaylaterNote = txtNote.text
        createContribution(id: item["item_id"].stringValue)
    }
    @IBAction func onClickPaylaterCancel(_ sender: Any) {
        self.viewOfPayLaterDetail.isHidden = true
    }
    @IBAction func onClickClosePaylaterDetails(_ sender: Any) {
        self.viewOfPayLaterDetail.isHidden = true
    }
    
    
    //MARK:- Paynow Options
    func onPaynowCardSelect(){
        self.view.endEditing(true)
        if popupTetxfieldQantity.text!.trimmingCharacters(in: .whitespacesAndNewlines).count != 0 && popupTetxfieldQantity.text?.count != 0{
            
            let temp = Int(popupTetxfieldQantity.text!)
            let amount = temp! * 100
            let togrp = self.requestDic["to_groups"].arrayValue
            let item = arrayOfRequestListItems[selectedIndex]
            ActivityIndicatorView.show("Please wait....")
            let param = ["amount":amount, "group_id":togrp[0]["id"].stringValue ,"user_id":UserDefaults.standard.value(forKey: "userId") as! String,"to_type":"request","to_subtype":"item","to_type_id":self.requestId,"to_subtype_id":item["item_id"].stringValue,"os":"ios","is_anonymous":self.isAnonymous] as [String:Any]
            
            networkProvider.request(.stripeCreatePaymentIntent(parameter: param)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                        print("view contents : \(jsonData)")
                        if response.statusCode == 200{
                            
                            if let urlStr = jsonData["weburl"].string, !urlStr.isEmpty{
                                self.viewOfContributionAdd.isHidden = true
                                self.appDelegate.isRequestUpdated = true
                                appDel.isRequestUpdated = true
                                appDel.paymentStart = true
                                let url = URL(string: urlStr)
                                UIApplication.shared.open(url!)
                            }
                        }
                        else if response.statusCode == 401 {
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                self.onPaynowCardSelect()
                            }
                        }
                        else{
                            self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                        }
                        ActivityIndicatorView.hiding()
                        
                    }catch let err {
                        ActivityIndicatorView.hiding()
                        self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                        
                        // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                    }
                case .failure( let error):
                    ActivityIndicatorView.hiding()
                    self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                    
                    //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                    break
                }
            }
            
            
        }
        else{
            self.displayAlert(alertStr: "Please fill the quantity field!!", title: "")
        }
    }
    
    func onPayLaterDetailsSaveAction(){
        self.txtNote.text = ""
        self.viewOfContributionAdd.isHidden = true
        self.viewOfPayLaterDetail.isHidden = false
    }
    
    //MARK:- subtract
    static func subtact(total: Int ,contributed: Int) -> String{
        let needed = total - contributed
        if needed < 0{
            return "0"
        }
        else{
            return "\(needed)"
        }
    }
    //MARK:- create contribution
    
    func createContribution(id:String){
        self.view.endEditing(true)
       
        var param = [String:Any]()
        let togrp = self.requestDic["to_groups"].arrayValue
        
        if popupTetxfieldQantity.text == "0"{
            self.displayAlert(alertStr: "Please support with a valid contribution", title: "")
            return
        }
        
        ActivityIndicatorView.show("Please wait....")
        if paymentType.isEmpty{
            if fundTypeVew.isHidden{
                param = (["contribute_item_quantity":popupTetxfieldQantity.text!, "post_request_item_id":id,"contribute_user_id":UserDefaults.standard.value(forKey: "userId") as! String] as [String:Any])
            }
            else{
                param = (["contribute_item_quantity":popupTetxfieldQantity.text!, "post_request_item_id":id,"contribute_user_id":UserDefaults.standard.value(forKey: "userId") as! String,"is_anonymous":self.isAnonymous,"group_id":togrp[0]["id"].stringValue] as [String:Any])
            }
            
        }
        else{
            param = (["contribute_item_quantity":popupTetxfieldQantity.text!, "post_request_item_id":id,"contribute_user_id":UserDefaults.standard.value(forKey: "userId") as! String,"payment_note":strPaylaterNote,"payment_type":paymentType,"is_anonymous":self.isAnonymous,"group_id":togrp[0]["id"].stringValue] as [String:Any])
        }
        
        print(param)
        networkProvider.request(.contribution_create(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        if !self.viewOfPayLaterDetail.isHidden{
                            
                            self.viewOfPayLaterDetail.isHidden = true
                        }
                        self.viewOfContributionAdd.isHidden = true
                        self.viewOfContributionSuccess.isHidden = false
                        self.getRequestDetails(id: self.requestDic["post_request_id"].stringValue)
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.createContribution(id: id)
                        }
                    }
                    else{
                        self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                    
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
        
    }
    //MARK:- delete Request
    
    func deleteRequestCreater(id:String){
        self.view.endEditing(true)
        
        let alert = UIAlertController(title: "Familheey", message: "Do you really want to delete this request? Deleting this request will delete it from everywhere you posted it to", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "DELETE", style: .destructive, handler: { _ in
            
            
            let param = ["post_request_id" : id,"user_id":UserDefaults.standard.value(forKey: "userId") as! String]
            
            ActivityIndicatorView.show("Please wait....")
            self.networkProvider.request(.need_delete(parameter: param)) { (result) in
                switch result{
                    
                case .success( let response):
                    do{
                        let jsonData =  JSON(response.data)
                        print("view contents : \(jsonData)")
                        if response.statusCode == 200{
                            
                            self.navigationController?.popViewController(animated: true)
                            
                        }
                        else if response.statusCode == 401 {
                            Helpers.getAccessToken { (accessToken) in
                                setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                                self.deleteRequestCreater(id: id)
                            }
                        }
                        else{
                            self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                        }
                        
                        ActivityIndicatorView.hiding()
                        
                    }catch _ {
                        ActivityIndicatorView.hiding()
                        self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                        
                    }
                case .failure( _):
                    ActivityIndicatorView.hiding()
                    self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                    
                    break
                }
            }
            
        }))
        alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    //MARK:- update contribution
    
    func updateContribution(id:String){
        self.view.endEditing(true)
        
        ActivityIndicatorView.show("Please wait....")
        let param = ["contribute_item_quantity":popupTetxfieldQantity.text!, "id":id,"contribute_user_id":UserDefaults.standard.value(forKey: "userId") as! String]
        
        networkProvider.request(.contribution_update(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        self.viewOfContributionAdd.isHidden = true
                        self.viewOfContributionSuccess.isHidden = false
                        self.getRequestDetails(id: self.requestDic["post_request_id"].stringValue)
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.updateContribution(id: id)
                        }
                    }
                    else{
                        self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                    
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
        
    }
    
    
    
    //MARK:- get detail
    
    
    func getRequestDetails(id:String){
        self.view.endEditing(true)
        
        ActivityIndicatorView.show("Please wait....")
        let param = ["post_request_id":id,"user_id":UserDefaults.standard.value(forKey: "userId") as! String]
        print(param)
        networkProvider.request(.post_need_detail(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        self.isReqAdmin = jsonData["is_admin"].boolValue
                        if let need = jsonData["data"].dictionary{
                            
                            if (need["post_request_id"] != nil){
                                self.requestDic = jsonData["data"]
                                if let items = need["items"]?.array{
                                    self.arrayOfRequestListItems = items
                                }
                                
                                if self.requestDic["user_id"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")" {
                                    
                                    self.viewOfThreeDot.isHidden = false
                                    
                                }
                                else{
                                    self.viewOfThreeDot.isHidden = true
                                    
                                    
                                }
                                if let groups = need["to_groups"]?.array, groups.count > 0{
                                    if groups.count == 1{
                                        self.lblPostedInFamilyName.text = "Posted in \(groups[0]["group_name"].string ?? "Unknown")"
                                    }
                                    else if groups.count == 2{
                                        self.lblPostedInFamilyName.text = "Posted in \(groups[0]["group_name"].string ?? "Unknown") and 1 other family"
                                    }
                                    else{
                                        self.lblPostedInFamilyName.text = "Posted in \(groups[0]["group_name"].string ?? "Unknown") and \(groups.count - 1) other families"
                                    }
                                }
                                else{
                                    self.lblPostedInFamilyName.text = "Posted in Public"
                                }
                                
                                self.lblUsername.text = need["full_name"]?.string ?? "Unknown"
                                self.lblNeedLocation.text = need["request_location"]?.string ?? "Unknown"
                                self.lblDate.text = HomeTabViewController.dateFormatterCommon(dateStr: need["created_at"]?.stringValue ?? "")
                                //                                if let enddate = need["end_date"]?.intValue, !enddate.isEmpty{
                                let enddate = need["end_date"]?.intValue
                                if enddate == 0{
                                    self.lblNeedDate.text = "\(EventsTabViewController.convertTimeStampToDate(timestamp: need["start_date"]?.stringValue ?? ""))"
                                }
                                else{
                                    self.lblNeedDate.text = "\(EventsTabViewController.convertTimeStampToDate(timestamp: need["start_date"]?.stringValue ?? "")) - \(EventsTabViewController.convertTimeStampToDate(timestamp: need["end_date"]?.stringValue ?? ""))"
                                }
                                //                                }
                                //                                else{
                                //                                    self.lblNeedDate.text = "\(EventsTabViewController.convertTimeStampToDate(timestamp: need["start_date"]?.stringValue ?? ""))"
                                //                                }
                                
                                
                                let proPic = need["propic"]?.stringValue
                                
                                DispatchQueue.main.async {
                                    
                                    if proPic?.count != 0{
                                        let temp = "\(Helpers.imaginaryImageBaseUrl)width=250&height=250&url=\(Helpers.imageURl)\(BaseUrl.userImage)\(proPic ?? "")"
                                        let imgUrl = URL(string: temp)
                                        self.imageViewUserProfile.kf.indicatorType = .activity
                                        
                                        self.imageViewUserProfile.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
                                    }
                                    else{
                                        self.imageViewUserProfile.image = #imageLiteral(resourceName: "Male Colored")
                                    }
                                }
                                self.imageViewUserProfile.isTappable(id: need["user_id"]?.intValue ?? 0)
                                
                                self.tblVwRequestList.reloadData()
                                if self.fromNotiButton
                                {
                                    if self.arrayOfRequestListItems.count == 1
                                    {
                                        self.popupTetxfieldQantity.text = ""
                                        
                                        let item = self.arrayOfRequestListItems[0]
                                        self.popupLblTitle.text = item["request_item_title"].stringValue
                                        
                                        /*if let mine = item["contribute_item_quantity"].int, mine>0{
                                         popupTetxfieldQantity.text = "\(mine)"
                                         popupSupportbutton.setTitle("Update", for: .normal)
                                         }
                                         else{
                                         popupSupportbutton.setTitle("Contribute", for: .normal)
                                         }*/
                                        if self.requestDic["request_type"].stringValue.lowercased() == "general" || self.requestDic["request_type"].stringValue.isEmpty{
                                            self.fundTypeVew.isHidden = true
                                            self.popupSupportbutton.isHidden = false
                                            self.popupLblQuantity.text = "\(RequestDetailsViewController.subtact(total:item["item_quantity"].int ?? 0, contributed: item["received_amount"].int ?? 0)) of \(item["item_quantity"].int ?? 0) still needed"
                                            self.lblMyContributn.text = "My Contribution - \(item["sum"].int ?? 0)"
                                        }
                                        else{
                                            self.fundTypeVew.isHidden = false
                                            self.anonymousSwitch.isOn = false
                                            self.isAnonymous = false
                                            self.popupSupportbutton.isHidden = true
                                            self.popupLblQuantity.text = "$\(RequestDetailsViewController.subtact(total:item["item_quantity"].int ?? 0, contributed: item["received_amount"].int ?? 0)) of $\(item["item_quantity"].int ?? 0) still needed"
                                            self.lblMyContributn.text = "My Contribution - $\(item["sum"].int ?? 0)"
                                        }
                                        
                                        self.popupSupportbutton.setTitle("Contribute", for: .normal)
                                        self.viewOfContributionAdd.isHidden = false
                                        
                                    }
                                }
                                if self.requestDic["user_id"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")"{
                                    self.popupCallNow.isUserInteractionEnabled = false
                                    self.popupCallNow.alpha = 0.5
                                }
                            }
                            else{
                                self.displayAlert(alertStr: "Oops! This content is no longer available.", title: "")
                                self.navigationController?.popViewController(animated: true)
                            }
                        }
                        else{
                            
                            self.displayAlert(alertStr: "Oops! This content is no longer available.", title: "")
                            self.navigationController?.popViewController(animated: true)
                            
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getRequestDetails(id: id)
                        }
                    }
                    else if response.statusCode == 500{
                        
                        self.displayAlert(alertStr: "Sorry, details of this request are marked as private and cannot be displayed", title: "")
                        
                    }
                    else{
                        self.displayAlert(alertStr: "Oops! This content is no longer available.", title: "")
                        self.navigationController?.popViewController(animated: true)
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                    
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
        
    }
    
    @objc func requestUpdateAfterPayment(notification: Notification) {
        if appDel.isRequestUpdated && appDel.paymentStart{
            appDel.isRequestUpdated = false
            appDel.paymentStart = false
            getRequestDetails(id: requestId)
        }
    }
}


extension RequestDetailsViewController : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfRequestListItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        let item = arrayOfRequestListItems[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RequestTableViewCell", for: indexPath) as! RequestTableViewCell
        
        cell.selectionStyle = .none
        
        cell.buttonSupport.tag = indexPath.row
        cell.buttonViewContribution.tag = indexPath.row
        
        cell.lblDescription.text = item["request_item_description"].string ?? ""
        cell.lblTitle.text =  item["request_item_title"].string ?? ""
        if requestDic["request_type"].stringValue.lowercased() == "general" || requestDic["request_type"].stringValue.isEmpty{
            cell.lblNeededQy.text =  "\(RequestDetailsViewController.subtact(total:item["item_quantity"].int ?? 0, contributed: item["received_amount"].int ?? 0)) of \(item["item_quantity"].int ?? 0)"
            
            
            
        }
        else{
            cell.lblNeededQy.text =  "$\(RequestDetailsViewController.subtact(total:item["item_quantity"].int ?? 0, contributed: item["received_amount"].int ?? 0)) of $\(item["item_quantity"].int ?? 0)"
        }
        
        
        if let total = item["received_amount"].int, let quantity = item["item_quantity"].int{
            if total >= quantity{
                cell.lblStatus.text = "Completed"
                cell.lblStatus.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                cell.lblNeededQy.text = ""
            }
            else{
                cell.lblStatus.text = "Still needed"
            }
        }
        else{
            cell.lblStatus.text = "Still needed"
        }
        /* if let mine = item["contribute_item_quantity"].int, mine>0{
         cell.lblsupport.text = "Update"
         }
         else{
         cell.lblsupport.text = "Support"
         
         }*/
        cell.lblsupport.text = "Support"
        
        //        cell.lblStatus.text = "Still needed"
        //        if requestDic["user_id"].stringValue ==  "\(UserDefaults.standard.value(forKey: "userId") ?? "")"{
        cell.viewOfContribution.isHidden = false
        //        }
        //        else{
        //            cell.viewOfContribution.isHidden = true
        //        }
        //
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView
    }
    //    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    //        return UITableView.automaticDimension
    //    }
    //    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
    //        return UITableView.automaticDimension
    //    }
    //    
}
