//
//  RequestContri2ViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 08/06/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON
import Alamofire

class RequestContri2ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var currentContributeUser = [String:JSON]()
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    var arrayOfContributedUser = [JSON]()
    var requestDic = JSON()
    var item = JSON()
    var strPaylaterNote = ""
    var paymentType = ""
    var selectedIndex = 0
    var isReqAdmin = false
    
    @IBOutlet weak var imgViewNoData: UIImageView!
    @IBOutlet weak var tblViewOfContributors: UITableView!
    @IBOutlet weak var lblNoData: UILabel!
    
    @IBOutlet weak var viewOfNoData: UIView!
    @IBOutlet weak var viewOfPaylaterDetails: UIControl!
    
    @IBOutlet weak var txtNote: UITextView!
    
    @IBOutlet weak var viewOfContributionSuccess: UIControl!
    
    @IBOutlet weak var bgTopVew: UIView!
    @IBOutlet weak var clctnDefaultImage: UICollectionView!
    @IBOutlet weak var imgLeftArrow: UIImageView!
    @IBOutlet weak var imgRightArrow: UIImageView!
    @IBOutlet weak var txtThanksDesc: UITextView!
    @IBOutlet weak var btnPublishPost: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(requestDic)
        self.tblViewOfContributors.delegate = self
        
        self.apiContributionList()
        // Do any additional setup after loading the view.
    }
    // MARK Buttons actions
    @IBAction func btnBack_OnClick(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickPayNowAction(_ sender: UIButton) {
        selectedIndex = sender.tag
        let alert = UIAlertController(title: "Choose an Option", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Credit/Debit", style: .default, handler: { _ in
            self.onPaynowCardSelect(index: sender.tag)
        }))
        
        alert.addAction(UIAlertAction(title: "Cash", style: .default, handler: { _ in
            self.paymentType = "cash"
            self.onPayLaterDetailsSaveAction()
        }))
        alert.addAction(UIAlertAction(title: "Cheque", style: .default, handler: { _ in
            self.paymentType = "cheque"
            self.onPayLaterDetailsSaveAction()
        }))
        alert.addAction(UIAlertAction(title: "Others", style: .default, handler: { _ in
            self.paymentType = "others"
            self.onPayLaterDetailsSaveAction()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    @IBAction func onClickOutterView(_ sender: Any) {
        viewOfPaylaterDetails.isHidden = true
    }
    
    @IBAction func onClickDetailsSave(_ sender: Any) {
        strPaylaterNote = txtNote.text
        createContribution()
    }
    
    @IBAction func onClickDetailsCancel(_ sender: Any) {
        viewOfPaylaterDetails.isHidden = true
    }
    
    @IBAction func onClickViewSuccessOutterTouch(_ sender: Any) {
        self.viewOfContributionSuccess.isHidden = true
    }
    
    @IBAction func onClickGreenTick(_ sender: Any) {
        self.viewOfContributionSuccess.isHidden = true
    }
    @IBAction func onClickCall(_ sender: Any) {
        let phone = self.requestDic["phone"].stringValue
        if !phone.isEmpty{
            RequestContributionViewController.dialNumber(number: phone)
        }
    }
    
    @IBAction func onClickSayThanksAction(_ sender: UIButton) {
        
        
        let alert = UIAlertController(title: "Familheey", message: "Do you want to acknowledge this contribution ?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .destructive, handler: { _ in
            let currentContributeUser2 = self.arrayOfContributedUser[sender.tag].dictionaryValue
            
            self.apiContributionStatusUpdate(currentContributeUser :currentContributeUser2)
            
        }))
        alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
        
    }
    func apiContributionStatusUpdate(currentContributeUser :[String:JSON]) {
        
        print(currentContributeUser)
        
        let parameter = ["request_item_id": currentContributeUser["post_request_item_id"]!.stringValue, "contribute_user_id":currentContributeUser["contribute_user_id"]!.stringValue, "skip_thank_post":true,"contribution_id":currentContributeUser["id"]!.stringValue] as [String : Any]
        
        print(parameter)
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.requestcontributionStatusUpdation(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    print(jsonData["status"])
                    print(response.statusCode)
                    if response.statusCode == 200
                    {
                        self.apiContributionList()
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.apiContributionStatusUpdate(currentContributeUser: currentContributeUser)
                        }
                    }
                    else
                    {
                        
                        print(jsonData["code"])
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                    
                } catch let err {
                    ActivityIndicatorView.hiding()
                    
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    @IBAction func onClickPost(_ sender: Any) {
    }
    
    @IBAction func onClickSkipPost(_ sender: Any) {
    }
    
    // MARK:- Methods
    func onPaynowCardSelect(index:Int){
        
        let currentUser = self.arrayOfContributedUser[index].dictionaryValue
        
        let temp = Int(currentUser["contribute_item_quantity"]!.stringValue)
        let amount = temp! * 100
        let togrp = self.requestDic["to_groups"].arrayValue
        let contributeId = currentUser["id"]?.intValue
        var anonymous = "false"
        if currentUser["is_anonymous"]?.bool ?? false {
            anonymous = "true"
        }
        else{
            anonymous = "false"
        }
        
        ActivityIndicatorView.show("Please wait....")
        let param = ["amount":amount, "group_id":togrp[0]["id"].stringValue ,"user_id":UserDefaults.standard.value(forKey: "userId") as! String,"to_type":"request","to_subtype":"item","to_type_id":self.requestDic["post_request_id"].stringValue,"to_subtype_id":item["item_id"].stringValue,"os":"ios","contributionId": contributeId!,"is_anonymous":anonymous] as [String:Any]
        
        print(param)
        networkProvider.request(.stripeCreatePaymentIntent(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        
                        if let urlStr = jsonData["weburl"].string, !urlStr.isEmpty{
                            appDel.isRequestUpdated = true
                            appDel.paymentStart = true
                            let url = URL(string: urlStr)
                            UIApplication.shared.open(url!)
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.onPaynowCardSelect(index: index)
                        }
                    }
                    else{
                        self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                    
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    
    func onPayLaterDetailsSaveAction(){
        self.txtNote.text = ""
        self.viewOfPaylaterDetails.isHidden = false
    }
    
    //   MARK: - Api calling methods
    func createContribution(){
        self.view.endEditing(true)
        let currentUser = self.arrayOfContributedUser[selectedIndex].dictionaryValue
        let contributeId = currentUser["id"]?.intValue
        let togrp = self.requestDic["to_groups"].arrayValue
        var anonymous = "false"
        if currentUser["is_anonymous"]?.bool ?? false {
            anonymous = "true"
        }
        else{
            anonymous = "false"
        }
        ActivityIndicatorView.show("Please wait....")
        let param = ["contribute_item_quantity":currentUser["contribute_item_quantity"]!.stringValue, "post_request_item_id":item["item_id"].stringValue,"contribute_user_id":UserDefaults.standard.value(forKey: "userId") as! String,"payment_note":strPaylaterNote,"payment_type":paymentType,"contributionId": contributeId!,"is_anonymous":anonymous,"group_id":togrp[0]["id"].stringValue] as [String : Any]
        
        print(param)
        networkProvider.request(.contribution_create(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        if !self.viewOfPaylaterDetails.isHidden{
                            self.viewOfPaylaterDetails.isHidden = true
                        }
                        self.viewOfContributionSuccess.isHidden = false
                        self.apiContributionList()
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.createContribution()
                        }
                    }
                    else{
                        self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                    
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    func apiContributionList() {
        
        print(currentContributeUser)
        var parameter = [String : Any]()
        var userName:String = ""
        if let cId = currentContributeUser["contribute_user_id"]?.int, cId == 2{
            userName = currentContributeUser["paid_user_name"]?.string ?? ""
            
        }
        else
        {
            userName = currentContributeUser["full_name"]?.string ?? ""
        }
        
        
        if let type =  self.requestDic["request_type"].string, type.lowercased() == "fund"{
            var anonymous = "false"
            if currentContributeUser["is_anonymous"]?.bool ?? false {
                anonymous = "true"
            }
            else{
                anonymous = "false"
            }
            
            parameter = ["request_item_id": currentContributeUser["post_request_item_id"]!.stringValue, "contribute_use_id":currentContributeUser["contribute_user_id"]!.stringValue,"is_anonymous":anonymous,"user_name":userName] as [String : Any]
        }
        else{
            parameter = ["request_item_id": currentContributeUser["post_request_item_id"]!.stringValue,
                         "contribute_use_id":currentContributeUser["contribute_user_id"]!.stringValue,"user_name":userName] as [String : Any]
        }
        
        
        
        print(parameter)
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.requestContributionlistByUser(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    print(jsonData["status"])
                    print(response.statusCode)
                    if response.statusCode == 200
                    {
                        
                        self.arrayOfContributedUser = jsonData.arrayValue
                        print(self.arrayOfContributedUser)
                        ActivityIndicatorView.hiding()
                        
                        
                        if self.arrayOfContributedUser.count != 0
                        {
                            self.viewOfNoData.isHidden = true
                            self.imgViewNoData.isHidden = true
                            self.lblNoData.isHidden = true
                            
                            self.tblViewOfContributors.isHidden = false
                            self.tblViewOfContributors.dataSource = self
                            
                            self.tblViewOfContributors.reloadData()
                        }
                        else
                        {
                            self.viewOfNoData.isHidden = false
                            self.imgViewNoData.isHidden = false
                            self.lblNoData.isHidden = false
                            
                            self.tblViewOfContributors.isHidden = true
                        }
                        
                        
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.apiContributionList()
                        }
                    }
                    else
                    {
                        ActivityIndicatorView.hiding()
                        
                        
                        print(jsonData["code"])
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                } catch let err {
                    
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                    ActivityIndicatorView.hiding()
                    
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
    func formatDateString(dateString:String) -> String{
        if dateString.isEmpty{
            return "Nill"
        }
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        if let dateobj = dateFormatter.date(from: dateString) {
            dateFormatter.dateFormat = "MMM dd yyyy hh:mm a"
            dateFormatter.timeZone = .current
            return dateFormatter.string(from: dateobj)
        }
        else{
            return "Nill"
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfContributedUser.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Contri2TableViewCell", for: indexPath) as! Contri2TableViewCell
        cell.btnPayNw.tag = indexPath.row
        cell.btnSayThanks.tag = indexPath.row
        
        let currentContributeUsers = self.arrayOfContributedUser[indexPath.row].dictionaryValue
        
        print(currentContributeUsers)
        cell.heightOfNonApp.constant = 0
        cell.lblNonAppMem.isHidden = true
        
        if let cId = currentContributeUser["contribute_user_id"]?.int, cId == 2{
            if let name = currentContributeUser["paid_user_name"]?.string, name.isEmpty{
                
                cell.heightOfNonApp.constant = 15
                cell.lblNonAppMem.text =   "Non app member"
                cell.lblNonAppMem.isHidden = false
                
            }
            else{
                
                cell.heightOfNonApp.constant = 15
                cell.lblNonAppMem.text =   "Non app member"
                cell.lblNonAppMem.isHidden = false
            }
            
        }
        
        //        if let type = requestDic["user_id"].int, "\(type)" == (UserDefaults.standard.value(forKey: "userId") as! String) {
        
        if let anony = currentContributeUsers["is_anonymous"]?.bool, anony{
            cell.btnSayThanks.isHidden = true
            cell.imgTick.isHidden = true
            
            cell.imgOfUser.image = #imageLiteral(resourceName: "Male Colored")
            
            if isReqAdmin {
                cell.lblContributorName.text =  "\(currentContributeUser["full_name"]?.string ?? "Unknown") (Anonymous)"
            }
            else{
                
                if currentContributeUser["contribute_user_id"]?.stringValue  == (UserDefaults.standard.value(forKey: "userId") as! String){
                    cell.lblContributorName.text =  "\(currentContributeUser["full_name"]?.string ?? "Unknown") (Anonymous)"
                }
                else{
                    cell.lblContributorName.text = "Anonymous"
                }
            }
            
            if currentContributeUser["contribute_user_id"]?.stringValue  == (UserDefaults.standard.value(forKey: "userId") as! String){
                cell.lblPlaceOfContibutor.isHidden = false
                cell.lblPlaceOfContibutor.text = currentContributeUsers["location"]?.string ?? "Unknown"
            }
            else{
                cell.lblPlaceOfContibutor.isHidden = true
                cell.lblPlaceOfContibutor.text = currentContributeUsers["location"]?.string ?? "Unknown"
            }
            
        }
        else{
            cell.lblPlaceOfContibutor.isHidden = false
            
            cell.lblContributorName.text = currentContributeUsers["full_name"]?.string ?? "Unknown"
            cell.lblPlaceOfContibutor.text = currentContributeUsers["location"]?.string ?? "Unknown"
            
            let ContributePic = (currentContributeUsers["propic"]!.stringValue)
            if ContributePic != ""{
                let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+ContributePic
                let imgUrl = URL(string: temp)
                cell.imgOfUser.kf.indicatorType = .activity
                //            cell.imgOfUser.kf.setImage(with: imgUrl, placeholder: nil, options: [.forceRefresh], progressBlock: nil, completionHandler: nil)
                
                cell.imgOfUser.kf.setImage(with: imgUrl, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
            }
            else{
                cell.imgOfUser.image = #imageLiteral(resourceName: "Male Colored")
            }
        }
        
        if isReqAdmin &&  currentContributeUser["requested_by"]?.stringValue  != currentContributeUser["contribute_user_id"]?.stringValue{
            //            if !currentContributeUser["is_pending_thank_post"]!.boolValue{
            
            if currentContributeUsers["is_thank_post"]!.boolValue || currentContributeUsers["is_acknowledge"]!.boolValue
            {
                cell.btnSayThanks.isHidden = true
                cell.imgTick.isHidden = false
            }
            else{
                if let cId = currentContributeUser["contribute_user_id"]?.int, cId == 2{
                    cell.btnSayThanks.isHidden = true
                    cell.imgTick.isHidden = true
                }
                else{
                    cell.btnSayThanks.isHidden = false
                    cell.imgTick.isHidden = true
                }
                
            }
        }
        else{
              if currentContributeUser["requested_by"]?.stringValue == (UserDefaults.standard.value(forKey: "userId") as! String) {
                
                if currentContributeUsers["is_thank_post"]!.boolValue || currentContributeUsers["is_acknowledge"]!.boolValue
                {
                    cell.btnSayThanks.isHidden = true
                    cell.imgTick.isHidden = false
                }
                else{
                    if let cId = currentContributeUser["contribute_user_id"]?.int, cId == 2{
                        cell.btnSayThanks.isHidden = true
                        cell.imgTick.isHidden = true
                    }
                    else{
                        cell.btnSayThanks.isHidden = false
                        cell.imgTick.isHidden = true
                    }
                    
                }
            }
            else
              {
            cell.btnSayThanks.isHidden = true
            cell.imgTick.isHidden = true
            }
        }
        
        if currentContributeUsers["is_thank_post"]!.boolValue || currentContributeUsers["is_acknowledge"]!.boolValue {
            cell.diagonalView.backgroundColor = #colorLiteral(red: 0.3843137255, green: 0.9803921569, blue: 0.1803921569, alpha: 1)
            cell.btnPayNw.isHidden = true
        }
        else{
            if let type = currentContributeUsers["contribute_user_id"]?.int, "\(type)" == (UserDefaults.standard.value(forKey: "userId") as! String) {
                if let type =  self.requestDic["request_type"].string, type.lowercased() == "fund"{
                    cell.btnPayNw.isHidden = false
                }
                else{
                    cell.btnPayNw.isHidden = true
                }
            }
            else{
                cell.btnPayNw.isHidden = true
            }
            cell.diagonalView.backgroundColor = #colorLiteral(red: 0.9725490196, green: 0.9294117647, blue: 0.06666666667, alpha: 1)
        }
        
        
        
        cell.lblSlotCount.text = "\(currentContributeUsers["contribute_item_quantity"]!.stringValue)"
        cell.lblUpdateDate.text = "\( self.formatDateString(dateString:currentContributeUsers["created_at"]!.stringValue))"
        
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    

    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
