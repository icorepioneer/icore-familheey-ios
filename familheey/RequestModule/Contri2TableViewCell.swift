//
//  Contri2TableViewCell.swift
//  familheey
//
//  Created by Innovation Incubator on 10/06/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit

class Contri2TableViewCell: UITableViewCell {
    @IBOutlet weak var heightOfNonApp: NSLayoutConstraint!
       @IBOutlet weak var lblNonAppMem: UILabel!
    @IBOutlet weak var lblSlotCount: UILabel!
       @IBOutlet weak var lblUpdateDate: UILabel!
       @IBOutlet weak var lblContributorName: UILabel!
       @IBOutlet weak var imgOfUser: UIImageView!
    @IBOutlet weak var lblPlaceOfContibutor: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var diagonalView: UIView!
    @IBOutlet weak var btnPayNw: UIButton!
    @IBOutlet weak var viewOfLocation: UIView!
    @IBOutlet weak var btnSayThanks: UIButton!
    @IBOutlet weak var imgTick: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
