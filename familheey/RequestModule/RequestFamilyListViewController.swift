//
//  RequestFamilyListViewController.swift
//  familheey
//
//  Created by GiRi on 02/04/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit
import SwiftyJSON

class RequestFamilyListViewController: UIViewController {
    
    @IBOutlet weak var tableviewFamily : UITableView!

    var arrayFamilylist = [JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableviewFamily.delegate = self
        tableviewFamily.dataSource = self
        tableviewFamily.tableFooterView = UIView.init()
        tableviewFamily.reloadData()
    }
    
    //MARK:- Button Actions
    
    @IBAction func backbuttonAction(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension RequestFamilyListViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.arrayFamilylist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RequestFamilyListTableViewCell", for: indexPath) as! RequestFamilyListTableViewCell
        
        let famly = self.arrayFamilylist[indexPath.row]
        cell.lblitle.text = famly["group_name"].string ?? "Unknown"
        cell.lblLocation.text = famly["base_region"].string ?? "Unknown"
        cell.selectionStyle = .none
        if let logo = famly["logo"].string, logo.count != 0{
            let temp = "\(Helpers.imageURl)"+"\(BaseUrl.groupLogo)"+logo
            let imgurl = URL(string: temp)
            let newUrlStr = "\(Helpers.imaginaryImageBaseUrl)"+"width=250&height=250&url="+imgurl!.relativeString
            let url = URL(string: newUrlStr)
            cell.imageviewLogo.kf.indicatorType = .activity
            
            cell.imageviewLogo.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "imgNoImage"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        else{
            cell.imageviewLogo.image = #imageLiteral(resourceName: "Family Logo")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let famly = self.arrayFamilylist[indexPath.row]
        
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let intro = storyboard.instantiateViewController(withIdentifier: "FamilyDetailsTableViewController") as! FamilyDetailsTableViewController
        intro.groupId = famly["id"].stringValue
        self.navigationController?.pushViewController(intro, animated: true)
    }
    
}


class RequestFamilyListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageviewLogo : UIImageView!
    @IBOutlet weak var lblitle : UILabel!
    @IBOutlet weak var lblLocation : UILabel!
    
}
