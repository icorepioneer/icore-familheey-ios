//
//  RequestContributionViewController.swift
//  familheey
//
//  Created by Innovation Incubator on 30/03/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit
import Moya
import SwiftyJSON

class RequestContributionViewController: UIViewController,UITextFieldDelegate {
    var  totalneeded = 0
    @IBOutlet weak var imgRightArrow: UIImageView!
    @IBOutlet weak var imgLeftArrow: UIImageView!
    var sayThanksClickTag:Int = 0
    @IBOutlet weak var searchview: UIView!
    @IBOutlet weak var notificationViewHeight: NSLayoutConstraint!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var notificationView: UIView!
    @IBOutlet weak var btnSearchReset: UIButton!
    @IBOutlet weak var searchConstraintheight: NSLayoutConstraint!
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblTotalContributors: UILabel!
    @IBOutlet weak var lblNeededQty: UILabel!
    @IBOutlet weak var lblContributionItem: UILabel!
    @IBOutlet weak var tblVwContributionList: UITableView!
    @IBOutlet weak var bgCntrl: UIControl!
    @IBOutlet weak var txtThanksDesc: UITextView!
    @IBOutlet weak var btnPublishPost: UIButton!
    @IBOutlet weak var bgTopVew: UIView!
    @IBOutlet weak var clctnDefaultImage: UICollectionView!
    @IBOutlet weak var contributionAddView: UIControl!
    // contribution poup
    @IBOutlet weak var popupLblTitle: UILabel!
    @IBOutlet weak var popupLblQuantity: UILabel!
    @IBOutlet weak var popupTetxfieldQantity: UITextField!
    @IBOutlet weak var popupCallNow: UIButton!
    @IBOutlet weak var popupSupportbutton: UIButton!
    //
    @IBOutlet weak var lblMyContributn: UILabel!
    
    @IBOutlet weak var lblRecevedAmount: UILabel!
    
    @IBOutlet weak var lblPledgedAmount: UILabel!
    @IBOutlet weak var viewOfTotalAmount: UIStackView!
    
    @IBOutlet weak var viewOfPaylaterDetails: UIControl!
    @IBOutlet weak var txtNte: UITextView!
    
    @IBOutlet weak var viewOfContributionSuccess: UIControl!
    
    
    private var networkProvider = MoyaProvider<FamilyheeyApi>()
    var collectionViewLayout = UICollectionViewFlowLayout()
    
    var dataArr = JSON()
    var requestDic = JSON()
    var imgArr = [JSON]()
    var item = JSON()
    var arrayOfContributionList = [[String:String]] ()
    var ArrayPosts = [JSON]()
    
    let headerHeight: CGFloat = 60
    var post_request_item_id:String!
    var post_request_item_name:String!
    
    //Post params
    var selectedIdGroup   = NSMutableDictionary()
    var arrayOfSelectedUser = NSMutableArray()
    var postItemArr = NSMutableArray()
    var userIdArr = NSMutableArray()
    var currentIndex:Int = Int()
    var selectedAttachments = NSMutableArray()
    
    let privacyType = "Public"
    let categoryId = "3"
    let post_type = "only_groups"
    let type = "post"
    let postTitle = ""
    let postInfo = [String]()
    let is_shareable = false
    let conversation_enabled = true
    var attachmentIndex = 0
    var snapDesc = ""
    var strPaylaterNote = ""
    var paymentType = ""
    var selectedIndex = 0
    var isReqAdmin = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Helpers.setleftView(textfield: txtSearch, customWidth: 30)
        self.txtSearch.delegate = self
        closeSearch()
        self.tblVwContributionList.delegate = self
        self.tblVwContributionList.rowHeight = UITableView.automaticDimension
        self.tblVwContributionList.estimatedRowHeight = 190
        self.tabBarController?.tabBar.isHidden = true
        self.tblVwContributionList.tableFooterView = UIView.init()
        print(requestDic)
        getThankYouPostImages()
        getGroupIds()
        
        if let thanksId = requestDic["thank_post_id"].int, thanksId > 0{
            getPostDetails(PostId: "\(thanksId)")
        }
        
        self.bgTopVew.clipsToBounds = true
        bgTopVew.layer.cornerRadius = 20
        bgTopVew.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        self.clctnDefaultImage.delegate = self
        self.clctnDefaultImage.dataSource = self
        collectionViewLayout.scrollDirection = .horizontal
        collectionViewLayout.minimumLineSpacing = 0
        self.clctnDefaultImage.setCollectionViewLayout(collectionViewLayout, animated: true)
        self.viewOfTotalAmount.isHidden = true
        if let type =  self.requestDic["request_type"].string, type.lowercased() == "fund"{
            self.viewOfTotalAmount.isHidden = false
            self.lblRecevedAmount.text = "Received : 0"
            self.lblPledgedAmount.text = "Pledged : 0"
        }
        else{
            self.viewOfTotalAmount.isHidden = true
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getAllContributorsList()
        //        getUpdateForReceivedAndPledged()
    }
    override func viewDidLayoutSubviews(){
        super.viewDidLayoutSubviews()
        //        aboutClctnVew.reloadData()
        
    }
    
    //MARK:- Web API
    func getAllContributorsList(){
        var parameter =  [String : Any]()
        
        
        parameter = [ "user_id":UserDefaults.standard.value(forKey: "userId") as! String, "post_request_item_id":self.post_request_item_id!,"query":self.txtSearch.text!] as [String : Any]
        print(parameter)
        
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.GetRequestContributorsList(parameter: parameter)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    
                    if response.statusCode == 200{
                        print("jsonData : \(jsonData)")
                        
                        self.dataArr = jsonData["data"]
                        
                        if self.dataArr.count > 0{
                            let currentItemDetail = self.dataArr[0].dictionaryValue
                            self.lblContributionItem.text = currentItemDetail["request_item_title"]?.string ?? ""
                            self.lblTotalContributors.text = "\(self.dataArr.count) Contributors"
                            let totalneeded:Int = currentItemDetail["total_needed"]!.intValue
                            self.totalneeded = totalneeded
                            
                            
                            self.getUpdateForReceivedAndPledged()
                            self.tblVwContributionList.isHidden = false
                            self.noDataView.isHidden = true
                            self.tblVwContributionList.dataSource = self
                            self.tblVwContributionList.reloadData()
                            
                            
                        }
                        else{
                            self.tblVwContributionList.isHidden = true
                            self.noDataView.isHidden = false
                            self.viewOfTotalAmount.isHidden = true
                            
                        }
                        
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getAllContributorsList()
                        }
                    }
                    else{
                        
                        self.tblVwContributionList.isHidden = true
                        self.noDataView.isHidden = false
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    
                    self.tblVwContributionList.isHidden = true
                    self.noDataView.isHidden = false
                    
                    ActivityIndicatorView.hiding()
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                
                self.tblVwContributionList.isHidden = true
                self.noDataView.isHidden = false
                ActivityIndicatorView.hiding()
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    func getThankYouPostImages(){
        var parameter =  [String : Any]()
        parameter = [ "post_type":"request"] as [String : Any]
        networkProvider.request(.get_post_default_image(parameter: parameter)) { (result) in
            switch result{
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    
                    if response.statusCode == 200{
                        
                        self.imgArr = jsonData.arrayValue
                        if self.imgArr.count > 0{
                            print("jsonData : \(self.imgArr)")
                            
                            self.clctnDefaultImage.reloadData()
                            
                        }
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getThankYouPostImages()
                        }
                    }
                    else{
                        
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    func getPostDetails(PostId:String){
        let param = ["type":"post","post_id":PostId,"user_id":UserDefaults.standard.value(forKey: "userId") as! String]
        
        ActivityIndicatorView.show("Please wait....")
        networkProvider.request(.post_by_user(parameter: param)) { (result) in
            switch result{
            case .success( let response):
                do{
                    ActivityIndicatorView.hiding()
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        if let response = jsonData["data"].array{
                            self.ArrayPosts.append(contentsOf: response)
                            if self.ArrayPosts.count > 0 {
                                self.snapDesc = self.ArrayPosts[0]["snap_description"].stringValue
                                self.txtThanksDesc.text = self.snapDesc
                                if self.imgArr.count > 0{
                                    let temImgArr = self.ArrayPosts[0]["post_attachment"].arrayValue
                                    let tempId = temImgArr[0]["id"].stringValue
                                    let tempName = temImgArr[0]["filename"].stringValue
                                    print(tempName)
                                    print(tempId)
                                    print(self.imgArr)
                                    
                                    for (index,item) in self.imgArr.enumerated(){
                                        if tempName == item["filename"].stringValue{
                                            self.attachmentIndex = index
                                            break
                                        }
                                    }
                                    
                                }
                            }
                            else{
                                self.displayAlertChoice(alertStr: "Oops! This content is no longer available.", title: "", completion: { (result) in
                                    //                                    self.navigationController?.popViewController(animated: true)
                                })
                            }
                        }
                        else{
                            self.displayAlertChoice(alertStr: "Oops! This content is no longer available.", title: "", completion: { (result) in
                                self.navigationController?.popViewController(animated: true)
                            })
                        }
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getPostDetails(PostId: PostId)
                        }
                    }
                    else{
                        
                        self.displayAlertChoice(alertStr: "Oops! This content is no longer available.", title: "", completion: { (result) in
                            self.navigationController?.popViewController(animated: true)
                        })
                        
                        
                    }
                    //                        self.navigationController?.popViewController(animated: false)
                }catch let err {
                    ActivityIndicatorView.hiding()
                    //  Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    //Create post
    func publishPost(){
        var parameter = [String:Any]()
        
        parameter = ["category_id":categoryId, "type":type, "post_info":postInfo,"post_type":post_type,"title":postTitle, "is_shareable":is_shareable, "conversation_enabled":conversation_enabled, "privacy_type":privacyType, "selected_users":[], "snap_description":txtThanksDesc.text!, "post_attachment":selectedAttachments, "selected_groups": self.arrayOfSelectedUser,"created_by":UserDefaults.standard.value(forKey: "userId") as! String, "publish_type":"request", "publish_id":requestDic["post_request_id"].stringValue ,"publish_mention_users": userIdArr,"publish_mention_items":self.postItemArr] as [String : Any]
        print(parameter)
        
        ActivityIndicatorView.show("Please wait....")
        
        networkProvider.request(.publish_post(parameter: parameter)) { (result) in
            switch result{
            case .success( let response):
                do{
                    print(response)
                    let jsonData =  JSON(response.data)
                    print("jsonData : \(jsonData)")
                    if response.statusCode == 200{
                        self.displayAlertChoice(alertStr: "Post published successfully", title: "") { (result) in
                            self.bgCntrl.isHidden = true
                            self.bgTopVew.isHidden = true
                            self.getAllContributorsList()
                            
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.publishPost()
                        }
                    }
                    else{
                        print(response.statusCode)
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    //MARK:- Methods
    
    func getGroupIds(){
        if let toGroups = requestDic["to_groups"].array, toGroups.count > 0{
            var temp  = [[String:AnyObject]]()
            for index in toGroups{
                selectedIdGroup.removeAllObjects()
                
                if let Gid = index["id"].int{
                    self.selectedIdGroup.setValue("\(Gid)", forKey: "id")
                }
                
                if let postCreate = index["post_create"].int{
                    self.selectedIdGroup.setValue("\(postCreate)", forKey: "post_create")
                }
                temp.append(selectedIdGroup as! [String : AnyObject])
            }
            self.arrayOfSelectedUser.addObjects(from: temp)
            print(self.arrayOfSelectedUser)
        }
        let tempDic = NSMutableDictionary()
        tempDic.setValue(Int(self.post_request_item_id), forKey: "item_id")
        tempDic.setValue(self.post_request_item_name, forKey: "item_name")
        self.postItemArr.add(tempDic)
        print(self.postItemArr)
    }
    
    func formatDateString(dateString:String) -> String{
        if dateString.isEmpty{
            return "Nill"
        }
        let dateFormatter = DateFormatter()
        
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        if let dateobj = dateFormatter.date(from: dateString) {
            dateFormatter.dateFormat = "MMM dd yyyy hh:mm a"
            dateFormatter.timeZone = .current
            return dateFormatter.string(from: dateobj)
        }
        else{
            return "Nill"
        }
    }
    
    
    func openSearch(){
        self.notificationView.isHidden = false
        self.notificationViewHeight.constant = self.headerHeight
        self.searchview.isHidden = false
        self.searchConstraintheight.constant = self.headerHeight
        
        self.btnSearchReset.isHidden = false
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
            self.searchview.alpha = 1.0
        }, completion: nil)
    }
    
    func closeSearch(){
        DispatchQueue.main.async {
            
            UIView.animate(withDuration: 0.5, animations: {
                self.searchview.alpha = 0.0
                
            }) { (true) in
                
                self.searchview.isHidden = true
                self.searchConstraintheight.constant = 0
                self.notificationView.isHidden = false
                self.notificationViewHeight.constant = self.headerHeight
                
            }
        }
    }
    
    //MARK:- Button Ations
    @IBAction func btnBack_OnClick(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func searchAction(_ sender: Any) {
        openSearch()
    }
    @IBAction func onClickSearchReset(_ sender: Any) {
        txtSearch.text = ""
        getAllContributorsList()
        
        closeSearch()
        //        btnSearchReset.isHidden = true
        self.txtSearch.endEditing(true)
    }
    
    @IBAction func onClickSkip(_ sender: Any) {
        bgCntrl.isHidden = true
        bgTopVew.isHidden = true
        
    }
    @IBAction func onClickPostSkip(_ sender: Any) {
        bgCntrl.isHidden = true
        bgTopVew.isHidden = true
        
        print(self.sayThanksClickTag)
        
        let currentContributeUser2 = self.dataArr[self.sayThanksClickTag].dictionaryValue
        
        self.apiContributionStatusUpdate(currentContributeUser :currentContributeUser2)
    }
    
    @IBAction func onClickPublish(_ sender: Any) {
        let selectedImg = imgArr[currentIndex]
        self.attachmentIndex = currentIndex
        var temDic   = NSMutableDictionary()
        
        temDic.setValue(selectedImg["type"].stringValue, forKey: "type")
        //        temDic.setValue(selectedImg["post_type"].stringValue, forKey: "post_type")
        //        temDic.setValue(selectedImg["id"].stringValue, forKey: "id")
        temDic.setValue(selectedImg["filename"].stringValue, forKey: "filename")
        temDic.setValue(false, forKey: "is_play")
        
        temDic.setValue(selectedImg["height"].stringValue, forKey: "height")
        temDic.setValue(selectedImg["width"].stringValue, forKey: "width")
        
        selectedAttachments.add(temDic)
        print(selectedAttachments)
        self.publishPost()
    }
    
    @IBAction func onClickViewOutterTouch(_ sender: Any) {
        self.contributionAddView.isHidden = true
    }
    
    @IBAction func onClickSayThanks(_ sender: UIButton) {
        let user = dataArr[sender.tag]
        let tempDic = NSMutableDictionary()
        self.userIdArr.removeAllObjects()
        
        tempDic.setValue(Int(user["contribute_user_id"].stringValue), forKey: "user_id")
        tempDic.setValue(user["full_name"].stringValue, forKey: "user_name")
        
        self.userIdArr.add(tempDic)
        print(userIdArr)
        self.sayThanksClickTag = sender.tag
        bgCntrl.isHidden = false
        bgTopVew.isHidden = false
        if self.imgArr.count == 1
        {
            self.imgLeftArrow.isHidden = true
            self.imgRightArrow.isHidden = true
            
        }
        self.clctnDefaultImage.reloadData()
        print(attachmentIndex)
        let indexPath = IndexPath(item: attachmentIndex, section: 0)
        clctnDefaultImage.scrollToItem(at: indexPath, at:.centeredHorizontally, animated: true)
    }
    
    @IBAction func onClickAcknowledgeAction(_ sender: UIButton) {
        let currentContributeUser2 = self.dataArr[sender.tag].dictionaryValue
        
        self.apiContributionStatusUpdateAnonymous(currentContributeUser :currentContributeUser2)
    }
    
    
    @IBAction func onClickPayBtn(_ sender: UIButton) {
        selectedIndex = sender.tag
        let alert = UIAlertController(title: "Choose an Option", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Credit/Debit", style: .default, handler: { _ in
            self.onPaynowCardSelect(index: sender.tag)
        }))
        
        alert.addAction(UIAlertAction(title: "Cash", style: .default, handler: { _ in
            self.paymentType = "cash"
            self.onPayLaterDetailsSaveAction()
        }))
        alert.addAction(UIAlertAction(title: "Cheque", style: .default, handler: { _ in
            self.paymentType = "cheque"
            self.onPayLaterDetailsSaveAction()
        }))
        alert.addAction(UIAlertAction(title: "Others", style: .default, handler: { _ in
            self.paymentType = "others"
            self.onPayLaterDetailsSaveAction()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func onClickOutterViewAction(_ sender: Any) {
        viewOfPaylaterDetails.isHidden = true
    }
    
    @IBAction func onClickPaymentCancel(_ sender: Any) {
        viewOfPaylaterDetails.isHidden = true
    }
    
    @IBAction func onClickPaylaterSave(_ sender: Any) {
        strPaylaterNote = txtNte.text
        createContribution()
    }
    
    @IBAction func onClickViewOutterSuccessTouch(_ sender: Any) {
        self.viewOfContributionSuccess.isHidden = true
    }
    
    @IBAction func onClickCall(_ sender: Any) {
        let phone = self.requestDic["phone"].stringValue
        if !phone.isEmpty{
            RequestContributionViewController.dialNumber(number: phone)
        }
    }
    
    @IBAction func onClickGreenTick(_ sender: Any) {
        self.viewOfContributionSuccess.isHidden = true
    }
    
    //MARK:- Payment Methods
    func onPaynowCardSelect(index:Int){
        let currentUser = self.dataArr[index].dictionaryValue
        
        let temp = Int(currentUser["contribute_item_quantity"]!.stringValue)
        let amount = temp! * 100
        let togrp = self.requestDic["to_groups"].arrayValue
        let contributeId = currentUser["id"]?.intValue
        ActivityIndicatorView.show("Please wait....")
        var anonymous = "false"
        if currentUser["is_anonymous"]?.bool ?? false {
            anonymous = "true"
        }
        else{
            anonymous = "false"
        }
        let param = ["amount":amount, "group_id":togrp[0]["id"].stringValue ,"user_id":UserDefaults.standard.value(forKey: "userId") as! String,"to_type":"request","to_subtype":"item","to_type_id":self.requestDic["post_request_id"].stringValue,"to_subtype_id":item["item_id"].stringValue,"os":"ios","contributionId": contributeId!,"is_anonymous":anonymous] as [String:Any]
        
        print(param)
        networkProvider.request(.stripeCreatePaymentIntent(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        
                        if let urlStr = jsonData["weburl"].string, !urlStr.isEmpty{
                            appDel.isRequestUpdated = true
                            appDel.paymentStart = true
                            let url = URL(string: urlStr)
                            UIApplication.shared.open(url!)
                        }
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.onPaynowCardSelect(index: index)
                        }
                    }
                    else{
                        self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                    
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    func onPayLaterDetailsSaveAction(){
        self.txtNte.text = ""
        self.contributionAddView.isHidden = true
        self.viewOfPaylaterDetails.isHidden = false
    }
    
    //   MARK: - Api calling methods
    func createContribution(){
        self.view.endEditing(true)
        let currentUser = self.dataArr[selectedIndex].dictionaryValue
        let contributeId = currentUser["id"]?.intValue
        
        var anonymous = "false"
        if currentUser["is_anonymous"]?.bool ?? false {
            anonymous = "true"
        }
        else{
            anonymous = "false"
        }
        let togrp = self.requestDic["to_groups"].arrayValue
        
        ActivityIndicatorView.show("Please wait....")
        let param = ["contribute_item_quantity":currentUser["contribute_item_quantity"]!.stringValue, "post_request_item_id":item["item_id"].stringValue,"contribute_user_id":UserDefaults.standard.value(forKey: "userId") as! String,"payment_note":strPaylaterNote,"payment_type":paymentType,"contributionId": contributeId!,"is_anonymous":anonymous,"group_id":togrp[0]["id"].stringValue] as [String : Any]
        
        print(param)
        networkProvider.request(.contribution_create(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    if response.statusCode == 200{
                        if !self.viewOfPaylaterDetails.isHidden{
                            self.viewOfPaylaterDetails.isHidden = true
                        }
                        self.contributionAddView.isHidden = true
                        self.viewOfContributionSuccess.isHidden = false
                        self.getAllContributorsList()
                        //                        self.getUpdateForReceivedAndPledged()
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.createContribution()
                        }
                    }
                    else{
                        self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                    }
                    ActivityIndicatorView.hiding()
                    
                }catch let err {
                    ActivityIndicatorView.hiding()
                    self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                    
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                ActivityIndicatorView.hiding()
                self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    func getUpdateForReceivedAndPledged(){
        //        ActivityIndicatorView.show("Please wait....")
        let param = ["item_id":item["item_id"].stringValue] as [String : Any]
        
        print(param)
        networkProvider.request(.get_contribute_itemAmount_split(parameter: param)) { (result) in
            switch result{
                
            case .success( let response):
                do{
                    let jsonData =  JSON(response.data)
                    print("view contents : \(jsonData)")
                    //                    ActivityIndicatorView.hiding()
                    if response.statusCode == 200{
                        self.viewOfTotalAmount.isHidden = false
                        
                        let temp = jsonData[0].dictionaryValue
                        if let pledged = temp["pledged_amount"]?.int, pledged != 0{
                            self.lblPledgedAmount.text = "Pledged : \(pledged)"
                        }
                        else{
                            self.lblPledgedAmount.text = "Pledged : 0"
                        }
                        var receivedAmount = 0
                        if let received = temp["received_amount"]?.int, received != 0{
                            self.lblRecevedAmount.text = "Received : \(received)"
                            receivedAmount = received
                        }
                        else{
                            self.lblRecevedAmount.text = "Received : 0"
                            receivedAmount = 0
                            
                        }
                        self.lblNeededQty.text = "\(RequestDetailsViewController.subtact(total: self.totalneeded, contributed: receivedAmount)) of \(self.totalneeded)"
                        
                        if receivedAmount >= self.totalneeded{
                            self.lblStatus.text = "Completed"
                            self.lblStatus.textColor = #colorLiteral(red: 0.1215686275, green: 0.631372549, blue: 0.3215686275, alpha: 1)
                            self.lblNeededQty.text = ""
                        }
                        else{
                            self.lblStatus.text = "Still needed"
                        }
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.getUpdateForReceivedAndPledged()
                        }
                    }
                    else{
                        self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                    }
                    
                    
                }catch let err {
                    //                    ActivityIndicatorView.hiding()
                    self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                    
                    // Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
            case .failure( let error):
                //                ActivityIndicatorView.hiding()
                self.displayAlert(alertStr: "Oops! Something went wrong", title: "")
                
                //Helpers.showAlertDialog(message: error.localizedDescription, target: self)
                break
            }
        }
    }
    
    func apiContributionStatusUpdate(currentContributeUser :[String:JSON]) {
        
        print(currentContributeUser)
        
        let parameter = ["request_item_id": currentContributeUser["post_request_item_id"]!.stringValue, "contribute_user_id":currentContributeUser["contribute_user_id"]!.stringValue, "skip_thank_post":true] as [String : Any]
        
        print(parameter)
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.requestcontributionStatusUpdation(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    print(jsonData["status"])
                    print(response.statusCode)
                    if response.statusCode == 200
                    {
                        self.getAllContributorsList()
                        //                        self.getUpdateForReceivedAndPledged()
                        
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.apiContributionStatusUpdate(currentContributeUser: currentContributeUser)
                        }
                    }
                    else
                    {
                        
                        print(jsonData["code"])
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                    
                } catch let err {
                    ActivityIndicatorView.hiding()
                    
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
    func apiContributionStatusUpdateAnonymous(currentContributeUser :[String:JSON]) {
        
        print(currentContributeUser)
        
        let parameter = ["request_item_id": currentContributeUser["post_request_item_id"]!.stringValue, "contribute_user_id":currentContributeUser["contribute_user_id"]!.stringValue, "skip_thank_post":true,"is_anonymous":true] as [String : Any]
        
        print(parameter)
        ActivityIndicatorView.show("Loading....")
        networkProvider.request(.requestcontributionStatusUpdation(parameter: parameter), completion: {(result) in
            
            switch result {
                
            case .success(let response):
                
                do {
                    
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    print(jsonData["status"])
                    print(response.statusCode)
                    if response.statusCode == 200{
                        self.getAllContributorsList()
                        self.getUpdateForReceivedAndPledged()
                    }
                    else if response.statusCode == 401 {
                        Helpers.getAccessToken { (accessToken) in
                            setUserDefaultValues.setUserDefaultValue(userDefaultValue: accessToken!, userDefaultKey: "app_token")
                            self.apiContributionStatusUpdateAnonymous(currentContributeUser: currentContributeUser)
                        }
                    }
                    else{
                        print(jsonData["code"])
                        Helpers.showAlertDialog(message: jsonData["data"][0]["message"].string ?? "", target: self)
                    }
                    
                    ActivityIndicatorView.hiding()
                    
                    
                } catch let err {
                    ActivityIndicatorView.hiding()
                    
                    Helpers.showAlertDialog(message: err.localizedDescription, target: self)
                }
                
            case .failure(let error):
                
                Helpers.showAlertDialog(message: error.localizedDescription, target: self)
            }
            ActivityIndicatorView.hiding()
        })
    }
    
    //  MARK:- Textfield Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        textField.endEditing(true)
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        self.view.endEditing(true)
        if textField == txtSearch{
            getAllContributorsList()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
}

extension RequestContributionViewController : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArr.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RequestContributionTableViewCell", for: indexPath) as! RequestContributionTableViewCell
        
        cell.btnCall.tag = indexPath.row
        cell.btnImage.tag = indexPath.row
        
        cell.btnAcknowledgw.tag = indexPath.row
        cell.btnSayThanks.tag = indexPath.row
        cell.btnPay.tag = indexPath.row
        
        let currentContributeUser = self.dataArr[indexPath.row].dictionaryValue
        
        let ContributePic = (currentContributeUser["propic"]!.stringValue)
        
        
        cell.heightOfNonApp.constant = 0
        cell.lblNonAppMem.isHidden = true
        
        if  currentContributeUser["is_anonymous"]!.boolValue  {
            
            
            cell.imgOfContributor.image = #imageLiteral(resourceName: "Male Colored")
            
            if isReqAdmin {
                cell.lblNameOfContributor.text = "\(currentContributeUser["full_name"]?.string ?? "Unknown") (Anonymous)"
                cell.btnImage.isEnabled = true
                cell.viewOfLocation.isHidden = false
                //                cell.height_viewOfLocation.constant = 18
                
                if !currentContributeUser["is_acknowledge"]!.boolValue{
                    cell.btnSayThanks.isHidden = true
                    cell.btnAcknowledgw.isHidden = false
                    cell.imgOfGreenTick.isHidden = true
                }
                else{
                    cell.btnSayThanks.isHidden = true
                    cell.btnAcknowledgw.isHidden = true
                    cell.imgOfGreenTick.isHidden = false
                }
                
                if  currentContributeUser["requested_by"]?.stringValue  != currentContributeUser["contribute_user_id"]?.stringValue{
                    if let cId = currentContributeUser["contribute_user_id"]?.int, cId == 2{
                        cell.btnCall.isHidden  = true
                        cell.viewOfCall.isHidden  = true
                    }
                    else{
                        cell.btnCall.isHidden  = false
                        cell.viewOfCall.isHidden  = false
                    }
                    
                    
                }
                else{
                    cell.btnCall.isHidden  = true
                    cell.viewOfCall.isHidden  = true
                }
            }
            else{
                
                if currentContributeUser["requested_by"]?.stringValue == (UserDefaults.standard.value(forKey: "userId") as! String) {
                    
                    if !currentContributeUser["is_pending_thank_post"]!.boolValue{
                        cell.btnSayThanks.isHidden = true
                        cell.imgOfGreenTick.isHidden = false
                    }
                    else{
                        if let cId = currentContributeUser["contribute_user_id"]?.int, cId == 2{
                            cell.btnSayThanks.isHidden = true
                            cell.imgOfGreenTick.isHidden = true
                        }
                        else{
                            cell.btnSayThanks.isHidden = false
                            cell.imgOfGreenTick.isHidden = true
                        }
                        
                        
                    }
                    if let cId = currentContributeUser["contribute_user_id"]?.int, cId == 2{
                        cell.btnCall.isHidden  = true
                        cell.viewOfCall.isHidden  = true
                    }
                    else{
                        cell.btnCall.isHidden  = false
                        cell.viewOfCall.isHidden  = false
                    }
                    
                    //            if currentContributeUser["is_thank_post"]!.boolValue || currentContributeUser["skip_thank_post"]!.boolValue
                    
                    
                }
                else
                {
                    cell.btnSayThanks.isHidden = true
                    cell.btnAcknowledgw.isHidden = true
                    cell.imgOfGreenTick.isHidden = true
                    cell.btnCall.isHidden  = true
                    cell.viewOfCall.isHidden  = true
                }
                
                if currentContributeUser["contribute_user_id"]?.stringValue  == (UserDefaults.standard.value(forKey: "userId") as! String){
                    cell.viewOfLocation.isHidden = false
                    //   cell.height_viewOfLocation.constant = 18
                    cell.lblNameOfContributor.text =  "\(currentContributeUser["full_name"]?.string ?? "Unknown") (Anonymous)"
                    cell.btnImage.isEnabled = true
                }
                else{
                    cell.viewOfLocation.isHidden = true
                    // cell.height_viewOfLocation.constant = 0
                    cell.lblNameOfContributor.text =  "Anonymous"
                    cell.btnImage.isEnabled = false
                }
                
            }
        }
            
        else{
            cell.btnImage.isEnabled = true
            cell.viewOfLocation.isHidden = false
            cell.btnAcknowledgw.isHidden = true
            
            if let cId = currentContributeUser["contribute_user_id"]?.int, cId == 2{
                if let name = currentContributeUser["paid_user_name"]?.string, name.isEmpty{
                    cell.lblNameOfContributor.text =   "Unknown"
                    cell.heightOfNonApp.constant = 15
                    cell.lblNonAppMem.text =   "Non app member"
                    cell.lblNonAppMem.isHidden = false
                }
                else{
                    cell.lblNameOfContributor.text =  currentContributeUser["paid_user_name"]?.string ?? "Unknown"
                    cell.heightOfNonApp.constant = 15
                    cell.lblNonAppMem.text =   "Non app member"
                    cell.lblNonAppMem.isHidden = false
                    
                    
                }
                
            }
            else{
                cell.lblNameOfContributor.text =  currentContributeUser["full_name"]?.string ?? "Unknown"
            }
            
            
            
            if isReqAdmin{
                //                if !currentContributeUser["is_pending_thank_post"]!.boolValue{
                //                    cell.btnSayThanks.isHidden = true
                //                    cell.imgOfGreenTick.isHidden = false
                //                }
                //                else{
                //                    if let cId = currentContributeUser["contribute_user_id"]?.int, cId == 2{
                //                        cell.btnSayThanks.isHidden = true
                //                        cell.imgOfGreenTick.isHidden = true
                //                    }
                //                    else{
                //                        cell.btnSayThanks.isHidden = false
                //                        cell.imgOfGreenTick.isHidden = true
                //                    }
                //
                //
                //                }
                
                if !currentContributeUser["is_acknowledge"]!.boolValue {
                    cell.btnSayThanks.isHidden = false
                    cell.btnAcknowledgw.isHidden = true
                    cell.imgOfGreenTick.isHidden = true
                }
                else{
                    cell.btnSayThanks.isHidden = true
                    cell.btnAcknowledgw.isHidden = true
                    cell.imgOfGreenTick.isHidden = false
                }
                if  currentContributeUser["requested_by"]?.stringValue  != currentContributeUser["contribute_user_id"]?.stringValue  {
                    if let cId = currentContributeUser["contribute_user_id"]?.int, cId == 2{
                        cell.btnCall.isHidden  = true
                        cell.viewOfCall.isHidden  = true
                    }
                    else{
                        cell.btnCall.isHidden  = false
                        cell.viewOfCall.isHidden  = false
                    }
                    
                    //            if currentContributeUser["is_thank_post"]!.boolValue || currentContributeUser["skip_thank_post"]!.boolValue
                    
                    
                }
                else{
                    cell.btnCall.isHidden  = true
                    cell.viewOfCall.isHidden  = true
                    //                cell.btnSayThanks.isHidden = true
                    //                cell.imgOfGreenTick.isHidden = true
                }
            }
            else{
                if currentContributeUser["requested_by"]?.stringValue == (UserDefaults.standard.value(forKey: "userId") as! String) {
                    
                    if !currentContributeUser["is_pending_thank_post"]!.boolValue {
//                        cell.btnSayThanks.isHidden = false
//                        cell.imgOfGreenTick.isHidden = false
                        
                        cell.btnSayThanks.isHidden = true
                                               cell.imgOfGreenTick.isHidden = false
                    }
                    else{
                        if let cId = currentContributeUser["contribute_user_id"]?.int, cId == 2{
                            cell.btnSayThanks.isHidden = true
                            cell.imgOfGreenTick.isHidden = true
                        }
                        else{
                            cell.btnSayThanks.isHidden = false
                            cell.imgOfGreenTick.isHidden = true
                        }
                        
                        
                    }
                    if let cId = currentContributeUser["contribute_user_id"]?.int, cId == 2{
                        cell.btnCall.isHidden  = true
                        cell.viewOfCall.isHidden  = true
                    }
                    else{
                        cell.btnCall.isHidden  = false
                        cell.viewOfCall.isHidden  = false
                    }
                    
                    //            if currentContributeUser["is_thank_post"]!.boolValue || currentContributeUser["skip_thank_post"]!.boolValue
                    
                    
                }
                else
                {
                    
                    
                    cell.btnSayThanks.isHidden = true
                    cell.btnAcknowledgw.isHidden = true
                    cell.imgOfGreenTick.isHidden = true
                    cell.btnCall.isHidden  = true
                    cell.viewOfCall.isHidden  = true
                }
            }
            
            
            
            if ContributePic != ""{
                let temp = "\(Helpers.imaginaryImageBaseUrl)"+"width=100&height=100&url="+"\(Helpers.imageURl)"+"\(BaseUrl.userImage)"+ContributePic
                let imgUrl = URL(string: temp)
                cell.imgOfContributor.kf.indicatorType = .activity
                
                cell.imgOfContributor.kf.setImage(with: imgUrl, placeholder: #imageLiteral(resourceName: "Male Colored"), options: nil, progressBlock: nil, completionHandler: nil)
                
            }
            else{
                cell.imgOfContributor.image = #imageLiteral(resourceName: "Male Colored")
            }
        }
        
        if currentContributeUser["contribution_count"]!.intValue > 1{
            cell.imgOfMultipleCont.isHidden = false
            cell.bgView.backgroundColor = .white
            cell.payNowView.isHidden = true
            cell.diagonalView.isHidden = true
        }
        else{
            cell.imgOfMultipleCont.isHidden = true
            cell.diagonalView.isHidden = false
            //            if let type = requestDic["user_id"].int, "\(type)" == (UserDefaults.standard.value(forKey: "userId") as! String) {
            
            if currentContributeUser["is_thank_post"]!.boolValue  || currentContributeUser["is_acknowledge"]!.boolValue{
                cell.diagonalView.backgroundColor = #colorLiteral(red: 0.3843137255, green: 0.9803921569, blue: 0.1803921569, alpha: 1)
                cell.payNowView.isHidden = true
            }
            else{
                cell.diagonalView.backgroundColor = #colorLiteral(red: 0.9725490196, green: 0.9294117647, blue: 0.06666666667, alpha: 1)
                if let type = currentContributeUser["contribute_user_id"]?.int, "\(type)" == (UserDefaults.standard.value(forKey: "userId") as! String) {
                    if let type =  self.requestDic["request_type"].string, type.lowercased() == "fund"{
                        cell.payNowView.isHidden = false
                    }
                    else{
                        cell.payNowView.isHidden = true
                    }
                }
                else{
                    cell.payNowView.isHidden = true
                }
            }
            //            }
            //            else{
            //                cell.diagonalView.backgroundColor = .white
            //            }
            
        }
        cell.lblQty.text = "\(currentContributeUser["total_contribution"]!.intValue)"
        cell.lblPlaceOfContibutor.text = currentContributeUser["location"]?.string ?? "Unknown"
        cell.lblDateOfContributor.text = "\( self.formatDateString(dateString:currentContributeUser["created_at"]!.stringValue))"
        return cell
    }
    
    
    
    
    @IBAction func btnImage_onClick(_ sender: UIButton) {
        
        let storyboard = UIStoryboard.init(name: "second", bundle: nil)
        let intro = storyboard.instantiateViewController(withIdentifier: "UserProfileTableViewController") as! UserProfileTableViewController
        let currentContributeUser = self.dataArr[sender.tag].dictionaryValue
        intro.userID = currentContributeUser["contribute_user_id"]!.stringValue
        self.navigationController?.pushViewController(intro, animated: false)
    }
    
    @IBAction func btnCall_onClick(_ sender: UIButton) {
        
        let currentContributeUser = self.dataArr[sender.tag].dictionaryValue
        
        RequestContributionViewController.dialNumber(number: currentContributeUser["phone"]?.stringValue ?? "")
    }
    
    
    
    
    static func dialNumber(number : String) {
        
        if let url = URL(string: "tel://\(number)"),
            UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
            if let currentVC = UIApplication.topViewController(){
                currentVC.displayAlert(alertStr: "Not able to process your request!!", title: "Oops!")
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let currentContributeUser2 = self.dataArr[indexPath.row].dictionaryValue
        if currentContributeUser2["contribution_count"]!.intValue > 1
        {
            
            
            let storyBoard = UIStoryboard.init(name: "PostRequest", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "RequestContri2ViewController") as! RequestContri2ViewController
            
            let currentContributeUser2 = self.dataArr[indexPath.row].dictionaryValue
            vc.currentContributeUser  = currentContributeUser2
            vc.requestDic = self.requestDic
            vc.item = self.item
            vc.isReqAdmin = self.isReqAdmin
            //  loginView.regType = regType
            self.navigationController?.pushViewController(vc, animated: true)
        } 
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension RequestContributionViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "thanksPostImageCollectionViewCell", for: indexPath as IndexPath) as! thanksPostImageCollectionViewCell
        
        let type = imgArr[indexPath.row]
        if indexPath.row == 0
        {
            self.imgLeftArrow.isHidden = true
            self.imgRightArrow.isHidden = false
            
        }
        else if indexPath.row == imgArr.count-1
        {
            self.imgLeftArrow.isHidden = false
            self.imgRightArrow.isHidden = true
        }
        else{
            self.imgLeftArrow.isHidden = false
            self.imgRightArrow.isHidden = false
        }
        
        if let filename =  type["filename"].string, !filename.isEmpty{
            let temp = "\(Helpers.imageURl)"+"\(BaseUrl.post_image)"+type["filename"].stringValue
            
            let imgurl = URL(string:BaseUrl.imaginaryURLForThumb + temp )
            cell.imgDefaultPics.kf.setImage(with: imgurl, placeholder: #imageLiteral(resourceName: "Family Logo"), options: nil, progressBlock: nil, completionHandler: nil)
            
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        let type = imgArr[indexPath.row]
        
        /*  let story = UIStoryboard.init(name: "MembershipMngmnt", bundle: nil)
         let vc =  story.instantiateViewController(withIdentifier: "MemberTypeUserListViewController") as! MemberTypeUserListViewController
         vc.groupID = self.groupID
         vc.Typetitle = type["membership_name"].stringValue
         vc.typeItem = type
         self.navigationController?.pushViewController(vc, animated: true)*/
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: clctnDefaultImage.bounds.width , height: clctnDefaultImage.bounds.height)
        
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        for cell in clctnDefaultImage.visibleCells {
            let indexPath = clctnDefaultImage.indexPath(for: cell)
            print(indexPath)
            currentIndex = indexPath!.row
        }
    }
    
}
