//
//  ShareViewController.swift
//  shareExt
//
//  Created by familheey on 09/04/20.
//  Copyright © 2020 familheey. All rights reserved.
//

import UIKit
import Social
import MobileCoreServices


class ShareViewController: SLComposeServiceViewController {
    var imageType = ""
    
    private var urlString: String?
    private var textString: String?
    let contentTypeImage = kUTTypeImage as String
    let contentTypeURL = kUTTypeURL as String
    let contentTypeText = kUTTypeText as String
    let contectTypeVideo = kUTTypeMovie as String
    let defaults = UserDefaults(suiteName: "group.com.familheey.app")
    
    override func isContentValid() -> Bool {
        // Do validation of contentText and/or NSExtensionContext attachments here
        return true
    }
    
    override func viewDidLoad() {
//
//        self.clearData(fileName: "imageData")
//        self.clearData(fileName: "videoData")
        self.defaults?.set(nil, forKey: "imageData")
        self.defaults?.set(nil, forKey: "videoData")
        self.defaults?.setValue(nil, forKey: "urlString")
        self.defaults?.setValue(nil, forKey: "stringDesc")


        let extensionItem = extensionContext?.inputItems[0] as! NSExtensionItem
        DispatchQueue.main.async {
            
            for attachment in extensionItem.attachments! {
                if attachment.hasItemConformingToTypeIdentifier(self.contentTypeImage) {
                    
                    attachment.loadItem(forTypeIdentifier: self.contentTypeImage, options: nil) { data, error in
                        if error == nil {
                            let url = data as! URL
//                            self.defaults?.set(url, forKey: "imageData")
//                            self.defaults?.synchronize()
                            
                            do{
                                let data = try Data.init(contentsOf: url)
                                self.defaults?.set(data, forKey: "imageData")
                                self.defaults?.synchronize()
                                
                            }
                            catch{
                                print(error)
                            }
                            
                        }
                    }
                }
                if attachment.hasItemConformingToTypeIdentifier(self.contectTypeVideo) {
                    attachment.loadItem(forTypeIdentifier: self.contectTypeVideo, options: nil) { data, error in
                        if error == nil {
                            let url = data as! URL
                    
                            do{
                                let data = try Data.init(contentsOf: url)
                                self.defaults?.set(data, forKey: "videoData")
//                                self.defaults?.set(url, forKey: "videoData")
                                self.defaults?.synchronize()
                                
                            }
                            catch{
                                print(error)
                            }
                            
                            //self.defaults?.set(url, forKey: "videoData")
                        }
                    }
                }
                if attachment.hasItemConformingToTypeIdentifier(self.contentTypeURL) {
                    attachment.loadItem(forTypeIdentifier: self.contentTypeURL, options: nil) { data, error in
                        if error == nil {
                            let url = data as! URL?
                            self.urlString = url!.absoluteString
                            self.defaults?.setValue(self.urlString, forKey: "urlString")
                        }
                    }
                }
                if attachment.hasItemConformingToTypeIdentifier(self.contentTypeText) {
                    attachment.loadItem(forTypeIdentifier: self.contentTypeText, options: nil) { data, error in
                        if error == nil {
                            let str = data as! String?
                            self.defaults?.setValue(str, forKey: "stringDesc")
                        }
                    }
                }
            }
        }
        defaults?.synchronize()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewWillLayoutSubviews() {
        
    }
    override func viewWillAppear(_ animated: Bool) {
        redirectToHostApp()
    }
    override func didSelectPost() {
        
    }
    func saveData(fileName: String, data: Data) {

    var fileNameString = ""
        if fileName == "imageData"{
            fileNameString = fileName + ".png"
        }
        else {
            fileNameString = fileName + ".mov"
        }
     guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }

        let fileURL = documentsDirectory.appendingPathComponent(fileNameString)

                if fileName == "imageData"{
                    self.defaults?.set(fileURL, forKey: "imageData")
                                                self.defaults?.synchronize()
        }
                else{
                    self.defaults?.set(fileURL, forKey: "videoData")
                    self.defaults?.synchronize()

        }
        
        //Checks if file exists, removes it if so.
        if FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                try FileManager.default.removeItem(atPath: fileURL.path)
                print("Removed old data")
            } catch let removeError {
                print("couldn't remove file at path", removeError)
            }

        }

        do {
            try data.write(to: fileURL)
        } catch let error {
            print("error saving file with error", error)
        }

    }
    
    func clearData(fileName: String){
        guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        if FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                try FileManager.default.removeItem(atPath: fileURL.path)
                print("Removed old data")
            } catch let removeError {
                print("couldn't remove file at path", removeError)
            }
            
        }
    }

    
    override func configurationItems() -> [Any]! {
        // To add configuration options via table cells at the bottom of the sheet, return an array of SLComposeSheetConfigurationItem here.
        return []
    }
    func redirectToHostApp() {
        let url = URL(string: "familheey://share")
        var responder = self as UIResponder?
        let selectorOpenURL = sel_registerName("openURL:")
        
        while (responder != nil) {
            if (responder?.responds(to: selectorOpenURL))! {
                let _ = responder?.perform(selectorOpenURL, with: url)
            }
            responder = responder!.next
        }
        self.extensionContext!.completeRequest(returningItems: [], completionHandler: nil)
    }
}
